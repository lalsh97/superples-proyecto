
    window.onload = function( e ) {

        //Obtener la url actual
        var urlactual = window.location.href;
        var arrayurl = urlactual.split("/");

        var opcionfiltrado = arrayurl[4];

        if( opcionfiltrado == "Categorias" ) {

            document.getElementById("botonmodificar").innerText = "Modificar información de la categoría";

        } else if( opcionfiltrado == "Marcas" ) {

            document.getElementById("botonmodificar").innerText = "Modificar información de la marca";

        } else if( opcionfiltrado == "Presentaciones" ) {

            document.getElementById("botonmodificar").innerText = "Modificar información de la presentación";

        } else {

            document.getElementById("botonmodificar").innerText = "Modificar";

        }

    }

    function ObtenerURL() {

        //Obtener la url actual
        var urlactual = window.location.href;
        var arrayurl = urlactual.split("/");

        var opcionfiltrado = arrayurl[4];
        console.log("OPCION FILTRADO: ", opcionfiltrado );
        var filtro = arrayurl[5];
        console.log("FILTRO: ", filtro );
                        
        location.href ="/cambiarinformacionfiltrocatalogoproductos/"+ opcionfiltrado + "/" + filtro;

    }