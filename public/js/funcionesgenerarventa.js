

    window.onload = function( e ) {
        
        console.log("Pagina cargada");
        document.getElementById("codigo").focus();
    }
    
    var productoprecioventa = 0.0;
    var promocionprecionuevo = 0.0;
    var descuentoproducto = 0.0;
    var informacionpromocion;

    var textocantidadmaxinventario = 0;

    var arregloproductodescuentos = [];

    var pagoacredito = false;
    var cantidadadeudadareal;

    //Bandera para saber si el botón de agregar producto a sido presionado y no tener que cargar la lista de 
    //productos al modal listadoproductos más de una vez.
    var banderalistadoproductos = false;


        //Llamada a API - Producto
    function BuscarProducto() {

        var codigoproducto = $("#codigo").val();

        var ruta  = "/api/buscarproductoventa";
        var url = ruta + "/" + codigoproducto;
        console.log("Codigo a buscar: " , codigoproducto );

        //hace la búsqueda
        $.get( url ).done( function( data ) {    

            var content = JSON.parse( JSON.stringify( data ) );   
            console.log("Content: " , content );

            var textocodigo = content.Codigo;
            var textodescripcion = content.Descripcion;
            var textoprecioventa = content.PrecioVenta;
            textocantidadmaxinventario = content.Cantidad;

            if(  textocodigo === undefined ) {

                //console.log("Consulta vacía");
                document.getElementById("codigo").value = "";
                document.getElementById("descripcion").value = "";
                document.getElementById("precio").value = "";
                document.getElementById("cantidad").value = "";
                document.getElementById("codigo").focus();
                    
                //document.getElementById("textoalerta").value = "No hay ningún producto registrado con ese código de barras";
                document.getElementById("textoalerta").value = content;
                document.getElementById("contentalert").style.display = "block";

            } else {            

                //BuscarOfertaProducto( codigoproducto );
                console.log("Consula exitosa");

                var ruta = "/api/verificarpromocionesproducto";
                var url = ruta + "/" + codigoproducto;

                $.get( url ).done( function ( datapromocion ) {

                    var contenidopromocion = JSON.parse( JSON.stringify( datapromocion ) );
                    var tipopromocion = contenidopromocion[0].TipoPromocion;

                    console.log( "CONTENIDO PROMOCION: ", contenidopromocion );

                    var posicion = BuscarProductoArray( textocodigo );

                    if( posicion > -1 ) {

                        //CambiarMaximoCantidadInput( textocodigo, "Agregar" );
                        console.log("FUNCION CAMBIAR MAXIMOCANTIDADINPUT");

                        var retornomaximocantidad = CambiarMaximoCantidadInput( textocodigo, "Agregar" );

                    } else {
                        
                        document.getElementById('cantidad').setAttribute('max', textocantidadmaxinventario );

                    }


                    
                    if( retornomaximocantidad == 404 ) {           

                        console.log("IF 404");

                        document.getElementById("enlaceagregar").text = "Agregar";
                        document.getElementById("enlaceeliminar").style.display = "none";

                        document.getElementById("codigo").value = "";
                        document.getElementById("descripcion").value = "";
                        document.getElementById("cantidad").value = "";
                        document.getElementById("precio").value = "";
                        document.getElementById("importe").value = "";
                        
                        document.getElementById("cantidad").readOnly = true;
                        document.getElementById("codigo").readOnly = false;
                        document.getElementById("codigo").focus();

                    } else {
                        

                        if( tipopromocion == "0" ) {
                
                                    //SIN PROMOCION

                            informacionpromocion = "";
                            
                            textoprecioventa = ( Math.round( textoprecioventa * 100 ) / 100 ).toFixed( 2 );

                            document.getElementById("codigo").value = textocodigo;
                            document.getElementById("descripcion").value = textodescripcion;
                            document.getElementById("precio").value = textoprecioventa;
                            document.getElementById("cantidad").value = "1";
                            document.getElementById("cantidad").readOnly = false;

                            CalcularImporte();

                            document.getElementById("cantidad").focus();

                        } else if( tipopromocion == "1" ) {

                                    //PROMOCION - OFERTA

                            var tipoofertaaplicado;

                            if( contenidopromocion[0].TipoOferta == "1" ) {
                                tipoofertaaplicado = "2x1";
                            } else if( contenidopromocion[0].TipoOferta == "2" ) {
                                tipoofertaaplicado = "3x2";
                            } else if( contenidopromocion[0].TipoOferta == "3" ) {
                                tipoofertaaplicado = "4x3";
                            } else if( contenidopromocion[0].TipoOferta == "4" ) {
                                tipoofertaaplicado = "5x4";
                            }

                            informacionpromocion =  "TipoPromocion:" + tipopromocion + 
                                                    ", TipoOferta:" + contenidopromocion[0].TipoOferta + 
                                                    ", DescripcionPromocion:" + textodescripcion + " " + tipoofertaaplicado;

                            productoprecioventa = contenidopromocion[0].ProductoPrecioVenta;
                            promocionprecionuevo = contenidopromocion[0].PromocionPrecioNuevo;
                            promocionprecionuevo = ( Math.round( promocionprecionuevo * 100 ) / 100 ).toFixed( 2 );

                            descuentoproducto =   textoprecioventa - promocionprecionuevo;

                            descuentoproducto = ( Math.round( descuentoproducto * 100 ) / 100 ).toFixed( 2 );                        
                            
                            //
                            textoprecioventa = ( Math.round( textoprecioventa * 100 ) / 100 ).toFixed( 2 );

                            document.getElementById("codigo").value = textocodigo;
                            document.getElementById("descripcion").value = textodescripcion;
                            document.getElementById("precio").value = textoprecioventa;
                            //document.getElementById("precio").value = promocionprecionuevo;
                            document.getElementById("cantidad").value = "1";
                            document.getElementById("cantidad").readOnly = false;

                            CalcularImporte();

                            document.getElementById("cantidad").focus();


                        } else if( tipopromocion == "2" ) {

                                    //PROMOCION - DESCUENTO

                            console.log("DESCUENTO");

                            informacionpromocion =  "TipoPromocion:" + tipopromocion + 
                                                    ", TipoDescuento:" + contenidopromocion[0].TipoDescuento +                                                
                                                    ", DescripcionPromocion:" + textodescripcion + " con descuento del " + contenidopromocion[0].TipoDescuento + "%" ;                                                

                            productoprecioventa = contenidopromocion[0].ProductoPrecioVenta;
                            promocionprecionuevo = contenidopromocion[0].PromocionPrecioNuevo;
                            promocionprecionuevo = ( Math.round( promocionprecionuevo * 100 ) / 100 ).toFixed( 2 );

                            descuentoproducto =   textoprecioventa - promocionprecionuevo;

                            descuentoproducto = ( Math.round( descuentoproducto * 100 ) / 100 ).toFixed( 2 );                        

                            //
                            textoprecioventa = ( Math.round( textoprecioventa * 100 ) / 100 ).toFixed( 2 );

                            document.getElementById("codigo").value = textocodigo;
                            document.getElementById("descripcion").value = textodescripcion;
                            document.getElementById("precio").value = textoprecioventa;
                            //document.getElementById("precio").value = promocionprecionuevo;
                            document.getElementById("cantidad").value = "1";
                            document.getElementById("cantidad").readOnly = false;

                            CalcularImporte();

                            document.getElementById("cantidad").focus();

                        } else if( tipopromocion == "3" ) {

                                    //PROMOCION - PAQUETES

                            console.log("PAQUETES");

                            informacionpromocion = "TipoPromocion:" + tipopromocion + 
                                                ",PromocionPrecioNuevo:" + contenidopromocion[0].PromocionPrecioNuevo +                                            
                                                ",CodigoComplementarioPaquete:" + contenidopromocion[0].CodigoComplementarioPaquete +                                    
                                                ",PromocionPrecioComplementarioNuevo:" + contenidopromocion[0].PromocionPrecioComplementarioNuevo +                                            
                                                ",DescuentoPrecioComplementario:" + contenidopromocion[0].DescuentoPrecioComplementario +
                                                ",TotalPromocion:" + contenidopromocion[0].TotalPromocion +                                            
                                                ",DescripcionPromocion:" + contenidopromocion[0].DescripcionPromocion;

                            productoprecioventa = contenidopromocion[0].ProductoPrecioVenta;
                            console.log("PRODUCTOPRECIOVENTA AGREGAR: ", productoprecioventa );

                            promocionprecionuevo = contenidopromocion[0].PromocionPrecioNuevo;
                            promocionprecionuevo = ( Math.round( promocionprecionuevo * 100 ) / 100 ).toFixed( 2 );

                            descuentoproducto =   textoprecioventa - promocionprecionuevo;

                            descuentoproducto = ( Math.round( descuentoproducto * 100 ) / 100 ).toFixed( 2 );                        

                            //
                            textoprecioventa = ( Math.round( textoprecioventa * 100 ) / 100 ).toFixed( 2 );

                            document.getElementById("codigo").value = textocodigo;
                            document.getElementById("descripcion").value = textodescripcion;
                            document.getElementById("precio").value = textoprecioventa;
                            //document.getElementById("precio").value = promocionprecionuevo;
                            document.getElementById("cantidad").value = "1";
                            document.getElementById("cantidad").readOnly = false;

                            CalcularImporte();

                            document.getElementById("cantidad").focus();

                        }

                    }                                                

                });
                
                /*
                document.getElementById("codigo").value = textocodigo;
                document.getElementById("descripcion").value = textodescripcion;
                document.getElementById("precio").value = textoprecioventa;
                document.getElementById("cantidad").value = "1";
                document.getElementById("cantidad").readOnly = false;
                CalcularImporte();
                document.getElementById("cantidad").focus();
                */

            }

            /*
            document.getElementById("tablaproductos").insertRow(-1).innerHTML = 
            "<td>" + textocodigo + "</td>" +
            "<td>" + textodescripcion + "</td>" +
            "<td></td>" +
            "<td>" + textoprecioventa + "</td>" +
            "<td></td>";
            */
            
        }).catch(function( error) {
            console.log("El error es: ", error);
        });

    }

        //Llamada a API - Cliente
    function FiltrarClientesPorNombre() {

        var nombrecliente = $("#nombrecliente").val();

        if( nombrecliente != "" ) {

            var ruta  = "/api/filtrarclientespornombre";
            var url = ruta + "/" + nombrecliente;

            //hace la búsqueda
            $.get( url ).done( function( data ) {    
                var content = JSON.parse( JSON.stringify( data ) );  

                //Verificar si el contenido de la respuesta de la api no esta vacía.
                if( content.length > 0 ) {

                    var selectclientes = document.getElementById("idcliente");
                    
                    //Eliminar todas las opciones del select
                    for ( var i = selectclientes.length; i >= 0; i--) {
                        selectclientes.remove(i);
                    }

                        //Añadir una opción por defecto.
                    var optioncliente = document.createElement('option');
                        optioncliente.text = "Seleccione al cliente";
                        optioncliente.value = "";
                        selectclientes.add( optioncliente );

                        //Agregar las opciones de la respuesta de la consulta.
                    for( var k in content ) {

                        var optioncliente = document.createElement( 'option' );              
                        optioncliente.text = content[k].nombrecompleto;
                        optioncliente.value = content[k].idcliente; //id para el valor
                        selectclientes.add( optioncliente );

                    }

                    document.getElementById("nombrecliente").focus();

                } else {

                    //console.log("Consulta vacía");
                    document.getElementById("nombrecliente").focus();
                        
                    //document.getElementById("textoalerta").value = "No hay ningún producto registrado con ese código de barras";
                    //document.getElementById("contentalert").style.display = "block";

                }
                
            }).catch(function( error) {
                console.log("El error es: ", error);
            });

        } else {

            ActualizarSelectClientes();

        }

    }

    function InformacionCliente() {

        document.getElementById("nombrecliente").value = "";

        LimpiarInputsModal("CAMBIO");
        
        var select = document.getElementById("idcliente");
        var options = document.getElementsByTagName("option");

        console.log( "Select value: ", select.value );

        var idcliente = select.value;

        var ruta  = "/api/buscarinformacionclientecredito";
        var url = ruta + "/" + idcliente;

        //hace la búsqueda
        $.get( url ).done( function( data ) {    

            var content = JSON.parse( JSON.stringify( data ) );                       

            if( content.length == 0 ) {
                
                console.log( "Entra if 0" );

                document.getElementById("alertacreditocliente").style.display = "block";
                document.getElementById("textoalertacreditocliente").value = "El cliente aún no tiene habilitado el pago con crédito."

            } else {

                var limitecredito = content.LimiteCredito;
                limitecredito = ( Math.round( limitecredito * 100 ) / 100 ).toFixed( 2 );

                var informacioncredito = content.InformacionCreditoCliente;
                informacioncredito = ( Math.round( informacioncredito * 100 ) / 100 ).toFixed( 2 );

                document.getElementById("limitecredito").value = limitecredito;
                document.getElementById("informacioncredito").value = informacioncredito;
                cantidadadeudadareal = informacioncredito;

                document.getElementById("divfieldsettipopagocredito").style.display = "block";

            }                    

        }).catch(function( error) {
            console.log("El error es: ", error);
        });
    
    }

    function CambiarEstadoPagoCredito( caso ) {

        pagoacredito = caso;
        console.log( "Valor estado pagoacredito: ", pagoacredito );

        var total = document.getElementById("total").value;

        if( total == "" ) {
                    
            $(function () {
                $('#modalventacredito').modal('hide');
            });
            
            OcultarModal( "ERROR", "ERROR AL PROCESAR EL PAGO CON CRÉDITO", "Para procesar el pago de la venta con crédito, primero debe agregar algunos productos." );

        } else {
            
            if( pagoacredito == true ) {

                document.getElementById("enlaceenviar").style.visibility = "hidden";

                var importetotalventa = document.getElementById("total").value;

                if( importetotalventa == "" || isNaN( importetotalventa ) ) {

                    importetotalventa = 0.0;

                }  

                importetotalventa = ( Math.round( importetotalventa * 100 ) / 100 ).toFixed( 2 ); 
                document.getElementById("importetotalventa").value = importetotalventa;
                
                //ActualizarSelectClientes();

            } else if( pagoacredito == false ) {
                
                LimpiarInputsModal("CIERRE");

            }

        }

    }

    function ActualizarSelectClientes() {
        
        var url  = "/api/listadoselectclientes/";

        //hace la búsqueda
        $.get( url ).done( function( data ) {

            var content = JSON.parse( JSON.stringify( data ) );

            console.log( "Contenido clientes: ", content );

            var selectclientes = document.getElementById( 'idcliente' );            
            
            for ( var i = selectclientes.length; i >= 0; i--) {
                selectclientes.remove(i);
            }

            
            if( content.length > 0 ) {
                
                var optionclientedefault = document.createElement( 'option' );
                optionclientedefault.text = "Seleccione al cliente";
                optionclientedefault.value = "";
                selectclientes.add( optionclientedefault );

                for( var i = 0; i < content.length; i++ ) {

                    var optioncliente = document.createElement('option');
                    optioncliente.text = content[i].nombrecompleto;
                    optioncliente.value = content[i].idcliente;
                    selectclientes.add( optioncliente );

                }

            }

        });

    }

    function CalcularCambioPagoCredito() {

        var cambiocredito;            
        var keypresionada = event.which;

        if( ( keypresionada >= 48 && keypresionada <= 57 ) || ( keypresionada >=96 && keypresionada <= 105 ) ) {
        
            var importetotalventa = document.getElementById("total").value;
            var cantidadpagocredito = document.getElementById("cantidadpagocredito").value;

             if( isNaN( cantidadpagocredito ) ) {

                cambiocredito = 0;

            } else {

                cambiocredito =  cantidadpagocredito - importetotalventa;

            }

            if( cambiocredito <= 0 ) {

                cambiocredito = Math.abs( cambiocredito );
                cambiocredito = ( Math.round( cambiocredito * 100 ) / 100 ).toFixed( 2 );
                document.getElementById("cantidadadeberocredito").value = cambiocredito;

                var auxinformacioncredito = Number.parseFloat( cantidadadeudadareal ) + Number.parseFloat( cambiocredito );
                auxinformacioncredito = ( Math.round( auxinformacioncredito * 100 ) / 100 ).toFixed( 2 );
                
                document.getElementById("informacioncredito").value = auxinformacioncredito;

                cambiocredito = ( Math.round( cambiocredito * 100 ) / 100 ).toFixed( 2 );
                document.getElementById("cambiocredito").value = cambiocredito;

            } else {

                cambiocredito = ( Math.round( cambiocredito * 100 ) / 100 ).toFixed( 2 );
                document.getElementById("cambiocredito").value = cambiocredito;
                
                document.getElementById("cantidadadeberocredito").value = "0.00";

                document.getElementById("informacioncredito").value = cantidadadeudadareal;

            }

        } else if( ( keypresionada >= 65 && keypresionada <= 90 ) ) {

            console.log("entra else if");
                        
            var importetotalventa = document.getElementById("total").value;
            var cantidadpagocredito = document.getElementById("cantidadpagocredito").value;

            /*
            if( cantidadpagocredito == "" || isNaN( cantidadpagocredito ) ) {

                cambiocredito = 0;

            }

            cambiocredito = ( Math.round( cambiocredito * 100 ) / 100 ).toFixed( 2 );
            document.getElementById("cambiocredito").value = cambiocredito;
            */

            if( cantidadpagocredito == "" || isNaN( cantidadpagocredito ) ) {

                cambiocredito = 0;

            } else {

                cambiocredito =  cantidadpagocredito - importetotalventa;

            }
            

            if( cambiocredito <= 0 ) {

                console.log("if cambiocredito");

                document.getElementById("cantidadadeberocredito").value = "";

                var auxinformacioncredito = Number.parseFloat( cantidadadeudadareal ) + Number.parseFloat( cambiocredito );
                auxinformacioncredito = ( Math.round( auxinformacioncredito * 100 ) / 100 ).toFixed( 2 );
                
                document.getElementById("informacioncredito").value = auxinformacioncredito;

            } else {

                console.log("else cambiocredito");

                cambiocredito = ( Math.round( cambiocredito * 100 ) / 100 ).toFixed( 2 );
                document.getElementById("cambiocredito").value = cambiocredito;
                
                document.getElementById("cantidadadeberocredito").value = "";

                document.getElementById("informacioncredito").value = cantidadadeudadareal;

            }

        } else if( keypresionada == 8 ) {

            console.log("else if keypresionada 8");

            if( document.getElementById("cantidadpagocredito").value == "" ) {
                
                var importetotalventa = document.getElementById("total").value;
                
                document.getElementById("cambiocredito").value = "0.00";
                
                cambiocredito = ( Math.round( importetotalventa * 100 ) / 100 ).toFixed( 2 );
                document.getElementById("cantidadadeberocredito").value = importetotalventa;

            } else {
        
                var importetotalventa = document.getElementById("total").value;
                var cantidadpagocredito = document.getElementById("cantidadpagocredito").value;

                if( isNaN( cantidadpagocredito ) ) {

                    cambiocredito = 0;

                } else {

                    cambiocredito =  cantidadpagocredito - importetotalventa;

                }

                if( cambiocredito <= 0 ) {

                    if( cambiocredito >= importetotalventa ) {

                        cambiocredito = ( Math.round( cambiocredito * 100 ) / 100 ).toFixed( 2 );
                        document.getElementById("cambiocredito").value = cambiocredito;

                    } else {

                        cambiocredito = ( Math.round( cambiocredito * 100 ) / 100 ).toFixed( 2 );
                        cambiocredito = Math.abs( cambiocredito );
                        document.getElementById("cantidadadeberocredito").value = cambiocredito;

                        var auxinformacioncredito = Number.parseFloat( cantidadadeudadareal ) + Number.parseFloat( cambiocredito );
                        auxinformacioncredito = ( Math.round( auxinformacioncredito * 100 ) / 100 ).toFixed( 2 );
                        
                        document.getElementById("informacioncredito").value = auxinformacioncredito;

                        document.getElementById("informacioncredito").value = cantidadadeudadareal;

                    }

                } else {

                    cambiocredito = ( Math.round( cambiocredito * 100 ) / 100 ).toFixed( 2 );
                    document.getElementById("cambiocredito").value = cambiocredito;
                    
                    document.getElementById("cantidadadeberocredito").value = "";

                    document.getElementById("informacioncredito").value = cantidadadeudadareal;

                }

            }

        }

    }

    function MostrarFieldsetInformacionCredito( bandera ) {

        //true => Completamente             false=>Parcial

        document.getElementById("divfieldsetinformacioncredito").style.display = "block";

        if( bandera == true ) {

            document.getElementById("divcambiocredito").style.display = "none";
            document.getElementById("divcantidadpagocredito").style.display = "none";

            var limitecredito = document.getElementById("limitecredito").value;
            limitecredito = Number.parseFloat( limitecredito );
            limitecredito = ( Math.round( limitecredito * 100 ) / 100 ).toFixed( 2 );

            cantidadadeudadareal = Number.parseFloat( cantidadadeudadareal );
            cantidadadeudadareal = ( Math.round( cantidadadeudadareal * 100 ) / 100 ).toFixed( 2 );

            var importetotalventa = document.getElementById("importetotalventa").value;
            importetotalventa = Number.parseFloat( importetotalventa );
            importetotalventa = ( Math.round( importetotalventa * 100 ) / 100 ).toFixed( 2 );

            var cantidadadeudadatotal = Number.parseFloat( cantidadadeudadareal ) + Number.parseFloat( importetotalventa );
            cantidadadeudadatotal = ( Math.round( cantidadadeudadatotal * 100 ) / 100 ).toFixed( 2 );

            if( Number.parseFloat( cantidadadeudadatotal ) < Number.parseFloat( limitecredito )  ) {
                
                console.log( "IF menor limite");
                
                document.getElementById("informacioncredito").value = cantidadadeudadatotal;
                document.getElementById("botonmodalcreditoaceptar").style.visibility = "visible";

                document.getElementById("alertapagocredito").style.display = "none";

            } else {
                
                console.log( "ELSE menor limite");

                document.getElementById("informacioncredito").value = cantidadadeudadatotal;

                document.getElementById("botonmodalcreditoaceptar").style.visibility = "hidden";

                document.getElementById("alertapagocredito").style.display = "block";
                document.getElementById("textoalertapagocredito").value = "El limite de crédito se ha rebasado."

            }

        } else {
                            
            document.getElementById("alertapagocredito").style.display = "none";

            document.getElementById("divcambiocredito").style.display = "block";
            document.getElementById("cambiocredito").value = "";

            document.getElementById("divcantidadadeberocredito").style.display = "block";
            document.getElementById("cantidadadeberocredito").value = "";

            document.getElementById("divcantidadpagocredito").style.display = "block";
            document.getElementById("cantidadpagocredito").value = "";

            document.getElementById("botonmodalcreditoaceptar").style.visibility = "visible";
            //document.getElementById("botonmodalcreditoaceptar").style.visibility = "hidden";

        }

    }        

    function OcultarAlertaPagoCredito() {

        if( document.getElementById("alertapagocredito").style.display == "none" ) {

            document.getElementById("alertapagocredito").style.display = "block";

        } else {

            document.getElementById("alertapagocredito").style.display = "none";

        }

    }

    function OcultarAlertaCreditoCliente() {

        if( document.getElementById("alertacreditocliente").style.display == "none" ) {

            document.getElementById("alertacreditocliente").style.display = "block";

        } else {

            document.getElementById("alertacreditocliente").style.display = "none";

        }            

    }

    //CARGA LOS DATOS DE LA TABLA PRODUCTOS AL MODAL DE LISTADO PRODUCTOS
    function CargarListadoProductosModal() {
        
        if( !banderalistadoproductos ) {
            
            var url  = "/api/listadoproductosmodal/";

            //hace la búsqueda
            $.get( url ).done( function( data ) {

                var content = JSON.parse( JSON.stringify( data ) );

                console.log( "Contenido listadoproductos: ", content );

                const tablalistadoproductos = document.getElementById( 'tablalistadoproductos' );
                var rowCount = tablalistadoproductos.rows.length;         

                for  ( var x = rowCount - 1; x > 0; x-- ) { 
                    tablalistadoproductos.deleteRow(x); 
                }
                
                if( content.length > 0 ) {

                    for( var i = 0; i < content.length; i++ ) {

                        var codigoproducto = content[i].CodigoProducto;
                        var descripcionproducto = content[i].DescripcionProducto;
                        var precioventaproducto = content[i].PrecioVentaProducto;
                        
                        precioventaproducto = ( Math.round( precioventaproducto * 100 ) / 100 ).toFixed( 2 );

                        document.getElementById("tablalistadoproductos").insertRow(-1).innerHTML = 
                            "<td>" + codigoproducto + "</td>" +
                            "<td>" + descripcionproducto + "</td>" +
                            "<td>"+ precioventaproducto + "</td>";

                    }

                }

            });

        } else {

            banderalistadoproductos = true;

        }
        
    }

    //FUNCION PARA OBTENER EL CODIGO SELECCIONADO DE LA TABLA DEL MODAL LISTADOPRODUCTO
    function ObtenerFilaListadoProductos( event ) {

        var tablalistadoproductos = document.getElementById('tablalistadoproductos'),
        selected = tablalistadoproductos.getElementsByClassName('selected');
        
        if (selected[0]) selected[0].className = '';
        event.target.parentNode.className = 'selected';

        document.getElementById("codigo").value = $("tr.selected td:first" ).html();
        
        BuscarProducto();

        $("#modalEmergenteListadoProductos").modal('hide');

    }
    
    function onKeyUp(event) {

        var keycode = event.keyCode;

        if(keycode == '13'){

            BuscarProducto();

        }

    }

    function BuscarProductoArray( codigo ) {

        var posicion = -1;

        for( let i = 0; i < arregloproductodescuentos.length; i++ ) {

            if( arregloproductodescuentos[i].Codigo == codigo ) {
                posicion = i;
            }

        }

        return posicion;

    }

    function getNombrePromocionArray( codigo ) {

        var descripcionpromocionbuscada = "";
        
        const tabla = document.getElementById("tablapromociones");
        var rowCount = document.getElementById("tablapromociones").rows.length;

        var bool = false;
        var filaentabla = -1;

        for( let i = 0; i < arregloproductodescuentos.length; i++ ) {

            if( arregloproductodescuentos[i].Codigo == codigo ) {
            
                //Obtener la informacion de la promoción para saber que promocion es y como tratar la promoción.
                var textoinfopromocion = arregloproductodescuentos[i].InformacionPromocion;
                var arrayauxinfo = textoinfopromocion.split(",");

                var arraytipopromocion = arrayauxinfo[0].split(":");
                var auxtipopromocion = arraytipopromocion[1];

                if( auxtipopromocion == "1" || auxtipopromocion == "2" ) {

                    var arraydescripcionpromocion = arrayauxinfo[2].split(":");
                    descripcionpromocionbuscada = arraydescripcionpromocion[1];

                } else if( auxtipopromocion == "3" ) {

                    var arraydescripcionpromocion = arrayauxinfo[6].split(":");
                    descripcionpromocionbuscada = arraydescripcionpromocion[1];

                }

            }

        }

        if( rowCount >= 2 ) {

            for( var i = 1; i < tabla.rows.length; i++ ) {

                var textotabladescripcion = tabla.rows[i].cells[0].innerText;
                if( descripcionpromocionbuscada == textotabladescripcion ) {

                    filaentabla = i;

                }

            }

        }

        return filaentabla;

    }

    function BuscarProductoComplementarioPaquete( codigo ) {

        const tabla = document.getElementById('tablaproductos');
        var rowCount = document.getElementById("tablaproductos").rows.length;
                
        var bool = false;
        var filaentabla = -1;    
        
        for( var i = 1; i < tabla.rows.length; i++ ) {
            var textotablacodigo = tabla.rows[i].cells[0].innerText;
            if( codigo == textotablacodigo ) {
                filaentabla = i;
            }
        }

        return filaentabla;

    }

    function BuscarPromocionTabla( descripcion ) {

        const tabla = document.getElementById("tablapromociones");
        var rowCount = document.getElementById("tablapromociones").rows.length;

        var bool = false;
        var filaentabla = -1;

        if( rowCount >= 2 ) {

            for( var i = 1; i < tabla.rows.length; i++ ) {

                var textotabladescripcion = tabla.rows[i].cells[0].innerText;
                if( descripcion == textotabladescripcion ) {

                    filaentabla = i;

                }

            }

        }

        return filaentabla;

    }

    function CalcularImporte() {

        var textoprecio = document.getElementById("precio").value;
        var textocantidad = document.getElementById("cantidad").value;

        if( textocantidad != "" ) {

            var numeroprecio = parseFloat( textoprecio );
            var numerocantidad = parseInt( textocantidad );

            var cantidadimporte = ( numerocantidad * numeroprecio );
                    
            cantidadimporte = ( Math.round( cantidadimporte * 100 ) / 100 ).toFixed( 2 );            

            document.getElementById("importe").value = cantidadimporte;

        } else {

            document.getElementById("importe").value = "";

        }
            

    }

    function CalcularTotal() {
        
        var total = 0.0;

        $('#tablaproductos tr').each(function (row, tr ) {
            var textocoluman = 0;
            textocolumna = Number.parseFloat( $(tr).find('td:eq(4)').text().trim() );
            
            if( !isNaN( textocolumna ) ) {
                total = textocolumna  + total;
            }

        }); 

        total = ( Math.round( total * 100 ) / 100 ).toFixed( 2 );
        document.getElementById("total").value = total;
        
        document.getElementById("textototal").value = NumeroALetras( total );

    }

    function CalcularCambio() {


        var textototal = Number.parseFloat( document.getElementById("total").value );
        var textoefectivo = Number.parseFloat( document.getElementById("efectivo").value );

        if( isNaN( textoefectivo ) ) {
            textoefectivo = 0.0;
        }

        if( isNaN( textototal ) ) {
            textototal = 0.0;
        }

        var textocambio = textoefectivo - textototal;
        textocambio = ( Math.round( textocambio * 100 ) / 100 ).toFixed( 2 );
        document.getElementById("cambio").value = textocambio;

    }

    function CalcularDescuento() {   

        var descuentoproductos = 0.0;
        var descuentototal = 0.0;

        var bandera = true;

        var tamañoarray = arregloproductodescuentos.length;

        for( let i = 0 ; i < tamañoarray; i++ ) {

            //Obtener la informacion de la promoción para saber que promocion es y como tratar la promoción.
            var textoinfopromocion = arregloproductodescuentos[i].InformacionPromocion;

            var arrayauxinfo = textoinfopromocion.split(",");

            if( arrayauxinfo.length >= 1 ) {

                var arraytipopromocion = arrayauxinfo[0].split(":");
                var auxtipopromocion = arraytipopromocion[1];
                console.log( "Aux tipo promocion: ", auxtipopromocion );

                //OFERTA
                if( auxtipopromocion == "1" ) {

                    var arraytipooferta = arrayauxinfo[1].split(":");
                    var auxtipooferta = arraytipooferta[1];
                    var divisor;

                    if( auxtipooferta == "1" ) {
                        divisor = 2;
                    } else if( auxtipooferta == "2" ) {
                        divisor = 3;
                    } else if( auxtipooferta == "3" ) {
                        divisor = 4;
                    } else if( auxtipooferta == "4" ) {
                        divisor = 5;
                    }
                    
                    //console.log("Divisor");

                    var cantidadproductos = arregloproductodescuentos[i].CantidadProducto;

                    var resul = Number.parseInt( cantidadproductos / divisor );
                    var resto = cantidadproductos % divisor;

                    console.log("RESULTADO: ", resul, " RESTO: ", resto );

                    /*
                        Precio Real $40             PrecioOferta $26.67
                            
                        Oferta = 3x2              CantidadProductos = 5

                        Precio con oferta:      3 x 26.67   =   80.01
                                                                +
                        Precio sin oferta:      2 x 40      =   80
                                                                ---------
                        Importe ambos casos:                     160.01        

                        Importe real:           5 x 40      =       200
                                                                -   160.01
                                                                --------------
                                        Descuento aplicado:         39.99

                    */

                    var precioventareal = arregloproductodescuentos[i].ProductoPrecio;
                    var precioventaoferta = arregloproductodescuentos[i].PromocionPrecio;

                    var importepreciooferta = 0.0;
                    var importeprecionooferta = 0.0;
                    var importeambosprecios = 0.0;

                    var importereal;
                    var descuentoaplicado = 0.0;

                    var importeprecioofertaindividual = 0.0;

                    //PRECIO SIN OFERTA
                    importeprecionooferta = resto * precioventareal;                    
                    importeprecionooferta = ( Math.round( importeprecionooferta * 100 ) / 100 ).toFixed( 2 );
                    importeprecionooferta = Number.parseFloat( importeprecionooferta );
                    //console.log( "Importe sin oferta: ", importeprecionooferta );

                    //PRECIO CON OFERTA
                    importepreciooferta = ( divisor * resul ) * precioventaoferta;
                    importepreciooferta = ( Math.round( importepreciooferta * 100 ) / 100 ).toFixed( 2 );
                    importepreciooferta = Number.parseFloat( importepreciooferta );
                    //console.log( "Importe con oferta: ", importepreciooferta );

                    //SUMA PRECIO PRODUCTOS QUE APLICAN A LA OFERTA Y PRODUCTOS QUE NO APLICAN A LA OFERTA
                    importeambosprecios = importepreciooferta + importeprecionooferta;
                    importeambosprecios = ( Math.round( importeambosprecios * 100 ) / 100 ).toFixed( 2 );
                    importeambosprecios = Number.parseFloat( importeambosprecios );
                    //console.log( "Importe ambos casos: ", importeambosprecios );
                

                    importeprecioofertaindividual = divisor * precioventaoferta;
                    importeprecioofertaindividual = ( Math.round( importeprecioofertaindividual * 100 ) / 100 ).toFixed( 2 );
                    importeprecioofertaindividual = Number.parseFloat( importeprecioofertaindividual );

                    //IMPORTE PRODUCTOS CON PRECIO REAL
                    importereal =  cantidadproductos * precioventareal;
                    importereal = ( Math.round( importereal * 100 ) / 100 ).toFixed( 2 );
                    importereal = Number.parseFloat( importereal );
                    //console.log( "Importe real: ", importereal );

                    descuentoaplicado =  importereal - importeambosprecios;
                    descuentoaplicado = ( Math.round( descuentoaplicado * 100 ) / 100 ).toFixed( 2 );
                    descuentoaplicado = Number.parseFloat( descuentoaplicado );
                    //console.log( "Descuento aplicado: ", descuentoaplicado );

                    descuentototal += descuentoaplicado;

                    if( resul > 0 ) {

                        var arraydescripcionpromocion = arrayauxinfo[2].split(":");
                        var descripcionpromocion = arraydescripcionpromocion[1];

                        var posicionpromocion = BuscarPromocionTabla( descripcionpromocion );

                        if( posicionpromocion > -1 ) {
                            
                            //ActualizarPromocionTabla( posicionpromocion, resul, importepreciooferta );
                            ActualizarPromocionTabla( posicionpromocion, resul, importepreciooferta );

                        } else {
                            
                            //AgregarFilaPromociones( descripcionpromocion, resul, precioventaoferta, importepreciooferta );
                            AgregarFilaPromociones( descripcionpromocion, resul, importeprecioofertaindividual, importepreciooferta );

                        }

                    } else {                    
                
                        const tablapromociones = document.getElementById('tablapromociones');
                    
                        var posicionpromocion = getNombrePromocionArray( arregloproductodescuentos[i].Codigo );
                        tablapromociones.deleteRow( posicionpromocion );

                    }
                    

                } else if( auxtipopromocion == "2" ) {
                    //DESCUENTO
                    
                    //var porcentajedescuento;
                    var precioventareal = arregloproductodescuentos[i].ProductoPrecio;
                    var precioventadescuento = arregloproductodescuentos[i].PromocionPrecio;

                    var cantidadproductos = arregloproductodescuentos[i].CantidadProducto;

                    var importepreciodescuento = 0.0;
                    var importeprecionodescuento = 0.0;

                    var descuentoaplicado = 0.0;                  

                    /*
                        PrecioReal $0.6  PrecioDescuento $0.48  PorcentajeDescuento 20%

                        Cantidad Productos = 3

                        Importe sin descuento:           3 x 0.6 =       1.8
                                                                    -
                        Importe con descuento:           3 x 0.48 =      1.44
                                                                    -------------
                                                Descuento aplicado:     0.36 
                    */

                    //IMPORTE SIN DESCUENTO( REAL )
                    importeprecionodescuento = cantidadproductos * precioventareal;
                    importeprecionodescuento = ( Math.round( importeprecionodescuento * 100 ) / 100 ).toFixed( 2 );
                    importeprecionodescuento = Number.parseFloat( importeprecionodescuento );
                    //console.log( "Importe sin descuento: ", importeprecionodescuento );

                    //IMPORTE CON DESCUENTO
                    importepreciodescuento = cantidadproductos * precioventadescuento;
                    importepreciodescuento = ( Math.round( importepreciodescuento * 100 ) / 100 ).toFixed( 2 );
                    importepreciodescuento = Number.parseFloat( importepreciodescuento );
                    //console.log( "Importe con descuento: ", importepreciodescuento ); 

                    descuentoaplicado = importeprecionodescuento - importepreciodescuento;
                    descuentoaplicado = ( Math.round( descuentoaplicado * 100 ) / 100 ).toFixed( 2 );
                    descuentoaplicado = Number.parseFloat( descuentoaplicado );
                    //console.log( "Descuento aplicado: ", descuentoaplicado );

                    descuentototal += descuentoaplicado;

                    var arraydescripcionpromocion = arrayauxinfo[2].split(":");
                    var descripcionpromocion = arraydescripcionpromocion[1];

                    var posicionpromocion = BuscarPromocionTabla( descripcionpromocion );

                    if( posicionpromocion > -1 ) {

                        ActualizarPromocionTabla( posicionpromocion, cantidadproductos, importepreciodescuento )

                    } else {

                        AgregarFilaPromociones( descripcionpromocion, cantidadproductos, precioventadescuento, importepreciodescuento );

                    }

                } else if( auxtipopromocion == "3" ) {

                    //PAQUETE

                    var arraycodigocomplementario = arrayauxinfo[2].split(":");
                    var auxcodigocomplementario = arraycodigocomplementario[1];
                    console.log( "CODIGO COMPLEMENTARIO: ", auxcodigocomplementario );

                    var posicion = BuscarProductoComplementarioPaquete( auxcodigocomplementario );
                    console.log( "POSICION: ", posicion );

                    if( bandera ) {

                        if( posicion > -1 ) {

                            const tabla = document.getElementById("tablaproductos");

                            var productoprecioventadescuento = arregloproductodescuentos[i].PromocionPrecio;
                            console.log( "Producto precio descuento: ", productoprecioventadescuento );

                            var productocomplementarioprecioventadescuento = tabla.rows[posicion].cells[3].innerText;
                            console.log( "Producto complemetario precio descuento: ", productocomplementarioprecioventadescuento );                            

                            var preciopaquete = Number.parseFloat( productoprecioventadescuento ) + Number.parseFloat( productocomplementarioprecioventadescuento );
                            preciopaquete = ( Math.round( preciopaquete * 100 ) / 100 ).toFixed( 2 );                    

                            var cantidadproducto = arregloproductodescuentos[i].CantidadProducto;
                            console.log( "Cantidad producto: ", cantidadproducto );

                            var cantidadproductocomplementario = tabla.rows[posicion].cells[2].innerText;
                            console.log( "Cantidad producto complementario: ", cantidadproductocomplementario );

                            var importedescuentopaquete = 0.0;

                            /*

                                Precio descuento producto:  $14.8   Precio descuento complementario: $13.65
                                Total promocion: $38

                                CASO 1

                                    Cantidad producto =  2         Cantidad producto complementario = 2


                                                                    14.80 x 2 =     29.6
                                                                                +
                                                                    13.65 x 2 =     27.3
                                                                            ---------------                                                                           
                                    Importe paquete descuento:                      56.9
                                                                                -
                                    Importe total promocion         38 x 2 =        76
                                                                                -------------
                                                            Descuento aplicado:    19.1

                                CASO 2

                                    Cantidad producto = 3           Cantidad producto complementario = 2

                                        if( cantidad_producto > cantidad_producto_complementario ) {
                                            cantidad = cantidad_producto_complementario
                                        } else {
                                            cantidad = cantidad_producto
                                        }

                                        cantidad = 2 
                                                                    14.80 x 1 =     29.6
                                                                                +
                                                                    13.65 x 2 =     27.3
                                                                                --------------
                                    Importe paquete descuento                       56.9
                                                                                -
                                    Importe total promocion         38 x 2  =       76
                                                                            -----------------
                                                    Descuento aplicado:            19.1    

                            */

                            if( cantidadproducto == cantidadproductocomplementario ) {

                                var arraytotalpromocion =  arrayauxinfo[5].split(":");
                                var auxtotalpromocion = arraytotalpromocion[1];
                                console.log( "Total promocion: ", auxtotalpromocion );

                                var importetotalpromocion =  cantidadproducto * auxtotalpromocion;
                                console.log( "Importe total promocion: ", importetotalpromocion );

                                var importeproducto = cantidadproducto * productoprecioventadescuento;
                                console.log( "Importe producto: ", importeproducto );

                                var importeproductocomplementario = cantidadproducto * productocomplementarioprecioventadescuento;
                                console.log( "Importe producto complementario: ", importeproductocomplementario );

                                importedescuentopaquete = importeproducto + importeproductocomplementario;
                                importedescuentopaquete = ( Math.round( importedescuentopaquete * 100 ) / 100 ).toFixed( 2 );                            
                                console.log( "Importe descuento paquete: ", importedescuentopaquete );

                                descuentoaplicado = Math.abs( importetotalpromocion - importedescuentopaquete );
                                console.log("Descuento aplicado: ", descuentoaplicado );

                                descuentoaplicado = ( Math.round( descuentoaplicado * 100 ) / 100 ).toFixed( 2 );
                                descuentoaplicado = Number.parseFloat( descuentoaplicado );

                                
                                descuentototal += descuentoaplicado;
                                

                                var arraydescripcionpromocion = arrayauxinfo[6].split(":");
                                var descripcionpromocion = arraydescripcionpromocion[1];

                                var posicionpromocion = BuscarPromocionTabla( descripcionpromocion );

                                if( posicionpromocion > -1 ) {

                                    ActualizarPromocionTabla( posicionpromocion, cantidadproducto, importedescuentopaquete )

                                } else {

                                    AgregarFilaPromociones( descripcionpromocion, cantidadproducto, preciopaquete, importedescuentopaquete );

                                }
                                

                            } else {
                                console.log( "Cantidades diferentes" );

                                var cantidadacomprar = 0;

                                if( cantidadproducto > cantidadproductocomplementario ) {
                                    cantidadacomprar = cantidadproductocomplementario;
                                } else {
                                    cantidadacomprar = cantidadproducto;
                                }
            
                                var arraytotalpromocion =  arrayauxinfo[5].split(":");
                                var auxtotalpromocion = arraytotalpromocion[1];
                                console.log( "Total promocion: ", auxtotalpromocion );

                                var importetotalpromocion =  cantidadacomprar * auxtotalpromocion;
                                console.log( "Importe total promocion: ", importetotalpromocion );

                                var importeproducto = cantidadacomprar * productoprecioventadescuento;
                                console.log( "Importe producto: ", importeproducto );

                                var importeproductocomplementario = cantidadacomprar * productocomplementarioprecioventadescuento;
                                console.log( "Importe producto complementario: ", importeproductocomplementario );

                                importedescuentopaquete = importeproducto + importeproductocomplementario;
                                importedescuentopaquete = ( Math.round( importedescuentopaquete * 100 ) / 100 ).toFixed( 2 );                            
                                console.log( "Importe descuento paquete: ", importedescuentopaquete );

                                descuentoaplicado = Math.abs( importetotalpromocion - importedescuentopaquete );
                                console.log("Descuento aplicado: ", descuentoaplicado );

                                descuentoaplicado = ( Math.round( descuentoaplicado * 100 ) / 100 ).toFixed( 2 );
                                descuentoaplicado = Number.parseFloat( descuentoaplicado );

                                
                                descuentototal += descuentoaplicado;
                                

                                var arraydescripcionpromocion = arrayauxinfo[6].split(":");
                                var descripcionpromocion = arraydescripcionpromocion[1];

                                var posicionpromocion = BuscarPromocionTabla( descripcionpromocion );

                                if( posicionpromocion > -1 ) {

                                    ActualizarPromocionTabla( posicionpromocion, cantidadacomprar, importedescuentopaquete )

                                } else {

                                    AgregarFilaPromociones( descripcionpromocion, cantidadacomprar, preciopaquete, importedescuentopaquete );

                                }


                            }

                            bandera = false;

                        }

                    } else {

                        bandera = true;

                    }
                        


                }

            }

        }

        descuentototal = ( Math.round( descuentototal * 100 ) / 100 ).toFixed( 2 );

        document.getElementById("descuento").value =  descuentototal;

    }

    function CambiarMaximoCantidadInput( codigo, tipooperacion ) {

        var posicion = BuscarProductoArray( codigo );
        var retorno = 0;

        var cantidadmax;
        var cantidadproductoagregado;

        if( tipooperacion == "Agregar" ) {

            if( posicion > -1 ) {
                                    
                cantidadmax = arregloproductodescuentos[ posicion ].CantidadMaximaInventario;
                cantidadproductoagregado = arregloproductodescuentos[posicion].CantidadProducto;

                var nuevacantidadmax = cantidadmax - cantidadproductoagregado;

                if( cantidadmax == 0 ) {
                    
                    console.log("CANTIDAD MAX MENOR 0");
                    
                    document.getElementById("textoalerta").value = "Se han agotado las cantidades existentes en el inventario";
                    document.getElementById("contentalert").style.display = "block";
                    retorno = 404;

                } else {
                    
                    console.log("CANTIDAD MAX MAYOR 0");
                    console.log("Cantidad max: ", nuevacantidadmax );
                    
                    arregloproductodescuentos[ posicion ].CantidadMaximaInventario = nuevacantidadmax;

                    document.getElementById("cantidad").setAttribute("max", nuevacantidadmax );

                }

            } else {    
                                        
                cantidadmax = arregloproductodescuentos[ posicion ].CantidadMaximaInventario;
                document.getElementById("cantidad").setAttribute("max", cantidadmax );

            }

        } else if( tipooperacion == "Actualizar" || tipooperacion == "Eliminar" ) {
                                        
            cantidadmax = arregloproductodescuentos[ posicion ].CantidadMaximaInventario;
            document.getElementById("cantidad").setAttribute("max", cantidadmax );

        }
    
        console.log("ANTES RETORNO");
        return retorno;

    }

    //Saber si fue una tecla correspondiente a un número
    function onKeyDownHandler() {
        //console.log("key pressed ",  String.fromCharCode(event.keyCode));
        //console.log("key pressedx2 ", String.fromCharCode(event.which));

        if( document.getElementById("total").value == "" ) {                

            var keypresionada = event.which;
            if( ( keypresionada >= 48 && keypresionada <= 57 ) || ( keypresionada >=96 && keypresionada <= 105 ) ) {
                
                CalcularImporte();

            } else if( keypresionada == 13 ) {

                if( document.getElementById("codigo").value != "" ) {
                    document.getElementById("codigo").focus();
                    CalcularImporte();
                    VerificarFilaDefault();
                }
                
            }

        } else if( document.getElementById("total").value != "" && document.getElementById("efectivo").value != "" ) {
                            
            var keypresionada = event.which;
            if( ( keypresionada >= 48 && keypresionada <= 57 ) || ( keypresionada >=96 && keypresionada <= 105 ) || keypresionada == 13 ) {
                                
                CalcularCambio();

            }
        } else {

            var keypresionada = event.which;                

            if( keypresionada == 13 ) {                    

                if( document.getElementById("codigo").value != "" ) {
                    
                    document.getElementById("codigo").focus();
                    CalcularImporte();
                    VerificarFilaDefault();

                }
            }

        }
    }

    function VerificarFilaDefault() {

        const tabla = document.getElementById('tablaproductos');
        var rowCount = document.getElementById("tablaproductos").rows.length;

            //Sólo está la fila por defecto
        if( rowCount == 2 ) {            

            var bool = false;

            $("#tablaproductos td").each(function(){

                    //cambiar "CODIGO" por el texto que está por defecto en la primera columna de código de producto.
                if( $(this).text() == "-" ) {
                    bool = true;
                    //console.log($(this).text());
                }
            });

            //Tiene la fila por default.
            if( bool ) {
                tabla.deleteRow( rowCount - 1 );
                AgregarFila();

                //Ya tiene el registro de un producto en la primera fila.
            } else {

                var textocodigobuscado = document.getElementById("codigo").value;
                var bool = false;

                $("#tablaproductos td").each( function() {
                        //cambiar "CODIGO" por el texto que está por defecto en la primera columna de código de producto.
                    if( $(this).text() == textocodigobuscado ) {
                        bool = true;
                        //console.log($(this).text());
                    }
                });

                    //El producto que se quiere ingresar ya está en la tabla ( Cuando sólo hay un producto en la tabla ).
                if( bool ) {
                    ActualizarProductoTabla( 1 );
                } else {
                    AgregarFila();
                }

            }

            // Más de un producto agregado a la tabla
        } else {

            var textocodigobuscado = document.getElementById("codigo").value;
            var bool = false;
            var fila = 1;    
            
            for( var i = 1; i < tabla.rows.length; i++ ) {
                var textotablacodigo = tabla.rows[i].cells[0].innerText;
                if( textocodigobuscado == textotablacodigo ) {
                    bool = true;
                    fila = i;
                }
            }            
            
                    //El producto ya está en la tabla ( Cuando hay más de un producto en la tabla).
            if( bool ) {
                ActualizarProductoTabla( fila );
            } else {
                AgregarFila();
            }

        }

    }

    function ActualizarProductoTabla( fila ) {
                
        const tabla = document.getElementById('tablaproductos');

        var textocodigo = document.getElementById("codigo").value;
        var cantidadold = parseInt( tabla.rows[fila].cells[2].innerText );
        var cantidadnew = parseInt( document.getElementById("cantidad").value );
        var precio = parseFloat( tabla.rows[fila].cells[3].innerText );

        // Verificación del texto del botón de agregar producto.
        textoenlaceagregar = document.getElementById("enlaceagregar").text;
        console.log("Texto del enlace: " , textoenlaceagregar );

        if( document.getElementById("enlaceagregar").text == "Agregar" ) {

            cantidadnew += cantidadold;

        } else if( document.getElementById("enlaceagregar").text == "Editar" ) {

            document.getElementById("enlaceagregar").text = "Agregar";
            document.getElementById("enlaceagregar").style.marginTop = "35%";

            document.getElementById("enlaceeliminar").style.display = "none";
            document.getElementById("enlaceeliminar").style.marginTop = "1%";
            
            document.getElementById("codigo").readOnly = false;
            document.getElementById("codigo").focus();

        }

        var importenew = ( cantidadnew * precio );
        importenew = ( Math.round( importenew * 100 ) / 100 ).toFixed( 2 );


        document.getElementById("tablaproductos").rows[fila].cells[2].innerText = cantidadnew;
        document.getElementById("tablaproductos").rows[fila].cells[4].innerText = importenew;

        document.getElementById("codigo").value = "";
        document.getElementById("descripcion").value = "";
        document.getElementById("cantidad").value = "";
        document.getElementById("precio").value = "";
        document.getElementById("importe").value = "";

        document.getElementById("cantidad").readOnly = true;
        document.getElementById("codigo").readOnly = false;
        document.getElementById("codigo").focus();
                
        var posicion = BuscarProductoArray( textocodigo );

        if( posicion > -1 ) {

            arregloproductodescuentos[posicion].CantidadProducto = cantidadnew;

        }

        CalcularDescuento();

        CambiarMaximoCantidadInput( textocodigo, "Actualizar" );

        CalcularTotal();            

    }

    function ModificarFilaProducto( event ) {
        
        var table = document.getElementById('tablaproductos'),
        selected = table.getElementsByClassName('selected');
        
        if (selected[0]) selected[0].className = '';
        event.target.parentNode.className = 'selected';

        /*
        event.target.parentNode.className = 'selected';
    
        alert($("tr.selected td:first" ).html());
        console.log("Codigo: " ,$("tr.selected td:first" ).html() );
        console.log("Descripcion: ", $("tr.selected td:nth-child(2)").html() );
        console.log("Cantidad: ", $("tr.selected td:nth-child(3)").html() );
        console.log("Precio: ", $("tr.selected td:nth-child(4)").html() );
        console.log("Importe: ", $("tr.selected td:nth-child(5)").html() );
        */

        /*              SIN LA TABLA DE LISTADO PRODUCTOS MODAL
        document.getElementById("codigo").value = $("tr.selected td:first" ).html();
        document.getElementById("descripcion").value = $("tr.selected td:nth-child(2)").html();
        document.getElementById("cantidad").value = $("tr.selected td:nth-child(3)").html();
        document.getElementById("precio").value = $("tr.selected td:nth-child(4)").html();
        document.getElementById("importe").value = $("tr.selected td:nth-child(5)").html();
        */
        
        document.getElementById("codigo").value = $("#tablaproductos tr.selected td:first" ).html();
        document.getElementById("descripcion").value = $("#tablaproductos tr.selected td:nth-child(2)").html();
        document.getElementById("cantidad").value = $("#tablaproductos tr.selected td:nth-child(3)").html();
        document.getElementById("precio").value = $("#tablaproductos tr.selected td:nth-child(4)").html();     
        document.getElementById("importe").value = $("#tablaproductos tr.selected td:nth-child(5)").html();

        document.getElementById("codigo").readOnly = true;
        document.getElementById("descripcion").readOnly = true;
        document.getElementById("cantidad").readOnly = false;
        document.getElementById("precio").readOnly = true;
        document.getElementById("importe").readOnly = true;

        document.getElementById("cantidad").focus();

        document.getElementById("enlaceagregar").text = "Editar";
        document.getElementById("enlaceagregar").style.marginTop = "1%";
        document.getElementById("enlaceeliminar").style.display = "block";
        document.getElementById("enlaceeliminar").style.marginTop = "4%";

        document.getElementById("enlaceagregar").style.color = "rgb(255, 255, 255)";
        document.getElementById("enlaceeliminar").style.color = "rgb(255, 255, 255)";
        
        var textocodigobuscado = document.getElementById("codigo").value;
        
        CambiarMaximoCantidadInput( textocodigobuscado, "Actualizar" );

    }

    function AgregarFila() {
                
        var textocodigo = document.getElementById("codigo").value;
        var textodescripcion = document.getElementById("descripcion").value;
        var textocantidad = document.getElementById("cantidad").value;
        var textoprecioventa = document.getElementById("precio").value;
        var textoimporte = document.getElementById("importe").value;

        if( textocodigo != "" && textodescripcion != "" && textocantidad != "" && textoprecioventa != "" && textoimporte != "" ) {

            const tabla = document.getElementById('tablaproductos');
            var rowCount = document.getElementById("tablaproductos").rows.length;
            //console.log("rowCount: " , rowCount );

            document.getElementById("tablaproductos").insertRow(-1).innerHTML = 
                "<td>" + textocodigo + "</td>" +
                "<td>" + textodescripcion + "</td>" +
                "<td>"+ textocantidad + "</td>" +
                "<td>" + textoprecioventa + "</td>" +
                "<td>"+ textoimporte + "</td>";

            document.getElementById("codigo").value = "";
            document.getElementById("descripcion").value = "";
            document.getElementById("cantidad").value = "";
            document.getElementById("precio").value = "";
            document.getElementById("importe").value = "";
            document.getElementById("cantidad").readOnly = true;
        
            let datospromocion = {   
                                    'Codigo' : textocodigo, 
                                    'ProductoPrecio' : productoprecioventa, 
                                    'InformacionPromocion': informacionpromocion,
                                    'PromocionPrecio' : promocionprecionuevo,
                                    'DescuentoProducto' :  descuentoproducto,
                                    'CantidadProducto' : textocantidad,
                                    'CantidadMaximaInventario' : textocantidadmaxinventario
                                };

            arregloproductodescuentos.push( datospromocion );

            console.log("Contenido arreglo: ", arregloproductodescuentos );

            CalcularDescuento();
            
            CambiarMaximoCantidadInput( textocodigo, "Agregar" );
            
            CalcularTotal();

        } else {

            document.getElementById("textoalerta").value = "No hay producto que agregar";
            document.getElementById("contentalert").style.display = "block";

        }

    }

    function EliminarFilaProducto() {
                
        const tabla = document.getElementById('tablaproductos');
        var textocodigobuscado = document.getElementById("codigo").value;
        var bool = false;
        var fila = 1;    
        
        for( var i = 1; i < tabla.rows.length; i++ ) {
            var textotablacodigo = tabla.rows[i].cells[0].innerText;
            if( textocodigobuscado == textotablacodigo ) {
                bool = true;
                fila = i;
            }
        } 

        if( bool ) {

            tabla.deleteRow( fila, -1 ); 
            document.getElementById("enlaceagregar").text = "Agregar";
            document.getElementById("enlaceeliminar").style.display = "none";

            document.getElementById("codigo").value = "";
            document.getElementById("descripcion").value = "";
            document.getElementById("cantidad").value = "";
            document.getElementById("precio").value = "";
            document.getElementById("importe").value = "";

            document.getElementById("cantidad").readOnly = true;
            document.getElementById("codigo").readOnly = false;
            document.getElementById("codigo").focus();

            var posicion = BuscarProductoArray( textocodigobuscado );


            //ELIMINAR EL PRODUCTO DEL ARRAY A PARTIR DE SU POSICION
            if( posicion > -1 ) {
                
                const tablapromociones = document.getElementById('tablapromociones');
            
                var posicionpromocion = getNombrePromocionArray( textocodigobuscado );
                tablapromociones.deleteRow( posicionpromocion );

                arregloproductodescuentos.splice( posicion, posicion + 1 );

            }
            
            if( tablapromociones.rows.length == 1 ) {
                document.getElementById("tablapromociones").style.display = "none";
            }

            CalcularDescuento();
            
            //CambiarMaximoCantidadInput( textocodigobuscado, "Eliminar" );
            
            CalcularTotal();


        }

    }

    function VerificarFilaPromocionesDefault() {

        var rowCount = document.getElementById("tablapromociones").rows.length;

        var bool = false;

            //Sólo está la fila por defecto
        if( rowCount == 2 ) {

            $("#tablapromociones td").each(function(){

                    //cambiar "CODIGO" por el texto que está por defecto en la primera columna de código de producto.
                if( $(this).text() == "-" ) {
                    bool = true;
                    //console.log($(this).text());
                }
            });

        }

        return bool;

    }

    function AgregarFilaPromociones( descripcionpromocion, cantidadpromocion, preciopromocion, importepromocion ) {

        var bandera = VerificarFilaPromocionesDefault();

        if( bandera ) {

            const tabla = document.getElementById('tablapromociones');
            var rowCount = document.getElementById("tablapromociones").rows.length;

            tabla.deleteRow( rowCount - 1 );
                            
            document.getElementById("tablapromociones").style.display = "block";
            document.getElementById("tablapromociones").style.marginLeft = "20%";

            document.getElementById("tablapromociones").insertRow(-1).innerHTML = 
            "<td>" + descripcionpromocion + "</td>" +
            "<td>" + cantidadpromocion + "</td>" +
            "<td>" + preciopromocion + "</td>" +
            "<td>" + importepromocion + "</td>";

        }

        console.log("AGREGAR");

        var banderavaciotablapromociones = true;

         $("#tablapromociones td").each(function() {
             banderavaciotablapromociones = false;
        });

        if( banderavaciotablapromociones ) {
                            
            document.getElementById("tablapromociones").style.display = "block";
            document.getElementById("tablapromociones").style.marginLeft = "20%";

            document.getElementById("tablapromociones").insertRow(-1).innerHTML = 
            "<td>" + descripcionpromocion + "</td>" +
            "<td>" + cantidadpromocion + "</td>" +
            "<td>" + preciopromocion + "</td>" +
            "<td>" + importepromocion + "</td>";

        }

    }

    function ActualizarPromocionTabla( fila, cantidadpromocion, importepromocion ) {

        const tabla = document.getElementById("tablapromociones");

        document.getElementById("tablapromociones").rows[fila].cells[1].innerText = cantidadpromocion;
        document.getElementById("tablapromociones").rows[fila].cells[3].innerText = importepromocion;

    }

    function LimpiarInputs() {        

        const tabla = document.getElementById('tablaproductos');
        var rowCount = tabla.rows.length; 

        const tablapromociones = document.getElementById("tablapromociones");
        var rowCountPromociones = tablapromociones.rows.length;            

        for  ( var x = rowCount - 1; x > 0; x-- ) { 
            tabla.deleteRow(x); 
        } 

        for( var x = rowCountPromociones - 1; x > 0; x-- ) {
            tablapromociones.deleteRow(x);
        }

        document.getElementById("enlaceagregar").text = "Agregar";
        document.getElementById("enlaceeliminar").style.display = "none";

        document.getElementById("codigo").value = "";
        document.getElementById("descripcion").value = "";
        document.getElementById("cantidad").value = "";
        document.getElementById("precio").value = "";
        document.getElementById("importe").value = "";
        
        document.getElementById("total").value = "";
        document.getElementById("efectivo").value = "";
        document.getElementById("descuento").value = "";
        document.getElementById("cambio").value = "";
           
        document.getElementById("cantidad").readOnly = true;
        document.getElementById("codigo").readOnly = false;
        document.getElementById("codigo").focus();

        //CalcularTotal();

    }

    function LimpiarInputsModal( caso ) {
    
        document.getElementById("nombrecliente").value = "";

        document.getElementById("limitecredito").value = "";
        document.getElementById("informacioncredito").value = "";
        document.getElementById("cantidadpagocredito").value = "";
        document.getElementById("cambiocredito").value = "";

        document.getElementById("pagocreditocompleto").checked = false;
        document.getElementById("pagocreditopartes").checked = false;

        document.getElementById("divfieldsettipopagocredito").style.display = "none";
        
        document.getElementById("divfieldsetinformacioncredito").style.display = "none";
                    
        document.getElementById("enlaceenviar").style.visibility = "visible";
    
        document.getElementById("alertapagocredito").style.display = "none";
        document.getElementById("alertacreditocliente").style.display = "none";
        

        if( caso == "CIERRE" || caso == "VENTA" ) {
            
            document.getElementById("importetotalventa").value = "";
            ActualizarSelectClientes();

        }

    }

    function LimpiarArray() {

        var tamanoarray = arregloproductodescuentos.length;
        arregloproductodescuentos.splice(0, tamanoarray);

    }

    //Obtiene los datos de todas las columnas de la tabla
    function GuardarDatosTabla() {
        
        var TableData = new Array();

        $('#tablaproductos tr').each(function(row, tr){

        TableData[row]=
        {
            "ColumnaCodigos" : $(tr).find('td:eq(0)').text().trim() //for first column value
            , "ColumnaCantidades" :$(tr).find('td:eq(2)').text().trim()  //for third column value
            , "ColumnaDescripciones" :$(tr).find('td:eq(1)').text().trim()  //for second column value
            , "ColumnaPrecios" :$(tr).find('td:eq(3)').text().trim()  //for third fourth value
            , "ColumnaImportes" :$(tr).find('td:eq(4)').text().trim()  //for third column value
        }    
        }); 
        console.log("Valores: " , TableData);
        
        for (let index = 1; index < TableData.length; index++) {
            const element = TableData[index];
            console.log("Codigo: " , element);
            
        }
        TableData.shift();  // first row will be empty - so remove
        return TableData;

    }

    function EnviarDatosTabla() {   

        console.log("ENTRA A ENVIARDATOS");

        var TableData;
        TableData = GuardarDatosTabla()
        TableData = JSON.stringify(TableData);

        var IdCliente = null;

        IdCliente = JSON.stringify( IdCliente );
        console.log("ID seleccionado: ", IdCliente );

        LimpiarArray();

        
        if( document.getElementById("efectivo").value == "" ) {

            OcultarModal( "ERROR", "ERROR AL PROCESAR LA VENTA", "Para procesar la venta, debe especificar el efectivo con el que se pagará." );  

        } else {
            
            var Total;
            Total = JSON.stringify( document.getElementById("total").value );

            var Descuento;

            if( document.getElementById("descuento").value == "" ) {

                Descuento = JSON.stringify( "0.0" );

            } else {

                Descuento = JSON.stringify( document.getElementById("descuento").value );

            }

            var Efectivo;
            if(  Number.parseFloat(  document.getElementById("cambio").value ) < 0 ) {
                
                Efectivo = undefined;

            } else {

                Efectivo = JSON.stringify( document.getElementById("efectivo").value );

            }

            const tabla = document.getElementById('tablaproductos');
            var numfilas = tabla.rows.length;        
            var textotabla = undefined;

            if( numfilas > 1 ) {

                if( tabla.rows[1].cells[0].innerText == "-" ) {

                    textotabla = undefined;

                } else {

                    textotabla = "Aplica";

                }

            }

            if( textotabla == undefined ) {

                OcultarModal( "ERROR", "ERROR AL PROCESAR LA VENTA", "No ha agregado ningún producto." );

            } else if( Efectivo == undefined ) {
                
                OcultarModal( "ERROR", "ERROR AL PROCESAR LA VENTA", "No se ha podido procesar la venta, ya que el efectivo no cubre el total de la venta." );

            } else if( textotabla == "Aplica" ) {


                $('#modalProgress').modal('show');
                setTimeout(() => { $('#progressbarventa').css('width', 30+'%').attr('aria-valuenow', 30 ); }, 1000);

                $.ajaxSetup({
                    headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                setTimeout(() => { $('#progressbarventa').css('width', 60+'%').attr('aria-valuenow', 60 ); }, 2000);

                $.ajax({

                    type: "POST",
                    url: "/insertarventa",
                    data: { TableData : TableData, IdCliente : IdCliente, Total : Total, 
                            Descuento : Descuento, Efectivo : Efectivo,
                            PagoACredito : pagoacredito },
                    success: function(data){

                        DismissModalCreditoCliente();

                        setTimeout(() => { $('#progressbarventa').css('width', 100+'%').attr('aria-valuenow', 1000 ); }, 3000);
                        //EjecutarFuncionImprimirTicket( TableData );
                                
                        document.getElementById("total").value = "";

                        LimpiarInputs();
                        LimpiarInputsModal("VENTA");
                        LimpiarArray();
                                                
                        setTimeout(() => { $("#modalProgress").modal('hide'); }, 3500);
                        setTimeout(() => { $('#progressbarventa').css('width', 0+'%').attr('aria-valuenow', 0 ); }, 3600);
                        

                        setTimeout(() => { OcultarModal( "SUCCESS", "VENTA REALIZADA", "La venta se ha generado y procesado con exito" ); }, 4500);

                    },
                    error:function()
                    { 
                        
                                                
                        setTimeout(() => { $("#modalProgress").modal('hide'); }, 3500);
                        setTimeout(() => { $('#progressbarventa').css('width', 0+'%').attr('aria-valuenow', 0 ); }, 3600);

                        if( Number.parseFloat(  document.getElementById("cambio").value ) < 0 ) {                                
                                                
                            setTimeout(() => { OcultarModal( "ERROR", "ERROR AL PROCESAR LA VENTA", "ERROR DE CAMBIO" ); }, 4500);
                            //OcultarModal( "ERROR", "ERROR AL PROCESAR LA VENTA", "ERROR DE CAMBIO" );

                        } else {
                            
                            setTimeout(() => { OcultarModal( "ERROR", "ERROR AL PROCESAR LA VENTA", "La venta no ha podido ser procesada debido a un error, por favor revise todos los campos." ); }, 4500);
                            //OcultarModal( "ERROR", "ERROR AL PROCESAR LA VENTA", "La venta no ha podido ser procesada debido a un error, por favor revise todos los campos." );

                        }

                    }
                });


            }

        }
        

    }

    function EnviarDatosTablaCredito() {
        
        console.log("ENTRA A ENVIARDATOSCREDITO");

        var TableData;
        TableData = GuardarDatosTabla()
        TableData = JSON.stringify(TableData);
        
        var select = document.getElementById("idcliente");
        var options=document.getElementsByTagName("option");

        var IdCliente = select.value;

        IdCliente = JSON.stringify( IdCliente );
        console.log("ID seleccionado: ", IdCliente );
            
        var Total;
        Total = JSON.stringify( document.getElementById("total").value );
            
        var Descuento;

        if( document.getElementById("descuento").value == "" ) {
            Descuento = JSON.stringify( "0.0" );
        } else {
            Descuento = JSON.stringify( document.getElementById("descuento").value );
        }
        
        var Efectivo;

        if( document.getElementById("pagocreditocompleto").checked ) {
            console.log("Completo seleccionado");

            Efectivo = JSON.stringify( "0.0" );

            var CantidadAdeudada;
            CantidadAdeudada = JSON.stringify( document.getElementById("informacioncredito").value );
            
            //Ya que la variable pagoacredito se resetea al cerrar el modal.
            var auxpagoacredito = pagoacredito;
            DismissModalCreditoCliente();

            //
            $('#modalProgress').modal('show');
            setTimeout(() => { $('#progressbarventa').css('width', 30+'%').attr('aria-valuenow', 30 ); }, 1000);

            $.ajaxSetup({
                    headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
            });

            //
            setTimeout(() => { $('#progressbarventa').css('width', 60+'%').attr('aria-valuenow', 60 ); }, 2000);

            $.ajax({
                type: "POST",
                url: "/insertarventa",
                data: { TableData : TableData, IdCliente : IdCliente, Total : Total, 
                        Descuento : Descuento, Efectivo : Efectivo, CantidadAdeudada : CantidadAdeudada, 
                        PagoACredito : auxpagoacredito },
                        
                success: function(data){

                    //DismissModalCreditoCliente();

                    //
                    setTimeout(() => { $('#progressbarventa').css('width', 100+'%').attr('aria-valuenow', 1000 ); }, 3000);

                    document.getElementById("total").value = "";
                    
                    LimpiarInputs();
                    LimpiarInputsModal("VENTA");
                    LimpiarArray();
                    
                    //                            
                    setTimeout(() => { $("#modalProgress").modal('hide'); }, 3500);
                    //
                    setTimeout(() => { $('#progressbarventa').css('width', 0+'%').attr('aria-valuenow', 0 ); }, 3600);
                        
                    //
                    setTimeout(() => { OcultarModal( "SUCCESS", "VENTA REALIZADA", "La venta se ha generado y procesado con exito" ); }, 4500);

                    //OcultarModal( "SUCCESS", "VENTA REALIZADA", "La venta se ha generado y procesado con exito" );

                },
                error:function()
                { 
                    
                    //                                                                                
                    setTimeout(() => { $("#modalProgress").modal('hide'); }, 3500);
                    setTimeout(() => { $('#progressbarventa').css('width', 0+'%').attr('aria-valuenow', 0 ); }, 3600);
                    
                    setTimeout(() => { OcultarModal( "ERROR", "ERROR AL PROCESAR LA VENTA", "La venta no ha podido ser procesada debido a un error, por favor revise todos los campos." ); }, 4500);
                    //OcultarModal( "ERROR", "ERROR AL PROCESAR LA VENTA", "La venta no ha podido ser procesada debido a un error, por favor revise todos los campos." );            

                }

            });

        } else if( document.getElementById("pagocreditopartes").checked ) {
            console.log("Parcial seleccionado");

            if( document.getElementById("cantidadadeberocredito").value == "" || isNaN( document.getElementById("cantidadadeberocredito").value ) ) {
                
                DismissModalCreditoCliente();
                OcultarModal( "ERROR", "ERROR AL PROCESAR LA VENTA A PAGAR CON CRÉDITO", "La venta no ha podido ser procesada debido a que no se ha ingresado la cantidad a pagar." );

            } else {

                Efectivo = JSON.stringify( "0.0" );

                var CantidadAdeudada;
                CantidadAdeudada = JSON.stringify( document.getElementById("informacioncredito").value );


                $.ajaxSetup({
                        headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                });

                $.ajax({

                    type: "POST",
                    url: "/insertarventa",
                    data: { TableData : TableData, IdCliente : IdCliente, Total : Total, 
                            Descuento : Descuento, Efectivo : Efectivo, CantidadAdeudada : CantidadAdeudada },
                    success: function(data){

                        DismissModalCreditoCliente();
                                                            
                        $('#modalProgress').modal('show');
                        setTimeout(() => { $('#progressbarventa').css('width', 30+'%').attr('aria-valuenow', 30 ); }, 1000);                            

                        setTimeout(() => { $('#progressbarventa').css('width', 60+'%').attr('aria-valuenow', 60 ); }, 2000);

                        setTimeout(() => { $('#progressbarventa').css('width', 100+'%').attr('aria-valuenow', 1000 ); }, 3000);

                        //EjecutarFuncionImprimirTicket( TableData );

                        document.getElementById("total").value = "";
                        
                        LimpiarInputs();
                        LimpiarInputsModal("VENTA");
                        LimpiarArray();
                                                
                        setTimeout(() => { $("#modalProgress").modal('hide'); }, 3500);
                        setTimeout(() => { $('#progressbarventa').css('width', 0+'%').attr('aria-valuenow', 0 ); }, 3600);
                        

                        setTimeout(() => { OcultarModal( "SUCCESS", "VENTA REALIZADA", "La venta se ha generado y procesado con exito" ); }, 4500);

                        //OcultarModal( "SUCCESS", "VENTA REALIZADA", "La venta se ha generado y procesado con exito" );

                    },
                    error:function()
                    { 
                        
                                                            
                        $('#modalProgress').modal('show');
                        setTimeout(() => { $('#progressbarventa').css('width', 30+'%').attr('aria-valuenow', 30 ); }, 1000);                            

                        setTimeout(() => { $('#progressbarventa').css('width', 60+'%').attr('aria-valuenow', 60 ); }, 2000);

                        setTimeout(() => { $("#modalProgress").modal('hide'); }, 3500);
                        setTimeout(() => { $('#progressbarventa').css('width', 0+'%').attr('aria-valuenow', 0 ); }, 3600);

                        setTimeout(() => { OcultarModal( "ERROR", "ERROR AL PROCESAR LA VENTA", "La venta no ha podido ser procesada debido a un error, por favor revise todos los campos." ); }, 4500);

                        //OcultarModal( "ERROR", "ERROR AL PROCESAR LA VENTA", "La venta no ha podido ser procesada debido a un error, por favor revise todos los campos." );

                    }

                });

            }


        }

        /*
        Efectivo = JSON.stringify( "0.0" );

        var CantidadAdeudada;
        CantidadAdeudada = JSON.stringify( document.getElementById("informacioncredito").value );


        $.ajaxSetup({
                headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
        });

        $.ajax({

            type: "POST",
            url: "{{ route('insertarventa') }}",
            data: { TableData : TableData, IdCliente : IdCliente, Total : Total, 
                    Descuento : Descuento, Efectivo : Efectivo, CantidadAdeudada : CantidadAdeudada },
            success: function(data){

                DismissModalCreditoCliente();
        
                document.getElementById("total").value = "";
                
                LimpiarInputs();
                LimpiarInputsModal("VENTA");
                LimpiarArray();

                OcultarModal( "SUCCESS", "VENTA REALIZADA", "La venta se ha generado y procesado con exito" );

            },
            error:function()
            { 
                OcultarModal( "ERROR", "ERROR AL PROCESAR LA VENTA", "La venta no ha podido ser procesada debido a un error, por favor revise todos los campos." );            
            }

        });
        */

    }

    function EjecutarFuncionImprimirTicket( datosproductos ) {
        
        console.log("EJECUTARFUNCIONIMPRIMIRTICKET");
        
        $.ajaxSetup({
                        headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
        });

        $.ajax({

            type: "POST",
            url: "/imprimirticketventa",
            data: { DatosProductos : datosproductos, DatosPromociones : arregloproductodescuentos },
            success: function(data){

                console.log( "IMPRESION REALIZADA" );                
                console.log("DatosProductos: ", datosproductos );
                console.log("DatosPromociones: ", arregloproductodescuentos );

            },
            error:function( error ) {                   
                            
                console.log("DatosProductos: ", datosproductos );
                console.log("DatosPromociones: ", arregloproductodescuentos );          
                console.log( "IMPRESION NO REALIZADA, error: ", error );

            }

        });
        
    }

    function OcultarAlerta() {

        if( document.getElementById("contentalert").style.display == "none" ) {
            document.getElementById("contentalert").style.display = "block";
        } else {
            document.getElementById("contentalert").style.display = "none";
        }

    }

    function OcultarModal( caso, textoheader, textobody ) {
        
        //$('#contentmodal').modal('show');
        //document.getElementById("textomodal").innerHTML = caso;  

        
        $('#modalErrorVenta').modal('show');  
        document.getElementById("textostringheader").innerHTML = textoheader;
        document.getElementById("textoerrorbody").innerHTML = textobody;

        if( caso == "ERROR" ) {
            
            document.getElementById("modalheader").style.backgroundColor = "rgb(176, 28, 28)";
            document.getElementById("textostringheader").style.color = "rgb(255, 255, 255)";


        } else if( caso == "SUCCESS") {
            
            document.getElementById("modalheader").style.backgroundColor = "rgb(2, 102, 17)";
            document.getElementById("textostringheader").style.color = "rgb(255, 255, 255)";

        }    
        
    }

    function DismissModalCreditoCliente() {
         
        console.log("ANTES Bandera pago a credito: ", pagoacredito );
        pagoacredito = false;
        console.log("DESPUES Bandera pago a credito: ", pagoacredito );

        $("#modalventacredito").modal('hide');

    }

    //CONVERSION DEL TEXTO DE TOTAL A TEXO    
	
    function Unidades(num){

        switch(num)
        {
            case 1: return 'UN';
            case 2: return 'DOS';
            case 3: return 'TRES';
            case 4: return 'CUATRO';
            case 5: return 'CINCO';
            case 6: return 'SEIS';
            case 7: return 'SIETE';
            case 8: return 'OCHO';
            case 9: return 'NUEVE';
        }

        return '';
    }//Unidades()

	function Decenas(num){

        let decena = Math.floor(num/10);
        let unidad = num - (decena * 10);

        switch(decena)
        {
            case 1:
                switch(unidad)
                {
                    case 0: return 'DIEZ';
                    case 1: return 'ONCE';
                    case 2: return 'DOCE';
                    case 3: return 'TRECE';
                    case 4: return 'CATORCE';
                    case 5: return 'QUINCE';
                    default: return 'DIECI' + Unidades(unidad);
                }
            case 2:
                switch(unidad)
                {
                    case 0: return 'VEINTE';
                    default: return 'VEINTI' + Unidades(unidad);
                }
            case 3: return DecenasY('TREINTA', unidad);
            case 4: return DecenasY('CUARENTA', unidad);
            case 5: return DecenasY('CINCUENTA', unidad);
            case 6: return DecenasY('SESENTA', unidad);
            case 7: return DecenasY('SETENTA', unidad);
            case 8: return DecenasY('OCHENTA', unidad);
            case 9: return DecenasY('NOVENTA', unidad);
            case 0: return Unidades(unidad);
        }
    }//Decenas()

	function DecenasY(strSin, numUnidades) {
        if (numUnidades > 0)
            return strSin + ' Y ' + Unidades(numUnidades)

        return strSin;
    }//DecenasY()

    function Centenas(num) {
        let centenas = Math.floor(num / 100);
        let decenas = num - (centenas * 100);

        switch(centenas)
        {
            case 1:
                if (decenas > 0)
                    return 'CIENTO ' + Decenas(decenas);
                return 'CIEN';
            case 2: return 'DOSCIENTOS ' + Decenas(decenas);
            case 3: return 'TRESCIENTOS ' + Decenas(decenas);
            case 4: return 'CUATROCIENTOS ' + Decenas(decenas);
            case 5: return 'QUINIENTOS ' + Decenas(decenas);
            case 6: return 'SEISCIENTOS ' + Decenas(decenas);
            case 7: return 'SETECIENTOS ' + Decenas(decenas);
            case 8: return 'OCHOCIENTOS ' + Decenas(decenas);
            case 9: return 'NOVECIENTOS ' + Decenas(decenas);
        }

        return Decenas(decenas);
    }//Centenas()

	function Seccion(num, divisor, strSingular, strPlural) {
        let cientos = Math.floor(num / divisor)
        let resto = num - (cientos * divisor)

        let letras = '';

        if (cientos > 0)
            if (cientos > 1)
                letras = Centenas(cientos) + ' ' + strPlural;
            else
                letras = strSingular;

        if (resto > 0)
            letras += '';

        return letras;
    }//Seccion()

    function Miles(num) {
        let divisor = 1000;
        let cientos = Math.floor(num / divisor)
        let resto = num - (cientos * divisor)

        //let strMiles = Seccion(num, divisor, 'UN MIL', 'MIL');
		let strMiles = Seccion(num, divisor, 'MIL', 'MIL');
        let strCentenas = Centenas(resto);

        if(strMiles == '')
            return strCentenas;

        return strMiles + ' ' + strCentenas;
    }//Miles()

	function Millones(num) {
        let divisor = 1000000;
        let cientos = Math.floor(num / divisor)
        let resto = num - (cientos * divisor)

        let strMillones = Seccion(num, divisor, 'UN MILLON DE', 'MILLONES DE');
        let strMiles = Miles(resto);

        if(strMillones == '')
            return strMiles;

        return strMillones + ' ' + strMiles;
    }//Millones()

	function NumeroALetras(num, currency) {
        currency = currency || {};
        let data = {
            numero: num,
            enteros: Math.floor(num),
            centavos: (((Math.round(num * 100)) - (Math.floor(num) * 100))),
            letrasCentavos: '',
            letrasMonedaPlural: currency.plural || 'PESOS',//'PESOS', 'Dólares', 'Bolívares', 'etcs'
            letrasMonedaSingular: currency.singular || 'PESO', //'PESO', 'Dólar', 'Bolivar', 'etc'
            letrasMonedaCentavoPlural: currency.centPlural || 'CENTAVOS',
            letrasMonedaCentavoSingular: currency.centSingular || 'CENTAVO'
        };

        if (data.centavos > 0) {
            data.letrasCentavos = 'CON ' + (function () {
                    if (data.centavos == 1)
                        return Millones(data.centavos) + ' ' + data.letrasMonedaCentavoSingular;
                    else
                        return Millones(data.centavos) + ' ' + data.letrasMonedaCentavoPlural;
                })();
        };

        if(data.enteros == 0)
            return 'CERO ' + data.letrasMonedaPlural + ' ' + data.letrasCentavos;
        if (data.enteros == 1)
            return Millones(data.enteros) + ' ' + data.letrasMonedaSingular + ' ' + data.letrasCentavos;
        else
            return Millones(data.enteros) + ' ' + data.letrasMonedaPlural + ' ' + data.letrasCentavos;
    }