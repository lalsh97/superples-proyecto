

            function MostrarModalConfirmacionEliminacion() {

                var idproveedor = document.getElementById("idproveedor").value;

                var ruta  = "{{ route('verificarproductosproveedor') }}";
                var url = ruta + "/" + idproveedor;

                //Realiza la búsqueda
                $.get( url ).done( function( data ) {
                    var content = JSON.parse( JSON.stringify( data ) );

                    //Verificar si el contenido de la respuesta de la api no esta vacía.
                    if( content.length > 0 ) {

                        var totalproductos = content[0].total;

                        var nombreproveedor = document.getElementById("nombre").value + " " + document.getElementById("apellidopat").value + " " + document.getElementById("apellidomat").value;

                        //El proveedor tiene productos asociados y se deben mostrar dos opciones para eliminar.
                        if( totalproductos > 0 ) {

                            document.getElementById("textoerrorbody").innerHTML =   "<p>El proveedor <strong> " + nombreproveedor + "</strong> ,aún tiene <strong> " + totalproductos + "</strong> productos asociados</p>" +
                                                                                    "<p>¿Está seguro que desea eliminarlo?</p>" +
                                                                                    "<br>" +
                                                                                    "<p style='font-size:16px; text-align: left;'><strong> OPCIONES:</strong></p>" +
                                                                                    "<p style='text-align: left;'> <strong>Transferir</strong> Todos los productos asociados a este proveedor serán transferidos al <strong> proveedor general </strong> </p>" +
                                                                                    "<p style='text-align: left;'> <strong>Eliminar</strong> Tanto el proveedor como sus productos asociados serán eliminados </p>";

                            document.getElementById("botonTransferir").style.display = "block";

                            document.getElementById("botonCancelar").style = "margin-left: 42%;";
                            document.getElementById("botonEliminar").style = "margin-right: 15%;";

                        } else if( totalproductos <= 0 ) {//El proveedor no tiene productos asociados y se debe mostrar la opción default para eliminar.


                            document.getElementById("textoerrorbody").innerHTML =   "<p>¿Está seguro que desea eliminar al proveedor: <strong>" + nombreproveedor + "</strong> ? </p>";


                            document.getElementById("botonTransferir").style.display = "none";

                            document.getElementById("botonCancelar").style = "margin-left: 72%;";

                        }

                        $('#ModalConfirmacionEliminacion').modal('show');

                    } else {

                    }

                }).catch(function( error) {
                    console.log("El error es: ", error);
                });

            }

            function DismissModalConfirmacionEliminacion() {

                $("#ModalConfirmacionEliminacion").modal('hide');

            }

            function EliminarTransferirProductos() {

                $("#ModalConfirmacionEliminacion").modal('hide');


                var idproveedor = document.getElementById("idproveedor").value;

                location.href = "/transferircambiarestadoproveedor/" + idproveedor;

            }

            function EliminarProveedor() {

                $("#ModalConfirmacionEliminacion").modal('hide');

                var idproveedor = document.getElementById("idproveedor").value;

                location.href ="/eliminarcambiarestadoproveedor/" + idproveedor;

            }

            function Regresar() {
                location.href = "/home";
            }