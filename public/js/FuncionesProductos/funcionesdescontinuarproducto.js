function imgErrorProducto( image ) {
                
    var urldef = "/images/Productos/ProductoDefault.png";
    image.src = urldef;

}


function MostrarModalConfirmacionEliminacion() {

    var codigobarras = document.getElementById("codigo").value;

    var descripcion = document.getElementById("descripcion").value;

    document.getElementById("textoerrorbody").innerHTML = "<p>¿Está seguro que desea descontinuar el producto <strong>" + descripcion + "</strong> con el código de barras: <strong>" + codigobarras + "</strong> ?</p>";

    $('#ModalConfirmacionEliminacion').modal('show');                    

}

function DismissModalConfirmacionEliminacion() {

    $("#ModalConfirmacionEliminacion").modal('hide');

}

function EliminarProducto() {

    $("#ModalConfirmacionEliminacion").modal('hide');

    var codigo = document.getElementById("codigo").value;

    location.href ="/descontinuarcambiarestadoproducto/" + codigo;

}

function Regresar() {
    location.href = "/home";
}