
            function MostrarModalConfirmacionEliminacion() {

                var idempleado = document.getElementById("idempleado").value;

                var nombreempleado = document.getElementById("nombre").value + " " + document.getElementById("apellidopat").value + " " + document.getElementById("apellidomat").value;

                document.getElementById("textoerrorbody").innerHTML = "<p>¿Está seguro que desea eliminar al empleado <strong>" + nombreempleado + "</strong> ?</p>";

                $('#ModalConfirmacionEliminacion').modal('show');                    

            }

            function DismissModalConfirmacionEliminacion() {

                $("#ModalConfirmacionEliminacion").modal('hide');

            }

            function EliminarEmpleado() {

                $("#ModalConfirmacionEliminacion").modal('hide');

                var idempleado = document.getElementById("idempleado").value;

                location.href ="/eliminarcambiarestadoempleado/" + idempleado;

            }

            function Regresar() {
                location.href = "/home";
            }