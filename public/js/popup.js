  
    var btnAbrirPopup = document.getElementById('btn-abrir-popup'),
    overlayventana = document.getElementById('overlayventana'),
    popup = document.getElementById('popup'),
    btnCerrarPopup = document.getElementById('btn-cerrar-popup');

    btnAbrirPopup.addEventListener('click', function(){
    
        overlayventana.classList.add('active');
        popup.classList.add('active');

    });

    btnCerrarPopup.addEventListener('click', function(e){
    
        e.preventDefault();    
        overlayventana.classList.remove('active');    
        popup.classList.remove('active');

    });


        //Ventana Emergente Presentación Producto
    var btnAbrirPopupPresentacion = document.getElementById('btn-abrir-popuppresentacion'),
    overlayventanapresentacion = document.getElementById('overlayventanapresentacion'),
    popuppresentacion = document.getElementById('popuppresentacion'),
    btnCerrarPopupPresentacion = document.getElementById('btn-cerrar-popuppresentacion');

    btnAbrirPopupPresentacion.addEventListener('click', function(){
        
        overlayventanapresentacion.classList.add('active');
        popuppresentacion.classList.add('active');

    });

    btnCerrarPopupPresentacion.addEventListener('click', function(e){
        
        e.preventDefault();
        overlayventanapresentacion.classList.remove('active');
        popuppresentacion.classList.remove('active');

    });



    //Ventana Emergente Categoria Producto
    var btnAbrirPopupcategoria = document.getElementById('btn-abrir-popupcategoria'),
    overlayventanacategoria = document.getElementById('overlayventanacategoria'),
    popupcategoria = document.getElementById('popupcategoria'),
    btnCerrarPopupcategoria = document.getElementById('btn-cerrar-popupcategoria');

    btnAbrirPopupcategoria.addEventListener('click', function(){
        
        overlayventanacategoria.classList.add('active');
        popupcategoria.classList.add('active');

    });

    btnCerrarPopupcategoria.addEventListener('click', function(e){
        
        e.preventDefault();
        overlayventanacategoria.classList.remove('active');
        popupcategoria.classList.remove('active');

    });