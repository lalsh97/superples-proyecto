<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;


use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\Models\Tienda_Modelo;

class DescargarCortesCajaComando extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cortescaja:download';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Comando para descargar los cortes de caja de cada uno de los empleados';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        
        $listaidempleadosregistrados = DB::table('caja')
        ->join( 'empleado', 'caja.idempleado', '=', 'empleado.idempleado' )
        ->select('empleado.idempleado as idempleado')
        ->where('empleado.estado', '=', 1)
        ->get();
    
        if( count( $listaidempleadosregistrados ) > 0 ) {

            foreach( $listaidempleadosregistrados as $empleadoregistrado ) {

                $idempleado = $empleadoregistrado->{'idempleado'};
                $fechaactual = Carbon::now()->format('d-m-Y');

                $consultaidcaja = DB::table('caja')->select('idcaja as idcaja')
                                                    ->where('idempleado', $idempleado )
                                                    ->get();

                $idcaja = $consultaidcaja[0]->{'idcaja'};

                $informacionempleado = DB::table('empleado')->select('nombre as nombre', 'apellidopat as apellidopat', 'apellidomat as apellidomat' )
                                                            ->where('idempleado', $idempleado )
                                                            ->get();
                                                                
                $consultasaldoinicial = DB::table( 'empleadocaja' )
                                                    ->select('saldoinicial as saldoinicial')
                                                    ->where('idempleado', '=', $idempleado )
                                                    ->where('idcaja', '=', $idcaja )
                                                    ->where('fecha', '=', Carbon::today() )
                                                    ->where('estado', '=', 0 )
                                                    ->orderBy('idempleadocaja', 'DESC')
                                                    ->get();

                
                $consultanivelusuario = DB::table('users')
                            ->select('nivel as nivelusuario')
                            ->where('users.idempleado', '=', $idempleado )
                            ->get();
                
                $nivelusuario = $consultanivelusuario[0]->{'nivelusuario'};
                
                if( count( $consultasaldoinicial ) == 1 ) {

                    $consultasventasactual = DB::table( 'venta' )
                            ->join( 'detalleventa', 'venta.idventa', '=', 'detalleventa.idventa' )
                            ->join( 'movimientoinventario', 'detalleventa.idmovimientoinventario', 'movimientoinventario.idmovimientoinventario')
                            ->join( 'inventario', 'detalleventa.idproducto', '=', 'inventario.codigo' )
                            ->join('producto', 'detalleventa.idproducto', '=', 'producto.codigo' )
                                    ->select( 'venta.idventa',
                                            'detalleventa.idproducto as codigoproducto', 'detalleventa.cantidad as cantidadventa',
                                            'producto.descripcion as descripcionproducto', 'movimientoinventario.PrecioCostoAct  as preciocostoproducto',
                                            'movimientoinventario.PrecioVentaAct as precioventaproducto' )
                                    ->where( 'venta.fecha', '=', Carbon::today() )
                                    ->where( 'venta.idempleado', '=', $idempleado )
                                    ->where( 'venta.idcaja', '=', $idcaja )
                                    ->where( 'venta.estado', '=', 0 )
                                    ->orderBy('venta.idventa', 'ASC')
                                    ->get();

                    $consultatotal = DB::table( 'venta' )
                                        ->select( DB::raw( "SUM( venta.total ) as totalventas" ) )
                                        ->where( 'venta.fecha', '=', Carbon::today() )
                                        ->where( 'venta.idempleado', '=', $idempleado )
                                        ->where( 'venta.idcaja', '=', $idcaja )
                                        ->where( 'venta.estado', '=', 0 )
                                        ->get();
                                        
                    $consultatotaldescuento = DB::table( 'venta' )
                                        ->select( DB::raw( "SUM( venta.descuento ) as totaldescuento" ) )
                                        ->where( 'venta.fecha', '=', Carbon::today() )
                                        ->where( 'venta.idempleado', '=', $idempleado )
                                        ->where( 'venta.idcaja', '=', $idcaja )
                                        ->where( 'venta.estado', '=', 0 )
                                        ->get();
                    
                    $consultatotalproductosvendidos = DB::table( 'venta' )
                                                    ->join( 'detalleventa', 'venta.idventa', '=', 'detalleventa.idventa' )
                                                    ->select( DB::raw( "SUM( detalleventa.cantidad ) as cantidadtotalproductosvendidos" ) )
                                                    ->where( 'venta.fecha', '=', Carbon::today() )
                                                    ->where( 'venta.idempleado', '=', $idempleado )
                                                    ->where( 'venta.idcaja', '=', $idcaja )
                                                    ->where( 'venta.estado', '=', 0 )
                                                    ->get();
                                
                    $consultatotalventasefectivo = DB::table( 'venta' )
                                                    ->select( DB::raw( "SUM( venta.total ) as consultatotalventasefectivo" ) )
                                                    ->where( 'venta.fecha', '=', Carbon::today() )
                                                    ->where( 'venta.idtipopago', '=', 1 )
                                                    ->where( 'venta.idempleado', '=', $idempleado )
                                                    ->where( 'venta.idcaja', '=', $idcaja )
                                                    ->where( 'venta.estado', '=', 0 )
                                                    ->get();
                                
                    $consultatotalventascreditocompleto = DB::table( 'venta' )
                                                    ->select( DB::raw( "SUM( venta.total ) as totalventascreditocompleto" ) )
                                                    ->where( 'venta.fecha', '=', Carbon::today() )
                                                    ->where( 'venta.idtipopago', '=', 2 )
                                                    ->where( 'venta.idempleado', '=', $idempleado )
                                                    ->where( 'venta.idcaja', '=', $idcaja )
                                                    ->where( 'venta.estado', '=', 0 )
                                                    ->get();
                                
                    $consultatotalventascreditopartes = DB::table( 'venta' )
                                                    ->select( DB::raw( "SUM( venta.efectivo ) as totalpagadoefectivo" ),  DB::raw( "SUM( venta.total ) as total" ) )
                                                    ->where( 'venta.fecha', '=', Carbon::today() )
                                                    ->where( 'venta.idtipopago', '=', 3 )
                                                    ->where( 'venta.idempleado', '=', $idempleado )
                                                    ->where( 'venta.idcaja', '=', $idcaja )
                                                    ->where( 'venta.estado', '=', 0 )
                                                    ->get();
                                
                    $consultatotalventas = DB::table( 'venta' )
                                                    ->select( DB::raw( "count( venta.idventa ) as consultatotalventas" ) )
                                                    ->where( 'venta.fecha', '=', Carbon::today() )
                                                    ->where( 'venta.idempleado', '=', $idempleado )
                                                    ->where( 'venta.idcaja', '=', $idcaja )
                                                    ->where( 'venta.estado', '=', 0 )
                                                    ->get();
                        
                    $consultaabonoscreditos = DB::table('abonoscredito')
                                                        ->select( DB::raw( "SUM(Cantidad) as totalabonado" ) )
                                                        ->where('abonoscredito.fecha', '=', Carbon::today()  )
                                                        ->where('abonoscredito.idempleado', '=', $idempleado )
                                                        ->where('abonoscredito.idcaja', '=', $idcaja )
                                                        ->where('abonoscredito.estado', '=', 0 )
                                                        ->get();
                                                                                                            
                    $tienda = Tienda_Modelo::select('*')->take(1)->first();
                    
                    $usuariowindows =  get_current_user();
                    $nombreempleado = $informacionempleado[0]->{'nombre'};

                    /*
                    $pdf = \PDF::setOptions([
                                'images' => true,
                                'enable-javascript' => true,
                                'javascript-delay' => 5000,
                                'enable-smart-shrinking' => true,
                                'no-stop-slow-scripts' => true
                        ])
                    */

                    $pdf = \PDF::loadView('vistasventas/cortescaja/exportarcortediaventassesioncerrada', array(    'consultasventasactual' => $consultasventasactual, 
                                                                                                    'consultatotal' => $consultatotal, 
                                                                                                    'consultatotaldescuento' => $consultatotaldescuento,
                                                                                                    'consultatotalproductosvendidos' => $consultatotalproductosvendidos, 
                                                                                                    'consultatotalventasefectivo' => $consultatotalventasefectivo,
                                                                                                    'consultatotalventascreditocompleto' => $consultatotalventascreditocompleto,
                                                                                                    'consultatotalventascreditopartes' => $consultatotalventascreditopartes,
                                                                                                    'consultatotalventas' => $consultatotalventas,
                                                                                                    'fechaactual' => $fechaactual, 
                                                                                                    'tienda' => $tienda, 
                                                                                                    'consultasaldoinicial' => $consultasaldoinicial,
                                                                                                    'idempleado' => $idempleado,
                                                                                                    'nivelusuario' => $nivelusuario,
                                                                                                    'idcaja' => $idcaja,
                                                                                                    'informacionempleado' => $informacionempleado,
                                                                                                    'consultaabonoscreditos' => $consultaabonoscreditos ) )
                        ->setPaper('a4', 'landscape')->setWarnings(false);
                    
                    $pdf->save("C:/Users/" . $usuariowindows ."/Downloads/CorteCajaVentas ". $nombreempleado ." ". $fechaactual .".pdf");
                    
                    DB::table('venta')
                                ->where( 'venta.fecha', '=', Carbon::today() )
                                ->where( 'venta.idempleado', '=', $idempleado )
                                ->where( 'venta.idcaja', '=', $idcaja )
                                ->update( array( 'Estado' => 1 ) );
                                
                    DB::table('pedido')
                    ->where( 'pedido.fecha', '=', Carbon::today() )
                    ->where( 'pedido.idempleado', '=', $idempleado )
                    ->update( array( 'Estado' => 1 ) );
                        
                    DB::table('abonoscredito')
                    ->where('abonoscredito.fecha', '=', Carbon::today()  )
                    ->where('abonoscredito.idempleado', '=', $idempleado )
                    ->where('abonoscredito.idcaja', '=', $idcaja )
                    ->update( array( 'estado' => 1 ));

                }

            }

        }

        echo "DESCARGA DE TODOS LOS CORTES DE CAJA REALIZADA.";


    }
}
