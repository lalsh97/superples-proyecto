<?php

namespace App\Exports;

use App\Models\Producto_Modelo;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use DB;

class Productos implements FromView
{

  use Exportable;

  private $date;

    public function view():view {

      return view('exports.Productos',
      [
          //    'users'=>Producto_Modelo::all()
          'Productos' =>DB::table('producto')
          ->join('marca', 'producto.idmarca', '=', 'marca.idmarca')
          ->join('presentacion', 'producto.idpresentacion', '=', 'presentacion.idpresentacion')
          ->join('proveedor', 'producto.idproveedor', '=', 'proveedor.idproveedor')
          ->join('categoria', 'producto.idcategoria', '=', 'categoria.idcategoria')
          ->select('producto.codigo', 'producto.idmarca', 'marca.descripcion as nombremarca',
                  'producto.idpresentacion','presentacion.medida as nombrepresentacion', 'producto.idproveedor',
                  DB::raw( "CONCAT( proveedor.nombre,' ', proveedor.apellidopat,' ', proveedor.apellidomat ) AS nombrecompleto" ),
                  'producto.idcategoria', 'categoria.nombre as nombrecategoria', 'producto.descripcion',
                  'producto.preciocosto','producto.precioventa', 'producto.iva')
                  ->where('producto.estado',1)
                  ->orderBy('codigo','ASC')
                  ->get()

            ]);

    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Producto_Modelo::all();
    }
}
