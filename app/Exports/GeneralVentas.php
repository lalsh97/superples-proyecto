<?php

namespace App\Exports;


use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use DB;

use App\Models\Producto_Modelo;
use App\Models\Inventario_Modelo;

class GeneralVentas implements FromView {

    use Exportable;

    private $date;

    public function view():view {

        return view('exports.GeneralVentas',
        [
            //    'users'=>Producto_Modelo::all()
            'GeneralVentas' => DB::table('movimientoinventario')
                            ->join('producto', 'producto.codigo', '=', 'movimientoinventario.codigo')
                            ->select( 'movimientoinventario.codigo as codigo', 'producto.descripcion as descripcion',                       
                            DB::raw( "SUM(cantidadmov) as cantidad" ),
                            DB::raw( "SUM(preciocostoact * cantidadmov) as preciocosto" ),
                            DB::raw( "SUM(precioventaact * cantidadmov) as precioventa" ),
                            DB::raw( "SUM(precioventaact * cantidadmov) - SUM(preciocostoact * cantidadmov)  as gananciatotal" ), )
                            ->where( 'movimientoinventario.idtipomovimiento', '=', 203 )
                            ->groupBy( 'movimientoinventario.codigo' )
                            ->groupBy( 'producto.descripcion' )
                            ->get()
          ]);
          
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Producto_Modelo::all();
    }
}
