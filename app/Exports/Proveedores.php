<?php

namespace App\Exports;

use App\Models\Proveedor_Modelo;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use DB;

class Proveedores implements FromView {

      use Exportable;

      private $date;

      public function view():view {
        
          return view('exports.Proveedores',
                  [
                      //    'users'=>Producto_Modelo::all()
                      //'Proveedores' =>Proveedor_Modelo::where('Estado','1')->get()
                      'Proveedores' => DB::table('proveedor')->select('idProveedor',
                        DB::raw( "CONCAT( nombre,' ', apellidopat,' ', apellidomat ) AS nombrecompleto" ),
                        DB::raw( "CONCAT( calle,' #', numero,' ', codigop, ' ', colonia, ' ' ) AS direccion" ),
                        'telefono','correo', 'rfc as Rfc','estado')
                      ->where('estado',1)
                      ->orderBy('idproveedor','ASC')
                    ->get()
                  ]);
      }

      /**
      * @return \Illuminate\Support\Collection
      */
      public function collection() {
          
          return Proveedor_Modelo::all();

      }
}
