<?php

namespace App\Exports;

use App\Models\Promocion_Modelo;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use DB;

class Promociones implements FromView
{

  use Exportable;

  private $date;

  public function view():view
  {
    return view('exports.Promociones',
  [
//    'users'=>Producto_Modelo::all()
'promociones' => Promocion_Modelo::where('estado','1')->get() 

  ]);
  }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Producto_Modelo::all();
    }
}
