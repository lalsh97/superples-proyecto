<?php

namespace App\Exports;


use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use DB;

use App\Models\Cliente_Modelo;

class Clientes implements FromView {

    use Exportable;

    private $date;

    public function view():view {

        return view('exports.Clientes',
        [
            //    'users'=>Producto_Modelo::all()
            'Clientes' => DB::table('cliente')->select('idcliente',
                  DB::raw( "CONCAT( nombre,' ', apellidopat,' ', apellidomat ) AS nombrecompleto" ),
                  'fechanac', 'sexo', 'telefono', 'curp','estado')
                  ->where('estado',1)
                  ->orderBy('idcliente','ASC')
                ->get()
          ]);
          
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Cliente_Modelo::all();
    }
}
