<?php

namespace App\Exports;

use App\Models\Promocion_Modelo;
use Maatwebsite\Excel\Concerns\FromCollection;

class producto implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Promocion_Modelo::all();
    }
}
