<?php

namespace App\Exports;


use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use DB;

use App\Models\Venta_Modelo;

class Ventas implements FromView {

    use Exportable;

    private $date;

    public function view():view {

        return view('exports.Ventas',
        [
            //    'users'=>Producto_Modelo::all()
            'Ventas' => DB::table('venta')
                    ->join('empleado', 'venta.idempleado', '=', 'empleado.idempleado')
                    ->join('cliente', 'venta.idcliente', '=', 'cliente.idcliente')
                    ->select( DB::raw("CONCAT( empleado.nombre,' ', empleado.apellidopat,' ',empleado.apellidomat) as nombrecompletoempleado"),
                              DB::raw("CONCAT( cliente.nombre,' ', cliente.apellidopat,' ',cliente.apellidomat) as nombrecompletocliente"),
                         'venta.idventa as idventa','venta.fecha as fechaventa', 'venta.total as totalventa', 'venta.idtipopago as tipopago' )
                         ->orderBy('venta.idventa', 'asc')
                        ->get()
          ]);
          
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Venta_Modelo::all();        
    }
}
