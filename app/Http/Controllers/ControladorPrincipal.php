<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;


use App\Models\Proveedor_Modelo;

class ControladorPrincipal extends Controller
{

    public function imprimir() {

        $pdf = \PDF::loadView('ejemplopdf');
        return $pdf->download('ejemplo.pdf');

   }

   public function modificarinformacionusuario() {
        
        $iduser = Auth::User()->id;

        $informacionusuario = DB::table('users')->select( 'users.name as nombreusuario',
                                        'users.id as idusuario',
                                        'users.email as emailusuario', 
                                        'users.password as passwordusuario',
                                        'users.avatar as avatarusuario' )
                                        ->where('users.id',$iduser)
                                        ->take(1)
                                        ->first();

        return view('vistasajustes/vistasusuarios/modificarinformacionusuario')->with( 'informacionusuario', $informacionusuario );

   }

   public function actualizarinformacionusuario( Request $request , $idusuario ) {
                    
    
        if( $request->hasFile('avatar') ) {

                
            $file = $request->file( 'avatar' );
            $nameusuario = str_replace( "/", " ", $request -> input('name') );
            //$name = time().$file->getClientOriginalName();

            $nameavatar = 'Usuarios/' . $nameusuario . "." . $request->file('avatar')->extension();
            $file->move( public_path().'/images/Usuarios/', $nameusuario . "." . $request->file('avatar')->extension() );
            

            /*  //SE GUARDAN EN LA CARPETA STORAGE/APP/PUBLIC/PRODUCTOS
            //$name = $request->file('avatar')->store('public/Productos');

            $name = $request->file('avatar')->storeAs(
                'public/Productos',  $request -> input('descripcion') . "." . $request->file('avatar')->extension()
            );

            */

        } else {

            $consultausuario = DB::table('users')->select('avatar as avatar')->where('id', '=', $idusuario )->get();

            $nameavatar = $consultausuario[0]->{'avatar'};
            
        }
        

        //LA CONTRASEÑA FUE CAMBIADA
        if( $request->switchcambiarpassword == "on" ) {
            
            DB::table('users')
            ->where( 'id', $idusuario )  // find your user by their email
            ->limit(1)  // optional - to ensure only one record is updated.
            ->update( array( 'name' => $request->name,
                            'email'=> $request->email,
                            'password' => Hash::make( $request->password ),
                            'avatar' => $nameavatar ) );  // update the record in the DB.
    
        } else {
            
            DB::table('users')
            ->where( 'id', $idusuario )  // find your user by their email
            ->limit(1)  // optional - to ensure only one record is updated.
            ->update( array( 'name' => $request->name,
                            'email'=> $request->email,
                            'avatar' => $nameavatar ) );  // update the record in the DB.
            
            
        }

        
        return redirect()->to('home');

   }


   public function listadoempleadoscambioinformacionusuario() {

        $idempleado = Auth::User()->idEmpleado;
        
         $consultatiendaempleado = DB::table('empleado')->select('idtienda as idtienda')
                                                ->where('idempleado', $idempleado )
                                                ->get();

        $idtienda = $consultatiendaempleado[0]->{'idtienda'};
           
        $listadoempleadoscambioinformacionusuario = DB::table('empleado')
        ->join('users', 'empleado.idempleado', '=', 'users.idempleado')
        ->select('empleado.idempleado','empleado.idTienda',
        DB::raw( "CONCAT( empleado.nombre,' ', empleado.apellidopat,' ', empleado.apellidomat ) AS nombrecompleto" ),
                    'empleado.rfc', 'empleado.fechanac', 'empleado.sexo', 'empleado.curp', 'empleado.calle',
                    'empleado.codigop', 'empleado.numero', 'empleado.colonia', 'empleado.municipio', 'empleado.ciudad',
                    'empleado.telefono', 'empleado.correo','empleado.foto', 'empleado.estado' )
                ->where( 'empleado.estado', 1 )                
                ->where( 'empleado.idempleado', '<>', 1 )
                ->where( 'empleado.idtienda', '=', $idtienda )
                ->where( 'users.nivel', '=', 2 )
                ->orderBy( 'empleado.idempleado', 'ASC' )
                ->get();

        return view('vistasajustes/vistasusuarios/modificacioninformacionempleados/listadoempleadoscambioinformacionusuario')->with( 'listadoempleadoscambioinformacionusuario', $listadoempleadoscambioinformacionusuario );

   }
   

    public function modificarinformacionusuariocuentaempleado( $idempleado ) {

        $informacionusuario = DB::table('users')->select( 'users.name as nombreusuario',
                                        'users.id as idusuario',
                                        'users.email as emailusuario', 
                                        'users.password as passwordusuario',
                                        'users.avatar as avatarusuario' )
                                        ->where('users.idempleado',$idempleado)
                                        ->take(1)
                                        ->first();

        return view('vistasajustes/vistasusuarios/modificacioninformacionempleados/modificarinformacionusuariocuentaempleado')->with( 'informacionusuario', $informacionusuario );

    }
    

            //FUNCIONES REACTIVAR
   public function menuprincipalreactivar() {

       return view('vistasajustes/vistasreactivar/menuprincipalreactivar');

   }

   public function listadoempleadosdesactivados() {
       
        $listadoempleados = DB::table('empleado')
        ->select('idempleado','idTienda',
        DB::raw( "CONCAT( nombre,' ', apellidopat,' ', apellidomat ) AS nombrecompleto" ),
                    'rfc', 'fechanac', 'sexo', 'curp', 'calle', 'codigop', 'numero', 'colonia', 'municipio', 'ciudad',
                    'telefono', 'correo','foto', 'estado' )
                ->where( 'estado', 0 )                
                ->where( 'idempleado', '<>', 1 )
                ->orderBy( 'idempleado', 'ASC' )
                ->get();
            //->paginate(5);
                                
        if( count( $listadoempleados ) == 0 ) {

            Session::flash('danger', ' Aún no hay empleados desactivados ');

        }

        return view('vistasajustes/vistasreactivar/listadoempleadosdesactivados')->with('registroempleados', $listadoempleados);

   }

   public function reactivarempleado( $idempleado ) {

        DB::table('empleado')
            ->where( 'idempleado', $idempleado )
            ->update( array( 'Estado' => 1 ) );

        return redirect()->to('home');

   }

   public function listadoproductosdesactivados() {
        
        $listadoproductos = DB::table('producto')
        ->join('marca', 'producto.idmarca', '=', 'marca.idmarca')
        ->join('presentacion', 'producto.idpresentacion', '=', 'presentacion.idpresentacion')
        ->join('proveedor', 'producto.idproveedor', '=', 'proveedor.idproveedor')
        ->join('categoria', 'producto.idcategoria', '=', 'categoria.idcategoria')
        ->select('producto.codigo', 'producto.idmarca', 'marca.descripcion as nombremarca',
                'producto.idpresentacion','presentacion.medida as nombrepresentacion', 'producto.idproveedor',
                DB::raw( "CONCAT( proveedor.nombre,' ', proveedor.apellidopat,' ', proveedor.apellidomat ) AS nombrecompleto" ),
                'producto.idcategoria', 'categoria.nombre as nombrecategoria', 'producto.descripcion',
                'producto.preciocosto','producto.precioventa', 'producto.iva')
                ->where( 'producto.estado', 0 )
                ->where( 'proveedor.estado', 1 )
                ->orderBy('codigo','ASC')
                ->get();
                    
        if( count( $listadoproductos ) == 0 ) {

            Session::flash('danger', ' Aún no productos desactivados ');

        }

        return view('vistasajustes/vistasreactivar/listadoproductosdesactivados')->with('registroproductos', $listadoproductos);

   }

   public function reactivarproducto( $codigo ) {
        
        DB::table('producto')
        ->where( 'Codigo', $codigo )
        ->update( array( 'Estado' => 1 ) );

        return redirect()->to('home');

   }

   public function listadoproveedoresdesactivados() {
        
        $listadoproveedores = Proveedor_Modelo::select( 'idproveedor as idProveedor',
                DB::raw( "CONCAT( nombre,' ', apellidopat,' ', apellidomat ) AS nombrecompleto" ),
                'rfc as Rfc', 'telefono as Telefono', 'empresa as Empresa', 'estado' )
            ->where( 'estado', 0 )
            ->where('idProveedor', '!=', '1')
            ->orderBy( 'idproveedor', 'ASC' )
            ->get();
                                
        if( count( $listadoproveedores ) == 0 ) {

            Session::flash('danger', ' Aún no hay proveedores desactivados ');

        }

    return view ('vistasajustes/vistasreactivar/listadoproveedoresdesactivados')->with( 'registroproveedores', $listadoproveedores );

   }

   public function reactivarproveedor( $idproveedor ) {     

        DB::table('proveedor')
        ->where( 'idProveedor', $idproveedor )
        ->update( array( 'Estado' => 1 ) );

        return redirect()->to('home');

   } 

   public function listadoclientesdesactivados() {        

        $listadoclientes = DB::table('cliente')
                ->select('idcliente',
                DB::raw( "CONCAT( nombre,' ', apellidopat,' ', apellidomat ) AS nombrecompleto" ),
                'fechanac', 'sexo', 'telefono', 'curp','estado')
                ->where( 'estado', 0 )
                ->where( 'idCliente', '<>', 1)
                ->orderBy('idcliente','ASC')
                ->get();
                                
        if( count( $listadoclientes ) == 0 ) {

            Session::flash('danger', ' Aún no hay clientes desactivados ');

        }

        return view('vistasajustes/vistasreactivar/listadoclientesdesactivados')->with( 'registroclientes', $listadoclientes );

   }

   public function reactivarcliente( $idcliente ) {

        DB::table('cliente')
                    ->where( 'idCliente' ,$idcliente ) 
                    ->update( array( 'Estado' => 1 ) );

                    

        return redirect()->to('home');

   }

   public function manualdeusuario() {

        return view('vistasajustes/manuales/manualdeusuario');

   }

}
