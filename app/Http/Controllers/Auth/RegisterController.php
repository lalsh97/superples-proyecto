<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

use Carbon\Carbon;

use App\Models\Empleado_Modelo;
use App\Models\Caja_Modelo;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {

        
        $request = app('request');
        
        if( $request->hasFile('avatar') ) {
                
            $file = $request->file( 'avatar' );
            $nameusuario = str_replace( "/", " ", $request -> input('name') );

            $nameavatar = 'Usuarios/' . $nameusuario . "." . $request->file('avatar')->extension();
            $file->move( public_path().'/images/Usuarios/', $nameusuario . "." . $request->file('avatar')->extension() );

        } else {

            $nameavatar = 'Usuarios/AvatarUserDefault.png';
            
        }


        $idtienda = 1;
        $nombre =       $data['nombre'];
        $apellidopat =  $data['apellidopat'];
        $apellidomat =  $data['apellidomat'];
        $sexo =         $data['sexo'];
        $fechanac =     $data['fechanac'];
        $curp =         $data['curp'];
        $rfc =          $data['rfc'];
        $telefono =     $data['telefono'];
        $correo =       $data['email'];

        $calle =        $data['calle'];
        $numero =       $data['numero'];
        $codigop =      $data['codigop'];
        $colonia =      $data['colonia'];
        $municipio =    $data['municipio'];
        $ciudad =       $data['ciudad'];
        $estado = 1;
         
        $empleado = Empleado_Modelo::create( ['idtienda' => $idtienda, 'nombre' => $nombre,
        'apellidopat' => $apellidopat, 'apellidomat' => $apellidomat, 'sexo' => $sexo,
        'fechanac' => $fechanac, 'curp' => $curp, 'rfc' => $rfc, 'telefono' => $telefono,
        'correo' => $correo, 'calle' => $calle, 'numero' => $numero, 'codigop' => $codigop,
        'colonia' => $colonia, 'municipio' => $municipio, 'ciudad' => $ciudad,
         'estado' => $estado] );
        
                   
        $consulta = Empleado_Modelo::select('idEmpleado')
                    ->orderBy('idEmpleado', 'desc')
                    ->limit(1)
                    ->get();    

        foreach( $consulta as $k ) {
            $idEmpleado = $k->{'idEmpleado'};
        }
        
                        
        $fechaactual = Carbon::now();

        Caja_Modelo::create( [
            'idempleado' => $idEmpleado,
            'fecha' => $fechaactual
        ]);

        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'idempleado' => $idEmpleado,
            'nivel' => $data['nivel'],
            'avatar' => $nameavatar,
        ]);
    }
}
