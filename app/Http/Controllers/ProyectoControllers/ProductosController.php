<?php

namespace App\Http\Controllers\ProyectoControllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;

    //Modelos
use App\Models\Marca_Modelo;
use App\Models\Presentacion_Modelo;
use App\Models\Proveedor_Modelo;
use App\Models\Categoria_Modelo;
use App\Models\Producto_Modelo;
use App\Models\Promocion_Modelo;

class ProductosController extends Controller
{

    public function listadoproductos() {

        $listadoproductos = DB::table('producto')
        ->join('marca', 'producto.idmarca', '=', 'marca.idmarca')
        ->join('presentacion', 'producto.idpresentacion', '=', 'presentacion.idpresentacion')
        ->join('proveedor', 'producto.idproveedor', '=', 'proveedor.idproveedor')
        ->join('categoria', 'producto.idcategoria', '=', 'categoria.idcategoria')
        ->select('producto.codigo', 'producto.idmarca', 'marca.descripcion as nombremarca',
                'producto.idpresentacion','presentacion.medida as nombrepresentacion', 'producto.idproveedor',
                DB::raw( "CONCAT( proveedor.nombre,' ', proveedor.apellidopat,' ', proveedor.apellidomat ) AS nombrecompleto" ),
                'producto.idcategoria', 'categoria.nombre as nombrecategoria', 'producto.descripcion',
                'producto.preciocosto','producto.precioventa', 'producto.iva')
                ->where('producto.estado',1)
                ->where('proveedor.estado',1)
                ->orderBy('codigo','ASC')
                ->get();
    
        $listadoproductossortedmarca = DB::table('producto')
            ->join('marca', 'producto.idmarca', '=', 'marca.idmarca')
            ->join('presentacion', 'producto.idpresentacion', '=', 'presentacion.idpresentacion')
            ->join('proveedor', 'producto.idproveedor', '=', 'proveedor.idproveedor')
            ->join('categoria', 'producto.idcategoria', '=', 'categoria.idcategoria')
            ->select('producto.codigo', 'producto.idmarca', 'marca.descripcion as nombremarca',
                    'producto.idpresentacion','presentacion.medida as nombrepresentacion', 'producto.idproveedor',
                    DB::raw( "CONCAT( proveedor.nombre,' ', proveedor.apellidopat,' ', proveedor.apellidomat ) AS nombrecompleto" ),
                    'producto.idcategoria', 'categoria.nombre as nombrecategoria', 'producto.descripcion',
                    'producto.preciocosto','producto.precioventa', 'producto.iva')
                    ->where('producto.estado',1)
                    ->where('proveedor.estado',1)
                    ->orderBy('marca.descripcion','ASC')
                    ->get();
    
        $listadoproductossortedpresentacion = DB::table('producto')
            ->join('marca', 'producto.idmarca', '=', 'marca.idmarca')
            ->join('presentacion', 'producto.idpresentacion', '=', 'presentacion.idpresentacion')
            ->join('proveedor', 'producto.idproveedor', '=', 'proveedor.idproveedor')
            ->join('categoria', 'producto.idcategoria', '=', 'categoria.idcategoria')
            ->select('producto.codigo', 'producto.idmarca', 'marca.descripcion as nombremarca',
                    'producto.idpresentacion','presentacion.medida as nombrepresentacion', 'producto.idproveedor',
                    DB::raw( "CONCAT( proveedor.nombre,' ', proveedor.apellidopat,' ', proveedor.apellidomat ) AS nombrecompleto" ),
                    'producto.idcategoria', 'categoria.nombre as nombrecategoria', 'producto.descripcion',
                    'producto.preciocosto','producto.precioventa', 'producto.iva')
                    ->where('producto.estado',1)
                    ->where('proveedor.estado',1)
                    ->orderBy('presentacion.medida','ASC')
                    ->get();
    
        $listadoproductossortedcategoria = DB::table('producto')
            ->join('marca', 'producto.idmarca', '=', 'marca.idmarca')
            ->join('presentacion', 'producto.idpresentacion', '=', 'presentacion.idpresentacion')
            ->join('proveedor', 'producto.idproveedor', '=', 'proveedor.idproveedor')
            ->join('categoria', 'producto.idcategoria', '=', 'categoria.idcategoria')
            ->select('producto.codigo', 'producto.idmarca', 'marca.descripcion as nombremarca',
                    'producto.idpresentacion','presentacion.medida as nombrepresentacion', 'producto.idproveedor',
                    DB::raw( "CONCAT( proveedor.nombre,' ', proveedor.apellidopat,' ', proveedor.apellidomat ) AS nombrecompleto" ),
                    'producto.idcategoria', 'categoria.nombre as nombrecategoria', 'producto.descripcion',
                    'producto.preciocosto','producto.precioventa', 'producto.iva')
                    ->where('producto.estado',1)
                    ->where('proveedor.estado',1)
                    ->orderBy('categoria.nombre','ASC')
                    ->get();
        
        $listadoproductossorteddescripcion = DB::table('producto')
            ->join('marca', 'producto.idmarca', '=', 'marca.idmarca')
            ->join('presentacion', 'producto.idpresentacion', '=', 'presentacion.idpresentacion')
            ->join('proveedor', 'producto.idproveedor', '=', 'proveedor.idproveedor')
            ->join('categoria', 'producto.idcategoria', '=', 'categoria.idcategoria')
            ->select('producto.codigo', 'producto.idmarca', 'marca.descripcion as nombremarca',
                    'producto.idpresentacion','presentacion.medida as nombrepresentacion', 'producto.idproveedor',
                    DB::raw( "CONCAT( proveedor.nombre,' ', proveedor.apellidopat,' ', proveedor.apellidomat ) AS nombrecompleto" ),
                    'producto.idcategoria', 'categoria.nombre as nombrecategoria', 'producto.descripcion',
                    'producto.preciocosto','producto.precioventa', 'producto.iva')
                    ->where('producto.estado',1)
                    ->where('proveedor.estado',1)
                    ->orderBy('producto.descripcion','ASC')
                    ->get();

        if( count( $listadoproductos ) == 0 ) {

            Session::flash('danger', ' Aún no hay registros de productos ');

        }

        return view('vistasproductos/listadoproductos')->with( 'registroproductos', $listadoproductos )
                            ->with( 'registroproductosmarca', $listadoproductossortedmarca )            
                            ->with( 'registroproductospresentacion', $listadoproductossortedpresentacion )
                            ->with( 'registroproductoscategoria', $listadoproductossortedcategoria )
                            ->with( 'registroproductosdescripcion', $listadoproductossorteddescripcion );

    }

    public function busquedaproducto( $operacion ) {

        return view('vistasproductos/busquedaproducto')->with( 'operacion' , $operacion );

    }

    public function buscarproducto() {

        $proveedores = Proveedor_Modelo::select('*')->where('estado', 1)->get();
        $categorias = Categoria_Modelo::select('*')->where('estado', 1)->get();

        return view('vistasproductos/buscarproducto')->with( 'proveedores', $proveedores )
                                                        ->with( 'categorias',$categorias );

    }

    public function buscarproductocodigo( $codigo ) {        

        if( !empty( $codigo ) ) {

            $producto = Producto_Modelo::where('codigo', $codigo )->where('estado', 1)->get();

            if( count( $producto ) == 1 ) {

                $listadoproductos = DB::table('producto')
                ->join('marca', 'producto.idmarca', '=', 'marca.idmarca')
                ->join('presentacion', 'producto.idpresentacion', '=', 'presentacion.idpresentacion')
                ->join('proveedor', 'producto.idproveedor', '=', 'proveedor.idproveedor')
                ->join('categoria', 'producto.idcategoria', '=', 'categoria.idcategoria')
                ->select('producto.codigo', 'producto.idmarca', 'marca.descripcion as nombremarca',
                        'producto.idpresentacion','presentacion.medida as nombrepresentacion', 'producto.idproveedor',
                        DB::raw( "CONCAT( proveedor.nombre,' ', proveedor.apellidopat,' ', proveedor.apellidomat ) AS nombrecompleto" ),
                        'producto.idcategoria', 'categoria.nombre as nombrecategoria', 'producto.descripcion',
                        'producto.preciocosto','producto.precioventa', 'producto.iva')
                        ->where( 'producto.codigo',$codigo )
                        ->where('producto.estado',1)
                        ->where('proveedor.estado',1)
                        ->get();

                $listadoproductossortedmarca = DB::table('producto')
                    ->join('marca', 'producto.idmarca', '=', 'marca.idmarca')
                    ->join('presentacion', 'producto.idpresentacion', '=', 'presentacion.idpresentacion')
                    ->join('proveedor', 'producto.idproveedor', '=', 'proveedor.idproveedor')
                    ->join('categoria', 'producto.idcategoria', '=', 'categoria.idcategoria')
                    ->select('producto.codigo', 'producto.idmarca', 'marca.descripcion as nombremarca',
                            'producto.idpresentacion','presentacion.medida as nombrepresentacion', 'producto.idproveedor',
                            DB::raw( "CONCAT( proveedor.nombre,' ', proveedor.apellidopat,' ', proveedor.apellidomat ) AS nombrecompleto" ),
                            'producto.idcategoria', 'categoria.nombre as nombrecategoria', 'producto.descripcion',
                            'producto.preciocosto','producto.precioventa', 'producto.iva')
                            ->where( 'producto.codigo',$codigo )
                            ->where('producto.estado',1)
                            ->where('proveedor.estado',1)
                            ->orderBy('marca.descripcion','ASC')
                            ->get();
            
                $listadoproductossortedpresentacion = DB::table('producto')
                    ->join('marca', 'producto.idmarca', '=', 'marca.idmarca')
                    ->join('presentacion', 'producto.idpresentacion', '=', 'presentacion.idpresentacion')
                    ->join('proveedor', 'producto.idproveedor', '=', 'proveedor.idproveedor')
                    ->join('categoria', 'producto.idcategoria', '=', 'categoria.idcategoria')
                    ->select('producto.codigo', 'producto.idmarca', 'marca.descripcion as nombremarca',
                            'producto.idpresentacion','presentacion.medida as nombrepresentacion', 'producto.idproveedor',
                            DB::raw( "CONCAT( proveedor.nombre,' ', proveedor.apellidopat,' ', proveedor.apellidomat ) AS nombrecompleto" ),
                            'producto.idcategoria', 'categoria.nombre as nombrecategoria', 'producto.descripcion',
                            'producto.preciocosto','producto.precioventa', 'producto.iva')
                            ->where( 'producto.codigo',$codigo )
                            ->where('producto.estado',1)
                            ->where('proveedor.estado',1)
                            ->orderBy('presentacion.medida','ASC')
                            ->get();
            
                $listadoproductossortedcategoria = DB::table('producto')
                    ->join('marca', 'producto.idmarca', '=', 'marca.idmarca')
                    ->join('presentacion', 'producto.idpresentacion', '=', 'presentacion.idpresentacion')
                    ->join('proveedor', 'producto.idproveedor', '=', 'proveedor.idproveedor')
                    ->join('categoria', 'producto.idcategoria', '=', 'categoria.idcategoria')
                    ->select('producto.codigo', 'producto.idmarca', 'marca.descripcion as nombremarca',
                            'producto.idpresentacion','presentacion.medida as nombrepresentacion', 'producto.idproveedor',
                            DB::raw( "CONCAT( proveedor.nombre,' ', proveedor.apellidopat,' ', proveedor.apellidomat ) AS nombrecompleto" ),
                            'producto.idcategoria', 'categoria.nombre as nombrecategoria', 'producto.descripcion',
                            'producto.preciocosto','producto.precioventa', 'producto.iva')
                            ->where( 'producto.codigo',$codigo )
                            ->where('producto.estado',1)
                            ->where('proveedor.estado',1)
                            ->orderBy('categoria.nombre','ASC')
                            ->get();
                
                $listadoproductossorteddescripcion = DB::table('producto')
                    ->join('marca', 'producto.idmarca', '=', 'marca.idmarca')
                    ->join('presentacion', 'producto.idpresentacion', '=', 'presentacion.idpresentacion')
                    ->join('proveedor', 'producto.idproveedor', '=', 'proveedor.idproveedor')
                    ->join('categoria', 'producto.idcategoria', '=', 'categoria.idcategoria')
                    ->select('producto.codigo', 'producto.idmarca', 'marca.descripcion as nombremarca',
                            'producto.idpresentacion','presentacion.medida as nombrepresentacion', 'producto.idproveedor',
                            DB::raw( "CONCAT( proveedor.nombre,' ', proveedor.apellidopat,' ', proveedor.apellidomat ) AS nombrecompleto" ),
                            'producto.idcategoria', 'categoria.nombre as nombrecategoria', 'producto.descripcion',
                            'producto.preciocosto','producto.precioventa', 'producto.iva')
                            ->where( 'producto.codigo',$codigo )
                            ->where('producto.estado',1)
                            ->where('proveedor.estado',1)
                            ->orderBy('producto.descripcion','ASC')
                            ->get();

                    return view('vistasproductos/listadoproductos')->with('registroproductos', $listadoproductos)
                                                    ->with( 'registroproductosmarca', $listadoproductossortedmarca )            
                                                    ->with( 'registroproductospresentacion', $listadoproductossortedpresentacion )
                                                    ->with( 'registroproductoscategoria', $listadoproductossortedcategoria )
                                                    ->with( 'registroproductosdescripcion', $listadoproductossorteddescripcion );

            } else {

                Session::flash('warning', ' No hay productos registrados con ese código de barras ');
                return redirect()->back();

            }

        } else {

            Session::flash('danger', ' Ingrese el código de barra ');
            return redirect()->back();

        }

    }

    public function buscarproductoproveedores( $idproveedor ) {        

        if( !empty( $idproveedor ) ) {

            $producto = Producto_Modelo::where('idProveedor', $idproveedor )->where('estado',1)->get();

            if( count( $producto ) >= 1 ) {

                $listadoproductos = DB::table('producto')
                ->join('marca', 'producto.idmarca', '=', 'marca.idmarca')
                ->join('presentacion', 'producto.idpresentacion', '=', 'presentacion.idpresentacion')
                ->join('proveedor', 'producto.idproveedor', '=', 'proveedor.idproveedor')
                ->join('categoria', 'producto.idcategoria', '=', 'categoria.idcategoria')
                ->select('producto.codigo', 'producto.idmarca', 'marca.descripcion as nombremarca',
                        'producto.idpresentacion','presentacion.medida as nombrepresentacion', 'producto.idproveedor',
                        DB::raw( "CONCAT( proveedor.nombre,' ', proveedor.apellidopat,' ', proveedor.apellidomat ) AS nombrecompleto" ),
                        'producto.idcategoria', 'categoria.nombre as nombrecategoria', 'producto.descripcion',
                        'producto.preciocosto','producto.precioventa', 'producto.iva')
                        ->where( 'producto.idProveedor',$idproveedor )
                        ->where('producto.estado',1)
                        ->where('proveedor.estado',1)
                        ->orderBy('codigo','ASC')
                        ->get();
                        
                $listadoproductossortedmarca = DB::table('producto')
                        ->join('marca', 'producto.idmarca', '=', 'marca.idmarca')
                        ->join('presentacion', 'producto.idpresentacion', '=', 'presentacion.idpresentacion')
                        ->join('proveedor', 'producto.idproveedor', '=', 'proveedor.idproveedor')
                        ->join('categoria', 'producto.idcategoria', '=', 'categoria.idcategoria')
                        ->select('producto.codigo', 'producto.idmarca', 'marca.descripcion as nombremarca',
                                'producto.idpresentacion','presentacion.medida as nombrepresentacion', 'producto.idproveedor',
                                DB::raw( "CONCAT( proveedor.nombre,' ', proveedor.apellidopat,' ', proveedor.apellidomat ) AS nombrecompleto" ),
                                'producto.idcategoria', 'categoria.nombre as nombrecategoria', 'producto.descripcion',
                                'producto.preciocosto','producto.precioventa', 'producto.iva')
                                ->where( 'producto.idProveedor',$idproveedor )
                                ->where('producto.estado',1)
                                ->where('proveedor.estado',1)
                                ->orderBy('marca.descripcion','ASC')
                                ->get();
                
                    $listadoproductossortedpresentacion = DB::table('producto')
                        ->join('marca', 'producto.idmarca', '=', 'marca.idmarca')
                        ->join('presentacion', 'producto.idpresentacion', '=', 'presentacion.idpresentacion')
                        ->join('proveedor', 'producto.idproveedor', '=', 'proveedor.idproveedor')
                        ->join('categoria', 'producto.idcategoria', '=', 'categoria.idcategoria')
                        ->select('producto.codigo', 'producto.idmarca', 'marca.descripcion as nombremarca',
                                'producto.idpresentacion','presentacion.medida as nombrepresentacion', 'producto.idproveedor',
                                DB::raw( "CONCAT( proveedor.nombre,' ', proveedor.apellidopat,' ', proveedor.apellidomat ) AS nombrecompleto" ),
                                'producto.idcategoria', 'categoria.nombre as nombrecategoria', 'producto.descripcion',
                                'producto.preciocosto','producto.precioventa', 'producto.iva')
                                ->where( 'producto.idProveedor',$idproveedor )
                                ->where('producto.estado',1)
                                ->where('proveedor.estado',1)
                                ->orderBy('presentacion.medida','ASC')
                                ->get();
                
                    $listadoproductossortedcategoria = DB::table('producto')
                        ->join('marca', 'producto.idmarca', '=', 'marca.idmarca')
                        ->join('presentacion', 'producto.idpresentacion', '=', 'presentacion.idpresentacion')
                        ->join('proveedor', 'producto.idproveedor', '=', 'proveedor.idproveedor')
                        ->join('categoria', 'producto.idcategoria', '=', 'categoria.idcategoria')
                        ->select('producto.codigo', 'producto.idmarca', 'marca.descripcion as nombremarca',
                                'producto.idpresentacion','presentacion.medida as nombrepresentacion', 'producto.idproveedor',
                                DB::raw( "CONCAT( proveedor.nombre,' ', proveedor.apellidopat,' ', proveedor.apellidomat ) AS nombrecompleto" ),
                                'producto.idcategoria', 'categoria.nombre as nombrecategoria', 'producto.descripcion',
                                'producto.preciocosto','producto.precioventa', 'producto.iva')
                                ->where( 'producto.idProveedor',$idproveedor )
                                ->where('producto.estado',1)
                                ->where('proveedor.estado',1)
                                ->orderBy('categoria.nombre','ASC')
                                ->get();
                    
                    $listadoproductossorteddescripcion = DB::table('producto')
                        ->join('marca', 'producto.idmarca', '=', 'marca.idmarca')
                        ->join('presentacion', 'producto.idpresentacion', '=', 'presentacion.idpresentacion')
                        ->join('proveedor', 'producto.idproveedor', '=', 'proveedor.idproveedor')
                        ->join('categoria', 'producto.idcategoria', '=', 'categoria.idcategoria')
                        ->select('producto.codigo', 'producto.idmarca', 'marca.descripcion as nombremarca',
                                'producto.idpresentacion','presentacion.medida as nombrepresentacion', 'producto.idproveedor',
                                DB::raw( "CONCAT( proveedor.nombre,' ', proveedor.apellidopat,' ', proveedor.apellidomat ) AS nombrecompleto" ),
                                'producto.idcategoria', 'categoria.nombre as nombrecategoria', 'producto.descripcion',
                                'producto.preciocosto','producto.precioventa', 'producto.iva')
                                ->where( 'producto.idProveedor',$idproveedor )
                                ->where('producto.estado',1)
                                ->where('proveedor.estado',1)
                                ->orderBy('producto.descripcion','ASC')
                                ->get();

                    return view('vistasproductos/listadoproductos')->with('registroproductos', $listadoproductos)
                                                ->with( 'registroproductosmarca', $listadoproductossortedmarca )            
                                                ->with( 'registroproductospresentacion', $listadoproductossortedpresentacion )
                                                ->with( 'registroproductoscategoria', $listadoproductossortedcategoria )
                                                ->with( 'registroproductosdescripcion', $listadoproductossorteddescripcion );

            } else {

                Session::flash('warning', ' No hay productos registrados para el proveedor seleccionado ');
                return redirect()->back();

            }

        } else {

            Session::flash('danger', ' Seleccione a un proveedor ');
            return redirect()->back();

        }

    }

    public function buscarproductocategorias( $idcategoria ) {

        if( !empty( $idcategoria ) ) {

            $producto = Producto_Modelo::where('idCategoria', $idcategoria )->get();

            if( count( $producto ) >= 1 ) {

                $listadoproductos = DB::table('producto')
                ->join('marca', 'producto.idmarca', '=', 'marca.idmarca')
                ->join('presentacion', 'producto.idpresentacion', '=', 'presentacion.idpresentacion')
                ->join('proveedor', 'producto.idproveedor', '=', 'proveedor.idproveedor')
                ->join('categoria', 'producto.idcategoria', '=', 'categoria.idcategoria')
                ->select('producto.codigo', 'producto.idmarca', 'marca.descripcion as nombremarca',
                        'producto.idpresentacion','presentacion.medida as nombrepresentacion', 'producto.idproveedor',
                        DB::raw( "CONCAT( proveedor.nombre,' ', proveedor.apellidopat,' ', proveedor.apellidomat ) AS nombrecompleto" ),
                        'producto.idcategoria', 'categoria.nombre as nombrecategoria', 'producto.descripcion',
                        'producto.preciocosto','producto.precioventa', 'producto.iva')
                        ->where('producto.idCategoria', $idcategoria )
                        ->where('producto.estado',1)
                        ->where('proveedor.estado',1)
                        ->orderBy('codigo','ASC')
                        ->get();
                        
                $listadoproductossortedmarca = DB::table('producto')
                    ->join('marca', 'producto.idmarca', '=', 'marca.idmarca')
                    ->join('presentacion', 'producto.idpresentacion', '=', 'presentacion.idpresentacion')
                    ->join('proveedor', 'producto.idproveedor', '=', 'proveedor.idproveedor')
                    ->join('categoria', 'producto.idcategoria', '=', 'categoria.idcategoria')
                    ->select('producto.codigo', 'producto.idmarca', 'marca.descripcion as nombremarca',
                            'producto.idpresentacion','presentacion.medida as nombrepresentacion', 'producto.idproveedor',
                            DB::raw( "CONCAT( proveedor.nombre,' ', proveedor.apellidopat,' ', proveedor.apellidomat ) AS nombrecompleto" ),
                            'producto.idcategoria', 'categoria.nombre as nombrecategoria', 'producto.descripcion',
                            'producto.preciocosto','producto.precioventa', 'producto.iva')
                            ->where('producto.idCategoria', $idcategoria )
                            ->where('producto.estado',1)
                            ->where('proveedor.estado',1)
                            ->orderBy('marca.descripcion','ASC')
                            ->get();
            
                $listadoproductossortedpresentacion = DB::table('producto')
                    ->join('marca', 'producto.idmarca', '=', 'marca.idmarca')
                    ->join('presentacion', 'producto.idpresentacion', '=', 'presentacion.idpresentacion')
                    ->join('proveedor', 'producto.idproveedor', '=', 'proveedor.idproveedor')
                    ->join('categoria', 'producto.idcategoria', '=', 'categoria.idcategoria')
                    ->select('producto.codigo', 'producto.idmarca', 'marca.descripcion as nombremarca',
                            'producto.idpresentacion','presentacion.medida as nombrepresentacion', 'producto.idproveedor',
                            DB::raw( "CONCAT( proveedor.nombre,' ', proveedor.apellidopat,' ', proveedor.apellidomat ) AS nombrecompleto" ),
                            'producto.idcategoria', 'categoria.nombre as nombrecategoria', 'producto.descripcion',
                            'producto.preciocosto','producto.precioventa', 'producto.iva')
                            ->where('producto.idCategoria', $idcategoria )
                            ->where('producto.estado',1)
                            ->where('proveedor.estado',1)
                            ->orderBy('presentacion.medida','ASC')
                            ->get();
            
                $listadoproductossortedcategoria = DB::table('producto')
                    ->join('marca', 'producto.idmarca', '=', 'marca.idmarca')
                    ->join('presentacion', 'producto.idpresentacion', '=', 'presentacion.idpresentacion')
                    ->join('proveedor', 'producto.idproveedor', '=', 'proveedor.idproveedor')
                    ->join('categoria', 'producto.idcategoria', '=', 'categoria.idcategoria')
                    ->select('producto.codigo', 'producto.idmarca', 'marca.descripcion as nombremarca',
                            'producto.idpresentacion','presentacion.medida as nombrepresentacion', 'producto.idproveedor',
                            DB::raw( "CONCAT( proveedor.nombre,' ', proveedor.apellidopat,' ', proveedor.apellidomat ) AS nombrecompleto" ),
                            'producto.idcategoria', 'categoria.nombre as nombrecategoria', 'producto.descripcion',
                            'producto.preciocosto','producto.precioventa', 'producto.iva')
                            ->where('producto.idCategoria', $idcategoria )
                            ->where('producto.estado',1)
                            ->where('proveedor.estado',1)
                            ->orderBy('categoria.nombre','ASC')
                            ->get();
                
                $listadoproductossorteddescripcion = DB::table('producto')
                    ->join('marca', 'producto.idmarca', '=', 'marca.idmarca')
                    ->join('presentacion', 'producto.idpresentacion', '=', 'presentacion.idpresentacion')
                    ->join('proveedor', 'producto.idproveedor', '=', 'proveedor.idproveedor')
                    ->join('categoria', 'producto.idcategoria', '=', 'categoria.idcategoria')
                    ->select('producto.codigo', 'producto.idmarca', 'marca.descripcion as nombremarca',
                            'producto.idpresentacion','presentacion.medida as nombrepresentacion', 'producto.idproveedor',
                            DB::raw( "CONCAT( proveedor.nombre,' ', proveedor.apellidopat,' ', proveedor.apellidomat ) AS nombrecompleto" ),
                            'producto.idcategoria', 'categoria.nombre as nombrecategoria', 'producto.descripcion',
                            'producto.preciocosto','producto.precioventa', 'producto.iva')
                            ->where('producto.idCategoria', $idcategoria )
                            ->where('producto.estado',1)
                            ->where('proveedor.estado',1)
                            ->orderBy('producto.descripcion','ASC')
                            ->get();

                return view('vistasproductos/listadoproductos')->with('registroproductos', $listadoproductos)
                                            ->with( 'registroproductosmarca', $listadoproductossortedmarca )            
                                            ->with( 'registroproductospresentacion', $listadoproductossortedpresentacion )
                                            ->with( 'registroproductoscategoria', $listadoproductossortedcategoria )
                                            ->with( 'registroproductosdescripcion', $listadoproductossorteddescripcion );

            } else {

                Session::flash('warning', ' No hay productos registrados para la categoría seleccionada ');
                return redirect()->back();

            }

        } else {

            Session::flash('danger', ' Seleccione a una categoría ');
            return redirect()->back();

        }

    }


    public function registrarproductosrapido() {

        $marcas = Marca_Modelo::where('estado', 1)->pluck('descripcion','idmarca')->toArray();
        $presentaciones = Presentacion_Modelo::where('estado', 1)->pluck('medida','idpresentacion')->toArray();
        $proveedores = Proveedor_Modelo::select("idproveedor", DB::raw("CONCAT(proveedor.nombre,' ',
                        proveedor.apellidopat,' ',proveedor.apellidomat) as nombrecompleto"))
                        ->where('estado','=','1')
                        ->pluck('nombrecompleto', 'idproveedor')->toArray();

        $categorias = Categoria_Modelo::where('estado', 1)->pluck('nombre','idcategoria')->toArray();


        return view('vistasproductos/registrorapido/registrarproductorapido')->with('caso', 'onload')
            ->with('marcas',$marcas)
            ->with('presentaciones', $presentaciones)
            ->with('proveedores',$proveedores)
            ->with('categorias', $categorias);

    }

    public function registrarproductosnocodigorapido() {

        $marcas = Marca_Modelo::where('estado', 1)->pluck('descripcion','idmarca')->toArray();
        $presentaciones = Presentacion_Modelo::where('estado', 1)->pluck('medida','idpresentacion')->toArray();
        $proveedores = Proveedor_Modelo::select("idproveedor", DB::raw("CONCAT(proveedor.nombre,' ',
                        proveedor.apellidopat,' ',proveedor.apellidomat) as nombrecompleto"))
                        ->where('estado','=','1')
                        ->pluck('nombrecompleto', 'idproveedor')->toArray();

        $categorias = Categoria_Modelo::where('estado', 1)->pluck('nombre','idcategoria')->toArray();


        return view('vistasproductos/registrorapido/registrarproductonocodigorapido')->with('caso', 'onload')
            ->with('marcas',$marcas)
            ->with('presentaciones', $presentaciones)
            ->with('proveedores',$proveedores)
            ->with('categorias', $categorias);


    }

        //Funciones para registrar

    public function registrarproductos() {

        $marcas = Marca_Modelo::where('estado', 1)->pluck('descripcion','idmarca')->toArray();
        $presentaciones = Presentacion_Modelo::where('estado', 1)->pluck('medida','idpresentacion')->toArray();
        $proveedores = Proveedor_Modelo::select("idproveedor", DB::raw("CONCAT(proveedor.nombre,' ',
                        proveedor.apellidopat,' ',proveedor.apellidomat) as nombrecompleto"))
                        ->where('estado','=','1')
                        ->pluck('nombrecompleto', 'idproveedor')->toArray();

        $categorias = Categoria_Modelo::where('estado', 1)->pluck('nombre','idcategoria')->toArray();


        return view('vistasproductos/registrarproducto') ->with('marcas',$marcas)
         ->with('presentaciones', $presentaciones) ->with('proveedores',$proveedores)
         -> with('categorias', $categorias);

    }

    public function registrarproductosnocodigo() {

        $marcas = Marca_Modelo::where('estado', 1)->pluck('descripcion','idmarca')->toArray();
        $presentaciones = Presentacion_Modelo::where('estado', 1)->pluck('medida','idpresentacion')->toArray();
        $proveedores = Proveedor_Modelo::select("idproveedor", DB::raw("CONCAT(proveedor.nombre,' ',
                        proveedor.apellidopat,' ',proveedor.apellidomat) as nombrecompleto"))
                        ->where('estado','=','1')
                        ->pluck('nombrecompleto', 'idproveedor')->toArray();

        $categorias = Categoria_Modelo::where('estado', 1)->pluck('nombre','idcategoria')->toArray();        


        return view('vistasproductos/registrarproductonocodigo') ->with('marcas',$marcas)
         ->with('presentaciones', $presentaciones) ->with('proveedores',$proveedores)
         -> with('categorias', $categorias);
    }

    public function insertarproducto( Request $request ) {

        if( $request->hasFile('avatar') ) {


            $file = $request->file( 'avatar' );
            $descripcionproducto = str_replace( "/", " ", $request -> input('descripcion') );
            //$name = time().$file->getClientOriginalName();

            $name = 'Productos/' . $descripcionproducto . "." . $request->file('avatar')->extension();
            $file->move( public_path().'/images/Productos/', $descripcionproducto . "." . $request->file('avatar')->extension() );


            /*  //SE GUARDAN EN LA CARPETA STORAGE/APP/PUBLIC/PRODUCTOS
            //$name = $request->file('avatar')->store('public/Productos');

            $name = $request->file('avatar')->storeAs(
                'public/Productos',  $request -> input('descripcion') . "." . $request->file('avatar')->extension()
            );

            */

        } else {

            $name = 'Productos/ProductoDefault.png';
            //$name = "";

            //Session::flash('danger', ' No se ha podido cargar la imagen del producto ');
            //return redirect()->to('home')->with('status','Datos cargados correctamente');
            //return redirect()->back();
        }


        if( $request->botonformproducto == 'Registrar' ) {

            $validator = Validator::make($request->all(), Producto_Modelo::rules(), Producto_Modelo::messages());

            if( $validator ->fails() ) {
                return redirect()->back()->withInput()->withErrors($validator->errors());
            }

            $codigo         = $request ->input('codigo');
            $imagen         = $name;
            $descripcion    = $request -> input('descripcion');
            $marca          = $request -> input('idmarca');
            $idproveedor    = $request -> input('idproveedor');

            /*
            $consproveedor = Marca_Modelo::select('idProveedor')->where('idmarca', '=', $marca )->get();

            foreach( $consproveedor as $k ) {
                $idproveedor = $k->{'idProveedor'};
            }
            */

            $presentacion   = $request -> input('idpresentacion');
            //$idproveedor =    $request -> input('idproveedor');
            $categoria      = $request -> input('idcategoria');

            //$preciocosto    = $request -> input('preciocosto');
            //$precioventa    = $request -> input('precioventa');

            
            $preciocosto    = 0;
            $precioventa    = 0;

            $stockminimo    = $request -> input('stockminimo');
            $impuesto       = $request -> input('impuesto');

            if( $impuesto == 1 ) {

                $preciosiniva = bcadd( $precioventa, '0', 2 );
                $precioventa = ( $preciosiniva * 0.16 ) + $preciosiniva;

            }

            $estado = 1;

            Producto_Modelo::create( ['codigo' => $codigo, 'imagen' => $imagen, 'descripcion' => $descripcion, 'idmarca' => $marca,
            'idpresentacion' => $presentacion, 'idproveedor' => $idproveedor, 'idcategoria' => $categoria,
            'preciocosto' => $preciocosto, 'precioventa' => $precioventa, 'iva' => $impuesto,
            'estado' => $estado, 'stockminimo' => $stockminimo ] );

            Session::flash('success', ' Se ha registrado exitosamente el producto ');
            //return redirect()->to('home')->with('status','Datos cargados correctamente');
            return redirect()->back();

        } else if( $request->botonformproducto == 'RegistrarRapido' ) {

            $validator = Validator::make($request->all(), Producto_Modelo::rules(), Producto_Modelo::messages());

            if( $validator ->fails() ) {
                return redirect()->back()->withInput()->withErrors($validator->errors());
            }

            $codigo         = $request ->input('codigo');
            $imagen         =  $name;
            $descripcion    =  $request -> input('descripcion');
            $marca          =  $request -> input('idmarca');
            $idproveedor    =  $request -> input('idproveedor');

            /*
            $consproveedor = Marca_Modelo::select('idProveedor')->where('idmarca', '=', $marca )->get();

            foreach( $consproveedor as $k ) {
                $idproveedor = $k->{'idProveedor'};
            }
            */

            $presentacion   = $request -> input('idpresentacion');
            //$idproveedor =    $request -> input('idproveedor');
            $categoria      = $request -> input('idcategoria');

            //$preciocosto    = $request -> input('preciocosto');
            //$precioventa    = $request -> input('precioventa');

            
            $preciocosto    = 0;
            $precioventa    = 0;

            
            $stockminimo    = $request-> input('stockminimo');
            $impuesto       = $request -> input('impuesto');

            if( $impuesto == 1 ) {

                $preciosiniva = bcadd( $precioventa, '0', 2 );
                $precioventa = ( $preciosiniva * 0.16 ) + $preciosiniva;

            }

            $estado = 1;

            Producto_Modelo::create( ['codigo' => $codigo, 'imagen' => $imagen, 'descripcion' => $descripcion, 'idmarca' => $marca,
            'idpresentacion' => $presentacion, 'idproveedor' => $idproveedor, 'idcategoria' => $categoria,
            'preciocosto' => $preciocosto, 'precioventa' => $precioventa, 'iva' => $impuesto,
            'estado' => $estado, 'stockminimo' => $stockminimo ] );

            //Session::flash('success', ' Se ha registrado exitosamente el producto ');

            //return redirect()->to('home')->with('status','Datos cargados correctamente');

            //return redirect()->back();
            //return view('vistasproductos/registrorapido/registrarproductorapido')->with('caso', 'close');

            $marcas = Marca_Modelo::pluck('descripcion','idmarca')->toArray();
            $presentaciones = Presentacion_Modelo::pluck('medida','idpresentacion')->toArray();
            $proveedores = Proveedor_Modelo::select("idproveedor", DB::raw("CONCAT(proveedor.nombre,' ',
                            proveedor.apellidopat,' ',proveedor.apellidomat) as nombrecompleto"))
                            ->where('estado','=','1')
                            ->pluck('nombrecompleto', 'idproveedor')->toArray();

            $categorias = Categoria_Modelo::pluck('nombre','idcategoria')->toArray();


            return view('vistasproductos/registrorapido/registrarproductorapido')->with('caso', 'close')
                ->with('marcas',$marcas)
                ->with('presentaciones', $presentaciones)
                ->with('proveedores',$proveedores)
                ->with('categorias', $categorias);

        } else if ( $request ->botonformproducto == 'Cancelar' ) {
            return redirect()->to('home');
        } else if ( $request ->botonformproductonocodigo == 'Registrar' ) {

            $imagen         = $name;
            $descripcion    = $request -> input('descripcion');
            $marca          = $request -> input('idmarca');
            $idproveedor    = $request -> input('idproveedor');

            /*
            $consproveedor = Marca_Modelo::select('idProveedor')->where('idmarca', '=', $marca )->get();

            foreach( $consproveedor as $k ) {
                $idproveedor = $k->{'idProveedor'};
            }
            */

            $presentacion   = $request -> input('idpresentacion');
            //$idproveedor =    $request -> input('idproveedor');
            $categoria      = $request -> input('idcategoria');

            //$preciocosto    = $request -> input('preciocosto');
            //$precioventa    = $request -> input('precioventa');

            
            $preciocosto    = 0;
            $precioventa    = 0;

            
            $stockminimo    = $request->input('stockminimo');
            $impuesto       = $request -> input('impuesto');

            if( $impuesto == 1 ) {

                $preciosiniva = bcadd( $precioventa, '0', 2 );
                $precioventa = ( $preciosiniva * 0.16 ) + $preciosiniva;

            }

            $estado = 1;

            $codigo = $this->generarcodigobarras();

            Producto_Modelo::create( ['codigo' => $codigo, 'imagen' => $imagen, 'descripcion' => $descripcion, 'idmarca' => $marca,
            'idpresentacion' => $presentacion, 'idproveedor' => $idproveedor, 'idcategoria' => $categoria,
            'preciocosto' => $preciocosto, 'precioventa' => $precioventa, 'iva' => $impuesto,
            'estado' => $estado, 'stockminimo' => $stockminimo ] );

            Session::flash('success', ' Se ha registrado exitosamente el producto ');

            return redirect()->back();

        } else if ( $request ->botonformproductonocodigo == 'RegistrarRapido' ) {

            $imagen         = $name;
            $descripcion    = $request -> input('descripcion');
            $marca          = $request -> input('idmarca');
            $idproveedor    = $request -> input('idproveedor');

            /*
            $consproveedor = Marca_Modelo::select('idProveedor')->where('idmarca', '=', $marca )->get();

            foreach( $consproveedor as $k ) {
                $idproveedor = $k->{'idProveedor'};
            }
            */

            $presentacion   = $request -> input('idpresentacion');
            //$idproveedor =    $request -> input('idproveedor');
            $categoria      = $request -> input('idcategoria');

            //$preciocosto    = $request -> input('preciocosto');
            //$precioventa    = $request -> input('precioventa');

            
            $preciocosto    = 0;
            $precioventa    = 0;

            
            $stockminimo    = $request -> input('stockminimo');
            $impuesto       = $request -> input('impuesto');

            if( $impuesto == 1 ) {

                $preciosiniva = bcadd( $precioventa, '0', 2 );
                $precioventa = ( $preciosiniva * 0.16 ) + $preciosiniva;

            }

            $estado = 1;

            $codigo = $this->generarcodigobarras();

            Producto_Modelo::create( ['codigo' => $codigo, 'imagen' => $imagen, 'descripcion' => $descripcion, 'idmarca' => $marca,
            'idpresentacion' => $presentacion, 'idproveedor' => $idproveedor, 'idcategoria' => $categoria,
            'preciocosto' => $preciocosto, 'precioventa' => $precioventa, 'iva' => $impuesto,
            'estado' => $estado, 'stockminimo' => $stockminimo ] );


            //Session::flash('success', ' Se ha registrado exitosamente el producto ');

            //return redirect()->back();
            //return view('vistasproductos/registrorapido/registrarproductonocodigorapido')->with('caso', 'close');

            $marcas = Marca_Modelo::pluck('descripcion','idmarca')->toArray();
            $presentaciones = Presentacion_Modelo::pluck('medida','idpresentacion')->toArray();
            $proveedores = Proveedor_Modelo::select("idproveedor", DB::raw("CONCAT(proveedor.nombre,' ',
                            proveedor.apellidopat,' ',proveedor.apellidomat) as nombrecompleto"))
                            ->where('estado','=','1')
                            ->pluck('nombrecompleto', 'idproveedor')->toArray();

            $categorias = Categoria_Modelo::pluck('nombre','idcategoria')->toArray();


            return view('vistasproductos/registrorapido/registrarproductonocodigorapido')->with('caso', 'close')
                ->with('marcas',$marcas)
                ->with('presentaciones', $presentaciones)
                ->with('proveedores',$proveedores)
                ->with('categorias', $categorias);

        } else if ( $request ->botonformproductonocodigo == 'Cancelar' ) {

            return redirect()->to('home');

        }

    }

    public function generarcodigobarras(  ) {
        
        $codigobarras = 0;

        //Realizar la consulta para verificiar el número total de registros que coincidan con el código buscado.
        $consultacodigo = DB::table('producto')->select( DB::raw( "COUNT( codigo ) AS total" ) )
                                ->where( 'codigo', '=', '100000000' )
                                ->get();


        $cod = $consultacodigo[0]->{'total'};

        //Si el número total de la consulta es igual a 0, el código comenzará a partir de 100000000.
        if( $cod === 0 ) {

            $codigobarras = 100000000;

        } else {

            $filtro = "10000";
            $consulta = Producto_Modelo::select( 'codigo' )
                           ->where( 'codigo', 'LIKE', $filtro.'%' )
                          ->orderBy( 'codigo', 'desc' )
                          ->limit(1)
                          ->get();

            
            $codigo = $consulta[0]->{'codigo'};

            $codigobarras = $codigo + 1;

        }

        return $codigobarras;

    }

        //Funciones para editar información

    public function modificarproducto( $codigo ) {

        $producto = Producto_Modelo::select('*')->where('codigo', $codigo)->get();

        if( count( $producto ) == 1 ) {  

            $producto = Producto_Modelo::select('*')->where('codigo', $codigo)->take(1)->first();

            $marcas = Marca_Modelo::select('*')->get();

            $presentaciones = Presentacion_Modelo::select('*')->get();

            $proveedores = Proveedor_Modelo::select('idproveedor', DB::raw("CONCAT(proveedor.nombre,' ',
                                proveedor.apellidopat,' ',proveedor.apellidomat) as nombrecompleto"))
                                ->where('estado',1)
                                ->get();
            
            /*
            $proveedores = Proveedor_Modelo::select("idproveedor", DB::raw("CONCAT(proveedor.nombre,' ',
                            proveedor.apellidopat,' ',proveedor.apellidomat) as nombrecompleto"))
                            ->where('estado','=','1')
                            ->pluck('nombrecompleto', 'idproveedor')->toArray();
            */
                            
            $categorias = Categoria_Modelo::select('*')->get();

            return view('vistasproductos/modificarproducto')->with('producto',$producto)
            ->with('proveedores',$proveedores)->with('marcas',$marcas)
            ->with('presentaciones',$presentaciones)->with('categorias',$categorias);

        } else {

            Session::flash('warning', ' No existe producto registrado con ese código ');
            return redirect()->back();

        }

    }

    public function actualizarinfoproducto( Request $request , $codigo ) {

        if( $request->botonformmodificar == 'Modificar' ) {

            if( $request->hasFile('avatar') ) {


                $file = $request->file( 'avatar' );
                $descripcionproducto = str_replace( "/", " ", $request -> input('descripcion') );
                //$name = time().$file->getClientOriginalName();

                $name = 'Productos/' . $descripcionproducto . "." . $request->file('avatar')->extension();
                $file->move( public_path().'/images/Productos/', $descripcionproducto . "." . $request->file('avatar')->extension() );


                /*  //SE GUARDAN EN LA CARPETA STORAGE/APP/PUBLIC/PRODUCTOS
                //$name = $request->file('avatar')->store('public/Productos');

                $name = $request->file('avatar')->storeAs(
                    'public/Productos',  $request -> input('descripcion') . "." . $request->file('avatar')->extension()
                );

                */

            } else {

                $consulta = Producto_Modelo::select('Imagen')->where('Codigo', '=', $codigo )->get();

                foreach( $consulta as $k ) {
                    $name = $k->{'Imagen'};
                }

            }

            $idmarca = $request->idmarca;

            //$consproveedor = Marca_Modelo::select('idProveedor')->where('idmarca', '=', $idmarca )->get();

            //foreach( $consproveedor as $k ) {
                //$idproveedor = $k->{'idProveedor'};
            //}  // update the record in the DB.

            /*
            DB::table('producto')
            ->where('codigo',$codigo)  // find your user by their email
            ->limit(1)  // optional - to ensure only one record is updated.
            ->update( array( 'idMarca' => $idmarca,
                            'imagen'=> $name,
                            'idPresentacion' => $request->idpresentacion,
                            'idProveedor' => $request->idproveedor,
                            'idCategoria' => $request->idcategoria,
                            'PrecioCosto' => $request->preciocosto,
                            'PrecioVenta' => $request->precioventa,
                            'Stockminimo' => $request->stockminimo,
                            'IVA' => $request->impuesto) );  // update the record in the DB.
            */


            DB::table('producto')
            ->where('codigo',$codigo)
            ->limit(1)
            ->update( array( 'idMarca' => $idmarca,
                            'imagen'=> $name,
                            'idPresentacion' => $request->idpresentacion,
                            'idProveedor' => $request->idproveedor,
                            'idCategoria' => $request->idcategoria,
                            'PrecioVenta' => $request->precioventa,
                            'Stockminimo' => $request->stockminimo,
                            'IVA' => $request->impuesto) );

            return redirect()->to('listadoproductos');

        }  else if( $request->botonformmodificar == 'Cancelar' ) {

                return redirect()->to('home');

        }

    }

        //Funciones para descontinuar productos

    public function descontinuarproducto( $codigo ) {

        $producto = Producto_Modelo::select('*')->where('codigo', $codigo)->get();

        if( count( $producto ) == 1 ) {  


            $producto = Producto_Modelo::select('*')->where('codigo', $codigo)->take(1)->first();

            $marcas = Marca_Modelo::select('*')->get();

            $presentaciones = Presentacion_Modelo::select('*')->get();

            $proveedores = Proveedor_Modelo::select('idproveedor', DB::raw("CONCAT(proveedor.nombre,' ',
                                proveedor.apellidopat,' ',proveedor.apellidomat) as nombrecompleto"))
                                ->where('estado',1)
                                ->get();

            $categorias = Categoria_Modelo::select('*')->get();

            return view('vistasproductos/descontinuarproductos') -> with('producto',$producto)
            -> with('proveedores',$proveedores) -> with('marcas',$marcas)
            -> with('presentaciones',$presentaciones) -> with('categorias',$categorias);

        } else {

            Session::flash('warning', ' No existe producto registrado con ese código ');
            return redirect()->back();

        }

    }

    public function cambiarestadoproducto( Request $request, $codigo ) {

        if( $request->botonformdescontinuar == 'Descontinuar' ) {

                DB::table('producto')
                ->where('Codigo',$codigo)  // find your user by their email
                ->update( array( 'Estado' => 0 ) );  // update the record in the DB.

                return redirect()->to('listadoproductos');

        } else if ( $request->botonformdescontinuar == 'Cancelar' ) {

            return redirect()->to('home');

        }

    }

    public function descontinuarcambiarestadoproducto( $codigo ) {

        DB::table('producto')
                ->where( 'Codigo', $codigo )
                ->update( array( 'Estado' => 0 ) );

        return redirect()->to('listadoproductos');

    }


    public function añadirmarca( Request $request ) {

        if( $request->botonformmarca == "Registrar" ) {

            if( $request->hasFile('avatar') ) {

                $file = $request->file( 'avatar' );
                $nombreimagen = str_replace(' ', '', strtoupper( $request->input('descripcion') ) );
                //$name = time().$file->getClientOriginalName();

                $name = 'CatalogoProductos/Marcas/' . $nombreimagen . "Thumb." . $request->file('avatar')->extension();
                $file->move( public_path().'/images/CatalogoProductos/Marcas/', $nombreimagen . "Thumb." . $request->file('avatar')->extension() );

            } else {

                $name = 'CatalogoProductos/Marcas/DefaultThumb.jpg';

            }

            $idproveedor = $request->input('idproveedor');
            $descripcion = strtoupper( $request->input('descripcion') );

            $estado = 1;

            //COMPROBAR QUE NO HAYA UNA CATEGORÍA REGISTRADA CON ESE NOMBRE.

            //CONSULTA PARA OBTENER EL TOTAL DE REGISTROS CON ESE NOMBRE.
            $consultamarcas = DB::table('marca')->select( DB::raw( "COUNT( marca.descripcion ) AS total" ) )
                //->where( DB::raw('upper( marca.descripcion )' ), '=', $descripcion )
                ->whereRaw("upper(descripcion) like  upper('$descripcion') collate utf8mb4_general_ci ")
                ->get();

            //SE OBTIENE LA COLUMNA TOTAL DE LA CONSULTA.
            $totalmarcas = $consultamarcas[0]->{'total'};

            //Se verifica que el valor de total sea 0 para poder registrar a la nueva categoría.
            if( $totalmarcas == 0 ) {

                Marca_Modelo::create( [ 'descripcion' => $descripcion,
                                        'estado' => $estado, 'urlimagen' => $name ] );

                Session::flash('success', ' Se ha registrado exitosamente la marca ');

            } else {


                Session::flash('danger', ' Ya hay una marca registrada con ese nombre ');

            }

            return redirect()->back();

        } elseif( $request->botonformmarca == "Cancelar" ) {

            return redirect()->to('home');

        }

    }

    public function añadirpresentacion( Request $request ) {

        if( $request->botonformpresentacion == "Registrar" ) {

            if( $request->hasFile('avatar') ) {

                $file = $request->file( 'avatar' );
                $nombreimagen = str_replace(' ', '', strtoupper( $request->input('medida') ) );
                //$name = time().$file->getClientOriginalName();

                $name = 'CatalogoProductos/Presentaciones/' . $nombreimagen . "Thumb." . $request->file('avatar')->extension();
                $file->move( public_path().'/images/CatalogoProductos/Presentaciones/', $nombreimagen . "Thumb." . $request->file('avatar')->extension() );

            } else {

                $name = 'CatalogoProductos/Presentaciones/DefaultThumb.jpg';

            }

            $medida = strtoupper( $request->input('medida') );

            $estado = 1;

            //COMPROBAR QUE NO HAYA UNA CATEGORÍA REGISTRADA CON ESE NOMBRE.

            //CONSULTA PARA OBTENER EL TOTAL DE REGISTROS CON ESE NOMBRE.
            $consultapresentacion = DB::table('presentacion')->select( DB::raw( "COUNT( presentacion.medida ) AS total" ) )
                //->where( DB::raw('upper( presentacion.medida )' ), '=', $medida )
                ->whereRaw("upper(medida) like  upper('$medida') collate utf8mb4_general_ci ")
                ->get();

            //SE OBTIENE LA COLUMNA TOTAL DE LA CONSULTA.
            $totalpresentaciones = $consultapresentacion[0]->{'total'};

            //Se verifica que el valor de total sea 0 para poder registrar a la nueva categoría.
            if( $totalpresentaciones == 0 ) {

                Presentacion_Modelo::create( [ 'medida' => $medida, 'estado' => $estado, 'urlimagen' =>$name  ] );

                Session::flash('success', ' Se ha registrado exitosamente la presentación ');

            } else {


                Session::flash('danger', ' Ya hay una presentación registrada con esa medida ');

            }

            return redirect()->back();

        } elseif( $request->botonformpresentacion == "Cancelar" ) {

            return redirect()->to('home');

        }

    }

    public function añadircategoria( Request $request ) {

        if( $request->botonformcategoria == "Registrar" ) {

            if( $request->hasFile('avatar') ) {

                $file = $request->file( 'avatar' );
                $nombreimagen = str_replace(' ', '', strtoupper( $request->input('nombre') ) );
                //$name = time().$file->getClientOriginalName();

                $name = 'CatalogoProductos/Categorias/' . $nombreimagen. "Thumb." . $request->file('avatar')->extension();
                $file->move( public_path().'/images/CatalogoProductos/Categorias/', $nombreimagen . "Thumb." . $request->file('avatar')->extension() );

            } else {

                $name = 'CatalogoProductos/Categorias/DefaultThumb.jpg';

            }

            $nombre = strtoupper( $request->input('nombre') );
            $descripcion = strtoupper( $request->input('descripcion') );

            $estado = 1;

            //COMPROBAR QUE NO HAYA UNA CATEGORÍA REGISTRADA CON ESE NOMBRE.

            //CONSULTA PARA OBTENER EL TOTAL DE REGISTROS CON ESE NOMBRE.
            $consultacategoria = DB::table('categoria')->select( DB::raw( "COUNT( categoria.nombre ) AS total" ) )
                //->where( DB::raw('upper( categoria.nombre )' ), '=', $nombre )
                ->whereRaw("upper(nombre) like  upper('$nombre') collate utf8mb4_general_ci ")
                ->get();

            //SE OBTIENE LA COLUMNA TOTAL DE LA CONSULTA.
            $totalcategorias = $consultacategoria[0]->{'total'};

            //Se verifica que el valor de total sea 0 para poder registrar a la nueva categoría.
            if( $totalcategorias == 0 ) {

                Categoria_Modelo::create( [ 'nombre' => $nombre, 'descripcion' => $descripcion,
                                            'estado' => $estado, 'urlimagen' => $name ] );

                Session::flash('success', ' Se ha registrado exitosamente la categoría ');

            } else {


                Session::flash('danger', ' Ya hay una categoría registrada con ese nombre ');

            }

            return redirect()->back();

        } elseif( $request->botonformcategoria == "Cancelar" ) {

            return redirect()->to('home');

        }

    }

        //Otra ruta de ver Productos
    public function selecciondeproducto( Request $request ) {

        $valorfiltro = $request->get('descripcion');

        if( empty( $valorfiltro ) ) {

            //SE ENTRA A LA VISTA POR PRIMERA VEZ
            $productos = \DB::table('producto')
            ->whereNotIn('producto.codigo', function( $query) {
                $query->select('oferta.codigo')->from('oferta')
                ->join('promociones', 'oferta.idpromociones', '=', 'promociones.idpromociones')
                ->where('promociones.estado', '=', '1');
            } )
            ->whereNotIn('producto.codigo', function( $query) {
                $query->select('descuento.codigo')->from('descuento')
                ->join('promociones', 'descuento.idpromociones', '=', 'promociones.idpromociones')
                ->where('promociones.estado', '=', '1');
            } )
            ->whereNotIn('producto.codigo', function( $query) {
                $query->select('paquetes.codigo1')->from('paquetes')
                ->join('promociones', 'paquetes.idpromociones', '=', 'promociones.idpromociones')
                ->where('promociones.estado', '=', '1');
            } )
            ->whereNotIn('producto.codigo', function( $query) {
                $query->select('paquetes.codigo2')->from('paquetes')
                ->join('promociones', 'paquetes.idpromociones', '=', 'promociones.idpromociones')
                ->where('promociones.estado', '=', '1');
            } )
            ->join('marca', 'producto.idmarca', '=', 'marca.idmarca')
            ->join('presentacion', 'producto.idpresentacion', '=', 'presentacion.idpresentacion')
            ->join('proveedor', 'producto.idproveedor', '=', 'proveedor.idproveedor')
            ->join('categoria', 'producto.idcategoria', '=', 'categoria.idcategoria')
            ->select('producto.codigo', 'producto.idmarca', 'marca.descripcion as nombremarca',
                    'producto.idpresentacion','presentacion.medida as nombrepresentacion', 'producto.idproveedor',
                    DB::raw( "CONCAT( proveedor.nombre,' ', proveedor.apellidopat,' ', proveedor.apellidomat ) AS nombrecompleto" ),
                    'producto.idcategoria', 'categoria.nombre as nombrecategoria', 'producto.descripcion',
                    'producto.preciocosto','producto.precioventa', 'producto.iva')
                        ->where('producto.estado',1)
                        ->orderBy('producto.codigo', 'asc')
            ->paginate(10);

        } else {
            //SE INTENTA BUSCAR UN PRODUCTO O PRODUCTOS MEDIANTE EL FILTRADO

            if(  is_numeric( $valorfiltro ) ) {
                //EL VALOR QUE SE INTRODUJO ES UN NUMERO

                $productos = \DB::table('producto')
                ->whereNotIn('producto.codigo', function( $query) {
                    $query->select('oferta.codigo')->from('oferta')
                    ->join('promociones', 'oferta.idpromociones', '=', 'promociones.idpromociones')
                    ->where('promociones.estado', '=', '1');
                } )
                ->whereNotIn('producto.codigo', function( $query) {
                    $query->select('descuento.codigo')->from('descuento')
                    ->join('promociones', 'descuento.idpromociones', '=', 'promociones.idpromociones')
                    ->where('promociones.estado', '=', '1');
                } )
                ->whereNotIn('producto.codigo', function( $query) {
                    $query->select('paquetes.codigo1')->from('paquetes')
                    ->join('promociones', 'paquetes.idpromociones', '=', 'promociones.idpromociones')
                    ->where('promociones.estado', '=', '1');
                } )
                ->whereNotIn('producto.codigo', function( $query) {
                    $query->select('paquetes.codigo2')->from('paquetes')
                    ->join('promociones', 'paquetes.idpromociones', '=', 'promociones.idpromociones')
                    ->where('promociones.estado', '=', '1');
                } )
                ->join('marca', 'producto.idmarca', '=', 'marca.idmarca')
                ->join('presentacion', 'producto.idpresentacion', '=', 'presentacion.idpresentacion')
                ->join('proveedor', 'producto.idproveedor', '=', 'proveedor.idproveedor')
                ->join('categoria', 'producto.idcategoria', '=', 'categoria.idcategoria')
                ->select('producto.codigo', 'producto.idmarca', 'marca.descripcion as nombremarca',
                        'producto.idpresentacion','presentacion.medida as nombrepresentacion', 'producto.idproveedor',
                        DB::raw( "CONCAT( proveedor.nombre,' ', proveedor.apellidopat,' ', proveedor.apellidomat ) AS nombrecompleto" ),
                        'producto.idcategoria', 'categoria.nombre as nombrecategoria', 'producto.descripcion',
                        'producto.preciocosto','producto.precioventa', 'producto.iva')
                            ->where('producto.codigo', '=', $valorfiltro )
                            ->where('producto.estado',1)
                            ->orderBy('producto.codigo', 'asc')
                ->paginate(10);

            } else {
                //EL VALOR QUE SE INTRODUJO ES UNA STRING

                $productos = \DB::table('producto')
                ->whereNotIn('producto.codigo', function( $query) {
                    $query->select('oferta.codigo')->from('oferta')
                    ->join('promociones', 'oferta.idpromociones', '=', 'promociones.idpromociones')
                    ->where('promociones.estado', '=', '1');
                } )
                ->whereNotIn('producto.codigo', function( $query) {
                    $query->select('descuento.codigo')->from('descuento')
                    ->join('promociones', 'descuento.idpromociones', '=', 'promociones.idpromociones')
                    ->where('promociones.estado', '=', '1');
                } )
                ->whereNotIn('producto.codigo', function( $query) {
                    $query->select('paquetes.codigo1')->from('paquetes')
                    ->join('promociones', 'paquetes.idpromociones', '=', 'promociones.idpromociones')
                    ->where('promociones.estado', '=', '1');
                } )
                ->whereNotIn('producto.codigo', function( $query) {
                    $query->select('paquetes.codigo2')->from('paquetes')
                    ->join('promociones', 'paquetes.idpromociones', '=', 'promociones.idpromociones')
                    ->where('promociones.estado', '=', '1');
                } )
                ->join('marca', 'producto.idmarca', '=', 'marca.idmarca')
                ->join('presentacion', 'producto.idpresentacion', '=', 'presentacion.idpresentacion')
                ->join('proveedor', 'producto.idproveedor', '=', 'proveedor.idproveedor')
                ->join('categoria', 'producto.idcategoria', '=', 'categoria.idcategoria')
                ->select('producto.codigo', 'producto.idmarca', 'marca.descripcion as nombremarca',
                        'producto.idpresentacion','presentacion.medida as nombrepresentacion', 'producto.idproveedor',
                        DB::raw( "CONCAT( proveedor.nombre,' ', proveedor.apellidopat,' ', proveedor.apellidomat ) AS nombrecompleto" ),
                        'producto.idcategoria', 'categoria.nombre as nombrecategoria', 'producto.descripcion',
                        'producto.preciocosto','producto.precioventa', 'producto.iva')
                            ->where('producto.estado',1)
                            ->where(function ($query) use ( $valorfiltro ) {
                                $query->where('producto.descripcion', 'like', "%$valorfiltro%" )
                                      ->orWhere('marca.descripcion', 'like', "%$valorfiltro%"  );
                            } )
                            ->orderBy('producto.codigo', 'asc')
                ->paginate(10);


            }

        }

        /*
        $productos = \DB::table('producto')
        ->whereNotIn('producto.codigo', function( $query) {
            $query->select('oferta.codigo')->from('oferta')
            ->join('promociones', 'oferta.idpromociones', '=', 'promociones.idpromociones')
            ->where('promociones.estado', '=', '1');
        } )
        ->whereNotIn('producto.codigo', function( $query) {
            $query->select('descuento.codigo')->from('descuento')
            ->join('promociones', 'descuento.idpromociones', '=', 'promociones.idpromociones')
            ->where('promociones.estado', '=', '1');
        } )
        ->whereNotIn('producto.codigo', function( $query) {
            $query->select('paquetes.codigo1')->from('paquetes')
            ->join('promociones', 'paquetes.idpromociones', '=', 'promociones.idpromociones')
            ->where('promociones.estado', '=', '1');
        } )
        ->whereNotIn('producto.codigo', function( $query) {
            $query->select('paquetes.codigo2')->from('paquetes')
            ->join('promociones', 'paquetes.idpromociones', '=', 'promociones.idpromociones')
            ->where('promociones.estado', '=', '1');
        } )
        ->join('marca', 'producto.idmarca', '=', 'marca.idmarca')
        ->join('presentacion', 'producto.idpresentacion', '=', 'presentacion.idpresentacion')
        ->join('proveedor', 'producto.idproveedor', '=', 'proveedor.idproveedor')
        ->join('categoria', 'producto.idcategoria', '=', 'categoria.idcategoria')
        ->select('producto.codigo', 'producto.idmarca', 'marca.descripcion as nombremarca',
                'producto.idpresentacion','presentacion.medida as nombrepresentacion', 'producto.idproveedor',
                DB::raw( "CONCAT( proveedor.nombre,' ', proveedor.apellidopat,' ', proveedor.apellidomat ) AS nombrecompleto" ),
                'producto.idcategoria', 'categoria.nombre as nombrecategoria', 'producto.descripcion',
                'producto.preciocosto','producto.precioventa', 'producto.iva')
                    ->where('producto.estado',1)
                    ->orWhere('producto.codigo', '=', "{$request->get('descripcion')}" )
                    ->where('producto.descripcion', 'like', "%{$request->get('descripcion')}%" )
                    ->where('producto.estado',1)
                    ->orderBy('producto.codigo', 'asc')
        ->paginate(15);
        */

        /*
        $productos = \DB::table('producto')
            ->join('marca', 'producto.idmarca', '=', 'marca.idmarca')
            ->join('presentacion', 'producto.idpresentacion', '=', 'presentacion.idpresentacion')
            ->join('proveedor', 'producto.idproveedor', '=', 'proveedor.idproveedor')
            ->join('categoria', 'producto.idcategoria', '=', 'categoria.idcategoria')
            ->select('producto.codigo', 'producto.idmarca', 'marca.descripcion as nombremarca',
                    'producto.idpresentacion','presentacion.medida as nombrepresentacion', 'producto.idproveedor',
                    DB::raw( "CONCAT( proveedor.nombre,' ', proveedor.apellidopat,' ', proveedor.apellidomat ) AS nombrecompleto" ),
                    'producto.idcategoria', 'categoria.nombre as nombrecategoria', 'producto.descripcion',
                    'producto.preciocosto','producto.precioventa', 'producto.iva')
                    ->where('producto.descripcion', 'like', "%{$request->get('descripcion')}%" )
                    ->where('producto.estado',1)
                    ->orWhere('producto.codigo', 'like', "%{$request->get('descripcion')}%" )
                        ->where('producto.estado',1)
                        ->orWhere('marca.descripcion', 'like', "%{$request->get('descripcion')}%" )
                        ->where('producto.estado',1)
                    ->paginate(5);
        */

        if( count( $productos ) == 0 ) {

            Session::flash('danger', ' No hay productos que coincidan con la búsqueda ');

        }

        return view('vistasproductos/verproductos')->with('buscar', $productos);

    }

    public function listaproductos2( Request $request, $codigo ) {

        $input = $request->all();

        $var = $codigo;

        $producto1 = \DB::table('producto')
              ->join('marca', 'producto.idmarca', '=', 'marca.idmarca')
              ->join('presentacion', 'producto.idpresentacion', '=', 'presentacion.idpresentacion')
              ->join('proveedor', 'producto.idproveedor', '=', 'proveedor.idproveedor')
              ->join('categoria', 'producto.idcategoria', '=', 'categoria.idcategoria')
              ->select('producto.codigo', 'producto.idmarca', 'marca.descripcion as nombremarca',
                      'producto.idpresentacion','presentacion.medida as nombrepresentacion', 'producto.idproveedor',
                      DB::raw( "CONCAT( proveedor.nombre,' ', proveedor.apellidopat,' ', proveedor.apellidomat ) AS nombrecompleto" ),
                      'producto.idcategoria', 'categoria.nombre as nombrecategoria', 'producto.descripcion',
                      'producto.preciocosto','producto.precioventa', 'producto.iva')
                                        ->where( 'codigo', $codigo )
                                        ->take(1)->first();

        $consulta=Producto_Modelo::select('descripcion')
                                ->where( 'codigo',$codigo )
                                ->limit(1)
                                ->get();

        foreach ( $consulta as $k ) {
            $name = $k->{'descripcion'};
        }

        /*
        $productos = \DB::table('producto')
            ->join('marca', 'producto.idmarca', '=', 'marca.idmarca')
            ->join('presentacion', 'producto.idpresentacion', '=', 'presentacion.idpresentacion')
            ->join('proveedor', 'producto.idproveedor', '=', 'proveedor.idproveedor')
            ->join('categoria', 'producto.idcategoria', '=', 'categoria.idcategoria')
            ->select('producto.codigo', 'producto.idmarca', 'marca.descripcion as nombremarca',
                    'producto.idpresentacion','presentacion.medida as nombrepresentacion', 'producto.idproveedor',
                    DB::raw( "CONCAT( proveedor.nombre,' ', proveedor.apellidopat,' ', proveedor.apellidomat ) AS nombrecompleto" ),
                    'producto.idcategoria', 'categoria.nombre as nombrecategoria', 'producto.descripcion',
                    'producto.preciocosto','producto.precioventa', 'producto.iva')
                        ->where('producto.descripcion', 'like', "%{$request->get('descripcion')}%" )
                        ->where('producto.codigo', 'not like', $var )
                        ->where('producto.estado',1)
                        ->orWhere('producto.codigo', 'like', "%{$request->get('descripcion')}%" )
                        ->where('producto.codigo', 'not like', $var )
                        ->where('producto.estado',1)
                        ->orWhere('marca.descripcion', 'like', "%{$request->get('descripcion')}%" )
                        ->where('producto.codigo', 'not like', $var )
                        ->where('producto.estado',1)
                        ->paginate(5);
        */

        $valorfiltro = $request->get('descripcion');

        if( empty( $valorfiltro ) ) {

            //SE ENTRA A LA VISTA POR PRIMERA VEZ
            $productos = \DB::table('producto')
            ->whereNotIn('producto.codigo', function( $query) {
                $query->select('oferta.codigo')->from('oferta')
                ->join('promociones', 'oferta.idpromociones', '=', 'promociones.idpromociones')
                ->where('promociones.estado', '=', '1');
            } )
            ->whereNotIn('producto.codigo', function( $query) {
                $query->select('descuento.codigo')->from('descuento')
                ->join('promociones', 'descuento.idpromociones', '=', 'promociones.idpromociones')
                ->where('promociones.estado', '=', '1');
            } )
            ->whereNotIn('producto.codigo', function( $query) {
                $query->select('paquetes.codigo1')->from('paquetes')
                ->join('promociones', 'paquetes.idpromociones', '=', 'promociones.idpromociones')
                ->where('promociones.estado', '=', '1');
            } )
            ->whereNotIn('producto.codigo', function( $query) {
                $query->select('paquetes.codigo2')->from('paquetes')
                ->join('promociones', 'paquetes.idpromociones', '=', 'promociones.idpromociones')
                ->where('promociones.estado', '=', '1');
            } )
            ->whereNotIn('producto.codigo', function( $query ) use ( $codigo ) {
                $query->select('producto.codigo')->from('producto')
                ->where('producto.codigo', '=', $codigo );
            } )
            ->join('marca', 'producto.idmarca', '=', 'marca.idmarca')
            ->join('presentacion', 'producto.idpresentacion', '=', 'presentacion.idpresentacion')
            ->join('proveedor', 'producto.idproveedor', '=', 'proveedor.idproveedor')
            ->join('categoria', 'producto.idcategoria', '=', 'categoria.idcategoria')
            ->select('producto.codigo', 'producto.idmarca', 'marca.descripcion as nombremarca',
                    'producto.idpresentacion','presentacion.medida as nombrepresentacion', 'producto.idproveedor',
                    DB::raw( "CONCAT( proveedor.nombre,' ', proveedor.apellidopat,' ', proveedor.apellidomat ) AS nombrecompleto" ),
                    'producto.idcategoria', 'categoria.nombre as nombrecategoria', 'producto.descripcion',
                    'producto.preciocosto','producto.precioventa', 'producto.iva')
                        ->where('producto.estado',1)
                        ->orderBy('producto.codigo', 'asc')
            ->paginate(10);

        } else {
            //SE INTENTA BUSCAR UN PRODUCTO O PRODUCTOS MEDIANTE EL FILTRADO

            if(  is_numeric( $valorfiltro ) ) {
                //EL VALOR QUE SE INTRODUJO ES UN NUMERO

                $productos = \DB::table('producto')
                ->whereNotIn('producto.codigo', function( $query) {
                    $query->select('oferta.codigo')->from('oferta')
                    ->join('promociones', 'oferta.idpromociones', '=', 'promociones.idpromociones')
                    ->where('promociones.estado', '=', '1');
                } )
                ->whereNotIn('producto.codigo', function( $query) {
                    $query->select('descuento.codigo')->from('descuento')
                    ->join('promociones', 'descuento.idpromociones', '=', 'promociones.idpromociones')
                    ->where('promociones.estado', '=', '1');
                } )
                ->whereNotIn('producto.codigo', function( $query) {
                    $query->select('paquetes.codigo1')->from('paquetes')
                    ->join('promociones', 'paquetes.idpromociones', '=', 'promociones.idpromociones')
                    ->where('promociones.estado', '=', '1');
                } )
                ->whereNotIn('producto.codigo', function( $query) {
                    $query->select('paquetes.codigo2')->from('paquetes')
                    ->join('promociones', 'paquetes.idpromociones', '=', 'promociones.idpromociones')
                    ->where('promociones.estado', '=', '1');
                } )
                ->whereNotIn('producto.codigo', function( $query ) use ( $codigo ) {
                    $query->select('producto.codigo')->from('producto')
                    ->where('producto.codigo', '=', $codigo );
                } )
                ->join('marca', 'producto.idmarca', '=', 'marca.idmarca')
                ->join('presentacion', 'producto.idpresentacion', '=', 'presentacion.idpresentacion')
                ->join('proveedor', 'producto.idproveedor', '=', 'proveedor.idproveedor')
                ->join('categoria', 'producto.idcategoria', '=', 'categoria.idcategoria')
                ->select('producto.codigo', 'producto.idmarca', 'marca.descripcion as nombremarca',
                        'producto.idpresentacion','presentacion.medida as nombrepresentacion', 'producto.idproveedor',
                        DB::raw( "CONCAT( proveedor.nombre,' ', proveedor.apellidopat,' ', proveedor.apellidomat ) AS nombrecompleto" ),
                        'producto.idcategoria', 'categoria.nombre as nombrecategoria', 'producto.descripcion',
                        'producto.preciocosto','producto.precioventa', 'producto.iva')
                            ->where('producto.codigo', '=', $valorfiltro )
                            ->where('producto.estado',1)
                            ->orderBy('producto.codigo', 'asc')
                ->paginate(10);

            } else {
                //EL VALOR QUE SE INTRODUJO ES UNA STRING

                $productos = \DB::table('producto')
                ->whereNotIn('producto.codigo', function( $query) {
                    $query->select('oferta.codigo')->from('oferta')
                    ->join('promociones', 'oferta.idpromociones', '=', 'promociones.idpromociones')
                    ->where('promociones.estado', '=', '1');
                } )
                ->whereNotIn('producto.codigo', function( $query) {
                    $query->select('descuento.codigo')->from('descuento')
                    ->join('promociones', 'descuento.idpromociones', '=', 'promociones.idpromociones')
                    ->where('promociones.estado', '=', '1');
                } )
                ->whereNotIn('producto.codigo', function( $query) {
                    $query->select('paquetes.codigo1')->from('paquetes')
                    ->join('promociones', 'paquetes.idpromociones', '=', 'promociones.idpromociones')
                    ->where('promociones.estado', '=', '1');
                } )
                ->whereNotIn('producto.codigo', function( $query) {
                    $query->select('paquetes.codigo2')->from('paquetes')
                    ->join('promociones', 'paquetes.idpromociones', '=', 'promociones.idpromociones')
                    ->where('promociones.estado', '=', '1');
                } )
                ->whereNotIn('producto.codigo', function( $query ) use ( $codigo ) {
                    $query->select('producto.codigo')->from('producto')
                    ->where('producto.codigo', '=', $codigo );
                } )
                ->join('marca', 'producto.idmarca', '=', 'marca.idmarca')
                ->join('presentacion', 'producto.idpresentacion', '=', 'presentacion.idpresentacion')
                ->join('proveedor', 'producto.idproveedor', '=', 'proveedor.idproveedor')
                ->join('categoria', 'producto.idcategoria', '=', 'categoria.idcategoria')
                ->select('producto.codigo', 'producto.idmarca', 'marca.descripcion as nombremarca',
                        'producto.idpresentacion','presentacion.medida as nombrepresentacion', 'producto.idproveedor',
                        DB::raw( "CONCAT( proveedor.nombre,' ', proveedor.apellidopat,' ', proveedor.apellidomat ) AS nombrecompleto" ),
                        'producto.idcategoria', 'categoria.nombre as nombrecategoria', 'producto.descripcion',
                        'producto.preciocosto','producto.precioventa', 'producto.iva')
                            ->where('producto.estado',1)
                            ->where(function ($query) use ( $valorfiltro ) {
                                $query->where('producto.descripcion', 'like', "%$valorfiltro%" )
                                      ->orWhere('marca.descripcion', 'like', "%$valorfiltro%"  );
                            } )
                            ->orderBy('producto.codigo', 'asc')
                ->paginate(10);

            }

        }

        return view('vistasproductos/vercomplemento')->with( 'buscar', $productos )->with( 'codigo', $producto1 );

    }


                //API
    public function buscarproductocompleto( $codigo ) {

        return response()->json( $producto = DB::table('producto')
                ->join('marca', 'producto.idmarca', '=', 'marca.idmarca')
                ->join('presentacion', 'producto.idpresentacion', '=', 'presentacion.idpresentacion')
                ->join('proveedor', 'producto.idproveedor', '=', 'proveedor.idproveedor')
                ->join('categoria', 'producto.idcategoria', '=', 'categoria.idcategoria')
                ->join('inventario', 'producto.codigo', '=', 'inventario.codigo')
                ->select('producto.codigo as Codigo', 'producto.idmarca as idMarca',
                        'marca.descripcion as NombreMarca', 'producto.idpresentacion as idPresentacion',
                        'presentacion.medida as NombrePresentacion', 'producto.idproveedor as idProveedor',
                        DB::raw( "CONCAT( proveedor.nombre,' ', proveedor.apellidopat,' ', proveedor.apellidomat ) AS NombreCompleto" ),
                        'producto.idcategoria as idCategoria', 'categoria.nombre as NombreCategoria',
                        'producto.descripcion as Descripcion', 'producto.preciocosto as PrecioCosto',
                        'producto.precioventa as PrecioVenta', 'producto.iva as IVA',
                        'producto.imagen as Imagen', 'producto.estado as Estado',
                        'inventario.cantidad as Cantidad')
                        ->where('producto.codigo', $codigo )
                        ->take(1)->first() );


    }

           //API PARA REGISTROENTRADAINVENTARIO
    public function buscarproductocompletoentradainventario( $codigo ) {

        return response()->json( $producto = DB::table('producto')
                ->join('marca', 'producto.idmarca', '=', 'marca.idmarca')
                ->join('presentacion', 'producto.idpresentacion', '=', 'presentacion.idpresentacion')
                ->join('proveedor', 'producto.idproveedor', '=', 'proveedor.idproveedor')
                ->join('categoria', 'producto.idcategoria', '=', 'categoria.idcategoria')
                ->select('producto.codigo as Codigo', 'producto.idmarca as idMarca',
                        'marca.descripcion as NombreMarca', 'producto.idpresentacion as idPresentacion',
                        'presentacion.medida as NombrePresentacion', 'producto.idproveedor as idProveedor',
                        DB::raw( "CONCAT( proveedor.nombre,' ', proveedor.apellidopat,' ', proveedor.apellidomat ) AS NombreCompleto" ),
                        'producto.idcategoria as idCategoria', 'categoria.nombre as NombreCategoria',
                        'producto.descripcion as Descripcion', 'producto.preciocosto as PrecioCosto',
                        'producto.precioventa as PrecioVenta', 'producto.iva as IVA',
                        'producto.imagen as Imagen', 'producto.estado as Estado' )
                        ->where('producto.codigo', $codigo )
                        ->take(1)->first() );


    }

        //API PARA VISTA REGISTRARVENTA, REGISTRARENTRADAINVENTARIO
    public function listadoproductosmodal() {

        $listadoproductos = DB::table('producto')
                    ->select(   'producto.codigo as CodigoProducto', 'producto.descripcion as DescripcionProducto',
                                'producto.precioventa as PrecioVentaProducto' )
                    ->where('producto.estado', 1)
                    ->orderBy('producto.descripcion', 'ASC')
                    ->get();

        return response()->json( $listadoproductos );

    }

    //API PARA VISTA REGISTRARENTREGAPEDIDO, REGISTRARSALIDAPEDIDO Y REGISTRARSALIDAINVENTARIO
    public function proveedoreslistadoproductosmodal() {

        $listadoproductos = DB::table('producto')
                    ->join('inventario', 'producto.codigo', '=', 'inventario.codigo')
                    ->select(   'producto.codigo as CodigoProducto', 'producto.descripcion as DescripcionProducto',
                                'producto.precioventa as PrecioVentaProducto' )
                    ->where('producto.estado', 1)
                    ->orderBy('producto.descripcion', 'ASC')
                    ->get();

        return response()->json( $listadoproductos );

    }


    //API Oferta
    public function buscarpromocionoferta( $codigo ) {


        $consulta = DB::table('promociones')
        ->join('oferta', 'promociones.idpromociones', '=', 'oferta.idpromociones')
        ->join('producto', 'oferta.codigo', '=', 'producto.codigo')
        ->select('promociones.nombre as NombrePromocion', 'promociones.descripcion as DescripcionPromocion',
        'promociones.total as PrecioTotalPromocion', 'oferta.tipo as TipoPromocion',
        'oferta.precionuevo as PrecioPromocion', 'producto.codigo as CodigoProducto',
        'producto.descripcion as DescripcionProducto', 'producto.precioventa as PrecioProducto')
        ->where('promociones.estado', 1 )
        ->where('oferta.codigo' , $codigo )
        ->get();
        //->take(1)->first();

        return response()->json( $consulta );

    }

    //API Descuento
    public function buscarpromociondescuento( $codigo ) {

        $consulta = DB::table('promociones')
        ->join('descuento', 'promociones.idpromociones', '=', 'descuento.idpromociones')
        ->join('producto', 'descuento.codigo', '=', 'producto.codigo')
        ->select('promociones.nombre as NombrePromocion', 'promociones.descripcion as DescripcionPromocion',
        'promociones.total as PrecioTotalPromocion', 'descuento.precionuevo as PrecioPromocion',
        'descuento.descuento as DescuentoPromocion', 'producto.codigo as CodigoProducto',
        'producto.descripcion as DescripcionProducto',
        'producto.precioventa as PrecioProducto')
        ->where('promociones.estado', 1 )
        ->where('descuento.codigo' , $codigo )
        ->get();
        //->take(1)->first();

        return response()->json( $consulta );

    }

    //API Paquetes
    public function buscarpromocionpaquetes( $codigo ) {

        $consulta = DB::table('promociones')
        ->join('paquetes', 'promociones.idpromociones', '=', 'paquetes.idpromociones')
        ->join('producto', 'paquetes.codigo1', '=', 'producto.codigo' )
        ->join('producto as producto2', 'paquetes.codigo2', '=', 'producto2.codigo')
        ->select('promociones.idpromociones as idPromociones', 'paquetes.idpaquetes as idPaquetes',
        'promociones.nombre as NombrePromocion', 'promociones.descripcion as DescripcionPromocion',
        'promociones.total as PrecioTotalPromocion',
        'paquetes.codigo1 as CodigoPaquete1', 'paquetes.codigo2 as CodigoPaquetes2',
        'paquetes.precio1 as PrecioPromocion1', 'paquetes.precio2 as PrecioPromocion2',
        'paquetes.descuento1 as DescuentoPromocion1', 'paquetes.descuento2 as DescuentoPromocion2',
        'producto.codigo as CodigoProducto', 'producto.descripcion as DescripcionProducto',
        'producto.precioventa as PrecioProducto')
        ->where(function ($query) use ( $codigo ) {
            $query->where('paquetes.codigo1', '=', $codigo)
                  ->orWhere('paquetes.codigo2', '=', $codigo);
        })
        ->where('promociones.estado', 1 )
        ->get();


        return response()->json( $consulta );

    }

    //API VerificarSiHayPromocionesProducto
    public function verificarpromocionesproducto( $codigo ) {


        $consulta = \DB::table('promociones')->select('promociones.tipo', 'promociones.idpromociones')
        ->whereIn('promociones.idPromociones', function( $query)  use ( $codigo ) {

            $query->select('oferta.idPromociones')->from('promociones')
            ->join('oferta', 'promociones.idPromociones', '=', 'oferta.idPromociones')
            ->join('producto', 'oferta.Codigo', '=', 'producto.Codigo')
            ->where('producto.estado', '=', '1' )
            ->where('promociones.estado', '=', '1' )
            ->where('oferta.Codigo', '=', $codigo );
        } )
        ->orwhereIn('promociones.idPromociones', function( $query)  use ( $codigo ) {

            $query->select('descuento.idPromociones')->from('promociones')
            ->join('descuento', 'promociones.idPromociones', '=', 'descuento.idPromociones')
            ->join('producto', 'descuento.Codigo', '=', 'producto.Codigo')
            ->where('producto.estado', '=', '1' )
            ->where('promociones.estado', '=', '1' )
            ->where('descuento.Codigo', '=', $codigo );
        } )
        ->orwhereIn('promociones.idPromociones', function( $query)  use ( $codigo ) {

            $query->select('paquetes.idPromociones')->from('promociones')
            ->join('paquetes', 'promociones.idPromociones', '=', 'paquetes.idPromociones')
            ->join('producto', 'paquetes.Codigo1', '=', 'producto.Codigo')
            ->where('producto.estado', '=', '1' )
            ->where('promociones.estado', '=', '1' )
            ->where('paquetes.Codigo1', '=', $codigo );
        } )
        ->orwhereIn('promociones.idPromociones', function( $query)  use ( $codigo ) {

            $query->select('paquetes.idPromociones')->from('promociones')
            ->join('paquetes', 'promociones.idPromociones', '=', 'paquetes.idPromociones')
            ->join('producto', 'paquetes.Codigo2', '=', 'producto.Codigo')
            ->where('producto.estado', '=', '1' )
            ->where('promociones.estado', '=', '1' )
            ->where('paquetes.Codigo2', '=', $codigo );
        } )
        ->get();

        if( count( $consulta ) == 0 ) {

            $data[] = [
                'TipoPromocion' => "0"
            ];

            return response()->json( $data );

        } else if ( count( $consulta ) == 1 ) {

            if( $consulta[0]->tipo == "1" ) {

                $consultaoferta = \DB::table('producto')
                ->select('promociones.idpromociones as idPromociones', 'producto.codigo as Codigo', 'producto.descripcion as Descripcion',
                'producto.precioventa as ProductoPrecioVenta', 'oferta.idoferta as idOferta',
                'promociones.tipo as TipoPromocion', 'promociones.nombre as NombrePromocion',
                'promociones.descripcion as DescripcionPromocion', 'promociones.total as TotalPromocion',
                'oferta.tipo as TipoOferta', 'oferta.precionuevo as PromocionPrecioNuevo'
                )
                ->join('oferta', 'producto.Codigo', '=', 'oferta.Codigo')
                ->join('promociones', 'oferta.idPromociones', '=', 'promociones.idPromociones')
                ->where('producto.estado', '=', '1' )
                ->where('promociones.estado', '=', '1' )
                ->where('oferta.Codigo', '=', $codigo )->get();

                return response()->json( $consultaoferta );

            } else if( $consulta[0]->tipo == "2" ) {

                $consultadescuento = \DB::table('producto')
                ->select('promociones.idpromociones as idPromociones', 'producto.codigo as Codigo', 'producto.descripcion as Descripcion',
                'producto.precioventa as ProductoPrecioVenta', 'descuento.iddescuento as idDescuento',
                'promociones.tipo as TipoPromocion', 'promociones.nombre as NombrePromocion',
                'promociones.descripcion as DescripcionPromocion', 'promociones.total as TotalPromocion',
                'descuento.descuento as TipoDescuento', 'descuento.precionuevo as PromocionPrecioNuevo'
                )
                ->join('descuento', 'producto.Codigo', '=', 'descuento.Codigo')
                ->join('promociones', 'descuento.idPromociones', '=', 'promociones.idPromociones')
                ->where('producto.estado', '=', '1' )
                ->where('promociones.estado', '=', '1' )
                ->where('descuento.Codigo', '=', $codigo )->get();

                return response()->json( $consultadescuento );

            } else if( $consulta[0]->tipo == "3" ) {

                $subconsultapaquete1 = \DB::table('producto')
                ->select('promociones.idpromociones as idPromociones', 'producto.codigo as Codigo', 'producto.descripcion as Descripcion',
                'producto.precioventa as ProductoPrecioVenta', 'paquetes.idpaquetes as idPaquetes',
                'promociones.nombre as NombrePromocion', 'promociones.tipo as TipoPromocion',
                'promociones.descripcion as DescripcionPromocion', 'promociones.total as TotalPromocion',
                'paquetes.precio1 as PromocionPrecioNuevo', 'paquetes.precio2 as PromocionPrecioComplementarioNuevo',
                'paquetes.codigo2 as CodigoComplementarioPaquete', 'paquetes.descuento2 as DescuentoPrecioComplementario')
                ->join('paquetes', 'producto.Codigo', '=', 'paquetes.Codigo1')
                ->join('promociones', 'paquetes.idPromociones', '=', 'promociones.idPromociones')
                ->where('producto.estado', '=', '1' )
                ->where('promociones.estado', '=', '1' )
                ->where('paquetes.Codigo1', '=', $codigo )->get();

                $subconsultapaquete2 = \DB::table('producto')
                ->select('promociones.idpromociones as idPromociones', 'producto.codigo as Codigo', 'producto.descripcion as Descripcion',
                'producto.precioventa as ProductoPrecioVenta', 'paquetes.idpaquetes as idPaquetes',
                'promociones.nombre as NombrePromocion', 'promociones.tipo as TipoPromocion',
                'promociones.descripcion as DescripcionPromocion', 'promociones.total as TotalPromocion',
                'paquetes.precio2 as PromocionPrecioNuevo', 'paquetes.precio1 as PromocionPrecioComplementarioNuevo',
                'paquetes.codigo1 as CodigoComplementarioPaquete', 'paquetes.descuento1 as DescuentoPrecioComplementario')
                ->join('paquetes', 'producto.Codigo', '=', 'paquetes.Codigo2')
                ->join('promociones', 'paquetes.idPromociones', '=', 'promociones.idPromociones')
                ->where('producto.estado', '=', '1' )
                ->where('promociones.estado', '=', '1' )
                ->where('paquetes.Codigo2', '=', $codigo )->get();


                $consultapaquete = $subconsultapaquete1->merge( $subconsultapaquete2 );

                return response()->json( $consultapaquete );

            }

        } else {

        }

    }

    public function listadoproductosordenadocodigo() {
        
        $listadoproductos = DB::table('producto')
            ->join('marca', 'producto.idmarca', '=', 'marca.idmarca')
            ->join('presentacion', 'producto.idpresentacion', '=', 'presentacion.idpresentacion')
            ->join('proveedor', 'producto.idproveedor', '=', 'proveedor.idproveedor')
            ->join('categoria', 'producto.idcategoria', '=', 'categoria.idcategoria')
            ->select('producto.codigo', 'producto.idmarca', 'marca.descripcion as nombremarca',
                    'producto.idpresentacion','presentacion.medida as nombrepresentacion', 'producto.idproveedor',
                    DB::raw( "CONCAT( proveedor.nombre,' ', proveedor.apellidopat,' ', proveedor.apellidomat ) AS nombrecompleto" ),
                    'producto.idcategoria', 'categoria.nombre as nombrecategoria', 'producto.descripcion',
                    'producto.preciocosto','producto.precioventa', 'producto.iva')
                    ->where('producto.estado',1)
                    ->where('proveedor.estado',1)
                    ->orderBy('codigo','ASC')
                    ->get();
                
        return response()->json( $listadoproductos );

    }

        //ListadoProductos ordenado por categorias para el filtrado de la vista mostrar productos
    public function listadoproductosordenadomarca() {
    
        $listadoproductos = DB::table('producto')
        ->join('marca', 'producto.idmarca', '=', 'marca.idmarca')
        ->join('presentacion', 'producto.idpresentacion', '=', 'presentacion.idpresentacion')
        ->join('proveedor', 'producto.idproveedor', '=', 'proveedor.idproveedor')
        ->join('categoria', 'producto.idcategoria', '=', 'categoria.idcategoria')
        ->select('producto.codigo', 'producto.idmarca', 'marca.descripcion as nombremarca',
                'producto.idpresentacion','presentacion.medida as nombrepresentacion', 'producto.idproveedor',
                DB::raw( "CONCAT( proveedor.nombre,' ', proveedor.apellidopat,' ', proveedor.apellidomat ) AS nombrecompleto" ),
                'producto.idcategoria', 'categoria.nombre as nombrecategoria', 'producto.descripcion',
                'producto.preciocosto','producto.precioventa', 'producto.iva')
                ->where('producto.estado',1)
                ->where('proveedor.estado',1)
                ->orderBy('marca.descripcion','ASC')
                ->get();
                
        return response()->json( $listadoproductos );

    }  
    
        //ListadoProductos ordenado por presentación para el filtrado de la vista mostrar productos
    public function listadoproductosordenadopresentacion() {
    
        $listadoproductos = DB::table('producto')
        ->join('marca', 'producto.idmarca', '=', 'marca.idmarca')
        ->join('presentacion', 'producto.idpresentacion', '=', 'presentacion.idpresentacion')
        ->join('proveedor', 'producto.idproveedor', '=', 'proveedor.idproveedor')
        ->join('categoria', 'producto.idcategoria', '=', 'categoria.idcategoria')
        ->select('producto.codigo', 'producto.idmarca', 'marca.descripcion as nombremarca',
                'producto.idpresentacion','presentacion.medida as nombrepresentacion', 'producto.idproveedor',
                DB::raw( "CONCAT( proveedor.nombre,' ', proveedor.apellidopat,' ', proveedor.apellidomat ) AS nombrecompleto" ),
                'producto.idcategoria', 'categoria.nombre as nombrecategoria', 'producto.descripcion',
                'producto.preciocosto','producto.precioventa', 'producto.iva')
                ->where('producto.estado',1)
                ->where('proveedor.estado',1)
                ->orderBy('presentacion.medida','ASC')
                ->get();
                
        return response()->json( $listadoproductos );

    }

        //ListadoProductos ordenado por categorias para el filtrado de la vista mostrar productos
    public function listadoproductosordenadocategoria() {
    
        $listadoproductos = DB::table('producto')
        ->join('marca', 'producto.idmarca', '=', 'marca.idmarca')
        ->join('presentacion', 'producto.idpresentacion', '=', 'presentacion.idpresentacion')
        ->join('proveedor', 'producto.idproveedor', '=', 'proveedor.idproveedor')
        ->join('categoria', 'producto.idcategoria', '=', 'categoria.idcategoria')
        ->select('producto.codigo', 'producto.idmarca', 'marca.descripcion as nombremarca',
                'producto.idpresentacion','presentacion.medida as nombrepresentacion', 'producto.idproveedor',
                DB::raw( "CONCAT( proveedor.nombre,' ', proveedor.apellidopat,' ', proveedor.apellidomat ) AS nombrecompleto" ),
                'producto.idcategoria', 'categoria.nombre as nombrecategoria', 'producto.descripcion',
                'producto.preciocosto','producto.precioventa', 'producto.iva')
                ->where('producto.estado',1)
                ->where('proveedor.estado',1)
                ->orderBy('categoria.nombre','ASC')
                ->get();
                
        return response()->json( $listadoproductos );

    }

        //ListadoProductos ordenado por descripcion para el filtrado de la vista mostrar productos
    public function listadoproductosordenadodescripcion() {
        
        $listadoproductos = DB::table('producto')
        ->join('marca', 'producto.idmarca', '=', 'marca.idmarca')
        ->join('presentacion', 'producto.idpresentacion', '=', 'presentacion.idpresentacion')
        ->join('proveedor', 'producto.idproveedor', '=', 'proveedor.idproveedor')
        ->join('categoria', 'producto.idcategoria', '=', 'categoria.idcategoria')
        ->select('producto.codigo', 'producto.idmarca', 'marca.descripcion as nombremarca',
                'producto.idpresentacion','presentacion.medida as nombrepresentacion', 'producto.idproveedor',
                DB::raw( "CONCAT( proveedor.nombre,' ', proveedor.apellidopat,' ', proveedor.apellidomat ) AS nombrecompleto" ),
                'producto.idcategoria', 'categoria.nombre as nombrecategoria', 'producto.descripcion',
                'producto.preciocosto','producto.precioventa', 'producto.iva')
                ->where('producto.estado',1)
                ->where('proveedor.estado',1)
                ->orderBy('producto.descripcion','ASC')
                ->get();
                
        return response()->json( $listadoproductos );

    }

}
