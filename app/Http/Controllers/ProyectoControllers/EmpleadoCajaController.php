<?php

namespace App\Http\Controllers\ProyectoControllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use DB;
use Session;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

class EmpleadoCajaController extends Controller
{
    
    public function registrarsaldoinicialcaja( Request $request ) {

        $saldoinicial = json_decode( $request->SaldoInicial );
                
        $idempleado = Auth::User()->idEmpleado;

        $fecha = Carbon::today();

        $consultaidcaja = DB::table('caja')->select('idcaja as idcaja')
                                            ->where('idempleado', $idempleado )
                                            ->get();

        $idcaja = $consultaidcaja[0]->{'idcaja'};

        /*
        DB::insert('insert into empleadocaja ( idcaja, idempleado, saldoinicial, saldofinal, fecha, estado )
                values (?, ?, ?, ?, ?, ? )', [ $idcaja, $idempleado, $saldoinicial, 0.0,  $fecha, 1 ] );
        */
        
        DB::insert('insert into empleadocaja ( idcaja, idempleado, saldoinicial, saldofinal, fecha, estado )
                values (?, ?, ?, ?, ?, ? )', [ $idcaja, $idempleado, $saldoinicial, 0.0,  $fecha, 0 ] );
        
        return response()->json("REGISTRO EXITOSO");

    }

}
