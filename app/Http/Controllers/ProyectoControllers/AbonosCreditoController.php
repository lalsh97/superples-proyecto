<?php

namespace App\Http\Controllers\ProyectoControllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Session;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

use App\Models\AbonosCredito_Modelo;
use App\Models\Cliente_Modelo;
use App\Models\CreditoCliente_Modelo;

class AbonosCreditoController extends Controller
{

    public function listadocreditosclientes() {

        /*
        $listadocreditosclientes = DB::table('cliente')
                ->join('creditocliente', 'cliente.idcliente', '=', 'creditocliente.idcliente')
                ->select( 'creditocliente.idcreditocliente', 'cliente.idcliente',
                          DB::raw( "CONCAT(nombre,' ', apellidopat,' ', apellidomat ) AS nombrecompleto"),
                          DB::raw( "CONCAT(calle,' ', numero,' ', colonia ) AS direccion"), 'telefono',
                          'creditocliente.limite as limite', 'creditocliente.canadeudada as cantidadadeudada' )
                ->where('cliente.estado',1)
                ->where('creditocliente.estado',1)
                ->orderBy('idcliente','ASC')
                ->get();
        */

        $listadocreditosclientes = DB::table('cliente')
                ->join('creditocliente', 'cliente.idcliente', '=', 'creditocliente.idcliente')
                ->select( 'creditocliente.idcreditocliente', 'cliente.idcliente',
                          DB::raw( "CONCAT(nombre,' ', apellidopat,' ', apellidomat ) AS nombrecompleto"),
                          DB::raw( "CONCAT(calle,' ', numero,' ', colonia ) AS direccion"), 'telefono',
                          'creditocliente.limite as limite', 'creditocliente.canadeudada as cantidadadeudada' )
                ->orderBy('idcliente','ASC')
                ->get();

        $totalsaldoadeudados = DB::table('creditocliente')
                            ->select( DB::raw( "SUM(CanAdeudada) as totalsaldoadeudados" ) )
                            ->get();

        return view('vistasclientes/abonoscredito/listadocreditosclientes')->with( 'listadocreditosclientes', $listadocreditosclientes)->with( 'totalsaldoadeudados', $totalsaldoadeudados );

    }

    public function informacioncreditocliente( $idcreditocliente, $idcliente ) {

        /*
        $datoscreditocliente = DB::table('cliente')
                ->join('creditocliente', 'cliente.idcliente', '=', 'creditocliente.idcliente')
                ->select( 'creditocliente.idcreditocliente', 'cliente.idcliente',
                          DB::raw( "CONCAT(nombre,' ', apellidopat,' ', apellidomat ) AS nombrecompleto"),
                          DB::raw( "CONCAT(calle, ' ', numero, ', ' , colonia, ' ' , codigop, ', ' , municipio, ', ' , ciudad ) AS direccion"),
                           'telefono', 'curp',
                          'creditocliente.limite as limite', 'creditocliente.canadeudada as cantidadadeudada' , 'creditocliente.estado as estadocreditocliente')
                ->where('cliente.estado',1)
                ->where('cliente.idcliente', $idcliente )
                ->take(1)
                ->first();
        */
        $datoscreditocliente = DB::table('cliente')
        ->join('creditocliente', 'cliente.idcliente', '=', 'creditocliente.idcliente')
        ->select( 'creditocliente.idcreditocliente', 'cliente.idcliente',
                  DB::raw( "CONCAT(nombre,' ', apellidopat,' ', apellidomat ) AS nombrecompleto"),
                  DB::raw( "CONCAT(calle, ' ', numero, ', ' , colonia, ' ' , codigop, ', ' , municipio, ', ' , ciudad ) AS direccion"),
                   'telefono', 'curp',
                  'creditocliente.limite as limite', 'creditocliente.canadeudada as cantidadadeudada', 'creditocliente.estado as estadocreditocliente' )
        ->where('cliente.idcliente', $idcliente )
        ->where('cliente.idcliente', $idcliente )
        ->take(1)
        ->first();

        $datosabonocredito = AbonosCredito_Modelo::select('fecha as Fecha', 'cantidad as CantidadAbonada')
                            ->where('idcreditocliente' , $idcreditocliente )
                            ->orderBy('fecha','ASC')
                            ->get();

        return view('vistasclientes/abonoscredito/informacioncreditocliente')->with( 'datoscreditocliente', $datoscreditocliente )->with( 'datosabonocredito', $datosabonocredito );

    }

    public function abonarcantidadcredito( Request $request ) {
        
        $cambio = 0;

        if( $request->botonformabonoscreditocliente == 'Registrar' ) {

            $idcliente =   $request->input('idcliente');
            $idcreditocliente =   $request->input('idcreditocliente');
            $cantidadaabonar =  bcadd( $request->input('cantidadaabonar'), '0', 2 );

            if( $cantidadaabonar > 0 ) {

                $nombrecliente = $request->input('nombrecliente');
                $limite = bcadd( $request->input('limite'), '0', 2 );

                $fecha = Carbon::now();
                                                            
                $consultacantidadadeudada = CreditoCliente_Modelo::select('CanAdeudada')
                                        ->where('idCreditoCliente',$idcreditocliente)
                                        ->where('idCliente',$idcliente)
                                        ->limit(1)
                                        ->get();   
                        
                $cantidadadeudadaold = bcadd( $consultacantidadadeudada[0]->{'CanAdeudada'}, '0', 2 );

                if( $cantidadadeudadaold == 0 ) {
                    
                    Session::flash('warning', ' No se pudo realizar el abono al crédito ya que el cliente no tiene adeudo alguno ');

                    return redirect()->back();

                } else {

                    $cantidadadeudadanew = $cantidadadeudadaold - $cantidadaabonar;
    
                    if( $cantidadadeudadanew < 0 ) {
                        
                        $cambio = $cantidadadeudadanew;
                        $cantidadadeudadanew = bcadd( 0, '0', 2 );
                        $cantidadaabonar = $cantidadadeudadaold;
    
                    }

                    $idempleado = Auth::User()->idEmpleado;
          
                    $consultaidcaja = DB::table('caja')->select('idcaja as idcaja')
                                                        ->where('idempleado', $idempleado )
                                                        ->get();
            
                    $idcaja = $consultaidcaja[0]->{'idcaja'};
                                    
                    AbonosCredito_Modelo::create( [ 'idcreditocliente' => $idcreditocliente,
                                                    'cantidad' => $cantidadaabonar,
                                                    'fecha' => $fecha,
                                                    'idempleado' => $idempleado, 
                                                    'idcaja' => $idcaja,
                                                    'estado' => 0 ] );
    
                    DB::table('creditocliente')
                    ->where('idCreditoCliente',$idcreditocliente)
                    ->where('idCliente',$idcliente)
                    ->limit(1)
                    ->update( array( 'CanAdeudada' => $cantidadadeudadanew ) );
    
                    $this ->imprimircomprobanteabono( $cantidadadeudadaold, $cantidadaabonar, $nombrecliente, $limite );


                    if( $cambio < 0 ) {

                        $cambio *= -1;

                        /*
                        $datoscreditocliente = DB::table('cliente')
                            ->join('creditocliente', 'cliente.idcliente', '=', 'creditocliente.idcliente')
                            ->select( 'creditocliente.idcreditocliente', 'cliente.idcliente',
                                    DB::raw( "CONCAT(nombre,' ', apellidopat,' ', apellidomat ) AS nombrecompleto"),
                                    DB::raw( "CONCAT(calle, ' ', numero, ', ' , colonia, ' ' , codigop, ', ' , municipio, ', ' , ciudad ) AS direccion"),
                                    'telefono', 'curp',
                                    'creditocliente.limite as limite', 'creditocliente.canadeudada as cantidadadeudada' )
                            ->where('cliente.estado',1)
                            ->where('cliente.idcliente', $idcliente )
                            ->take(1)
                            ->first();

                            */
                            
                        $datoscreditocliente = DB::table('cliente')
                        ->join('creditocliente', 'cliente.idcliente', '=', 'creditocliente.idcliente')
                        ->select( 'creditocliente.idcreditocliente', 'cliente.idcliente',
                                DB::raw( "CONCAT(nombre,' ', apellidopat,' ', apellidomat ) AS nombrecompleto"),
                                DB::raw( "CONCAT(calle, ' ', numero, ', ' , colonia, ' ' , codigop, ', ' , municipio, ', ' , ciudad ) AS direccion"),
                                'telefono', 'curp',
                                'creditocliente.limite as limite', 'creditocliente.canadeudada as cantidadadeudada', 'creditocliente.estado as estadocreditocliente' )
                        ->where('cliente.idcliente', $idcliente )
                        ->take(1)
                        ->first();

                        $datosabonocredito = AbonosCredito_Modelo::select('fecha as Fecha', 'cantidad as CantidadAbonada')
                                            ->where('idcreditocliente' , $idcreditocliente )
                                            ->orderBy('fecha','ASC')
                                            ->get();
                        
                        return view('vistasclientes/abonoscredito/informacioncreditocliente')->with( 'datoscreditocliente', $datoscreditocliente )->with( 'datosabonocredito', $datosabonocredito )->with( 'cambio', $cambio );
    
                    }
    
                    return redirect()->to('informacioncreditocliente/'. $idcreditocliente.'/'.$idcliente );
                                    
                }

            } else {

                Session::flash('danger', ' La cantidad a abonar debe ser mayor a 0, para que el abono se pueda realizar ');

                return redirect()->back();

            }

        } elseif( $request->botonformabonoscreditocliente == 'Regresar' ) {

            return redirect()->to('listadocreditosclientes');

        } elseif( $request->botonformabonoscreditocliente == 'Desactivar' ) {

            $idcliente =   $request->input('idcliente');
            $idcreditocliente =   $request->input('idcreditocliente');

            DB::table('creditocliente')
                    ->where('idCreditoCliente',$idcreditocliente)
                    ->where('idCliente',$idcliente)
                    ->limit(1)
                    ->update( array( 'Estado' => '0' ) );

            return redirect()->to('listadocreditosclientes');

        }

    }

    public function abonarcantidadcreditoget() {

        $listadocreditosclientes = DB::table('cliente')
                ->join('creditocliente', 'cliente.idcliente', '=', 'creditocliente.idcliente')
                ->select( 'creditocliente.idcreditocliente', 'cliente.idcliente',
                          DB::raw( "CONCAT(nombre,' ', apellidopat,' ', apellidomat ) AS nombrecompleto"),
                          DB::raw( "CONCAT(calle,' ', numero,' ', colonia ) AS direccion"), 'telefono',
                          'creditocliente.limite as limite', 'creditocliente.canadeudada as cantidadadeudada' )
                ->orderBy('idcliente','ASC')
                ->get();

        $totalsaldoadeudados = DB::table('creditocliente')
                            ->select( DB::raw( "SUM(CanAdeudada) as totalsaldoadeudados" ) )
                            ->get();

        return view('vistasclientes/abonoscredito/listadocreditosclientes')->with( 'listadocreditosclientes', $listadocreditosclientes)->with( 'totalsaldoadeudados', $totalsaldoadeudados );

    }
    

    public function imprimircomprobanteabono( $adeudo, $abono, $nombrecliente, $limite ) {

        $saldoactual = bcadd( ( $adeudo - $abono ), '0', 2 );

        $today = Carbon::now()->format('d-m-Y');

        $pdf = \PDF::setOptions([
             'images' => true,
             'enable-javascript' => true,
             'javascript-delay' => 5000,
             'enable-smart-shrinking' => true,
             'no-stop-slow-scripts' => true
        ]);

        $pdf->loadHTML(
             '<html>
                  <head>
                       <link rel="stylesheet" href="css/estiloscomprobanteabono.css">
                  </head>

             <body>
                  <div class="ticket">
                       <img src="images/Ticket/SuperPLES.png" width="35" height="35">
                       <p class="centrado">Comprobante abono crédito</p>
                       <p class="centrado">Nombre del cliente</p>
                       <p class="centradonombre">' .$nombrecliente .'</p>
                       <p class="centrado">FECHA '. $today .'</p>

                       <table>
                            <thead>
                                 <tr>
                                 <th class="texto"> </th>
                                 <th class="precio"> </th>
                                 </tr>
                            </thead>
                            <tbody>
                            <tr class="filanoborde">
                                 <td class="texto">Limite crédito</td>
                                <td class="precio">$ '. $limite .' </td>
                            </tr>
                            <tr class="filanoborde">
                                 <td class="texto">Cant. adeudada</td>
                                <td class="precio">$ '. $adeudo .' </td>
                            </tr>
                            <tr class="filanoborde">
                                 <td class="texto">Cant abonada</td>
                                <td class="precio">$ '. $abono .' </td>
                            </tr>
                            <tr class="filanoborde">
                                 <td class="texto">Saldo act.</td>
                                <td class="precio">$ '. $saldoactual .' </td>
                            </tr>
                            </tbody>
                       </table>

                       <p class="centrado">¡GRACIAS POR SU PREFERENCIA! <br>www.plataformalatinoamericana.com </p>
                  </div>
             </body>

        </html>');

        //$pdf->setPaper('a4', 'landscape')->setWarnings(false);
        $pdf->setPaper(array(0,0,500,1000));

        //$pdf->save( 'TicketVentaNum: '. $idventa. ' - '. $today .'.pdf');
        $usuariowindows =  get_current_user();

      $pdf->save( "C:/Users/" . $usuariowindows . "/Downloads/ComprobanteAbono" . $nombrecliente . "-" . $today . ".pdf" );

    }

    public function desactivarcreditocliente( $idcreditocliente, $idcliente ) {

        DB::table('creditocliente')
                ->where('idCreditoCliente',$idcreditocliente)
                ->where('idCliente',$idcliente)
                ->limit(1)
                ->update( array( 'Estado' => '0' ) );

        return redirect()->to('listadocreditosclientes');

    }

}
