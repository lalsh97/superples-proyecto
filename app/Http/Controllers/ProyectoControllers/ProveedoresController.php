<?php

namespace App\Http\Controllers\ProyectoControllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Session;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;


use App\Models\Proveedor_Modelo;
use App\Models\Pedido_Modelo;
use App\Models\DetallePedido_Modelo;
use App\Models\Devoluciones_Modelo;
use App\Models\MovimientoInventario_Modelo;
use App\Models\Inventario_Modelo;
use App\Models\Producto_Modelo;

class ProveedoresController extends Controller
{


  	public function listadoproveedores() {

        $listadoproveedores = Proveedor_Modelo::select( 'idproveedor as idProveedor',
                                    DB::raw( "CONCAT( nombre,' ', apellidopat,' ', apellidomat ) AS nombrecompleto" ),
                                    'rfc as Rfc', 'telefono as Telefono', 'empresa as Empresa', 'estado' )
                ->where( 'estado', 1 )
                ->where('idProveedor', '!=', '1')
                ->orderBy( 'idproveedor', 'ASC' )
                ->get();
        
        //$proveedores = Proveedor_Modelo::where('Estado','1')->where('idProveedor', '!=', '1')->get();

		return view ('vistasproveedores/listadoproveedores')->with( 'registroproveedores', $listadoproveedores );

	}

    public function busquedaproveedor( $operacion ) {

      return view('vistasproveedores/busquedaproveedor')->with( 'operacion' , $operacion );

    }

    public function buscarproveedor( $rfc ) {
        
        $proveedor = Proveedor_Modelo::where('rfc', $rfc )->where('estado', 1)->get();

        if( count( $proveedor ) == 1 ) {                

            
            $listadoproveedor = Proveedor_Modelo::select( 'idproveedor as idProveedor',
                                DB::raw( "CONCAT( nombre,' ', apellidopat,' ', apellidomat ) AS nombrecompleto" ),
                                'rfc as Rfc', 'telefono as Telefono', 'empresa as Empresa', 'estado' )
            ->where( 'estado', 1 )
            ->where('rfc', '=', $rfc )
            ->orderBy( 'idproveedor', 'ASC' )
            ->get();
            
                /*
                    $listadoproveedor = Proveedor_Modelo::select('*')
                    ->where('rfc',$rfc)
                    ->orderBy('rfc','ASC')
                    ->get();
                */
                
                //return view ('vistasproveedores/listadoproveedores')->with('registroproveedores', $listadoproveedor);
                
                return view ('vistasproveedores/listadoproveedores')->with('registroproveedores', $listadoproveedor);

        } else {

            Session::flash('warning', ' No existe proveedor alguno registrado con ese RFC ');
            return redirect()->back();

        }

    }

    	//Funciones para registrar

	public function registrarproveedor() {

		return view('vistasproveedores/registrarproveedor');

	}

	public function insertarproveedor( Request $request ) {

		if( $request->botonformproveedor == 'Registrar') {


            $validator = Validator::make($request->all(), Proveedor_Modelo::rules(), Proveedor_Modelo::messages());

            if( $validator ->fails() ) {
                return redirect()->back()->withInput()->withErrors($validator->errors());
			}
    	$empresa = $request->input('empresa');
			$nombre = $request->input('nombre');
			$apellidopat = $request->input('apellidopat');
			$apellidomat = $request->input('apellidomat');
			$telefono = $request->input('telefono');
			$rfc = $request->input('rfc');
			$correo = $request->input('correo');

			$calle = $request->input('calle');
			$numero = $request->input('numero');
			$codigop = $request->input('codigop');
			$colonia = $request->input('colonia');
			$municipio = $request->input('municipio');
			$ciudad = $request->input('ciudad');

			$estado = 1;

			Proveedor_Modelo::create( [ 'empresa' => $empresa,'nombre' => $nombre,  'apellidopat' => $apellidopat,
				  'apellidomat' => $apellidomat,  'telefono' => $telefono,
				  'rfc' => $rfc,  'correo' => $correo, 'calle' => $calle,
				  'numero' => $numero, 'codigop' => $codigop, 'colonia' => $colonia,
				  'municipio' => $municipio, 'ciudad' => $ciudad, 'estado' => $estado ] );


			Session::flash('success', ' Se ha registrado exitosamente al proveedor ');

			return redirect()->back();

		} else if( $request->botonformproveedor == 'Cancelar') {

            return redirect()->to('home');

		}

	}

    	//Funciones para editar información

	public function modificarproveedor( $idProveedor ) {

		$proveedor = Proveedor_Modelo::select('*')->where('idProveedor', $idProveedor)->where('estado', 1)->take(1)->first();

		return view('vistasproveedores/modificarproveedor') -> with('proveedor',$proveedor);

    }
    
    public function modificarproveedorrfc( $rfc ) {
        
        $proveedor = Proveedor_Modelo::where('rfc', $rfc )->where('estado', 1)->get();

        if( count( $proveedor ) == 1 ) {   

            $proveedor = Proveedor_Modelo::select('*')->where('rfc', $rfc)->where('estado', 1)->take(1)->first();

            return view('vistasproveedores/modificarproveedor') -> with('proveedor',$proveedor);

        } else {

            Session::flash('warning', ' No existe proveedor alguno registrado con ese RFC ');
            return redirect()->back();

        }

	}

	public function actualizarinfoproveedor( Request $request, $idProveedor ) {

		if( $request->botonformmodificar == 'Modificar' ) {
      DB::table('proveedor')
      ->where('idProveedor',$idProveedor)  // find your user by their email
      ->limit(1)  // optional - to ensure only one record is updated.
      ->update( array('Nombre' => $request->nombre,
                        'ApellidoMat' => $request->apellidomat,
                        'ApellidoPat' => $request->apellidopat,
                        'Telefono' => $request->telefono,
                       'Correo' => $request->correo,
                       'Calle' => $request->calle,
                       'Numero' => $request->numero,
                       'CodigoP' => $request->codigop,
                       'Colonia' => $request->colonia,
                       'Municipio' => $request->municipio,
                     'Ciudad' => $request->ciudad) );  // update the record in the DB.

      return redirect()->to('listadoproveedores');


		} else if( $request->botonformmodificar == 'Cancelar' ) {

            return redirect()->to('home');

		}
	}

		//Funciones para descontinuar proveedor

	public function descontinuarproveedor( $idProveedor ) {

        $proveedor = Proveedor_Modelo::select('*')
                                        ->where('idProveedor', $idProveedor)
                                        ->where('estado', 1)
                                        ->take(1)->first();

        return view('vistasproveedores/descontinuarproveedor') -> with('proveedor',$proveedor);

    }

    public function descontinuarproveedorrfc( $rfc ) {
        
        $proveedor = Proveedor_Modelo::where('rfc', $rfc )->where('estado', 1)->get();

        if( count( $proveedor ) == 1 ) {   

            $proveedor = Proveedor_Modelo::select('*')
                                        ->where('rfc', $rfc)
                                        ->where('estado', 1)
                                        ->take(1)->first();

            return view('vistasproveedores/descontinuarproveedor') -> with('proveedor',$proveedor);

        } else {

            Session::flash('warning', ' No existe proveedor alguno registrado con ese RFC ');
            return redirect()->back();

        }

	}

	public function cambiarestadoproveedor( Request $request, $idproveedor ) {

		if( $request->botonformdescontinuar == 'Descontinuar' ) {

            DB::table('proveedor')
                ->where('idProveedor',$idproveedor)  // find your user by their email
                ->update( array( 'Estado' => 0 ) );  // update the record in the DB.

            return redirect()->to('listadoproveedores');

		} else if( $request->botonformdescontinuar == 'Cancelar' ) {

            return redirect()->to('home');

		}
    }

    //Cambiar el estado del proveedor y el estado de todos sus productos
    public function eliminarcambiarestadoproveedor( $idProveedor ) {

        DB::table( 'proveedor' )
                ->where( 'idProveedor', '=', $idProveedor )
                ->update( array( 'Estado' => 0 ) );

        DB::table( 'producto' )
                ->where( 'producto.idproveedor', '=', $idProveedor )
                ->update( array( 'producto.Estado' => 0 ) );


        return redirect()->to('listadoproveedores');


    }

    //Cambiar estado del proveedor y transferir sus productos al proveedor por default
    public function transferircambiarestadoproveedor( $idProveedor ) {

        DB::table( 'proveedor' )
                ->where( 'idProveedor', '=', $idProveedor )
                ->update( array( 'Estado' => 0 ) );

        DB::table( 'producto' )
                ->where( 'producto.idproveedor', '=', $idProveedor )
                ->update( array( 'producto.idproveedor' => 1 ) );


        return redirect()->to('listadoproveedores');

    }

        //Funciones para registrar entrega de pedido

    public function registrarentregapedido() {

        /*
        $proveedores = Proveedor_Modelo::select("idproveedor", DB::raw("CONCAT(proveedor.nombre,' ',
                        proveedor.apellidopat,' ',proveedor.apellidomat) as nombrecompleto"))
                        ->where('estado','=','1')
                        ->pluck('nombrecompleto', 'idproveedor')->toArray();

        return view('vistasproveedores/registroentregapedido') ->with('proveedores', $proveedores );
        */
        return view('vistasproveedores/registroentregapedido');

    }

    public function registrardevolucioncompra() {

        return view('vistasproveedores\registrodevolucioncompra');

    }


                //Funcion API
    public function filtrarproveedorespornombre( $nombre ) {

        return response()->json( $proveedor = Proveedor_Modelo::select("idproveedor", DB::raw("CONCAT(proveedor.nombre,' ',
                        proveedor.apellidopat,' ',proveedor.apellidomat) as nombrecompleto"))
                       ->where('proveedor.nombre','LIKE','%'.$nombre.'%')->get());


    }

    public function insertarentregapedido( Request $request ) {

        $info_array = json_decode( $request->TableData );
        $idempleado = Auth::User()->idEmpleado;
        //$idempleado = 1;
        $fecha = Carbon::now();
        //$total = json_decode( $request->Total );
        $estado = 0;

        $idtipomovimiento = 101;
        $descripcion = "Ingreso por compra a proveedor";

        //Se recorre el array de productos de la tabla html
        foreach( $info_array as $k ) {

            $idproducto = $k->{'ColumnaCodigos'};
            $cantidad = $k->{'ColumnaCantidades'};
            $preciocom = $k->{'ColumnaPrecioCom'};
            $precioven = $k->{'ColumnaPrecioVen'};

            

            $consultaidproveedor = DB::table('producto')->select( 'idproveedor' )
            ->where('codigo','=', $idproducto)
            ->get();

            $idproveedor = $consultaidproveedor[0]->{'idproveedor'};
            
            $total = bcadd( ( $preciocom * $cantidad ), '0', 2 );

            //Registrar el pedido
            DB::insert('insert into pedido( idempleado, idproveedor, fecha, total, estado )
                        values( ?, ?, ?, ?, ? )', [ $idempleado, $idproveedor, $fecha, $total, $estado ] );



            //Se hace un consulta para saber si hay algún producto con ese código en la tabla inventario
            $cons = DB::table('inventario')->select( DB::raw( "COUNT( codigo ) AS total" ))
            ->where('codigo','=', $idproducto)
            ->get();

            //Se obtiene el total de la consulta
            foreach( $cons as $k ) {
                $totalproductos = $k->{'total'};
            }

            //Se verifica si es igual a 0, lo que significa que no hay producto registrado en la tabla de inventario
            //y se tiene que crear su registro
            if( $totalproductos == 0 ) {
                

                $consultaproductostockmin = DB::table('producto')
                                        ->select( 'stockminimo', 'preciocosto', 'precioventa' )
                                        ->where('codigo','=', $idproducto)
                                        ->get();
                                
                $stockminimo = $consultaproductostockmin[0]->{'stockminimo'};                 

                DB::table('producto')
                ->where('codigo', $idproducto)
                ->limit(1)
                ->update( array( 'PrecioVenta' => $precioven, 'PrecioCosto' => $preciocom ) );

                $gananciasxpz = 0.0;
                $gananciastot = 0.0;

                DB::insert('insert into inventario ( codigo, cantidad, preciocom, precioven, gananciasxpz, gananciastot, stockminimo ) 
                            values ( ?, ?, ?, ?, ?, ?, ? )', [ $idproducto, $cantidad, $preciocom, $precioven, $gananciasxpz, $gananciastot, $stockminimo ] );

                $cantidadant = 0;
                $cantidadact = $cantidad;
                $cantidadmov = $cantidad;

                $preciocostoact = $preciocom;
                $precioventaact = $precioven;

                $preciocostoant = 0.0;
                $precioventaant = 0.0;

                DB::insert('insert into movimientoinventario( idTipoMovimiento , idempleado, codigo, fechahora, 
                            descripcion, cantidadant, cantidadact, cantidadmov, preciocostoant, preciocostoact, precioventaant, precioventaact ) 
                            values( ?, ?, ?, ?, ?, ?, ? ,?, ?, ?, ?, ? )',
                                [ $idtipomovimiento, $idempleado, $idproducto, $fecha,
                                $descripcion, $cantidadant, $cantidadact, $cantidadmov, 
                                $preciocostoant, $preciocostoact, $precioventaant, $precioventaact ] );


            } else {

                $consultaproductostockmin = DB::table('producto')
                                        ->select( 'stockminimo' )
                                        ->where('codigo','=', $idproducto)
                                        ->get();
                                
                $stockminimo = $consultaproductostockmin[0]->{'stockminimo'};

            

                //Se obtienen los datos del producto registrado en la tabla inventario
                $productoinventario = DB::table('inventario')
                                            ->select( 'cantidad', 'preciocom', 'precioven' )
                                                   ->where('codigo','=', $idproducto)
                                                   ->get();          

                //Se obtienen los atributos del objeto productoinventario                
                $cantidadant = $productoinventario[0]->{'cantidad'};
                $cantidadact = $cantidadant + $cantidad;
                $cantidadmov = $cantidad;                


                //Calcular los nuevos precios de compra y de venta                    
                $preciocostoant = $productoinventario[0]->{'preciocom'};
                $precioventaant = $productoinventario[0]->{'precioven'};
            
                DB::insert('insert into movimientoinventario( idTipoMovimiento , idempleado, codigo, fechahora, 
                descripcion, cantidadant, cantidadact, cantidadmov, preciocostoant, preciocostoact, precioventaant, precioventaact ) 
                values( ?, ?, ?, ?, ?, ?, ? ,?, ?, ?, ?, ? )',
                    [ $idtipomovimiento, $idempleado, $idproducto, $fecha,
                    $descripcion, $cantidadant, $cantidadact, $cantidadmov, 
                    $preciocostoant, $preciocom, $precioventaant, $precioven ] );

                                    
                $consultamovimientoinventarioentradasprecios = DB::table('movimientoinventario')
                                                ->select(   DB::raw('Sum(PrecioCostoAct *CantidadMov) as debe'),
                                                            DB::raw('Sum(PrecioVentaAct*CantidadMov) as obtenido')  )
                                                            ->where( 'codigo', '=', $idproducto )
                                                            //->where( 'idtipomovimiento', '=', $idtipomovimiento )
                                                            ->where('descripcion', 'like', '%ingreso%')
                                                            ->get();
                                                            
                $consultamovimientoinventariosalidasprecios = DB::table('movimientoinventario')
                                                ->select(   DB::raw('Sum(PrecioCostoAct *CantidadMov) as debe'),
                                                            DB::raw('Sum(PrecioVentaAct*CantidadMov) as obtenido')  )
                                                            ->where( 'codigo', '=', $idproducto )
                                                            //->where( 'idtipomovimiento', '=', $idtipomovimiento )
                                                            ->where('descripcion', 'like', '%salida%')
                                                            ->get();                                                            


                $sumaentradatotalpreciocompra = $consultamovimientoinventarioentradasprecios[0]->{'debe'};
                echo "TOTALPRECIOCOMPRA: ". $sumaentradatotalpreciocompra;

                $sumaentradatotalprecioventa = $consultamovimientoinventarioentradasprecios[0]->{'obtenido'};
                echo "sumatotalprecioventa: ". $sumaentradatotalprecioventa;

                $sumasalidatotalpreciocompra = $consultamovimientoinventariosalidasprecios[0]->{'debe'};
                $sumasalidatotalprecioventa = $consultamovimientoinventariosalidasprecios[0]->{'obtenido'};

                $totalpreciocompra = $sumaentradatotalpreciocompra - $sumasalidatotalpreciocompra;
                $totalprecioventa = $sumaentradatotalprecioventa - $sumasalidatotalprecioventa;

                $nuevopreciocompra = bcadd( ( $totalpreciocompra / $cantidadact ) , '0', 2 );
                echo "nuevopreciocompra: ". $nuevopreciocompra;

                $nuevoprecioventa = bcadd( ( $totalprecioventa / $cantidadact ), '0', 2 );
                echo "nuevoprecioventa: ". $nuevoprecioventa;

                //$gananciastot = bcadd( ( $totalprecioventa - $totalpreciocompra ), '0', 2 );
                //$gananciaxpz = bcadd( ( $gananciastot / $cantidadact ), '0', 2 );

                $gananciaxpz = 0.0;
                $gananciastot = 0.0;


                DB::table('producto')
                    ->where( 'codigo', $idproducto )
                    ->limit(1)
                    ->update( array( 'PrecioVenta' => $nuevoprecioventa, 'PrecioCosto' => $nuevopreciocompra ) );

                    //Actualizar la cantidad de existencias y precios del producto en inventario
                DB::table('inventario')
                    ->where( 'codigo', $idproducto )
                    ->limit(1)
                    ->update( array(    'Cantidad' => $cantidadact, 'PrecioCom' => $nuevopreciocompra, 'PrecioVen' => $nuevoprecioventa,
                                        'Gananciasxpz' => $gananciaxpz, 'Gananciastot' => $gananciastot,  'Stockminimo' => $stockminimo ) );
                                        

                $this->verificaryactualizarpreciospromociones( $idproducto );

            }

            //Obtener el id de la última venta
            $consulta = MovimientoInventario_Modelo::select('idMovimientoInventario')
                                            ->orderBy('idMovimientoInventario', 'desc')
                                            ->limit(1)
                                            ->get();

            foreach( $consulta as $k ) {
                $idMovimientoInventario = $k->{'idMovimientoInventario'};
            }

            

            //Obtener el id del último pedido
            $consultaidpedido = Pedido_Modelo::select('idPedido')
                                            ->orderBy('idPedido', 'desc')
                                            ->limit(1)
                                            ->get();
            
            $idpedido = $consultaidpedido[0]->{'idPedido'};

            DB::insert('insert into detallepedido ( idPedido, idMovimientoInventario, Codigo, Cantidad )
                values (?, ?, ?, ?)', [ $idpedido, $idMovimientoInventario, $idproducto, $cantidad ] );

        }

    }

    public function verificaryactualizarpreciospromociones( $codigo ) {

        echo "\NENTRA A METODO verificaryactualizarpreciospromociones";

        $consultaproducto = DB::table('producto')
        ->select('precioventa as precioventa' )
        ->where('codigo', '=', $codigo )
        ->get();

        $precioproducto = $consultaproducto[0]->{'precioventa'};
        
        $consultaverificaroferta = DB::table('oferta')
            ->join('promociones', 'oferta.idpromociones', '=', 'promociones.idpromociones')
            ->select( DB::raw("COUNT(oferta.codigo) as total" ) )            
            ->where('oferta.codigo', '=', $codigo )
            ->where('promociones.estado', '=', 1 )
            ->get();

        $totalpromocionoferta = $consultaverificaroferta[0]->{'total'};

        if( $totalpromocionoferta > 0 ) {

            echo "\NENTRA A IF OFERTA";

            $consultainfooferta = DB::table('oferta')
                        ->join('promociones', 'oferta.idpromociones', '=', 'promociones.idpromociones')
                        ->select(  'promociones.idpromociones as idpromociones', 'oferta.idoferta as idoferta',
                         'oferta.tipo as tipooferta' )
                        ->where('oferta.codigo', '=', $codigo )
                        ->where('promociones.estado', '=', 1 )
                        ->get();

            $tipooferta = $consultainfooferta[0]->{'tipooferta'};
            $idpromociones = $consultainfooferta[0]->{'idpromociones'};
            $idoferta = $consultainfooferta[0]->{'idoferta'};


            if( $tipooferta = 1 ) {

                $preciototalpromocion = bcadd( ( $precioproducto * $tipooferta ) , '0', 2);
                $preciooferta = bcadd( ( $preciototalpromocion / 2 ) , '0', 2 );

            } else if( $tipooferta = 2 ) {

                $preciototalpromocion = bcadd( ( $precioproducto * $tipooferta ) , '0', 2);
                $preciooferta = bcadd( ( $preciototalpromocion / 3 ) , '0', 2 );

            } else if( $tipooferta = 3 ) {

                $preciototalpromocion = bcadd( ( $precioproducto * $tipooferta ) , '0', 2);
                $preciooferta = bcadd( ( $preciototalpromocion / 4 ) , '0', 2 );

            } else if( $tipooferta = 4 ) {

                $preciototalpromocion = bcadd( ( $precioproducto * $tipooferta ) , '0', 2);
                $preciooferta = bcadd( ( $preciototalpromocion / 5 ) , '0', 2 );

            }

            DB::table('promociones')
                ->where( 'idpromociones', $idpromociones )
                ->limit(1)
                ->update( array(    'Total' => $preciototalpromocion ) );

            DB::table('oferta')
                ->where( 'idoferta', $idoferta )
                ->limit(1)
                ->update( array(    'Precionuevo' => $preciooferta ) );
            
        } else if( $totalpromocionoferta == 0 ) {

            echo "\NENTRA A ELSEIF OFERTA";

            $consultaverificardescuento = DB::table('descuento')
            ->join('promociones', 'descuento.idpromociones', '=', 'promociones.idpromociones')
            ->select( DB::raw("COUNT(descuento.codigo) as total" ) )            
            ->where('descuento.codigo', '=', $codigo )
            ->where('promociones.estado', '=', 1 )
            ->get();

            $totalpromociondescuento = $consultaverificardescuento[0]->{'total'};


            if( $totalpromociondescuento > 0 ) {

                echo "\NENTRA A IF DESCUENTO";

                $consultainfodescuento = DB::table('descuento')
                            ->join('promociones', 'descuento.idpromociones', '=', 'promociones.idpromociones')
                            ->select(  'promociones.idpromociones as idpromociones', 'descuento.iddescuento as iddescuento',
                             'descuento.descuento as porcentajedescuento' )
                            ->where('descuento.codigo', '=', $codigo )
                            ->where('promociones.estado', '=', 1 )
                            ->get();
    
                $porcentajedescuento = $consultainfodescuento[0]->{'porcentajedescuento'};
                $idpromociones = $consultainfodescuento[0]->{'idpromociones'};
                $iddescuento = $consultainfodescuento[0]->{'iddescuento'};

                $descuento = bcadd( ( ( $precioproducto * $porcentajedescuento ) / 100 ), '0', 2 );
                $preciodescuento = bcadd( ( $precioproducto - $descuento ) , '0', 2 );

                DB::table('promociones')
                    ->where( 'idpromociones', $idpromociones )
                    ->limit(1)
                    ->update( array(    'Total' => $preciodescuento ) );
    
                DB::table('descuento')
                    ->where( 'iddescuento', $iddescuento )
                    ->limit(1)
                    ->update( array(    'Precionuevo' => $preciodescuento ) );

            } else if( $totalpromociondescuento == 0 ) {

                echo "\NENTRA A ELSEIF OFERTA";

                $consultaverificarpaquete1 = DB::table('paquetes')
                ->join('promociones', 'paquetes.idpromociones', '=', 'promociones.idpromociones')
                ->select( DB::raw("COUNT(paquetes.codigo1) as total" ) )            
                ->where('paquetes.codigo1', '=', $codigo )
                ->where('promociones.estado', '=', 1 )
                ->get();

                $totalpromocionpaquete1 = $consultaverificarpaquete1[0]->{'total'};

                if( $totalpromocionpaquete1 > 0 ) {

                    echo "\NENTRA A IF PAQUETE1";

                    $consultainfopaquete1 = DB::table('paquetes')
                    ->join('promociones', 'paquetes.idpromociones', '=', 'promociones.idpromociones')
                    ->select( 'promociones.idpromociones as idpromociones', 'paquetes.idpaquetes as idpaquetes',
                    'paquetes.descuento1 as porcentajedescuento1', 'paquetes.precio2 as preciocomplementario' )            
                    ->where('paquetes.codigo1', '=', $codigo )
                    ->where('promociones.estado', '=', 1 )
                    ->get();
    
                    $porcentajedescuento1 = $consultainfopaquete1[0]->{'porcentajedescuento1'};
                    $idpromociones = $consultainfopaquete1[0]->{'idpromociones'};
                    $idpaquetes = $consultainfopaquete1[0]->{'idpaquetes'};
                    $preciocomplementario = $consultainfopaquete1[0]->{'preciocomplementario'};
    
                    $descuento = bcadd( ( ( $precioproducto * $porcentajedescuento1 ) / 100 ), '0', 2 );
                    $preciodescuentopaquete = bcadd( ( $precioproducto - $descuento ) , '0', 2 );

                    $nuevopreciopromocion = bcadd( ( $preciocomplementario + $preciodescuentopaquete ) , '0', 2 );

                    DB::table('promociones')
                        ->where( 'idpromociones', $idpromociones )
                        ->limit(1)
                        ->update( array(    'Total' => $nuevopreciopromocion ) );
        
                    DB::table('paquetes')
                        ->where( 'idpaquetes', $idpaquetes )
                        ->limit(1)
                        ->update( array(    'Precio1' => $preciodescuentopaquete ) );

                } else if( $totalpromocionpaquete1 == 0 ) {

                    echo "\NENTRA A ELSEIF PAQUETE1";

                    $consultaverificarpaquete2 = DB::table('paquetes')
                    ->join('promociones', 'paquetes.idpromociones', '=', 'promociones.idpromociones')
                    ->select( DB::raw("COUNT(paquetes.codigo2) as total" ) )            
                    ->where('paquetes.codigo2', '=', $codigo )
                    ->where('promociones.estado', '=', 1 )
                    ->get();
    
                    $totalpromocion2 = $consultaverificarpaquete2[0]->{'total'};

                    if( $totalpromocion2 > 0 ) {

                        echo "\NENTRA A IF PAQUETE2";

                        $consultainfopaquete2 = DB::table('paquetes')
                        ->join('promociones', 'paquetes.idpromociones', '=', 'promociones.idpromociones')
                        ->select( 'promociones.idpromociones as idpromociones', 'paquetes.idpaquetes as idpaquetes',
                        'paquetes.descuento2 as porcentajedescuento2', 'paquetes.precio1 as preciocomplementario' )            
                        ->where('paquetes.codigo2', '=', $codigo )
                        ->where('promociones.estado', '=', 1 )
                        ->get();
        
                        $porcentajedescuento2 = $consultainfopaquete2[0]->{'porcentajedescuento2'};
                        $idpromociones = $consultainfopaquete2[0]->{'idpromociones'};
                        $idpaquetes = $consultainfopaquete2[0]->{'idpaquetes'};
                        $preciocomplementario = $consultainfopaquete2[0]->{'preciocomplementario'};
        
                        $descuento = bcadd( ( ( $precioproducto * $porcentajedescuento2 ) / 100 ), '0', 2 );
                        $preciodescuentopaquete = bcadd( ( $precioproducto - $descuento ) , '0', 2 );
                        
                        $nuevopreciopromocion = bcadd( ( $preciocomplementario + $preciodescuentopaquete ) , '0', 2 );
    
                        DB::table('promociones')
                            ->where( 'idpromociones', $idpromociones )
                            ->limit(1)
                            ->update( array(    'Total' => $nuevopreciopromocion ) );
            
                        DB::table('paquetes')
                            ->where( 'idpaquetes', $idpaquetes )
                            ->limit(1)
                            ->update( array(    'Precio2' => $preciodescuentopaquete ) );

                    }

                }

            }

        }


    }

    public function insertardevolucioncompra( Request $request ) {

        $info_array = json_decode( $request->TableData );
        $idempleado = Auth::User()->idEmpleado;
        //$idempleado = 1;
        $fecha = Carbon::now();

        $idtipomovimiento = 201;
        $descripcion = "Salida por devolucion de compra proveedor";        

        //Se recorre el array de productos de la tabla html
        foreach( $info_array as $k ) {

            $idproducto = $k->{'ColumnaCodigos'};
            $cantidad = $k->{'ColumnaCantidades'};
            $preciocom = $k->{'ColumnaPrecioCom'};
            $precioven = $k->{'ColumnaPrecioVen'};

            


            $consultaidproveedor = DB::table('producto')->select( 'idproveedor' )
            ->where('codigo','=', $idproducto)
            ->get();
                
            $idproveedor = $consultaidproveedor[0]->{'idproveedor'};
    
            //Registrar la devolución
            DB::insert('insert into devoluciones(  idproveedor, idempleado, fecha )
                        values( ?, ?, ? )', [ $idproveedor, $idempleado, $fecha ] );
            

            //Se hace un consulta para saber si hay algún producto con ese código en la tabla inventario
            $cons = DB::table('inventario')->select( DB::raw( "COUNT( codigo ) AS total" ))
            ->where('codigo','=', $idproducto)
            ->get();

            //Se obtiene el total de la consulta
            foreach( $cons as $k ) {
                $totalproductos = $k->{'total'};
            }

            //Se verifica si es igual a 0, lo que significa que no hay producto registrado en la tabla de inventario
            //y se tiene que crear su registro
            if( $totalproductos == 0 ) {
                

                $consultaproductostockmin = DB::table('producto')
                                        ->select( 'stockminimo', 'preciocosto', 'precioventa' )
                                        ->where('codigo','=', $idproducto)
                                        ->get();
                                
                $stockminimo = $consultaproductostockmin[0]->{'stockminimo'};                 

                DB::table('producto')
                ->where('codigo', $idproducto)
                ->limit(1)
                ->update( array( 'PrecioVenta' => $precioven, 'PrecioCosto' => $preciocom ) );


                $gananciasxpz = 0.0;
                $gananciastot = 0.0;

                DB::insert('insert into inventario ( codigo, cantidad, preciocom, precioven, gananciasxpz, gananciastot, stockminimo ) 
                            values ( ?, ?, ?, ?, ?, ?, ? )', [ $idproducto, $cantidad, $preciocom, $precioven, $gananciasxpz, $gananciastot, $stockminimo ] );

                $cantidadant = 0;
                $cantidadact = $cantidad;
                $cantidadmov = $cantidad;

                $preciocostoact = $preciocom;
                $precioventaact = $precioven;

                $preciocostoant = 0.0;
                $precioventaant = 0.0;

                DB::insert('insert into movimientoinventario( idTipoMovimiento , idempleado, codigo, fechahora, 
                            descripcion, cantidadant, cantidadact, cantidadmov, preciocostoant, preciocostoact, precioventaant, precioventaact ) 
                            values( ?, ?, ?, ?, ?, ?, ? ,?, ?, ?, ?, ? )',
                                [ $idtipomovimiento, $idempleado, $idproducto, $fecha,
                                $descripcion, $cantidadant, $cantidadact, $cantidadmov, 
                                $preciocostoant, $preciocostoact, $precioventaant, $precioventaact ] );


            } else {

                $consultaproductostockmin = DB::table('producto')
                                        ->select( 'stockminimo' )
                                        ->where('codigo','=', $idproducto)
                                        ->get();
                                
                $stockminimo = $consultaproductostockmin[0]->{'stockminimo'};

            

                //Se obtienen los datos del producto registrado en la tabla inventario
                $productoinventario = DB::table('inventario')
                                            ->select( 'cantidad', 'preciocom', 'precioven' )
                                                   ->where('codigo','=', $idproducto)
                                                   ->get();          

                //Se obtienen los atributos del objeto productoinventario                
                $cantidadant = $productoinventario[0]->{'cantidad'};
                $cantidadact = $cantidadant - $cantidad;
                $cantidadmov = $cantidad;                


                //Calcular los nuevos precios de compra y de venta                    
                $preciocostoant = $productoinventario[0]->{'preciocom'};
                $precioventaant = $productoinventario[0]->{'precioven'};
            
                DB::insert('insert into movimientoinventario( idTipoMovimiento , idempleado, codigo, fechahora, 
                descripcion, cantidadant, cantidadact, cantidadmov, preciocostoant, preciocostoact, precioventaant, precioventaact ) 
                values( ?, ?, ?, ?, ?, ?, ? ,?, ?, ?, ?, ? )',
                    [ $idtipomovimiento, $idempleado, $idproducto, $fecha,
                    $descripcion, $cantidadant, $cantidadact, $cantidadmov, 
                    $preciocostoant, $preciocom, $precioventaant, $precioven ] );

                                    
                $consultamovimientoinventarioentradasprecios = DB::table('movimientoinventario')
                                                ->select(   DB::raw('Sum(PrecioCostoAct *CantidadMov) as debe'),
                                                            DB::raw('Sum(PrecioVentaAct*CantidadMov) as obtenido')  )
                                                            ->where( 'codigo', '=', $idproducto )
                                                            //->where( 'idtipomovimiento', '=', $idtipomovimiento )
                                                            ->where('descripcion', 'like', '%ingreso%')
                                                            ->get();
                                                            
                $consultamovimientoinventariosalidasprecios = DB::table('movimientoinventario')
                                                ->select(   DB::raw('Sum(PrecioCostoAct *CantidadMov) as debe'),
                                                            DB::raw('Sum(PrecioVentaAct*CantidadMov) as obtenido')  )
                                                            ->where( 'codigo', '=', $idproducto )
                                                            //->where( 'idtipomovimiento', '=', $idtipomovimiento )
                                                            ->where('descripcion', 'like', '%salida%')
                                                            ->get();                                                            


                $sumaentradatotalpreciocompra = $consultamovimientoinventarioentradasprecios[0]->{'debe'};
                echo "sumaentradatotalpreciocompra: ". $sumaentradatotalpreciocompra;

                $sumaentradatotalprecioventa = $consultamovimientoinventarioentradasprecios[0]->{'obtenido'};
                echo "sumaentradatotalprecioventa: ". $sumaentradatotalprecioventa;

                $sumasalidatotalpreciocompra = $consultamovimientoinventariosalidasprecios[0]->{'debe'};
                echo "sumasalidatotalpreciocompra: ". $sumaentradatotalpreciocompra;

                $sumasalidatotalprecioventa = $consultamovimientoinventariosalidasprecios[0]->{'obtenido'};
                echo "sumasalidatotalprecioventa: ". $sumaentradatotalprecioventa;

                $totalpreciocompra = $sumaentradatotalpreciocompra - $sumasalidatotalpreciocompra;
                echo "nuevopreciocompra: ". $totalpreciocompra;

                $totalprecioventa = $sumaentradatotalprecioventa - $sumasalidatotalprecioventa;
                echo "nuevopreciocompra: ". $totalprecioventa;

                $nuevopreciocompra = 0.0;
                $nuevoprecioventa = 0.0;
 
                if( $cantidadact > 0 ) {
 
                     $nuevopreciocompra = bcadd( ( $totalpreciocompra / $cantidadact ) , '0', 2 );
                     echo "nuevopreciocompra: ". $nuevopreciocompra;
 
                     $nuevoprecioventa = bcadd( ( $totalprecioventa / $cantidadact ), '0', 2 );
                     echo "nuevoprecioventa: ". $nuevoprecioventa;
 
                } else {
 
                     $nuevopreciocompra = $preciocom;
                     $nuevoprecioventa = $precioven;
 
                }

                //$gananciastot = bcadd( ( $totalprecioventa - $totalpreciocompra ), '0', 2 );
                //$gananciaxpz = bcadd( ( $gananciastot / $cantidadact ), '0', 2 );

                //$gananciaxpz = 0.0;
                //$gananciastot = 0.0;


                DB::table('producto')
                    ->where( 'codigo', $idproducto )
                    ->limit(1)
                    ->update( array( 'PrecioVenta' => $nuevoprecioventa, 'PrecioCosto' => $nuevopreciocompra ) );

                    //Actualizar la cantidad de existencias y precios del producto en inventario
                /*
                    DB::table('inventario')
                    ->where( 'codigo', $idproducto )
                    ->limit(1)
                    ->update( array(    'Cantidad' => $cantidadact, 'PrecioCom' => $nuevopreciocompra, 'PrecioVen' => $nuevoprecioventa,
                                        'Gananciasxpz' => $gananciaxpz, 'Gananciastot' => $gananciastot,  'Stockminimo' => $stockminimo ) );
                */
                DB::table('inventario')
                ->where( 'codigo', $idproducto )
                ->limit(1)
                ->update( array(    'Cantidad' => $cantidadact, 'PrecioCom' => $nuevopreciocompra, 'PrecioVen' => $nuevoprecioventa,
                                    'Stockminimo' => $stockminimo ) );

            }

            //Obtener el id de la última venta
            $consulta = MovimientoInventario_Modelo::select('idMovimientoInventario')
                                            ->orderBy('idMovimientoInventario', 'desc')
                                            ->limit(1)
                                            ->get();

            foreach( $consulta as $k ) {
                $idMovimientoInventario = $k->{'idMovimientoInventario'};
            }

            
    
            //Obtener el id de la última devolución
            $consultaiddevolucion = Devoluciones_Modelo::select('idDevoluciones')
                                            ->orderBy('idDevoluciones', 'desc')
                                            ->limit(1)
                                            ->get();

            $iddevoluciones = $consultaiddevolucion[0]->{'idDevoluciones'};

            DB::insert('insert into detalledevoluciones ( idDevoluciones, idMovimientoInventario, Codigo, Cantidad )
                values (?, ?, ?, ?)', [ $iddevoluciones, $idMovimientoInventario, $idproducto, $cantidad ] );

       }

    }

    public function verificarproductosproveedor( $idproveedor ) {

        return response()->json( Producto_Modelo::select( DB::raw( "COUNT( codigo ) AS total" ) )
                                            ->where('idproveedor', '=', $idproveedor )
                                            ->get() );

    }

}
