<?php

namespace App\Http\Controllers\ProyectoControllers\ImportarExportar;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Session;
use DB;
use Response;
use App\Models\Producto_Modelo;
use Exception;
use Importer;

//use Maatwebsite\Excel\Facades\Excel;

class ImportarController extends Controller
{


    public function importardatos() {

        return view('vistasajustes/vistasimportar/importardatos');

    }
    
    //Bussinescontrol
    public function importardatosproductosbusinesscontrol() {
        
        return view('vistasajustes/vistasimportar/importardatosproductosbusinesscontrol');
        
    }

    public function guardardatosproductosbusinesscontrol( Request $request ) {
        
        if( $request->botonformimportarproductos == 'Importar' ) {

            if( !$request->hasFile('rutaarchivo') ) {
                                                
                Session::flash('danger', ' Hay un problema al abrir el archivo ');

                return redirect()->back();
    
            } else {
                            
                $file = $request->file('rutaarchivo');
                $nombre =  $file->getClientOriginalName();

                $imagen = 'public/Productos/ProductoDefault.png';

                /*
                    1 => INICIO     2 => CONFIG     3 => SALIDAS
                    4 => CLIENTES     5 => PRODUCTOS     6 => CAMBIO
                    7 => FACTURA     8 => TICKET     9 => PROMOCION
                */

                //$sheetNumber = 8;
                $sheetNumber = 5;

                try {
                    
                    $excel = Importer::make('Excel');
                    $excel->load( $file );
                    $excel->setSheet( $sheetNumber );
                    $collection = $excel->getCollection();
                    
                    //dd( "Col: ", $collection );
                    
                    $tamanocollection = sizeof( $collection );

                    //El archivo sólo tiene la cabecera en la hoja de productos
                    if( $tamanocollection == 1 ) {
                                                    
                        Session::flash('danger', ' No se encontraron datos que importar ');
        
                        return redirect()->back();
    
                    } else {
    
                        //Tiempo = 0, => tiempo indefinido/ilimitado. 300 seconds = 5 minutes
                        //set_time_limit( 300 );
                    
                        
                        //for( $index = 1; $index < 5; $index++ ) {
                        for( $index = 1; $index < $tamanocollection; $index++ ) {
                            
                            
                            //Comprobar si el código del producto aún no está almacenado en la base de datos.
                            $consultaproducto = DB::table('producto')->select( DB::raw( "COUNT( codigo ) AS total" ) )
                                        ->where( 'codigo', '=', $collection[$index][0] )
                                        ->get();
        
                            $total = $consultaproducto[0]->{'total'};
        
                            //El código no está registrado.
                            if( $total == 0 ) {
        
                                Producto_Modelo::create( [  
                                        'codigo' => $collection[$index][0], 
                                        'imagen' => $imagen,
                                        'descripcion' => $collection[$index][1],
                                        'idmarca' => 1, 
                                        'idpresentacion' => 1,
                                        'idproveedor' => 1, 
                                        'idcategoria' => 1,
                                        'preciocosto' => bcadd( $collection[$index][3], '0', 2), 
                                        'precioventa' => bcadd( $collection[$index][4], '0', 2),
                                        'imagen' => "Productos/ProductoDefault.png",
                                        'stockminimo' => 1,
                                        'iva' => 0, 
                                        'estado' => 1
                                    ] );
        
                            }                    
        
                            
                        }
                        
                        //Session::flash('success', ' Los datos se han importado correctamente ');
                        Session::flash('successimportar', ' Los datos se han importado correctamente ');
    
                        return redirect()->back();
    
                    }
                                    
                    //dd($collection); 

                } catch ( Exception $e ) {
                                                                
                    Session::flash('danger', ' No se ha encontrado la hoja productos dentro del archivo ');    

                    return redirect()->back();

                }
            }
            

        } else if( $request->botonformimportarproductos == 'Cancelar' ) {
            
            return redirect()->to('importardatos');

        }

    }

    //Para cualquier archivo Excel
    public function importardatosproductosexcel() {
        
        return view('vistasajustes/vistasimportar/importardatosproductosexcel');
        
    }

    public function guardardatosproductosexcel( Request $request ) {
        
        if( $request->botonformimportarproductos == 'Importar' ) {

            if( !$request->hasFile('rutaarchivo') ) {
                                                
                Session::flash('danger', ' Hay un problema al abrir el archivo ');

                return redirect()->back();
    
            } else {
                            
                $file = $request->file('rutaarchivo');
                $nombre =  $file->getClientOriginalName();

                $imagen = 'public/Productos/ProductoDefault.png';

                $sheetNumber = 1;

                try {
                    
                    $excel = Importer::make('Excel');
                    $excel->load( $file );
                    $excel->setSheet( $sheetNumber );
                    $collection = $excel->getCollection();                    
                                        
                    $tamanocollection = sizeof( $collection );

                    //El archivo sólo tiene la cabecera en la hoja de productos
                    if( $tamanocollection == 1 ) {
                                                    
                        Session::flash('danger', ' No se encontraron datos que importar ');
        
                        return redirect()->back();
    
                    } else {
    
                        //Tiempo = 0, => tiempo indefinido/ilimitado. 300 seconds = 5 minutes
                        //set_time_limit( 300 );
                    
                        for( $index = 1; $index < $tamanocollection; $index++ ) {
                            
                            
                            //Comprobar si el código del producto aún no está almacenado en la base de datos.
                            $consultaproducto = DB::table('producto')->select( DB::raw( "COUNT( codigo ) AS total" ) )
                                        ->where( 'codigo', '=', $collection[$index][0] )
                                        ->get();
        
                            $total = $consultaproducto[0]->{'total'};
        
                            //El código no está registrado.
                            if( $total == 0 ) {
        
                                Producto_Modelo::create( [  
                                        'codigo' => $collection[$index][0], 
                                        'imagen' => $imagen,
                                        'descripcion' => $collection[$index][1],
                                        'idmarca' => 1, 
                                        'idpresentacion' => 1,
                                        'idproveedor' => 1, 
                                        'idcategoria' => 1,
                                        'preciocosto' => bcadd( $collection[$index][3], '0', 2), 
                                        'precioventa' => bcadd( $collection[$index][4], '0', 2),
                                        'imagen' => "Productos/ProductoDefault.png",
                                        'stockminimo' => 1,
                                        'iva' => 0, 
                                        'estado' => 1
                                    ] );
        
                            }                    
        
                            
                        }
                        
                        //Session::flash('success', ' Los datos se han importado correctamente ');
                        Session::flash('successimportar', ' Los datos se han importado correctamente ');
    
                        return redirect()->back();
    
                    }
                                    
                    //dd($collection); 

                } catch ( Exception $e ) {
                                                                
                    Session::flash('danger', ' No se ha encontrado la hoja productos dentro del archivo ');    

                    return redirect()->back();

                }
            }
            

        } else if( $request->botonformimportarproductos == 'Cancelar' ) {
            
            return redirect()->to('importardatos');

        }

    }

}
