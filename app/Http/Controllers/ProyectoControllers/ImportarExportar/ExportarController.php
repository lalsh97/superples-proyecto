<?php

namespace App\Http\Controllers\ProyectoControllers\ImportarExportar;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Session;
use DB;
use Response;
use App\Models\Producto_Modelo;
use Exception;
use Exporter;
use Carbon\Carbon;

use App\Serialisers\ExampleSerialiser;

class ExportarController extends Controller
{

    public function exportardatos() {

        return view('vistasajustes/vistasexportar/exportardatos');

    }

    public function exportardatosproductos() {
                  
        $today = Carbon::now()->format('d-m-Y');

        $consultatotal = DB::table('producto')->select( DB::raw( "COUNT( producto.codigo ) AS total" ) )
            ->where( 'producto.estado', 1 )
            ->get();                        

        //SE OBTIENE LA COLUMNA TOTAL DE LA CONSULTA.
        $total = $consultatotal[0]->{'total'};

        if( $total ==  0 ) {
                        
            //Session::flash('success', ' Los datos se han importado correctamente ');
            Session::flash('danger', ' Aún no hay registros de productos que exportar ');

            return redirect()->back();

        } else {
            
            $consultaproductos = DB::table('producto')
            ->join('marca', 'producto.idmarca', '=', 'marca.idmarca')
            ->select( 'producto.codigo', 'producto.descripcion' ,'marca.descripcion as nombremarca',
                    'producto.preciocosto','producto.precioventa')
                    ->where('producto.estado',1)
                    ->orderBy('codigo','ASC');

            $datosexportarexcel = Exporter::make('Excel');
            $datosexportarexcel->loadQuery( $consultaproductos );
            $datosexportarexcel->setChunk(1000);
            //$datosexportarexcel->setSerialiser(new ExampleSerialiser);
            $datosexportarexcel->stream( 'DatosExportados-' . $today .'.xlsx' );

        }     

    }


    public function headings(): array
    {
        return [
            '#',
            'Name',
            'Email',
            'Created at',
            'Updated at'
        ];
    }

}
