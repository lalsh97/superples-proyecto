<?php

namespace App\Http\Controllers\ProyectoControllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Session;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;

use App\Models\Promocion_Modelo;
use App\Models\Producto_Modelo;
use App\Models\Oferta_Modelo;
use App\Models\Paquetes_Modelo;
use App\Models\Descuento_Modelo;


class PromocionesController extends Controller
{

    //Funcion para mostrar Actualizar
    public function listapromociones() {

        $promociones = Promocion_Modelo::where('estado','1')->get();
        
        return view ('vistaspromociones/listapromociones')->with('registropromociones', $promociones);

    }

    //Funcion para mostrar Actualizar
    public function listadopromociones() {

        $promociones = Promocion_Modelo::where('estado','1')->get();

        return view ('vistaspromociones/listadopromociones')->with('registropromociones', $promociones);

    }

    //Funcion para mostrar Eliminar
    public function listadopromociones2() {
      
        $promociones = Promocion_Modelo::where('estado','1')->get();

        return view ('vistaspromociones/listadopromociones2')->with('registropromociones', $promociones);

    }

    public function terminarpromocion( Request $request, $idPromociones ) {

        if( $request->botonformdescontinuar == 'Terminar' ) {

                DB::table('promociones')
                    ->where( 'idPromociones',$idPromociones )  // find your user by their email
                    ->update( array( 'Estado' => 0 ) );  // update the record in the DB.

                return redirect()->to('listadopromoterminar');

        } else if( $request->botonformdescontinuar == 'Cancelar' ) {

                return redirect()->to('home');

        }

    }

    //RUTA PARA ELIMINAR DESDE EL MODAL DE CONFIRMACION
    public function eliminarcambiarestadopromocion( $idPromociones ) {

            DB::table('promociones')
                ->where( 'idPromociones',$idPromociones )
                ->update( array( 'Estado' => 0 ) );

            return redirect()->to('home');

    }

          //OFERTA

      //Funciones para registrar promocion Oferta
    
    public function registrarpromocion2( $codigo ) {

        $producto = Producto_Modelo::select('*')
                      ->where( 'Codigo', $codigo )
                      ->take(1)->first();


        return view('vistaspromociones/registrarpromo')->with( 'producto', $producto );

    }

    public function insertarpromocion( Request $request ) {

        $name ="";

        if( $request->hasFile('avatar') ) {
            
              $file = $request->file( 'avatar' );
              $nombrepromocion = str_replace( "/", " ", $request -> input('nombre') );
              //$name = time().$file->getClientOriginalName();

              $name = 'Promociones/Ofertas/' . $nombrepromocion . "." . $request->file('avatar')->extension();
              $file->move( public_path().'/images/Promociones/Ofertas/', $nombrepromocion . "." . $request->file('avatar')->extension() );

        } else {
          
            $name = 'Promociones/Ofertas/OfertaDefault.png';

        }

        if( $request->botonformpromocion == 'Registrar') {

            $validator = Validator::make($request->all(), Promocion_Modelo::rules(), Promocion_Modelo::messages());

            if( $validator ->fails() ) {

                return redirect()->back()->withInput()->withErrors($validator->errors());

            }

            $nombre = $request->input('nombre');
            $descripcion = $request->input('descripcion');
            $fechaini = $request->input('fechaini');
            $fechafin = $request->input('fechafin');
            $total = $request->input('total');
            $codigo= $request->input('codigo');
            $tipo = $request->input('tipo');
            $estado = 1;
            $precionuevo= $request->input('nuevoprecio');

            DB::insert ('insert into promociones (Nombre, Descripcion, Fechaini, Fechafin,Total,Estado,Tipo,Imagen)
                values(?,?,?,?,?,?,?,?)', [$nombre, $descripcion, $fechaini, $fechafin, $total, $estado,1,$name]);

            $consulta = Promocion_Modelo::select('idPromociones')
                                ->orderby('idPromociones','desc')
                                ->limit(1)
                                ->get();

            foreach( $consulta as $k ) {

              $idpromo=$k->{'idPromociones'};

            }

            DB::insert ('insert into oferta (idPromociones, Codigo, Tipo,Precionuevo)
                values(?,?,?,?)',[$idpromo, $codigo, $tipo,$precionuevo]);


            Session::flash('success', ' Se ha registrado exitosamente la promoción ');

            return redirect()->to('selecciondeproducto');

        } else if( $request->botonformpromocion == 'Cancelar') {

              return redirect()->to('home');

        }

    }

      //Funciones para Modificar Oferta
    public function modificarpromocion( $idPromociones ) {

        $promocion = Promocion_Modelo::select('*')
                      ->where( 'idPromociones', $idPromociones )
                      ->take(1)->first();

        $oferta = Oferta_Modelo::select('*')
                    ->where( 'idPromociones', $idPromociones )
                    ->take(1)->first();

        $consulta = $oferta->Codigo;

        $producto = Producto_Modelo::select('*')
                      ->where( 'codigo',$consulta )
                      ->take(1)->first();

        return view( 'vistaspromociones/modificarpromocion' )
                        ->with( 'promocion', $promocion )
                        ->with( 'oferta', $oferta )
                        ->with( 'producto', $producto );

    }

    public function actualizarinfopromocion( Request $request, $idPromociones ) {

        $promocion = Promocion_Modelo::select('*')
                        ->where( 'idPromociones', $idPromociones )
                        ->take(1)->first();

        if( $request->botonformmodificar == 'Modificar' ) {
          
              $name ="";

              if( $request->hasFile('avatar') ) {
                  
                    $file = $request->file( 'avatar' );
                    $nombrepromocion = str_replace( "/", " ", $request -> input('nombre') );
                    //$name = time().$file->getClientOriginalName();

                    $name = 'Promociones/Ofertas/' . $nombrepromocion . "." . $request->file('avatar')->extension();
                    $file->move( public_path().'/images/Promociones/Ofertas/', $nombrepromocion . "." . $request->file('avatar')->extension() );

              } else {

                  $consultapromocion = Promocion_Modelo::select('Imagen')->where('idPromociones', '=', $idPromociones )->get();
                  
                  $name = $consultapromocion[0]->{'Imagen'};

              }  
          
              $consultapromocion = Promocion_Modelo::select('*')
                        ->where( 'idPromociones', $idPromociones )
                        ->get();

              $consultaoferta = Oferta_Modelo::select('*')
                      ->where( 'idPromociones', $idPromociones )
                      ->get();

            $precionuevooferta = $consultaoferta[0]->{'Precionuevo'};
            $preciototalpromocion = $consultapromocion[0]->{'Total'};

            if( $precionuevooferta == $request->nuevoprecio && $preciototalpromocion == $request->total ) {                

                  DB::table('promociones')
                        ->where( 'idPromociones', $idPromociones )  // find your user by their email
                        ->limit(1)  // optional - to ensure only one record is updated.
                        ->update( array(
                                      'Imagen'    => $name,
                                      'Fechaini'  => $request->fechaini,
                                      'Fechafin'  => $request->fechafin,
                                      'Total'     => $request->total 
                                    ) 
                                );

                  DB::table('oferta')
                      ->where( 'idPromociones',$idPromociones )  // find your user by their email
                      ->limit(1)  // optional - to ensure only one record is updated.
                      ->update( array(
                                      'Tipo'        => $request->tipo,
                                      'Precionuevo' => $request->nuevoprecio
                                  )
                              );

            } else {
            
                $fechaactual = Carbon::now();
                
                //Se termina la promoción, se debe modificar la fecha de finalización a la fecha actual
                DB::table('promociones')
                      ->where( 'idPromociones',$idPromociones )  // find your user by their email
                      ->update( array( 
                                      'Fechafin' => $fechaactual,
                                      'Estado' => 0 
                                    ) 
                        );

                $validator = Validator::make($request->all(), Promocion_Modelo::rules(), Promocion_Modelo::messages());

                if( $validator ->fails() ) {

                    return redirect()->back()->withInput()->withErrors($validator->errors());

                }

                $nombre = $request->input('nombre');
                $descripcion = $request->input('descripcion');
                $fechaini = $request->input('fechaini');
                $fechafin = $request->input('fechafin');
                $total = $request->input('total');
                $codigo= $request->input('codigo');
                $tipo = $request->input('tipo');
                $estado = 1;
                $precionuevo= $request->input('nuevoprecio');

                DB::insert ('insert into promociones (Nombre, Descripcion, Fechaini, Fechafin,Total,Estado,Tipo,Imagen)
                    values(?,?,?,?,?,?,?,?)', [ $nombre, $descripcion, $fechaini, $fechafin, $total, $estado, 1, $name ] );

                $consulta = Promocion_Modelo::select('idPromociones')
                                    ->orderby('idPromociones','desc')
                                    ->limit(1)
                                    ->get();

                foreach( $consulta as $k ) {

                    $idpromo=$k->{'idPromociones'};

                }

                DB::insert ('insert into oferta (idPromociones, Codigo, Tipo,Precionuevo)
                    values(?,?,?,?)',[ $idpromo, $codigo, $tipo, $precionuevo ] );                

            }

          Session::flash('success', ' Se ha modificado exitosamente la promoción ');

          return redirect()->to('listadopromociones');

        } else if( $request->botonformmodificar == 'Cancelar' ) {

            return redirect()->to('home');

        }

    }

      //Funciones para Eliminar Oferta
    public function confirmaroferta( $idPromociones ) {

        $promocion = Promocion_Modelo::select('*')
                      ->where( 'idPromociones', $idPromociones )
                      ->take(1)->first();

        $oferta = Oferta_Modelo::select('*')
                    ->where( 'idPromociones', $idPromociones )
                    ->take(1)->first();

        $consulta = $oferta->Codigo;

        $producto = Producto_Modelo::select('*')
                      ->where( 'codigo',$consulta )
                      ->take(1)->first();

        return view( 'vistaspromociones/confirmaroferta' )
                    ->with( 'promocion',$promocion )
                    ->with( 'oferta',$oferta )
                    ->with( 'producto',$producto );

    }


          //DESCUENTO

      //Funciones para registrar promocion Descuento
    public function registrardescuento( $codigo ) {

        $producto = Producto_Modelo::select('*')
                        ->where( 'Codigo', $codigo )
                        ->take(1)->first();

        return view('vistaspromociones/registrardescuento')->with( 'producto', $producto );

    }

    public function insertardescuento( Request $request ) {

        $name = "";

        if( $request->hasFile('avatar') ) {
            
              $file = $request->file( 'avatar' );
              $nombrepromocion = str_replace( "/", " ", $request -> input('nombre') );
              //$name = time().$file->getClientOriginalName();

              $name = 'Promociones/Descuentos/' . $nombrepromocion . "." . $request->file('avatar')->extension();
              $file->move( public_path().'/images/Promociones/Descuentos/', $nombrepromocion . "." . $request->file('avatar')->extension() );

        } else {
          
            $name = 'Promociones/Descuentos/DescuentoDefault.png';

        }

        if( $request->botonformpromocion == 'Registrar') {


              $validator = Validator::make($request->all(), Promocion_Modelo::rules(), Promocion_Modelo::messages());

              if( $validator ->fails() ) {

                return redirect()->back()->withInput()->withErrors($validator->errors());

              }

              $nombre = $request->input('nombre');
              $descripcion = $request->input('descripcion');
              $fechaini = $request->input('fechaini');
              $fechafin = $request->input('fechafin');
              $total = $request->input('total');
              $codigo = $request->input('codigo');
              $tipo = $request->input('tipo');
              $estado = 1;
              $precionuevo = $request->input('nuevoprecio');

              DB::insert ('insert into promociones (Nombre, Descripcion, Fechaini, Fechafin,Total,Estado,Tipo,Imagen)
                  values(?,?,?,?,?,?,?,?)', [ $nombre, $descripcion, $fechaini, $fechafin, $precionuevo, $estado, 2, $name ] );

              $consulta = Promocion_Modelo::select('idPromociones')
                            ->orderby('idPromociones','desc')
                            ->limit(1)
                            ->get();

              foreach ( $consulta as $k ) {

                $idpromo = $k->{'idPromociones'};

              }

              DB::insert ('insert into descuento (idPromociones, Codigo, descuento,Precionuevo)
                  values(?,?,?,?)',[$idpromo, $codigo, $tipo,$precionuevo]);


              Session::flash('success', ' Se ha registrado exitosamente la promoción ');
            
              return redirect()->to('selecciondeproducto');

        } else if( $request->botonformpromocion == 'Cancelar') {

            return redirect()->to('home');

        }

    }

      //Funciones para Modificar Descuento
    public function modificarpromocion2( $idPromociones ) {

        $promocion = Promocion_Modelo::select('*')
                      ->where( 'idPromociones', $idPromociones )
                      ->take(1)->first();

        $descuento = Descuento_Modelo::select('*')
                      ->where( 'idPromociones', $idPromociones )
                      ->take(1)->first();

        $consulta = $descuento->Codigo;

        $producto = Producto_Modelo::select('*')
                      ->where( 'codigo',$consulta )
                      ->take(1)->first();

        return view( 'vistaspromociones/modificarpromocion2' )
                              ->with( 'promocion', $promocion )
                              ->with( 'oferta', $descuento )
                              ->with( 'producto', $producto );

    }

    public function actualizarinfopromocion2( Request $request, $idPromociones ) {

        $promocion = Promocion_Modelo::select('*')->where('idPromociones', $idPromociones)->take(1)->first();

        if( $request->botonformmodificar == 'Modificar' ) {
          
            $name ="";

            if( $request->hasFile('avatar') ) {
                
                  $file = $request->file( 'avatar' );
                  $nombrepromocion = str_replace( "/", " ", $request -> input('nombre') );
                  //$name = time().$file->getClientOriginalName();

                  $name = 'Promociones/Descuentos/' . $nombrepromocion . "." . $request->file('avatar')->extension();
                  $file->move( public_path().'/images/Promociones/Descuentos/', $nombrepromocion . "." . $request->file('avatar')->extension() );

            } else {

                $consultapromocion = Promocion_Modelo::select('Imagen')->where('idPromociones', '=', $idPromociones )->get();
                
                $name = $consultapromocion[0]->{'Imagen'};

            }

            $consultapromocion = Promocion_Modelo::select('*')
                          ->where( 'idPromociones', $idPromociones )
                          ->get();
      
            $consultadescuento = Descuento_Modelo::select('*')
                          ->where( 'idPromociones', $idPromociones )
                          ->get();

            $porcentajedescuento = $consultadescuento[0]->{'descuento'};
            $preciototalpromocion = $consultapromocion[0]->{'Total'};

            if( $porcentajedescuento == $request->descuento && $preciototalpromocion == $request->nuevoprecio ) {
                        
                DB::table('promociones')
                      ->where( 'idPromociones',$idPromociones )  // find your user by their email
                      ->limit(1)  // optional - to ensure only one record is updated.
                      ->update( array(
                                      'Imagen'=>$name,
                                      'Fechaini' => $request->fechaini,
                                      'Fechafin' => $request->fechafin,
                                      'Total' => $request->nuevoprecio
                                  )
                              );

                DB::table('descuento')
                      ->where( 'idPromociones',$idPromociones )  // find your user by their email
                      ->limit(1)  // optional - to ensure only one record is updated.
                      ->update( array(
                                      'descuento' => $request->descuento,
                                      'Precionuevo' => $request->nuevoprecio
                                  )
                              );

            } else {
            
                $fechaactual = Carbon::now();
                
                //Se termina la promoción, se debe modificar la fecha de finalización a la fecha actual
                DB::table('promociones')
                      ->where( 'idPromociones',$idPromociones )  // find your user by their email
                      ->update( array( 
                                      'Fechafin' => $fechaactual,
                                      'Estado' => 0 
                                    ) 
                        );

                $validator = Validator::make($request->all(), Promocion_Modelo::rules(), Promocion_Modelo::messages());

                if( $validator ->fails() ) {
  
                  return redirect()->back()->withInput()->withErrors($validator->errors());
  
                }
          
                $nombre = $request->input('nombre');
                $descripcion = $request->input('descripcion');
                $fechaini = $request->input('fechaini');
                $fechafin = $request->input('fechafin');
                $total = $request->input('total');
                $codigo = $request->input('codigo');
                $tipo = $request->input('tipo');
                $estado = 1;
                $precionuevo = $request->input('nuevoprecio');
          
                DB::insert ('insert into promociones (Nombre, Descripcion, Fechaini, Fechafin,Total,Estado,Tipo,Imagen)
                    values(?,?,?,?,?,?,?,?)', [ $nombre, $descripcion, $fechaini, $fechafin, $precionuevo, $estado, 2, $name ] );
  
                $consulta = Promocion_Modelo::select('idPromociones')
                              ->orderby('idPromociones','desc')
                              ->limit(1)
                              ->get();
  
                foreach ( $consulta as $k ) {
  
                  $idpromo = $k->{'idPromociones'};
  
                }
  
                DB::insert ('insert into descuento (idPromociones, Codigo, descuento,Precionuevo)
                    values(?,?,?,?)',[$idpromo, $codigo, $tipo,$precionuevo]);              

            }

            Session::flash('success', ' Se ha modificado exitosamente la promoción ');

            return redirect()->to('listadopromociones');

        } else if( $request->botonformmodificar == 'Cancelar' ) {

          return redirect()->to('home');

        }

    }

      //Funciones para Eliminar Descuento
    public function confirmardescuento( $idPromociones ) {

      $promocion = Promocion_Modelo::select('*')
                    ->where( 'idPromociones', $idPromociones )
                    ->take(1)->first();

      $descuento = Descuento_Modelo::select('*')
                    ->where( 'idPromociones', $idPromociones )
                    ->take(1)->first();

      $consulta = $descuento->Codigo;

      $producto = Producto_Modelo::select('*')
                    ->where( 'codigo',$consulta )
                    ->take(1)->first();

      return view('vistaspromociones/confirmardescuento')
                      ->with( 'promocion',$promocion )
                      ->with( 'oferta',$descuento )
                      ->with( 'producto',$producto );

    }

          //PAQUETE 

    //Funciones para registrar promocion Paquete
    public function registrarpaquete(  $codigo1, $codigo2 ) {

      $producto = Producto_Modelo::select('*')
                        ->where( 'Codigo', $codigo1 )
                        ->take(1)->first();

      $producto2 = Producto_Modelo::select('*')
                        ->where( 'Codigo', $codigo2 )
                        ->take(1)->first();

      return view( 'vistaspromociones/registrarpaquete' ) 
                          ->with( 'producto',$producto )
                          ->with( 'producto2',$producto2 );

    }

    public function insertarpaquete( Request $request ) {

        $name = "";

        if( $request->hasFile('avatar') ) {
            
              $file = $request->file( 'avatar' );
              $nombrepromocion = str_replace( "/", " ", $request -> input('nombre') );
              //$name = time().$file->getClientOriginalName();

              $name = 'Promociones/Paquetes/' . $nombrepromocion . "." . $request->file('avatar')->extension();
              $file->move( public_path().'/images/Promociones/Paquetes/', $nombrepromocion . "." . $request->file('avatar')->extension() );

        } else {          

            $name = 'Promociones/Paquetes/PaqueteDefault.png';

        }

        if( $request->botonformpromocion == 'Registrar') {

              $validator = Validator::make($request->all(), Promocion_Modelo::rules(), Promocion_Modelo::messages());

              if( $validator ->fails() ) {

                  return redirect()->back()->withInput()->withErrors($validator->errors());

              }

              $nombre = $request->input('nombre');
              $descripcion = $request->input('descripcion');
              $fechaini = $request->input('fechaini');
              $fechafin = $request->input('fechafin');
              $total = $request->input('total');
              $codigo1= $request->input('codigo');
              $codigo2= $request->input('codigo2');

              $estado = 1;
              $tipo1 = $request->input('descuento');
              $tipo2 = $request->input('descuento2');
              $precionuevo1= $request->input('nuevoprecio');
              $precionuevo2= $request->input('nuevoprecio2');


              DB::insert ('insert into promociones (Nombre, Descripcion, Fechaini, Fechafin,Total,Estado,Tipo,Imagen)
                  values(?,?,?,?,?,?,?,?)', [ $nombre, $descripcion, $fechaini, $fechafin, $total, $estado, 3, $name ] );

              $consulta = Promocion_Modelo::select('idPromociones')
                                ->orderby('idPromociones','desc')
                                ->limit(1)
                                ->get();

              foreach ( $consulta as $k ) {

                $idpromo = $k->{'idPromociones'};

              }

              DB::insert ('insert into paquetes (idPromociones, Codigo1, Descuento1,Precio1, Codigo2, Descuento2,Precio2)
                  values(?,?,?,?,?,?,?)', [$idpromo, $codigo1, $tipo1,$precionuevo1, $codigo2, $tipo2,$precionuevo2]);


              Session::flash('success', ' Se ha registrado exitosamente la promoción ');

              return redirect()->to('selecciondeproducto');
              //return redirect()->to('listapromociones');              

        } else if( $request->botonformpromocion == 'Cancelar') {

            return redirect()->to('home');

        }

    }

      //Funciones para Modificar Paquete
    public function modificarpromocion3( $idPromociones ) {

        $promocion = Promocion_Modelo::select('*')
                    ->where( 'idPromociones', $idPromociones )
                    ->take(1)->first();

        $paquete = Paquetes_Modelo::select('*')
                    ->where( 'idPromociones', $idPromociones )
                    ->take(1)->first();

        $consulta1 = $paquete->Codigo1;
        $consulta2 = $paquete->Codigo2;

        $producto1 = Producto_Modelo::select('*')
                      ->where( 'codigo',$consulta1 )
                      ->take(1)->first();

        $producto2 = Producto_Modelo::select('*')
                      ->where( 'codigo',$consulta2 )
                      ->take(1)->first();

        return view( 'vistaspromociones/modificarpromocion3' )
                              ->with( 'promocion', $promocion )
                              ->with( 'oferta', $paquete )
                              ->with( 'producto', $producto1 )
                              ->with( 'producto2', $producto2 );

    }
    
    public function actualizarinfopromocion3( Request $request, $idPromociones ) {

      $promocion = Promocion_Modelo::select('*')
                    ->where( 'idPromociones', $idPromociones )
                    ->take(1)->first();

      if( $request->botonformmodificar == 'Modificar' ) {
                  
          $name ="";

          if( $request->hasFile('avatar') ) {
              
                $file = $request->file( 'avatar' );
                $nombrepromocion = str_replace( "/", " ", $request -> input('nombre') );
                //$name = time().$file->getClientOriginalName();

                $name = 'Promociones/Paquetes/' . $nombrepromocion . "." . $request->file('avatar')->extension();
                $file->move( public_path().'/images/Promociones/Paquetes/', $nombrepromocion . "." . $request->file('avatar')->extension() );

          } else {

              $consultapromocion = Promocion_Modelo::select('Imagen')->where('idPromociones', '=', $idPromociones )->get();
              
              $name = $consultapromocion[0]->{'Imagen'};

          }
        
          $consultapromocion = Promocion_Modelo::select('*')
                      ->where( 'idPromociones', $idPromociones )
                      ->get();

          $consultapaquete = Paquetes_Modelo::select('*')
                      ->where( 'idPromociones', $idPromociones )
                      ->get();
                    
          $porcentajedescuento1 = $consultapaquete[0]->{'Descuento1'};
          $porcentajedescuento2 = $consultapaquete[0]->{'Descuento2'};
          $preciototalpromocion = $consultapromocion[0]->{'Total'};

          if( ( $preciototalpromocion == $request->total ) && ( $porcentajedescuento1 == $request->descuento ) && ( $porcentajedescuento2 == $request->descuento2 ) ) {
            
                DB::table('promociones')
                ->where( 'idPromociones',$idPromociones )  // find your user by their email
                ->limit(1)  // optional - to ensure only one record is updated.
                ->update( array(
                                'Imagen'=>$name,
                                'Fechaini' => $request->fechaini,
                                'Fechafin' => $request->fechafin,
                                'Total' => $request->total
                            )
                        );

                DB::table('paquetes')
                      ->where( 'idPromociones',$idPromociones )  // find your user by their email
                      ->limit(1)  // optional - to ensure only one record is updated.
                      ->update( array(
                                        'Descuento1' => $request->descuento,
                                        'Precio1' => $request->nuevoprecio,
                                        'Descuento2' => $request->descuento2,
                                        'Precio2' => $request->nuevoprecio2
                                    )
                              );

          } else {
            
                $fechaactual = Carbon::now();
                
                //Se termina la promoción, se debe modificar la fecha de finalización a la fecha actual
                DB::table('promociones')
                      ->where( 'idPromociones',$idPromociones )  // find your user by their email
                      ->update( array( 
                                      'Fechafin' => $fechaactual,
                                      'Estado' => 0 
                                    ) 
                        );
              
                $validator = Validator::make($request->all(), Promocion_Modelo::rules(), Promocion_Modelo::messages());

                if( $validator ->fails() ) {
  
                    return redirect()->back()->withInput()->withErrors($validator->errors());
  
                }
  
                $nombre = $request->input('nombre');
                $descripcion = $request->input('descripcion');
                $fechaini = $request->input('fechaini');
                $fechafin = $request->input('fechafin');
                $total = $request->input('total');
                $codigo1= $request->input('codigo');
                $codigo2= $request->input('codigo2');
  
                $estado = 1;
                $tipo1 = $request->input('descuento');
                $tipo2 = $request->input('descuento2');
                $precionuevo1= $request->input('nuevoprecio');
                $precionuevo2= $request->input('nuevoprecio2');
  
  
                DB::insert ('insert into promociones (Nombre, Descripcion, Fechaini, Fechafin,Total,Estado,Tipo,Imagen)
                    values(?,?,?,?,?,?,?,?)', [ $nombre, $descripcion, $fechaini, $fechafin, $total, $estado, 3, $name ] );
  
                $consulta = Promocion_Modelo::select('idPromociones')
                                  ->orderby('idPromociones','desc')
                                  ->limit(1)
                                  ->get();
  
                foreach ( $consulta as $k ) {
  
                  $idpromo = $k->{'idPromociones'};
  
                }
  
                DB::insert ('insert into paquetes (idPromociones, Codigo1, Descuento1,Precio1, Codigo2, Descuento2,Precio2)
                    values(?,?,?,?,?,?,?)', [ $idpromo, $codigo1, $tipo1, $precionuevo1, $codigo2, $tipo2, $precionuevo2 ] );

          }

          Session::flash('success', ' Se ha modificado exitosamente la promoción ');

          return redirect()->to('listadopromociones');

        } else if( $request->botonformmodificar == 'Cancelar' ) {

          return redirect()->to('home');

        }

    }

      //Funciones para Eliminar Paquetes
    public function confirmarpaquete( $idPromociones ) {

      $promocion = Promocion_Modelo::select('*')
                      ->where( 'idPromociones', $idPromociones )
                      ->take(1)->first();

      $paquete = Paquetes_Modelo::select('*')
                    ->where( 'idPromociones', $idPromociones )
                    ->take(1)->first();

      $consulta1 = $paquete->Codigo1;
      $consulta2 = $paquete->Codigo2;

      $producto1 = Producto_Modelo::select('*')
                      ->where( 'codigo',$consulta1 )
                      ->take(1)->first();

      $producto2 = Producto_Modelo::select('*')
                      ->where( 'codigo',$consulta2 )
                      ->take(1)->first();

      return view( 'vistaspromociones/confirmarpaquete' )
                  ->with( 'promocion',$promocion )
                  ->with( 'oferta',$paquete )
                  ->with( 'producto',$producto1 )
                  ->with( 'producto2',$producto2) ;

    }

}