<?php

namespace App\Http\Controllers\ProyectoControllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Session;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;
use Mike42\Escpos\Printer;
use Mike42\Escpos\EscposImage;

use App\Models\Tienda_Modelo;
use App\Models\Producto_Modelo;
use App\Models\Venta_Modelo;
use App\Models\DetalleVenta_Modelo;
use App\Models\Cliente_Modelo;
use App\Models\Empleado_Modelo;
use App\Models\Caja_Modelo;
use App\Models\Inventario_Modelo;
use App\Models\CreditoCliente_Modelo;
use PhpParser\JsonDecoder;

/*
          Promociones
               1 - Oferta
               2 - Descuento
               3 - Paquetes
     */

class VentasController extends Controller
{

     public function listadoventas() {

          $listadoventas = DB::table('venta')
          ->join('empleado', 'venta.idempleado', '=', 'empleado.idempleado')
          ->join('cliente', 'venta.idcliente', '=', 'cliente.idcliente')
          ->select( DB::raw("CONCAT( empleado.nombre,' ', empleado.apellidopat,' ',empleado.apellidomat) as nombrecompletoempleado"),
                    DB::raw("CONCAT( cliente.nombre,' ', cliente.apellidopat,' ',cliente.apellidomat) as nombrecompletocliente"),
               'venta.idventa as idventa','venta.fecha as fechaventa', 'venta.total as totalventa', 'venta.idtipopago as tipopago' )
               ->orderBy('venta.idventa', 'asc')
               ->get();

          return view('vistasventas/listadoventas')->with( 'listadoventas', $listadoventas );

     }

     public function busquedaventa() {

          return view('vistasventas/busquedaventa');

     }

     public function realizarbusquedaventa( Request $request ) {

          if( $request->botonbuscarventa == 'Buscar' ) {

               $idventa = $request->input('idventa');

               $ventas = Venta_Modelo::where('idventa', $idventa )->get();

               if( count( $ventas ) == 1 ) {

                    $datosventa = DB::table('venta')
                    ->join('detalleventa', 'venta.idventa', '=', 'detalleventa.idventa')
                    ->join('empleado', 'venta.idempleado', '=', 'empleado.idempleado')
                    ->join('cliente', 'venta.idcliente', '=', 'cliente.idcliente')
                    ->select( DB::raw("CONCAT( empleado.nombre,' ', empleado.apellidopat,' ',empleado.apellidomat) as nombrecompletoempleado"),
                              DB::raw("CONCAT( cliente.nombre,' ', cliente.apellidopat,' ',cliente.apellidomat) as nombrecompletocliente"),
                         'venta.idventa as idventa','venta.fecha as fechaventa', 'venta.total as totalventa', 'venta.idtipopago as tipopago' )
                         ->where('venta.idventa', $idventa )
                         ->take(1)
                         ->first();

                    $datosdetalleventa = DB::table('detalleventa')
                    ->join('producto', 'detalleventa.idproducto', '=', 'producto.codigo')
                    ->join('marca', 'producto.idmarca', '=', 'marca.idmarca')
                    ->join('presentacion', 'producto.idpresentacion', '=', 'presentacion.idpresentacion')
                    ->join('proveedor', 'producto.idproveedor', '=', 'proveedor.idproveedor')
                    ->join('categoria', 'producto.idcategoria', '=', 'categoria.idcategoria')
                    ->select( 'detalleventa.cantidad as CantidadDetalleVenta', 'producto.codigo as CodigoProductoDetalleVenta', 'producto.idmarca', 'marca.descripcion as nombremarca',
                              'producto.idpresentacion','presentacion.medida as nombrepresentacion', 'producto.idproveedor',
                              DB::raw( "CONCAT( proveedor.nombre,' ', proveedor.apellidopat,' ', proveedor.apellidomat ) AS nombrecompleto" ),
                              'producto.idcategoria', 'categoria.nombre as nombrecategoria', 'producto.descripcion as DescripcionProducto',
                              'producto.preciocosto','producto.precioventa as PrecioVentaProducto', 'producto.iva')
                              ->where('detalleventa.idventa', $idventa )
                              ->get();

                    return view( 'vistasventas/informaciondetalleventa')->with( 'datosventa', $datosventa )->with( 'datosdetalleventa', $datosdetalleventa );

               } else {

                    Session::flash('warning', ' No hay ninguna venta registrada con ese número de venta ');
                    return redirect()->back();

               }

          } elseif( $request->botonbuscarventa == 'Cancelar' ) {

               return redirect()->to('home');

          }

     }

     public function informaciondetalleventa( $idventa ) {

          $ventas = Venta_Modelo::where('idventa', $idventa )->get();

          if( count( $ventas ) == 1 ) {

               $datosventa = DB::table('venta')
               ->join('detalleventa', 'venta.idventa', '=', 'detalleventa.idventa')
               ->join('empleado', 'venta.idempleado', '=', 'empleado.idempleado')
               ->join('cliente', 'venta.idcliente', '=', 'cliente.idcliente')
               ->select( DB::raw("CONCAT( empleado.nombre,' ', empleado.apellidopat,' ',empleado.apellidomat) as nombrecompletoempleado"),
                         DB::raw("CONCAT( cliente.nombre,' ', cliente.apellidopat,' ',cliente.apellidomat) as nombrecompletocliente"),
                    'venta.idventa as idventa','venta.fecha as fechaventa', 'venta.total as totalventa', 'venta.idtipopago as tipopago' )
                    ->where('venta.idventa', $idventa )
                    ->take(1)
                    ->first();

               $datosdetalleventa = DB::table('detalleventa')
               ->join('producto', 'detalleventa.idproducto', '=', 'producto.codigo')
               ->join('marca', 'producto.idmarca', '=', 'marca.idmarca')
               ->join('presentacion', 'producto.idpresentacion', '=', 'presentacion.idpresentacion')
               ->join('proveedor', 'producto.idproveedor', '=', 'proveedor.idproveedor')
               ->join('categoria', 'producto.idcategoria', '=', 'categoria.idcategoria')
               ->select( 'detalleventa.cantidad as CantidadDetalleVenta', 'producto.codigo as CodigoProductoDetalleVenta', 'producto.idmarca', 'marca.descripcion as nombremarca',
                         'producto.idpresentacion','presentacion.medida as nombrepresentacion', 'producto.idproveedor',
                         DB::raw( "CONCAT( proveedor.nombre,' ', proveedor.apellidopat,' ', proveedor.apellidomat ) AS nombrecompleto" ),
                         'producto.idcategoria', 'categoria.nombre as nombrecategoria', 'producto.descripcion as DescripcionProducto',
                         'producto.preciocosto','producto.precioventa as PrecioVentaProducto', 'producto.iva')
                         ->where('detalleventa.idventa', $idventa )
                         ->get();

               return view( 'vistasventas/informaciondetalleventa')->with( 'datosventa', $datosventa )->with( 'datosdetalleventa', $datosdetalleventa );

          } else {

               Session::flash('warning', ' No hay alguna venta registrada con ese id ');
               return redirect()->back();

          }

     }

     public function registrarventa() {

          /*
          $clientes = Cliente_Modelo::select("idcliente", DB::raw("CONCAT(cliente.nombre,' ',
                    cliente.apellidopat,' ',cliente.apellidomat) as nombrecompleto") )
                    ->where('cliente.estado', '=', '1')
                    ->get();
                    //->pluck('nombrecompleto', 'idcliente')->toArray();
          */

          $consultainformacionempleado = Empleado_Modelo::select( DB::raw("CONCAT( empleado.nombre,' ', empleado.apellidopat,' ',empleado.apellidomat) as nombrecompleto") )
                                        ->where( 'empleado.idempleado', '=', Auth::User()->idEmpleado )
                                        ->get();

          $nombrevendedor = $consultainformacionempleado[0]->{'nombrecompleto'};

          $consultanumventas = Venta_Modelo::select( DB::raw('COUNT( venta.idVenta ) as totalventas') )
                                                  ->get();

          $numeroventa = 0;
          
          if(  count( $consultanumventas ) > 0 ) {
               
               $auxnumeroventa = $consultanumventas[0]->{'totalventas'};
               $numeroventa = $auxnumeroventa + 1;

          } else {

               $numeroventa = 1;

          }

          //$auxnumeroventa = $consultanumventas[0]->{'totalventas'};
          //$numeroventa = $auxnumeroventa + 1;   

          $clientes = Cliente_Modelo::select("cliente.idcliente",
          DB::raw("CONCAT(cliente.nombre,' ', cliente.apellidopat,' ',cliente.apellidomat) as nombrecompleto") )
          ->join('creditocliente', 'cliente.idcliente', '=', 'creditocliente.idcliente')
          ->where('cliente.estado', '=', '1')
          ->where('creditocliente.estado', '=', '1')
          ->get();

          return view('vistasventas/registrarventa')->with( 'clientes', $clientes )->with( 'nombrevendedor', $nombrevendedor )->with( 'numeroventa', $numeroventa );

     }

     public function insertarventa( Request $request ) {

          $banderapagocredito = json_decode( $request->PagoACredito );

          //$info_array = json_decode($request->TableData, true);
          $info_array = json_decode( $request->TableData );

          //$arregloproductodescuentos = json_decode( $request-> arregloproductodescuentos );
          //$this ->imprimirventa( $info_array, $arregloproductodescuentos );


          //$idempleado = json_decode($request->IdEmpleado);
          $idcliente = json_decode( $request->IdCliente );

          $total = json_decode( $request->Total );
          $total = bcadd( $total,'0', 2 );

          $textototal = json_decode( $request->TextoTotal );

          $descuento = json_decode( $request->Descuento);
          $descuento = bcadd( $descuento,'0', 2 );

          $efectivo = json_decode( $request->Efectivo );
          $efectivo = bcadd( $efectivo,'0', 2 );

          $tipopago = json_decode( $request->TipoPago );

          $idtipomovimiento = 203;
          $descripcion = "Salida por compra cliente";

          //$idempleado = 1;
          $idempleado = Auth::User()->idEmpleado;

          $fecha = Carbon::now();

          $consultaidcaja = Caja_Modelo::select('idCaja')
                         ->where('idEmpleado', $idempleado)
                         ->limit(1)
                         ->get();

          foreach( $consultaidcaja as $k ) {
               $idcaja = $k->{'idCaja'};
          }

          //SABER QUE CLIENTE REALIZO LA VENTA
          if( $idcliente == null ) {

               $detalleventa = 1; // 1 => pagado al contado
               $idcliente = 1;

          } else {

               $detalleventa = 0; // 0 => pagado con credito

          }

          $estado = 0;

          if( $tipopago == 1 || $tipopago == 2 ) {

               DB::insert('insert into venta ( idEmpleado, idCliente, idCaja, Fecha, Detalleventa, Total, Descuento, Efectivo, idTipoPago, Estado )
               values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)', [ $idempleado, $idcliente,
               $idcaja, $fecha, $detalleventa, $total, $descuento , $efectivo, $tipopago, $estado] );

          } else if( $tipopago == 3 ) {
               
               $efectivo = $total - $efectivo;               
               DB::insert('insert into venta ( idEmpleado, idCliente, idCaja, Fecha, Detalleventa, Total, Descuento, Efectivo, idTipoPago, Estado )
               values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)', [ $idempleado, $idcliente,
               $idcaja, $fecha, $detalleventa, $total, $descuento , $efectivo, $tipopago, $estado] );

          }

          


               //Obtener el id de la última venta
          $consulta = Venta_Modelo::select('idVenta')
                    ->orderBy('idVenta', 'desc')
                    ->limit(1)
                    ->get();

          foreach( $consulta as $k ) {
               $idventa = $k->{'idVenta'};
          }

          foreach( $info_array as $k ) {

               $idproducto = $k->{'ColumnaCodigos'};
               $cantidad = $k->{'ColumnaCantidades'};
               $precioven = $k->{'ColumnaPrecios'};

               /*
               DB::insert('insert into detalleventa (idVenta, idProducto, Cantidad, Fecha)
               values (?, ?, ?, ?)', [ $idventa, $idproducto,
                                             $cantidad, $fecha] );
               */


               $consultaproductostockmin = DB::table('producto')
                                        ->select( 'stockminimo' )
                                        ->where('codigo','=', $idproducto)
                                        ->get();
                                
               $stockminimo = $consultaproductostockmin[0]->{'stockminimo'};


               //Se obtienen los datos del producto registrado en la tabla inventario
               $productoinventario = DB::table('inventario')
                                            ->select( 'cantidad', 'preciocom', 'precioven','gananciastot' )
                                                   ->where('codigo','=', $idproducto)
                                                   ->get();          

               //Se obtienen los atributos del objeto productoinventario                
               $cantidadant = $productoinventario[0]->{'cantidad'};
               $cantidadact = $cantidadant - $cantidad;
               $cantidadmov = $cantidad;    

               $gananciastotant = $productoinventario[0]->{'gananciastot'};
                
               $preciocom = $productoinventario[0]->{'preciocom'};


               //Calcular los nuevos precios de compra y de venta                    
               $preciocostoant = $productoinventario[0]->{'preciocom'};
               $precioventaant = $productoinventario[0]->{'precioven'};
          
               DB::insert('insert into movimientoinventario( idTipoMovimiento , idempleado, codigo, fechahora, 
                         descripcion, cantidadant, cantidadact, cantidadmov, preciocostoant, preciocostoact, precioventaant, precioventaact ) 
                         values( ?, ?, ?, ?, ?, ?, ? ,?, ?, ?, ?, ? )',
                         [ $idtipomovimiento, $idempleado, $idproducto, $fecha,
                         $descripcion, $cantidadant, $cantidadact, $cantidadmov, 
                         $preciocostoant, $preciocom, $precioventaant, $precioven ] );

                         
            //Obtener el id de la última devolución
            $consultaidmovimientoinventario = DB::table('movimientoinventario')
                                             ->select('idMovimientoInventario as idMovimientoInventario')
                                            ->orderBy('idMovimientoInventario', 'desc')
                                            ->limit(1)
                                            ->get();

               $idMovimientoInventario = $consultaidmovimientoinventario[0]->{'idMovimientoInventario'};

                         
               DB::insert('insert into detalleventa (idVenta, idProducto, Cantidad, Fecha, idMovimientoInventario )
               values (?, ?, ?, ?, ? )', [ $idventa, $idproducto,
                                             $cantidad, $fecha, $idMovimientoInventario ] );

                                    
               $consultamovimientoinventarioentradasprecios = DB::table('movimientoinventario')
                                                ->select(   DB::raw('Sum(PrecioCostoAct *CantidadMov) as debe'),
                                                            DB::raw('Sum(PrecioVentaAct*CantidadMov) as obtenido')  )
                                                            ->where( 'codigo', '=', $idproducto )
                                                            //->where( 'idtipomovimiento', '=', $idtipomovimiento )
                                                            ->where('descripcion', 'like', '%ingreso%')
                                                            ->get();
                                                            
               $consultamovimientoinventariosalidasprecios = DB::table('movimientoinventario')
                                                ->select(   DB::raw('Sum(PrecioCostoAct *CantidadMov) as debe'),
                                                            DB::raw('Sum(PrecioVentaAct*CantidadMov) as obtenido')  )
                                                            ->where( 'codigo', '=', $idproducto )
                                                            //->where( 'idtipomovimiento', '=', $idtipomovimiento )
                                                            ->where('descripcion', 'like', '%salida%')
                                                            ->get();                                                            


               $sumaentradatotalpreciocompra = $consultamovimientoinventarioentradasprecios[0]->{'debe'};
               echo "sumaentradatotalpreciocompra: ". $sumaentradatotalpreciocompra;

               $sumaentradatotalprecioventa = $consultamovimientoinventarioentradasprecios[0]->{'obtenido'};
               echo "sumaentradatotalprecioventa: ". $sumaentradatotalprecioventa;

               $sumasalidatotalpreciocompra = $consultamovimientoinventariosalidasprecios[0]->{'debe'};
               echo "sumasalidatotalpreciocompra: ". $sumaentradatotalpreciocompra;
               
               $sumasalidatotalprecioventa = $consultamovimientoinventariosalidasprecios[0]->{'obtenido'};
               echo "sumasalidatotalprecioventa: ". $sumaentradatotalprecioventa;

               $totalpreciocompra = $sumaentradatotalpreciocompra - $sumasalidatotalpreciocompra;
               echo "nuevopreciocompra: ". $totalpreciocompra;

               $totalprecioventa = $sumaentradatotalprecioventa - $sumasalidatotalprecioventa;
               echo "nuevopreciocompra: ". $totalprecioventa;

               $nuevopreciocompra = 0.0;
               $nuevoprecioventa = 0.0;

               if( $cantidadact > 0 ) {

                    $nuevopreciocompra = bcadd( ( $totalpreciocompra / $cantidadact ) , '0', 2 );
                    echo "nuevopreciocompra: ". $nuevopreciocompra;

                    $nuevoprecioventa = bcadd( ( $totalprecioventa / $cantidadact ), '0', 2 );
                    echo "nuevoprecioventa: ". $nuevoprecioventa;

               } else {

                    $nuevopreciocompra = $preciocom;
                    $nuevoprecioventa = $precioven;

               }

               
               //$gananciastot = bcadd( ( $totalprecioventa - $totalpreciocompra ), '0', 2 );
               $gananciastotnew = bcadd( ( ( $nuevoprecioventa * $cantidad ) - ( $nuevopreciocompra * $cantidad ) ), '0', 2 );
               $gananciastot = bcadd( ( $gananciastotant + $gananciastotnew ) , '0', 2 );
               $gananciaxpz = bcadd( ( $gananciastot / $cantidad ), '0', 2 );


               DB::table('producto')
                    ->where( 'codigo', $idproducto )
                    ->limit(1)
                    ->update( array( 'PrecioVenta' => $nuevoprecioventa, 'PrecioCosto' => $nuevopreciocompra ) );

                    //Actualizar la cantidad de existencias y precios del producto en inventario
               DB::table('inventario')
                    ->where( 'codigo', $idproducto )
                    ->limit(1)
                    ->update( array(    'Cantidad' => $cantidadact, 'PrecioCom' => $nuevopreciocompra, 'PrecioVen' => $nuevoprecioventa,
                                        'Gananciasxpz' => $gananciaxpz, 'Gananciastot' => $gananciastot,  'Stockminimo' => $stockminimo ) );

          }

          //PAGADO A CREDITO
          if( $detalleventa == 0 ) { //PAGADO A CREDITO

               if( $tipopago == 2 || $tipopago == 3 ) {

                    $CantidadAdeudada = json_decode( $request->CantidadAdeudada );
     
                    DB::table('creditocliente')
                    ->where('creditocliente.idcliente', $idcliente )
                    ->limit(1)
                    ->update( array( 'CanAdeudada' => $CantidadAdeudada ) );

               }

          }

          if( $descuento == 0 ) {
               
               $this ->imprimirticketventa( $info_array, $total, $descuento, $efectivo, $tipopago, $idcliente, $textototal );
               //$this ->generarticketventalaravel( $info_array, $total, $descuento,  $efectivo, $tipopago, $idcliente, $textototal );

          } else if( $descuento > 0 ) {
               
               $info_array_promociones = json_decode( $request->TableDataPromociones );
               
               $this ->imprimirticketventadescuento( $info_array, $info_array_promociones, $total, $descuento, $efectivo, $tipopago, $idcliente, $textototal );
               //$this ->generarticketventalaraveldescuento( $info_array, info_array_promociones, $total, $descuento,  $efectivo, $tipopago, $idcliente, $textototal );

          }
          

     }

     //PDF
     public function imprimirticketventa( $arrayproductos, $total, $descuento, $efectivo, $tipopago, $idcliente, $textototal ) {

          //$arrayproductos = json_decode( $request->DatosProductos );
          //$arraypromociones = json_decode( $request->DatosPromociones );
          print_r( $arrayproductos );

          $filasproductos = "";
          $filasinformacion = "";

          foreach( $arrayproductos as $itemproducto ) {

               $precioproducto =  bcadd( $itemproducto->{'ColumnaPrecios'}, '0', 2 );
               $importeproducto =  bcadd( $itemproducto->{'ColumnaImportes'}, '0', 2 );

               $filasproductos .=   '<tr>
                                        <td class="cantidad"> '. $itemproducto->{'ColumnaCantidades'} .'</td>
                                        <td class="producto">'.  $itemproducto->{'ColumnaDescripciones'} .'</td>
                                        <td class="precio">  $'. $precioproducto .'</td>
                                        <td class="importe"> $'. $importeproducto .'</td>
                                   </tr>';

               echo "\nIteración";

          }

          $subtotal = bcadd( ( $total + $descuento ), '0', 2 );
          $cambio   = bcadd( ( $efectivo -  $total ), '0', 2 );

          if( $tipopago == 1 ) {

               $filasinformacion .=  '<tr class="filanobordeprincipal">
                                        <td class="cantidad"> </td>
                                        <td class="producto">SUBTOTAL</td>
                                        <td class="precio"> </td>
                                        <td class="importe"> $'. $subtotal .'</td>
                                   </tr>
                                   <tr class="filanoborde">
                                        <td class="cantidad"> </td>
                                        <td class="producto">DESCUENTO</td>
                                        <td class="precio"> </td>
                                        <td class="importe"> $'. $descuento .'</td>
                                   </tr>
                                   <tr class="filanoborde">
                                        <td class="cantidad"> </td>
                                        <td class="producto">TOTAL</td>
                                        <td class="precio"> </td>
                                        <td class="importe"> $'. $total .'</td>
                                   </tr>
                                   <tr class="filanoborde">
                                        <td class="cantidad"> </td>
                                        <td class="producto">PAGO</td>
                                        <td class="precio"> </td>
                                        <td class="importe"> $'. $efectivo .'</td>
                                   </tr>
                                   <tr class="filanoborde">
                                        <td class="cantidad"> </td>
                                        <td class="producto">CAMBIO</td>
                                        <td class="precio"> </td>
                                        <td class="importe"> $'. $cambio .'</td>
               </tr>';          

          } else if( $tipopago == 2 ) {

               $informacioncliente = DB::table('cliente')
               ->join('creditocliente', 'cliente.idcliente', '=', 'creditocliente.idcliente')
               ->select( 'creditocliente.idcreditocliente', 'cliente.idcliente',
                         DB::raw( "CONCAT(nombre,' ', apellidopat,' ', apellidomat ) AS nombrecompleto"),
                         'creditocliente.limite as limite', 'creditocliente.canadeudada as cantidadadeudada' )
               ->where('cliente.idcliente', $idcliente)
               ->limit(1)
               ->get();

               $nombrecompleto = $informacioncliente[0]->{'nombrecompleto'};
               $limite = bcadd( $informacioncliente[0]->{'limite'}, '0', 2 );
               $cantidadadeudada = bcadd ( $informacioncliente[0]->{'cantidadadeudada'}, '0', 2 );

               $cantidadadeudadaold = bcadd( $cantidadadeudada - $total, '0', 2);

               $filasinformacion .=  '<tr class="filanobordeprincipal">
                                        <td class="cantidad"> </td>
                                        <td class="producto">SUBTOTAL</td>
                                        <td class="precio"> </td>
                                        <td class="importe"> $'. $subtotal .'</td>
                                   </tr>
                                   <tr class="filanoborde">
                                        <td class="cantidad"> </td>
                                        <td class="producto">DESCUENTO</td>
                                        <td class="precio"> </td>
                                        <td class="importe"> $'. $descuento .'</td>
                                   </tr>
                                   <tr class="filanoborde">
                                        <td class="cantidad"> </td>
                                        <td class="producto">TOTAL</td>
                                        <td class="precio"> </td>
                                        <td class="importe"> $'. $total .'</td>
                                   </tr>
                                   <tr class="filanoborde">
                                        <td colspan="4">                     </td>
                                   </tr>
                                   <tr class="filanoborde">
                                        <td colspan ="4" class="textoventacentrado">     VENTA PAGADA A CRÉDITO POR: </td>
                                   </tr>
                                   <tr class="filanoborde">
                                        <td colspan ="4" class="textocentrado">      '. $nombrecompleto.' </td>
                                   </tr>

                                   <tr class="filanoborde">
                                        <td colspan="2" class="textoinfo">Limite crédito</td>
                                        <td class="precio"> </td>
                                        <td class="texto">      $'. $limite .'</td>
                                   </tr>
                                   <tr class="filanoborde">
                                        <td colspan="2" colspan="2" class="textoinfo">Saldo ant.</td>
                                        <td class="precio"> </td>
                                        <td class="texto">      $'. $cantidadadeudadaold .'</td>
                                   </tr>
                                   <tr class="filanoborde">
                                        <td colspan="2" class="textoinfo">Saldo act.</td>
                                        <td class="precio"> </td>
                                        <td class="texto">      $'. $cantidadadeudada .'</td>
               </tr>';

          } else if( $tipopago == 3 ) {

               $informacioncliente = DB::table('cliente')
               ->join('creditocliente', 'cliente.idcliente', '=', 'creditocliente.idcliente')
               ->select( 'creditocliente.idcreditocliente', 'cliente.idcliente',
                         DB::raw( "CONCAT(nombre,' ', apellidopat,' ', apellidomat ) AS nombrecompleto"),
                         'creditocliente.limite as limite', 'creditocliente.canadeudada as cantidadadeudada' )
               ->where('cliente.idcliente', $idcliente)
               ->limit(1)
               ->get();

               $nombrecompleto = $informacioncliente[0]->{'nombrecompleto'};
               $limite = bcadd( $informacioncliente[0]->{'limite'}, '0', 2 );
               $cantidadadeudada = bcadd ( $informacioncliente[0]->{'cantidadadeudada'}, '0', 2 );

               $cantidadpagadaefectivo = bcadd( $efectivo, '0', 2);
               $cantidadpagadaacredito = bcadd( $total - $efectivo, '0', 2);

               $cantidadadeudadaold = bcadd( $cantidadadeudada - $cantidadpagadaacredito, '0', 2);
               //$cantidadadeudadaold = bcadd( $cantidadadeudada - $cantidadpagadaacredito, '0', 2);

               $filasinformacion .=  '<tr class="filanobordeprincipal">
                                        <td class="cantidad"> </td>
                                        <td class="producto">SUBTOTAL</td>
                                        <td class="precio"> </td>
                                        <td class="importe"> $'. $subtotal .'</td>
                                   </tr>
                                   <tr class="filanoborde">
                                        <td class="cantidad"> </td>
                                        <td class="producto">DESCUENTO</td>
                                        <td class="precio"> </td>
                                        <td class="importe"> $'. $descuento .'</td>
                                   </tr>
                                   <tr class="filanoborde">
                                        <td class="cantidad"> </td>
                                        <td class="producto">TOTAL</td>
                                        <td class="precio"> </td>
                                        <td class="importe"> $'. $total .'</td>
                                   </tr>
                                   <tr class="filanoborde">
                                        <td colspan="4">                     </td>
                                   </tr>
                                   <tr class="filanoborde">
                                        <td colspan ="4" class="textoventacentrado">     VENTA PAGADA A CRÉDITO Y EFECTIVO POR: </td>
                                   </tr>
                                   <tr class="filanoborde">
                                        <td colspan ="4" class="textocentrado">      '. $nombrecompleto.' </td>
                                   </tr>

                                   <tr class="filanoborde">
                                        <td colspan="2" class="textoinfo">Limite crédito</td>
                                        <td class="precio"> </td>
                                        <td class="texto">      $'. $limite .'</td>
                                   </tr>
                                   <tr class="filanoborde">
                                        <td colspan="2" colspan="2" class="textoinfo">Saldo ant.</td>
                                        <td class="precio"> </td>
                                        <td class="texto">      $'. $cantidadadeudadaold .'</td>
                                   </tr>
                                   <tr class="filanoborde">
                                        <td colspan="2" colspan="2" class="textoinfo">Importe pagado en efectivo.</td>
                                        <td class="precio"> </td>
                                        <td class="texto">      $'. $cantidadpagadaefectivo .'</td>
                                   </tr>
                                   <tr class="filanoborde">
                                        <td colspan="2" colspan="2" class="textoinfo">Importe pagado en crédito.</td>
                                        <td class="precio"> </td>
                                        <td class="texto">      $'. $cantidadpagadaacredito .'</td>
                                   </tr>
                                   <tr class="filanoborde">
                                        <td colspan="2" class="textoinfo">Saldo act.</td>
                                        <td class="precio"> </td>
                                        <td class="texto">      $'. $cantidadadeudada .'</td>
               </tr>';

          }


          //Obtener el id de la última venta
          $consulta = Venta_Modelo::select('idVenta')
          ->orderBy('idVenta', 'desc')
          ->limit(1)
          ->get();

          $idventa = $consulta[0]->{'idVenta'};

          $today = Carbon::now()->format('d-m-Y');

          $pdf = \PDF::setOptions([
               'images' => true,
               'enable-javascript' => true,
               'javascript-delay' => 5000,
               'enable-smart-shrinking' => true,
               'no-stop-slow-scripts' => true
          ]);

          $pdf->loadHTML(
               '<html>
                    <head>
                         <link rel="stylesheet" href="css/estiloticketventa.css">
                    </head>

               <body>
                    <div class="ticket">
                         <img src="images/Ticket/SuperPLES.png" width="35" height="35">
                         <p class="centrado">TICKET DE VENTA Num '. $idventa .'      ' . $today .'</p>

                         <table>
                              <thead>
                                   <tr>
                                   <th class="cantidad">CANT</th>
                                   <th class="producto">PRODUCTO</th>
                                   <th class="precio">PRECIO</th>
                                   <th class="importe">IMPORTE</th>
                                   </tr>
                              </thead>
                              <tbody>
                                   ' . $filasproductos . '
                              </tbody>
                         </table>

                         <table>
                              <thead>
                                   <tr>
                                   <th class="cantidad"> </th>
                                   <th class="producto"> </th>
                                   <th class="precio"> </th>
                                   <th class="importe"> </th>
                                   </tr>
                              </thead>
                              <tbody>
                                   '. $filasinformacion .'
                              </tbody>
                         </table>
                         <br>
                         <br>
                         <p class="centrado"> '. $textototal .'</p>
                         <p class="centrado">¡GRACIAS POR SU COMPRA! <br>www.plataformalatinoamericana.com </p>
                    </div>
               </body>

          </html>');
          
          $usuariowindows =  get_current_user();
          //echo "USUARIO: ", $usuariowindows;

          //$pdf->setPaper('a4', 'landscape')->setWarnings(false); 
          $pdf->setPaper( array( 0, 0, 500, 1000 ) );
   
          /*
               if( !file_exists( "C:/Users/" . $usuariowindows . "/Downloads/Tickets/TicketVenta " ) ) {

                    mkdir( "C:/Users/" . $usuariowindows . "/Downloads/Tickets/TicketVenta " , 0755, false );

               }
          */

          //$pdf->save( 'TicketVentaNum: '. $idventa. ' - '. $today .'.pdf');
          //$pdf->save('C:\Users\lalsh\Downloads\TicketVenta '. $idventa .'.pdf');
          $pdf->save("C:/Users/" . $usuariowindows ."/Downloads/TicketVenta ". $idventa .".pdf");
          //$pdf->save( "C:/Users/" . $usuariowindows . "/Downloads/Tickets/TicketVenta ". $idventa . ".pdf" );

     }

     //EN CASO DE QUE SE HAYA COMPRADO ALGUNA PROMOCION
     public function imprimirticketventadescuento( $arrayproductos, $arraypromociones, $total, $descuento, $efectivo, $tipopago, $idcliente, $textototal ) {

          //$arrayproductos = json_decode( $request->DatosProductos );
          //$arraypromociones = json_decode( $request->DatosPromociones );
          print_r( $arrayproductos );

          $filasproductos = "";
          $filasinformacion = "";

          foreach( $arrayproductos as $itemproducto ) {

               $banderapromocionproducto = false;
               
               $codigoproducto = $itemproducto->{'ColumnaCodigos'};

               $precioproducto =  bcadd( $itemproducto->{'ColumnaPrecios'}, '0', 2 );
               $importeproducto =  bcadd( $itemproducto->{'ColumnaImportes'}, '0', 2 );

               $preciopromocion = 0;
               $cantidadpromocion = 0;
               $descripcionpromocion = "";

               foreach( $arraypromociones as $itempromociones ) {

                    $codigoproductopromocion = $itempromociones->{'ColumnaCodigos'};

                    if( $codigoproducto == $codigoproductopromocion ) {
                         $banderapromocionproducto = true;

                         $preciopromocion = bcadd( $itempromociones->{'ColumnaPrecios'}, '0', 2 );;
                         $cantidadpromocion = $itempromociones->{'ColumnaCantidades'};
                         $descripcionpromocion = $itempromociones->{'ColumnaDescripciones'};

                    }

               }

               if( $banderapromocionproducto ) {

                    $importepromocion = $preciopromocion * $cantidadpromocion;
                    $descuentoaplicadopromocion = $importeproducto - $importepromocion;

                    $filasproductos .=   '<tr>
                                             <td class="cantidad"> '. $itemproducto->{'ColumnaCantidades'} .'</td>
                                             <td class="producto">'.  $itemproducto->{'ColumnaDescripciones'} .'</td>
                                             <td class="precio">  $'. $precioproducto .'</td>
                                             <td class="importe"> $'. $importeproducto .'</td>
                                        </tr>

                                        <tr>
                                             <td colspan="3"> '. $descripcionpromocion .'</td>
                                             <td class="importe"> -$'. $importepromocion .'</td>
                                        </tr>

                                        <tr>
                                             <td class="cantidad"> </td>
                                             <td class="producto"> </td>
                                             <td class="precio"> </td>
                                             <td class="importe"> $'. $descuentoaplicadopromocion .'</td>
                                        </tr>';

               } else {

                    $filasproductos .=   '<tr>
                                             <td class="cantidad"> '. $itemproducto->{'ColumnaCantidades'} .'</td>
                                             <td class="producto">'.  $itemproducto->{'ColumnaDescripciones'} .'</td>
                                             <td class="precio">  $'. $precioproducto .'</td>
                                             <td class="importe"> $'. $importeproducto .'</td>
                                        </tr>';

               }

               echo "\nIteración";

          }

          $subtotal = bcadd( ( $total + $descuento ), '0', 2 );
          $cambio   = bcadd( ( $efectivo -  $total ), '0', 2 );

          if( $tipopago == 1 ) {

               $filasinformacion .=  '<tr class="filanobordeprincipal">
                                        <td class="cantidad"> </td>
                                        <td class="producto">SUBTOTAL</td>
                                        <td class="precio"> </td>
                                        <td class="importe"> $'. $subtotal .'</td>
                                   </tr>
                                   <tr class="filanoborde">
                                        <td class="cantidad"> </td>
                                        <td class="producto">DESCUENTO</td>
                                        <td class="precio"> </td>
                                        <td class="importe"> $'. $descuento .'</td>
                                   </tr>
                                   <tr class="filanoborde">
                                        <td class="cantidad"> </td>
                                        <td class="producto">TOTAL</td>
                                        <td class="precio"> </td>
                                        <td class="importe"> $'. $total .'</td>
                                   </tr>
                                   <tr class="filanoborde">
                                        <td class="cantidad"> </td>
                                        <td class="producto">PAGO</td>
                                        <td class="precio"> </td>
                                        <td class="importe"> $'. $efectivo .'</td>
                                   </tr>
                                   <tr class="filanoborde">
                                        <td class="cantidad"> </td>
                                        <td class="producto">CAMBIO</td>
                                        <td class="precio"> </td>
                                        <td class="importe"> $'. $cambio .'</td>
               </tr>';          

          } else if( $tipopago == 2 ) {

               $informacioncliente = DB::table('cliente')
               ->join('creditocliente', 'cliente.idcliente', '=', 'creditocliente.idcliente')
               ->select( 'creditocliente.idcreditocliente', 'cliente.idcliente',
                         DB::raw( "CONCAT(nombre,' ', apellidopat,' ', apellidomat ) AS nombrecompleto"),
                         'creditocliente.limite as limite', 'creditocliente.canadeudada as cantidadadeudada' )
               ->where('cliente.idcliente', $idcliente)
               ->limit(1)
               ->get();

               $nombrecompleto = $informacioncliente[0]->{'nombrecompleto'};
               $limite = bcadd( $informacioncliente[0]->{'limite'}, '0', 2 );
               $cantidadadeudada = bcadd ( $informacioncliente[0]->{'cantidadadeudada'}, '0', 2 );

               $cantidadadeudadaold = bcadd( $cantidadadeudada - $total, '0', 2);

               $filasinformacion .=  '<tr class="filanobordeprincipal">
                                        <td class="cantidad"> </td>
                                        <td class="producto">SUBTOTAL</td>
                                        <td class="precio"> </td>
                                        <td class="importe"> $'. $subtotal .'</td>
                                   </tr>
                                   <tr class="filanoborde">
                                        <td class="cantidad"> </td>
                                        <td class="producto">DESCUENTO</td>
                                        <td class="precio"> </td>
                                        <td class="importe"> $'. $descuento .'</td>
                                   </tr>
                                   <tr class="filanoborde">
                                        <td class="cantidad"> </td>
                                        <td class="producto">TOTAL</td>
                                        <td class="precio"> </td>
                                        <td class="importe"> $'. $total .'</td>
                                   </tr>
                                   <tr class="filanoborde">
                                        <td colspan="4">                     </td>
                                   </tr>
                                   <tr class="filanoborde">
                                        <td colspan ="4" class="textoventacentrado">     VENTA PAGADA A CRÉDITO POR: </td>
                                   </tr>
                                   <tr class="filanoborde">
                                        <td colspan ="4" class="textocentrado">      '. $nombrecompleto.' </td>
                                   </tr>

                                   <tr class="filanoborde">
                                        <td colspan="2" class="textoinfo">Limite crédito</td>
                                        <td class="precio"> </td>
                                        <td class="texto">      $'. $limite .'</td>
                                   </tr>
                                   <tr class="filanoborde">
                                        <td colspan="2" colspan="2" class="textoinfo">Saldo ant.</td>
                                        <td class="precio"> </td>
                                        <td class="texto">      $'. $cantidadadeudadaold .'</td>
                                   </tr>
                                   <tr class="filanoborde">
                                        <td colspan="2" class="textoinfo">Saldo act.</td>
                                        <td class="precio"> </td>
                                        <td class="texto">      $'. $cantidadadeudada .'</td>
               </tr>';

          } else if( $tipopago == 3 ) {

               $informacioncliente = DB::table('cliente')
               ->join('creditocliente', 'cliente.idcliente', '=', 'creditocliente.idcliente')
               ->select( 'creditocliente.idcreditocliente', 'cliente.idcliente',
                         DB::raw( "CONCAT(nombre,' ', apellidopat,' ', apellidomat ) AS nombrecompleto"),
                         'creditocliente.limite as limite', 'creditocliente.canadeudada as cantidadadeudada' )
               ->where('cliente.idcliente', $idcliente)
               ->limit(1)
               ->get();

               $nombrecompleto = $informacioncliente[0]->{'nombrecompleto'};
               $limite = bcadd( $informacioncliente[0]->{'limite'}, '0', 2 );
               $cantidadadeudada = bcadd ( $informacioncliente[0]->{'cantidadadeudada'}, '0', 2 );

               $cantidadpagadaefectivo = bcadd( $efectivo, '0', 2);
               $cantidadpagadaacredito = bcadd( $total - $efectivo, '0', 2);

               $cantidadadeudadaold = bcadd( $cantidadadeudada - $cantidadpagadaacredito, '0', 2);
               //$cantidadadeudadaold = bcadd( $cantidadadeudada - $cantidadpagadaacredito, '0', 2);

               $filasinformacion .=  '<tr class="filanobordeprincipal">
                                        <td class="cantidad"> </td>
                                        <td class="producto">SUBTOTAL</td>
                                        <td class="precio"> </td>
                                        <td class="importe"> $'. $subtotal .'</td>
                                   </tr>
                                   <tr class="filanoborde">
                                        <td class="cantidad"> </td>
                                        <td class="producto">DESCUENTO</td>
                                        <td class="precio"> </td>
                                        <td class="importe"> $'. $descuento .'</td>
                                   </tr>
                                   <tr class="filanoborde">
                                        <td class="cantidad"> </td>
                                        <td class="producto">TOTAL</td>
                                        <td class="precio"> </td>
                                        <td class="importe"> $'. $total .'</td>
                                   </tr>
                                   <tr class="filanoborde">
                                        <td colspan="4">                     </td>
                                   </tr>
                                   <tr class="filanoborde">
                                        <td colspan ="4" class="textoventacentrado">     VENTA PAGADA A CRÉDITO Y EFECTIVO POR: </td>
                                   </tr>
                                   <tr class="filanoborde">
                                        <td colspan ="4" class="textocentrado">      '. $nombrecompleto.' </td>
                                   </tr>

                                   <tr class="filanoborde">
                                        <td colspan="2" class="textoinfo">Limite crédito</td>
                                        <td class="precio"> </td>
                                        <td class="texto">      $'. $limite .'</td>
                                   </tr>
                                   <tr class="filanoborde">
                                        <td colspan="2" colspan="2" class="textoinfo">Saldo ant.</td>
                                        <td class="precio"> </td>
                                        <td class="texto">      $'. $cantidadadeudadaold .'</td>
                                   </tr>
                                   <tr class="filanoborde">
                                        <td colspan="2" colspan="2" class="textoinfo">Importe pagado en efectivo.</td>
                                        <td class="precio"> </td>
                                        <td class="texto">      $'. $cantidadpagadaefectivo .'</td>
                                   </tr>
                                   <tr class="filanoborde">
                                        <td colspan="2" colspan="2" class="textoinfo">Importe pagado en crédito.</td>
                                        <td class="precio"> </td>
                                        <td class="texto">      $'. $cantidadpagadaacredito .'</td>
                                   </tr>
                                   <tr class="filanoborde">
                                        <td colspan="2" class="textoinfo">Saldo act.</td>
                                        <td class="precio"> </td>
                                        <td class="texto">      $'. $cantidadadeudada .'</td>
               </tr>';

          }


          //Obtener el id de la última venta
          $consulta = Venta_Modelo::select('idVenta')
          ->orderBy('idVenta', 'desc')
          ->limit(1)
          ->get();

          $idventa = $consulta[0]->{'idVenta'};

          $today = Carbon::now()->format('d-m-Y');

          $pdf = \PDF::setOptions([
               'images' => true,
               'enable-javascript' => true,
               'javascript-delay' => 5000,
               'enable-smart-shrinking' => true,
               'no-stop-slow-scripts' => true
          ]);

          $pdf->loadHTML(
               '<html>
                    <head>
                         <link rel="stylesheet" href="css/estiloticketventa.css">
                    </head>

               <body>
                    <div class="ticket">
                         <img src="images/Ticket/SuperPLES.png" width="35" height="35">
                         <p class="centrado">TICKET DE VENTA Num '. $idventa .'      ' . $today .'</p>

                         <table>
                              <thead>
                                   <tr>
                                   <th class="cantidad">CANT</th>
                                   <th class="producto">PRODUCTO</th>
                                   <th class="precio">PRECIO</th>
                                   <th class="importe">IMPORTE</th>
                                   </tr>
                              </thead>
                              <tbody>
                                   ' . $filasproductos . '
                              </tbody>
                         </table>

                         <table>
                              <thead>
                                   <tr>
                                   <th class="cantidad"> </th>
                                   <th class="producto"> </th>
                                   <th class="precio"> </th>
                                   <th class="importe"> </th>
                                   </tr>
                              </thead>
                              <tbody>
                                   '. $filasinformacion .'
                              </tbody>
                         </table>
                         <br>
                         <br>
                         <p class="centrado"> '. $textototal .'</p>
                         <p class="centrado">¡GRACIAS POR SU COMPRA! <br>www.plataformalatinoamericana.com </p>
                    </div>
               </body>

          </html>');
          
          $usuariowindows =  get_current_user();
          //echo "USUARIO: ", $usuariowindows;

          //$pdf->setPaper('a4', 'landscape')->setWarnings(false); 
          $pdf->setPaper( array( 0, 0, 500, 1000 ) );
   
          /*
               if( !file_exists( "C:/Users/" . $usuariowindows . "/Downloads/Tickets/TicketVenta " ) ) {

                    mkdir( "C:/Users/" . $usuariowindows . "/Downloads/Tickets/TicketVenta " , 0755, false );

               }
          */

          //$pdf->save( 'TicketVentaNum: '. $idventa. ' - '. $today .'.pdf');
          //$pdf->save('C:\Users\lalsh\Downloads\TicketVenta '. $idventa .'.pdf');
          $pdf->save("C:/Users/" . $usuariowindows ."/Downloads/TicketVenta ". $idventa .".pdf");
          //$pdf->save( "C:/Users/" . $usuariowindows . "/Downloads/Tickets/TicketVenta ". $idventa . ".pdf" );

     }

     //PARA LA IMPRESORA TERMICA
     public function generarticketventalaravel( $arrayproductos, $total, $descuento, $efectivo, $tipopago, $idcliente, $textototal ) {

          $subtotal = bcadd( ( $total + $descuento ), '0', 2 );
          $cambio   = bcadd( ( $efectivo -  $total ), '0', 2 );

          //Obtener el id de la última venta
          $consulta = Venta_Modelo::select('idVenta')
          ->orderBy('idVenta', 'desc')
          ->limit(1)
          ->get();

          $idventa = $consulta[0]->{'idVenta'};

          $today = Carbon::now()->format('d-m-Y');


          echo "\nConfigurando generar ticket";
          $nombre_impresora = "POS-58";
          $connector = new WindowsPrintConnector($nombre_impresora);
          $printer = new Printer($connector);

          echo "\nInicnando generar ticket";
          $nombreImpresora = "POS-58";
          $connector = new WindowsPrintConnector($nombreImpresora);
          $ticket = new Printer($connector);
          $ticket->setJustification(Printer::JUSTIFY_CENTER);
          $ticket->setTextSize(2, 2);

          $imagenLogo = EscposImage::load( "images/Ticket/SuperPLES.png" );

          $ticket->graphics( $imagenLogo );
          $ticket->text("TICKET DE VENTA Num" + $idventa + "     " + $today +"\n");
          $ticket->text("CANT       PRODUCTO       PRECIO       IMPORTE\n");

          foreach( $arrayproductos as $itemproducto ) {

               $precioproducto =  bcadd( $itemproducto->{'ColumnaPrecios'}, '0', 2 );
               $importeproducto =  bcadd( $itemproducto->{'ColumnaImportes'}, '0', 2 );

               $ticket->text( $itemproducto->{'ColumnaCantidades'} + "       "  +$itemproducto->{'ColumnaDescripciones'} + "       " + $precioproducto + "       " + $importeproducto + "\n");

          }

          if( $tipopago == 1 ) {

               $ticket->text("           SUBTOTAL   $" + $subtotal + "\n" );
               $ticket->text("           DESCUENTO  $" + $descuento + "\n" );
               $ticket->text("           TOTAL      $" + $total + "\n" );
               $ticket->text("           PAGO       $" + $efectivo + "\n" );
               $ticket->text("           CAMBIO     $" + $cambio + "\n" );
               $ticket->text(" \n" );
               $ticket->text(" \n" );
               $ticket->text(" " + $textototal + "\n" );

          } else if( $tipopago == 2 ) {

               $informacioncliente = DB::table('cliente')
               ->join('creditocliente', 'cliente.idcliente', '=', 'creditocliente.idcliente')
               ->select( 'creditocliente.idcreditocliente', 'cliente.idcliente',
                         DB::raw( "CONCAT(nombre,' ', apellidopat,' ', apellidomat ) AS nombrecompleto"),
                         'creditocliente.limite as limite', 'creditocliente.canadeudada as cantidadadeudada' )
               ->where('cliente.idcliente', $idcliente)
               ->limit(1)
               ->get();

               $nombrecompleto = $informacioncliente[0]->{'nombrecompleto'};
               $limite = bcadd( $informacioncliente[0]->{'limite'}, '0', 2 );
               $cantidadadeudada = bcadd ( $informacioncliente[0]->{'cantidadadeudada'}, '0', 2 );

               $cantidadadeudadaold = bcadd( $cantidadadeudada - $total, '0', 2);

               $ticket->text("           SUBTOTAL   $" + $subtotal + "\n" );
               $ticket->text("           DESCUENTO  $" + $descuento + "\n" );
               $ticket->text("           TOTAL      $" + $total + "\n" );
               $ticket->text(" VENTA PAGADA A CRÉDITO POR: \n");
               $ticket->text(" " +$nombrecompleto +"\n");
               $ticket->text("           Limite crédito      $" + $limite + "\n" );
               $ticket->text("           Saldo ant.      $" + $cantidadadeudadaold + "\n" );
               $ticket->text("           Saldo act.      $" + $cantidadadeudada + "\n" );
               $ticket->text(" \n" );
               $ticket->text(" \n" );
               $ticket->text(" " + $textototal + "\n" );

          } else if( $tipopago == 3 ) {

               $informacioncliente = DB::table('cliente')
               ->join('creditocliente', 'cliente.idcliente', '=', 'creditocliente.idcliente')
               ->select( 'creditocliente.idcreditocliente', 'cliente.idcliente',
                         DB::raw( "CONCAT(nombre,' ', apellidopat,' ', apellidomat ) AS nombrecompleto"),
                         'creditocliente.limite as limite', 'creditocliente.canadeudada as cantidadadeudada' )
               ->where('cliente.idcliente', $idcliente)
               ->limit(1)
               ->get();

               $nombrecompleto = $informacioncliente[0]->{'nombrecompleto'};
               $limite = bcadd( $informacioncliente[0]->{'limite'}, '0', 2 );
               $cantidadadeudada = bcadd ( $informacioncliente[0]->{'cantidadadeudada'}, '0', 2 );
               

               $nombrecompleto = $informacioncliente[0]->{'nombrecompleto'};
               $limite = bcadd( $informacioncliente[0]->{'limite'}, '0', 2 );
               $cantidadadeudada = bcadd ( $informacioncliente[0]->{'cantidadadeudada'}, '0', 2 );

               $cantidadpagadaefectivo = bcadd( $efectivo, '0', 2);
               $cantidadpagadaacredito = bcadd( $total - $efectivo, '0', 2);

               $cantidadadeudadaold = bcadd( $cantidadadeudada - $cantidadpagadaacredito, '0', 2);
               //$cantidadadeudadaold = bcadd( $cantidadadeudada - $cantidadpagadaacredito, '0', 2);

               //$cantidadadeudadaold = bcadd( $cantidadadeudada - $total, '0', 2);

               $ticket->text("           SUBTOTAL   $" + $subtotal + "\n" );
               $ticket->text("           DESCUENTO  $" + $descuento + "\n" );
               $ticket->text("           TOTAL      $" + $total + "\n" );
               $ticket->text(" VENTA PAGADA A CRÉDITO Y EFECTIVO POR: \n");
               $ticket->text(" " +$nombrecompleto +"\n");
               $ticket->text("           Limite crédito      $" + $limite + "\n" );
               $ticket->text("           Saldo ant.      $" + $cantidadadeudadaold + "\n" );
               $ticket->text("           Importe pagado en efectivo.      $" + $cantidadpagadaefectivo + "\n" );
               $ticket->text("           Importe pagado en crédito.      $" + $cantidadpagadaacredito + "\n" );               
               $ticket->text("           Saldo act.      $" + $cantidadadeudada + "\n" );
               $ticket->text(" \n" );
               $ticket->text(" \n" );
               $ticket->text(" " + $textototal + "\n" );

          }

          $ticket->text("          ¡GRACIAS POR SU COMPRA!");
          $ticket->text("     www.plataformalatinoamericana.com ");

          $ticket->feed(5);
          $ticket->close();
          echo "\nTermino generar ticket";


     }
     
     //EN CASO DE QUE SE HAYA COMPRADO ALGUNA PROMOCION
     public function generarticketventalaraveldescuento( $arrayproductos, $arraypromociones,  $total, $descuento, $efectivo, $tipopago, $idcliente, $textototal ) {

          $subtotal = bcadd( ( $total + $descuento ), '0', 2 );
          $cambio   = bcadd( ( $efectivo -  $total ), '0', 2 );

          //Obtener el id de la última venta
          $consulta = Venta_Modelo::select('idVenta')
          ->orderBy('idVenta', 'desc')
          ->limit(1)
          ->get();

          $idventa = $consulta[0]->{'idVenta'};

          $today = Carbon::now()->format('d-m-Y');


          echo "\nConfigurando generar ticket";
          $nombre_impresora = "POS-58";
          $connector = new WindowsPrintConnector($nombre_impresora);
          $printer = new Printer($connector);

          echo "\nInicnando generar ticket";
          $nombreImpresora = "POS-58";
          $connector = new WindowsPrintConnector($nombreImpresora);
          $ticket = new Printer($connector);
          $ticket->setJustification(Printer::JUSTIFY_CENTER);
          $ticket->setTextSize(2, 2);

          $imagenLogo = EscposImage::load( "images/Ticket/SuperPLES.png" );

          $ticket->graphics( $imagenLogo );
          $ticket->text("TICKET DE VENTA Num" + $idventa + "     " + $today +"\n");
          $ticket->text("CANT       PRODUCTO       PRECIO       IMPORTE\n");

          foreach( $arrayproductos as $itemproducto ) {

               $banderapromocionproducto = false;
               
               $codigoproducto = $itemproducto->{'ColumnaCodigos'};

               $precioproducto =  bcadd( $itemproducto->{'ColumnaPrecios'}, '0', 2 );
               $importeproducto =  bcadd( $itemproducto->{'ColumnaImportes'}, '0', 2 );

               $preciopromocion = 0;
               $cantidadpromocion = 0;
               $descripcionpromocion = "";

               foreach( $arraypromociones as $itempromociones ) {

                    $codigoproductopromocion = $itempromociones->{'ColumnaCodigos'};

                    if( $codigoproducto == $codigoproductopromocion ) {
                         $banderapromocionproducto = true;

                         $preciopromocion = bcadd( $itempromociones->{'ColumnaPrecios'}, '0', 2 );;
                         $cantidadpromocion = $itempromociones->{'ColumnaCantidades'};
                         $descripcionpromocion = $itempromociones->{'ColumnaDescripciones'};

                    }

               }

               if( $banderapromocionproducto ) {

                    $importepromocion = $preciopromocion * $cantidadpromocion;
                    $descuentoaplicadopromocion = $importeproducto - $importepromocion;

                    $ticket->text( $itemproducto->{'ColumnaCantidades'} + "       "  +$itemproducto->{'ColumnaDescripciones'} + "       " + $precioproducto + "       " + $importeproducto + "\n");
                    $ticket->text( $descripcionpromocion + "              "  + $importepromocion  + "\n");
                    $ticket->text(  "                            " + $descuentoaplicadopromocion + "\n");

               } else {
                    
                    $ticket->text( $itemproducto->{'ColumnaCantidades'} + "       "  +$itemproducto->{'ColumnaDescripciones'} + "       " + $precioproducto + "       " + $importeproducto + "\n");

               }


          }

          if( $tipopago == 1 ) {

               $ticket->text("           SUBTOTAL   $" + $subtotal + "\n" );
               $ticket->text("           DESCUENTO  $" + $descuento + "\n" );
               $ticket->text("           TOTAL      $" + $total + "\n" );
               $ticket->text("           PAGO       $" + $efectivo + "\n" );
               $ticket->text("           CAMBIO     $" + $cambio + "\n" );
               $ticket->text(" \n" );
               $ticket->text(" \n" );
               $ticket->text(" " + $textototal + "\n" );

          } else if( $tipopago == 2 ) {

               $informacioncliente = DB::table('cliente')
               ->join('creditocliente', 'cliente.idcliente', '=', 'creditocliente.idcliente')
               ->select( 'creditocliente.idcreditocliente', 'cliente.idcliente',
                         DB::raw( "CONCAT(nombre,' ', apellidopat,' ', apellidomat ) AS nombrecompleto"),
                         'creditocliente.limite as limite', 'creditocliente.canadeudada as cantidadadeudada' )
               ->where('cliente.idcliente', $idcliente)
               ->limit(1)
               ->get();

               $nombrecompleto = $informacioncliente[0]->{'nombrecompleto'};
               $limite = bcadd( $informacioncliente[0]->{'limite'}, '0', 2 );
               $cantidadadeudada = bcadd ( $informacioncliente[0]->{'cantidadadeudada'}, '0', 2 );

               $cantidadadeudadaold = bcadd( $cantidadadeudada - $total, '0', 2);

               $ticket->text("           SUBTOTAL   $" + $subtotal + "\n" );
               $ticket->text("           DESCUENTO  $" + $descuento + "\n" );
               $ticket->text("           TOTAL      $" + $total + "\n" );
               $ticket->text(" VENTA PAGADA A CRÉDITO POR: \n");
               $ticket->text(" " +$nombrecompleto +"\n");
               $ticket->text("           Limite crédito      $" + $limite + "\n" );
               $ticket->text("           Saldo ant.      $" + $cantidadadeudadaold + "\n" );
               $ticket->text("           Saldo act.      $" + $cantidadadeudada + "\n" );
               $ticket->text(" \n" );
               $ticket->text(" \n" );
               $ticket->text(" " + $textototal + "\n" );

          } else if( $tipopago == 3 ) {

               $informacioncliente = DB::table('cliente')
               ->join('creditocliente', 'cliente.idcliente', '=', 'creditocliente.idcliente')
               ->select( 'creditocliente.idcreditocliente', 'cliente.idcliente',
                         DB::raw( "CONCAT(nombre,' ', apellidopat,' ', apellidomat ) AS nombrecompleto"),
                         'creditocliente.limite as limite', 'creditocliente.canadeudada as cantidadadeudada' )
               ->where('cliente.idcliente', $idcliente)
               ->limit(1)
               ->get();

               $nombrecompleto = $informacioncliente[0]->{'nombrecompleto'};
               $limite = bcadd( $informacioncliente[0]->{'limite'}, '0', 2 );
               $cantidadadeudada = bcadd ( $informacioncliente[0]->{'cantidadadeudada'}, '0', 2 );
               

               $nombrecompleto = $informacioncliente[0]->{'nombrecompleto'};
               $limite = bcadd( $informacioncliente[0]->{'limite'}, '0', 2 );
               $cantidadadeudada = bcadd ( $informacioncliente[0]->{'cantidadadeudada'}, '0', 2 );

               $cantidadpagadaefectivo = bcadd( $efectivo, '0', 2);
               $cantidadpagadaacredito = bcadd( $total - $efectivo, '0', 2);

               $cantidadadeudadaold = bcadd( $cantidadadeudada - $cantidadpagadaacredito, '0', 2);
               //$cantidadadeudadaold = bcadd( $cantidadadeudada - $cantidadpagadaacredito, '0', 2);

               //$cantidadadeudadaold = bcadd( $cantidadadeudada - $total, '0', 2);

               $ticket->text("           SUBTOTAL   $" + $subtotal + "\n" );
               $ticket->text("           DESCUENTO  $" + $descuento + "\n" );
               $ticket->text("           TOTAL      $" + $total + "\n" );
               $ticket->text(" VENTA PAGADA A CRÉDITO Y EFECTIVO POR: \n");
               $ticket->text(" " +$nombrecompleto +"\n");
               $ticket->text("           Limite crédito      $" + $limite + "\n" );
               $ticket->text("           Saldo ant.      $" + $cantidadadeudadaold + "\n" );
               $ticket->text("           Importe pagado en efectivo.      $" + $cantidadpagadaefectivo + "\n" );
               $ticket->text("           Importe pagado en crédito.      $" + $cantidadpagadaacredito + "\n" );               
               $ticket->text("           Saldo act.      $" + $cantidadadeudada + "\n" );
               $ticket->text(" \n" );
               $ticket->text(" \n" );
               $ticket->text(" " + $textototal + "\n" );

          }

          $ticket->text("          ¡GRACIAS POR SU COMPRA!");
          $ticket->text("     www.plataformalatinoamericana.com ");

          $ticket->feed(5);
          $ticket->close();
          echo "\nTermino generar ticket";


     }

     
                    //CORTES DIA

     //FUNCION VISTA CORTEDIA (VENTAS Y COMPRAS)
     public function cortediacaja() {
          

          $fechaactual = Carbon::now()->format('d-m-Y');

          $idempleado = Auth::User()->idEmpleado;

          $consultaidcaja = DB::table('caja')->select('idcaja as idcaja')
                                              ->where('idempleado', $idempleado )
                                              ->get();
  
          $idcaja = $consultaidcaja[0]->{'idcaja'};

          $informacionempleado = DB::table('empleado')->select('nombre as nombre', 'apellidopat as apellidopat', 'apellidomat as apellidomat' )
                                                       ->where('idempleado', $idempleado )
                                                       ->get();
                    
          $cortediaventas = DB::table( 'venta' )
                                   ->join( 'detalleventa', 'venta.idventa', '=', 'detalleventa.idventa' )
                                   ->join( 'movimientoinventario', 'detalleventa.idmovimientoinventario', 'movimientoinventario.idmovimientoinventario')
                                   ->join( 'tipomovimiento', 'movimientoinventario.idtipomovimiento', '=', 'tipomovimiento.idtipomovimiento' )
                                   ->join( 'inventario', 'detalleventa.idproducto', '=', 'inventario.codigo' )
                                   ->join('producto', 'detalleventa.idproducto', '=', 'producto.codigo' )
                                        ->select( 'venta.idventa as id',
                                                  'detalleventa.idproducto as codigoproducto', 'detalleventa.cantidad as cantidad',
                                                  'producto.descripcion as descripcionproducto', 'movimientoinventario.PrecioCostoAct  as preciocostoproducto',
                                                  'movimientoinventario.PrecioVentaAct as precioventaproducto', 'tipomovimiento.nombre as nombretipomovimiento' )
                                        ->where( 'venta.fecha', '=', Carbon::today() )
                                        ->where( 'venta.idempleado', '=', $idempleado )
                                        ->where( 'venta.idcaja', '=', $idcaja )
                                        ->where( 'venta.estado', '=', 0 )
                                        ->orderBy('venta.idventa', 'ASC')
                                        ->get();
          
          $cortediacompras = DB::table( 'pedido' )
                                   ->join( 'detallepedido', 'pedido.idpedido', '=', 'detallepedido.idpedido' )
                                   ->join( 'movimientoinventario', 'detallepedido.idmovimientoinventario', 'movimientoinventario.idmovimientoinventario')
                                   ->join( 'inventario', 'detallepedido.codigo', '=', 'inventario.codigo' )
                                   ->join( 'tipomovimiento', 'movimientoinventario.idtipomovimiento', '=', 'tipomovimiento.idtipomovimiento' )
                                   ->join('producto', 'detallepedido.codigo', '=', 'producto.codigo' )
                                        ->select( 'pedido.idpedido as id',
                                                  'detallepedido.codigo as codigoproducto', 'detallepedido.cantidad as cantidad',
                                                  'producto.descripcion as descripcionproducto', 'movimientoinventario.PrecioCostoAct  as preciocostoproducto',
                                                  'movimientoinventario.PrecioVentaAct as precioventaproducto', 'tipomovimiento.nombre as nombretipomovimiento' )
                                        ->where( 'pedido.fecha', '=', Carbon::today() )
                                        ->where( 'pedido.idempleado', '=', $idempleado )
                                        ->where( 'pedido.estado', '=', 0 )
                                        ->orderBy('pedido.idpedido', 'ASC')
                                        ->get();

          /*
          $informacioncortedia = DB::table('movimientoinventario')
                              ->join( 'producto', 'movimientoinventario.codigo', '=', 'producto.codigo' )
                              ->join( 'tipomovimiento', 'movimientoinventario.idtipomovimiento', '=', 'tipomovimiento.idtipomovimiento' )
                              ->select('movimientoinventario.idmovimientoinventario as detalle',
                                   'movimientoinventario.idmovimientoinventario as idventa', 'movimientoinventario.cantidadmov as cantidadvendida',
                                   'producto.codigo as codigoproducto', 'producto.descripcion as descripcionproducto',
                                   'movimientoinventario.preciocostoact as preciocostoproducto', 'movimientoinventario.precioventaact as precioventaproducto',
                                   'movimientoinventario.cantidadmov as cantidad', 'tipomovimiento.nombre as nombretipomovimiento' )
                              ->where( 'movimientoinventario.idtipomovimiento', '=', 101 )
                              ->orwhere( 'movimientoinventario.idtipomovimiento', '=', 203 )
                              ->where( 'movimientoinventario.fechahora', '=', Carbon::today() )
                              ->where( 'movimientoinventario.idempleado', '=', $idempleado )
                              ->orderBy('movimientoinventario.idmovimientoinventario', 'ASC')
                              ->get();
          */

          $informacioncortedia = $cortediaventas->merge( $cortediacompras );

          $consultatotalventas = DB::table( 'venta' )
                              ->select( DB::raw( "SUM( venta.total ) as totalventas" ) )
                              ->where( 'venta.fecha', '=', Carbon::today() )
                              ->where( 'venta.idempleado', '=', $idempleado )
                              ->where( 'venta.estado', '=', 0 )
                              ->get();

          $consultatotalcompras = DB::table( 'pedido' )
                              ->select( DB::raw( "SUM( pedido.total ) as totalcompras" ) )
                              ->where( 'pedido.fecha', '=', Carbon::today() )
                              ->where( 'pedido.idempleado', '=', $idempleado )
                              ->where( 'pedido.estado', '=', 0 )
                              ->get();
          
          $cantidadtotalproductosvendidos = DB::table( 'venta' )
                                        ->join( 'detalleventa', 'venta.idventa', '=', 'detalleventa.idventa' )
                                        ->select( DB::raw( "SUM( detalleventa.cantidad ) as cantidadtotalproductosvendidos" ) )
                                        ->where( 'venta.fecha', '=', Carbon::today() )
                                        ->where( 'venta.idempleado', '=', $idempleado )
                                        ->where( 'venta.estado', '=', 0 )
                                        ->get();
          
          $cantidadtotalproductoscomprados = DB::table( 'pedido' )
                                        ->join( 'detallepedido', 'pedido.idpedido', '=', 'detallepedido.idpedido' )
                                        ->select( DB::raw( "SUM( detallepedido.cantidad ) as cantidadtotalproductoscomprados" ) )
                                        ->where( 'pedido.fecha', '=', Carbon::today() )
                                        ->where( 'pedido.idempleado', '=', $idempleado )
                                        ->where( 'pedido.estado', '=', 0 )
                                        ->get();
          

          
          return view('vistasventas/cortescaja/cortediacaja')->with( 'informacioncortedia', $informacioncortedia )
                                                  ->with( 'consultatotalventas', $consultatotalventas )
                                                  ->with( 'consultatotalcompras', $consultatotalcompras )
                                                  ->with( 'cantidadtotalproductosvendidos', $cantidadtotalproductosvendidos )
                                                  ->with( 'cantidadtotalproductoscomprados', $cantidadtotalproductoscomprados )
                                                  ->with( 'fechaactual', $fechaactual )
                                                  ->with( 'idcaja', $idcaja )
                                                  ->with( 'informacionempleado', $informacionempleado );

          

          return response()->json( $informacioncortedia );

     }

     //FUNCION EXPORTAR PDF CORTEDIA (VENTAS Y COMPRAS)
     public function exportarcortediacaja() {                    

          $fechaactual = Carbon::now()->format('d-m-Y');

          $idempleado = Auth::User()->idEmpleado;

          $consultaidcaja = DB::table('caja')->select('idcaja as idcaja')
                                              ->where('idempleado', $idempleado )
                                              ->get();
  
          $idcaja = $consultaidcaja[0]->{'idcaja'};

          $informacionempleado = DB::table('empleado')->select('nombre as nombre', 'apellidopat as apellidopat', 'apellidomat as apellidomat' )
                                                       ->where('idempleado', $idempleado )
                                                       ->get();
                    
          $cortediaventas = DB::table( 'venta' )
                                   ->join( 'detalleventa', 'venta.idventa', '=', 'detalleventa.idventa' )
                                   ->join( 'movimientoinventario', 'detalleventa.idmovimientoinventario', 'movimientoinventario.idmovimientoinventario')
                                   ->join( 'tipomovimiento', 'movimientoinventario.idtipomovimiento', '=', 'tipomovimiento.idtipomovimiento' )
                                   ->join( 'inventario', 'detalleventa.idproducto', '=', 'inventario.codigo' )
                                   ->join('producto', 'detalleventa.idproducto', '=', 'producto.codigo' )
                                        ->select( 'venta.idventa as id',
                                                  'detalleventa.idproducto as codigoproducto', 'detalleventa.cantidad as cantidad',
                                                  'producto.descripcion as descripcionproducto', 'movimientoinventario.PrecioCostoAct  as preciocostoproducto',
                                                  'movimientoinventario.PrecioVentaAct as precioventaproducto', 'tipomovimiento.nombre as nombretipomovimiento' )
                                        ->where( 'venta.fecha', '=', Carbon::today() )
                                        ->where( 'venta.idempleado', '=', $idempleado )
                                        ->where( 'venta.idcaja', '=', $idcaja )
                                        ->where( 'venta.estado', '=', 0 )
                                        ->orderBy('venta.idventa', 'ASC')
                                        ->get();
          
          $cortediacompras = DB::table( 'pedido' )
                                   ->join( 'detallepedido', 'pedido.idpedido', '=', 'detallepedido.idpedido' )
                                   ->join( 'movimientoinventario', 'detallepedido.idmovimientoinventario', 'movimientoinventario.idmovimientoinventario')
                                   ->join( 'inventario', 'detallepedido.codigo', '=', 'inventario.codigo' )
                                   ->join( 'tipomovimiento', 'movimientoinventario.idtipomovimiento', '=', 'tipomovimiento.idtipomovimiento' )
                                   ->join('producto', 'detallepedido.codigo', '=', 'producto.codigo' )
                                        ->select( 'pedido.idpedido as id',
                                                  'detallepedido.codigo as codigoproducto', 'detallepedido.cantidad as cantidad',
                                                  'producto.descripcion as descripcionproducto', 'movimientoinventario.PrecioCostoAct  as preciocostoproducto',
                                                  'movimientoinventario.PrecioVentaAct as precioventaproducto', 'tipomovimiento.nombre as nombretipomovimiento' )
                                        ->where( 'pedido.fecha', '=', Carbon::today() )
                                        ->where( 'pedido.idempleado', '=', $idempleado )
                                        ->where( 'pedido.estado', '=', 0 )
                                        ->orderBy('pedido.idpedido', 'ASC')
                                        ->get();
          
          
                    
          /*
          $informacioncortedia = DB::table('movimientoinventario')
                              ->join( 'producto', 'movimientoinventario.codigo', '=', 'producto.codigo' )
                              ->join( 'tipomovimiento', 'movimientoinventario.idtipomovimiento', '=', 'tipomovimiento.idtipomovimiento' )
                              ->select('movimientoinventario.idmovimientoinventario as detalle',
                                   'movimientoinventario.idmovimientoinventario as idventa', 'movimientoinventario.cantidadmov as cantidadvendida',
                                   'producto.codigo as codigoproducto', 'producto.descripcion as descripcionproducto',
                                   'movimientoinventario.preciocostoact as preciocostoproducto', 'movimientoinventario.precioventaact as precioventaproducto',
                                   'movimientoinventario.cantidadmov as cantidad', 'tipomovimiento.nombre as nombretipomovimiento' )
                              ->where( 'movimientoinventario.idtipomovimiento', '=', 101 )
                              ->orwhere( 'movimientoinventario.idtipomovimiento', '=', 203 )
                              ->where( 'movimientoinventario.fechahora', '=', Carbon::today() )
                              ->where( 'movimientoinventario.idempleado', '=', $idempleado )
                              ->orderBy('movimientoinventario.idmovimientoinventario', 'ASC')
                              ->get();
          */
          
          $informacioncortedia = $cortediaventas->merge( $cortediacompras );

          $consultatotalventas = DB::table( 'venta' )
                              ->select( DB::raw( "SUM( venta.total ) as totalventas" ) )
                              ->where( 'venta.fecha', '=', Carbon::today() )
                              ->where( 'venta.idempleado', '=', $idempleado )
                              ->where( 'venta.estado', '=', 0 )
                              ->get();

          $consultatotalcompras = DB::table( 'pedido' )
                              ->select( DB::raw( "SUM( pedido.total ) as totalcompras" ) )
                              ->where( 'pedido.fecha', '=', Carbon::today() )
                              ->where( 'pedido.idempleado', '=', $idempleado )
                              ->where( 'pedido.estado', '=', 0 )
                              ->get();
          
          $cantidadtotalproductosvendidos = DB::table( 'venta' )
                                        ->join( 'detalleventa', 'venta.idventa', '=', 'detalleventa.idventa' )
                                        ->select( DB::raw( "SUM( detalleventa.cantidad ) as cantidadtotalproductosvendidos" ) )
                                        ->where( 'venta.fecha', '=', Carbon::today() )
                                        ->where( 'venta.idempleado', '=', $idempleado )
                                        ->where( 'venta.estado', '=', 0 )
                                        ->get();
          
          $cantidadtotalproductoscomprados = DB::table( 'pedido' )
                                        ->join( 'detallepedido', 'pedido.idpedido', '=', 'detallepedido.idpedido' )
                                        ->select( DB::raw( "SUM( detallepedido.cantidad ) as cantidadtotalproductoscomprados" ) )
                                        ->where( 'pedido.fecha', '=', Carbon::today() )
                                        ->where( 'pedido.idempleado', '=', $idempleado )
                                        ->where( 'pedido.estado', '=', 0 )
                                        ->get();
                                        
          $tienda = Tienda_Modelo::select('*')->take(1)->first();
                                        
          /*
          $pdf = \PDF::setOptions([
                    'images' => true,
                    'enable-javascript' => true,
                    'javascript-delay' => 5000,
                    'enable-smart-shrinking' => true,
                    'no-stop-slow-scripts' => true
               ])
          */
          
          $pdf = \PDF::loadView('vistasventas/cortescaja/exportarcortediacaja', array(     'informacioncortedia' => $informacioncortedia, 
                                                                                     'consultatotalventas' => $consultatotalventas, 
                                                                                     'consultatotalcompras' => $consultatotalcompras,
                                                                                     'cantidadtotalproductosvendidos' => $cantidadtotalproductosvendidos,
                                                                                     'cantidadtotalproductoscomprados' => $cantidadtotalproductoscomprados,
                                                                                     'fechaactual' => $fechaactual, 
                                                                                     'tienda' => $tienda,
                                                                                     'idcaja' => $idcaja,
                                                                                     'informacionempleado' => $informacionempleado ) )
               ->setPaper('a4', 'landscape')->setWarnings(false);

          return $pdf->download( 'CorteCaja '. $fechaactual . '.pdf');

     }


     //FUNCION VISTA CORTEDIA VENTAS
     public function cortediaventas() {

          $fechaactual = Carbon::now()->format('d-m-Y');

          $idempleado = Auth::User()->idEmpleado;

          $consultaidcaja = DB::table('caja')->select('idcaja as idcaja')
                                              ->where('idempleado', $idempleado )
                                              ->get();
  
          $idcaja = $consultaidcaja[0]->{'idcaja'};

          $informacionempleado = DB::table('empleado')->select('nombre as nombre', 'apellidopat as apellidopat', 'apellidomat as apellidomat' )
                                                       ->where('idempleado', $idempleado )
                                                       ->get();

          $auxfechaactual = Carbon::today();          
          $auxfechaactual->addDays(-1); 
          //$auxfechaactual = $auxfechaactual->format('m/d/Y');
          
          $consultacambiodia = DB::table('empleadocaja')
                              ->select( DB::raw("COUNT(*) as totalconsultacambiodia"), 'idempleadocaja as idempleadocaja' )          
                              ->where('idempleado', '=', $idempleado )
                              ->where('idcaja', '=', $idcaja )
                              ->where('fecha', '=', $auxfechaactual )
                              ->where('estado', '=', 0 )
                              ->groupBy('idempleadocaja' )
                              ->get();

          if( count( $consultacambiodia ) == 1 ) {
                                        
               if( $consultacambiodia[0]->{'totalconsultacambiodia'} == 1 ) {                         
               
                    $auxidempleadocaja = $consultacambiodia[0]->{'idempleadocaja'};
     
                    DB::table('empleadocaja')
                              ->where( 'empleadocaja.idempleadocaja', '=', $auxidempleadocaja )
                              ->update( array( 'fecha' => Carbon::today() ) );
          
               }                    

          }
          

          $consultasaldoinicial = DB::table( 'empleadocaja' )
                                             ->select('saldoinicial as saldoinicial')
                                             ->where('idempleado', '=', $idempleado )
                                             ->where('idcaja', '=', $idcaja )
                                             ->where('fecha', '=', Carbon::today() )
                                             ->orderBy('idempleadocaja', 'DESC')
                                             ->get();
          

          /*
          $consultasventasactual = DB::table( 'venta' )
                                   ->join( 'detalleventa', 'venta.idventa', '=', 'detalleventa.idventa' )
                                   ->join( 'inventario', 'detalleventa.idproducto', '=', 'inventario.codigo' )
                                   ->join('producto', 'detalleventa.idproducto', '=', 'producto.codigo' )
                                   ->join( 'marca', 'producto.idmarca', '=', 'marca.idmarca' )
                                   ->join( 'presentacion', 'producto.idpresentacion', '=', 'presentacion.idpresentacion' )
                                   ->join( 'proveedor', 'producto.idproveedor', '=', 'proveedor.idproveedor' )
                                   ->join( 'categoria', 'producto.idcategoria', '=', 'categoria.idcategoria' )
                                        ->select( 'venta.idventa', 'venta.fecha', 'venta.total', 'venta.descuento', 'venta.efectivo',
                                                  'detalleventa.idproducto as codigoproducto', 'detalleventa.cantidad as cantidadventa',
                                                  'inventario.cantidad as cantidadinventario', 'inventario.preciocom as preciocomprainventario',
                                                  'inventario.precioven as precioventainventario', 'inventario.gananciasxpz as gananciasxpzinventario',
                                                  'inventario.gananciastot as gananciastotalesinventario',
                                                  'producto.descripcion as descripcionproducto', 'producto.preciocosto as preciocostoproducto',
                                                  'producto.precioventa as precioventaproducto',
                                                  'marca.descripcion as descripcionmarca',
                                                  'presentacion.medida as medidapresentacion', 
                                                  DB::raw( "CONCAT( proveedor.nombre,' ', proveedor.apellidopat,' ', proveedor.apellidomat ) AS nombreproveedor" ),
                                                  'categoria.nombre as nombrecategoria' )
                                        ->where( 'venta.fecha', '=', Carbon::today() )
                                        ->where( 'venta.idempleado', '=', $idempleado )
                                        ->where( 'venta.idcaja', '=', $idcaja )
                                        ->orderBy('venta.idventa', 'ASC')
                                        ->get();
          */
          

          
          $consultasventasactual = DB::table( 'venta' )
                                   ->join( 'detalleventa', 'venta.idventa', '=', 'detalleventa.idventa' )
                                   ->join( 'movimientoinventario', 'detalleventa.idmovimientoinventario', 'movimientoinventario.idmovimientoinventario')
                                   ->join( 'inventario', 'detalleventa.idproducto', '=', 'inventario.codigo' )
                                   ->join('producto', 'detalleventa.idproducto', '=', 'producto.codigo' )
                                        ->select( 'venta.idventa',
                                                  'detalleventa.idproducto as codigoproducto', 'detalleventa.cantidad as cantidadventa',
                                                  'producto.descripcion as descripcionproducto', 'movimientoinventario.PrecioCostoAct  as preciocostoproducto',
                                                  'movimientoinventario.PrecioVentaAct as precioventaproducto' )
                                        ->where( 'venta.fecha', '=', Carbon::today() )
                                        ->where( 'venta.idempleado', '=', $idempleado )
                                        ->where( 'venta.idcaja', '=', $idcaja )
                                        ->where( 'venta.estado', '=', 0 )
                                        ->orderBy('venta.idventa', 'ASC')
                                        ->get();

          $consultatotal = DB::table( 'venta' )
                              ->select( DB::raw( "SUM( venta.total ) as totalventas" ) )
                              ->where( 'venta.fecha', '=', Carbon::today() )
                              ->where( 'venta.idempleado', '=', $idempleado )
                              ->where( 'venta.idcaja', '=', $idcaja )
                              ->where( 'venta.estado', '=', 0 )
                              ->get();
                              
          $consultatotaldescuento = DB::table( 'venta' )
                              ->select( DB::raw( "SUM( venta.descuento ) as totaldescuento" ) )
                              ->where( 'venta.fecha', '=', Carbon::today() )
                              ->where( 'venta.idempleado', '=', $idempleado )
                              ->where( 'venta.idcaja', '=', $idcaja )
                              ->where( 'venta.estado', '=', 0 )
                              ->get();
          
          $consultatotalproductosvendidos = DB::table( 'venta' )
                                        ->join( 'detalleventa', 'venta.idventa', '=', 'detalleventa.idventa' )
                                        ->select( DB::raw( "SUM( detalleventa.cantidad ) as cantidadtotalproductosvendidos" ) )
                                        ->where( 'venta.fecha', '=', Carbon::today() )
                                        ->where( 'venta.idempleado', '=', $idempleado )
                                        ->where( 'venta.idcaja', '=', $idcaja )
                                        ->where( 'venta.estado', '=', 0 )
                                        ->get();
                    
          $consultatotalventasefectivo = DB::table( 'venta' )
                                        ->select( DB::raw( "SUM( venta.total ) as consultatotalventasefectivo" ) )
                                        ->where( 'venta.fecha', '=', Carbon::today() )
                                        ->where( 'venta.idtipopago', '=', 1 )
                                        ->where( 'venta.idempleado', '=', $idempleado )
                                        ->where( 'venta.idcaja', '=', $idcaja )
                                        ->where( 'venta.estado', '=', 0 )
                                        ->get();
                    
          $consultatotalventascreditocompleto = DB::table( 'venta' )
                                        ->select( DB::raw( "SUM( venta.total ) as totalventascreditocompleto" ) )
                                        ->where( 'venta.fecha', '=', Carbon::today() )
                                        ->where( 'venta.idtipopago', '=', 2 )
                                        ->where( 'venta.idempleado', '=', $idempleado )
                                        ->where( 'venta.idcaja', '=', $idcaja )
                                        ->where( 'venta.estado', '=', 0 )
                                        ->get();
                    
          $consultatotalventascreditopartes = DB::table( 'venta' )
                                        ->select( DB::raw( "SUM( venta.efectivo ) as totalpagadoefectivo" ),  DB::raw( "SUM( venta.total ) as total" ) )
                                        ->where( 'venta.fecha', '=', Carbon::today() )
                                        ->where( 'venta.idtipopago', '=', 3 )
                                        ->where( 'venta.idempleado', '=', $idempleado )
                                        ->where( 'venta.idcaja', '=', $idcaja )
                                        ->where( 'venta.estado', '=', 0 )
                                        ->get();
                    
          $consultatotalventas = DB::table( 'venta' )
                                        ->select( DB::raw( "count( venta.idventa ) as consultatotalventas" ) )
                                        ->where( 'venta.fecha', '=', Carbon::today() )
                                        ->where( 'venta.idempleado', '=', $idempleado )
                                        ->where( 'venta.idcaja', '=', $idcaja )
                                        ->where( 'venta.estado', '=', 0 )
                                        ->get();
               
          $consultaabonoscreditos = DB::table('abonoscredito')
                                             ->select( DB::raw( "SUM(Cantidad) as totalabonado" ) )
                                             ->where('abonoscredito.fecha', '=', Carbon::today()  )
                                             ->where('abonoscredito.idempleado', '=', $idempleado )
                                             ->where('abonoscredito.idcaja', '=', $idcaja )
                                             ->where('abonoscredito.estado', '=', 0 )
                                             ->get();


          return view('vistasventas/cortescaja/cortecajadiaventas')->with( 'consultasventasactual', $consultasventasactual )
                                                  ->with( 'consultatotal', $consultatotal )
                                                  ->with( 'consultatotaldescuento', $consultatotaldescuento )
                                                  ->with( 'consultatotalventasefectivo', $consultatotalventasefectivo )
                                                  ->with( 'consultatotalproductosvendidos', $consultatotalproductosvendidos )
                                                  ->with( 'consultatotalventascreditocompleto', $consultatotalventascreditocompleto )
                                                  ->with( 'consultatotalventascreditopartes', $consultatotalventascreditopartes )
                                                  ->with( 'consultatotalventas', $consultatotalventas )
                                                  ->with( 'fechaactual', $fechaactual )
                                                  ->with( 'consultasaldoinicial', $consultasaldoinicial )
                                                  ->with( 'informacionempleado', $informacionempleado )
                                                  ->with( 'idcaja', $idcaja )
                                                  ->with( 'consultaabonoscreditos', $consultaabonoscreditos );

     }

     //FUNCION EXPORTAR PDF CORTEDIA VENTAS
     public function exportarcortecajadiaventas() {

          $fechaactual = Carbon::now()->format('d-m-Y');

          $idempleado = Auth::User()->idEmpleado;

          $consultaidcaja = DB::table('caja')->select('idcaja as idcaja')
                                              ->where('idempleado', $idempleado )
                                              ->get();
  
          $idcaja = $consultaidcaja[0]->{'idcaja'};

          $informacionempleado = DB::table('empleado')->select('nombre as nombre', 'apellidopat as apellidopat', 'apellidomat as apellidomat' )
                                                       ->where('idempleado', $idempleado )
                                                       ->get();

          $consultasaldoinicial = DB::table( 'empleadocaja' )
                                             ->select('saldoinicial as saldoinicial')
                                             ->where('idempleado', '=', $idempleado )
                                             ->where('idcaja', '=', $idcaja )
                                             ->where('fecha', '=', Carbon::today() )
                                             ->orderBy('idempleadocaja', 'DESC')
                                             ->get();

          /*
          $consultasventasactual = DB::table( 'venta' )
                                   ->join( 'detalleventa', 'venta.idventa', '=', 'detalleventa.idventa' )
                                   ->join( 'inventario', 'detalleventa.idproducto', '=', 'inventario.codigo' )
                                   ->join('producto', 'detalleventa.idproducto', '=', 'producto.codigo' )
                                   ->join( 'marca', 'producto.idmarca', '=', 'marca.idmarca' )
                                   ->join( 'presentacion', 'producto.idpresentacion', '=', 'presentacion.idpresentacion' )
                                   ->join( 'proveedor', 'producto.idproveedor', '=', 'proveedor.idproveedor' )
                                   ->join( 'categoria', 'producto.idcategoria', '=', 'categoria.idcategoria' )
                                        ->select( 'venta.idventa', 'venta.fecha', 'venta.total', 'venta.descuento', 'venta.efectivo',
                                                  'detalleventa.idproducto as codigoproducto', 'detalleventa.cantidad as cantidadventa',
                                                  'inventario.cantidad as cantidadinventario', 'inventario.preciocom as preciocomprainventario',
                                                  'inventario.precioven as precioventainventario', 'inventario.gananciasxpz as gananciasxpzinventario',
                                                  'inventario.gananciastot as gananciastotalesinventario',
                                                  'producto.descripcion as descripcionproducto', 'producto.preciocosto as preciocostoproducto',
                                                  'producto.precioventa as precioventaproducto',
                                                  'marca.descripcion as descripcionmarca',
                                                  'presentacion.medida as medidapresentacion', 
                                                  DB::raw( "CONCAT( proveedor.nombre,' ', proveedor.apellidopat,' ', proveedor.apellidomat ) AS nombreproveedor" ),
                                                  'categoria.nombre as nombrecategoria' )
                                        ->where( 'venta.fecha', '=', Carbon::today() )
                                        ->where( 'venta.idempleado', '=', $idempleado )
                                        ->where( 'venta.idcaja', '=', $idcaja )
                                        ->orderBy('venta.idventa', 'ASC')
                                        ->get();
          */
          

          
          $consultasventasactual = DB::table( 'venta' )
                                   ->join( 'detalleventa', 'venta.idventa', '=', 'detalleventa.idventa' )
                                   ->join( 'movimientoinventario', 'detalleventa.idmovimientoinventario', 'movimientoinventario.idmovimientoinventario')
                                   ->join( 'inventario', 'detalleventa.idproducto', '=', 'inventario.codigo' )
                                   ->join('producto', 'detalleventa.idproducto', '=', 'producto.codigo' )
                                        ->select( 'venta.idventa',
                                                  'detalleventa.idproducto as codigoproducto', 'detalleventa.cantidad as cantidadventa',
                                                  'producto.descripcion as descripcionproducto', 'movimientoinventario.PrecioCostoAct  as preciocostoproducto',
                                                  'movimientoinventario.PrecioVentaAct as precioventaproducto' )
                                        ->where( 'venta.fecha', '=', Carbon::today() )
                                        ->where( 'venta.idempleado', '=', $idempleado )
                                        ->where( 'venta.idcaja', '=', $idcaja )
                                        ->where( 'venta.estado', '=', 0 )
                                        ->orderBy('venta.idventa', 'ASC')
                                        ->get();

          $consultatotal = DB::table( 'venta' )
                              ->select( DB::raw( "SUM( venta.total ) as totalventas" ) )
                              ->where( 'venta.fecha', '=', Carbon::today() )
                              ->where( 'venta.idempleado', '=', $idempleado )
                              ->where( 'venta.idcaja', '=', $idcaja )
                              ->where( 'venta.estado', '=', 0 )
                              ->get();
                              
          $consultatotaldescuento = DB::table( 'venta' )
                              ->select( DB::raw( "SUM( venta.descuento ) as totaldescuento" ) )
                              ->where( 'venta.fecha', '=', Carbon::today() )
                              ->where( 'venta.idempleado', '=', $idempleado )
                              ->where( 'venta.idcaja', '=', $idcaja )
                              ->where( 'venta.estado', '=', 0 )
                              ->get();
          
          $consultatotalproductosvendidos = DB::table( 'venta' )
                                        ->join( 'detalleventa', 'venta.idventa', '=', 'detalleventa.idventa' )
                                        ->select( DB::raw( "SUM( detalleventa.cantidad ) as cantidadtotalproductosvendidos" ) )
                                        ->where( 'venta.fecha', '=', Carbon::today() )
                                        ->where( 'venta.idempleado', '=', $idempleado )
                                        ->where( 'venta.idcaja', '=', $idcaja )
                                        ->where( 'venta.estado', '=', 0 )
                                        ->get();
                    
          $consultatotalventasefectivo = DB::table( 'venta' )
                                        ->select( DB::raw( "SUM( venta.total ) as consultatotalventasefectivo" ) )
                                        ->where( 'venta.fecha', '=', Carbon::today() )
                                        ->where( 'venta.idtipopago', '=', 1 )
                                        ->where( 'venta.idempleado', '=', $idempleado )
                                        ->where( 'venta.idcaja', '=', $idcaja )
                                        ->where( 'venta.estado', '=', 0 )
                                        ->get();
                    
          $consultatotalventascreditocompleto = DB::table( 'venta' )
                                        ->select( DB::raw( "SUM( venta.total ) as totalventascreditocompleto" ) )
                                        ->where( 'venta.fecha', '=', Carbon::today() )
                                        ->where( 'venta.idtipopago', '=', 2 )
                                        ->where( 'venta.idempleado', '=', $idempleado )
                                        ->where( 'venta.idcaja', '=', $idcaja )
                                        ->where( 'venta.estado', '=', 0 )
                                        ->get();
                    
          $consultatotalventascreditopartes = DB::table( 'venta' )
                                        ->select( DB::raw( "SUM( venta.efectivo ) as totalpagadoefectivo" ),  DB::raw( "SUM( venta.total ) as total" ) )
                                        ->where( 'venta.fecha', '=', Carbon::today() )
                                        ->where( 'venta.idtipopago', '=', 3 )
                                        ->where( 'venta.idempleado', '=', $idempleado )
                                        ->where( 'venta.idcaja', '=', $idcaja )
                                        ->where( 'venta.estado', '=', 0 )
                                        ->get();
                    
          $consultatotalventas = DB::table( 'venta' )
                                        ->select( DB::raw( "count( venta.idventa ) as consultatotalventas" ) )
                                        ->where( 'venta.fecha', '=', Carbon::today() )
                                        ->where( 'venta.idempleado', '=', $idempleado )
                                        ->where( 'venta.idcaja', '=', $idcaja )
                                        ->where( 'venta.estado', '=', 0 )
                                        ->get();
               
          $consultaabonoscreditos = DB::table('abonoscredito')
                                             ->select( DB::raw( "SUM(Cantidad) as totalabonado" ) )
                                             ->where('abonoscredito.fecha', '=', Carbon::today()  )
                                             ->where('abonoscredito.idempleado', '=', $idempleado )
                                             ->where('abonoscredito.idcaja', '=', $idcaja )
                                             ->where('abonoscredito.estado', '=', 0 )
                                             ->get();

                                        
          $tienda = Tienda_Modelo::select('*')->take(1)->first();
                                        
          /*
          $pdf = \PDF::setOptions([
                    'images' => true,
                    'enable-javascript' => true,
                    'javascript-delay' => 5000,
                    'enable-smart-shrinking' => true,
                    'no-stop-slow-scripts' => true
               ])
               */

          $pdf = \PDF::loadView('vistasventas/cortescaja/exportarcortecajadiaventas', array(    'consultasventasactual' => $consultasventasactual, 
                                                                                          'consultatotal' => $consultatotal, 
                                                                                          'consultatotaldescuento' => $consultatotaldescuento,
                                                                                          'consultatotalproductosvendidos' => $consultatotalproductosvendidos, 
                                                                                          'consultatotalventasefectivo' => $consultatotalventasefectivo,
                                                                                          'consultatotalventascreditocompleto' => $consultatotalventascreditocompleto,
                                                                                          'consultatotalventascreditopartes' => $consultatotalventascreditopartes,
                                                                                          'consultatotalventas' => $consultatotalventas,
                                                                                          'fechaactual' => $fechaactual, 
                                                                                          'tienda' => $tienda, 
                                                                                          'consultasaldoinicial' => $consultasaldoinicial,
                                                                                          'idcaja' => $idcaja,
                                                                                          'informacionempleado' => $informacionempleado,
                                                                                          'consultaabonoscreditos' => $consultaabonoscreditos ) )
               ->setPaper('a4', 'landscape')->setWarnings(false);

          return $pdf->download( 'CorteCajaVentas '. $fechaactual . '.pdf');
          
     }


     //FUNCION VISTA CORTEDIA VENTAS
     public function cortediaventassesioncerrada() {

          $fechaactual = Carbon::now()->format('d-m-Y');

          if( !isset( Auth::User()->idEmpleado ) ) {
               return redirect('/'); 
          }
          
          $idempleado = Auth::User()->idEmpleado;
          $nivelusuario = Auth::user()->Nivel;

          $consultaidcaja = DB::table('caja')->select('idcaja as idcaja')
                                              ->where('idempleado', $idempleado )
                                              ->get();
  
          $idcaja = $consultaidcaja[0]->{'idcaja'};

          $informacionempleado = DB::table('empleado')->select('nombre as nombre', 'apellidopat as apellidopat', 'apellidomat as apellidomat' )
                                                       ->where('idempleado', $idempleado )
                                                       ->get();

          $auxfechaactual = Carbon::today();          
          $auxfechaactual->addDays(-1); 
          //$auxfechaactual = $auxfechaactual->format('m/d/Y');
          
          $consultacambiodia = DB::table('empleadocaja')
                              ->select( DB::raw("COUNT(*) as totalconsultacambiodia"), 'idempleadocaja as idempleadocaja' )          
                              ->where('idempleado', '=', $idempleado )
                              ->where('idcaja', '=', $idcaja )
                              ->where('fecha', '=', $auxfechaactual )
                              ->where('estado', '=', 0 )
                              ->groupBy('idempleadocaja' )
                              ->get();

          if( count( $consultacambiodia ) == 1 ) {
                                        
               if( $consultacambiodia[0]->{'totalconsultacambiodia'} == 1 ) {                         
               
                    $auxidempleadocaja = $consultacambiodia[0]->{'idempleadocaja'};
     
                    DB::table('empleadocaja')
                              ->where( 'empleadocaja.idempleadocaja', '=', $auxidempleadocaja )
                              ->update( array( 'fecha' => Carbon::today() ) );
          
               }                    

          }

          $consultasaldoinicial = DB::table( 'empleadocaja' )
                                             ->select('saldoinicial as saldoinicial')
                                             ->where('idempleado', '=', $idempleado )
                                             ->where('idcaja', '=', $idcaja )
                                             ->where('fecha', '=', Carbon::today() )
                                             ->orderBy('idempleadocaja', 'DESC')
                                             ->get();

          /*

          $consultasventasactual = DB::table( 'venta' )
                                   ->join( 'detalleventa', 'venta.idventa', '=', 'detalleventa.idventa' )
                                   ->join( 'inventario', 'detalleventa.idproducto', '=', 'inventario.codigo' )
                                   ->join('producto', 'detalleventa.idproducto', '=', 'producto.codigo' )
                                   ->join( 'marca', 'producto.idmarca', '=', 'marca.idmarca' )
                                   ->join( 'presentacion', 'producto.idpresentacion', '=', 'presentacion.idpresentacion' )
                                   ->join( 'proveedor', 'producto.idproveedor', '=', 'proveedor.idproveedor' )
                                   ->join( 'categoria', 'producto.idcategoria', '=', 'categoria.idcategoria' )
                                        ->select( 'venta.idventa', 'venta.fecha', 'venta.total', 'venta.descuento', 'venta.efectivo',
                                                  'detalleventa.idproducto as codigoproducto', 'detalleventa.cantidad as cantidadventa',
                                                  'inventario.cantidad as cantidadinventario', 'inventario.preciocom as preciocomprainventario',
                                                  'inventario.precioven as precioventainventario', 'inventario.gananciasxpz as gananciasxpzinventario',
                                                  'inventario.gananciastot as gananciastotalesinventario',
                                                  'producto.descripcion as descripcionproducto', 'producto.preciocosto as preciocostoproducto',
                                                  'producto.precioventa as precioventaproducto',
                                                  'marca.descripcion as descripcionmarca',
                                                  'presentacion.medida as medidapresentacion', 
                                                  DB::raw( "CONCAT( proveedor.nombre,' ', proveedor.apellidopat,' ', proveedor.apellidomat ) AS nombreproveedor" ),
                                                  'categoria.nombre as nombrecategoria' )
                                        ->where( 'venta.fecha', '=', Carbon::today() )
                                        ->where( 'venta.idempleado', '=', $idempleado )
                                        ->where( 'venta.idcaja', '=', $idcaja )
                                        ->orderBy('venta.idventa', 'ASC')
                                        ->get();
          */
          

          
          $consultasventasactual = DB::table( 'venta' )
                                   ->join( 'detalleventa', 'venta.idventa', '=', 'detalleventa.idventa' )
                                   ->join( 'movimientoinventario', 'detalleventa.idmovimientoinventario', 'movimientoinventario.idmovimientoinventario')
                                   ->join( 'inventario', 'detalleventa.idproducto', '=', 'inventario.codigo' )
                                   ->join('producto', 'detalleventa.idproducto', '=', 'producto.codigo' )
                                        ->select( 'venta.idventa',
                                                  'detalleventa.idproducto as codigoproducto', 'detalleventa.cantidad as cantidadventa',
                                                  'producto.descripcion as descripcionproducto', 'movimientoinventario.PrecioCostoAct  as preciocostoproducto',
                                                  'movimientoinventario.PrecioVentaAct as precioventaproducto' )
                                        ->where( 'venta.fecha', '=', Carbon::today() )
                                        ->where( 'venta.idempleado', '=', $idempleado )
                                        ->where( 'venta.idcaja', '=', $idcaja )
                                        ->where( 'venta.estado', '=', 0 )
                                        ->orderBy('venta.idventa', 'ASC')
                                        ->get();

          $consultatotal = DB::table( 'venta' )
                              ->select( DB::raw( "SUM( venta.total ) as totalventas" ) )
                              ->where( 'venta.fecha', '=', Carbon::today() )
                              ->where( 'venta.idempleado', '=', $idempleado )
                              ->where( 'venta.idcaja', '=', $idcaja )
                              ->where( 'venta.estado', '=', 0 )
                              ->get();
                              
          $consultatotaldescuento = DB::table( 'venta' )
                              ->select( DB::raw( "SUM( venta.descuento ) as totaldescuento" ) )
                              ->where( 'venta.fecha', '=', Carbon::today() )
                              ->where( 'venta.idempleado', '=', $idempleado )
                              ->where( 'venta.idcaja', '=', $idcaja )
                              ->where( 'venta.estado', '=', 0 )
                              ->get();
          
          $consultatotalproductosvendidos = DB::table( 'venta' )
                                        ->join( 'detalleventa', 'venta.idventa', '=', 'detalleventa.idventa' )
                                        ->select( DB::raw( "SUM( detalleventa.cantidad ) as cantidadtotalproductosvendidos" ) )
                                        ->where( 'venta.fecha', '=', Carbon::today() )
                                        ->where( 'venta.idempleado', '=', $idempleado )
                                        ->where( 'venta.idcaja', '=', $idcaja )
                                        ->where( 'venta.estado', '=', 0 )
                                        ->get();
                    
          $consultatotalventasefectivo = DB::table( 'venta' )
                                        ->select( DB::raw( "SUM( venta.total ) as consultatotalventasefectivo" ) )
                                        ->where( 'venta.fecha', '=', Carbon::today() )
                                        ->where( 'venta.idtipopago', '=', 1 )
                                        ->where( 'venta.idempleado', '=', $idempleado )
                                        ->where( 'venta.idcaja', '=', $idcaja )
                                        ->where( 'venta.estado', '=', 0 )
                                        ->get();
                    
          $consultatotalventascreditocompleto = DB::table( 'venta' )
                                        ->select( DB::raw( "SUM( venta.total ) as totalventascreditocompleto" ) )
                                        ->where( 'venta.fecha', '=', Carbon::today() )
                                        ->where( 'venta.idtipopago', '=', 2 )
                                        ->where( 'venta.idempleado', '=', $idempleado )
                                        ->where( 'venta.idcaja', '=', $idcaja )
                                        ->where( 'venta.estado', '=', 0 )
                                        ->get();
                    
          $consultatotalventascreditopartes = DB::table( 'venta' )
                                        ->select( DB::raw( "SUM( venta.efectivo ) as totalpagadoefectivo" ),  DB::raw( "SUM( venta.total ) as total" ) )
                                        ->where( 'venta.fecha', '=', Carbon::today() )
                                        ->where( 'venta.idtipopago', '=', 3 )
                                        ->where( 'venta.idempleado', '=', $idempleado )
                                        ->where( 'venta.idcaja', '=', $idcaja )
                                        ->where( 'venta.estado', '=', 0 )
                                        ->get();
                    
          $consultatotalventas = DB::table( 'venta' )
                                        ->select( DB::raw( "count( venta.idventa ) as consultatotalventas" ) )
                                        ->where( 'venta.fecha', '=', Carbon::today() )
                                        ->where( 'venta.idempleado', '=', $idempleado )
                                        ->where( 'venta.idcaja', '=', $idcaja )
                                        ->where( 'venta.estado', '=', 0 )
                                        ->get();
               
          $consultaabonoscreditos = DB::table('abonoscredito')
                                             ->select( DB::raw( "SUM(Cantidad) as totalabonado" ) )
                                             ->where('abonoscredito.fecha', '=', Carbon::today()  )
                                             ->where('abonoscredito.idempleado', '=', $idempleado )
                                             ->where('abonoscredito.idcaja', '=', $idcaja )
                                             ->where('abonoscredito.estado', '=', 0 )
                                             ->get();

          $importetotalpagadoefectivo = bcadd( ( $consultatotalventasefectivo[0]->consultatotalventasefectivo ) + ( $consultatotalventascreditopartes[0]->totalpagadoefectivo ) , '0', 2 );
          $saldoactualcaja = bcadd( ( $consultasaldoinicial[0]->{'saldoinicial'} ) + ( $importetotalpagadoefectivo ) , '0', 2 );
                                        
          $tienda = Tienda_Modelo::select('*')->take(1)->first();
          
          $usuariowindows =  get_current_user();
          $nombreempleado = $informacionempleado[0]->{'nombre'};

          /*
          $pdf = \PDF::setOptions([
                    'images' => true,
                    'enable-javascript' => true,
                    'javascript-delay' => 5000,
                    'enable-smart-shrinking' => true,
                    'no-stop-slow-scripts' => true
               ])
          */

          $pdf = \PDF::loadView('vistasventas/cortescaja/exportarcortecajadiaventas', array(    'consultasventasactual' => $consultasventasactual, 
                                                                                          'consultatotal' => $consultatotal, 
                                                                                          'consultatotaldescuento' => $consultatotaldescuento,
                                                                                          'consultatotalproductosvendidos' => $consultatotalproductosvendidos, 
                                                                                          'consultatotalventasefectivo' => $consultatotalventasefectivo,
                                                                                          'consultatotalventascreditocompleto' => $consultatotalventascreditocompleto,
                                                                                          'consultatotalventascreditopartes' => $consultatotalventascreditopartes,
                                                                                          'consultatotalventas' => $consultatotalventas,
                                                                                          'fechaactual' => $fechaactual, 
                                                                                          'tienda' => $tienda, 
                                                                                          'consultasaldoinicial' => $consultasaldoinicial,
                                                                                          'idcaja' => $idcaja,
                                                                                          'informacionempleado' => $informacionempleado,
                                                                                          'consultaabonoscreditos' => $consultaabonoscreditos ) )
               ->setPaper('a4', 'landscape')->setWarnings(false);
          
          $pdf->save("C:/Users/" . $usuariowindows ."/Downloads/CorteCajaVentas ". $nombreempleado ." ". $fechaactual .".pdf");
          
          DB::table('venta')
                    ->where( 'venta.fecha', '=', Carbon::today() )
                    ->where( 'venta.idempleado', '=', $idempleado )
                    ->where( 'venta.idcaja', '=', $idcaja )
                    ->update( array( 'Estado' => 1 ) );
                    
          DB::table('pedido')
          ->where( 'pedido.fecha', '=', Carbon::today() )
          ->where( 'pedido.idempleado', '=', $idempleado )
          ->update( array( 'Estado' => 1 ) );
               
          DB::table('abonoscredito')
          ->where('abonoscredito.fecha', '=', Carbon::today()  )
          ->where('abonoscredito.idempleado', '=', $idempleado )
          ->where('abonoscredito.idcaja', '=', $idcaja )
          ->update( array( 'estado' => 1 ) );
               
          DB::table('empleadocaja')
          ->where('empleadocaja.fecha', '=', Carbon::today()  )
          ->where('empleadocaja.idempleado', '=', $idempleado )
          ->where('empleadocaja.idcaja', '=', $idcaja )
          ->where('empleadocaja.estado', '=', 0 )
          ->update( array( 'estado' => 1, 'saldofinal' => $saldoactualcaja ) );
          
 
          Auth::logout(); 
          Session::flush(); 

          return view('vistasventas/cortescaja/cortediaventassesioncerrada')->with( 'consultasventasactual', $consultasventasactual )
                                                  ->with( 'consultatotal', $consultatotal )
                                                  ->with( 'consultatotaldescuento', $consultatotaldescuento )
                                                  ->with( 'consultatotalventasefectivo', $consultatotalventasefectivo )
                                                  ->with( 'consultatotalproductosvendidos', $consultatotalproductosvendidos )
                                                  ->with( 'consultatotalventascreditocompleto', $consultatotalventascreditocompleto )
                                                  ->with( 'consultatotalventascreditopartes', $consultatotalventascreditopartes )
                                                  ->with( 'consultatotalventas', $consultatotalventas )
                                                  ->with( 'fechaactual', $fechaactual )
                                                  ->with( 'consultasaldoinicial', $consultasaldoinicial )
                                                  ->with( 'idempleado', $idempleado )
                                                  ->with( 'nivelusuario', $nivelusuario )
                                                  ->with( 'idcaja', $idcaja )
                                                  ->with( 'informacionempleado', $informacionempleado )
                                                  ->with( 'consultaabonoscreditos', $consultaabonoscreditos );

     }//FUNCION EXPORTAR PDF CORTEDIA VENTAS
     
     public function exportarcortecajadiaventascerrada( $idempleado ) {          

          $fechaactual = Carbon::now()->format('d-m-Y');

          $consultaidcaja = DB::table('caja')->select('idcaja as idcaja')
                                              ->where('idempleado', $idempleado )
                                              ->get();
  
          $idcaja = $consultaidcaja[0]->{'idcaja'};

          $informacionempleado = DB::table('empleado')->select('nombre as nombre', 'apellidopat as apellidopat', 'apellidomat as apellidomat' )
                                                       ->where('idempleado', $idempleado )
                                                       ->get();

          $consultasaldoinicial = DB::table( 'empleadocaja' )
                                             ->select('saldoinicial as saldoinicial')
                                             ->where('idempleado', '=', $idempleado )
                                             ->where('idcaja', '=', $idcaja )
                                             ->where('fecha', '=', Carbon::today() )
                                             ->orderBy('idempleadocaja', 'DESC')
                                             ->get();

          
          /*
          $consultasventasactual = DB::table( 'venta' )
                                   ->join( 'detalleventa', 'venta.idventa', '=', 'detalleventa.idventa' )
                                   ->join( 'inventario', 'detalleventa.idproducto', '=', 'inventario.codigo' )
                                   ->join('producto', 'detalleventa.idproducto', '=', 'producto.codigo' )
                                   ->join( 'marca', 'producto.idmarca', '=', 'marca.idmarca' )
                                   ->join( 'presentacion', 'producto.idpresentacion', '=', 'presentacion.idpresentacion' )
                                   ->join( 'proveedor', 'producto.idproveedor', '=', 'proveedor.idproveedor' )
                                   ->join( 'categoria', 'producto.idcategoria', '=', 'categoria.idcategoria' )
                                        ->select( 'venta.idventa', 'venta.fecha', 'venta.total', 'venta.descuento', 'venta.efectivo',
                                                  'detalleventa.idproducto as codigoproducto', 'detalleventa.cantidad as cantidadventa',
                                                  'inventario.cantidad as cantidadinventario', 'inventario.preciocom as preciocomprainventario',
                                                  'inventario.precioven as precioventainventario', 'inventario.gananciasxpz as gananciasxpzinventario',
                                                  'inventario.gananciastot as gananciastotalesinventario',
                                                  'producto.descripcion as descripcionproducto', 'producto.preciocosto as preciocostoproducto',
                                                  'producto.precioventa as precioventaproducto',
                                                  'marca.descripcion as descripcionmarca',
                                                  'presentacion.medida as medidapresentacion', 
                                                  DB::raw( "CONCAT( proveedor.nombre,' ', proveedor.apellidopat,' ', proveedor.apellidomat ) AS nombreproveedor" ),
                                                  'categoria.nombre as nombrecategoria' )
                                        ->where( 'venta.fecha', '=', Carbon::today() )
                                        ->where( 'venta.idempleado', '=', $idempleado )
                                        ->where( 'venta.idcaja', '=', $idcaja )
                                        ->orderBy('venta.idventa', 'ASC')
                                        ->get();
          */
          
                    
          $consultasventasactual = DB::table( 'venta' )
                                   ->join( 'detalleventa', 'venta.idventa', '=', 'detalleventa.idventa' )
                                   ->join( 'movimientoinventario', 'detalleventa.idmovimientoinventario', 'movimientoinventario.idmovimientoinventario')
                                   ->join( 'inventario', 'detalleventa.idproducto', '=', 'inventario.codigo' )
                                   ->join('producto', 'detalleventa.idproducto', '=', 'producto.codigo' )
                                        ->select( 'venta.idventa',
                                                  'detalleventa.idproducto as codigoproducto', 'detalleventa.cantidad as cantidadventa',
                                                  'producto.descripcion as descripcionproducto', 'movimientoinventario.PrecioCostoAct  as preciocostoproducto',
                                                  'movimientoinventario.PrecioVentaAct as precioventaproducto' )
                                        ->where( 'venta.fecha', '=', Carbon::today() )
                                        ->where( 'venta.idempleado', '=', $idempleado )
                                        ->where( 'venta.idcaja', '=', $idcaja )
                                        ->where( 'venta.estado', '=', 0 )
                                        ->orderBy('venta.idventa', 'ASC')
                                        ->get();

          $consultatotal = DB::table( 'venta' )
                              ->select( DB::raw( "SUM( venta.total ) as totalventas" ) )
                              ->where( 'venta.fecha', '=', Carbon::today() )
                              ->where( 'venta.idempleado', '=', $idempleado )
                              ->where( 'venta.idcaja', '=', $idcaja )
                              ->where( 'venta.estado', '=', 0 )
                              ->get();
                              
          $consultatotaldescuento = DB::table( 'venta' )
                              ->select( DB::raw( "SUM( venta.descuento ) as totaldescuento" ) )
                              ->where( 'venta.fecha', '=', Carbon::today() )
                              ->where( 'venta.idempleado', '=', $idempleado )
                              ->where( 'venta.idcaja', '=', $idcaja )
                              ->where( 'venta.estado', '=', 0 )
                              ->get();
          
          $consultatotalproductosvendidos = DB::table( 'venta' )
                                        ->join( 'detalleventa', 'venta.idventa', '=', 'detalleventa.idventa' )
                                        ->select( DB::raw( "SUM( detalleventa.cantidad ) as cantidadtotalproductosvendidos" ) )
                                        ->where( 'venta.fecha', '=', Carbon::today() )
                                        ->where( 'venta.idempleado', '=', $idempleado )
                                        ->where( 'venta.idcaja', '=', $idcaja )
                                        ->where( 'venta.estado', '=', 0 )
                                        ->get();
                    
          $consultatotalventasefectivo = DB::table( 'venta' )
                                        ->select( DB::raw( "SUM( venta.total ) as consultatotalventasefectivo" ) )
                                        ->where( 'venta.fecha', '=', Carbon::today() )
                                        ->where( 'venta.idtipopago', '=', 1 )
                                        ->where( 'venta.idempleado', '=', $idempleado )
                                        ->where( 'venta.idcaja', '=', $idcaja )
                                        ->where( 'venta.estado', '=', 0 )
                                        ->get();
                    
          $consultatotalventascreditocompleto = DB::table( 'venta' )
                                        ->select( DB::raw( "SUM( venta.total ) as totalventascreditocompleto" ) )
                                        ->where( 'venta.fecha', '=', Carbon::today() )
                                        ->where( 'venta.idtipopago', '=', 2 )
                                        ->where( 'venta.idempleado', '=', $idempleado )
                                        ->where( 'venta.idcaja', '=', $idcaja )
                                        ->where( 'venta.estado', '=', 0 )
                                        ->get();
                    
          $consultatotalventascreditopartes = DB::table( 'venta' )
                                        ->select( DB::raw( "SUM( venta.efectivo ) as totalpagadoefectivo" ),  DB::raw( "SUM( venta.total ) as total" ) )
                                        ->where( 'venta.fecha', '=', Carbon::today() )
                                        ->where( 'venta.idtipopago', '=', 3 )
                                        ->where( 'venta.idempleado', '=', $idempleado )
                                        ->where( 'venta.idcaja', '=', $idcaja )
                                        ->where( 'venta.estado', '=', 0 )
                                        ->get();
                    
          $consultatotalventas = DB::table( 'venta' )
                                        ->select( DB::raw( "count( venta.idventa ) as consultatotalventas" ) )
                                        ->where( 'venta.fecha', '=', Carbon::today() )
                                        ->where( 'venta.idempleado', '=', $idempleado )
                                        ->where( 'venta.idcaja', '=', $idcaja )
                                        ->where( 'venta.estado', '=', 0 )
                                        ->get();

                                        
          $tienda = Tienda_Modelo::select('*')->take(1)->first();
                                        
          
          $pdf = \PDF::setOptions([
                    'images' => true,
                    'enable-javascript' => true,
                    'javascript-delay' => 5000,
                    'enable-smart-shrinking' => true,
                    'no-stop-slow-scripts' => true
               ])
               ->loadView('vistasventas/cortescaja/exportarcortecajadiaventas', array(    'consultasventasactual' => $consultasventasactual, 
                                                                                          'consultatotal' => $consultatotal, 
                                                                                          'consultatotaldescuento' => $consultatotaldescuento,
                                                                                          'consultatotalproductosvendidos' => $consultatotalproductosvendidos, 
                                                                                          'consultatotalventasefectivo' => $consultatotalventasefectivo,
                                                                                          'consultatotalventascreditocompleto' => $consultatotalventascreditocompleto,
                                                                                          'consultatotalventascreditopartes' => $consultatotalventascreditopartes,
                                                                                          'consultatotalventas' => $consultatotalventas,
                                                                                          'fechaactual' => $fechaactual, 
                                                                                          'tienda' => $tienda, 
                                                                                          'consultasaldoinicial' => $consultasaldoinicial,
                                                                                          'idcaja' => $idcaja,
                                                                                          'informacionempleado' => $informacionempleado ) )
               ->setPaper('a4', 'landscape')->setWarnings(false);

          return $pdf->download( 'CorteCajaVentas '. $fechaactual . '.pdf');
          
     }


     //FUNCION VISTA CORTEDIA COMPRAS
     public function cortediacompras() {

          $fechaactual = Carbon::now()->format('d-m-Y');

          $idempleado = Auth::User()->idEmpleado;

          $consultaidcaja = DB::table('caja')->select('idcaja as idcaja')
                                              ->where('idempleado', $idempleado )
                                              ->get();
  
          $idcaja = $consultaidcaja[0]->{'idcaja'};

          $informacionempleado = DB::table('empleado')->select('nombre as nombre', 'apellidopat as apellidopat', 'apellidomat as apellidomat' )
                                                       ->where('idempleado', $idempleado )
                                                       ->get();

          /*
          $consultascomprasactual = DB::table( 'pedido' )
                                   ->join( 'detallepedido', 'pedido.idpedido', '=', 'detallepedido.idpedido' )
                                   ->join( 'inventario', 'detallepedido.codigo', '=', 'inventario.codigo' )
                                   ->join( 'producto', 'detallepedido.codigo', '=', 'producto.codigo' )
                                   ->join( 'marca', 'producto.idmarca', '=', 'marca.idmarca' )
                                   ->join( 'presentacion', 'producto.idpresentacion', '=', 'presentacion.idpresentacion' )
                                   ->join( 'proveedor', 'producto.idproveedor', '=', 'proveedor.idproveedor' )
                                   ->join( 'categoria', 'producto.idcategoria', '=', 'categoria.idcategoria' )
                                        ->select( 'pedido.idpedido', 'pedido.fecha', 'pedido.total',
                                                  'detallepedido.codigo as codigoproducto', 'detallepedido.cantidad as cantidadpedido',
                                                  'inventario.cantidad as cantidadinventario', 'inventario.preciocom as preciocomprainventario',
                                                  'inventario.precioven as precioventainventario', 'inventario.gananciasxpz as gananciasxpzinventario',
                                                  'inventario.gananciastot as gananciastotalesinventario',
                                                  'producto.descripcion as descripcionproducto', 'producto.preciocosto as preciocostoproducto',
                                                  'producto.precioventa as precioventaproducto',
                                                  'marca.descripcion as descripcionmarca',
                                                  'presentacion.medida as medidapresentacion', 
                                                  DB::raw( "CONCAT( proveedor.nombre,' ', proveedor.apellidopat,' ', proveedor.apellidomat ) AS nombreproveedor" ),
                                                  'categoria.nombre as nombrecategoria' )
                                        ->where( 'pedido.fecha', '=', Carbon::today() )
                                        ->where( 'pedido.idempleado', '=', $idempleado )
                                        ->get();
          */
          
          $consultascomprasactual = DB::table( 'pedido' )
                                   ->join( 'detallepedido', 'pedido.idpedido', '=', 'detallepedido.idpedido' )
                                   ->join( 'movimientoinventario', 'detallepedido.idmovimientoinventario', 'movimientoinventario.idmovimientoinventario')
                                   ->join( 'inventario', 'detallepedido.codigo', '=', 'inventario.codigo' )
                                   ->join('producto', 'detallepedido.codigo', '=', 'producto.codigo' )
                                        ->select( 'pedido.idpedido',
                                                  'detallepedido.codigo as codigoproducto', 'detallepedido.cantidad as cantidadpedido',
                                                  'producto.descripcion as descripcionproducto', 'movimientoinventario.PrecioCostoAct  as preciocostoproducto',
                                                  'movimientoinventario.PrecioVentaAct as precioventaproducto' )
                                        ->where( 'pedido.fecha', '=', Carbon::today() )
                                        ->where( 'pedido.idempleado', '=', $idempleado )
                                        ->where( 'pedido.estado', '=', 0 )
                                        ->orderBy('pedido.idpedido', 'ASC')
                                        ->get();

          $consultatotal = DB::table( 'pedido' )
                              ->select( DB::raw( "SUM( pedido.total ) as totalcompras" ) )
                              ->where( 'pedido.fecha', '=', Carbon::today() )
                              ->where( 'pedido.idempleado', '=', $idempleado )
                              ->where( 'pedido.estado', '=', 0 )
                              ->get();
          
          $cantidadtotalproductoscomprados = DB::table( 'pedido' )
                                        ->join( 'detallepedido', 'pedido.idpedido', '=', 'detallepedido.idpedido' )
                                        ->select( DB::raw( "SUM( detallepedido.cantidad ) as cantidadtotalproductoscomprados" ) )
                                        ->where( 'pedido.fecha', '=', Carbon::today() )
                                        ->where( 'pedido.idempleado', '=', $idempleado )
                                        ->where( 'pedido.estado', '=', 0 )
                                        ->get();


          return view('vistasventas/cortescaja/cortecajadiacompras')->with( 'consultascomprasactual', $consultascomprasactual )
                                                  ->with( 'consultatotal', $consultatotal )
                                                  ->with( 'cantidadtotalproductoscomprados', $cantidadtotalproductoscomprados )
                                                  ->with( 'idcaja', $idcaja )
                                                  ->with( 'informacionempleado', $informacionempleado )
                                                  ->with( 'fechaactual', $fechaactual );

     }

     //FUNCION EXPORTAR PDF CORTEDIA COMPRAS
     public function exportarcortecajadiacompras() {
          

          $fechaactual = Carbon::now()->format('d-m-Y');

          $idempleado = Auth::User()->idEmpleado;

          $consultaidcaja = DB::table('caja')->select('idcaja as idcaja')
                                              ->where('idempleado', $idempleado )
                                              ->get();
  
          $idcaja = $consultaidcaja[0]->{'idcaja'};

          $informacionempleado = DB::table('empleado')->select('nombre as nombre', 'apellidopat as apellidopat', 'apellidomat as apellidomat' )
                                                       ->where('idempleado', $idempleado )
                                                       ->get();

          /*
          $consultascomprasactual = DB::table( 'pedido' )
                                   ->join( 'detallepedido', 'pedido.idpedido', '=', 'detallepedido.idpedido' )
                                   ->join( 'inventario', 'detallepedido.codigo', '=', 'inventario.codigo' )
                                   ->join( 'producto', 'detallepedido.codigo', '=', 'producto.codigo' )
                                   ->join( 'marca', 'producto.idmarca', '=', 'marca.idmarca' )
                                   ->join( 'presentacion', 'producto.idpresentacion', '=', 'presentacion.idpresentacion' )
                                   ->join( 'proveedor', 'producto.idproveedor', '=', 'proveedor.idproveedor' )
                                   ->join( 'categoria', 'producto.idcategoria', '=', 'categoria.idcategoria' )
                                        ->select( 'pedido.idpedido', 'pedido.fecha', 'pedido.total',
                                                  'detallepedido.codigo as codigoproducto', 'detallepedido.cantidad as cantidadpedido',
                                                  'inventario.cantidad as cantidadinventario', 'inventario.preciocom as preciocomprainventario',
                                                  'inventario.precioven as precioventainventario', 'inventario.gananciasxpz as gananciasxpzinventario',
                                                  'inventario.gananciastot as gananciastotalesinventario',
                                                  'producto.descripcion as descripcionproducto', 'producto.preciocosto as preciocostoproducto',
                                                  'producto.precioventa as precioventaproducto',
                                                  'marca.descripcion as descripcionmarca',
                                                  'presentacion.medida as medidapresentacion', 
                                                  DB::raw( "CONCAT( proveedor.nombre,' ', proveedor.apellidopat,' ', proveedor.apellidomat ) AS nombreproveedor" ),
                                                  'categoria.nombre as nombrecategoria' )
                                        ->where( 'pedido.fecha', '=', Carbon::today() )
                                        ->where( 'pedido.idempleado', '=', $idempleado )
                                        ->get();
          */
          
          
          $consultascomprasactual = DB::table( 'pedido' )
                                   ->join( 'detallepedido', 'pedido.idpedido', '=', 'detallepedido.idpedido' )
                                   ->join( 'movimientoinventario', 'detallepedido.idmovimientoinventario', 'movimientoinventario.idmovimientoinventario')
                                   ->join( 'inventario', 'detallepedido.codigo', '=', 'inventario.codigo' )
                                   ->join('producto', 'detallepedido.codigo', '=', 'producto.codigo' )
                                        ->select( 'pedido.idpedido',
                                                  'detallepedido.codigo as codigoproducto', 'detallepedido.cantidad as cantidadpedido',
                                                  'producto.descripcion as descripcionproducto', 'movimientoinventario.PrecioCostoAct  as preciocostoproducto',
                                                  'movimientoinventario.PrecioVentaAct as precioventaproducto' )
                                        ->where( 'pedido.fecha', '=', Carbon::today() )
                                        ->where( 'pedido.idempleado', '=', $idempleado )
                                        ->where( 'pedido.estado', '=', 0 )
                                        ->orderBy('pedido.idpedido', 'ASC')
                                        ->get();

          $consultatotal = DB::table( 'pedido' )
                              ->select( DB::raw( "SUM( pedido.total ) as totalcompras" ) )
                              ->where( 'pedido.fecha', '=', Carbon::today() )
                              ->where( 'pedido.idempleado', '=', $idempleado )
                              ->where( 'pedido.estado', '=', 0 )
                              ->get();
          
          $cantidadtotalproductoscomprados = DB::table( 'pedido' )
                                        ->join( 'detallepedido', 'pedido.idpedido', '=', 'detallepedido.idpedido' )
                                        ->select( DB::raw( "SUM( detallepedido.cantidad ) as cantidadtotalproductoscomprados" ) )
                                        ->where( 'pedido.fecha', '=', Carbon::today() )
                                        ->where( 'pedido.idempleado', '=', $idempleado )
                                        ->where( 'pedido.estado', '=', 0 )
                                        ->get();

                                        
          $tienda = Tienda_Modelo::select('*')->take(1)->first();
                                        
          /*
          $pdf = \PDF::setOptions([
                    'images' => true,
                    'enable-javascript' => true,
                    'javascript-delay' => 5000,
                    'enable-smart-shrinking' => true,
                    'no-stop-slow-scripts' => true
               ])
          */
          $pdf = \PDF::loadView('vistasventas/cortescaja/exportarcortecajadiacompras', array(   'consultascomprasactual' => $consultascomprasactual, 
                                                                                          'consultatotal' => $consultatotal, 
                                                                                          'cantidadtotalproductoscomprados' => $cantidadtotalproductoscomprados, 
                                                                                          'fechaactual' => $fechaactual, 
                                                                                          'tienda' => $tienda,
                                                                                          'idcaja' => $idcaja,
                                                                                          'informacionempleado' => $informacionempleado ) )
               ->setPaper('a4', 'landscape')->setWarnings(false);

          return $pdf->download( 'CorteCajaCompras '. $fechaactual . '.pdf');

     }




               //FUNCIONES API
     public function buscarproductoventa( $codigo ) {

          $producto = Producto_Modelo::select('*')
          ->join('inventario', 'producto.codigo', '=', 'inventario.codigo' )
          ->where('producto.codigo', '=', $codigo )
          ->take(1)->first();


          $subconsultaproducto = Producto_Modelo::select("*")
          ->where('producto.codigo', '=', $codigo )
          ->where('producto.estado', '=', '1' )
          ->get();

          if( count( $subconsultaproducto ) == 0 ) {

               return response( "El producto aún no está registrado en el sistema" );
               //dd( "No hay producto registrado" );

          } else {

               $subconsultainventario = Inventario_Modelo::select('*')
               ->where('inventario.codigo', '=', $codigo )
               ->where('inventario.cantidad', '>', 0 )
               ->get();

               if( count( $subconsultainventario ) == 0 ) {

                    return response( 'Aún no existen unidades registradas de este productos en el inventario' );
                    //dd( "No productos en el inventario " );

               } else {

                    return response()->json( $producto );

               }

          }

     }

     public function cantidadmaximainventarioproducto ( $codigo ) {

          $resultadoconsulta = Inventario_Modelo::select("*")
                    ->where('inventario.codigo', '=', $codigo )
                    ->get();

          return response()->json( $resultadoconsulta )->header('Content-Type', 'application/json');;

     }

     public function filtrarclientespornombre( $nombre ) {

          /*
          return response()->json( $cliente = Cliente_Modelo::select("idcliente", DB::raw("CONCAT(cliente.nombre,' ',
                         cliente.apellidopat,' ',cliente.apellidomat) as nombrecompleto"))
                         ->where('cliente.nombre','LIKE','%'.$nombre.'%')->get());
          */

          return response()->json(
                              Cliente_Modelo::select("cliente.idcliente",
                              DB::raw("CONCAT(cliente.nombre,' ', cliente.apellidopat,' ',cliente.apellidomat) as nombrecompleto") )
                              ->join('creditocliente', 'cliente.idcliente', '=', 'creditocliente.idcliente')
                              ->where('cliente.estado', '=', '1')
                              ->where('creditocliente.estado', '=', '1')
                              ->where('cliente.nombre','LIKE','%'.$nombre.'%')->get()
                              );

     }

}
