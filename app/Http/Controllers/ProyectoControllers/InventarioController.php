<?php

namespace App\Http\Controllers\ProyectoControllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Session;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

use App\Models\MovimientoInventario_Modelo;
use App\Models\Inventario_Modelo;
use App\Models\TipoMovimiento_Modelo;

use App\Models\Proveedor_Modelo;
use App\Models\Categoria_Modelo;
use App\Models\Marca_Modelo;
use App\Models\Presentacion_Modelo;
use PhpOffice\PhpSpreadsheet\Calculation\Category;

class InventarioController extends Controller
{

    public function listadomovimientosinventario( Request $request ) {
        
        //$movimientos = MovimientoInventario_Modelo::select('*')->paginate(7);
        $movimientos = MovimientoInventario_Modelo::select('*')->get();
        
        if( count( $movimientos ) == 0 ) {
            
            Session::flash('danger', ' No hay movimientos que coincidan con la búsqueda ');

        }

        return view('vistasinventario/listadomovimientosinventario')->with('registromovimientos', $movimientos );

    }

    public function catalogoproductos() {

        /*
            $listadocategorias = DB::table('categoria')
                            ->select( DB::raw(' DISTINCT nombre') )
                            ->groupBy( 'nombre' )
                            ->get();

            $listadomarcas = DB::table('marca')
                            ->select( 'descripcion as descripcion', 'urlimagen as urlimagen' )
                            ->distinct('marca.descripcion')
                            ->orderBy( 'marca.descripcion', 'asc' )
                            ->get();

            $listadopresentaciones = DB::table('presentacion')
                            ->select( DB::raw(' DISTINCT medida') )
                            ->groupBy( 'presentacion.medida' )
                            ->get();
        */
        
        $listadocategorias = DB::table('categoria')
                        ->select( 'nombre as nombre', 'urlimagen as urlimagen' )
                        ->distinct('categoria.nombre')
                        ->orderBy( 'categoria.nombre', 'asc' )
                        ->get();

        $listadomarcas = DB::table('marca')
                        ->select( 'descripcion as descripcion', 'urlimagen as urlimagen' )
                        ->distinct('marca.descripcion')
                        ->orderBy( 'marca.descripcion', 'asc' )
                        ->get();

        $listadopresentaciones = DB::table('presentacion')
                        ->select( 'medida as medida', 'urlimagen as urlimagen' )
                        ->distinct('presentacion.medida')
                        ->orderBy( 'presentacion.medida', 'asc' )
                        ->get();

        $listaalertasinventario = DB::table('inventario')
            ->join('producto' , 'inventario.codigo' , '=' ,'producto.codigo')
            ->join('marca', 'producto.idmarca', '=', 'marca.idmarca')
            ->join('presentacion', 'producto.idpresentacion', '=', 'presentacion.idpresentacion')
            ->select( 'marca.descripcion as nombremarca',
                        'inventario.idInventario as idinventario',
                        'inventario.codigo as codigoinventario',
                        'inventario.Stockminimo as stockminimoinventario',
                        'producto.Descripcion as descripcionproducto',
                        'producto.Stockminimo as stockminimoproducto',
                        'presentacion.medida as presentacionproducto',
                        'inventario.Cantidad as cantidadproducto')
            ->where("inventario.Cantidad", "<=" , \DB::raw('inventario.Stockminimo'))
            ->orderBy('inventario.Cantidad','ASC')
            ->get();

        return view('vistasinventario/catalogoproductos') ->with( 'listadocategorias', $listadocategorias )
                ->with( 'listadomarcas', $listadomarcas ) ->with( 'listadopresentaciones', $listadopresentaciones )->with( 'listaalertasinventario', $listaalertasinventario );

    }

    public function filtradocatalogoproductos( $opcion, $filtro ) {        
        
        //$filtro = strtoupper( $filtro );
        $filtro = str_replace(' ', '', strtoupper( $filtro ) );

        if( $opcion == "Categorias" ) {              

            $listadoproductos = DB::table('producto')
            ->join('marca', 'producto.idmarca', '=', 'marca.idmarca')
            ->join('presentacion', 'producto.idpresentacion', '=', 'presentacion.idpresentacion')
            ->join('proveedor', 'producto.idproveedor', '=', 'proveedor.idproveedor')
            ->join('categoria', 'producto.idcategoria', '=', 'categoria.idcategoria')
            ->select('producto.codigo', 'producto.idmarca', 'marca.descripcion as nombremarca',
                    'producto.idpresentacion','presentacion.medida as nombrepresentacion', 'producto.idproveedor',
                    DB::raw( "CONCAT( proveedor.nombre,' ', proveedor.apellidopat,' ', proveedor.apellidomat ) AS nombrecompleto" ),
                    'producto.idcategoria', 'categoria.nombre as nombrecategoria', 'producto.descripcion',
                    'producto.preciocosto','producto.precioventa', 'producto.iva')
                    ->where('producto.estado',1)
                    ->where( DB::raw('replace( upper( categoria.nombre ), " ", "" )' ), '=', $filtro )
                    ->orderBy('codigo','ASC')
                    ->get();

        } else if( $opcion == "Marcas" ) {              

            $listadoproductos = DB::table('producto')
            ->join('marca', 'producto.idmarca', '=', 'marca.idmarca')
            ->join('presentacion', 'producto.idpresentacion', '=', 'presentacion.idpresentacion')
            ->join('proveedor', 'producto.idproveedor', '=', 'proveedor.idproveedor')
            ->join('categoria', 'producto.idcategoria', '=', 'categoria.idcategoria')
            ->select('producto.codigo', 'producto.idmarca', 'marca.descripcion as nombremarca',
                    'producto.idpresentacion','presentacion.medida as nombrepresentacion', 'producto.idproveedor',
                    DB::raw( "CONCAT( proveedor.nombre,' ', proveedor.apellidopat,' ', proveedor.apellidomat ) AS nombrecompleto" ),
                    'producto.idcategoria', 'categoria.nombre as nombrecategoria', 'producto.descripcion',
                    'producto.preciocosto','producto.precioventa', 'producto.iva')
                    ->where('producto.estado',1)
                    ->where( DB::raw('replace( upper( marca.descripcion ), " ", "")' ), '=', $filtro )
                    ->orderBy('codigo','ASC')
                    ->get();

        } else if( $opcion == "Presentaciones" ) {                          

            $listadoproductos = DB::table('producto')
            ->join('marca', 'producto.idmarca', '=', 'marca.idmarca')
            ->join('presentacion', 'producto.idpresentacion', '=', 'presentacion.idpresentacion')
            ->join('proveedor', 'producto.idproveedor', '=', 'proveedor.idproveedor')
            ->join('categoria', 'producto.idcategoria', '=', 'categoria.idcategoria')
            ->select('producto.codigo', 'producto.idmarca', 'marca.descripcion as nombremarca',
                    'producto.idpresentacion','presentacion.medida as nombrepresentacion', 'producto.idproveedor',
                    DB::raw( "CONCAT( proveedor.nombre,' ', proveedor.apellidopat,' ', proveedor.apellidomat ) AS nombrecompleto" ),
                    'producto.idcategoria', 'categoria.nombre as nombrecategoria', 'producto.descripcion',
                    'producto.preciocosto','producto.precioventa', 'producto.iva')
                    ->where('producto.estado',1)
                    ->where( DB::raw('replace( upper( presentacion.medida ), " ", "" )' ), '=', $filtro )
                    ->orderBy('codigo','ASC')
                    ->get();

        }
        
        if( count( $listadoproductos ) == 0 ) {

            if( $opcion == "Categorias" ) {
                
                Session::flash('danger', ' Aún no hay productos registrados en esta categoría');

            } else if( $opcion == "Marcas" ) { 
                
                Session::flash('danger', ' Aún no hay productos registrados para esta marca');

            } else if( $opcion == "Presentaciones" ) {
                
                Session::flash('danger', ' Aún no hay productos registrados con esta presentación');

            }

            

        }

        return view('vistasinventario/filtradocatalogoproductos')->with( 'listadoproductos', $listadoproductos );

    }


    public function alertasstockminimo() {
        
        $listaalertasinventario = DB::table('inventario')
            ->join('producto' , 'inventario.codigo' , '=' ,'producto.codigo')
            ->join('marca', 'producto.idmarca', '=', 'marca.idmarca')
            ->join('presentacion', 'producto.idpresentacion', '=', 'presentacion.idpresentacion')
            ->select( 'marca.descripcion as nombremarca',
                        'inventario.idInventario as idinventario',
                        'inventario.codigo as codigoinventario',
                        'inventario.Stockminimo as stockminimoinventario',
                        'producto.Descripcion as descripcionproducto',
                        'producto.Stockminimo as stockminimoproducto',
                        'presentacion.medida as presentacionproducto',
                        'inventario.Cantidad as cantidadproducto')
            ->where("inventario.Cantidad", "<=" , \DB::raw('inventario.Stockminimo') )
            ->orderBy('inventario.Cantidad','ASC')
            ->get();

        return view('vistasinventario/alertasstockminimo')->with( 'listaalertasinventario', $listaalertasinventario );
    }

        //Funciones para registrar entradas
        
    public function registrarentradainventario() {

        $tipomovimientos = TipoMovimiento_Modelo::select('idtipomovimiento','descripcion')
                                                  ->where('nombre', '=', 'Entrada')
                                                  ->where('idtipomovimiento', '<>', 103 )
                                                  ->get();

        return view('vistasinventario/registrarentradainventario')->with('movimientosinventario', $tipomovimientos );
        
    }

    public function insertarentradainventario ( Request $request ) {
               
        $info_array = json_decode( $request->TableData );
        //$idempleado = 1;
        $idempleado = Auth::User()->idEmpleado;
        $fecha = Carbon::now();

        $idtipomovimiento   = json_decode( $request->IdTipoMovimiento );
        $descripcion        = json_decode( $request->DescripcionMovimiento);

   
        //Se recorre el array de productos de la tabla html
        foreach( $info_array as $k ) {
   
            $idproducto = $k->{'ColumnaCodigos'};
            $cantidad = $k->{'ColumnaCantidades'};
            $preciocom = $k->{'ColumnaPrecioCom'};
            $precioven = $k->{'ColumnaPrecioVen'};
            
            //Se hace un consulta para saber si hay algún producto con ese código en la tabla inventario
            $cons = DB::table('inventario')->select( DB::raw( "COUNT( codigo ) AS total" ))
            ->where('codigo','=', $idproducto)
            ->get();            

            //Se obtiene el total de la consulta
            foreach( $cons as $k ) {
                $totalproductos = $k->{'total'};
            }

            //Se verifica si es igual a 0, lo que significa que no hay producto registrado en la tabla de inventario
            //y se tiene que crear su registro
            if( $totalproductos == 0 ) {
                

                $consultaproductostockmin = DB::table('producto')
                                        ->select( 'stockminimo', 'preciocosto', 'precioventa' )
                                        ->where('codigo','=', $idproducto)
                                        ->get();
                                
                $stockminimo = $consultaproductostockmin[0]->{'stockminimo'};                 

                DB::table('producto')
                ->where('codigo', $idproducto)
                ->limit(1)
                ->update( array( 'PrecioVenta' => $precioven, 'PrecioCosto' => $preciocom ) );


                $gananciasxpz = 0.00;
                $gananciastot = 0.00;

                DB::insert('insert into inventario ( codigo, cantidad, preciocom, precioven, gananciasxpz, gananciastot, stockminimo ) 
                            values ( ?, ?, ?, ?, ?, ?, ? )', [ $idproducto, $cantidad, $preciocom, $precioven, $gananciasxpz, $gananciastot, $stockminimo ] );

                $cantidadant = 0;
                $cantidadact = $cantidad;
                $cantidadmov = $cantidad;

                $preciocostoact = $preciocom;
                $precioventaact = $precioven;

                $preciocostoant = 0.0;
                $precioventaant = 0.0;

                DB::insert('insert into movimientoinventario( idTipoMovimiento , idempleado, codigo, fechahora, 
                            descripcion, cantidadant, cantidadact, cantidadmov, preciocostoant, preciocostoact, precioventaant, precioventaact ) 
                            values( ?, ?, ?, ?, ?, ?, ? ,?, ?, ?, ?, ? )',
                                [ $idtipomovimiento, $idempleado, $idproducto, $fecha,
                                $descripcion, $cantidadant, $cantidadact, $cantidadmov, 
                                $preciocostoant, $preciocostoact, $precioventaant, $precioventaact ] );


            } else {

                $consultaproductostockmin = DB::table('producto')
                                        ->select( 'stockminimo' )
                                        ->where('codigo','=', $idproducto)
                                        ->get();
                                
                $stockminimo = $consultaproductostockmin[0]->{'stockminimo'};

            

                //Se obtienen los datos del producto registrado en la tabla inventario
                $productoinventario = DB::table('inventario')
                                            ->select( 'cantidad', 'preciocom', 'precioven' )
                                                   ->where('codigo','=', $idproducto)
                                                   ->get();          

                //Se obtienen los atributos del objeto productoinventario                
                $cantidadant = $productoinventario[0]->{'cantidad'};
                $cantidadact = $cantidadant + $cantidad;
                $cantidadmov = $cantidad;                


                //Calcular los nuevos precios de compra y de venta                    
                $preciocostoant = $productoinventario[0]->{'preciocom'};
                $precioventaant = $productoinventario[0]->{'precioven'};
            
                DB::insert('insert into movimientoinventario( idTipoMovimiento , idempleado, codigo, fechahora, 
                descripcion, cantidadant, cantidadact, cantidadmov, preciocostoant, preciocostoact, precioventaant, precioventaact ) 
                values( ?, ?, ?, ?, ?, ?, ? ,?, ?, ?, ?, ? )',
                    [ $idtipomovimiento, $idempleado, $idproducto, $fecha,
                    $descripcion, $cantidadant, $cantidadact, $cantidadmov, 
                    $preciocostoant, $preciocom, $precioventaant, $precioven ] );

                                    
                $consultamovimientoinventarioentradasprecios = DB::table('movimientoinventario')
                                                ->select(   DB::raw('Sum(PrecioCostoAct *CantidadMov) as debe'),
                                                            DB::raw('Sum(PrecioVentaAct*CantidadMov) as obtenido')  )
                                                            ->where( 'codigo', '=', $idproducto )
                                                            //->where( 'idtipomovimiento', '=', $idtipomovimiento )
                                                            ->where('descripcion', 'like', '%ingreso%')
                                                            ->get();
                                                            
                $consultamovimientoinventariosalidasprecios = DB::table('movimientoinventario')
                                                ->select(   DB::raw('Sum(PrecioCostoAct *CantidadMov) as debe'),
                                                            DB::raw('Sum(PrecioVentaAct*CantidadMov) as obtenido')  )
                                                            ->where( 'codigo', '=', $idproducto )
                                                            //->where( 'idtipomovimiento', '=', $idtipomovimiento )
                                                            ->where('descripcion', 'like', '%salida%')
                                                            ->get();                                                            


                $sumaentradatotalpreciocompra = $consultamovimientoinventarioentradasprecios[0]->{'debe'};
                echo "TOTALPRECIOCOMPRA: ". $sumaentradatotalpreciocompra;

                $sumaentradatotalprecioventa = $consultamovimientoinventarioentradasprecios[0]->{'obtenido'};
                echo "sumatotalprecioventa: ". $sumaentradatotalprecioventa;

                $sumasalidatotalpreciocompra = $consultamovimientoinventariosalidasprecios[0]->{'debe'};
                $sumasalidatotalprecioventa = $consultamovimientoinventariosalidasprecios[0]->{'obtenido'};

                $totalpreciocompra = $sumaentradatotalpreciocompra - $sumasalidatotalpreciocompra;
                $totalprecioventa = $sumaentradatotalprecioventa - $sumasalidatotalprecioventa;

                $nuevopreciocompra = bcadd( ( $totalpreciocompra / $cantidadact ) , '0', 2 );
                echo "nuevopreciocompra: ". $nuevopreciocompra;

                $nuevoprecioventa = bcadd( ( $totalprecioventa / $cantidadact ), '0', 2 );
                echo "nuevoprecioventa: ". $nuevoprecioventa;

                //$gananciastot = bcadd( ( $totalprecioventa - $totalpreciocompra ), '0', 2 );
                //$gananciaxpz = bcadd( ( $gananciastot / $cantidadact ), '0', 2 );

                $gananciaxpz = 0.0;
                $gananciastot = 0.0;


                DB::table('producto')
                    ->where( 'codigo', $idproducto )
                    ->limit(1)
                    ->update( array( 'PrecioVenta' => $nuevoprecioventa, 'PrecioCosto' => $nuevopreciocompra ) );

                    //Actualizar la cantidad de existencias y precios del producto en inventario
                DB::table('inventario')
                    ->where( 'codigo', $idproducto )
                    ->limit(1)
                    ->update( array(    'Cantidad' => $cantidadact, 'PrecioCom' => $nuevopreciocompra, 'PrecioVen' => $nuevoprecioventa,
                                        'Gananciasxpz' => $gananciaxpz, 'Gananciastot' => $gananciastot,  'Stockminimo' => $stockminimo ) );
                    

                $this->verificaryactualizarpreciospromociones( $idproducto );

            }

       }
        
    }

    public function verificaryactualizarpreciospromociones( $codigo ) {

        echo "\NENTRA A METODO verificaryactualizarpreciospromociones";

        $consultaproducto = DB::table('producto')
        ->select('precioventa as precioventa' )
        ->where('codigo', '=', $codigo )
        ->get();

        $precioproducto = $consultaproducto[0]->{'precioventa'};
        
        $consultaverificaroferta = DB::table('oferta')
            ->join('promociones', 'oferta.idpromociones', '=', 'promociones.idpromociones')
            ->select( DB::raw("COUNT(oferta.codigo) as total" ) )            
            ->where('oferta.codigo', '=', $codigo )
            ->where('promociones.estado', '=', 1 )
            ->get();

        $totalpromocionoferta = $consultaverificaroferta[0]->{'total'};

        if( $totalpromocionoferta > 0 ) {

            echo "\NENTRA A IF OFERTA";

            $consultainfooferta = DB::table('oferta')
                        ->join('promociones', 'oferta.idpromociones', '=', 'promociones.idpromociones')
                        ->select(  'promociones.idpromociones as idpromociones', 'oferta.idoferta as idoferta',
                         'oferta.tipo as tipooferta' )
                        ->where('oferta.codigo', '=', $codigo )
                        ->where('promociones.estado', '=', 1 )
                        ->get();

            $tipooferta = $consultainfooferta[0]->{'tipooferta'};
            $idpromociones = $consultainfooferta[0]->{'idpromociones'};
            $idoferta = $consultainfooferta[0]->{'idoferta'};


            if( $tipooferta = 1 ) {

                $preciototalpromocion = bcadd( ( $precioproducto * $tipooferta ) , '0', 2);
                $preciooferta = bcadd( ( $preciototalpromocion / 2 ) , '0', 2 );

            } else if( $tipooferta = 2 ) {

                $preciototalpromocion = bcadd( ( $precioproducto * $tipooferta ) , '0', 2);
                $preciooferta = bcadd( ( $preciototalpromocion / 3 ) , '0', 2 );

            } else if( $tipooferta = 3 ) {

                $preciototalpromocion = bcadd( ( $precioproducto * $tipooferta ) , '0', 2);
                $preciooferta = bcadd( ( $preciototalpromocion / 4 ) , '0', 2 );

            } else if( $tipooferta = 4 ) {

                $preciototalpromocion = bcadd( ( $precioproducto * $tipooferta ) , '0', 2);
                $preciooferta = bcadd( ( $preciototalpromocion / 5 ) , '0', 2 );

            }

            DB::table('promociones')
                ->where( 'idpromociones', $idpromociones )
                ->limit(1)
                ->update( array(    'Total' => $preciototalpromocion ) );

            DB::table('oferta')
                ->where( 'idoferta', $idoferta )
                ->limit(1)
                ->update( array(    'Precionuevo' => $preciooferta ) );
            
        } else if( $totalpromocionoferta == 0 ) {

            echo "\NENTRA A ELSEIF OFERTA";

            $consultaverificardescuento = DB::table('descuento')
            ->join('promociones', 'descuento.idpromociones', '=', 'promociones.idpromociones')
            ->select( DB::raw("COUNT(descuento.codigo) as total" ) )            
            ->where('descuento.codigo', '=', $codigo )
            ->where('promociones.estado', '=', 1 )
            ->get();

            $totalpromociondescuento = $consultaverificardescuento[0]->{'total'};


            if( $totalpromociondescuento > 0 ) {

                echo "\NENTRA A IF DESCUENTO";

                $consultainfodescuento = DB::table('descuento')
                            ->join('promociones', 'descuento.idpromociones', '=', 'promociones.idpromociones')
                            ->select(  'promociones.idpromociones as idpromociones', 'descuento.iddescuento as iddescuento',
                             'descuento.descuento as porcentajedescuento' )
                            ->where('descuento.codigo', '=', $codigo )
                            ->where('promociones.estado', '=', 1 )
                            ->get();
    
                $porcentajedescuento = $consultainfodescuento[0]->{'porcentajedescuento'};
                $idpromociones = $consultainfodescuento[0]->{'idpromociones'};
                $iddescuento = $consultainfodescuento[0]->{'iddescuento'};

                $descuento = bcadd( ( ( $precioproducto * $porcentajedescuento ) / 100 ), '0', 2 );
                $preciodescuento = bcadd( ( $precioproducto - $descuento ) , '0', 2 );

                DB::table('promociones')
                    ->where( 'idpromociones', $idpromociones )
                    ->limit(1)
                    ->update( array(    'Total' => $preciodescuento ) );
    
                DB::table('descuento')
                    ->where( 'iddescuento', $iddescuento )
                    ->limit(1)
                    ->update( array(    'Precionuevo' => $preciodescuento ) );

            } else if( $totalpromociondescuento == 0 ) {

                echo "\NENTRA A ELSEIF OFERTA";

                $consultaverificarpaquete1 = DB::table('paquetes')
                ->join('promociones', 'paquetes.idpromociones', '=', 'promociones.idpromociones')
                ->select( DB::raw("COUNT(paquetes.codigo1) as total" ) )            
                ->where('paquetes.codigo1', '=', $codigo )
                ->where('promociones.estado', '=', 1 )
                ->get();

                $totalpromocionpaquete1 = $consultaverificarpaquete1[0]->{'total'};

                if( $totalpromocionpaquete1 > 0 ) {

                    echo "\NENTRA A IF PAQUETE1";

                    $consultainfopaquete1 = DB::table('paquetes')
                    ->join('promociones', 'paquetes.idpromociones', '=', 'promociones.idpromociones')
                    ->select( 'promociones.idpromociones as idpromociones', 'paquetes.idpaquetes as idpaquetes',
                    'paquetes.descuento1 as porcentajedescuento1', 'paquetes.precio2 as preciocomplementario' )            
                    ->where('paquetes.codigo1', '=', $codigo )
                    ->where('promociones.estado', '=', 1 )
                    ->get();
    
                    $porcentajedescuento1 = $consultainfopaquete1[0]->{'porcentajedescuento1'};
                    $idpromociones = $consultainfopaquete1[0]->{'idpromociones'};
                    $idpaquetes = $consultainfopaquete1[0]->{'idpaquetes'};
                    $preciocomplementario = $consultainfopaquete1[0]->{'preciocomplementario'};
    
                    $descuento = bcadd( ( ( $precioproducto * $porcentajedescuento1 ) / 100 ), '0', 2 );
                    $preciodescuentopaquete = bcadd( ( $precioproducto - $descuento ) , '0', 2 );

                    $nuevopreciopromocion = bcadd( ( $preciocomplementario + $preciodescuentopaquete ) , '0', 2 );

                    DB::table('promociones')
                        ->where( 'idpromociones', $idpromociones )
                        ->limit(1)
                        ->update( array(    'Total' => $nuevopreciopromocion ) );
        
                    DB::table('paquetes')
                        ->where( 'idpaquetes', $idpaquetes )
                        ->limit(1)
                        ->update( array(    'Precio1' => $preciodescuentopaquete ) );

                } else if( $totalpromocionpaquete1 == 0 ) {

                    echo "\NENTRA A ELSEIF PAQUETE1";

                    $consultaverificarpaquete2 = DB::table('paquetes')
                    ->join('promociones', 'paquetes.idpromociones', '=', 'promociones.idpromociones')
                    ->select( DB::raw("COUNT(paquetes.codigo2) as total" ) )            
                    ->where('paquetes.codigo2', '=', $codigo )
                    ->where('promociones.estado', '=', 1 )
                    ->get();
    
                    $totalpromocion2 = $consultaverificarpaquete2[0]->{'total'};

                    if( $totalpromocion2 > 0 ) {

                        echo "\NENTRA A IF PAQUETE2";

                        $consultainfopaquete2 = DB::table('paquetes')
                        ->join('promociones', 'paquetes.idpromociones', '=', 'promociones.idpromociones')
                        ->select( 'promociones.idpromociones as idpromociones', 'paquetes.idpaquetes as idpaquetes',
                        'paquetes.descuento2 as porcentajedescuento2', 'paquetes.precio1 as preciocomplementario' )            
                        ->where('paquetes.codigo2', '=', $codigo )
                        ->where('promociones.estado', '=', 1 )
                        ->get();
        
                        $porcentajedescuento2 = $consultainfopaquete2[0]->{'porcentajedescuento2'};
                        $idpromociones = $consultainfopaquete2[0]->{'idpromociones'};
                        $idpaquetes = $consultainfopaquete2[0]->{'idpaquetes'};
                        $preciocomplementario = $consultainfopaquete2[0]->{'preciocomplementario'};
        
                        $descuento = bcadd( ( ( $precioproducto * $porcentajedescuento2 ) / 100 ), '0', 2 );
                        $preciodescuentopaquete = bcadd( ( $precioproducto - $descuento ) , '0', 2 );
                        
                        $nuevopreciopromocion = bcadd( ( $preciocomplementario + $preciodescuentopaquete ) , '0', 2 );
    
                        DB::table('promociones')
                            ->where( 'idpromociones', $idpromociones )
                            ->limit(1)
                            ->update( array(    'Total' => $nuevopreciopromocion ) );
            
                        DB::table('paquetes')
                            ->where( 'idpaquetes', $idpaquetes )
                            ->limit(1)
                            ->update( array(    'Precio2' => $preciodescuentopaquete ) );

                    }

                }

            }

        }


    }

        //Funciones para registrar salidas
        
    public function registrarsalidainventario() {

        $tipomovimientos = TipoMovimiento_Modelo::select('idtipomovimiento','descripcion')
                                                  ->where('nombre', '=', 'Salida')                                                  
                                                  ->where('idtipomovimiento', '<>', 203 )
                                                  ->get();

        return view('vistasinventario/registrarsalidainventario')->with('movimientosinventario', $tipomovimientos );

    }

    public function insertarsalidainventario ( Request $request ) {
               
        $info_array = json_decode( $request->TableData );
        //$idempleado = 1;
        $idempleado = Auth::User()->idEmpleado;
        $fecha = Carbon::now();

        $idtipomovimiento   = json_decode( $request->IdTipoMovimiento );;
        $descripcion        = json_decode( $request->DescripcionMovimiento);

   
        //Se recorre el array de productos de la tabla html
        foreach( $info_array as $k ) {
   
            $idproducto = $k->{'ColumnaCodigos'};
            $cantidad = $k->{'ColumnaCantidades'};
            $preciocom = $k->{'ColumnaPrecioCom'};
            $precioven = $k->{'ColumnaPrecioVen'};
            
            //Se hace un consulta para saber si hay algún producto con ese código en la tabla inventario
            $cons = DB::table('inventario')->select( DB::raw( "COUNT( codigo ) AS total" ))
            ->where('codigo','=', $idproducto)
            ->get();            

            //Se obtiene el total de la consulta
            foreach( $cons as $k ) {
                $totalproductos = $k->{'total'};
            }

            //Se verifica si es igual a 0, lo que significa que no hay producto registrado en la tabla de inventario
            //y se tiene que crear su registro            
            if( $totalproductos == 0 ) {
                

                $consultaproductostockmin = DB::table('producto')
                                        ->select( 'stockminimo', 'preciocosto', 'precioventa' )
                                        ->where('codigo','=', $idproducto)
                                        ->get();
                                
                $stockminimo = $consultaproductostockmin[0]->{'stockminimo'};                 

                DB::table('producto')
                ->where('codigo', $idproducto)
                ->limit(1)
                ->update( array( 'PrecioVenta' => $precioven, 'PrecioCosto' => $preciocom ) );

                $gananciasxpz = 0.0;
                $gananciastot = 0.0;

                DB::insert('insert into inventario ( codigo, cantidad, preciocom, precioven, gananciasxpz, gananciastot, stockminimo ) 
                            values ( ?, ?, ?, ?, ?, ?, ? )', [ $idproducto, $cantidad, $preciocom, $precioven, $gananciasxpz, $gananciastot, $stockminimo ] );

                $cantidadant = 0;
                $cantidadact = $cantidad;
                $cantidadmov = $cantidad;

                $preciocostoact = $preciocom;
                $precioventaact = $precioven;

                $preciocostoant = 0.0;
                $precioventaant = 0.0;

                DB::insert('insert into movimientoinventario( idTipoMovimiento , idempleado, codigo, fechahora, 
                            descripcion, cantidadant, cantidadact, cantidadmov, preciocostoant, preciocostoact, precioventaant, precioventaact ) 
                            values( ?, ?, ?, ?, ?, ?, ? ,?, ?, ?, ?, ? )',
                                [ $idtipomovimiento, $idempleado, $idproducto, $fecha,
                                $descripcion, $cantidadant, $cantidadact, $cantidadmov, 
                                $preciocostoant, $preciocostoact, $precioventaant, $precioventaact ] );


            } else {

                $consultaproductostockmin = DB::table('producto')
                                        ->select( 'stockminimo' )
                                        ->where('codigo','=', $idproducto)
                                        ->get();
                                
                $stockminimo = $consultaproductostockmin[0]->{'stockminimo'};

            

                //Se obtienen los datos del producto registrado en la tabla inventario
                $productoinventario = DB::table('inventario')
                                            ->select( 'cantidad', 'preciocom', 'precioven' )
                                                   ->where('codigo','=', $idproducto)
                                                   ->get();          

                //Se obtienen los atributos del objeto productoinventario                
                $cantidadant = $productoinventario[0]->{'cantidad'};
                $cantidadact = $cantidadant - $cantidad;
                $cantidadmov = $cantidad;                


                //Calcular los nuevos precios de compra y de venta                    
                $preciocostoant = $productoinventario[0]->{'preciocom'};
                $precioventaant = $productoinventario[0]->{'precioven'};
                $preciocostoact = $preciocostoant;
                $precioventaact = $precioventaant;
            
                DB::insert('insert into movimientoinventario( idTipoMovimiento , idempleado, codigo, fechahora, 
                descripcion, cantidadant, cantidadact, cantidadmov, preciocostoant, preciocostoact, precioventaant, precioventaact ) 
                values( ?, ?, ?, ?, ?, ?, ? ,?, ?, ?, ?, ? )',
                    [ $idtipomovimiento, $idempleado, $idproducto, $fecha,
                    $descripcion, $cantidadant, $cantidadact, $cantidadmov, 
                    $preciocostoant, $preciocom, $precioventaant, $precioven ] );
                                    
                $consultamovimientoinventarioentradasprecios = DB::table('movimientoinventario')
                                                ->select(   DB::raw('Sum(PrecioCostoAct *CantidadMov) as debe'),
                                                            DB::raw('Sum(PrecioVentaAct*CantidadMov) as obtenido')  )
                                                            ->where( 'codigo', '=', $idproducto )
                                                            //->where( 'idtipomovimiento', '=', $idtipomovimiento )
                                                            ->where('descripcion', 'like', '%ingreso%')
                                                            ->get();
                                                            
                $consultamovimientoinventariosalidasprecios = DB::table('movimientoinventario')
                                                ->select(   DB::raw('Sum(PrecioCostoAct *CantidadMov) as debe'),
                                                            DB::raw('Sum(PrecioVentaAct*CantidadMov) as obtenido')  )
                                                            ->where( 'codigo', '=', $idproducto )
                                                            //->where( 'idtipomovimiento', '=', $idtipomovimiento )
                                                            ->where('descripcion', 'like', '%salida%')
                                                            ->get();                                                            


                $sumaentradatotalpreciocompra = $consultamovimientoinventarioentradasprecios[0]->{'debe'};
                echo "sumaentradatotalpreciocompra: ". $sumaentradatotalpreciocompra;

                $sumaentradatotalprecioventa = $consultamovimientoinventarioentradasprecios[0]->{'obtenido'};
                echo "sumaentradatotalprecioventa: ". $sumaentradatotalprecioventa;

                $sumasalidatotalpreciocompra = $consultamovimientoinventariosalidasprecios[0]->{'debe'};
                echo "sumasalidatotalpreciocompra: ". $sumaentradatotalpreciocompra;

                $sumasalidatotalprecioventa = $consultamovimientoinventariosalidasprecios[0]->{'obtenido'};
                echo "sumasalidatotalprecioventa: ". $sumaentradatotalprecioventa;

                $totalpreciocompra = $sumaentradatotalpreciocompra - $sumasalidatotalpreciocompra;
                echo "nuevopreciocompra: ". $totalpreciocompra;

                $totalprecioventa = $sumaentradatotalprecioventa - $sumasalidatotalprecioventa;
                echo "nuevopreciocompra: ". $totalprecioventa;

                $nuevopreciocompra = 0.0;
                $nuevoprecioventa = 0.0;
 
                if( $cantidadact > 0 ) {
 
                     $nuevopreciocompra = bcadd( ( $totalpreciocompra / $cantidadact ) , '0', 2 );
                     echo "nuevopreciocompra: ". $nuevopreciocompra;
 
                     $nuevoprecioventa = bcadd( ( $totalprecioventa / $cantidadact ), '0', 2 );
                     echo "nuevoprecioventa: ". $nuevoprecioventa;
 
                } else {
 
                     $nuevopreciocompra = $preciocom;
                     $nuevoprecioventa = $precioven;
 
                }

                //$gananciastot = bcadd( ( $totalprecioventa - $totalpreciocompra ), '0', 2 );
                //$gananciaxpz = bcadd( ( $gananciastot / $cantidadact ), '0', 2 );

                //$gananciaxpz = 0.0;
                //$gananciastot = 0.0;


                DB::table('producto')
                    ->where( 'codigo', $idproducto )
                    ->limit(1)
                    ->update( array( 'PrecioVenta' => $nuevoprecioventa, 'PrecioCosto' => $nuevopreciocompra ) );

                    //Actualizar la cantidad de existencias y precios del producto en inventario
                    /*
                    DB::table('inventario')
                    ->where( 'codigo', $idproducto )
                    ->limit(1)
                    ->update( array(    'Cantidad' => $cantidadact, 'PrecioCom' => $nuevopreciocompra, 'PrecioVen' => $nuevoprecioventa,
                                        'Gananciasxpz' => $gananciaxpz, 'Gananciastot' => $gananciastot,  'Stockminimo' => $stockminimo ) );
                                        
                    */
                    DB::table('inventario')
                    ->where( 'codigo', $idproducto )
                    ->limit(1)
                    ->update( array( 'Cantidad' => $cantidadact, 'PrecioCom' => $nuevopreciocompra, 'PrecioVen' => $nuevoprecioventa, 'Stockminimo' => $stockminimo ) );

            }

       }

    }

    

    //FUNCIONES PARA CAMBIAR IMAGENES DE LAS CATEGORIAS, MARCAS O PRESENTACIONES
    function cambiarinformacionfiltrocatalogoproductos( $opcionfiltrado, $filtro ) {

        //dd("OPCION FILTRADO: ", $opcionfiltrado , " FILTRO: ", $filtro );

        if( $opcionfiltrado == "Categorias") {

            $informacioncategoria = DB::table('categoria')
                ->select( 'categoria.idcategoria as idCategoria', 'categoria.nombre as NombreCategoria',
                        'categoria.descripcion as DescripcionCategoria', 'categoria.estado as EstadoCategoria', 
                        'categoria.urlimagen as UrlImagen' )
                ->where('categoria.estado',1)
                ->where( DB::raw('replace( upper( categoria.nombre ), " ", "" )' ), '=', $filtro )
                ->take(1)
                ->first();

            //$urlimagen = "/images/CatalogoProductos/Categorias/" . $informacioncategoria->{'NombreCategoria'} ."Thumb.jpg";

            //return view('vistasinventario/vistascategorias/modificarcategoria')->with( 'informacioncategoria', $informacioncategoria )->with( 'urlimagen', $urlimagen );
            return view('vistasinventario/vistascategorias/modificarcategoria')->with( 'informacioncategoria', $informacioncategoria );

        } else if( $opcionfiltrado == "Marcas" ) {

            $informacionmarca = DB::table('marca')
                ->select( 'marca.idmarca as idMarca', 'marca.descripcion as DescripcionMarca',
                        'marca.idproveedor as idProveedor', 'marca.estado as EstadoMarca',
                        'marca.urlimagen as UrlImagen' )
                ->where('marca.estado',1)
                ->where( DB::raw('replace( upper( marca.descripcion ), " ", "" )' ), '=', $filtro )
                ->take(1)
                ->first();
                
            $proveedores = Proveedor_Modelo::select("idproveedor", DB::raw("CONCAT(proveedor.nombre,' ',
                proveedor.apellidopat,' ',proveedor.apellidomat) as nombrecompleto"))
                ->where('estado','=','1')
                ->pluck('nombrecompleto', 'idproveedor')->toArray();

            //$urlimagen = "/images/CatalogoProductos/Marcas/" . $informacionmarca->{'DescripcionMarca'} ."Thumb.jpg";

            //return view('vistasinventario/vistasmarcas/modificarmarca')->with( 'informacionmarca', $informacionmarca )->with( 'urlimagen', $urlimagen );
            return view('vistasinventario/vistasmarcas/modificarmarca')->with( 'informacionmarca', $informacionmarca )->with( 'proveedores', $proveedores );

        } else if( $opcionfiltrado == "Presentaciones" ) {

            $informacionpresentacion = DB::table('presentacion')
                ->select( 'presentacion.idpresentacion as idPresentacion', 'presentacion.medida as MedidaPresentacion',
                          'presentacion.estado as EstadoPresentacion',
                          'presentacion.urlimagen as UrlImagen'  )
                ->where('presentacion.estado',1)
                ->where( DB::raw('replace( upper( presentacion.medida ), " ", "" )' ), '=', $filtro )
                ->take(1)
                ->first();                                    

            //$urlimagen = "/images/CatalogoProductos/Presentaciones/" . $informacionpresentacion->{'DescripcionMarca'} ."Thumb.jpg";

            //return view('vistasinventario/vistaspresentaciones/modificarpresentacion')->with( 'informacionmarca', $informacionpresentacion )->with( 'urlimagen', $urlimagen );
            return view('vistasinventario/vistaspresentaciones/modificarpresentacion')->with( 'informacionpresentacion', $informacionpresentacion );

        } else {

            return redirect()->back();

        }        

    }

    function registrarcategoria() {

        return view('vistasinventario/vistascategorias/registrarcategoria');

    }
    
    function actualizarinformacioncategoria( Request $request, $idcategoria ) {
            
        if( $request->botonformcategoria == "Modificar" ) {   

            if( $request->hasFile('avatar') ) {
                
            
                $file = $request->file( 'avatar' );
                $nombreimagen = str_replace(' ', '', strtoupper( $request->nombre ) );

                //$name = time().$file->getClientOriginalName();

                $name = 'CatalogoProductos/Categorias/' . $nombreimagen . "Thumb." . $request->file('avatar')->extension();
                $file->move( public_path().'/images/CatalogoProductos/Categorias/', $nombreimagen . "Thumb." . $request->file('avatar')->extension() );
                

                /*  //SE GUARDAN EN LA CARPETA STORAGE/APP/PUBLIC/PRODUCTOS
                //$name = $request->file('avatar')->store('public/Productos');

                $name = $request->file('avatar')->storeAs(
                    'public/Productos',  $request -> input('descripcion') . "." . $request->file('avatar')->extension()
                );

                */
                

                DB::table('categoria')
                ->where('idcategoria',$idcategoria)  // find your user by their email
                ->limit(1)  // optional - to ensure only one record is updated.
                ->update( array( 'Nombre' => $request->nombre,
                                'Descripcion'=> $request->descripcion,
                                'UrlImagen' => $name ) ); 

            } else {                    

                DB::table('categoria')
                ->where('idcategoria',$idcategoria)  // find your user by their email
                ->limit(1)  // optional - to ensure only one record is updated.
                ->update( array( 'Nombre' => $request->nombre,
                            'Descripcion'=> $request->descripcion ) ); 

            } // update the record in the DB.


            Session::flash('success', ' Se ha actualizado exitosamente la información de la categoría ');

            return redirect()->to('catalogoproductos');

        } else if( $request->botonformcategoria == "Cancelar" ) {

            return redirect()->to('filtradocatalogoproductos/Categorias/'. str_replace(' ', '', $request->nombre ) );

        }

    }

    function registrarmarca() {

        $proveedores = Proveedor_Modelo::select("idproveedor", DB::raw("CONCAT(proveedor.nombre,' ',
                        proveedor.apellidopat,' ',proveedor.apellidomat) as nombrecompleto"))
                        ->where('estado','=','1')
                        ->pluck('nombrecompleto', 'idproveedor')->toArray();

        return view('vistasinventario/vistasmarcas/registrarmarca')->with( 'proveedores', $proveedores );

    }

    function actualizarinformacionmarca( Request $request, $idmarca ) {
            
        if( $request->botonformmarca == "Modificar" ) {  

            if( $request->hasFile('avatar') ) {

                
                $file = $request->file( 'avatar' );                
                $nombreimagen = str_replace(' ', '', strtoupper( $request->descripcion ) );
                //$name = time().$file->getClientOriginalName();

                $name = 'CatalogoProductos/Marcas/' . $nombreimagen . "Thumb." . $request->file('avatar')->extension();
                $file->move( public_path().'/images/CatalogoProductos/Marcas/', $nombreimagen . "Thumb." . $request->file('avatar')->extension() );

                /*  //SE GUARDAN EN LA CARPETA STORAGE/APP/PUBLIC/PRODUCTOS
                //$name = $request->file('avatar')->store('public/Productos');

                $name = $request->file('avatar')->storeAs(
                    'public/Productos',  $request -> input('descripcion') . "." . $request->file('avatar')->extension()
                );

                */    

                DB::table('marca')
                ->where('idmarca',$idmarca)  // find your user by their email
                ->limit(1)  // optional - to ensure only one record is updated.
                ->update( array(    'Descripcion'=> $request->descripcion,
                                    'UrlImagen' => $name ) );  // update the record in the DB.

            } else {    

                DB::table('marca')
                ->where('idmarca',$idmarca)  // find your user by their email
                ->limit(1)  // optional - to ensure only one record is updated.
                ->update( array(    'Descripcion'=> $request->descripcion ) );  // update the record in the DB.

            }

            Session::flash('success', ' Se ha actualizado exitosamente la información de la marca ');


            return redirect()->to('catalogoproductos');

        } else if( $request->botonformmarca == "Cancelar" ) {

            return redirect()->to('filtradocatalogoproductos/Marcas/'. str_replace(' ', '', $request->descripcion ) );

        }

    }

    function registrarpresentacion() {

        return view('vistasinventario/vistaspresentaciones/registrarpresentacion');

    }

    function actualizarinformacionpresentacion( Request $request, $idpresentacion ) {
            
        if( $request->botonformpresentacion == "Modificar" ) { 

            if( $request->hasFile('avatar') ) {

                
                $file = $request->file( 'avatar' );
                $nombreimagen = str_replace(' ', '', strtoupper( $request->medida ) );
                //$name = time().$file->getClientOriginalName();

                $name = 'CatalogoProductos/Presentaciones/' . $nombreimagen . "Thumb." . $request->file('avatar')->extension();
                $file->move( public_path().'/images/CatalogoProductos/Presentaciones/', $nombreimagen . "Thumb." . $request->file('avatar')->extension() );
                

                /*  //SE GUARDAN EN LA CARPETA STORAGE/APP/PUBLIC/PRODUCTOS
                //$name = $request->file('avatar')->store('public/Productos');

                $name = $request->file('avatar')->storeAs(
                    'public/Productos',  $request -> input('descripcion') . "." . $request->file('avatar')->extension()
                );

                */
                    

                DB::table('presentacion')
                ->where('idpresentacion',$idpresentacion)  // find your user by their email
                ->limit(1)  // optional - to ensure only one record is updated.
                ->update( array(    'Medida' => $request->medida,
                                    'UrlImagen' => $name ) );  // update the record in the DB.

            } else {    

                DB::table('presentacion')
                ->where('idpresentacion',$idpresentacion)  // find your user by their email
                ->limit(1)  // optional - to ensure only one record is updated.
                ->update( array( 'Medida' => $request->medida ) );  // update the record in the DB.

            }

            Session::flash('success', ' Se ha actualizado exitosamente la información de la presentación ');


            return redirect()->to('catalogoproductos');



        } else if( $request->botonformpresentacion == "Cancelar" ) {

            return redirect()->to('filtradocatalogoproductos/Presentaciones/'. str_replace(' ', '', $request->medida ) );

        }

    }
    
}
