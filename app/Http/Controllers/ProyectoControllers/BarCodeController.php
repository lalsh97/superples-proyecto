<?php

namespace App\Http\Controllers\ProyectoControllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Producto_Modelo;
use DB;

class BarCodeController extends Controller
{
    
    public function codigobarrasproducto() {

        $listadoproductos = DB::table('producto')
        ->join('marca', 'producto.idmarca', '=', 'marca.idmarca')
        ->join('presentacion', 'producto.idpresentacion', '=', 'presentacion.idpresentacion')
        ->join('proveedor', 'producto.idproveedor', '=', 'proveedor.idproveedor')
        ->join('categoria', 'producto.idcategoria', '=', 'categoria.idcategoria')
        ->select('producto.codigo', 'producto.idmarca', 'marca.descripcion as nombremarca',
                'producto.idpresentacion','presentacion.medida as nombrepresentacion', 'producto.idproveedor',
                DB::raw( "CONCAT( proveedor.nombre,' ', proveedor.apellidopat,' ', proveedor.apellidomat ) AS nombrecompleto" ),
                'producto.idcategoria', 'categoria.nombre as nombrecategoria', 'producto.descripcion',
                'producto.preciocosto','producto.precioventa', 'producto.iva')
                ->where('producto.estado',1)
                ->where('proveedor.estado',1)
                ->orderBy('codigo','ASC')
                ->get();

        return view('vistasproductos/codigobarras/codigobarrasproducto')->with('registroproductos', $listadoproductos );

    }

    public function imprimircodigobarra( $codigo ) {

        $codigoproducto = DB::table('producto')
                            ->where('codigo', '=', $codigo )
                            ->where('estado', '=', 1 )
                            ->get();

        return view('vistasproductos/codigobarras/imprimircodigobarra')->with( 'codigoproducto', $codigoproducto );

    }

}
