
<?php

namespace App\Http\Controllers\ProyectoControllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Session;
use Illuminate\Support\Facades\Validator;

    //Modelos
use App\Models\Marca_Modelo;
use App\Models\Presentacion_Modelo;
use App\Models\Proveedor_Modelo;
use App\Models\Categoria_Modelo;
use App\Models\Producto_Modelo;

class Hojacontrolador extends Controller
{

    public function listadoproductos() {

        $listadoproductos = DB::table('producto')
        ->join('marca', 'producto.idmarca', '=', 'marca.idmarca')
        ->join('presentacion', 'producto.idpresentacion', '=', 'presentacion.idpresentacion')
        ->join('proveedor', 'producto.idproveedor', '=', 'proveedor.idproveedor')
        ->join('categoria', 'producto.idcategoria', '=', 'categoria.idcategoria')
        ->select('producto.codigo', 'producto.idmarca', 'marca.descripcion as nombremarca',
                'producto.idpresentacion','presentacion.medida as nombrepresentacion', 'producto.idproveedor',
                DB::raw( "CONCAT( proveedor.nombre,' ', proveedor.apellidopat,' ', proveedor.apellidomat ) AS nombrecompleto" ),
                'producto.idcategoria', 'categoria.nombre as nombrecategoria', 'producto.descripcion',
                'producto.preciocosto','producto.precioventa', 'producto.iva')
                ->where('producto.estado',1)
                ->orderBy('codigo','ASC')
                ->get();

        return view('vistasreportes/productos/generarreporteproductos')->with('productos', $listadoproductos);

    }

}
