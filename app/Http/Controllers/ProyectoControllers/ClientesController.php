<?php

namespace App\Http\Controllers\ProyectoControllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Session;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;

use App\Models\AbonosCredito_Modelo;
use App\Models\Cliente_Modelo;
use App\Models\CreditoCliente_Modelo;

class ClientesController extends Controller
{


    public function listadoclientes() {

      $listadoclientes = DB::table('cliente')
            ->select('idcliente',
            DB::raw( "CONCAT( nombre,' ', apellidopat,' ', apellidomat ) AS nombrecompleto" ),
           'fechanac', 'sexo', 'telefono', 'curp','estado')
            ->where( 'estado', 1 )
            ->where( 'idCliente', '<>', 1)
            ->orderBy('idcliente','ASC')
            ->get();



        return view('vistasclientes/listadoclientes')->with( 'registroclientes', $listadoclientes );

  	}

    public function busquedacliente( $operacion ) {

        return view('vistasclientes/busquedacliente')->with( 'operacion' , $operacion );

    }

    public function buscarcliente( $curp ) {

        
        $cliente = Cliente_Modelo::where('curp', $curp )->where('estado', 1)->get();

        if( count( $cliente ) == 1 ) {
            
            $listadoclientes = DB::table('cliente')
                ->select('idcliente',
                    DB::raw( "CONCAT( nombre,' ', apellidopat,' ', apellidomat ) AS nombrecompleto" ),
                   'fechanac', 'sexo', 'telefono', 'curp','estado')
                    ->where('curp',$curp)
                    ->where('estado', 1)
                    ->orderBy('idcliente','ASC')
                    ->get();


                return view ('vistasclientes/resultadobusquedacliente')->with('registroclientes', $listadoclientes);

        } else {

            Session::flash('warning', ' No existe cliente registrado con esa CURP ');
            return redirect()->back();

        }

    }

    //Funciones para registrar
    public function registrarclientes() {

        return view('vistasclientes/registrarcliente');

    }

    public function registrarrapidoclientes() {

        return view('vistasclientes/registrarrapidocliente')->with('caso', 'onload');

    }

    public function insertarcliente( Request $request ) {

        if( $request->botonformcliente == 'Registrar' ) {

            $validator = Validator::make($request->all(), Cliente_Modelo::rules(), Cliente_Modelo::messages());

            if( $validator ->fails() ) {
                return redirect()->back()->withInput()->withErrors($validator->errors());
            }


            $nombre =       $request -> input('nombre');
            $apellidopat =  $request -> input('apellidopat');
            $apellidomat =  $request -> input('apellidomat');
            $sexo =         $request-> input('sexo');
            $fechanac =     $request-> input('fechanac');
            $curp =         $request-> input('curp');
            $telefono =     $request-> input('telefono');
            $limite =       $request-> input('limite');

            $calle =        $request -> input('calle');
            $numero =       $request -> input('numero');
            $codigop =      $request -> input('codigop');
            $colonia =      $request -> input('colonia');
            $municipio =    $request -> input('municipio');
            $ciudad =       $request-> input('ciudad');
            $estado = 1;

            Cliente_Modelo::create( ['nombre' => $nombre,
            'apellidopat' => $apellidopat, 'apellidomat' => $apellidomat, 'sexo' => $sexo,
            'fechanac' => $fechanac, 'curp' => $curp, 'telefono' => $telefono,
            'calle' => $calle, 'numero' => $numero, 'codigop' => $codigop,
            'colonia' => $colonia, 'municipio' => $municipio, 'ciudad' => $ciudad,
            'estado' => $estado] );

            $consulta = Cliente_Modelo::select('idcliente')
                            ->orderBy('idcliente', 'desc')
                            ->limit(1)
                            ->get();

            foreach( $consulta as $k ) {
                $idcliente = $k->{'idcliente'};
            }

            DB::insert('insert into creditocliente (idCliente, Limite, CanAdeudada, Estado)
                    values ( ?, ?, ?, ?)', [ $idcliente, $limite, 0, $estado]);

            Session::flash('success', ' Se ha registrado exitosamente al cliente ');

            return redirect()->back();

        } else if( $request->botonformcliente == 'RegistrarRapido' ) {

            $validator = Validator::make($request->all(), Cliente_Modelo::rules(), Cliente_Modelo::messages());

            if( $validator ->fails() ) {
                return redirect()->back()->withInput()->withErrors($validator->errors());
            }


            $nombre =       $request -> input('nombre');
            $apellidopat =  $request -> input('apellidopat');
            $apellidomat =  $request -> input('apellidomat');
            $sexo =         $request-> input('sexo');
            $fechanac =     $request-> input('fechanac');
            $curp =         $request-> input('curp');
            $telefono =     $request-> input('telefono');
            $limite =       $request-> input('limite');

            $calle =        $request -> input('calle');
            $numero =       $request -> input('numero');
            $codigop =      $request -> input('codigop');
            $colonia =      $request -> input('colonia');
            $municipio =    $request -> input('municipio');
            $ciudad =       $request-> input('ciudad');
            $estado = 1;

            Cliente_Modelo::create( ['nombre' => $nombre,
            'apellidopat' => $apellidopat, 'apellidomat' => $apellidomat, 'sexo' => $sexo,
            'fechanac' => $fechanac, 'curp' => $curp, 'telefono' => $telefono,
            'calle' => $calle, 'numero' => $numero, 'codigop' => $codigop,
            'colonia' => $colonia, 'municipio' => $municipio, 'ciudad' => $ciudad,
            'estado' => $estado] );

            $consulta = Cliente_Modelo::select('idcliente')
                            ->orderBy('idcliente', 'desc')
                            ->limit(1)
                            ->get();

            foreach( $consulta as $k ) {
                $idcliente = $k->{'idcliente'};
            }

            DB::insert('insert into creditocliente (idCliente, Limite, CanAdeudada, Estado)
                    values ( ?, ?, ?, ?)', [ $idcliente, $limite, 0, $estado]);

            return view('vistasclientes/registrarrapidocliente')->with('caso', 'close');

        } else if ( $request ->botonformcliente == 'Cancelar' ) {

            return redirect()->to('home');

        }

    }

    //Funciones para actualizar cliente
    public function modificarcliente( $idCliente ) {

        $cliente = DB::table('cliente')
        ->join('creditocliente', 'cliente.idCliente', '=', 'creditocliente.idCliente')
        ->select('cliente.idCliente', 'cliente.nombre as Nombre', 'cliente.apellidopat as ApellidoPat',
                'cliente.apellidomat as ApellidoMat','cliente.fechanac as FechaNac', 'cliente.sexo as Sexo',
                'cliente.curp as Curp','cliente.calle as Calle','cliente.numero as Numero',
                'cliente.codigop as CodigoP','cliente.colonia as Colonia', 'cliente.municipio as Municipio',
                'cliente.ciudad as Ciudad','cliente.telefono as Telefono',
                'cliente.estado as Estado','creditocliente.Limite as Limite')
                ->where('cliente.idCliente',$idCliente)
                ->where('cliente.estado', 1)
                ->take(1)
                ->first();

        //$cliente = Cliente_Modelo::select('*')->where('idCliente', $idCliente)->take(1)->first();

        return view('vistasclientes/modificarcliente') -> with('cliente',$cliente);

    }
    
    public function modificarclientecurp( $curp ) {

        $cliente = Cliente_Modelo::select('*')->where('curp', $curp)->where('estado', 1)->get();

        if( count( $cliente ) == 1 ) {
            
            $cliente = DB::table('cliente')
            ->join('creditocliente', 'cliente.idCliente', '=', 'creditocliente.idCliente')
            ->select('cliente.idCliente', 'cliente.nombre as Nombre', 'cliente.apellidopat as ApellidoPat',
                    'cliente.apellidomat as ApellidoMat','cliente.fechanac as FechaNac', 'cliente.sexo as Sexo',
                    'cliente.curp as Curp','cliente.calle as Calle','cliente.numero as Numero',
                    'cliente.codigop as CodigoP','cliente.colonia as Colonia', 'cliente.municipio as Municipio',
                    'cliente.ciudad as Ciudad','cliente.telefono as Telefono',
                    'cliente.estado as Estado','creditocliente.Limite as Limite')
                    ->where('cliente.curp',$curp)
                    ->where('cliente.estado', 1)
                    ->take(1)
                    ->first();
            
            return view('vistasclientes/modificarcliente') -> with('cliente',$cliente);

        } else {

            Session::flash('warning', ' No hay cliente registrado con esa CURP ');
            return redirect()->back();

        }

    }

    public function actualizarinfocliente( Request $request , $idCliente ) {

        if( $request->botonformmodificar == 'Modificar' ) {

                DB::table('cliente')
                ->where('idCliente',$idCliente)  // find your user by their email
                ->limit(1)  // optional - to ensure only one record is updated.
                ->update( array( 'telefono' => $request->telefono,
                                'calle' => $request->calle,
                                'numero' => $request->numero,
                                'codigop' => $request->codigop,
                                'colonia' => $request->colonia,
                                'municipio' => $request->municipio,
                                'ciudad' => $request->ciudad) );  // update the record in the DB.

                DB::table('creditocliente')
                ->where('idCliente',$idCliente)
                ->limit(1)
                ->update( array( 'limite' => $request->limite ) );

                return redirect()->to('listadoclientes');

        }  else if( $request->botonformmodificar == 'Cancelar' ) {

            return redirect()->to('home');

        }

    }

      //Funciones para descontinuar cliente
    public function descontinuarcliente( $idCliente ) {

        $cliente = DB::table('cliente')
        ->join('creditocliente', 'cliente.idCliente', '=', 'creditocliente.idCliente')
        ->select('cliente.idCliente', 'cliente.nombre as Nombre', 'cliente.apellidopat as ApellidoPat',
                'cliente.apellidomat as ApellidoMat', 'cliente.fechanac as FechaNac', 'cliente.sexo as Sexo',
                'cliente.curp as Curp', 'cliente.calle as Calle', 'cliente.numero as Numero',
                'cliente.codigop as CodigoP', 'cliente.colonia as Colonia', 'cliente.municipio as Municipio',
                'cliente.ciudad as Ciudad', 'cliente.telefono as Telefono',
                'cliente.estado as Estado', 
                'creditocliente.Limite as Limite', 'creditocliente.CanAdeudada as CantidadAdeudada')
                ->where( 'cliente.idCliente', $idCliente )
                ->where( 'cliente.estado', 1)
                ->take(1)
                ->first();

        //$cliente = Cliente_Modelo::select('*')->where('idCliente', $idCliente)->take(1)->first();

        return view('vistasclientes/descontinuarcliente') -> with( 'cliente', $cliente );
    }
    
    public function descontinuarclientecurp( $curp ) {
        

        $cliente = Cliente_Modelo::select('*')->where('curp', $curp)->where('estado', 1)->get();

        if( count( $cliente ) == 1 ) {

            $cliente = DB::table('cliente')
            ->join('creditocliente', 'cliente.idCliente', '=', 'creditocliente.idCliente')
            ->select('cliente.idCliente', 'cliente.nombre as Nombre', 'cliente.apellidopat as ApellidoPat',
                    'cliente.apellidomat as ApellidoMat', 'cliente.fechanac as FechaNac', 'cliente.sexo as Sexo',
                    'cliente.curp as Curp', 'cliente.calle as Calle', 'cliente.numero as Numero',
                    'cliente.codigop as CodigoP', 'cliente.colonia as Colonia', 'cliente.municipio as Municipio',
                    'cliente.ciudad as Ciudad', 'cliente.telefono as Telefono',
                    'cliente.estado as Estado', 
                    'creditocliente.Limite as Limite', 'creditocliente.CanAdeudada as CantidadAdeudada')
                    ->where( 'cliente.curp', $curp )
                    ->where( 'cliente.estado', 1)
                    ->take(1)
                    ->first();

            return view('vistasclientes/descontinuarcliente') -> with( 'cliente', $cliente );

        } else {

            Session::flash('warning', ' No hay cliente registrado con esa CURP ');
            return redirect()->back();

        }
    }

    public function cambiarestadocliente( Request $request, $idCliente ) {

        if( $request->botonformdescontinuar == 'Descontinuar' ) {

            DB::table('cliente')
            ->where( 'idCliente' ,$idCliente )  // find your user by their email
            ->update( array( 'Estado' => 0 ) );  // update the record in the DB.

            DB::table('creditocliente')
            ->where( 'idCliente', $idCliente )
            ->limit(1)
            ->update( array( 'Estado' => 0 ) );

            return redirect()->to('listadoclientes');

        } else if( $request->botonformdescontinuar == 'Cancelar' ) {

            return redirect()->to('home');

        }

    }

    //Nueva ruta para que funcione el botón de eliminar del modal de confirmación de la vista descontinuarcliente
    public function eliminarcambiarestadocliente( $idcliente ) {

        DB::table('cliente')
                ->where( 'idCliente' ,$idcliente ) 
                ->update( array( 'Estado' => 0 ) );

        DB::table('creditocliente')
                ->where( 'idCliente', $idcliente )
                ->limit(1)
                ->update( array( 'Estado' => 0 ) );

        return redirect()->to('listadoclientes');

    }

    public function listadocreditossincredito( ) {


        $listadoclientessincredito = DB::table( 'cliente' )
                        ->select( 'cliente.idcliente as idcliente',
                            DB::raw( "CONCAT( nombre,' ', apellidopat,' ', apellidomat ) AS nombrecompleto" ),
                            DB::raw( "CONCAT( calle,' #', numero, ', ', codigop, ',Colonia ', colonia, ', ', municipio ) AS direccioncompleta" ),
                            'cliente.telefono as telefonocliente' )
                        ->leftJoin('creditocliente', 'cliente.idcliente', '=', 'creditocliente.idcliente')
                        ->where( 'cliente.idCliente', '<>', 1)
                        ->whereNull('creditocliente.idcliente')
                        ->orWhere('creditocliente.estado', '=', 0)
                        ->get();

        return view('vistasclientes/creditosclientes/listadoclientessincredito')->with( 'listadoclientessincredito', $listadoclientessincredito );

    }

    public function activarcreditocliente( $idCliente ) {

        $informacioncliente = DB::table( 'cliente' )
                                ->select( 'cliente.idcliente as idcliente',
                                DB::raw( "CONCAT( nombre,' ', apellidopat,' ', apellidomat ) AS nombrecompleto" ),
                                DB::raw( "CONCAT( calle,' #', numero, ', ', codigop, ',Colonia ', colonia, ', ', municipio ) AS direccioncompleta" ),
                                'cliente.telefono as telefonocliente',
                                'creditocliente.limite as limitecredito',
                                'creditocliente.canadeudada as cantidadadeudada',
                                'creditocliente.estado as estadocredito' )
                            ->join('creditocliente', 'cliente.idcliente', '=', 'creditocliente.idcliente')
                            ->where( 'cliente.idcliente', $idCliente )
                            ->get();

        if( count( $informacioncliente ) > 0 ) {

                $informacioncliente = DB::table('cliente')
                ->join('creditocliente', 'cliente.idCliente', '=', 'creditocliente.idCliente')
                ->select('cliente.idCliente', 'cliente.nombre as Nombre', 'cliente.apellidopat as ApellidoPat',
                        'cliente.apellidomat as ApellidoMat','cliente.fechanac as FechaNac', 'cliente.sexo as Sexo',
                        'cliente.curp as Curp','cliente.calle as Calle','cliente.numero as Numero',
                        'cliente.codigop as CodigoP','cliente.colonia as Colonia', 'cliente.municipio as Municipio',
                        'cliente.ciudad as Ciudad','cliente.telefono as Telefono',
                        'cliente.estado as Estado', 'creditocliente.Limite as LimiteCredito',
                        'creditocliente.canadeudada as CantidadAdeudada', 'creditocliente.estado as EstadoCredito')
                        ->where('cliente.idCliente',$idCliente)
                        ->take(1)
                        ->first();

        } else {

            $informacioncliente = DB::table('cliente')
                ->select('cliente.idCliente', 'cliente.nombre as Nombre', 'cliente.apellidopat as ApellidoPat',
                    'cliente.apellidomat as ApellidoMat','cliente.fechanac as FechaNac', 'cliente.sexo as Sexo',
                    'cliente.curp as Curp','cliente.calle as Calle','cliente.numero as Numero',
                    'cliente.codigop as CodigoP','cliente.colonia as Colonia', 'cliente.municipio as Municipio',
                    'cliente.ciudad as Ciudad','cliente.telefono as Telefono',
                    'cliente.estado as Estado' )
                    ->where('cliente.idCliente',$idCliente)
                    ->take(1)
                    ->first();

        }

        return view( 'vistasclientes/creditosclientes/activarcreditocliente')->with( 'informacioncliente', $informacioncliente );

    }

    public function realizaractivacioncreditocliente( Request $request, $idcliente ) {

        if( $request->botonformactivarcreditocliente == 'Reactivar' ) {

            DB::table('creditocliente')
                ->where('idCliente',$idcliente)
                ->limit(1)
                ->update( array( 'Estado' => 1 ) );
            
            DB::table('cliente')
                ->where( 'idCliente', $idcliente )
                ->limit(1)
                ->update( array( 'Estado' => 1) );


            Session::flash('success', ' El crédito ha sido reactivado de manera exitosa ');

            return redirect()->to('listadocreditossincredito');

        } else if( $request->botonformactivarcreditocliente == 'Activar' ) {

            $limite = $request->input('limite');
            $estado = 1;


            DB::insert('insert into creditocliente (idCliente, Limite, CanAdeudada, Estado)
                    values ( ?, ?, ?, ?)', [ $idcliente, $limite, 0, $estado]);


            Session::flash('success', ' El crédito ha sido activado de manera exitosa ');

            return redirect()->to('listadocreditossincredito');

        } else if( $request->botonformactivarcreditocliente == 'Cancelar' ) {

            return redirect()->to('listadocreditossincredito');

        }

    }


    //FUNCIÓN API
    public function buscarinformacionclientecredito( $idcliente ) {

        $registrocreditos = Cliente_Modelo::select("cliente.idcliente as idCliente",
        DB::raw("CONCAT(cliente.nombre,' ', cliente.apellidopat,' ',cliente.apellidomat) as nombrecompleto" ),
        "cliente.estado as EstadoCliente", "creditocliente.idcreditocliente as idCreditoCliente",
        "creditocliente.limite as LimiteCredito", "creditocliente.canadeudada as InformacionCreditoCliente",
        "creditocliente.estado as EstadoCreditoCliente" )
        ->join("creditocliente", "cliente.idcliente", "=", "creditocliente.idcliente")
        ->where("cliente.idcliente", "=", $idcliente )
        ->where("creditocliente.estado", "=", "1" )
        ->take(1)->first();

        return response( $registrocreditos );

    }

    public function listadoselectclientes() {

        $listadoclientes = DB::table('cliente')
        ->join('creditocliente', 'cliente.idcliente', '=', 'creditocliente.idcliente')
        ->select('cliente.idcliente', DB::raw( "CONCAT( nombre,' ', apellidopat,' ', apellidomat ) AS nombrecompleto" ),
                'fechanac', 'sexo', 'telefono', 'curp','cliente.estado')
        ->where('cliente.estado', '=', '1')
        ->where('creditocliente.estado', '=', '1')
        ->orderBy('cliente.idcliente','ASC')
        ->get();

        return response( $listadoclientes );

    }

    public function verificarcreditocliente( $idcliente ) {

        return response()->json( CreditoCliente_Modelo::select( DB::raw( "COUNT(idcliente) AS total" ) )
                                    ->where('idcliente', '=', $idcliente )
                                    ->get() );
    }
}
