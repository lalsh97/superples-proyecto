<?php

namespace App\Http\Controllers\ProyectoControllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;
use DB;
use Session;
use Carbon\Carbon;


use App\Charts\UserChart;

    //FORMATO REPORTES EXPORTAR EXCEL
use App\Exports;
use App\Exports\Proveedores;
use App\Exports\Clientes;
use App\Exports\Productos;
use App\Exports\Promociones;
use App\Exports\GeneralVentas;
use App\Exports\Ventas;

use App\Models\MovimientoInventario_Modelo;
use App\Models\Proveedor_Modelo;
use App\Models\Promocion_Modelo;
use App\Models\Tienda_Modelo;

class ReportesController extends Controller
{

            //PRINCIPAL REPORTES
    public function reportes() {

        return view('vistasreportes/reporteshome');

    }

                            //REPORTES GENERALES



            //REPORTE CLIENTES

        //Función reporte
    public function reporteclientes() {

        $listadoclientes = DB::table('cliente')->select('idcliente',
                    DB::raw( "CONCAT( nombre,' ', apellidopat,' ', apellidomat ) AS nombrecompleto" ),
                            'fechanac', 'sexo', 'telefono', 'curp','estado')
                    ->where('estado',1)
                    ->orderBy('idcliente','ASC')
                    ->get();

        if( count( $listadoclientes ) == 0 ) {

            Session::flash('danger', ' Aún no hay registros para este reporte ');

        }

        return view('vistasreportes/clientes/reporteclientes') ->with('clientes', $listadoclientes);

    }

        //Vista a pdf
    public function generarreporteclientes() {
    
        $fechaactual = Carbon::now()->format('d-m-Y');

            $clientes = DB::table('cliente')->select('idcliente', DB::raw( "CONCAT( nombre,' ', apellidopat,' ', apellidomat ) AS nombrecompleto" ),
                              'fechanac', 'sexo', 'telefono', 'curp','estado')
                        ->where('estado',1)
                        ->orderBy('idcliente','ASC')
                        ->get();

        $tienda = Tienda_Modelo::select('*')->take(1)->first();

        /*
        $pdf = \PDF::setOptions([
                            'images' => true,
                            'enable-javascript' => true,
                            'javascript-delay' => 5000,
                            'enable-smart-shrinking' => true,
                            'no-stop-slow-scripts' => true
                    ])
        */
        
        $pdf = \PDF::loadView('vistasreportes/clientes/hoja', compact('clientes'),compact('tienda') )
                    ->setPaper('a4', 'landscape')->setWarnings(false);

        return $pdf->download('ReporteClientes '. $fechaactual .'.pdf');

    }

        //Vista Excel
    public function reporteclientesexcel() {
    
        $fechaactual = Carbon::now()->format('d-m-Y');

        return ( new Clientes )->download( 'ReporteClientes '. $fechaactual .'.xlsx' );        

    }
                   
                            

            //REPORTE PROVEEDORES

        //Función reporte
    public function reportesprovedores() {

        $listadoproveedores = Proveedor_Modelo::where('Estado','1')->get();

        if( count( $listadoproveedores ) == 0 ) {

            Session::flash('danger', ' No hay movimientos que coincidan con la búsqueda ');

        }

        return view('vistasreportes/proveedores/reporteproveedores') ->with('proveedores', $listadoproveedores);

    }

        //Vista a pdf
    public function generarreporteproveedores() {
    
        $fechaactual = Carbon::now()->format('d-m-Y');

        $proveedores = Proveedor_Modelo::where('Estado','1')->get();

        $tienda = Tienda_Modelo::select('*')->take(1)->first();

        /*
        $pdf = \PDF::setOptions([
                        'images' => true,
                        'enable-javascript' => true,

                        'javascript-delay' => 5000,
                        'enable-smart-shrinking' => true,
                        'no-stop-slow-scripts' => true
                    ])
        */

        $pdf = \PDF::loadView('vistasreportes/proveedores/hoja', compact('proveedores'), compact('tienda') )
                    ->setPaper('a4', 'landscape')->setWarnings(false);

        return $pdf->download('ReporteProveedores '. $fechaactual .'.pdf');

    }

        //Vista Excel
    public function reporteproveedoresexcel() {
    
        $fechaactual = Carbon::now()->format('d-m-Y');

        return (new Proveedores)->download('ReporteProveedores' . $fechaactual .'.xlsx');

    }


    
            //REPORTE PRODUCTOS

        //Función reporte
    public function reportesproductos() {

        $productos = DB::table('producto')
        ->join('marca', 'producto.idmarca', '=', 'marca.idmarca')
        ->join('presentacion', 'producto.idpresentacion', '=', 'presentacion.idpresentacion')
        ->join('proveedor', 'producto.idproveedor', '=', 'proveedor.idproveedor')
        ->join('categoria', 'producto.idcategoria', '=', 'categoria.idcategoria')
        ->select('producto.codigo', 'producto.idmarca', 'marca.descripcion as nombremarca',
                'producto.idpresentacion','presentacion.medida as nombrepresentacion', 'producto.idproveedor',
                DB::raw( "CONCAT( proveedor.nombre,' ', proveedor.apellidopat,' ', proveedor.apellidomat ) AS nombrecompleto" ),
                'producto.idcategoria', 'categoria.nombre as nombrecategoria', 'producto.descripcion',
                'producto.preciocosto','producto.precioventa', 'producto.iva')
                ->where('producto.estado',1)
                ->orderBy('codigo','ASC')
                ->get();

        if( count( $productos ) == 0 ) {

            Session::flash('danger', ' Aún no hay registros para este reporte ');

        }

        return view('vistasreportes/productos/reporteproductos') ->with('productos', $productos);

    }

        //Vista a pdf
    public function generarreporteproductos() {
    
        $fechaactual = Carbon::now()->format('d-m-Y');

        $tienda = Tienda_Modelo::select('*')->take(1)->first();

        $productos = DB::table('producto')
        ->join('marca', 'producto.idmarca', '=', 'marca.idmarca')
        ->join('presentacion', 'producto.idpresentacion', '=', 'presentacion.idpresentacion')
        ->join('proveedor', 'producto.idproveedor', '=', 'proveedor.idproveedor')
        ->join('categoria', 'producto.idcategoria', '=', 'categoria.idcategoria')
        ->select('producto.codigo', 'producto.idmarca', 'marca.descripcion as nombremarca',
                'producto.idpresentacion','presentacion.medida as nombrepresentacion', 'producto.idproveedor',
                DB::raw( "CONCAT( proveedor.nombre,' ', proveedor.apellidopat,' ', proveedor.apellidomat ) AS nombrecompleto" ),
                'producto.idcategoria', 'categoria.nombre as nombrecategoria', 'producto.descripcion',
                'producto.preciocosto','producto.precioventa', 'producto.iva')
                ->where('producto.estado',1)
                ->orderBy('codigo','ASC')
                ->get();

        /*
        $pdf = \PDF::setOptions([
                'images' => true,
                'enable-javascript' => true,
                'javascript-delay' => 5000,
                'enable-smart-shrinking' => true,
                'no-stop-slow-scripts' => true
            ])
        */

        $pdf = \PDF::loadView('vistasreportes/productos/hoja', compact('productos') , compact('tienda') )
                ->setPaper('a4', 'landscape')->setWarnings(false);

        return $pdf->download('ReporteProductos '. $fechaactual .'.pdf');

    }

        //Vista Excel
    public function reporteproductosexcel() {
    
        $fechaactual = Carbon::now()->format('d-m-Y');

        //  return Excel::download(new Productos, "lista.xlsx");
        return (new Productos)->download('ReporteProductos '. $fechaactual .'.xlsx');

    }    
    
    
    
            //REPORTE PROMOCIONES

        //Función reporte
    public function reportepromociones() {

        $listadopromociones = Promocion_Modelo::where('estado','1')->get();

        if( count( $listadopromociones ) == 0 ) {

            Session::flash('danger', ' Aún no hay registros para este reporte ');

        }

        return view('vistasreportes/promociones/reportepromociones') ->with('promociones', $listadopromociones);

    }

        //Vista a pdf
    public function generarreportepromociones() {
    
        $fechaactual = Carbon::now()->format('d-m-Y');

        $promociones = Promocion_Modelo::where('estado','1')->get();

        $tienda = Tienda_Modelo::select('*')->take(1)->first();

        /*
        $pdf = \PDF::setOptions([
                        'images' => true,
                        'enable-javascript' => true,
                        'javascript-delay' => 5000,
                        'enable-smart-shrinking' => true,
                        'no-stop-slow-scripts' => true
                        ])
        */

        $pdf = \PDF::loadView('vistasreportes/promociones/hoja', compact('promociones'), compact('tienda') )
                    ->setPaper('a4', 'landscape')->setWarnings(false);

        return $pdf->download('ReportePromociones '. $fechaactual .'.pdf');

    }

        //Vista Excel
    public function reportepromocionesexcel() {
    
        $fechaactual = Carbon::now()->format('d-m-Y');

        libxml_use_internal_errors(true);

        return (new Promociones)->download('ReportePromociones '. $fechaactual .'.xlsx');
    }              
    


            //REPORTE GENERAL VENTAS

        //Función reporte
    public function reportegeneralventas() {

        /*
        $listadoproductos = DB::table('producto')
        ->join('inventario', 'producto.codigo', '=', 'inventario.codigo')
        ->join('marca', 'producto.idmarca', '=', 'marca.idmarca')
        ->join('presentacion', 'producto.idpresentacion', '=', 'presentacion.idpresentacion')
        ->join('proveedor', 'producto.idproveedor', '=', 'proveedor.idproveedor')
        ->join('categoria', 'producto.idcategoria', '=', 'categoria.idcategoria')
        ->select('producto.codigo as codigo', 'producto.idmarca', 'marca.descripcion as nombremarca',
                'producto.idpresentacion','presentacion.medida as nombrepresentacion', 'producto.idproveedor',
                DB::raw( "CONCAT( proveedor.nombre,' ', proveedor.apellidopat,' ', proveedor.apellidomat ) AS nombrecompleto" ),
                'producto.idcategoria', 'categoria.nombre as nombrecategoria', 'producto.descripcion as descripcion',
                'producto.preciocosto','producto.precioventa', 'producto.iva', 
                'inventario.cantidad as cantidadinventario', 'inventario.gananciasxpz as ganancias', 'inventario.gananciastot as gananciatotal' )
                ->where('producto.estado',1)
                ->orderBy('codigo','ASC')
                ->get();
            */

        $listadoproductos = DB::table('movimientoinventario')
                            ->join('producto', 'producto.codigo', '=', 'movimientoinventario.codigo')
                            ->select( 'movimientoinventario.codigo as codigo', 'producto.descripcion as descripcion',                       
                            DB::raw( "SUM(cantidadmov) as cantidad" ),
                            DB::raw( "SUM(preciocostoact * cantidadmov) as preciocosto" ),
                            DB::raw( "SUM(precioventaact * cantidadmov) as precioventa" ),
                            DB::raw( "SUM(precioventaact * cantidadmov) - SUM(preciocostoact * cantidadmov)  as gananciatotal" ), )
                            ->where( 'movimientoinventario.idtipomovimiento', '=', 203 )
                            ->groupBy( 'movimientoinventario.codigo' )
                            ->groupBy( 'producto.descripcion' )
                            ->get();

        if( count( $listadoproductos ) == 0 ) {

            Session::flash('danger', ' Aún no hay registros para este reporte ');
        }

        return view('vistasreportes/ventasgeneral/reportegeneralventas') ->with('listadoproductos', $listadoproductos );

    }

        //Vista a pdf
    public function generarreportegeneralventas() {   
    
        $fechaactual = Carbon::now()->format('d-m-Y');     

        /*
        $listadoproductos = DB::table('producto')
        ->join('inventario', 'producto.codigo', '=', 'inventario.codigo')
        ->join('marca', 'producto.idmarca', '=', 'marca.idmarca')
        ->join('presentacion', 'producto.idpresentacion', '=', 'presentacion.idpresentacion')
        ->join('proveedor', 'producto.idproveedor', '=', 'proveedor.idproveedor')
        ->join('categoria', 'producto.idcategoria', '=', 'categoria.idcategoria')
        ->select('producto.codigo as codigo', 'producto.idmarca', 'marca.descripcion as nombremarca',
                'producto.idpresentacion','presentacion.medida as nombrepresentacion', 'producto.idproveedor',
                DB::raw( "CONCAT( proveedor.nombre,' ', proveedor.apellidopat,' ', proveedor.apellidomat ) AS nombrecompleto" ),
                'producto.idcategoria', 'categoria.nombre as nombrecategoria', 'producto.descripcion as descripcion',
                'producto.preciocosto','producto.precioventa', 'producto.iva', 
                'inventario.cantidad as cantidadinventario', 'inventario.gananciasxpz as ganancias', 'inventario.gananciastot as gananciatotal' )
                ->where('producto.estado',1)
                ->orderBy('codigo','ASC')
                ->get();
        */
        

        $listadoproductos = DB::table('movimientoinventario')
                            ->join('producto', 'producto.codigo', '=', 'movimientoinventario.codigo')
                            ->select( 'movimientoinventario.codigo as codigo', 'producto.descripcion as descripcion',                       
                            DB::raw( "SUM(cantidadmov) as cantidad" ),
                            DB::raw( "SUM(preciocostoact * cantidadmov) as preciocosto" ),
                            DB::raw( "SUM(precioventaact * cantidadmov) as precioventa" ),
                            DB::raw( "SUM(precioventaact * cantidadmov) - SUM(preciocostoact * cantidadmov)  as gananciatotal" ), )
                            ->where( 'movimientoinventario.idtipomovimiento', '=', 203 )
                            ->groupBy( 'movimientoinventario.codigo' )
                            ->groupBy( 'producto.descripcion' )
                            ->get();

        $tienda = Tienda_Modelo::select('*')->take(1)->first();

        /*
        $pdf = \PDF::setOptions([
            'images' => true,
            'enable-javascript' => true,
            'javascript-delay' => 5000,
            'enable-smart-shrinking' => true,
            'no-stop-slow-scripts' => true
        ])
        */

        $pdf = \PDF::loadView('vistasreportes/ventasgeneral/hoja', compact('listadoproductos'), compact('tienda') )
            ->setPaper('a4', 'landscape')->setWarnings(false);

        return $pdf->download('ReporteGeneralVentas '. $fechaactual .'.pdf');

    }

        //Vista Excel
    public function reportegeneralventasexcel() {
    
        $fechaactual = Carbon::now()->format('d-m-Y');      

        return (new GeneralVentas)->download('ReporteGeneralVentas '. $fechaactual .'.xlsx');

    }



                //REPORTE VENTAS

        //Función reporte ventas - Vista con gráficas
    public function reporteventas() {

        $listadoventas = DB::table('venta')
                ->join('empleado', 'venta.idempleado', '=', 'empleado.idempleado')
                ->join('cliente', 'venta.idcliente', '=', 'cliente.idcliente')
                ->select( DB::raw("CONCAT( empleado.nombre,' ', empleado.apellidopat,' ',empleado.apellidomat) as nombrecompletoempleado"),
                          DB::raw("CONCAT( cliente.nombre,' ', cliente.apellidopat,' ',cliente.apellidomat) as nombrecompletocliente"),
                     'venta.idventa as idventa','venta.fecha as fechaventa', 'venta.total as totalventa', 'venta.idtipopago as tipopago' )
                     ->orderBy('venta.idventa', 'asc')
                    ->get();

        if( count( $listadoventas ) == 0 ) {

            Session::flash('danger', ' Aún no hay registros para este reporte ');

        }

        return view('vistasreportes/ventas/reporteventas') ->with('listadoventas', $listadoventas);

    }

        //Vista a pdf
    public function generarreporteventas() {
    
        $fechaactual = Carbon::now()->format('d-m-Y');     

        $tienda = Tienda_Modelo::select('*')->take(1)->first();

        $listadoventas = DB::table('venta')
            ->join('empleado', 'venta.idempleado', '=', 'empleado.idempleado')
            ->join('cliente', 'venta.idcliente', '=', 'cliente.idcliente')
            ->select( DB::raw("CONCAT( empleado.nombre,' ', empleado.apellidopat,' ',empleado.apellidomat) as nombrecompletoempleado"),
                        DB::raw("CONCAT( cliente.nombre,' ', cliente.apellidopat,' ',cliente.apellidomat) as nombrecompletocliente"),
                'venta.idventa as idventa','venta.fecha as fechaventa', 'venta.total as totalventa', 'venta.idtipopago as tipopago' )
                ->orderBy('venta.idventa', 'asc')
                ->get();

        /*
        $pdf = \PDF::setOptions([
            'images' => true,
            'enable-javascript' => true,
            'javascript-delay' => 5000,
            'enable-smart-shrinking' => true,
            'no-stop-slow-scripts' => true
        ])
        */

        $pdf = \PDF::loadView('vistasreportes/ventas/hoja', compact('listadoventas'),compact('tienda')  )
            ->setPaper('a4', 'landscape')->setWarnings(false);

        return $pdf->download('ReporteVentas '. $fechaactual .'.pdf');

    }
    
        //Vista Excel
    public function reporteventasexcel() {
    
        $fechaactual = Carbon::now()->format('d-m-Y');      

        return (new Ventas)->download('ReporteVentas '. $fechaactual .'.xlsx');

    }




                                //REPORTES ESPECIFICOS



        //REPORTE EXISTENCIAS INVENTARIO

        //Función reporte
    public function reporteexistenciasinventario() {

        $existencias = DB::table('inventario')
        ->join('producto', 'inventario.codigo', '=', 'producto.codigo')
        ->select('inventario.codigo as Codigo', 'producto.descripcion as DescripcionProducto',
                'inventario.cantidad as Cantidad')
                ->orderBy('inventario.idInventario','ASC')
                ->get();

        if( count( $existencias ) == 0 ) {

            Session::flash('danger', ' Aún no hay registros para este reporte ');

        }

        return view('vistasreportes/existenciasinventario/reporteexistenciasinventario') ->with('existencias', $existencias );

    }

        //Vista a pdf
    public function generarreporteexistenciasinventario() {
    
        $fechaactual = Carbon::now()->format('d-m-Y');   

        $existencias = DB::table('inventario')
        ->join('producto', 'inventario.codigo', '=', 'producto.codigo')
        ->select('inventario.codigo as Codigo', 'producto.descripcion as DescripcionProducto',
                'inventario.cantidad as Cantidad')
                ->orderBy('inventario.idInventario','ASC')
                ->get();

        $tienda = Tienda_Modelo::select('*')->take(1)->first();

        /*
        $pdf = \PDF::setOptions([
            'images' => true,
            'enable-javascript' => true,
            'javascript-delay' => 5000,
            'enable-smart-shrinking' => true,
            'no-stop-slow-scripts' => true
        ])
        */

        $pdf = \PDF::loadView('vistasreportes/existenciasinventario/hoja', compact('existencias'),compact('tienda') )
        ->setPaper('a4', 'landscape')->setWarnings(false);

        return $pdf->download('ReporteExistenciasInventario '. $fechaactual .'.pdf');

    }


                //REPORTE ENTRADAS

        //Función reporte
    public function reporteentradasinventario() {

        $movimientos = DB::table('movimientoinventario')
        ->join('empleado', 'movimientoinventario.idempleado', '=', 'empleado.idempleado')
        ->join('producto', 'movimientoinventario.codigo', '=', 'producto.codigo')
        ->join('tipomovimiento', 'movimientoinventario.idtipomovimiento', '=', 'tipomovimiento.idtipomovimiento')
        ->select('movimientoinventario.descripcion as DescripcionMovimiento',
                DB::raw( "CONCAT( empleado.nombre ) as NombreCompleto" ),
                'movimientoinventario.codigo as Codigo', 'producto.descripcion as DescripcionProducto',
                'movimientoinventario.fechahora as FechaHora','movimientoinventario.cantidadant as CantidadAnt',
                'movimientoinventario.cantidadact as CantidadAct', 'movimientoinventario.cantidadmov as CantidadMov')
                ->where('tipomovimiento.nombre', '=', 'Entrada')
                ->orderBy('movimientoinventario.idMovimientoInventario','ASC')
                ->get();


        if( count( $movimientos ) == 0 ) {

            Session::flash('danger', ' Aún no hay registros para este reporte ');

        }

        return view('vistasreportes/entradas/reporteentradasinventario') ->with('movimientos', $movimientos);

    }

        //Vista a pdf
    public function generarreporteentradasinventario() {
    
        $fechaactual = Carbon::now()->format('d-m-Y');   

        $movimientos = DB::table('movimientoinventario')
        ->join('empleado', 'movimientoinventario.idempleado', '=', 'empleado.idempleado')
        ->join('producto', 'movimientoinventario.codigo', '=', 'producto.codigo')
        ->join('tipomovimiento', 'movimientoinventario.idtipomovimiento', '=', 'tipomovimiento.idtipomovimiento')
        ->select('movimientoinventario.descripcion as DescripcionMovimiento',
                DB::raw( "CONCAT( empleado.nombre ) as NombreCompleto" ),
                'movimientoinventario.codigo as Codigo', 'producto.descripcion as DescripcionProducto',
                'movimientoinventario.fechahora as FechaHora','movimientoinventario.cantidadant as CantidadAnt',
                'movimientoinventario.cantidadact as CantidadAct', 'movimientoinventario.cantidadmov as CantidadMov')
                ->where('tipomovimiento.nombre', '=', 'Entrada')
                ->orderBy('movimientoinventario.idMovimientoInventario','ASC')
                ->get();

        $tienda = Tienda_Modelo::select('*')->take(1)->first();

        /*
        $pdf = \PDF::setOptions([
            'images' => true,
            'enable-javascript' => true,
            'javascript-delay' => 5000,
            'enable-smart-shrinking' => true,
            'no-stop-slow-scripts' => true
        ])
        */

        $pdf = \PDF::loadView('vistasreportes/entradas/hoja', compact('movimientos'),compact('tienda'))
            ->setPaper('a4', 'landscape')->setWarnings(false);

        return $pdf->download('ReporteEntradasInventario ' . $fechaactual .'.pdf');

    }

                //REPORTE SALIDAS

        //Función reporte
    public function reportesalidasinventario() {

        $movimientos = DB::table('movimientoinventario')
        ->join('empleado', 'movimientoinventario.idempleado', '=', 'empleado.idempleado')
        ->join('producto', 'movimientoinventario.codigo', '=', 'producto.codigo')
        ->join('tipomovimiento', 'movimientoinventario.idtipomovimiento', '=', 'tipomovimiento.idtipomovimiento')
        ->select('movimientoinventario.descripcion as DescripcionMovimiento',
                DB::raw( "CONCAT( empleado.nombre ) as NombreCompleto" ),
                'movimientoinventario.codigo as Codigo', 'producto.descripcion as DescripcionProducto',
                'movimientoinventario.fechahora as FechaHora','movimientoinventario.cantidadant as CantidadAnt',
                'movimientoinventario.cantidadact as CantidadAct', 'movimientoinventario.cantidadmov as CantidadMov')
                ->where('tipomovimiento.nombre', '=', 'Salida')
                ->orderBy('movimientoinventario.idMovimientoInventario','ASC')
                ->get();


        if( count( $movimientos ) == 0 ) {

            Session::flash('danger', ' Aún no hay registros para este reporte ');

        }

        return view('vistasreportes/salidas/reportesalidasinventario') ->with('movimientos', $movimientos);

    }

        //Vista a pdf
    public function generarreportesalidasinventario() {
    
        $fechaactual = Carbon::now()->format('d-m-Y');   

        $movimientos = DB::table('movimientoinventario')
        ->join('empleado', 'movimientoinventario.idempleado', '=', 'empleado.idempleado')
        ->join('producto', 'movimientoinventario.codigo', '=', 'producto.codigo')
        ->join('tipomovimiento', 'movimientoinventario.idtipomovimiento', '=', 'tipomovimiento.idtipomovimiento')
        ->select('movimientoinventario.descripcion as DescripcionMovimiento',
                DB::raw( "CONCAT( empleado.nombre ) as NombreCompleto" ),
                'movimientoinventario.codigo as Codigo', 'producto.descripcion as DescripcionProducto',
                'movimientoinventario.fechahora as FechaHora','movimientoinventario.cantidadant as CantidadAnt',
                'movimientoinventario.cantidadact as CantidadAct', 'movimientoinventario.cantidadmov as CantidadMov')
                ->where('tipomovimiento.nombre', '=', 'Salida')
                ->orderBy('movimientoinventario.idMovimientoInventario','ASC')
                ->get();

        $tienda = Tienda_Modelo::select('*')->take(1)->first();

        /*
        $pdf = \PDF::setOptions([
            'images' => true,
            'enable-javascript' => true,
            'javascript-delay' => 5000,
            'enable-smart-shrinking' => true,
            'no-stop-slow-scripts' => true
        ])
        */

        $pdf = \PDF::loadView('vistasreportes/salidas/hoja', compact('movimientos'),compact('tienda') )
            ->setPaper('a4', 'landscape')->setWarnings(false);

        return $pdf->download('ReporteSalidasInventario '. $fechaactual .'.pdf');

    }



             //REPORTE PRODUCTOS VENDIDOS

        //Función reporte
    public function reporteproductosvendidos() {

        /*
        $productos = DB::table('detalleventa')
                ->join('producto', 'detalleventa.idProducto', '=', 'producto.codigo')
                ->select('detalleventa.idDetalleventa as detalle',
                    'detalleventa.idventa as idventa', 'detalleventa.cantidad as cantidadvendida',
                    'producto.codigo as codigoproducto',
                    'producto.descripcion as DescripcionProducto', 'producto.precioventa as precioventa')
                ->orderBy('detalleventa.idDetalleventa','ASC')
                ->get();
        */

        $productos = DB::table('movimientoinventario')
                    ->join( 'producto', 'movimientoinventario.codigo', '=', 'producto.codigo' )
                    ->select('movimientoinventario.idmovimientoinventario as detalle',
                        'movimientoinventario.idmovimientoinventario as idventa', 'movimientoinventario.cantidadmov as cantidadvendida',
                        'producto.codigo as codigoproducto',
                        'producto.descripcion as DescripcionProducto', 'movimientoinventario.precioventaact as precioventa')
                    ->where( 'movimientoinventario.idtipomovimiento', '=', 203 )
                    ->orderBy('movimientoinventario.idmovimientoinventario')
                    ->get();

        if( count( $productos ) == 0 ) {

            Session::flash('danger', ' Aún no hay registros para este reporte ');

        }

        return view('vistasreportes/productosvendidos/reporteproductosvendidos') ->with('productos', $productos);

    }

        //Vista a pdf
    public function generarreporteproductosvendidos() {
    
        $fechaactual = Carbon::now()->format('d-m-Y');   

        /*
        $productos = DB::table('detalleventa')
                ->join('producto', 'detalleventa.idProducto', '=', 'producto.codigo')
                ->select('detalleventa.idDetalleventa as detalle',
                    'detalleventa.idventa as idventa', 'detalleventa.cantidad as cantidadvendida',
                    'producto.codigo as codigoproducto',
                    'producto.descripcion as DescripcionProducto', 'producto.precioventa as precioventa')
                ->orderBy('detalleventa.idDetalleventa','ASC')
                ->get();
        */
        

        $productos = DB::table('movimientoinventario')
                    ->join( 'producto', 'movimientoinventario.codigo', '=', 'producto.codigo' )
                    ->select('movimientoinventario.idmovimientoinventario as detalle',
                        'movimientoinventario.idmovimientoinventario as idventa', 'movimientoinventario.cantidadmov as cantidadvendida',
                        'producto.codigo as codigoproducto',
                        'producto.descripcion as DescripcionProducto', 'movimientoinventario.precioventaact as precioventa')
                    ->where( 'movimientoinventario.idtipomovimiento', '=', 203 )
                    ->orderBy('movimientoinventario.idmovimientoinventario')
                    ->get();

        $tienda = Tienda_Modelo::select('*')->take(1)->first();

        /*
        $pdf = \PDF::setOptions([
            'images' => true,
            'enable-javascript' => true,
            'javascript-delay' => 5000,
            'enable-smart-shrinking' => true,
            'no-stop-slow-scripts' => true
        ])
        */

        $pdf = \PDF::loadView('vistasreportes/productosvendidos/hoja', compact('productos'), compact('tienda')  )
            ->setPaper('a4', 'landscape')->setWarnings(false);

        return $pdf->download('ReporteProductosVendidos '. $fechaactual .'.pdf');

    }
    


                //REPORTE PRODUCTOS BAJOS

        //Función reporte
    public function reporteproductosbajos() {


        $lista = DB::table('inventario')
            ->join('producto' , 'inventario.codigo' , '=' ,'producto.codigo')
            ->join('marca', 'producto.idmarca', '=', 'marca.idmarca')
            ->join('categoria', 'producto.idcategoria', '=', 'categoria.idcategoria')
            ->join('presentacion', 'producto.idpresentacion', '=', 'presentacion.idpresentacion')
            ->select( 'marca.descripcion as nombremarca','inventario.idInventario' , 'categoria.nombre as nombrecategoria'
            ,'inventario.codigo','inventario.Stockminimo', 'producto.codigo as codigoproducto', 'producto.descripcion as nombre',
            'producto.Stockminimo as stock' , 'presentacion.medida as presentacion',
            'inventario.Cantidad')
            ->where("inventario.Cantidad", "<=" , \DB::raw('inventario.Stockminimo'))
            ->orderBy('producto.codigo','ASC')
            ->get();

        if( count( $lista ) == 0 ) {

            Session::flash('danger', ' Aún no hay registros para este reporte ');

        }

        return view('vistasreportes/productosbajos/reporteproductos') ->with('lista', $lista);

    }

        //Vista a pdf
    public function generarreporteproductosbajos() {
    
        $fechaactual = Carbon::now()->format('d-m-Y');   

        $tienda = Tienda_Modelo::select('*')->take(1)->first();

        $lista = DB::table('inventario')
            ->join('producto' , 'inventario.codigo' , '=' ,'producto.codigo')
            ->join('marca', 'producto.idmarca', '=', 'marca.idmarca')
            ->join('categoria', 'producto.idcategoria', '=', 'categoria.idcategoria')
            ->join('presentacion', 'producto.idpresentacion', '=', 'presentacion.idpresentacion')
            ->select( 'marca.descripcion as nombremarca','inventario.idInventario' , 'categoria.nombre as nombrecategoria'
            ,'inventario.codigo','inventario.Stockminimo', 'producto.codigo as codigoproducto', 'producto.descripcion as nombre',
            'producto.Stockminimo as stock' , 'presentacion.medida as presentacion',
            'inventario.Cantidad')
            ->where("inventario.Cantidad", "<=" , \DB::raw('inventario.Stockminimo'))
            ->orderBy('producto.codigo','ASC')
            ->get();

        /*
        $pdf = \PDF::setOptions([
            'images' => true,
            'enable-javascript' => true,
            'javascript-delay' => 5000,
            'enable-smart-shrinking' => true,
            'no-stop-slow-scripts' => true
        ])
        */

        $pdf = \PDF::loadView('vistasreportes/productosbajos/hoja', compact('lista'),compact('tienda') )
            ->setPaper('a4', 'landscape')->setWarnings(false);

        return $pdf->download('ReporteProductosBajos '. $fechaactual .'.pdf');

    }

  

                //REPORTE CREDITOS CLIENTES

        //Función reporte
    public function reportecreditosclientes() {

        $creditos = DB::table('creditocliente')
                ->join('cliente', 'creditocliente.idcliente', '=', 'cliente.idcliente')
                ->select(DB::raw( "CONCAT( cliente.nombre, ' ', cliente.apellidopat, ' ', cliente.apellidomat  ) as NombreCompleto" ),
                    'cliente.telefono as TelefonoCliente', 'creditocliente.limite as Limite', 'creditocliente.CanAdeudada as CanAdeudada', 'creditocliente.estado as Estado' )
                ->orderBy('creditocliente.idcliente','ASC')
                //->where('cliente.estado', '=', '1')
                ->get();

        if( count( $creditos ) == 0 ) {

            Session::flash('danger', ' Aún no hay registros para este reporte ');

        }

        return view('vistasreportes/creditosclientes/reportecreditosclientes') ->with('creditos', $creditos );

    }

        //Vista a pdf
    public function generarreportecreditosclientes() {
    
        $fechaactual = Carbon::now()->format('d-m-Y');   

        $tienda = Tienda_Modelo::select('*')->take(1)->first();        

        $creditos = DB::table('creditocliente')
                ->join('cliente', 'creditocliente.idcliente', '=', 'cliente.idcliente')
                ->select(DB::raw( "CONCAT( cliente.nombre, ' ', cliente.apellidopat, ' ', cliente.apellidomat  ) as NombreCompleto" ),
                    'cliente.telefono as TelefonoCliente', 'creditocliente.limite as Limite', 'creditocliente.CanAdeudada as CanAdeudada', 'creditocliente.estado as Estado' )
                ->orderBy('creditocliente.idcliente','ASC')
                //->where('cliente.estado', '=', '1')
                ->get();

        /*
        $pdf = \PDF::setOptions([
            'images' => true,
            'enable-javascript' => true,
            'javascript-delay' => 5000,
            'enable-smart-shrinking' => true,
            'no-stop-slow-scripts' => true
        ])
        */

        $pdf = \PDF::loadView('vistasreportes/creditosclientes/hoja', compact('creditos'),compact('tienda')  )
        ->setPaper('a4', 'landscape')->setWarnings(false);

        return $pdf->download('ReporteCreditosClientes '. $fechaactual .'.pdf');

    }

   

                //REPORTE MOVIMIENTOS PROVEEDORES

        //Función reporte
    public function reporteproveedoresmovimientos() {

        $detallepedidos = DB::table('detallepedido')
        ->join('producto', 'detallepedido.codigo', '=', 'producto.codigo')
        ->join('pedido', 'detallepedido.idPedido', '=', 'pedido.idPedido')
        ->join('proveedor', 'pedido.idProveedor', '=', 'proveedor.idProveedor')
        ->join('movimientoinventario', 'detallepedido.idMovimientoInventario', '=', 'movimientoinventario.idMovimientoInventario')
        ->join('tipomovimiento', 'movimientoinventario.idTipoMovimiento', '=', 'tipomovimiento.idTipoMovimiento')
        ->select('detallepedido.idDetallePedido as idDetalle', 'detallepedido.idPedido as idMov',
            'detallepedido.Codigo as CodigoProducto', 'producto.Descripcion as DescripcionProducto',
            'detallepedido.Cantidad as CantidadMov', 'pedido.Fecha as FechaMov', 'tipomovimiento.Nombre as TipoMov',
            'proveedor.idProveedor as idProveedor',
            DB::raw( "CONCAT( proveedor.nombre, ' ', proveedor.apellidopat, ' ', proveedor.apellidomat  ) as NombreCompleto" ) )
        ->where('proveedor.estado', '=', '1')
        ->get();

        $detalledevoluciones = DB::table('detalledevoluciones')
        ->join('producto', 'detalledevoluciones.codigo', '=', 'producto.codigo')
        ->join('devoluciones', 'detalledevoluciones.idDevoluciones', '=', 'devoluciones.idDevoluciones')
        ->join('proveedor', 'devoluciones.idProveedor', '=', 'proveedor.idProveedor')
        ->join('movimientoinventario', 'detalledevoluciones.idMovimientoInventario', '=', 'movimientoinventario.idMovimientoInventario')
        ->join('tipomovimiento', 'movimientoinventario.idTipoMovimiento', '=', 'tipomovimiento.idTipoMovimiento')
        ->select('detalledevoluciones.idDetalleDevoluciones as idDetalle', 'detalledevoluciones.idDevoluciones as idMov',
            'detalledevoluciones.Codigo as CodigoProducto', 'producto.Descripcion as DescripcionProducto',
            'detalledevoluciones.Cantidad as CantidadMov', 'devoluciones.Fecha as FechaMov', 'tipomovimiento.Nombre as TipoMov',
            'proveedor.idProveedor as idProveedor',
            DB::raw( "CONCAT( proveedor.nombre, ' ', proveedor.apellidopat, ' ', proveedor.apellidomat  ) as NombreCompleto" ) )
        ->where('proveedor.estado', '=', '1')
        ->get();

        $detalles = $detallepedidos->merge( $detalledevoluciones )->sortBy('idDetalle');

        if( count( $detalles ) == 0 ) {

            Session::flash('danger', ' Aún no hay registros para este reporte ');

        }

        return view('vistasreportes/proveedoresmovimientos/reporteproveedoresmovimientos') ->with('detalles', $detalles );

    }

        //Vista a pdf
    public function generarreporteproveedoresmovimientos() {
    
        $fechaactual = Carbon::now()->format('d-m-Y');   

        $tienda = Tienda_Modelo::select('*')->take(1)->first();

        $detallepedidos = DB::table('detallepedido')
        ->join('producto', 'detallepedido.codigo', '=', 'producto.codigo')
        ->join('pedido', 'detallepedido.idPedido', '=', 'pedido.idPedido')
        ->join('proveedor', 'pedido.idProveedor', '=', 'proveedor.idProveedor')
        ->join('movimientoinventario', 'detallepedido.idMovimientoInventario', '=', 'movimientoinventario.idMovimientoInventario')
        ->join('tipomovimiento', 'movimientoinventario.idTipoMovimiento', '=', 'tipomovimiento.idTipoMovimiento')
        ->select('detallepedido.idDetallePedido as idDetalle', 'detallepedido.idPedido as idMov',
        'detallepedido.Codigo as CodigoProducto', 'producto.Descripcion as DescripcionProducto',
        'detallepedido.Cantidad as CantidadMov', 'pedido.Fecha as FechaMov', 'tipomovimiento.Nombre as TipoMov',
        'proveedor.idProveedor as idProveedor',
        DB::raw( "CONCAT( proveedor.nombre, ' ', proveedor.apellidopat, ' ', proveedor.apellidomat  ) as NombreCompleto" ) )
        ->where('proveedor.estado', '=', '1')
        ->get();

        $detalledevoluciones = DB::table('detalledevoluciones')
        ->join('producto', 'detalledevoluciones.codigo', '=', 'producto.codigo')
        ->join('devoluciones', 'detalledevoluciones.idDevoluciones', '=', 'devoluciones.idDevoluciones')
        ->join('proveedor', 'devoluciones.idProveedor', '=', 'proveedor.idProveedor')
        ->join('movimientoinventario', 'detalledevoluciones.idMovimientoInventario', '=', 'movimientoinventario.idMovimientoInventario')
        ->join('tipomovimiento', 'movimientoinventario.idTipoMovimiento', '=', 'tipomovimiento.idTipoMovimiento')
        ->select('detalledevoluciones.idDetalleDevoluciones as idDetalle', 'detalledevoluciones.idDevoluciones as idMov',
        'detalledevoluciones.Codigo as CodigoProducto', 'producto.Descripcion as DescripcionProducto',
        'detalledevoluciones.Cantidad as CantidadMov', 'devoluciones.Fecha as FechaMov', 'tipomovimiento.Nombre as TipoMov',
        'proveedor.idProveedor as idProveedor',
        DB::raw( "CONCAT( proveedor.nombre, ' ', proveedor.apellidopat, ' ', proveedor.apellidomat  ) as NombreCompleto" ) )
        ->where('proveedor.estado', '=', '1')
        ->get();

        $detalles = $detallepedidos->merge( $detalledevoluciones )->sortBy('idDetalle');

        /*
        $pdf = \PDF::setOptions([
            'images' => true,
            'enable-javascript' => true,
            'javascript-delay' => 5000,
            'enable-smart-shrinking' => true,
            'no-stop-slow-scripts' => true
        ])

        ->loadView('vistasreportes/proveedoresmovimientos/hoja', compact('detalles') ,compact('tienda') )
        ->setPaper('a4', 'landscape')->setWarnings(false);
        */

        $pdf = \PDF::loadView('vistasreportes/proveedoresmovimientos/hoja', compact('detalles') ,compact('tienda') )
        ->setPaper('a4', 'landscape')->setWarnings(false);

        return $pdf->download('ReporteMovimientosProveedores '. $fechaactual .'.pdf');

    }

}
