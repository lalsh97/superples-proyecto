<?php

namespace App\Http\Controllers\ProyectoControllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Session;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;

use App\Models\Empleado_Modelo;
use App\Models\Caja_Modelo;
use App\User;

class EmpleadosController extends Controller
{


    public function listadoempleados() {

        $listadoempleados = DB::table('empleado')
        ->select('idempleado','idTienda',
        DB::raw( "CONCAT( nombre,' ', apellidopat,' ', apellidomat ) AS nombrecompleto" ),
                    'rfc', 'fechanac', 'sexo', 'curp', 'calle', 'codigop', 'numero', 'colonia', 'municipio', 'ciudad',
                    'telefono', 'correo','foto', 'estado' )
                ->where( 'estado', 1 )                
                ->where( 'idempleado', '<>', 1 )
                ->orderBy( 'idempleado', 'ASC' )
                ->get();
              //->paginate(5);

        return view('vistasempleados/listadoempleados')->with('registroempleados', $listadoempleados);

    }


    public function busquedaempleado( $operacion ) {

        return view('vistasempleados/busquedaempleado')->with( 'operacion' , $operacion );
    }

    public function buscarempleado() {

        return view('vistasempleados/buscarempleado');

    }
    

        //Funciones para registrar

    public function registrarempleados() {

        return view('vistasempleados/registrarempleado');

    }

    public function insertarempleado( Request $request ) {

        if( $request->botonformempleado == 'Registrar' ) {

            $validator = Validator::make($request->all(), Empleado_Modelo::rules(), Empleado_Modelo::messages());

            if( $validator ->fails() ) {
                return redirect()->back()->withInput()->withErrors($validator->errors());
            }

            if( $request->hasFile('avatar') ) {

                $file = $request->file( 'avatar' );
                $nameusuario = str_replace( "/", " ", $request -> input('name') );

                $nameavatar = 'Usuarios/' . $nameusuario . "." . $request->file('avatar')->extension();
                $file->move( public_path().'/images/Usuarios/', $nameusuario . "." . $request->file('avatar')->extension() );

            } else {

                $nameavatar = 'Usuarios/AvatarUserDefault.png';

            }


            $idtienda = 1;
            $nombre =       $request -> input('nombre');
            $apellidopat =  $request -> input('apellidopat');
            $apellidomat =  $request -> input('apellidomat');
            $sexo =         $request-> input('sexo');
            $fechanac =     $request-> input('fechanac');
            $curp =         $request-> input('curp');
            $rfc =          $request-> input('rfc');
            $telefono =     $request-> input('telefono');
            $correo =       $request-> input('email');

            $calle =        $request -> input('calle');
            $numero =       $request -> input('numero');
            $codigop =      $request -> input('codigop');
            $colonia =      $request -> input('colonia');
            $municipio =    $request -> input('municipio');
            $ciudad =       $request-> input('ciudad');
            $estado = 1;

            $name = $request-> input('name');
            $email = $request-> input('email');
            $password = $request-> input('password');
            $nivel = 2;

            Empleado_Modelo::create( ['idtienda' => $idtienda, 'nombre' => $nombre,
            'apellidopat' => $apellidopat, 'apellidomat' => $apellidomat, 'sexo' => $sexo,
            'fechanac' => $fechanac, 'curp' => $curp, 'rfc' => $rfc, 'telefono' => $telefono,
            'correo' => $correo, 'calle' => $calle, 'numero' => $numero, 'codigop' => $codigop,
            'colonia' => $colonia, 'municipio' => $municipio, 'ciudad' => $ciudad,
             'estado' => $estado] );

            $consulta = Empleado_Modelo::select('idEmpleado')
            ->orderBy('idEmpleado', 'desc')
            ->limit(1)
            ->get();

            foreach( $consulta as $k ) {
                $idEmpleado = $k->{'idEmpleado'};
            }

            $fechaactual = Carbon::now();

            Caja_Modelo::create( [
                'idempleado' => $idEmpleado,
                'fecha' => $fechaactual
            ]);

            User::create([
                'name' => $name,
                'email' => $email,
                'password' => Hash::make($password),
                'idempleado' => $idEmpleado,
                'nivel' => $nivel,
                'avatar' => $nameavatar,
            ]);

            Session::flash('success', ' Se ha registrado exitosamente al empleado ');

            return redirect()->back();

        } else if ( $request ->botonformempleado == 'Cancelar' ) {
            return redirect()->to('home');
        }

    }

        //Funciones para editar información

    public function modificarempleado( $idempleado ) {

        $empleado = Empleado_Modelo::select('*')->where('estado', 1)->where('idempleado', $idempleado)->take(1)->first();

        return view('vistasempleados/modificarempleado')->with('empleado',$empleado);

    }

    public function actualizarinfoempleado( Request $request , $idempleado ) {

        if( $request->botonformmodificar == 'Modificar' ) {

                DB::table('empleado')
                ->where('idempleado',$idempleado)  // find your user by their email
                ->limit(1)  // optional - to ensure only one record is updated.
                ->update( array( 'telefono' => $request->telefono,
                                 'correo' => $request->correo,
                                 'calle' => $request->calle,
                                 'numero' => $request->numero,
                                 'rfc' => $request->rfc,
                                 'codigop' => $request->codigop,
                                 'colonia' => $request->colonia,
                                 'municipio' => $request->municipio,
                                 'ciudad' => $request->ciudad) );  // update the record in the DB.

                return redirect()->to('listadoempleados');

        }  else if( $request->botonformmodificar == 'Cancelar' ) {

            return redirect()->to('home');

        }

    }

        //Funciones para descontinuar empleados

    public function descontinuarempleado( $idempleado ) {

        $empleado = Empleado_Modelo::select('*')->where('estado', 1)->where('idempleado', $idempleado)->take(1)->first();

        return view('vistasempleados/descontinuarempleado')->with( 'empleado', $empleado);

    }

    public function cambiarestadoempleado( Request $request, $idempleado ) {

        if( $request->botonformdescontinuar == 'Descontinuar' ) {

                DB::table('empleado')
                ->where('idempleado',$idempleado)  // find your user by their email
                ->update( array( 'Estado' => 0 ) );  // update the record in the DB.

                return redirect()->to('listadoempleados');

        } else if ( $request->botonformdescontinuar == 'Cancelar' ) {

            return redirect()->to('home');

        }

    }

    public function eliminarcambiarestadoempleado( $idempleado ) {
        
            DB::table('empleado')
            ->where( 'idempleado', $idempleado )
            ->update( array( 'Estado' => 0 ) );

            return redirect()->to('listadoempleados');

    }

}
