<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {                    

        //COMENTAR CUANDO SE HAGA PUSH A HEROKU
        DB::statement( "SET @@global.event_scheduler = 'ON';" );

        /*
        $listaalertasinventario = DB::table('inventario')
            ->join('producto' , 'inventario.codigo' , '=' ,'producto.codigo')
            ->join('marca', 'producto.idmarca', '=', 'marca.idmarca')
            ->join('presentacion', 'producto.idpresentacion', '=', 'presentacion.idpresentacion')
            ->select( 'marca.descripcion as nombremarca',
                        'inventario.idInventario as idinventario',
                        'inventario.codigo as codigoinventario',
                        'inventario.Stockminimo as stockminimoinventario',
                        'producto.Descripcion as descripcionproducto',
                        'producto.Stockminimo as stockminimoproducto',
                        'presentacion.medida as presentacionproducto',
                        'inventario.Cantidad as cantidadproducto')
            ->where("inventario.Cantidad", "<=" , \DB::raw('inventario.Stockminimo'))
            ->orderBy('inventario.Cantidad','ASC')
            ->get();
            */

            $idempleado = Auth::User()->idEmpleado;

            $fecha =  Carbon::today();

            $consultaidcaja = DB::table('caja')->select('idcaja as idcaja')
                                                ->where('idempleado', $idempleado )
                                                ->get();
    
            $idcaja = $consultaidcaja[0]->{'idcaja'};

            /*
            $consultainiciosesion = DB::table('empleadocaja')
                ->select( DB::raw( "COUNT(*) as totalregistro" ) )
                ->where('idempleado', '=', $idempleado )
                ->where('idcaja', '=', $idcaja )
                ->where('fecha', '=', $fecha )
                ->where('estado', '=', 1 )
                ->get();
            */

            $consultainiciosesion = DB::table('empleadocaja')
                ->select( DB::raw( "COUNT(*) as totalregistro" ) )
                ->where('idempleado', '=', $idempleado )
                ->where('idcaja', '=', $idcaja )
                ->where('fecha', '=', $fecha )
                ->where('estado', '=', 0 )
                ->get();
          
            $banderainiciosesion = $consultainiciosesion[0]->{'totalregistro'};
            
            //$banderainiciosesion = 0;

        //return view('home')->with( 'listaalertasinventario' , $listaalertasinventario )->with( 'banderainiciosesion', $banderainiciosesion );
        return view('home')->with( 'banderainiciosesion', $banderainiciosesion );

        
    }
    
}
