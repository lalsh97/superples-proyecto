<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DetallePedido_Modelo extends Model
{

    protected $table = 'detallepedido';

    protected $primarykey = 'iddetallepedido';
    
    public $timestamps = false;

    protected $fillable = [
        'iddetallepedido', 'idpedido', 'idmovimientoinventario','codigo','cantidad'
    ];

}
