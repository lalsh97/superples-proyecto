<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
class Descuento_Modelo extends Model
{
  protected $table = 'descuento';

  protected $primarykey = 'idDescuento';

  public $timestamps = false;

  protected $fillable = [
  'idDescuento' , 'idpromociones','codigo', 'descuento',
    'precionuevo'
  ];

  public static function rules() {
      return [

          'idPromociones' => 'unique:producto'
          /*
          'codigo'            => 'required|unique:producto|size:2|min:1',
          'idmarca'           => 'required',
          'idpresentacion'    => 'required',
          'idproveedor'       => 'required',
          'idcategoria'       => 'required',
          'preciocosto'       => 'required|min:1',
          'precioventa'       => 'required|min:1',
          'impuesto'          => 'required',
          */
      ];
  }

  public static function messages() {
      return [

         'idPromociones.unique' => 'Ya existe una promocion con ese id'

      ];

  }
}
