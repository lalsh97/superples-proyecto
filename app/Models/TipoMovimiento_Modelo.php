<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TipoMovimiento_Modelo extends Model
{
    protected $table = 'tipomovimiento';

    protected $primarykey = 'idtipomovimiento';
    
    public $timestamps = false;

    protected $fillable = [
        'idtipomovimiento', 'nombre','descripcion'
    ];
}
