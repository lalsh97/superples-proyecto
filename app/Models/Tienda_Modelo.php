<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tienda_Modelo extends Model
{
    protected $table = 'tienda';

    protected $primarykey = 'idTienda';

    public $timestamps = false;

    protected $fillable = [
        'idtienda', 'nombre','calle','numero','codigop','colonia'
        ,'municipio','ciudad','telefono','rfc','correo'
    ];
}
