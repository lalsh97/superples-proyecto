<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;


class Producto_Modelo extends Model
{
    protected $table = 'producto';

    protected $primarykey = 'Codigo';

    public $timestamps = false;

    protected $fillable = [
        'codigo', 'idmarca', 'idpresentacion', 'idproveedor', 'idcategoria', 'descripcion', 'preciocosto',
        'precioventa', 'iva', 'imagen', 'estado', 'stockminimo'
    ];
    
    protected $casts = [
        'codigo' => 'bigint',
    ];

    public static function rules() {
        return [

            'codigo' => 'unique:producto'
            /*
            'codigo'            => 'required|unique:producto|size:2|min:1',
            'idmarca'           => 'required',
            'idpresentacion'    => 'required',
            'idproveedor'       => 'required',
            'idcategoria'       => 'required',
            'preciocosto'       => 'required|min:1',
            'precioventa'       => 'required|min:1',
            'impuesto'          => 'required',
            */
        ];
    }

    public static function messages() {
        return [

           'codigo.unique' => 'Ya existe un producto registrado con ese código de barras'
            /*
            'codigo.required' => 'El campo del código de barras es necesario.',
            'codigo.unique' => 'Ya existe un producto registrado con ese código de barras',
            'codigo.size' => 'El número de digitos del código de barras debe ser de 12 digitos',
            'codigo.min' => 'El valor del código de barra debe ser válido',

            'idmarca.required' => 'El campo de la marca es necesario',

            'idpresentacion.required' => 'El campo de la presentación es necesario',

            'idproveedor.required' => 'El campo del proveedor es necesario',

            'idcategoria.required' => 'El campo de la categoria es necesario',

            'preciocosto.required' => 'El campo del precio de costo es necesario',
            'preciocosto.min' => 'El valor del precio de costo debe ser válido',

            'precioventa.required' => 'El campo del precio de venta es necesario',
            'precioventa.min' => 'El valor del precio de venta debe ser válido',

            'impuesto.required' => 'El campo del impuesto es necesario',
            */
        ];

    }

}
