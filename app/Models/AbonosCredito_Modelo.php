<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AbonosCredito_Modelo extends Model
{
    protected $table = 'abonoscredito';

    protected $primarykey = 'idabonoscredito';
    
    public $timestamps = false;

    protected $fillable = [
        'idabonoscredito', 'idcreditocliente','fecha', 'cantidad', 'idempleado', 'idcaja', 'estado'
    ];
    
}
