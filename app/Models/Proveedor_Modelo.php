<?php

namespace App\Models;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model; //Variable (Model) de la libreria a invocar

class Proveedor_Modelo extends Model //siempre la clase se tiene que llamar como el archivo
{

    protected $table = 'proveedor';

    protected $primarykey = 'idproveedor';

    public $timestamps = false;

    protected $fillable = [
        'idproveedor', 'empresa', 'nombre','apellidopat','apellidomat','calle','numero','codigop','colonia','municipio',
        'ciudad','telefono','rfc','correo','estado'
    ];


    public static function rules() {
        return [

            'rfc' => 'unique:proveedor'
            /*
            'codigo'            => 'required|unique:producto|size:2|min:1',
            'idmarca'           => 'required',
            'idpresentacion'    => 'required',
            'idproveedor'       => 'required',
            'idcategoria'       => 'required',
            'preciocosto'       => 'required|min:1',
            'precioventa'       => 'required|min:1',
            'impuesto'          => 'required',
            */
        ];
    }

    public static function messages() {
        return [

           'rfc.unique' => 'Ya existe un proveedor registrado con ese R.F.C.'
            /*
            'codigo.required' => 'El campo del código de barras es necesario.',
            'codigo.unique' => 'Ya existe un producto registrado con ese código de barras',
            'codigo.size' => 'El número de digitos del código de barras debe ser de 12 digitos',
            'codigo.min' => 'El valor del código de barra debe ser válido',

            'idmarca.required' => 'El campo de la marca es necesario',

            'idpresentacion.required' => 'El campo de la presentación es necesario',

            'idproveedor.required' => 'El campo del proveedor es necesario',

            'idcategoria.required' => 'El campo de la categoria es necesario',

            'preciocosto.required' => 'El campo del precio de costo es necesario',
            'preciocosto.min' => 'El valor del precio de costo debe ser válido',

            'precioventa.required' => 'El campo del precio de venta es necesario',
            'precioventa.min' => 'El valor del precio de venta debe ser válido',

            'impuesto.required' => 'El campo del impuesto es necesario',
            */
        ];

    }
}
