<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Inventario_Modelo extends Model
{
    
    protected $table = 'inventario';

    protected $primarykey = 'idinventario';
    
    public $timestamps = false;

    protected $fillable = [
        'idinventario', 'codigo','cantidad', 'preciocom', 'precioven', 'gananciasxpz', 'gananciastot', 'stockminimo'
    ];

}
