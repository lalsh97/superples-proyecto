<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DetalleVenta_Modelo extends Model
{

    protected $table = 'detalleventa';

    protected $primarykey = 'iddetalleventa';

    public $timestamps = false;

    protected $fillable = [
        'iddetalleventa', 'idventa', 'idproducto', 'cantidad', 'fecha', 'idmovimientoinventario'
        //'iddetalleventa', 'idventa', 'idproducto', 'idtipopago', 'cantidad', 'fecha', 'estado'
    ];

}
