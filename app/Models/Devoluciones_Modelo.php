<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Devoluciones_Modelo extends Model
{

    protected $table = 'devoluciones';

    protected $primarykey = 'iddevoluciones';
    
    public $timestamps = false;

    protected $fillable = [
        'iddevoluciones', 'idproveedor','idempleado','fecha'
    ];
    
}
