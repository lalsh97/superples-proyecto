<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CreditoCliente_Modelo extends Model
{

    protected $table = 'creditocliente';

    protected $primarykey = 'idcreditocliente';

    public $timestamps = false;

    protected $fillable = [
        'idcreditocliente', 'idcliente', 'limite', 'canadeudada', 'estado'
    ];
}
