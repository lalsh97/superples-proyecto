<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Caja_Modelo extends Model
{
    protected $table = 'caja';

    protected $primarykey = 'idcaja';
    
    public $timestamps = false;

    protected $fillable = [
        'idcaja', 'idempleado','fecha'
    ];
}
