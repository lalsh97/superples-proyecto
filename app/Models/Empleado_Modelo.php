<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Empleado_Modelo extends Model
{

    protected $table = 'empleado';

    protected $primarykey = 'idempleado';

    public $timestamps = false;

    protected $fillable = [
        'idempleado', 'idtienda', 'nombre', 'apellidopat', 'apellidomat','calle', 'numero',
        'codigop', 'colonia', 'municipio','ciudad','telefono','rfc','correo','curp','fechanac',
        'sexo','foto','estado' 
    ];

    public static function rules() {

        return [

            'idempleado' => 'unique:empleado',
            'curp' => 'unique:empleado',
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            /*
            'codigo'            => 'required|unique:producto|size:2|min:1',
            'idmarca'           => 'required',
            'idpresentacion'    => 'required',
            'idproveedor'       => 'required',
            'idcategoria'       => 'required',
            'preciocosto'       => 'required|min:1',
            'precioventa'       => 'required|min:1',
            'impuesto'          => 'required',
            */
        ];

    }

    public static function messages() {

        return [

           'idempleado.unique' => 'Ya existe un empleado registrado con ese id',
           'curp.unique' => 'Ya existe un empleado registrado con esa curp',
           'name.required' => 'Nombre de usuario necesario',
           'name.string' => 'El nombre de usuario debe ser texto',
           'name.max' => 'El tamaño del nombre de usuario debe ser menor a 255 carácteres',
           'email.required' => 'La contraseña es necesaria',
           'email.string' => 'La contraseña debe ser texto',
           'email.email' => 'El correo debe tener la estructura de un correo electrónico',
           'email.max' => 'El correo electrónico debe tener máximo 255 carácteres',
           'email.unique' => 'El correo electrónico ya está registrado',
           'password.required' => 'La contraseña es necesaria',
           'password.string' => 'La contraseña debe ser texto',
           'password.min' => 'La contraseña debe tener mínimo 8 carácteres',
           'password.confirmed' => 'La contraseña debe ser confirmada',
            /*
            'codigo.required' => 'El campo del código de barras es necesario.',
            'codigo.unique' => 'Ya existe un producto registrado con ese código de barras',
            'codigo.size' => 'El número de digitos del código de barras debe ser de 12 digitos',
            'codigo.min' => 'El valor del código de barra debe ser válido',

            'idmarca.required' => 'El campo de la marca es necesario',

            'idpresentacion.required' => 'El campo de la presentación es necesario',

            'idproveedor.required' => 'El campo del proveedor es necesario',

            'idcategoria.required' => 'El campo de la categoria es necesario',

            'preciocosto.required' => 'El campo del precio de costo es necesario',
            'preciocosto.min' => 'El valor del precio de costo debe ser válido',

            'precioventa.required' => 'El campo del precio de venta es necesario',
            'precioventa.min' => 'El valor del precio de venta debe ser válido',

            'impuesto.required' => 'El campo del impuesto es necesario',
            */
        ];

    }
    
}
