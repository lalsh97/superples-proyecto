<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MovimientoInventario_Modelo extends Model
{

    protected $table = 'movimientoinventario';

    protected $primarykey = 'idmovimientoinventario';
    
    public $timestamps = false;

    protected $fillable = [
        'idmovimientoinventario', 'idtipomovimiento ', 'idempleado','codigo','fechahora', 'descripcion',
        'cantidadant', 'cantidadact', 'cantidadmov', 'preciocostoant', 'preciocostoact', 'precioventaant',
        'precioventaact'
    ];

}
