<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DetalleDevoluciones_Modelo extends Model
{

    protected $table = 'detalledevoluciones';

    protected $primarykey = 'iddetalledevoluciones';
    
    public $timestamps = false;

    protected $fillable = [
        'iddetalledevoluciones', 'iddevoluciones', 'idmovimientoinventario','codigo','cantidad'
    ];
    
}
