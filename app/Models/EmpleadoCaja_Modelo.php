<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EmpleadoCaja_Modelo extends Model
{
    protected $table = 'empleadocaja';

    protected $primarykey = "idempleadocaja";

    public $timestamps = false;

    protected $fillable =   [ 
                                  'idempleadocaja', 'idcaja', 'idempleado',
                                    'saldoinicial', 'saldofinal', 'fecha', 'estado'
                            ];
}
