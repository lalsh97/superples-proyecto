<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
class Paquetes_Modelo extends Model
{
  protected $table = 'paquetes';

  protected $primarykey = 'idPaquetes';

  public $timestamps = false;

  protected $fillable = [
  'idpaquetes' , 'idPromociones','codigo1', 'precio1',
    'descuento1','codigo2', 'precio2',
      'descuento2'
  ];

  public static function rules() {
      return [

          'idPromociones' => 'unique:producto'
          /*
          'codigo'            => 'required|unique:producto|size:2|min:1',
          'idmarca'           => 'required',
          'idpresentacion'    => 'required',
          'idproveedor'       => 'required',
          'idcategoria'       => 'required',
          'preciocosto'       => 'required|min:1',
          'precioventa'       => 'required|min:1',
          'impuesto'          => 'required',
          */
      ];
  }

  public static function messages() {
      return [

         'idPromociones.unique' => 'Ya existe una promocion con ese id'

      ];

  }
}
