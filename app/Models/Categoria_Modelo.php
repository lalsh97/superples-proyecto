<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Categoria_Modelo extends Model
{
    protected $table = 'categoria';

    protected $primarykey = 'idcategoria';
    
    public $timestamps = false;

    protected $fillable = [
        'idcategoria', 'nombre','descripcion','estado', 'urlimagen'
    ];
}
