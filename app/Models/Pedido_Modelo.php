<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pedido_Modelo extends Model
{
    protected $table = 'pedido';

    protected $primarykey = 'idpedido';
    
    public $timestamps = false;

    protected $fillable = [
        'idpedido', 'idempleado','idproveedor','fecha', 'total', 'estado'
    ];
}
