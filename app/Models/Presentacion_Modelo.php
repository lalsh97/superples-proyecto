<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Presentacion_Modelo extends Model
{
   protected $table = 'presentacion';

   protected $primarykey = 'idPresentacion';

   public $timestamps = false;

   protected $fillable = [
       'idPresentacion', 'medida','estado', 'urlimagen'
   ];
}
