<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Marca_Modelo extends Model
{
    protected $table = 'marca';

    protected $primarykey = 'idmarca';
    
    public $timestamps = false;

    protected $fillable = [
        'idmarca', 'idproveedor','descripcion','estado', 'urlimagen'
    ];
}
