<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

/*
Route::group(['prefix'=>'auth'], function() {
  
    Route::get('/buscarproductoventa/{codigo}','ProyectoControllers\VentasController@buscarproductoventa')->name('buscarproductoventa');
  });
*/



          //VENTAS

Route::get('/buscarproductoventa/{codigo?}','ProyectoControllers\VentasController@buscarproductoventa')->name('buscarproductoventa');
  
Route::get('/cantidadmaximainventarioproducto/{codigo?}','ProyectoControllers\VentasController@cantidadmaximainventarioproducto')->name('cantidadmaximainventarioproducto');

          //API PARA VISTA REGISTRARVENTA, REGISTRARENTRADAINVENTARIO Y REGISTRARSALIDAINVENTARIO
Route::get('/listadoproductosmodal', 'ProyectoControllers\ProductosController@listadoproductosmodal')->name('listadoproductosmodal');

          //API PARA VISTA REGISTROENTRADAPEDIDO Y REGISTROSALIDAPEDIDO
Route::get('/proveedoreslistadoproductosmodal', 'ProyectoControllers\ProductosController@proveedoreslistadoproductosmodal')->name('proveedoreslistadoproductosmodal');

Route::get('/buscarproductocompleto/{codigo?}','ProyectoControllers\ProductosController@buscarproductocompleto')->name('buscarproductocompleto');


          //API PARA REGISTROENTRADAINVENTARIO
Route::get('/buscarproductocompletoentradainventario/{codigo?}','ProyectoControllers\ProductosController@buscarproductocompletoentradainventario')->name('buscarproductocompletoentradainventario');



          //PRODUCTOS
Route::get('/listadoproductosordenadocodigo', 'ProyectoControllers\ProductosController@listadoproductosordenadocodigo')->name('listadoproductosordenadocodigo');
Route::get('/listadoproductosordenadomarca', 'ProyectoControllers\ProductosController@listadoproductosordenadomarca')->name('listadoproductosordenadomarca');
Route::get('/listadoproductosordenadopresentacion', 'ProyectoControllers\ProductosController@listadoproductosordenadopresentacion')->name('listadoproductosordenadopresentacion');
Route::get('/listadoproductosordenadocategoria', 'ProyectoControllers\ProductosController@listadoproductosordenadocategoria')->name('listadoproductosordenadocategoria');
Route::get('/listadoproductosordenadodescripcion', 'ProyectoControllers\ProductosController@listadoproductosordenadodescripcion')->name('listadoproductosordenadodescripcion');



          //CLIENTES

Route::get('/listadoselectclientes', 'ProyectoControllers\ClientesController@listadoselectclientes')->name('listadoselectclientes');

Route::get('/buscarinformacionclientecredito/{idcliente?}', 'ProyectoControllers\ClientesController@buscarinformacionclientecredito')->name('buscarinformacionclientecredito');

Route::get('/filtrarclientespornombre/{nombre?}','ProyectoControllers\VentasController@filtrarclientespornombre')->name('filtrarclientespornombre');

Route::get('/filtrarproveedorespornombre/{nombre?}','ProyectoControllers\ProveedoresController@filtrarproveedorespornombre')->name('filtrarproveedorespornombre');

Route::get('/verificarcreditocliente/{idcliente?}', 'ProyectoControllers\ClientesController@verificarcreditocliente')->name('verificarcreditocliente');



              //PROVEEDOR Verificar si un proveedor tiene productos asociados
Route::get('/verificarproductosproveedor/{idproveedor?}', 'ProyectoControllers\ProveedoresController@verificarproductosproveedor')->name('verificarproductosproveedor');



              //PROMOCIONES Rutas verificar promociones

  //VerificarSiHayPromocionesProducto
Route::get('/verificarpromocionesproducto/{codigo?}', 'ProyectoControllers\ProductosController@verificarpromocionesproducto')->name('verificarpromocionesproducto');

  //Verificar el tipo de Oferta
Route::get('/buscarpromocionoferta/{codigo?}','ProyectoControllers\ProductosController@buscarpromocionoferta')->name('buscarpromocionoferta');

  //Verificar el tipo de Descuento
Route::get('/buscarpromociondescuento/{codigo?}','ProyectoControllers\ProductosController@buscarpromociondescuento')->name('buscarpromociondescuento');

  //Verificar el tipo de Paquetes
Route::get('/buscarpromocionpaquetes/{codigo?}','ProyectoControllers\ProductosController@buscarpromocionpaquetes')->name('buscarpromocionpaquetes');
