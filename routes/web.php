<?php

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('principal');
});

Route::get('/inicio', function () {
    return view('principal');
    //return view('welcome');    
});

Route::get('/salida', 'ProyectoControllers\VentasController@cortediaventassesioncerrada')->name('/salida');
Route::get('/exportarcortecajadiaventascerrada/{idempleado?}', 'ProyectoControllers\VentasController@exportarcortecajadiaventascerrada')->name('exportarcortecajadiaventascerrada');

Auth::routes();

Route::group(['middleware' => 'auth'], function () {
    
    // Las rutas que incluyas aquí pasarán por el middleware 'auth'

    Route::get('/home', 'HomeController@index')->name('home');
    
    Route::get('/modificarinformacionusuario', 'ControladorPrincipal@modificarinformacionusuario')->name('modificarinformacionusuario');
    Route::put('/actualizarinformacionusuario/{idusuario}', 'ControladorPrincipal@actualizarinformacionusuario')->name('actualizarinformacionusuario');


        //Modificación de la información cuentas de los empleados de la tienda
    Route::get('/listadoempleadoscambioinformacionusuario', 'ControladorPrincipal@listadoempleadoscambioinformacionusuario')->name('listadoempleadoscambioinformacionusuario');
    Route::get('/modificarinformacionusuariocuentaempleado/{idempleado?}', 'ControladorPrincipal@modificarinformacionusuariocuentaempleado')->name('modificarinformacionusuariocuentaempleado');

    Route::get('/manualdeusuario', 'ControladorPrincipal@manualdeusuario')->name('manualdeusuario');

                //OPCIONES REACTIVAR
    Route::get('/menuprincipalreactivar', 'ControladorPrincipal@menuprincipalreactivar')->name('menuprincipalreactivar');
                
    //empleados
    Route::get('/listadoempleadosdesactivados', 'ControladorPrincipal@listadoempleadosdesactivados')->name('listadoempleadosdesactivados');
    Route::get('/reactivarempleado/{idempleado}', 'ControladorPrincipal@reactivarempleado')->name('reactivarempleado');
    //productos
    Route::get('/listadoproductosdesactivados', 'ControladorPrincipal@listadoproductosdesactivados')->name('listadoproductosdesactivados');
    Route::get('/reactivarproducto/{codigo}', 'ControladorPrincipal@reactivarproducto')->name('reactivarproducto');
    //proveedores
    Route::get('/listadoproveedoresdesactivados', 'ControladorPrincipal@listadoproveedoresdesactivados')->name('listadoproveedoresdesactivados');
    Route::get('/reactivarproveedor/{idproveedor}', 'ControladorPrincipal@reactivarproveedor')->name('reactivarproveedor');
    //clientes
    Route::get('/listadoclientesdesactivados', 'ControladorPrincipal@listadoclientesdesactivados')->name('listadoclientesdesactivados');
    Route::get('/reactivarcliente/{idcliente}', 'ControladorPrincipal@reactivarcliente')->name('reactivarcliente');





    // Las rutas que incluyas aquí pasarán por el middleware 'auth'


            //INDICE RUTAS
                //PRODUCTOS
                //PROMOCIONES
                //PROVEEDORES
                //CLIENTES
                //EMPLEADOS
                //VENTAS
                //INVENTARIO
                //REPORTES
    









                                            // Rutas de productos

        //Ruta de Productos
    Route::get('Productos','ProyectoControllers\ProductosController@ver_productos');

    //  Registrar
    Route::get('/registrarproductos','ProyectoControllers\ProductosController@registrarproductos')->name('registrarproductos');
    Route::get('/registrarproductosnocodigo','ProyectoControllers\ProductosController@registrarproductosnocodigo')->name('registrarproductosnocodigo');
    Route::post('/insertarproducto', 'ProyectoControllers\ProductosController@insertarproducto')->name('insertarproducto');


    Route::get('/registrarproductosrapido', 'ProyectoControllers\ProductosController@registrarproductosrapido')->name('registrarproductosrapido');
    Route::get('/registrarproductosnocodigorapido', 'ProyectoControllers\ProductosController@registrarproductosnocodigorapido')->name('registrarproductosnocodigorapido');

    //  Modificar
    Route::get('/modificarproducto/{codigo}','ProyectoControllers\ProductosController@modificarproducto')->name('modificarproducto');
    Route::put('/actualizarinfoproducto/{codigo}','ProyectoControllers\ProductosController@actualizarinfoproducto')->name('actualizarinfoproducto');


    //Descontinuar
    Route::get('descontinuarproducto/{codigo}','ProyectoControllers\ProductosController@descontinuarproducto')->name('descontinuarproducto');
    Route::put('cambiarestadoproducto/{codigo}','ProyectoControllers\ProductosController@cambiarestadoproducto')->name('cambiarestadoproducto');
    Route::get('descontinuarcambiarestadoproducto/{codigo}', 'ProyectoControllers\ProductosController@descontinuarcambiarestadoproducto')->name('descontinuarcambiarestadoproducto');

    Route::get('/buscarproducto','ProyectoControllers\ProductosController@buscarproducto')->name('buscarproducto');
    Route::get('/buscarproductocodigo/{codigo}','ProyectoControllers\ProductosController@buscarproductocodigo')->name('buscarproductocodigo');
    Route::get('/buscarproductoproveedores/{idproveedor}','ProyectoControllers\ProductosController@buscarproductoproveedores')->name('buscarproductoproveedores');
    Route::get('/buscarproductocategorias/{idcategoria}','ProyectoControllers\ProductosController@buscarproductocategorias')->name('buscarproductocategorias');

    Route::get('/busquedaproducto/{operacion}','ProyectoControllers\ProductosController@busquedaproducto')->name('busquedaproducto');

    Route::get('/listadoproductos','ProyectoControllers\ProductosController@listadoproductos')->name('listadoproductos');


    Route::post('/añadirmarca','ProyectoControllers\ProductosController@añadirmarca')->name('añadirmarca');
    Route::post('/añadirpresentacion','ProyectoControllers\ProductosController@añadirpresentacion')->name('añadirpresentacion');
    Route::post('/añadircategoria','ProyectoControllers\ProductosController@añadircategoria')->name('añadircategoria');

    Route::get('/codigobarrasproducto', 'ProyectoControllers\BarCodeController@codigobarrasproducto')->name('codigobarrasproducto');
    Route::get('/imprimircodigobarra/{codigo}',  'ProyectoControllers\BarCodeController@imprimircodigobarra') ->name('imprimircodigobarra');
    Route::get('/vistaimprimircodigobarra/{codigo}', 'ProyectoControllers\BarCodeController@vistaimprimircodigobarra')->name('vistaimprimircodigobarra');









                                            // Rutas de Promociones

    //Ruta de Promociones
    Route::get('/Seleccionarproducto','ProyectoControllers\ProductosController@selecciondeproducto')->name('Seleccionarproducto');

    //  Registrar
    Route::get('/registrarpromocion2','ProyectoControllers\PromocionesController@registrarpromocion')->name('registrarpromocion');
    Route::post('/insertarpromocion', 'ProyectoControllers\PromocionesController@insertarpromocion2')->name('insertarpromocion2');

    //Lista
    Route::get('/listapromociones','ProyectoControllers\PromocionesController@listapromociones')->name('listapromociones');
    Route::get('/listadopromociones','ProyectoControllers\PromocionesController@listadopromociones')->name('listadopromociones');
    Route::get('/modificarpromociones','ProyectoControllers\PromocionesController@listadopromociones')->name('modificarpromociones');
    Route::get('/listadopromoterminar','ProyectoControllers\PromocionesController@listadopromociones2')->name('listadopromoterminar');

    //Registrar - Insertar

    //Oferta
    Route::get('/regitrarpromo/{idProducto}','ProyectoControllers\PromocionesController@registrarpromocion2')->name('regitrarpromo');
    Route::post('/insertarpromocion', 'ProyectoControllers\PromocionesController@insertarpromocion')->name('insertarpromocion');

    //Descuento
    Route::get('/registrardescuento/{idProducto}','ProyectoControllers\PromocionesController@registrardescuento')->name('registrardescuento');
    Route::post('/insertardescuento', 'ProyectoControllers\PromocionesController@insertardescuento')->name('insertardescuento');

    //Paquete
    Route::get('/producto2/{idProducto}','ProyectoControllers\ProductosController@listaproductos2')->name('producto2');
    Route::get('/registrarpaquete/{idProducto}/{idProducto2}','ProyectoControllers\PromocionesController@registrarpaquete')->name('registrarpaquete');
    Route::post('/insertarpaquete', 'ProyectoControllers\PromocionesController@insertarpaquete')->name('insertarpaquete');


    //Modificar
    //Oferta
    Route::get('/modificarpromocion/{idPromociones}','ProyectoControllers\PromocionesController@modificarpromocion')->name('modificarpromocion');
    Route::put('/actualizarinfopromocion/{idPromociones}','ProyectoControllers\PromocionesController@actualizarinfopromocion')->name('actualizarinfopromocion');

    //Descuento
    Route::get('/modificarpromocion2/{idPromociones}','ProyectoControllers\PromocionesController@modificarpromocion2')->name('modificarpromocion2');
    Route::put('/actualizarinfopromocion2/{idPromociones}','ProyectoControllers\PromocionesController@actualizarinfopromocion2')->name('actualizarinfopromocion2');

    //Paquetes
    Route::get('/modificarpromocion3/{idPromociones}','ProyectoControllers\PromocionesController@modificarpromocion3')->name('modificarpromocion3');
    Route::put('/actualizarinfopromocion3/{idPromociones}','ProyectoControllers\PromocionesController@actualizarinfopromocion3')->name('actualizarinfopromocion3');


    Route::get('/selecciondeproducto','ProyectoControllers\ProductosController@selecciondeproducto')->name('selecciondeproducto');

    //Eliminar
    //Oferta
    Route::get('/confirmaroferta/{idPromociones}','ProyectoControllers\PromocionesController@confirmaroferta')->name('confirmaroferta');

    //Descuento
    Route::get('/confirmardescuento/{idPromociones}','ProyectoControllers\PromocionesController@confirmardescuento')->name('confirmardescuento');

    //Paquete
    Route::get('/confirmarpaquete/{idPromociones}','ProyectoControllers\PromocionesController@confirmarpaquete')->name('confirmarpaquete');

    Route::put('/terminarpromocion/{idPromociones}','ProyectoControllers\PromocionesController@terminarpromocion')->name('terminarpromocion');
            //RUTA ELIMINAR MODAL CONFIRMACION
    Route::get('/eliminarcambiarestadopromocion/{idPromociones}','ProyectoControllers\PromocionesController@eliminarcambiarestadopromocion')->name('eliminarcambiarestadopromocion');









                                            // Rutas de Proveedores

    //  Registrar
    Route::get('/registrarproveedor','ProyectoControllers\ProveedoresController@registrarproveedor')->name('registrarproveedor');
    Route::post('/insertarproveedor', 'ProyectoControllers\ProveedoresController@insertarproveedor')->name('insertarproveedor');

    //  Modificar
    Route::get('/modificarproveedor/{idProveedor}','ProyectoControllers\ProveedoresController@modificarproveedor')->name('modificarproveedor');
    Route::get('/modificarproveedorrfc/{rfc}','ProyectoControllers\ProveedoresController@modificarproveedorrfc')->name('modificarproveedorrfc');
    Route::put('/actualizarinfoproveedor/{idProveedor}','ProyectoControllers\ProveedoresController@actualizarinfoproveedor')->name('actualizarinfoproveedor');

    //Descontinuar
    Route::get('descontinuarproveedor/{idProveedor}','ProyectoControllers\ProveedoresController@descontinuarproveedor')->name('descontinuarproveedor');
    Route::get('descontinuarproveedorrfc/{rfc}','ProyectoControllers\ProveedoresController@descontinuarproveedorrfc')->name('descontinuarproveedorrfc');    
    Route::put('cambiarestadoproveedor/{idProveedor}','ProyectoControllers\ProveedoresController@cambiarestadoproveedor')->name('cambiarestadoproveedor');

    Route::get('eliminarcambiarestadoproveedor/{idProveedor}','ProyectoControllers\ProveedoresController@eliminarcambiarestadoproveedor')->name('eliminarcambiarestadoproveedor');
    Route::get('transferircambiarestadoproveedor/{idProveedor}','ProyectoControllers\ProveedoresController@transferircambiarestadoproveedor')->name('transferircambiarestadoproveedor');


    Route::get('/busquedaproveedor/{operacion}','ProyectoControllers\ProveedoresController@busquedaproveedor')->name('busquedaproveedor');
    Route::get('/buscarproveedor/{rfc}','ProyectoControllers\ProveedoresController@buscarproveedor')->name('buscarproveedor');

    Route::get('/listadoproveedores','ProyectoControllers\ProveedoresController@listadoproveedores')->name('listadoproveedores');


    //Registrar entrega pedido
    Route::get('/registrarentregapedido','ProyectoControllers\ProveedoresController@registrarentregapedido')->name('registrarentregapedido');
    Route::post('/insertarentregapedido','ProyectoControllers\ProveedoresController@insertarentregapedido')->name('insertarentregapedido');

    //Registrar devolución compra
    Route::get('/registrardevolucioncompra','ProyectoControllers\ProveedoresController@registrardevolucioncompra')->name('registrardevolucioncompra');
    Route::post('/insertardevolucioncompra','ProyectoControllers\ProveedoresController@insertardevolucioncompra')->name('insertardevolucioncompra');










                                            // Rutas de Clientes

    //  Registrar
    Route::get('/registrarcliente','ProyectoControllers\ClientesController@registrarclientes')->name('registrarcliente');
    Route::post('/insertarcliente', 'ProyectoControllers\ClientesController@insertarcliente')->name('insertarcliente');

    //Registro Rápido
    Route::get('/registrarrapidoclientes','ProyectoControllers\ClientesController@registrarrapidoclientes')->name('registrorapidocliente');

    //  Modificar
    Route::get('/modificarcliente/{idcliente}','ProyectoControllers\ClientesController@modificarcliente')->name('modificarcliente');
    Route::get('/modificarclientecurp/{curp}','ProyectoControllers\ClientesController@modificarclientecurp')->name('modificarclientecurp');
    Route::put('/actualizarinfocliente/{idCliente}','ProyectoControllers\ClientesController@actualizarinfocliente')->name('actualizarinfocliente');

    //Descontinuar
    Route::get('descontinuarcliente/{idCliente}','ProyectoControllers\ClientesController@descontinuarcliente')->name('descontinuarcliente');
    Route::get('descontinuarclientecurp/{curp}','ProyectoControllers\ClientesController@descontinuarclientecurp')->name('descontinuarclientecurp');
    Route::put('cambiarestadocliente/{idCliente}','ProyectoControllers\ClientesController@cambiarestadocliente')->name('cambiarestadocliente');


    Route::get('eliminarcambiarestadocliente/{idcliente}','ProyectoControllers\ClientesController@eliminarcambiarestadocliente')->name('eliminarcambiarestadocliente');

    //Búsquedas
    Route::get('/busquedacliente/{operacion}','ProyectoControllers\ClientesController@busquedacliente')->name('busquedacliente');
    Route::get('/buscarcliente/{curp}','ProyectoControllers\ClientesController@buscarcliente')->name('buscarcliente');
    
    Route::get('/listadoclientes','ProyectoControllers\ClientesController@listadoclientes')->name('listadoclientes');


    // Activar crédito cliente

    Route::get('/listadocreditossincredito', 'ProyectoControllers\ClientesController@listadocreditossincredito')->name('listadocreditossincredito');
    Route::get('/activarcreditocliente/{idCliente}', 'ProyectoControllers\ClientesController@activarcreditocliente')->name('activarcreditocliente');
    Route::post('/realizaractivacioncreditocliente/{idCliente}', 'ProyectoControllers\ClientesController@realizaractivacioncreditocliente')->name('realizaractivacioncreditocliente');

    // Rutas de Abonos Créditos Clientes

    Route::get('/listadocreditosclientes','ProyectoControllers\AbonosCreditoController@listadocreditosclientes')->name('listadocreditosclientes');
    Route::get('/informacioncreditocliente/{idcreditocliente}/{idcliente}', 'ProyectoControllers\AbonosCreditoController@informacioncreditocliente')->name('informacioncreditocliente');

    
    Route::post('/abonarcantidadcredito', 'ProyectoControllers\AbonosCreditoController@abonarcantidadcredito')->name('abonarcantidadcredito');
    Route::get('/abonarcantidadcredito', 'ProyectoControllers\AbonosCreditoController@abonarcantidadcreditoget')->name('abonarcantidadcredito');
    Route::get('/desactivarcreditocliente/{idcreditocliente}/{idcliente}','ProyectoControllers\AbonosCreditoController@desactivarcreditocliente')->name('desactivarcreditocliente');










                                            // Rutas de Empleados

    //  Registrar
    Route::get('/registrarempleado','ProyectoControllers\EmpleadosController@registrarempleados')->name('registrarempleado');
    Route::post('/insertarempleado', 'ProyectoControllers\EmpleadosController@insertarempleado')->name('insertarempleado');

    //  Modificar
    Route::get('/modificarempleado/{idempleado}','ProyectoControllers\EmpleadosController@modificarempleado')->name('modificarempleado');
    Route::put('/actualizarinfoempleado/{idempleado}','ProyectoControllers\EmpleadosController@actualizarinfoempleado')->name('actualizarinfoempleado');

    //Descontinuar
    Route::get('descontinuarempleado/{idempleado}','ProyectoControllers\EmpleadosController@descontinuarempleado')->name('descontinuarempleado');
    Route::put('cambiarestadoempleado/{idempleado}','ProyectoControllers\EmpleadosController@cambiarestadoempleado')->name('cambiarestadoempleado');

    Route::get('eliminarcambiarestadoempleado/{idempleado}', 'ProyectoControllers\EmpleadosController@eliminarcambiarestadoempleado')->name('eliminarcambiarestadoempleado');

    Route::get('/buscarempleado','ProyectoControllers\EmpleadosController@buscarempleado')->name('buscarempleado');

    Route::get('/busquedaempleado/{operacion}','ProyectoControllers\EmpleadosController@busquedaempleado')->name('busquedaempleado');


    Route::get('/listadoempleados','ProyectoControllers\EmpleadosController@listadoempleados')->name('listadoempleados');









                                            //Rutas de ventas

                                            
    Route::get('/registrarventa','ProyectoControllers\VentasController@registrarventa')->name('registrarventa');
    Route::post('/insertarventa','ProyectoControllers\VentasController@insertarventa')->name('insertarventa');

    Route::get('/busquedaventa', 'ProyectoControllers\VentasController@busquedaventa')->name('busquedaventa');
    Route::post('/realizarbusquedaventa', 'ProyectoControllers\VentasController@realizarbusquedaventa')->name('realizarbusquedaventa');

    Route::get('/listadoventas', 'ProyectoControllers\VentasController@listadoventas')->name('listadoventas');
    Route::get('/informaciondetalleventa/{idventa}', 'ProyectoControllers\VentasController@informaciondetalleventa')->name('informaciondetalleventa');

    Route::post('/imprimirticketventa', 'ProyectoControllers\VentasController@imprimirticketventa')->name('imprimirticketventa');

    //PDF corte caja (Ventas y Compras)
    Route::get('/cortediacaja', 'ProyectoControllers\VentasController@cortediacaja')->name('cortediacaja');
    Route::get('/exportarcortediacaja', 'ProyectoControllers\VentasController@exportarcortediacaja')->name('exportarcortediacaja');


    //PDF ventas del día
    Route::get('/cortediaventas', 'ProyectoControllers\VentasController@cortediaventas')->name('cortediaventas');
    Route::get('/exportarcortecajadiaventas', 'ProyectoControllers\VentasController@exportarcortecajadiaventas')->name('exportarcortecajadiaventas');

    //PDF compras del día
    Route::get('/cortediacompras', 'ProyectoControllers\VentasController@cortediacompras')->name('cortediacompras');
    Route::get('/exportarcortecajadiacompras', 'ProyectoControllers\VentasController@exportarcortecajadiacompras')->name('exportarcortecajadiacompras');










                                            // Rutas de Inventario

    //  Registrar entrada inventario
    Route::get('/registrarentradainventario','ProyectoControllers\InventarioController@registrarentradainventario')->name('registrarentradainventario');
    Route::post('/insertarentradainventario','ProyectoControllers\InventarioController@insertarentradainventario')->name('insertarentradainventario');

    //  Registrar salida inventario
    Route::get('/registrarsalidainventario','ProyectoControllers\InventarioController@registrarsalidainventario')->name('registrarsalidainventario');
    Route::post('/insertarsalidainventario','ProyectoControllers\InventarioController@insertarsalidainventario')->name('insertarsalidainventario');

    Route::get('/listadomovimientosinventario','ProyectoControllers\InventarioController@listadomovimientosinventario')->name('listadomovimientosinventario');

    Route::get('/catalogoproductos', 'ProyectoControllers\InventarioController@catalogoproductos')->name('catalogoproductos');
    Route::get('/filtradocatalogoproductos/{opcion}/{filtro}', 'ProyectoControllers\InventarioController@filtradocatalogoproductos')->name('filtradocatalogoproductos');

    Route::get('/alertasstockminimo', 'ProyectoControllers\InventarioController@alertasstockminimo')->name('alertasstockminimo');

    Route::get('/generarcodigo','ProyectoControllers\ProductosController@generarcodigo')->name('generarcodigo');

    //  Rutas para cambiar la imagen de las categorías, marcas o presentaciones
    Route::get('/cambiarinformacionfiltrocatalogoproductos/{opcionfiltrado}/{filtro}', 'ProyectoControllers\InventarioController@cambiarinformacionfiltrocatalogoproductos')->name('cambiarinformacionfiltrocatalogoproductos');

    Route::get('/registrarcategoria', 'ProyectoControllers\InventarioController@registrarcategoria')->name('registrarcategoria');
    Route::put('/actualizarinformacioncategoria/{idcategoria}','ProyectoControllers\InventarioController@actualizarinformacioncategoria')->name('actualizarinformacioncategoria');

    Route::get('/registrarmarca', 'ProyectoControllers\InventarioController@registrarmarca')->name('registrarmarca');
    Route::put('/actualizarinformacionmarca/{idmarca}','ProyectoControllers\InventarioController@actualizarinformacionmarca')->name('actualizarinformacionmarca');

    Route::get('/registrarpresentacion', 'ProyectoControllers\InventarioController@registrarpresentacion')->name('registrarpresentacion');
    Route::put('/actualizarinformacionpresentacion/{idpresentacion}','ProyectoControllers\InventarioController@actualizarinformacionpresentacion')->name('actualizarinformacionpresentacion');









                                            //Rutas de Reportes

        //RUTA PRINCIPAL
    Route::get('/reportes','ProyectoControllers\ReportesController@reportes')->name('reportes');

                        //REPORTES GENERALES

        //Reporte de clientes
    Route::get('/reporteclientes','ProyectoControllers\ReportesController@reporteclientes')->name('reporteclientes');
    Route::get('/generarreporteclientes','ProyectoControllers\ReportesController@generarreporteclientes')->name('generarreporteclientes');
    Route::get('/reporteclientesexcel','ProyectoControllers\ReportesController@reporteclientesexcel')->name('reporteclientesexcel');

        //Reporte de provedores
    Route::get('/reporteproveedores','ProyectoControllers\ReportesController@reportesprovedores')->name('reporteproveedores');
    Route::get('/generarreporteproveedores','ProyectoControllers\ReportesController@generarreporteproveedores')->name('generarreporteproveedores');
    Route::get('/reporteproveedoresexcel','ProyectoControllers\ReportesController@reporteproveedoresexcel')->name('reporteproveedoresexcel');

        //Reporte de productos
    Route::get('/reporteproductos','ProyectoControllers\ReportesController@reportesproductos')->name('reporteproductos');
    Route::get('/generarreporteproductos','ProyectoControllers\ReportesController@generarreporteproductos')->name('generarreporteproductos');
    Route::get('/reporteproductosexcel','ProyectoControllers\ReportesController@reporteproductosexcel')->name('reporteproductosexcel');

        //Reporte de promociones
    Route::get('/reportepromociones','ProyectoControllers\ReportesController@reportepromociones')->name('reportepromociones');
    Route::get('/generarreportepromociones','ProyectoControllers\ReportesController@generarreportepromociones')->name('generarreportepromociones');
    Route::get('/reportepromocionesexcel','ProyectoControllers\ReportesController@reportepromocionesexcel')->name('reportepromocionesexcel');

        //Reporte de ventas
    Route::get('/reporteventas','ProyectoControllers\ReportesController@reporteventas')->name('reporteventas');
    Route::get('/generarreporteventas','ProyectoControllers\ReportesController@generarreporteventas')->name('generarreporteventas');
    Route::get('/reporteventasexcel','ProyectoControllers\ReportesController@reporteventasexcel')->name('reporteventasexcel');

        //Reporte general de ventas
    Route::get('/reportegeneralventas', 'ProyectoControllers\ReportesController@reportegeneralventas')->name('reportegeneralventas');
    Route::get('/generarreportegeneralventas','ProyectoControllers\ReportesController@generarreportegeneralventas')->name('generarreportegeneralventas');
    Route::get('/reportegeneralventasexcel','ProyectoControllers\ReportesController@reportegeneralventasexcel')->name('reportegeneralventasexcel');



                        //REPORTES ESPECIFICOS

        //Reporte existencias de inventario
    Route::get('/reporteexistenciasinventario','ProyectoControllers\ReportesController@reporteexistenciasinventario')->name('reporteexistenciasinventario');
    Route::get('/generarreporteexistenciasinventario','ProyectoControllers\ReportesController@generarreporteexistenciasinventario')->name('generarreporteexistenciasinventario');

        //Reporte entradas de inventario
    Route::get('/reporteentradasinventario','ProyectoControllers\ReportesController@reporteentradasinventario')->name('reporteentradasinventario');
    Route::get('/generarreporteentradasinventario','ProyectoControllers\ReportesController@generarreporteentradasinventario')->name('generarreporteentradasinventario');

        //Reporte salidas de inventario
    Route::get('/reportesalidasinventario','ProyectoControllers\ReportesController@reportesalidasinventario')->name('reportesalidasinventario');
    Route::get('/generarreportesalidasinventario','ProyectoControllers\ReportesController@generarreportesalidasinventario')->name('generarreportesalidasinventario');

        //Reporte de productos vendidos
    Route::get('/reporteproductosvendidos','ProyectoControllers\ReportesController@reporteproductosvendidos')->name('reporteproductosvendidos');
    Route::get('/generarreporteproductosvendidos','ProyectoControllers\ReportesController@generarreporteproductosvendidos')->name('generarreporteproductosvendidos');
    
        //Reporte de productos bajos
    Route::get('/reporteproductosbajos','ProyectoControllers\ReportesController@reporteproductosbajos')->name('reporteproductosbajos');
    Route::get('/generarreporteproductosbajos','ProyectoControllers\ReportesController@generarreporteproductosbajos')->name('generarreporteproductosbajos');

    //Reporte de créditos de clientes
    Route::get('/reportecreditosclientes','ProyectoControllers\ReportesController@reportecreditosclientes')->name('reportecreditosclientes');
    Route::get('/generarreportecreditosclientes','ProyectoControllers\ReportesController@generarreportecreditosclientes')->name('generarreportecreditosclientes');

        //Reporte de movimientos de proveedores
    Route::get('/reporteproveedoresmovimientos','ProyectoControllers\ReportesController@reporteproveedoresmovimientos')->name('reporteproveedoresmovimientos');
    Route::get('/generarreporteproveedoresmovimientos','ProyectoControllers\ReportesController@generarreporteproveedoresmovimientos')->name('generarreporteproveedoresmovimientos');


    Route::get('/hoja','ProyectoControllers\Controladorhoja@listadoproductos')->name('hoja');
    Route::get('/hoja2','ProyectoControllers\Controladorhoja@listadoproductos2')->name('hoja');


    Route::name('print')->get('/imprimir', 'ControladorPrincipal@imprimir');


    //IMPORTAR

    // Rutas para Importar Datos Excel

    Route::get('/importardatos', 'ProyectoControllers\ImportarExportar\ImportarController@importardatos')->name('importardatos');

    //DATOS DEL ARCHIVO BUSSINESSCONTROL
    Route::get('/importardatosproductosbusinesscontrol', 'ProyectoControllers\ImportarExportar\ImportarController@importardatosproductosbusinesscontrol')->name('importardatosproductosbusinesscontrol');
    Route::post('/guardardatosproductosbusinesscontrol', 'ProyectoControllers\ImportarExportar\ImportarController@guardardatosproductosbusinesscontrol')->name('guardardatosproductosbusinesscontrol');

    //DATOS PARA CUALQUIER ARCHIVO EXCEL
    Route::get('/importardatosproductosexcel', 'ProyectoControllers\ImportarExportar\ImportarController@importardatosproductosexcel')->name('importardatosproductosexcel');
    Route::post('/guardardatosproductosexcel', 'ProyectoControllers\ImportarExportar\ImportarController@guardardatosproductosexcel')->name('guardardatosproductosexcel');


    //EXPORTAR

    // Rutas para Exportar Datos a Excel

    Route::get('/exportardatos', 'ProyectoControllers\ImportarExportar\ExportarController@exportardatos')->name('exportardatos');

    //EXPORTAR DATOS DE PRODUCTOS
    Route::get('/exportardatosproductos', 'ProyectoControllers\ImportarExportar\ExportarController@exportardatosproductos')->name('exportardatosproductos');
    
    
    //HOME
    Route::get('/registrarsaldoinicialcaja', 'ProyectoControllers\EmpleadoCajaController@registrarsaldoinicialcaja')->name('registrarsaldoinicialcaja');

});
