
        //Llamada a API - Producto
    function BuscarProducto() {

        var codigoproducto = $("#codigo").val();

        var ruta  = "{{ route('buscarproductoventa') }}";
        var url = ruta + "/" + codigoproducto;
        console.log("Codigo a buscar: " , codigoproducto );

          //hace la búsqueda
          $.get( url ).done( function( data ) {    
            var content = JSON.parse( JSON.stringify( data ) );   
            console.log("Content: " , content );
            var textocodigo = content.Codigo;
            var textodescripcion = content.Descripcion;
            var textoprecioventa = content.PrecioVenta;

            if(  textocodigo === undefined ) {
                //console.log("Consulta vacía");
                document.getElementById("codigo").value = "";
                document.getElementById("descripcion").value = "";
                document.getElementById("precio").value = "";
                document.getElementById("cantidad").value = "";
                document.getElementById("codigo").focus();
                    
                document.getElementById("textoalerta").value = "No hay ningún producto registrado con ese código de barras";
                document.getElementById("contentalert").style.display = "block";

            } else {            

                //BuscarOfertaProducto( codigoproducto );
                console.log("Consula exitosa");
                
                VerificarPromocionesProducto( codigoproducto );
                
                document.getElementById("codigo").value = textocodigo;
                document.getElementById("descripcion").value = textodescripcion;
                document.getElementById("precio").value = textoprecioventa;
                document.getElementById("cantidad").value = "1";
                document.getElementById("cantidad").readOnly = false;
                CalcularImporte();
                document.getElementById("cantidad").focus();
            }

            /*
            document.getElementById("tablaproductos").insertRow(-1).innerHTML = 
            "<td>" + textocodigo + "</td>" +
            "<td>" + textodescripcion + "</td>" +
            "<td></td>" +
            "<td>" + textoprecioventa + "</td>" +
            "<td></td>";
            */
              
          }).catch(function( error) {
              console.log("El error es: ", error);
          });

    }

    function VerificarPromocionesProducto( $codigo ) {

        var ruta = "{{ route('verificarpromocionesproducto') }}";
        var url = ruta + "/" + $codigo;

        $.get( url ).done( function ( data ) {

            var content = JSON.parse( JSON.stringify( data ) );   
            console.log("Content VerificarPromocionesProducto: " , content );

            var tamañojson = Object.keys( data ).length;
            console.log("Tamaño JSON: " , tamañojson );

        });

    }

    function BuscarOfertaProducto ( $codigo ) {

        var ruta = "{{ route('buscarpromocionoferta') }}";
        var url = ruta + "/" + $codigo;

        $.get( url ).done( function ( data ) {

            var content = JSON.parse( JSON.stringify( data ) );   
            console.log("Content ofertaproducto: " , content );
            var tamañojson = Object.keys( data ).length;
            console.log("Tamaño JSON: " , tamañojson );

            if( tamañojson > 1 ) {
                var tipooferta = content.TipoPromocion;
                var textotipooferta;

                if(  TipoPromocion == 1 ) {

                    textotipooferta = "2x1";

                } else if( TipoPromocion == 2 ) {

                    textotipooferta = "3x2";

                } else if( TipoPromocion == 3 ) {

                    textotipooferta = "4x3";

                } else if( TipoPromocion == 4 ) {

                    textotipooferta = "5x4";

                }

            } else {


            }

        });

    }

    function BuscarDescuentoProducto( $codigo ) {
        
        var obj = 1;

        var ruta = "{{ route('buscarpromociondescuento' ) }}";
        var url = ruta + "/" + $codigo;

        $.get( url ).done( function( data ) {
            obj = 2;
        });

        return obj;

    }

        //Llamada a API - Cliente
    function FiltrarClientesPorNombre() {

        var nombrecliente = $("#nombrecliente").val();

        var ruta  = "{{ route('filtrarclientespornombre') }}";
        var url = ruta + "/" + nombrecliente;

          //hace la búsqueda
          $.get( url ).done( function( data ) {    
            var content = JSON.parse( JSON.stringify( data ) );  

            //Verificar si el contenido de la respuesta de la api no esta vacía.
            if( content.length > 0 ) {

                var selectclientes = document.getElementById("idcliente");
                
                //Eliminar todas las opciones del select
                for ( var i = selectclientes.length; i >= 0; i--) {
                    selectclientes.remove(i);
                }

                    //Añadir una opción por defecto.
                var optioncliente = document.createElement('option');
                    optioncliente.text = "Seleccione al cliente";
                    optioncliente.value = "";
                    selectclientes.add( optioncliente );

                    //Agregar las opciones de la respuesta de la consulta.
                for( var k in content ) {

                    var optioncliente = document.createElement( 'option' );              
                    optioncliente.text = content[k].nombrecompleto;
                    optioncliente.value = content[k].idcliente; //id para el valor
                    selectclientes.add( optioncliente );

                }

                document.getElementById("nombrecliente").focus();

            } else {

                //console.log("Consulta vacía");
                document.getElementById("nombrecliente").focus();
                    
                //document.getElementById("textoalerta").value = "No hay ningún producto registrado con ese código de barras";
                //document.getElementById("contentalert").style.display = "block";

            }
              
          }).catch(function( error) {
              console.log("El error es: ", error);
          });

    }

    function onKeyUp(event) {
        var keycode = event.keyCode;

        if(keycode == '13'){
            BuscarProducto();
        }
    }

    function CalcularImporte() {

        var textoprecio = document.getElementById("precio").value;
        var textocantidad = document.getElementById("cantidad").value;

        if( textocantidad != "" ) {

            var numeroprecio = parseFloat( textoprecio );
            var numerocantidad = parseInt( textocantidad );

            var cantidadimporte = ( numerocantidad * numeroprecio );
            document.getElementById("importe").value = cantidadimporte;

        } else {
            document.getElementById("importe").value = "";
        }
            

    }

    function CalcularTotal() {
        
        var total = 0.0;

        $('#tablaproductos tr').each(function (row, tr ) {
            var textocoluman = 0;
            textocolumna = Number.parseFloat( $(tr).find('td:eq(4)').text().trim() );
            
            if( !isNaN( textocolumna ) ) {
                total = textocolumna  + total;
            }

        }); 

        document.getElementById("total").value = total;

    }

    function CalcularCambio() {


        var textototal = Number.parseFloat( document.getElementById("total").value );
        var textoefectivo = Number.parseFloat( document.getElementById("efectivo").value );

        if( isNaN( textoefectivo ) ) {
            textoefectivo = 0.0;
        }

        var textocambio = textoefectivo - textototal;
        document.getElementById("cambio").value = textocambio;

    }

    //Saber si fue una tecla correspondiente a un número
    function onKeyDownHandler() {
         //console.log("key pressed ",  String.fromCharCode(event.keyCode));
         //console.log("key pressedx2 ", String.fromCharCode(event.which));

         if( document.getElementById("total").value == "" ) {

            var keypresionada = event.which;
            if( ( keypresionada >= 48 && keypresionada <= 57 ) || ( keypresionada >=96 && keypresionada <= 105 ) ) {
                
                CalcularImporte();

            } else if( keypresionada == 13 ) {

                if( document.getElementById("codigo").value != "" ) {
                    document.getElementById("codigo").focus();
                    CalcularImporte();
                    VerificarFilaDefault();
                }
                
            }

         } else if( document.getElementById("total").value != "" && document.getElementById("efectivo").value != "" ) {
            
            var keypresionada = event.which;
            if( ( keypresionada >= 48 && keypresionada <= 57 ) || ( keypresionada >=96 && keypresionada <= 105 ) || keypresionada == 13 ) {
                                
                CalcularCambio();

            }
         }
    }

    function VerificarFilaDefault() {

        const tabla = document.getElementById('tablaproductos');
        var rowCount = document.getElementById("tablaproductos").rows.length;

            //Sólo está la fila por defecto
        if( rowCount == 2 ) {            

            var bool = false;

            $("td").each(function(){
                    //cambiar "CODIGO" por el texto que está por defecto en la primera columna de código de producto.
                if( $(this).text() == "-" ) {
                    bool = true;
                    //console.log($(this).text());
                }
            });

            //Tiene la fila por default.
            if( bool ) {
                tabla.deleteRow(rowCount -1);
                AgregarFila();

                //Ya tiene el registro de un producto en la primera fila.
            } else {

                var textocodigobuscado = document.getElementById("codigo").value;
                var bool = false;

                $("td").each(function(){
                        //cambiar "CODIGO" por el texto que está por defecto en la primera columna de código de producto.
                    if( $(this).text() == textocodigobuscado ) {
                        bool = true;
                        //console.log($(this).text());
                    }
                });

                    //El producto ya está en la tabla ( Cuando sólo hay un producto en la tabla).
                if( bool ) {
                    ActualizarProductoTabla( 1 );
                } else {
                    AgregarFila();
                }

            }

            // Más de un producto agregado a la tabla
        } else {

            var textocodigobuscado = document.getElementById("codigo").value;
            var bool = false;
            var fila = 1;    
            
            for( var i = 1; i < tabla.rows.length; i++ ) {
                var textotablacodigo = tabla.rows[i].cells[0].innerText;
                if( textocodigobuscado == textotablacodigo ) {
                    bool = true;
                    fila = i;
                }
            }            
            
                    //El producto ya está en la tabla ( Cuando sólo hay más de producto en la tabla).
            if( bool ) {
                ActualizarProductoTabla( fila );
            } else {
                AgregarFila();
            }

        }

    }

    function ActualizarProductoTabla( fila ) {
                
        const tabla = document.getElementById('tablaproductos');

        var cantidadold = parseInt( tabla.rows[fila].cells[2].innerText );
        var cantidadnew = parseInt( document.getElementById("cantidad").value );
        var precio = parseFloat( tabla.rows[fila].cells[3].innerText );

        // Verificación del texto del botón de agregar producto.
        textoenlaceagregar = document.getElementById("enlaceagregar").text;
        console.log("Texto del enlace: " , textoenlaceagregar );

        if( document.getElementById("enlaceagregar").text == "Agregar" ) {
            cantidadnew += cantidadold;

        } else if( document.getElementById("enlaceagregar").text == "Modificar" ) {

            document.getElementById("enlaceagregar").text = "Agregar";
            document.getElementById("enlaceagregar").style.marginTop = "35%";

            document.getElementById("enlaceeliminar").style.display = "none";
            document.getElementById("enlaceeliminar").style.marginTop = "1%";
            
            document.getElementById("codigo").readOnly = false;
            document.getElementById("codigo").focus();

        }

        var importenew = ( cantidadnew * precio );
        document.getElementById("tablaproductos").rows[fila].cells[2].innerText = cantidadnew;
        document.getElementById("tablaproductos").rows[fila].cells[4].innerText = importenew;

        document.getElementById("codigo").value = "";
        document.getElementById("descripcion").value = "";
        document.getElementById("cantidad").value = "";
        document.getElementById("precio").value = "";
        document.getElementById("importe").value = "";

        document.getElementById("cantidad").readOnly = true;
        document.getElementById("codigo").focus();
        
        CalcularTotal();

    }

    function ModificarFilaProducto( event ) {
        
        var table = document.getElementById('tablaproductos'),
        selected = table.getElementsByClassName('selected');
        
        if (selected[0]) selected[0].className = '';
        event.target.parentNode.className = 'selected';

        /*
        event.target.parentNode.className = 'selected';
    
        alert($("tr.selected td:first" ).html());
        console.log("Codigo: " ,$("tr.selected td:first" ).html() );
        console.log("Descripcion: ", $("tr.selected td:nth-child(2)").html() );
        console.log("Cantidad: ", $("tr.selected td:nth-child(3)").html() );
        console.log("Precio: ", $("tr.selected td:nth-child(4)").html() );
        console.log("Importe: ", $("tr.selected td:nth-child(5)").html() );
        */

        document.getElementById("codigo").value = $("tr.selected td:first" ).html();
        document.getElementById("descripcion").value = $("tr.selected td:nth-child(2)").html();
        document.getElementById("cantidad").value = $("tr.selected td:nth-child(3)").html();
        document.getElementById("precio").value = $("tr.selected td:nth-child(4)").html();
        document.getElementById("importe").value = $("tr.selected td:nth-child(5)").html();

        document.getElementById("codigo").readOnly = true;
        document.getElementById("descripcion").readOnly = true;
        document.getElementById("cantidad").readOnly = false;
        document.getElementById("precio").readOnly = true;
        document.getElementById("importe").readOnly = true;

        document.getElementById("cantidad").focus();

        document.getElementById("enlaceagregar").text = "Editar";
        document.getElementById("enlaceagregar").style.marginTop = "1%";
        document.getElementById("enlaceeliminar").style.display = "block";
        document.getElementById("enlaceeliminar").style.marginTop = "4%";

    }

    function AgregarFila() {
                
        var textocodigo = document.getElementById("codigo").value;
        var textodescripcion = document.getElementById("descripcion").value;
        var textocantidad = document.getElementById("cantidad").value;
        var textoprecioventa = document.getElementById("precio").value;
        var textoimporte = document.getElementById("importe").value;

        if( textocodigo != "" && textodescripcion != "" && textocantidad != "" && textoprecioventa != "" && textoimporte != "" ) {

            const tabla = document.getElementById('tablaproductos');
            var rowCount = document.getElementById("tablaproductos").rows.length;
            //console.log("rowCount: " , rowCount );

            document.getElementById("tablaproductos").insertRow(-1).innerHTML = 
                "<td>" + textocodigo + "</td>" +
                "<td>" + textodescripcion + "</td>" +
                "<td>"+ textocantidad + "</td>" +
                "<td>" + textoprecioventa + "</td>" +
                "<td>"+ textoimporte + "</td>";

            document.getElementById("codigo").value = "";
            document.getElementById("descripcion").value = "";
            document.getElementById("cantidad").value = "";
            document.getElementById("precio").value = "";
            document.getElementById("importe").value = "";
            document.getElementById("cantidad").readOnly = true;
            
            CalcularTotal();

        } else {

            document.getElementById("textoalerta").value = "No hay producto que agregar";
            document.getElementById("contentalert").style.display = "block";

        }

    }

    function EliminarFilaProducto() {
                
        const tabla = document.getElementById('tablaproductos');
        var textocodigobuscado = document.getElementById("codigo").value;
        var bool = false;
        var fila = 1;    
        
        for( var i = 1; i < tabla.rows.length; i++ ) {
            var textotablacodigo = tabla.rows[i].cells[0].innerText;
            if( textocodigobuscado == textotablacodigo ) {
                bool = true;
                fila = i;
            }
        } 

        if( bool ) {

            tabla.deleteRow( fila, -1 ); 
            document.getElementById("enlaceagregar").text = "Agregar";
            document.getElementById("enlaceeliminar").style.display = "none";

            document.getElementById("codigo").value = "";
            document.getElementById("descripcion").value = "";
            document.getElementById("cantidad").value = "";
            document.getElementById("precio").value = "";
            document.getElementById("importe").value = "";

            document.getElementById("cantidad").readOnly = true;
            document.getElementById("codigo").focus();


        }

    }

    function LimpiarInputs() {        

        const tabla = document.getElementById('tablaproductos');
        var rowCount = tabla.rows.length; 

        for  ( var x = rowCount - 1; x > 0; x-- ) { 
            tabla.deleteRow(x); 
        } 
 
        document.getElementById("enlaceagregar").text = "Agregar";
        document.getElementById("enlaceeliminar").style.display = "none";

        document.getElementById("codigo").value = "";
        document.getElementById("descripcion").value = "";
        document.getElementById("cantidad").value = "";
        document.getElementById("precio").value = "";
        document.getElementById("importe").value = "";

        document.getElementById("cantidad").readOnly = true;
        document.getElementById("codigo").readOnly = false;
        document.getElementById("codigo").focus();

        CalcularTotal();

    }

        //Obtiene los datos de todas las columnas de la tabla
    function GuardarDatosTabla() {
        
        var TableData = new Array();

        $('#tablaproductos tr').each(function(row, tr){

        TableData[row]=
        {
            "ColumnaCodigos" : $(tr).find('td:eq(0)').text().trim() //for first column value
            , "ColumnaCantidades" :$(tr).find('td:eq(2)').text().trim()  //for third column value
        }    
        }); 
        console.log("Valores: " , TableData);
        
        for (let index = 1; index < TableData.length; index++) {
            const element = TableData[index];
            console.log("Codigo: " , element);
            
        }
        TableData.shift();  // first row will be empty - so remove
        return TableData;

    }

    function EnviarDatosTabla() {   

        var TableData;
        TableData = GuardarDatosTabla()
        TableData = JSON.stringify(TableData);

        
        var selectclientes = document.getElementById("idcliente").value;
        var IdCliente;
            
        if( document.getElementById("nombrecliente").value == "" && selectclientes != "" ) {
            //Se pagará a crédito pero no se hizo el filtrado y se seleccionó un cliente del select original
            IdCliente = ( parseInt( selectclientes ) + 1 );

        } else if( document.getElementById("nombrecliente").value != "" && selectclientes == "" ) {
            //Se pagará con crédito pero no se selecciono al cliente
            TableData = undefined;

        } else if( document.getElementById("nombrecliente").value != "" && selectclientes != "" ) {
            //Se pagará con crédito y se selecciono al cliente
            IdCliente = selectclientes;

        } else if( document.getElementById("nombrecliente").value == "" && selectclientes == "" ) {
            //Se pagará a contado.
            selectclientes = null;        
            IdCliente = selectclientes;

        }

        IdCliente = JSON.stringify( IdCliente );
        console.log("ID seleccionado: ", IdCliente );


        var Total;
        Total = JSON.stringify( document.getElementById("total").value );
        console.log("Total: " , Total );

        var Descuento;
        if( document.getElementById("descuento").value == "" ) {
            Descuento = JSON.stringify( "0.0" );
        } else {
            Descuento = JSON.stringify( document.getElementById("descuento").value );
        }
        console.log("Descuento: ", Descuento );

        var Efectivo;
        if(  Number.parseFloat(  document.getElementById("cambio").value ) < 0 ) {
            
            Efectivo = undefined;

        } else {

            Efectivo = JSON.stringify( document.getElementById("efectivo").value );

        }
        console.log("Efectivo", Efectivo );

        const tabla = document.getElementById('tablaproductos');
        var numfilas = tabla.rows.length;        
        var textotabla = undefined;

        if( numfilas > 1 ) {
            if( tabla.rows[1].cells[0].innerText == "-" ) {
                textotabla = undefined;
            } else {
                textotabla = "Aplica";
            }
        }

        if( textotabla == undefined && Efectivo == "" ) {
            OcultarModal( "No ha agregado ningún producto" );            
        } else if( textotabla == "Aplica" ) {

            $.ajaxSetup({
                headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                type: "POST",
                url: "{{ route('insertarventa') }}",
                data: { TableData : TableData, IdCliente : IdCliente, Total : Total, 
                        Descuento : Descuento, Efectivo : Efectivo },
                success: function(data){
                    LimpiarInputs();
                    alert('success');                
                },
                error:function()
                { 
                    if( Number.parseFloat(  document.getElementById("cambio").value ) < 0 ) {
                        alert(' ERROR DE CAMBIO');
                    } else {
                        alert('error');
                    }

                }
            });


        }

    }

    function OcultarAlerta() {

        if( document.getElementById("contentalert").style.display == "none" ) {
            document.getElementById("contentalert").style.display = "block";
        } else {
            document.getElementById("contentalert").style.display = "none";
        }

    }

    function OcultarModal( caso ) {
        
        $('#contentmodal').modal('show');
        document.getElementById("textomodal").innerHTML = caso;        

    }