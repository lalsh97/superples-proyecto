
    
    function generarcurp() {

        var curpgenerada = "";

        if( $('#curp').val() == "" ) {

            if( $('#nombre').val() != "" && $('#apellidopat').val() !="" && $('apellidomat').val() !=""  ) {

                var primeraletraapellidopat;
                var vocalapellidopat;
                var primeraletraapellidomat;
                var primeraletranombre;
                var fechanacimientodigitos;
                var caractersexo;
                var entidaddenacimiento;
                var consonanteinternaapellidopat;
                var consonanteinternaapellidomat;
                var consonanteinternanombre;

                var textonombre = document.getElementById('nombre').value.toUpperCase();
                    textonombre = textonombre.replace("Á","A");
                    textonombre = textonombre.replace("É","E");
                    textonombre = textonombre.replace("Í","I");
                    textonombre = textonombre.replace("Ó","O");
                    textonombre = textonombre.replace("Ú","U");
                    
                var textoapellidopat = document.getElementById('apellidopat').value.toUpperCase();
                    textoapellidopat = textoapellidopat.replace("Á","A");
                    textoapellidopat = textoapellidopat.replace("É","E");
                    textoapellidopat = textoapellidopat.replace("Í","I");
                    textoapellidopat = textoapellidopat.replace("Ó","O");
                    textoapellidopat = textoapellidopat.replace("Ú","U");

                var textoapellidomat = document.getElementById('apellidomat').value.toUpperCase();
                    textoapellidomat = textoapellidomat.replace("Á","A");
                    textoapellidomat = textoapellidomat.replace("É","E");
                    textoapellidomat = textoapellidomat.replace("Í","I");
                    textoapellidomat = textoapellidomat.replace("Ó","O");
                    textoapellidomat = textoapellidomat.replace("Ú","U");

                        // PRIMER CARACTER
                primeraletraapellidopat = textoapellidopat.substr(0,1);

                        //  SEGUNDO CARACTER
                for( var i = 1; i< textoapellidopat.length; i++ ) {
                    if( textoapellidopat.charAt(i) == 'A' || textoapellidopat.charAt(i) == 'E' || textoapellidopat.charAt(i) == 'I' || textoapellidopat.charAt(i) == 'O' || textoapellidopat.charAt(i) == 'U' ) {
                        vocalapellidopat = textoapellidopat.charAt(i);
                        break;
                    } else {
                        vocalapellidopat = "";
                    }
                }

                        //  TERCER CARACTER
                primeraletraapellidomat = textoapellidomat.substr(0,1);

                        //  CUARTO CARACTER
                var arraynombres = textonombre.split(" ");                

                if( arraynombres.length > 1 ) {
                    var nombreingresado = arraynombres[0];
                    if( nombreingresado == 'MARIA' || nombreingresado == 'JOSE' ) {
                        primeraletranombre =  arraynombres[1].substr(0,1);
                    } else {
                        primeraletranombre = arraynombres[0].substr(0,1);
                    }
                } else {
                    primeraletranombre = arraynombres[0].substr(0,1);
                }

                        // QUINTO AL DECIMO CARACTER
                fecha = document.getElementById('fechanac').value;

                arrayfecha = fecha.split("-");

                digitoanio = arrayfecha[0];
                digitoanio = digitoanio.substr( 2, digitoanio.length );
                digitomes = arrayfecha[1];
                digitodia = arrayfecha[2];
                
                
                if( digitoanio === undefined ) {
                    digitoanio = "--";
                } else if( digitomes === undefined ) {
                    digitomes = "-";    
                } else if( digitodia === undefined ) {
                    digitodia = "-";    
                }

                fechanacimientodigitos = "";
                fechanacimientodigitos = fechanacimientodigitos.concat( digitoanio, digitomes, digitodia );

                    //ONCEAVO CARACTER

                if( document.getElementById('sexoh').checked == true ) {
                    caractersexo = "H";
                } else if( document.getElementById('sexom').checked == true ) {
                    caractersexo = "M";
                } else {
                    caractersexo = "-";
                }
                        
                //caractersexo = $('#sexo').val();

                        //  DOCEAVO Y TRECEAVO CARACTER
                entidaddenacimiento = 'OC';

                        //CATORCEAVO CARACTER
                for( var i = 1; i< textoapellidopat.length; i++ ) {
                    if( textoapellidopat.charAt(i) == 'A' || textoapellidopat.charAt(i) == 'E' || textoapellidopat.charAt(i) == 'I' || textoapellidopat.charAt(i) == 'O' || textoapellidopat.charAt(i) == 'U' ) {
                        consonanteinternaapellidopat = "";
                    } else {
                        consonanteinternaapellidopat = textoapellidopat.charAt(i);
                        break;
                    }
                }

                        //QUINCEAVO CARACTER
                for( var i = 1; i< textoapellidomat.length; i++ ) {
                    if( textoapellidomat.charAt(i) == 'A' || textoapellidomat.charAt(i) == 'E' || textoapellidomat.charAt(i) == 'I' || textoapellidomat.charAt(i) == 'O' || textoapellidomat.charAt(i) == 'U' ) {
                        consonanteinternaapellidomat = "";
                    } else {
                        consonanteinternaapellidomat = textoapellidomat.charAt(i);
                        break;
                    }
                }
                          
                        //DIECISEISAVO CARACTER
                for( var i = 1; i< textonombre.length; i++ ) {
                    if( textonombre.charAt(i) == 'A' || textonombre.charAt(i) == 'E' || textonombre.charAt(i) == 'I' || textonombre.charAt(i) == 'O' || textonombre.charAt(i) == 'U' ) {
                        consonanteinternanombre = "";
                    } else {
                        consonanteinternanombre = textonombre.charAt(i);
                        break;
                    }
                }


                curpgenerada = curpgenerada.concat( primeraletraapellidopat, vocalapellidopat,
                 primeraletraapellidomat, primeraletranombre, fechanacimientodigitos,
                 caractersexo, entidaddenacimiento, consonanteinternaapellidopat,
                 consonanteinternaapellidomat, consonanteinternanombre );

                if( curpgenerada.includes("-") ) {
                    curpgenerada = "";
                } else {
                    document.getElementById('curp').value = curpgenerada;
                }

            }


        } else {
            
            var textocurp = document.getElementById('curp').value;

            if( textocurp.length <= 16 ) {

                //document.getElementById('curp').value = document.getElementById('curp').value;
                document.getElementById('curp').value = "";
                generarcurp();

            }

        }

    }

    function mayusculas() {

        var textocurp = document.getElementById('curp').value;
        textocurp = textocurp.toUpperCase();

        document.getElementById('curp').value = textocurp;
    }

