@if( Auth::user()->Nivel == 1 )

    @extends('home')

    @section('title')
        Registro de proveedor
    @endsection

    @section('contenido')

        <style>
            strong {

                color: red;
                font-size: 20px;

            }

        </style>

        <script>

            function generarrfc() {

                var rfcgenerado = "";

                if( $('#nombre').val() != "" && $('#apellidopat').val() != ""  ) {

                    var primeraletraapellidopat;
                    var segundaletraapellidopat;
                    var primeraletraapellidomat;
                    var primeraletranombre;
                    var fechanacimientodigitos;
                    var primerasletrasrazonsocial;

                    var textonombre = document.getElementById('nombre').value.toUpperCase();
                        textonombre = textonombre.replace("Á","A");
                        textonombre = textonombre.replace("É","E");
                        textonombre = textonombre.replace("Í","I");
                        textonombre = textonombre.replace("Ó","O");
                        textonombre = textonombre.replace("Ú","U");

                    var textoapellidopat = document.getElementById('apellidopat').value.toUpperCase();
                        textoapellidopat = textoapellidopat.replace("Á","A");
                        textoapellidopat = textoapellidopat.replace("É","E");
                        textoapellidopat = textoapellidopat.replace("Í","I");
                        textoapellidopat = textoapellidopat.replace("Ó","O");
                        textoapellidopat = textoapellidopat.replace("Ú","U");

                    var textoapellidomat = document.getElementById('apellidomat').value.toUpperCase();
                        textoapellidomat = textoapellidomat.replace("Á","A");
                        textoapellidomat = textoapellidomat.replace("É","E");
                        textoapellidomat = textoapellidomat.replace("Í","I");
                        textoapellidomat = textoapellidomat.replace("Ó","O");
                        textoapellidomat = textoapellidomat.replace("Ú","U");

                    //Personas físicas 13 carácteres ( 4 de iniciales de apellidos y nombre, 6 fecha y 3 homoclave ).
                    //Personas morales 12 carácteres( 3 -> "Primeras letras de su razón social", 6 fecha, 3 homoclave ).


                            // PRIMERO AL TERCER CARACTER INICIALES DE LA RAZON SOCIAL
                    primerasletrasrazonsocial = ("AAA");

                            // CUARTO AL NOVENO CARACTER - año, mes, día.
                    //fecha = document.getElementById('fechanac').value;
                    fecha = $('#fechanac').val();

                    arrayfecha = fecha.split("-");

                    digitoanio = arrayfecha[0];
                    digitoanio = digitoanio.substr( 2, digitoanio.length );
                    digitomes = arrayfecha[1];
                    digitodia = arrayfecha[2];

                    if( fecha == "" ) {
                        fechanacimientodigitos = "******";
                    } else {
                        fechanacimientodigitos = "";
                        fechanacimientodigitos = fechanacimientodigitos.concat( digitoanio, digitomes, digitodia );
                    }

                    rfcgenerado = rfcgenerado.concat( primerasletrasrazonsocial, fechanacimientodigitos, "000");

                    if( rfcgenerado.includes("-") ) {
                        rfcgenerado = "";
                    } else {
                        document.getElementById('rfc').value = "";
                        document.getElementById('rfc').value = rfcgenerado;
                        mayusculas();
                    }

                }

            }

            function mayusculas() {

                var textorfc = document.getElementById('rfc').value;
                textorfc = textorfc.toUpperCase();

                document.getElementById('rfc').value = textorfc;
            }

            function consultar() {

                var endpoint_sepomex  = "http://api-sepomex.hckdrk.mx/query/";
                var method_sepomex = 'info_cp/';
                //var cp = "09810";
                var cp = document.getElementById('codigop').value;
                var variable_string = '?type=simplified';
                var url = endpoint_sepomex + method_sepomex + cp + variable_string;

                $.get( url ).done( function( data ) {
                    var content = JSON.parse( JSON.stringify( data ) );

                    //console.log( content );
                    //document.getElementById('colonia').value = content.response.asentamiento;
                    var opcionescolonia = content.response.asentamiento;

                    var selectcolonias = document.getElementById( 'colonia' );

                    for ( var i = selectcolonias.length; i >= 0; i--) {
                        selectcolonias.remove(i);
                    }

                    if( opcionescolonia.length > 0 ) {
                        var optioncolonia = document.createElement('option');
                                optioncolonia.text = "Seleccione una colonia";
                                optioncolonia.value = "";
                                selectcolonias.add( optioncolonia );

                        for( var i = 0; i < opcionescolonia.length; i++ ) {
                            var optioncolonia = document.createElement( 'option' );
                                optioncolonia.text = opcionescolonia[i];
                                optioncolonia.value = opcionescolonia[i];
                                selectcolonias.add( optioncolonia );

                        }

                    }

                    if( content.response.ciudad == "" && content.response.municipio != "" ) {

                        document.getElementById('ciudad').value = "Oaxaca de Juárez";
                        document.getElementById('municipio').value = content.response.municipio;

                    } else if( content.response.ciudad != "" && content.response.municipio != "" ) {

                        document.getElementById('ciudad').value = content.response.ciudad;
                        document.getElementById('municipio').value = content.response.municipio;

                    }
                }).catch(function( error) {

                    var selectcolonias = document.getElementById( 'colonia' );

                    for ( var i = selectcolonias.length; i >= 0; i--) {
                        selectcolonias.remove(i);
                    }


                    var optioncolonia = document.createElement('option');
                        optioncolonia.text = "Seleccione una colonia";
                        optioncolonia.value = "";
                        selectcolonias.add( optioncolonia );

                    optioncolonia = document.createElement('option');
                        optioncolonia.text = "DOMICILIO CONOCIDO";
                        optioncolonia.value = "DOMICILIO CONOCIDO";
                        selectcolonias.add( optioncolonia );

                    document.getElementById('ciudad').value = "";
                    document.getElementById('municipio').value = "";

                });

            }

        </script>

        @if(Session::has('success'))
            <div class="alert alert-success">
                {{session('success')}}
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            </div>
        @elseif(Session::has('danger'))
            <div class="alert alert-danger">
                {{session('danger')}}
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            </div>
        @elseif(Session::has('warning'))
            <div class="alert alert-warning">
                {{session('warning')}}
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            </div>
        @endif

        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header"  align="center">{{ __('Registro de proveedor') }}</div>
                            <div class="card-body">

                                {!!Form::open(array('name' => 'formproducto', 'url'=>'insertarproveedor','method'=>'POST' ,'autocomplete'=>'off'))!!}

                                    <!-- DATOS PERSONALES -->
                                    <fieldset  class="border p-2">
                                        <legend  class="w-auto"> Datos del Proovedor </legend>

                                        <!-- Nombre de la empresa  -->
                                        <div class="form-group row">
                                          <label for="empresa" class="col-md-4 col-form-label text-md-right">{{ __('Nombre de la empresa ') }}<strong>*</strong></label>
                                            <div class="col-md-6">
                                                <input type="text" id="empresa" name="empresa" placeholder="Ingrese el nombre de la empresa" class="form-control"
                                                    value="{{ old('empresa') }}" maxlength="45" minlength="1" pattern="[\w\sÁ-ÿ\u00C0-\u00FF]{1,45}" required
                                                    oninvalid="setCustomValidity('El nombre es obligatorio y debe tener como máximo 45 carácteres')"
                                                    oninput="setCustomValidity('')" >
                                            </div>
                                        </div>

                                        <!-- R.F.C. del proveedor  -->
                                        <div class="form-group row">
                                          <label for="rfc" class="col-md-4 col-form-label text-md-right">{{ __('RFC del proveedor ') }}<strong>*</strong></label>
                                          <div class="col-md-6">
                                                <input type="text" id="rfc" name="rfc" placeholder="Ingrese el rfc del proveedor" class="form-control"
                                                    value="{{old('rfc')}}" maxlength="13" minlength="1" required
                                                    pattern="^([A-ZÑ\x26]{3,4}([0-9]{2})(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1]))((-)?([A-Z\d]{3}))?$"
                                                    oninvalid="setCustomValidity('El rfc debe respetar el formato del rfc y puede tener un máximo de 13 carácteres')"
                                                    oninput="setCustomValidity('')">
                                            </div>
                                        </div>

                                        <!-- Nombre del proveedor  -->
                                        <div class="form-group row">
                                          <label for="nombre" class="col-md-4 col-form-label text-md-right">{{ __('Nombre(s) del contacto ') }}<strong>*</strong></label>
                                          <div class="col-md-6">
                                                <input type="text" id="nombre" name="nombre" placeholder="Ingrese el nombre del contacto" class="form-control"
                                                value="{{ old('nombre') }}" maxlength="45" minlength="1" required pattern="[\w\sÁ-ÿ\u00C0-\u00FF]{1,45}" required
                                                onkeypress="return !(event.charCode >= 48 && event.charCode <= 57)"
                                                oninvalid="setCustomValidity('El nombre es obligatorio y debe tener como máximo 45 carácteres')"
                                                oninput="setCustomValidity('')" >
                                            </div>
                                        </div>

                                        <!-- Apellido Paterno del proveedor  -->
                                        <div class="form-group row">
                                          <label for="apellidopat" class="col-md-4 col-form-label text-md-right">{{ __('Apellido paterno ') }}<strong>*</strong></label>
                                          <div class="col-md-6">
                                                <input type="text" id="apellidopat" name="apellidopat" placeholder="Ingrese el apellido paterno del contacto" class="form-control"
                                                value="{{ old('apellidopat') }}" maxlength="45" minlength="1" required pattern="[\w\sÁ-ÿ\u00C0-\u00FF]{1,45}" required
                                                onkeypress="return !(event.charCode >= 48 && event.charCode <= 57)"
                                                oninvalid="setCustomValidity('El apellido paterno es obligatorio y debe tener como máximo 45 carácteres')"
                                                oninput="setCustomValidity('')" >
                                            </div>
                                        </div>

                                        <!-- Apellido Materno del proveedor  -->
                                        <div class="form-group row">
                                            {!! Form::label( 'apellidomat','Apellido materno', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                            <div class="col-md-6">
                                                <input type="text" id="apellidomat" name="apellidomat" placeholder="Ingrese el apellido materno del proveedor" class="form-control"
                                                value="{{ old('apellidomat') }}" maxlength="45" minlength="1" pattern="[\w\sÁ-ÿ\u00C0-\u00FF]{1,45}"
                                                onkeypress="return !(event.charCode >= 48 && event.charCode <= 57)"
                                                oninvalid="setCustomValidity('El apellido materno es obligatorio y debe tener como máximo 45 carácteres')"
                                                oninput="setCustomValidity('')" >
                                            </div>
                                        </div>

                                        <!-- Teléfono del proveedor  -->
                                        <div class="form-group row">
                                          <label for="telefono" class="col-md-4 col-form-label text-md-right">{{ __('Teléfono del contacto ') }}<strong>*</strong></label>
                                          <div class="col-md-6">
                                                <input type="text" id="telefono" name="telefono" placeholder="Ingrese el número telefónico del proveedor" class="form-control"
                                                value="{{ old('telefono') }}" maxlength="12" minlength="10" required attern="[\d]{1,12}"
                                                onkeypress="return (event.charCode >= 48 && event.charCode <= 57)"
                                                oninvalid="setCustomValidity('El telefono es obligatorio y debe tener como máximo 12 digitos')"
                                                oninput="setCustomValidity('')" >
                                            </div>
                                        </div>

                                        <!-- Correo del proveedor  -->
                                        <div class="form-group row">
                                            {!! Form::label( 'correo','Correo del proveedor', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                            <div class="col-md-6">
                                                <input type="text" id="correo" name="correo" placeholder="Ingrese el correo electrónico del proveedor" class="form-control"
                                                value="{{session('correo')}}" maxlength="45" minlength="1" pattern="^[A-Za-z0-9]+(.[A-Za-z0-9]+)*@([A-Za-z0-9_])+(.[A-Za-z0-9]+)*(.[A-Za-z]{2,})$"
                                                oninvalid="setCustomValidity('El correo electrónico es obligatorio y debe tener como máximo 45 carácteres')"
                                                oninput="setCustomValidity('')" >
                                            </div>
                                        </div>

                                    </fieldset>


                                    <!-- DATOS DEL DOMICILIO -->
                                    <fieldset class="border p-2">
                                          <legend class="w-auto"> Datos de la ubicación </legend>

                                            <!-- Código Postal -->
                                            <div class="form-group row" >
                                                <label for="codigop" class="col-md-4 col-form-label text-md-right">{{ __('Código Postal ') }}<strong>*</strong></label>
                                                <div class="col-md-6">
                                                <input type="text" id="codigop" name="codigop"  placeholder="Ingrese el código postal del domicilio" class="form-control"
                                                    value="{{ old('codigop') }}" maxlength="5" minlength="1" required pattern="[\d]{1,5}"
                                                    oninvalid="setCustomValidity('El código postal del domicilio es obligatorio y debe tener como máximo 5 digitos')"
                                                    oninput="setCustomValidity('')"
                                                    onfocusout="javascript:consultar()"
                                                    >
                                                </div>
                                            </div>

                                            <!-- Calle -->
                                            <div class="form-group row" >
                                                {!! Form::label( 'calle','Nombre de la calle', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                                <div class="col-md-6">
                                                <input type="text" id="calle" name="calle"  placeholder="Ingrese el nombre de la calle" class="form-control"
                                                    value="{{ old('calle') }}" maxlength="45" minlength="1"  pattern="[\w\sÁ-ÿ\u00C0-\u00FF]{1,45}"
                                                    oninvalid="setCustomValidity('La nombre de la calle es obligatorio y debe tener como máximo 45 carácteres')"
                                                    oninput="setCustomValidity('')">
                                                </div>
                                            </div>

                                            <!-- Número -->
                                            <div class="form-group row" >
                                                {!! Form::label( 'numero','Número del domicilio', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                                <div class="col-md-6">
                                                <input type="number" id="numero" name="numero"  placeholder="Ingrese el número del domicilio" class="form-control"
                                                    value="{{ old('numero') }}" min="1" max="9999"  pattern="[\d]{1,4}"
                                                    oninvalid="setCustomValidity('El número del domicilio es obligatorio y debe tener como máximo 4 digitos')"
                                                    oninput="setCustomValidity('')">
                                                </div>
                                            </div>

                                            <!-- Colonia -->
                                            <div class="form-group row" >
                                                <label for="colonia" class="col-md-4 col-form-label text-md-right">{{ __('Nombre de la colonia ') }}<strong>*</strong></label>
                                                <div class="col-md-6">
                                                    <select id="colonia" name="colonia" class="form-control" required>
                                                        <option value = ""> Seleccione una colonia </option>
                                                    </select>
                                                </div>
                                            </div>

                                            <!-- Municipio -->
                                            <div class="form-group row" >
                                                {!! Form::label( 'municipio','Nombre del municipio', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                                <div class="col-md-6">
                                                <input type="text" id="municipio" name="municipio"  placeholder="Ingrese el nombre del municipio" class="form-control"
                                                    value="{{ old('municipio') }}" maxlength="45" minlength="1"  pattern="[\w\sÁ-ÿ\u00C0-\u00FF]{1,45}"
                                                    oninvalid="setCustomValidity('El nombre del municipio es obligatorio y debe tener como máximo 45 carácteres')"
                                                    oninput="setCustomValidity('')">
                                                </div>
                                            </div>

                                            <!-- Ciudad -->
                                            <div class="form-group row" >
                                                {!! Form::label( 'ciudad','Nombre de la ciudad', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                                <div class="col-md-6">
                                                <input type="text" id="ciudad" name="ciudad"  placeholder="Ingrese el nombre de la ciudad" class="form-control"
                                                    value="{{ old('ciudad') }}" maxlength="45" minlength="1"  pattern="[\w\sÁ-ÿ\u00C0-\u00FF]{1,45}"
                                                    oninvalid="setCustomValidity('El nombre de la ciudad es obligatorio y debe tener como máximo 45 carácteres')"
                                                    oninput="setCustomValidity('')">
                                                </div>
                                            </div>

                                    </fieldset>

                                    <br>

                                    <div class="form-group row">
                                        <div class="col-md-4 col-form-label text-md-right">
                                            <button name="botonformproveedor" value="Registrar" type="submit" class="btn btn-success" style="margin-right:50px">Registrar</button>
                                        </div>
                                        <div class="col-md-4 col-form-label text-md-right">
                                            <button name="botonformproveedor" value="Cancelar" type="submit" class="btn btn-danger" style="margin-left:250px" formnovalidate>Cancelar</button>
                                        </div>
                                    </div>

                                {!!Form::close()!!}

                            </div>
                    </div>
                </div>
            </div>
        </div>

    @endsection

@endif
