@extends('home')

    @section('title')
        Listado de proveedores
    @endsection

@section('contenido')


    <link rel="STYLESHEET" href="css/estilostooltipylistados.css">


    <script>

        function FiltrarTabla() {
            
            if( document.getElementById("nombrefiltrado").value != "" ) {
                
                RealizarFiltradoTabla( document.getElementById("nombrefiltrado").value );

            } else {
                        
                document.getElementById("textoalerta").value = "Por favor ingrese el nombre a buscar";
                document.getElementById("contentalert").style.display = "block";

            }

        }

        function RealizarFiltradoTabla( nombre ) {
        
            var arrayregistroproveedores = {!! json_encode( $registroproveedores->toArray(), JSON_HEX_TAG ) !!};
            var niveluser = {{ ( Auth::user()->Nivel ) }};

            VaciarTabla();

            var auxnombreproveedor = "";

            for( var i = 0; i < arrayregistroproveedores.length; i++ ) {

                auxnombreproveedor = arrayregistroproveedores[i].nombrecompleto;
                auxnombreproveedor = auxnombreproveedor.toUpperCase();
                nombre = nombre.toUpperCase();

                if( auxnombreproveedor.includes( nombre ) ) {

                    if( niveluser == 1 ) {

                        document.getElementById("tablaproveedores").insertRow(-1).innerHTML = 
                            "<td>" + arrayregistroproveedores[i].idProveedor + "</td>" +
                            "<td>" + arrayregistroproveedores[i].Rfc + "</td>" +
                            "<td>" + arrayregistroproveedores[i].nombrecompleto + "</td>" +
                            "<td>" + arrayregistroproveedores[i].Empresa + "</td>" +
                            "<td>" + arrayregistroproveedores[i].Telefono + "</td>" +
                            "<td>" + 
                                "<a href=/modificarproveedor/" + arrayregistroproveedores[i].idProveedor + " }}>" +
                                    "<button type='submit' class='btn btn-info'> Modificar </button>" +
                                "</a>" +
                            "</td>" +
                            "<td>" +
                                "<a href=/descontinuarproveedor/" + arrayregistroproveedores[i].idProveedor + ">" +
                                    "<button type='submit' class='btn btn-danger'> Eliminar </button>" +
                                "</a>" +
                            "</td>";


                    } else if( niveluser == 2 ) {
                        
                        document.getElementById("tablaproveedores").insertRow(-1).innerHTML = 
                            "<td>" + arrayregistroproveedores[i].idProveedor + "</td>" +
                            "<td>" + arrayregistroproveedores[i].Rfc + "</td>" +
                            "<td>" + arrayregistroproveedores[i].nombrecompleto + "</td>" +
                            "<td>" + arrayregistroproveedores[i].Empresa + "</td>" +
                            "<td>" + arrayregistroproveedores[i].Telefono + "</td>";

                    }

                }
            
            }
            
            const tabla = document.getElementById('tablaproveedores');
            var rowCount = tabla.rows.length;

            if( rowCount == 1 ) {
                        
                document.getElementById("textoalerta").value = "Ningún proveedor coincide con el nombre buscado";
                document.getElementById("contentalert").style.display = "block";


            } else {
                
                document.getElementById("resultadofiltrado").innerHTML = "<p> Total de coincidencias: <strong>"+ ( rowCount - 1) + "</strong> </p>";

            }


        }

        function VaciarTabla() {

            document.getElementById("nombrefiltrado").value = "";
            document.getElementById("resultadofiltrado").innerHTML = "";

            const tabla = document.getElementById('tablaproveedores');
            var rowCount = tabla.rows.length;         

            for  ( var x = rowCount - 1; x > 0; x-- ) { 
                tabla.deleteRow(x); 
            } 

        }

        function RestablecerTabla() {
        
            var arrayregistroproveedores = {!! json_encode( $registroproveedores->toArray(), JSON_HEX_TAG ) !!};
            var niveluser = {{ ( Auth::user()->Nivel ) }};
            
            VaciarTabla();

            for( var i = 0; i < arrayregistroproveedores.length; i++ ) {
    
                if( niveluser == 1 ) {

                    document.getElementById("tablaproveedores").insertRow(-1).innerHTML = 
                        "<td>" + arrayregistroproveedores[i].idProveedor + "</td>" +
                        "<td>" + arrayregistroproveedores[i].Rfc + "</td>" +
                        "<td>" + arrayregistroproveedores[i].nombrecompleto + "</td>" +
                        "<td>" + arrayregistroproveedores[i].Empresa + "</td>" +
                        "<td>" + arrayregistroproveedores[i].Telefono + "</td>" +
                        "<td>" + 
                            "<a href=/modificarproveedor/" + arrayregistroproveedores[i].idProveedor + " }}>" +
                                "<button type='submit' class='btn btn-info'> Modificar </button>" +
                            "</a>" +
                        "</td>" +
                        "<td>" +
                            "<a href=/descontinuarproveedor/" + arrayregistroproveedores[i].idProveedor + ">" +
                                "<button type='submit' class='btn btn-danger'> Eliminar </button>" +
                            "</a>" +
                        "</td>";


                } else if( niveluser == 2 ) {
                    
                    document.getElementById("tablaproveedores").insertRow(-1).innerHTML = 
                        "<td>" + arrayregistroproveedores[i].idProveedor + "</td>" +
                        "<td>" + arrayregistroproveedores[i].Rfc + "</td>" +
                        "<td>" + arrayregistroproveedores[i].nombrecompleto + "</td>" +
                        "<td>" + arrayregistroproveedores[i].Empresa + "</td>" +
                        "<td>" + arrayregistroproveedores[i].Telefono + "</td>";

                }
            
            }

        }

        function OcultarAlerta() {

            if( document.getElementById("contentalert").style.display == "none" ) {
                document.getElementById("contentalert").style.display = "block";
            } else {
                document.getElementById("contentalert").style.display = "none";
            }

        }

    </script>
        
    @if(Session::has('success'))
        <div class="alert alert-success">
            {{session('success')}}
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        </div>
    @elseif( Session::has('warning'))
        <div class="alert alert-warning">
            {{session('warning')}}
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        </div>
    @elseif( Session::has('danger'))
        <div class="alert alert-danger">
            {{session('danger')}}
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        </div>
    @endif

        <!-- DIV FILTRADO -->
    <div class="container" style="margin-bottom: 10px;">
        
        <!-- DIV NOMBRE FILTRADO -->
        <div class="form-row align-items-center">
            
            <!-- NOMBRE ESPECIFICA -->
            <div class="col-auto" style="margin-left: 32%; margin-bottom:10px;">
                <label for="nombrefiltrado"> Ingrese el nombre a buscar </label>
                <input type="text" class="form-control-sm" id="nombrefiltrado" style="margin-left: 10px;">

                
                <label id="resultadofiltrado" style="margin-left: 20px;">  </label>
            </div>
            
        </div>


        <!-- Botones -->
        <div class="form-row align-items-center" style="margin-top: 2px; margin-bottom: 5px;">

            <a style="margin-left: 35%;"> <button  class="btn btn-primary" id="botonfiltrado"    onclick="javascript:FiltrarTabla();">Filtrar Tabla</button> </a>
            <a style="margin-left: 5%;">  <button  class="btn btn-primary" id="botonrestablecer" onclick="javascript:RestablecerTabla();">Restablecer Tabla</button> </a>

        </div>
        
        
    </div>

        <!-- Alerta -->
    <div class="container"  id="contentalert" style="display: none;">
        <div class="alert alert-danger alert-dismissible" role="alert" style="margin-top: 1%" >
            <input class="form-control-plaintext" value = "" id="textoalerta" disabled>    
            <button type="button" class="close" onclick="javascript:OcultarAlerta();" style="margin-top: 5px">
            <span>&times;</span>
            </button>
        </div>
    </div>



    <!-- Tabla -->
    <div class="container">
        <div class="row justify-content-center">

            <table class="table table-hover" id="tablaproveedores">
                <thead  class="thead-dark">
                    <tr>
                        <th scope="col>"> ID Proveedor </th>
                        <th scope="col>"> RFC </th>
                        <th scope="col>"> Nombre </th>
                        <th scope="col>"> Empresa </th>
                        <th scope="col>"> Teléfono</th>
                        <?php
                            if( Auth::user()->Nivel == 1 ) {
                        ?>
                            <th scope="col>" colspan="2"> Acciones </th>
                        <?php
                            }
                        ?>
                    </tr>
                </thead>
                <tbody>
                    @foreach ( $registroproveedores as $proveedores )
                        <tr>
                          <td>{{ $proveedores->idProveedor }}</td>
                          <td>{{ $proveedores->Rfc }}</td>
                          <td>{{ $proveedores->nombrecompleto }}</td>
                          <td>{{ $proveedores->Empresa }}</td>
                          <td>{{ $proveedores->Telefono }}</td>

                        <?php
                            if( Auth::user()->Nivel == 1 ) {
                        ?>
                            <td>
                                <a href="{{ url('/modificarproveedor/'.$proveedores->idProveedor ) }}">
                                    <button type="submit" class="btn btn-info"> Modificar </button>
                                </a>
                            </td>

                            <td>
                                <a href="{{ url('/descontinuarproveedor/'.$proveedores->idProveedor ) }}">
                                    <button type="submit" class="btn btn-danger"> Eliminar </button>
                                </a>
                            </td>
                        <?php
                            }
                        ?>

                    </tr>
                    @endforeach
                </tbody>
            </table>

        </div>
    </div>

@endsection
