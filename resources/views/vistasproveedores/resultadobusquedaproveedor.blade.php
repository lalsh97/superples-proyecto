@extends('home')

    @section('title')
        Resultado búsqueda de proveedor
    @endsection

@section('contenido')

        
    @if(Session::has('success'))
        <div class="alert alert-success">
            {{session('success')}}
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        </div>
    @elseif( Session::has('warning'))
        <div class="alert alert-warning">
            {{session('warning')}}
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        </div>
    @elseif( Session::has('danger'))
        <div class="alert alert-danger">
            {{session('danger')}}
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        </div>
    @endif



    <!-- Tabla -->
    <div class="container">
        <div class="row justify-content-center">

            <table class="table table-hover" id="tablaproveedores">
                <thead  class="thead-dark">
                    <tr>
                        <th scope="col>"> ID Proveedor </th>
                        <th scope="col>"> RFC </th>
                        <th scope="col>"> Nombre </th>
                        <th scope="col>"> Empresa </th>
                        <th scope="col>"> Telefono</th>
                        <?php
                            if( Auth::user()->Nivel == 1 ) {
                        ?>
                            <th scope="col>" colspan="2"> Acciones </th>
                        <?php
                            }
                        ?>
                    </tr>
                </thead>
                <tbody>
                    @foreach ( $registroproveedores as $proveedores )
                        <tr>
                          <td>{{ $proveedores->idProveedor }}</td>
                          <td>{{ $proveedores->Rfc }}</td>
                          <td>{{ $proveedores->nombrecompleto }}</td>
                          <td>{{ $proveedores->Empresa }}</td>
                          <td>{{ $proveedores->Telefono }}</td>

                        <?php
                            if( Auth::user()->Nivel == 1 ) {
                        ?>
                            <td>
                                <a href="{{ url('/modificarproveedor/'.$proveedores->idProveedor ) }}">
                                    <button type="submit" class="btn btn-info"> Modificar </button>
                                </a>
                            </td>

                            <td>
                                <a href="{{ url('/descontinuarproveedor/'.$proveedores->idProveedor ) }}">
                                    <button type="submit" class="btn btn-danger"> Eliminar </button>
                                </a>
                            </td>
                        <?php
                            }
                        ?>

                    </tr>
                    @endforeach
                </tbody>
            </table>

        </div>
    </div>

@endsection
