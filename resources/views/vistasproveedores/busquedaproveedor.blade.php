@extends('home')

    @section('title')
        Búsqueda de proveedor
    @endsection

@section('contenido')

    <script src="{{ asset('js/FuncionesProveedores/funcionesbusquedaproveedor.js') }}">
        //
    </script>

    @if(Session::has('success'))
        <div class="alert alert-success">
            {{session('success')}}
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        </div>
    @elseif( Session::has('warning'))
        <div class="alert alert-warning">
            {{session('warning')}}
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        </div>
    @elseif( Session::has('danger'))
        <div class="alert alert-danger">
            {{session('danger')}}
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        </div>
    @endif


    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    @if( $operacion == 'descontinuar' )
                        <div class="card-header"  align="center">{{ __('Búsqueda eliminar proveedor') }}</div>
                    @elseif( $operacion == 'buscar' )
                            <div class="card-header"  align="center">{{ __('Búsqueda de proveedores') }}</div>
                    @elseif( $operacion == 'modificar' )
                        <div class="card-header"  align="center">{{ __('Búsqueda modificar información de proveedor') }}</div>
                    @endif

                    <!-- DIV -->
                    <div class="card-body">

                            <!-- Buscar proveedor por rfc-->
                            <div class="form-group"  style="margin-bottom: 10px">
                                <div class="form-check"   style="margin-bottom: 10px">
                                    {!! Form::label( 'codigo','Buscar proveedor por rfc', array( 'class' => 'col-md-8 col-form-label', 'onclick' => 'cambiarcheckcodigo2(this);' ) ) !!}
                                </div>

                                <div class="form-group"  id="contentcodigo">
                                    <input type="text" id="rfc" name="rfc" class="form-control-sm" value="{{  old('rfc')  }}" style="margin-left: 35%">
                                </div>
                            </div>


                            <div class="form-group row" style="margin-top: 20px">
                                <div class="col-md-4 col-form-label text-md-right">
                                    @if( $operacion == 'descontinuar' )
                                        <button name="botonbusquedaproveedor" value="BusquedaDescontinuar" type="submit" class="btn btn-success" style="margin-right:50px" onclick="javascript:IrDescontinuarProveedor();">Buscar</button>
                                    @elseif( $operacion == 'buscar' )
                                        <button name="botonbusquedaproveedor" value="Buscar" type="submit" class="btn btn-success" style="margin-right:50px" onclick="javascript:IrBuscarProveedor();">Buscar</button>
                                    @elseif( $operacion == 'modificar' )
                                        <button name="botonbusquedaproveedor" value="BusquedaModificar" type="submit" class="btn btn-success" style="margin-right:50px" onclick="javascript:IrModificarProveedor();">Buscar</button>
                                    @endif
                                </div>
                                <div class="col-md-4 col-form-label text-md-right">
                                    <a>
                                        <button name="botonbusquedaproveedor" value="Cancelar" type="submit" class="btn btn-danger" style="margin-left:250px">Cancelar</button>
                                    </a>
                                </div>
                            </div>

                    </div>

                </div>
            </div>
        </div>
    </div>

@endsection
