@if( Auth::user()->Nivel == 1 )

    @extends('home')

    @section('title')
        Eliminación de prooveedor
    @endsection


    @section('contenido')

        <script>

            function MostrarModalConfirmacionEliminacion() {

                var idproveedor = document.getElementById("idproveedor").value;

                var ruta  = "{{ route('verificarproductosproveedor') }}";
                var url = ruta + "/" + idproveedor;

                //Realiza la búsqueda
                $.get( url ).done( function( data ) {
                    var content = JSON.parse( JSON.stringify( data ) );

                    //Verificar si el contenido de la respuesta de la api no esta vacía.
                    if( content.length > 0 ) {

                        var totalproductos = content[0].total;

                        var nombreproveedor = document.getElementById("nombre").value + " " + document.getElementById("apellidopat").value + " " + document.getElementById("apellidomat").value;

                        //El proveedor tiene productos asociados y se deben mostrar dos opciones para eliminar.
                        if( totalproductos > 0 ) {

                            document.getElementById("textoerrorbody").innerHTML =   "<p>El proveedor <strong> " + nombreproveedor + "</strong> ,aún tiene <strong> " + totalproductos + "</strong> productos asociados</p>" +
                                                                                    "<p>¿Está seguro que desea eliminarlo?</p>" +
                                                                                    "<br>" +
                                                                                    "<p style='font-size:16px; text-align: left;'><strong> OPCIONES:</strong></p>" +
                                                                                    "<p style='text-align: left;'> <strong>Transferir</strong> Todos los productos asociados a este proveedor serán transferidos al <strong> proveedor general </strong> </p>" +
                                                                                    "<p style='text-align: left;'> <strong>Eliminar</strong> Tanto el proveedor como sus productos asociados serán eliminados </p>";

                            document.getElementById("botonTransferir").style.display = "block";

                            document.getElementById("botonCancelar").style = "margin-left: 42%;";
                            document.getElementById("botonEliminar").style = "margin-right: 15%;";

                        } else if( totalproductos <= 0 ) {//El proveedor no tiene productos asociados y se debe mostrar la opción default para eliminar.


                            document.getElementById("textoerrorbody").innerHTML =   "<p>¿Está seguro que desea eliminar al proveedor: <strong>" + nombreproveedor + "</strong> ? </p>";


                            document.getElementById("botonTransferir").style.display = "none";

                            document.getElementById("botonCancelar").style = "margin-left: 72%;";

                        }

                        $('#ModalConfirmacionEliminacion').modal('show');

                    } else {

                    }

                }).catch(function( error) {
                    console.log("El error es: ", error);
                });

            }

            function DismissModalConfirmacionEliminacion() {

                $("#ModalConfirmacionEliminacion").modal('hide');

            }

            function EliminarTransferirProductos() {

                $("#ModalConfirmacionEliminacion").modal('hide');


                var idproveedor = document.getElementById("idproveedor").value;

                location.href = "/transferircambiarestadoproveedor/" + idproveedor;

            }

            function EliminarProveedor() {

                $("#ModalConfirmacionEliminacion").modal('hide');

                var idproveedor = document.getElementById("idproveedor").value;

                location.href ="/eliminarcambiarestadoproveedor/" + idproveedor;

            }

            function Regresar() {
                location.href = "/home";
            }

        </script>


        @if(Session::has('success'))
            <div class="alert alert-success">
                {{session('success')}}
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            </div>
        @elseif(Session::has('danger'))
            <div class="alert alert-danger">
                {{session('danger')}}
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            </div>
        @elseif(Session::has('warning'))
            <div class="alert alert-warning">
                {{session('warning')}}
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            </div>
        @endif

        <!-- INFORMACION PROVEEDOR -->
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header"  align="center">{{ __('Eliminación del proveedor') }}</div>

                        <div class="card-body">

                            {!!Form::open(array('name' => 'formprove', 'url'=>'cambiarestadoproveedor/'.$proveedor->idProveedor,'method'=>'PUT' ,'autocomplete'=>'off'))!!}

                                <fieldset  class="border p-2">
                                    <legend  class="w-auto"> Datos personales </legend>

                                        <!-- ID proveedor  -->
                                        <div class="form-group row" style="display: none;">
                                            {!! Form::label( 'nombre','Nombre de la empresa', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                            <div class="col-md-6">
                                                <input type="text" id="idproveedor" name="idproveedor" class="form-control" value="{{ ( $proveedor->idProveedor) }}" readonly >
                                            </div>
                                        </div>

                                        <!-- Nombre de la empresa  -->
                                        <div class="form-group row">
                                            {!! Form::label( 'nombreempresa','Nombre de la empresa', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                            <div class="col-md-6">
                                                <input type="text" id="nombreempresa" name="nombreempresa" class="form-control" value="{{ ( $proveedor->Empresa) }}" readonly >
                                            </div>
                                        </div>

                                        <!-- Nombre del proveedor  -->
                                        <div class="form-group row">
                                            {!! Form::label( 'nombre','Nombre del contacto', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                            <div class="col-md-6">
                                                <input type="text" id="nombre" name="nombre" class="form-control" value="{{ ( $proveedor->Nombre) }}" readonly >
                                            </div>
                                        </div>

                                        <!-- Apellido Paterno del proveedor  -->
                                        <div class="form-group row">
                                            {!! Form::label( 'apellidopat','Apellido paterno', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                            <div class="col-md-6">
                                                <input type="text" id="apellidopat" name="apellidopat" class="form-control" value="{{($proveedor->ApellidoPat)}}" readonly >
                                            </div>
                                        </div>

                                        <!-- Apellido Materno del proveedor  -->
                                        <div class="form-group row">
                                            {!! Form::label( 'apellidomat','Apellido materno', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                            <div class="col-md-6">
                                                <input type="text" id="apellidomat" name="apellidomat" class="form-control"value="{{($proveedor->ApellidoMat)}}" readonly>
                                            </div>
                                        </div>

                                        <!-- Teléfono del proveedor  -->
                                        <div class="form-group row">
                                            {!! Form::label( 'telefono','Teléfono del contacto', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                            <div class="col-md-6">
                                                <input type="text" id="telefono" name="telefono" placeholder="Ingrese el número telefónico del proveedor" class="form-control"
                                                    value="{{($proveedor->Telefono)}}" maxlength="12" minlength="1" pattern="[\d]{1,12}"
                                                    oninvalid="setCustomValidity('El telefono es obligatorio y debe tener como máximo 12 digitos')"
                                                    oninput="setCustomValidity('')"readonly >
                                            </div>
                                        </div>

                                        <!-- R.F.C. del proveedor  -->
                                        <div class="form-group row">
                                            {!! Form::label( 'rfc','RFC del proveedor', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                            <div class="col-md-6">
                                                <input type="text" id="rfc" name="rfc"  class="form-control" value="{{($proveedor->Rfc)}}" readonly >
                                            </div>
                                        </div>

                                        <!-- Correo del proveedor  -->
                                        <div class="form-group row">
                                            {!! Form::label( 'correo','Correo del contacto', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                            <div class="col-md-6">
                                                <input type="text" id="correo" name="correo" placeholder="Ingrese el correo electrónico del proveedor" class="form-control"
                                                    value="{{($proveedor->Correo)}}" maxlength="45" minlength="1" pattern="[A-Za-z\s\W]{1,45}"
                                                    oninvalid="setCustomValidity('El correo electrónico es obligatorio y debe tener como máximo 45 carácteres')"
                                                    oninput="setCustomValidity('')" readonly >
                                            </div>
                                        </div>

                                </fieldset>

                            {!!Form::close()!!}

                            <!-- BOTONES -->
                            <div class="form-group row">
                                <div class="col-md-4 col-form-label text-md-right">
                                    <button name="botonformdescontinuar" value="Descontinuar" type="submit" class="btn btn-danger" style="margin-right:50px" formnovalidate onclick="javascript:MostrarModalConfirmacionEliminacion();">Eliminar</button>
                                </div>
                                <div class="col-md-4 col-form-label text-md-right">
                                    <button name="botonformdescontinuar" value="Cancelar" type="submit" class="btn btn-info" style="margin-left:250px" formnovalidate onclick="javascript:Regresar();">Cancelar</button>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- ModalConfirmacionEliminacion -->
        <div class="modal" tabindex="-1" role="dialog" id="ModalConfirmacionEliminacion">
            <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                <div class="modal-content modal-lg">

                    <!--Header-->
                    <div class="modal-header" id="modalheader" style="background: rgb(33, 37, 41)">
                        <h5 id="textostringheader" class="heading" style="margin-left: 25%; color: white; text-align: center;">CONFIRMACIÓN DE ELIMINACIÓN</h5>

                        <button type="button" class="close" onclick="javascript:DismissModalConfirmacionEliminacion();" data-dismiss="modal" aria-label="Close">
                            <span  aria-hidden="true" style="color: rgb(255, 255, 255)" >&times;</span>
                        </button>
                    </div>

                    <!--Body-->
                    <div class="modal-body" id="modalbody">
                        <div class="row">
                            <div class="col">
                                <p id="textoerrorbody" style="text-align: center;"></p>
                            </div>
                        </div>

                    </div>

                    <!--Footer-->
                    <div class="modal-footer" id="modalfooter">
                        <button type="button" class="btn btn-primary" data-dismiss="modal" id="botonTransferir" style="display: none;" onclick="javascript:EliminarTransferirProductos();">TRANSFERIR</button>
                        <button type="button" class="btn btn-primary" data-dismiss="modal" id="botonEliminar" onclick="javascript:EliminarProveedor();">ELIMINAR</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal" id="botonCancelar" onclick="javascript:DismissModalConfirmacionEliminacion();" style="margin-left: 72%;">CANCELAR</button>
                    </div>

                </div>
            </div>
        </div>

        <br><br><br>

    @endsection

@endif
