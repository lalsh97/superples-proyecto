@extends('layouts.app')

    <style>
        strong {

            color: red;
            font-size: 20px;

        }

    </style>

    <script>

        window.onload = function( e ) {
            
            var fechacasolimite = new Date();                
            fechacasolimite.setFullYear( fechacasolimite.getFullYear() - 18 );
            fechacasolimite = new Date( fechacasolimite ).toISOString().split("T")[0];
            
            //console.log("FECHA: ", fechacasolimite );                
            document.getElementById('fechanac').max = fechacasolimite;

        }

        function generarcurp() {

            var curpgenerada = "";

            if( $('#nombre').val() != "" && $('#apellidopat').val() != ""  ) {

                var primeraletraapellidopat;
                var vocalapellidopat;
                var primeraletraapellidomat;
                var primeraletranombre;
                var fechanacimientodigitos;
                var caractersexo;
                var entidaddenacimiento;
                var consonanteinternaapellidopat;
                var consonanteinternaapellidomat;
                var consonanteinternanombre;
                var  pcl = ""; // primerascuatroletras
                var expreg = /(BACA|BAKA|BUEI|BUEY|CACA|CACO|CAGA|CAGO|CAKA|CAKO|COGE|COGI|COJA|COJE|COJI|COJO|COLA|CULO|FALO|FETO|GETA|GUEI|GUEY|JETA|JOTO|KACA|KACO|KAGA|KAGO|KAKA|KAKO|KOGE|KOGI|KOJA|KOJE|KOJI|KOJO|KOLA|KULO|LILO|LOCA|LOCO|LOKA|LOKO|MAME|MAMO|MEAR|MEAS|MEON|MIAR|MION|MOCO|MOKO|MULA|MULO|NACA|NACO|PEDA|PEDO|PENE|PIPI|PITO|POPO|PUTA|PUTO|QULO|RATA|ROBA|ROBE|ROBO|RUIN|SENO|TETA|VACA|VAGA|VAGO|VAKA|VUEI|VUEY|WUEI|WUEY)/;

                var textonombre = document.getElementById('nombre').value.toUpperCase();
                    textonombre = textonombre.replace("Á","A");
                    textonombre = textonombre.replace("É","E");
                    textonombre = textonombre.replace("Í","I");
                    textonombre = textonombre.replace("Ó","O");
                    textonombre = textonombre.replace("Ú","U");

                var textoapellidopat = document.getElementById('apellidopat').value.toUpperCase();
                    textoapellidopat = textoapellidopat.replace("Á","A");
                    textoapellidopat = textoapellidopat.replace("É","E");
                    textoapellidopat = textoapellidopat.replace("Í","I");
                    textoapellidopat = textoapellidopat.replace("Ó","O");
                    textoapellidopat = textoapellidopat.replace("Ú","U");

                var textoapellidomat = document.getElementById('apellidomat').value.toUpperCase();
                    textoapellidomat = textoapellidomat.replace("Á","A");
                    textoapellidomat = textoapellidomat.replace("É","E");
                    textoapellidomat = textoapellidomat.replace("Í","I");
                    textoapellidomat = textoapellidomat.replace("Ó","O");
                    textoapellidomat = textoapellidomat.replace("Ú","U");

                        // PRIMER CARACTER - Primera letra primer apellido.
                var auxletra = textoapellidopat.substr(0,1);
                if( auxletra == 'Ñ' ) {
                    primeraletraapellidopat = 'X';
                } else {
                    primeraletraapellidopat = textoapellidopat.substr(0,1);
                }

                        //  SEGUNDO CARACTER - Primera vocal interna del primer apellido.
                for( var i = 1; i< textoapellidopat.length; i++ ) {
                    if( textoapellidopat.charAt(i) == 'A' || textoapellidopat.charAt(i) == 'E' || textoapellidopat.charAt(i) == 'I' || textoapellidopat.charAt(i) == 'O' || textoapellidopat.charAt(i) == 'U' ) {
                            vocalapellidopat = textoapellidopat.charAt(i);
                            break;
                    }
                }

                if( vocalapellidopat == null ) {
                    vocalapellidopat = 'X';
                }

                        //  TERCER CARACTER - Primera letra del segundo apellido.
                if( textoapellidomat != "" ) {
                    auxletra = textoapellidomat.substr(0,1);
                    if( auxletra == 'Ñ' ) {
                        primeraletraapellidomat = 'X';
                    } else {
                        primeraletraapellidomat = textoapellidomat.substr(0,1);
                    }
                } else {
                    primeraletraapellidomat = 'X';
                }


                        //  CUARTO CARACTER - Primera letra del nombre
                var arraynombres = textonombre.split(" ");

                if( arraynombres.length > 1 ) {
                    var nombreingresado = arraynombres[0];
                    if( nombreingresado == 'MARIA' || nombreingresado == 'JOSE' ) {
                        auxletra = arraynombres[1].substr(0,1);
                        if( auxletra == 'Ñ' ) {
                            primeraletranombre = 'X';
                        } else {
                            primeraletranombre =  arraynombres[1].substr(0,1);
                        }
                    } else {
                        auxletra = arraynombres[0].substr(0,1);
                        if( auxletra == 'Ñ' ) {
                            primeraletranombre = 'X';
                        } else {
                            primeraletranombre = arraynombres[0].substr(0,1);
                        }
                    }
                } else {
                    auxletra = arraynombres[0].substr(0,1);
                    if( auxletra == 'Ñ' ) {
                        primeraletranombre = 'X';
                    } else {
                        primeraletranombre = arraynombres[0].substr(0,1);
                    }
                }

                pcl = pcl.concat (primeraletraapellidopat, vocalapellidopat,
                    primeraletraapellidomat, primeraletranombre );

                var resexpreg = expreg.test( pcl );
                if( resexpreg ) {
                    pcl = "";
                    pcl = pcl.concat (primeraletraapellidopat, "X",
                    primeraletraapellidomat, primeraletranombre );
                }


                        // QUINTO AL DECIMO CARACTER - año, mes, día.
                //fecha = document.getElementById('fechanac').value;
                fecha = $('#fechanac').val();

                arrayfecha = fecha.split("-");

                digitoanio = arrayfecha[0];
                digitoanio = digitoanio.substr( 2, digitoanio.length );
                digitomes = arrayfecha[1];
                digitodia = arrayfecha[2];

                if( fecha == "" ) {
                    fechanacimientodigitos = "******";
                } else {
                    fechanacimientodigitos = "";
                    fechanacimientodigitos = fechanacimientodigitos.concat( digitoanio, digitomes, digitodia );
                }

                    //ONCEAVO CARACTER - H -> hombre / M -> mujer

                if( document.getElementById('sexoh').checked == true ) {
                    caractersexo = "H";
                } else if( document.getElementById('sexom').checked == true ) {
                    caractersexo = "M";
                } else {
                    caractersexo = "-";
                }

                        //  DOCEAVO Y TRECEAVO CARACTER - Letra inicial y última consonante del estado de nacimiento.
                entidaddenacimiento = 'OC';

                        //CATORCEAVO CARACTER - Primera consonante interna del primer apellido
                for( var i = 1; i< textoapellidopat.length; i++ ) {
                    if( textoapellidopat.charAt(i) == 'A' || textoapellidopat.charAt(i) == 'E' || textoapellidopat.charAt(i) == 'I' || textoapellidopat.charAt(i) == 'O' || textoapellidopat.charAt(i) == 'U' ) {
                    } else {
                        consonanteinternaapellidopat = textoapellidopat.charAt(i);
                        break;

                    }
                }
                if( consonanteinternaapellidopat == null || consonanteinternaapellidopat == 'Ñ' ) {
                    consonanteinternaapellidopat = 'X';
                }

                        //QUINCEAVO CARACTER - Primera consonante interna del segundo apellido
                for( var i = 1; i< textoapellidomat.length; i++ ) {
                    if( textoapellidomat.charAt(i) == 'A' || textoapellidomat.charAt(i) == 'E' || textoapellidomat.charAt(i) == 'I' || textoapellidomat.charAt(i) == 'O' || textoapellidomat.charAt(i) == 'U' ) {
                    } else {
                        consonanteinternaapellidomat = textoapellidomat.charAt(i);
                        break;
                    }
                }
                if( consonanteinternaapellidomat == null || consonanteinternaapellidomat == 'Ñ' ) {
                    consonanteinternaapellidomat = 'X';
                }

                        //DIECISEISAVO CARACTER - Primera consonante interna del nombre
                arraynombres = textonombre.split(" ");

                if( arraynombres.length > 1 ) {
                    var nombreingresado = arraynombres[0];
                    if( nombreingresado == 'MARIA' || nombreingresado == 'JOSE' ) {
                        auxnombre = arraynombres[1];
                        var contador = 0;
                        for( var i = 0; i< auxnombre.length; i++ ) {
                            if( auxnombre.charAt(i) == 'A' || auxnombre.charAt(i) == 'E' || auxnombre.charAt(i) == 'I' || auxnombre.charAt(i) == 'O' || auxnombre.charAt(i) == 'U' ) {
                            } else {
                                contador++;
                                if( contador == 1 ) {
                                    consonanteinternanombre = auxnombre.charAt(i);
                                    break;
                                }
                            }
                        }
                    } else {
                        for( var i = 1; i< textonombre.length; i++ ) {
                            if( textonombre.charAt(i) == 'A' || textonombre.charAt(i) == 'E' || textonombre.charAt(i) == 'I' || textonombre.charAt(i) == 'O' || textonombre.charAt(i) == 'U' ) {
                            } else {
                                consonanteinternanombre = textonombre.charAt(i);
                                break;
                            }
                        }
                    }
                } else {
                    for( var i = 1; i< textonombre.length; i++ ) {
                        if( textonombre.charAt(i) == 'A' || textonombre.charAt(i) == 'E' || textonombre.charAt(i) == 'I' || textonombre.charAt(i) == 'O' || textonombre.charAt(i) == 'U' ) {
                        } else {
                            consonanteinternanombre = textonombre.charAt(i);
                            break;
                        }
                    }
                }

                if( consonanteinternanombre == null || consonanteinternanombre == 'Ñ' ) {
                    consonanteinternanombre = 'X';
                }


                curpgenerada = curpgenerada.concat( pcl, fechanacimientodigitos,
                    caractersexo, entidaddenacimiento, consonanteinternaapellidopat,
                    consonanteinternaapellidomat, consonanteinternanombre,"00" );

                if( curpgenerada.includes("-") ) {
                    curpgenerada = "";
                } else {
                    document.getElementById('curp').value = curpgenerada;
                    mayusculas();
                }

            }

        }

        function generarrfc() {

            var rfcgenerado = "";

            if( $('#nombre').val() != "" && $('#apellidopat').val() != ""  ) {

                var primeraletraapellidopat;
                var segundaletraapellidopat;
                var primeraletraapellidomat;
                var primeraletranombre;
                var fechanacimientodigitos;
                var primerasletrasrazonsocial;

                var textonombre = document.getElementById('nombre').value.toUpperCase();
                    textonombre = textonombre.replace("Á","A");
                    textonombre = textonombre.replace("É","E");
                    textonombre = textonombre.replace("Í","I");
                    textonombre = textonombre.replace("Ó","O");
                    textonombre = textonombre.replace("Ú","U");

                var textoapellidopat = document.getElementById('apellidopat').value.toUpperCase();
                    textoapellidopat = textoapellidopat.replace("Á","A");
                    textoapellidopat = textoapellidopat.replace("É","E");
                    textoapellidopat = textoapellidopat.replace("Í","I");
                    textoapellidopat = textoapellidopat.replace("Ó","O");
                    textoapellidopat = textoapellidopat.replace("Ú","U");

                var textoapellidomat = document.getElementById('apellidomat').value.toUpperCase();
                    textoapellidomat = textoapellidomat.replace("Á","A");
                    textoapellidomat = textoapellidomat.replace("É","E");
                    textoapellidomat = textoapellidomat.replace("Í","I");
                    textoapellidomat = textoapellidomat.replace("Ó","O");
                    textoapellidomat = textoapellidomat.replace("Ú","U");

                //Personas físicas 13 carácteres ( 4 de iniciales de apellidos y nombre, 6 fecha y 3 homoclave ).
                //Personas morales 12 carácteres( 4 -> "Primeras letras de su razón social", 6 fecha, 3 homoclave ).


                    // PRIMER Y SEGUNDO CARACTER - Primeras dos letras del apellido paterno.
                primeraletraapellidopat = textoapellidopat.substr(0,1);
                segundaletraapellidopat = textoapellidopat.substr(1,1);


                        //  TERCER CARACTER - Primera letra del apellido materno.
                primeraletraapellidomat = textoapellidomat.substr(0,1);


                        //  CUARTO CARACTER - Primera letra del nombre
                primeraletranombre = textonombre.substr(0,1);


                        // QUINTO AL DECIMO CARACTER - año, mes, día.
                //fecha = document.getElementById('fechanac').value;
                fecha = $('#fechanac').val();

                arrayfecha = fecha.split("-");

                digitoanio = arrayfecha[0];
                digitoanio = digitoanio.substr( 2, digitoanio.length );
                digitomes = arrayfecha[1];
                digitodia = arrayfecha[2];

                if( fecha == "" ) {
                    fechanacimientodigitos = "******";
                } else {
                    fechanacimientodigitos = "";
                    fechanacimientodigitos = fechanacimientodigitos.concat( digitoanio, digitomes, digitodia );
                }

                rfcgenerado = rfcgenerado.concat( primeraletraapellidopat, segundaletraapellidopat,
                primeraletraapellidomat, primeraletranombre, fechanacimientodigitos, "000");

                if( rfcgenerado.includes("-") ) {
                    rfcgenerado = "";
                } else {
                    document.getElementById('rfc').value = rfcgenerado;
                    mayusculas();
                }

            }

        }

        function mayusculas() {

            var textocurp = document.getElementById('curp').value;
            textocurp = textocurp.toUpperCase();

            var textorfc = document.getElementById('rfc').value;
            textorfc = textorfc.toUpperCase();

            document.getElementById('curp').value = textocurp;
            document.getElementById('rfc').value = textorfc;

        }

        function consultar() {

            var endpoint_sepomex  = "http://api-sepomex.hckdrk.mx/query/";
            var method_sepomex = 'info_cp/';
            //var cp = "09810";
            var cp = document.getElementById('codigop').value;
            var variable_string = '?type=simplified';
            var url = endpoint_sepomex + method_sepomex + cp + variable_string;

            $.get( url ).done( function( data ) {
                var content = JSON.parse( JSON.stringify( data ) );

                //console.log( content );
                //document.getElementById('colonia').value = content.response.asentamiento;
                var opcionescolonia = content.response.asentamiento;

                var selectcolonias = document.getElementById( 'colonia' );

                for ( var i = selectcolonias.length; i >= 0; i--) {
                    selectcolonias.remove(i);
                }

                if( opcionescolonia.length > 0 ) {

                    var optioncolonia = document.createElement('option');
                            optioncolonia.text = "Seleccione una colonia";
                            optioncolonia.value = "";
                            selectcolonias.add( optioncolonia );

                    for( var i = 0; i < opcionescolonia.length; i++ ) {
                        var optioncolonia = document.createElement( 'option' );
                            optioncolonia.text = opcionescolonia[i];
                            optioncolonia.value = opcionescolonia[i];
                            selectcolonias.add( optioncolonia );

                    }

                }

                if( content.response.ciudad == "" && content.response.municipio != "" ) {

                    document.getElementById('ciudad').value = "Oaxaca de Juarez";
                    document.getElementById('municipio').value = content.response.municipio;

                } else if( content.response.ciudad != "" && content.response.municipio != "" ) {

                    document.getElementById('ciudad').value = content.response.ciudad;
                    document.getElementById('municipio').value = content.response.municipio;

                }
            }).catch(function( error) {

                var selectcolonias = document.getElementById( 'colonia' );

                for ( var i = selectcolonias.length; i >= 0; i--) {
                    selectcolonias.remove(i);
                }

                var optioncolonia = document.createElement('option');
                    optioncolonia.text = "Seleccione una colonia";
                    optioncolonia.value = "";
                    selectcolonias.add( optioncolonia );

                optioncolonia = document.createElement('option');
                    optioncolonia.text = "DOMICILIO CONOCIDO";
                    optioncolonia.value = "DOMICILIO CONOCIDO";
                    selectcolonias.add( optioncolonia );

                document.getElementById('ciudad').value = "";
                document.getElementById('municipio').value = "";

            });

        }

    </script>


@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header" align="center" >{{ __('Registro de usuario') }}</div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('register') }}" align="center" enctype="multipart/form-data" autocomplete="off">
                            @csrf

                            <!-- NOMBRE DE USUARIO -->
                            <div class="form-group row">
                                <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Nombre de usuario') }}<strong>*</strong></label>

                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required
                                        placeholder="Ingrese el nombre de usuario"
                                        oninvalid="setCustomValidity('El nombre de usuario es obligatorio')"
                                        oninput="setCustomValidity('')" autocomplete="name" autofocus>

                                    @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>


                            <!-- Avatar  del usuario -->
                            <div class="form-group row">
                                {!! Form::label( 'avatar','Imagen del usuario', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                <div class="col-md-6">
                                <input type="file" id="avatar" name="avatar"  value="{{ old('avatar')}}" >
                                </div>
                            </div>


                            <!-- EMAIL -->
                            <div class="form-group row">
                                <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('Correo electrónico') }}<strong>*</strong></label>

                                <div class="col-md-6">
                                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}"
                                        placeholder="Ingrese el correo electrónico"
                                        oninvalid="setCustomValidity('El correo del usuario es un campo obligatorio')"
                                        oninput="setCustomValidity('')" required autocomplete="email">

                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <!-- PASSWORD -->
                            <div class="form-group row">
                                <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Contraseña') }}<strong>*</strong></label>

                                <div class="col-md-6">
                                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password"
                                        placeholder="Ingrese la contraseña"
                                        oninvalid="setCustomValidity('La contraseña es un campo obligatorio')"
                                        oninput="setCustomValidity('')" required autocomplete="new-password">

                                    @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <!-- CONFIRMAR PASSWORD -->
                            <div class="form-group row">
                                <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirmar contraseña') }}<strong>*</strong></label>

                                <div class="col-md-6">
                                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation"
                                        placeholder="Ingrese la confirmación de contraseña"
                                        oninvalid="setCustomValidity('La confirmación de la contraseña es un campo obligatorio')"
                                        oninput="setCustomValidity('')" required autocomplete="new-password">
                                </div>
                            </div>


                            <!-- Nivel empleado -->
                            <div class="form-group row">
                                <label for="nivel" class="col-md-4 col-form-label text-md-right">{{ __('Selecionar nivel de empleado') }}<strong>*</strong></label>

                                <div class="col-md-4">
                                    <select id="nivel" name="nivel"  placeholder="Seleccione una opción"  class="form-control"  required value="{{ old('nivel')}}"
                                        oninvalid="setCustomValidity('Debe seleccionar una opción')"
                                        oninput="setCustomValidity('')"  placeholder="Seleccione una opción" >
                                        <option value="1"> Administrador </option>
                                        <option value="2"> Vendedor </option>
                                    </select>
                                </div>
                            </div>



                            <!-- FIELSET DATOS PERSONALES -->
                            <fieldset class="border p-2">
                                <legend class="w-auto"> Datos personales </legend>

                                <!-- Nombre del empleado -->
                                <div class="form-group row" >
                                    <label for="nombre" class="col-md-4 col-form-label text-md-right">{{ __('Nombre del empleado') }}<strong>*</strong></label>
                                    <div class="col-md-6">
                                    <input type="text" id="nombre" name="nombre"  placeholder="Ingrese el nombre del empleado" class="form-control"
                                        value="{{ old('nombre') }}" maxlength="45" minlength="1" required pattern="[\w\sÁ-ÿ\u00C0-\u00FF]{1,45}"
                                        onkeypress="return !(event.charCode >= 48 && event.charCode <= 57)"
                                        oninvalid="setCustomValidity('El nombre es obligatorio y debe tener como máximo 45 carácteres')"
                                        oninput="setCustomValidity('')">
                                    </div>
                                </div>

                                <!-- Apellido Paterno del empleado -->
                                <div class="form-group row" >
                                    <label for="apellidopat" class="col-md-4 col-form-label text-md-right">{{ __('Apellido paterno del empleado') }}<strong>*</strong></label>
                                    <div class="col-md-6">
                                    <input type="text" id="apellidopat" name="apellidopat"  placeholder="Ingrese el apellido paterno del empleado" class="form-control"
                                        value="{{ old('apellidopat') }}" maxlength="45" minlength="1" required pattern="[\w\sÁ-ÿ\u00C0-\u00FF]{1,45}"
                                        onkeypress="return !(event.charCode >= 48 && event.charCode <= 57)"
                                        oninvalid="setCustomValidity('El apellido paterno es obligatorio y debe tener como máximo 45 carácteres')"
                                        oninput="setCustomValidity('')">
                                    </div>
                                </div>

                                <!-- Apellido Materno del empleado -->
                                <div class="form-group row" >
                                    {!! Form::label( 'apellidomat','Apellido materno del empleado', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                    <div class="col-md-6">
                                    <input type="text" id="apellidomat" name="apellidomat"  placeholder="Ingrese el apellido materno del empleado" class="form-control"
                                        value="{{ old('apellidomat') }}" maxlength="45" minlength="1" pattern="[\w\sÁ-ÿ\u00C0-\u00FF]{1,45}"
                                        onkeypress="return !(event.charCode >= 48 && event.charCode <= 57)"
                                        oninvalid="setCustomValidity('El apellido materno debe tener como máximo 45 carácteres')"
                                        oninput="setCustomValidity('')">
                                    </div>
                                </div>

                                <!-- Fecha de nacimiento del empleado -->
                                <div class="form-group row" >
                                    <label for="fechanac" class="col-md-4 col-form-label text-md-right">{{ __('Fecha de nacimiento') }}<strong>*</strong></label>
                                    <div class="col-md-6">
                                    <input type="date" id="fechanac" name="fechanac" class="form-control"
                                        max="2000-12-31" min="1940-01-01" required
                                        oninvalid="setCustomValidity('La fecha de nacimiento es obligatoria')"
                                        oninput="setCustomValidity('')"
                                        value="{{ old('fechanac') }}">
                                    </div>
                                </div>

                                <!-- Sexo del empleado -->
                                <div class="form-group row" >
                                    <label for="sexo" class="col-md-4 col-form-label text-md-right">{{ __('Sexo del empleado') }}<strong>*</strong></label>
                                    <div class="col-md-6">
                                        <label> <input type="radio" name="sexo" id="sexoh" value="H" value="{{ old('sexo') }}" required
                                            oninvalid="setCustomValidity('El sexo es obligatorio, selecciona una de las dos opciones')"
                                            oninput="setCustomValidity('')"  style="margin-left: 10px"
                                            <?php if( old('sexo') == 'H' ) { ?> checked = "true"; <?php } ?> > Hombre
                                        </label>
                                        <label> <input type="radio" name="sexo" id="sexom" value="M" value="{{ old('sexo') }}" style="margin-left: 10px"
                                            <?php if( old('sexo') == 'M' ) { ?> checked = "true"; <?php } ?> > Mujer
                                        </label>
                                    </div>
                                </div>

                                <!-- Curp  del empleado -->
                                <div class="form-group row" >
                                    {!! Form::label( 'curp','Curp del empleado', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                    <div class="col-md-6">
                                    <input type="text" id="curp" name="curp"  placeholder="Ingrese la curp del empleado" class="form-control"
                                        value="{{ old('curp') }}" maxlength="18"
                                        pattern="^([A-Z]{1})([AEIOU]{1})([A-Z]{2})([0-9]{2})(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1])([HM]{1})([AS|BC|BS|CC|CS|CH|CL|CM|DF|DG|GT|GR|HG|JC|MC|MN|MS|NT|NL|OC|PL|QT|QR|SP|SL|SR|TC|TS|TL|VZ|YN|ZS|NE]{2})([^A|E|I|O|U]{3})([A-Z\d]{1})([0-9]{1})$"
                                        oninvalid="setCustomValidity('La curp es obligatoria, debe respetar el formato de la curp y tener 18 carácteres')"
                                        oninput="setCustomValidity('')"
                                        onfocus="javascript:generarcurp()"
                                        >
                                    </div>
                                </div>

                                <!-- Teléfono del empleado  -->
                                <div class="form-group row">
                                    <label for="telefono" class="col-md-4 col-form-label text-md-right">{{ __('Teléfono del empleado') }}<strong>*</strong></label>
                                    <div class="col-md-6">
                                        <input type="text" id="telefono" name="telefono" placeholder="Ingrese el número telefónico del empleado" class="form-control"
                                        value="{{old('telefono')}}" maxlength="12" minlength="1" pattern="[0-9]{10,12}" required
                                        onkeypress="return (event.charCode >= 48 && event.charCode <= 57)"
                                        oninvalid="setCustomValidity('El número telefónico es obligatorio y debe tener como máximo 12 digitos')"
                                        oninput="setCustomValidity('')" >
                                    </div>
                                </div>


                                <!-- R.F.C. del empleado  -->
                                <div class="form-group row">
                                    {!! Form::label( 'rfc','RFC del empleado', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                    <div class="col-md-6">
                                        <input type="text" id="rfc" name="rfc" placeholder="Ingrese el rfc del empleado" class="form-control"
                                        value="{{old('rfc')}}" maxlength="45" minlength="1"
                                        pattern="^(([A-Z]{4})|([A-Z]{3}))([0-9]{2})(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1])([\w]{3})$"
                                        oninvalid="setCustomValidity('El rfc debe respetar el formato del rfc y puede tener un máximo de 13 carácteres')"
                                        oninput="setCustomValidity('')"
                                        onfocus="javascript:generarrfc()">
                                    </div>
                                </div>

                            </fieldset>

                            <!-- FIELDSET DATOS DOMICILIO -->
                            <fieldset class="border p-2">
                                <legend class="w-auto"> Datos del domicilio </legend>

                                    <!-- Código Postal -->
                                <div class="form-group row" >
                                    <label for="codigop" class="col-md-4 col-form-label text-md-right">{{ __('Código postal del domicilio') }}<strong>*</strong></label>
                                    <div class="col-md-6">
                                    <input type="text" id="codigop" name="codigop"  placeholder="Ingrese el código postal del domicilio" class="form-control"
                                        value="{{ old('codigop') }}" maxlength="5" minlength="1" required pattern="[\d]{1,5}"
                                        oninvalid="setCustomValidity('El código postal del domicilio es obligatorio y debe tener como máximo 5 digitos')"
                                        oninput="setCustomValidity('')"
                                        onfocusout="javascript:consultar()">
                                    </div>
                                </div>

                                    <!-- Calle -->
                                <div class="form-group row" >
                                    <label for="calle" class="col-md-4 col-form-label text-md-right">{{ __('Nombre de la calle') }}<strong>*</strong></label>
                                    <div class="col-md-6">
                                    <input type="text" id="calle" name="calle"  placeholder="Ingrese el nombre de la calle" class="form-control"
                                        value="{{ old('calle') }}" maxlength="45" minlength="1" required pattern="[\w\sÁ-ÿ\u00C0-\u00FF]{1,45}"
                                        oninvalid="setCustomValidity('La nombre de la calle es obligatorio y debe tener como máximo 45 carácteres')"
                                        oninput="setCustomValidity('')">
                                    </div>
                                </div>

                                    <!-- Número -->
                                <div class="form-group row" >
                                    <label for="numero" class="col-md-4 col-form-label text-md-right">{{ __('Número del domicilio') }}<strong>*</strong></label>
                                    <div class="col-md-6">
                                    <input type="number" id="numero" name="numero"  placeholder="Ingrese el número del domicilio" class="form-control"
                                        value="{{ old('numero') }}" min="1" max="9999" required pattern="[\d]{1,4}"
                                        oninvalid="setCustomValidity('El número del domicilio es obligatorio y debe tener como máximo 4 digitos')"
                                        oninput="setCustomValidity('')">
                                    </div>
                                </div>

                                    <!-- Colonia -->
                                <div class="form-group row" >
                                    <label for="colonia" class="col-md-4 col-form-label text-md-right">{{ __('Nombre de la colonia') }}<strong>*</strong></label>
                                    <div class="col-md-6">
                                        <select id="colonia" name="colonia" class="form-control" required
                                            oninvalid="setCustomValidity('Debe seleccionar una colonia')"
                                            oninput="setCustomValidity('')"  placeholder="Seleccione una colonia" >
                                            <option value = ""> Seleccione una colonia </option>
                                        </select>
                                    </div>
                                </div>

                                <!-- Municipio -->
                                <div class="form-group row" >
                                    <label for="municipio" class="col-md-4 col-form-label text-md-right">{{ __('Nombre del municipio') }}<strong>*</strong></label>
                                    <div class="col-md-6">
                                    <input type="text" id="municipio" name="municipio"  placeholder="Ingrese el nombre del municipio" class="form-control"
                                        value="{{ old('municipio') }}" maxlength="45" minlength="1" required pattern="[\w\sÁ-ÿ\u00C0-\u00FF]{1,45}"
                                        oninvalid="setCustomValidity('El nombre del municipio es obligatorio y debe tener como máximo 45 carácteres')"
                                        oninput="setCustomValidity('')">
                                    </div>
                                </div>

                                    <!-- Ciudad -->
                                <div class="form-group row" >
                                    <label for="ciudad" class="col-md-4 col-form-label text-md-right">{{ __('Nombre del ciudad') }}<strong>*</strong></label>
                                    <div class="col-md-6">
                                    <input type="text" id="ciudad" name="ciudad"  placeholder="Ingrese el nombre de la ciudad" class="form-control"
                                        value="{{ old('ciudad') }}" maxlength="45" minlength="1" required pattern="[\w\sÁ-ÿ\u00C0-\u00FF]{1,45}"
                                        oninvalid="setCustomValidity('El nombre de la ciudad es obligatorio y debe tener como máximo 45 carácteres')"
                                        oninput="setCustomValidity('')">
                                    </div>
                                </div>


                            </fieldset>
                            
                            <br>

                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Registrar') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
