<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Bienvenido</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">

    </head>
    <body>
        
    <nav class="navbar navbar-light" style="background-color: orange;">
            @if (Route::has('login'))
                <div class="container">
                    @auth
                        <a href="{{ url('/home') }}" class="btn btn-light">Inicio</a>
                    @else
                        <a href="{{ route('login') }}" class="btn btn-light">Iniciar Sesión</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}" class="btn btn-light">Registrarse</a>
                        @endif
                    @endauth
                </div>
            @endif
    </nav>

    <div class="container" style="width: 45rem;">
        <img src="images/LogoPLES.png" style="margin-top: 15%;" style="margin-left: 25%">
    </div>

    </body>
</html>
