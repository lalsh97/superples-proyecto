<!DOCTYPE html>

  <html lang="en">

      <head>
        <meta charset="utf-8">
        <title>System-PLES</title>
        <meta content="width=device-width, initial-scale=1.0" name="viewport">
        <meta content="" name="keywords">
        <meta content="" name="description">

        <!-- Favicons -->
        <link href="img/incono.ico" rel="icon">

        <!-- Bootstrap CSS File -->
        <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

        <!-- Libraries CSS Files -->
        <link href="lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <link href="lib/animate/animate.min.css" rel="stylesheet">
        <link href="lib/ionicons/css/ionicons.min.css" rel="stylesheet">
        <link href="lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
        <link href="lib/lightbox/css/lightbox.min.css" rel="stylesheet">

        <!-- Main Stylesheet File -->
        <link href="css/prin/style.css" rel="stylesheet">
        <link href="css/independientes.css" rel="stylesheet">
        <link href="css/estilo.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="/css/vistaPrueba.css">
        <!-- =======================================================
          Theme URL: https://bootstrapmade.com/devfolio-bootstrap-portfolio-html-template/

        ======================================================= -->
      </head>

      <body id="page-top" class="fondos">

          <!--/ Nav Star style="background:#942c2cd9;"  contenedor de botones/-->
          <nav  class="navbar navbar-b navbar-trans navbar-expand-md fixed-top" id="mainNav">

            <div class="container" >
              <a href="{{ url('/inicio') }}"><img src="img/icono.png" width="250"  alt="Logo" /></a>
              <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarDefault"
                aria-controls="navbarDefault" aria-expanded="false" aria-label="Toggle navigation">
                <span></span>
                <span></span>
                <span></span>
              </button>
                @if (Route::has('login'))
              <div class="navbar-collapse collapse justify-content-end" id="navbarDefault">

                <ul class="navbar-nav">
                  @auth
                  <li class="nav-item">
                    <a class="nav-link js-scroll active est" href="{{ url('/home') }}">Cuenta</a>
                  </li>
                  @else
                  <li class="nav-item">
                    <a class="nav-link js-scroll est" href="{{ route('login') }}">Ingresar</a>
                  </li>
                  @if (Route::has('register'))
                  <li class="nav-item">
                    <a class="nav-link js-scroll est" href="{{ route('register') }}">Registrarse</a>
                  </li>
                  @endif


                  @endauth
                </ul>

              </div>
                @endif
            </div>
          </nav>
          <!--/ Nav End /-->

          <!--/ Intro Skew Star /Mensajes de bienvenida-->
          <div id="home" class="intro route bg-image" style="background-image: url(img/imagen1.jpg)">
            <div class="overlay-itro"></div>
            <div class="intro-content display-table">
              <div class="table-cell">
                <div class="container">
                  <!--<p class="display-6 color-d">Hello, world!</p>-->

                  <div class="center-outer">
                    <div class="center-inner">
                      <div class="bubbles ">
                        <h3>SYSTEM-PLES</h3>
                        <h2>El sistema que facilita tus ventas</h2>
                      </div>
                    </div>
                  </div>

                  <p class="intro-subtitle"><span class="text-slider-items">Todo lo que estabas buscando en un solo sistema,
                    El sistema hecho a tu medida, Cobra facíl y rápido </span><strong class="text-slider"></strong></p>
                <p class="pt-2"><a class="btn btn-primary btn js-scroll px-4" href="#contac" role="button">Plataforma Latinoamericana</a></p>
                </div>
              </div>
            </div>
          </div>

          <!--/ Intro Skew End /-->
          <section id="conoce" class="services-mf route">
            <div class="container">
              <div class="row">
                <div class="col-sm-12">
                  <div class="title-box text-center">
                    <h3 class="title-a">
                      Plataforma Latinoamericana, Económica y Social
                    </h3>
                    <p class="subtitle-a">
                      Siempre, brindando el mejor servicio
                    </p>
                    <div class="line-mf"></div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-4">
                  <div class="service-box">
                    <div class="service-ico">
                      <span class="ico-circle"><h3><br>Misión</h3></span>
                    </div>
                    <div class="service-content">
                      <p class="s-description texto" style="text-align: justify">
                        <br>
                        Somos una organización orientada a llevar productos y servicios subsidiadas, a la población mas vulnerable de Latinoamerica, incrementando sensiblemente su poder adquisitivo
                        <br><br><br>
                      </p>
                    </div>
                  </div>
                </div>
                <div class="col-md-4" >
                  <div class="service-box">
                    <div class="service-ico">
                      <span class="ico-circle"><h3><br>Visión</h3></i></span>
                    </div>
                    <div class="service-content">
                      <p class="s-description texto" style="text-align: justify" >
                        <br><br>
                        Ser una organización lider en Latinoamérica llevando bienestar a la gente, estimulando la productividad, la salud y la educación.
                      <br><br><br>
                    </p>
                    </div>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="service-box" >
                    <div class="service-ico">

                    <a href="#instalacion"> <h2 class="tit"><br>Instalaciones</h2> </a>
                    <div class="line-mf"></div>
                    </div>

                    <div class="service-ico">
                      <br><br>
                      <a href="#contac"> <h2 class="tit"><br>Contacto</h2> </a>
                      <div class="line-mf"></div>
                      <br><br>
                    </div>

                  </div>
                </div>
              </div>
            </div>
          </section>

          <!-- Instalaciones -->
          <!--/ Section Portfolio Star /-->
          <section  class="portfolio-mf sect-pt4 route">
            <div class="container">
              <div class="row">
                <div class="col-sm-12">
                  <div class="title-box text-center">
                    <h3 id="instalacion" class="title-a">
                      Instalaciones
                    </h3>
                    <p class="subtitle-a">
                      Los mejores equipos
                    </p>
                    <div class="line-mf"></div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-4">
                  <div class="work-box">
                    <a href="tienda/tienda1.jpg" data-lightbox="gallery-mf">
                      <div class="work-img">
                        <img src="tienda/tienda1.jpg" alt="" class="img-fluid">
                      </div>
                      <div class="work-content">
                        <div class="row">
                        </div>
                      </div>
                    </a>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="work-box">
                    <a href="tienda/tienda2.jpg" data-lightbox="gallery-mf">
                      <div class="work-img">
                        <img src="tienda/tienda2.jpg" alt="" class="img-fluid">
                      </div>
                      <div class="work-content">
                        <div class="row">

                        </div>
                      </div>
                    </a>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="work-box">
                    <a href="tienda/tienda3.jpg" data-lightbox="gallery-mf">
                      <div class="work-img">
                        <img src="tienda/tienda3.jpg" alt="" class="img-fluid">
                      </div>
                      <div class="work-content">
                        <div class="row">

                        </div>
                      </div>
                    </a>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="work-box">
                    <a href="tienda/tienda4.jpg" data-lightbox="gallery-mf">
                      <div class="work-img">
                        <img src="tienda/tienda4.jpg" alt="" class="img-fluid">
                      </div>
                      <div class="work-content">
                        <div class="row">

                        </div>
                      </div>
                    </a>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="work-box">
                    <a href="tienda/tienda5.jpg" data-lightbox="gallery-mf">
                      <div class="work-img">
                        <img src="tienda/tienda5.jpg" alt="" class="img-fluid">
                      </div>
                      <div class="work-content">
                        <div class="row">

                        </div>
                      </div>
                    </a>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="work-box">
                    <a href="tienda/tienda6.jpg" data-lightbox="gallery-mf">
                      <div class="work-img">
                        <img src="tienda/tienda6.jpg" alt="" class="img-fluid">
                      </div>
                      <div class="work-content">
                        <div class="row">

                        </div>
                      </div>
                    </a>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="work-box">
                    <a href="tienda/tienda7.jpg" data-lightbox="gallery-mf">
                      <div class="work-img">
                        <img src="tienda/tienda7.jpg" alt="" class="img-fluid">
                      </div>
                      <div class="work-content">
                        <div class="row">

                        </div>
                      </div>
                    </a>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="work-box">
                    <a href="tienda/tienda8.jpg" data-lightbox="gallery-mf">
                      <div class="work-img">
                        <img src="tienda/tienda8.jpg" alt="" class="img-fluid">
                      </div>
                      <div class="work-content">
                        <div class="row">

                        </div>
                      </div>
                    </a>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="work-box">
                    <a href="tienda/tienda9.jpg" data-lightbox="gallery-mf">
                      <div class="work-img">
                        <img src="tienda/tienda9.jpg" alt="" class="img-fluid">
                      </div>
                      <div class="work-content">
                        <div class="row">

                        </div>
                      </div>
                    </a>
                  </div>
                </div>

              </div>
            </div>
          </section>
          <!--/ Section Portfolio End contenedor de informacion /-->

          <div id="contac" class="testimonials paralax-mf bg-image" style="background-image: url(img/overlay-bg.jpg)">
            <div class="overlay-mf"></div>
            <div class="container">
              <div class="row">
                <div class="col-md-12">
                  <div id="testimonial-mf" class="owl-carousel owl-theme">
                    <div class="testimonial-box">
                      <div class="author-test">
                        <span class="author">Ponte en contacto con nosotros</span>
                      </div>
                      <div class="content-test">
                        <div class="content-test">
                          <div class="more-info">

                            <ul class="list-ico">

                              <a href="https://www.google.com/maps/place/Plataforma+Latinoamericana+Econ%C3%B3mica+y+Social/@17.0911898,-96.713989,17z/data=!3m1!4b1!4m5!3m4!1s0x85c7232347fc5be5:0xd427be91297242af!8m2!3d17.0911847!4d-96.7118003" target="_blank">
                                <li>
                                  <span class="ion-ios-location">
                                  </span>Privada de, Prol. de Eucaliptos 105, Ricardo Flores Magon, 68020 Oaxaca de Juárez, Oax.
                              </li>
                            </a>

                        <li><span class="ion-ios-telephone"></span> 951 143 3017 </li>
                        <li><span class="ion-ios-telephone"></span> 951 318 7385 </li>
                        <li><span class="ion-ios-telephone"></span> 951 207 7966 </li>
                            </div>
                        </div>
                        <div class="socials">
                          <ul>

                            <li><a href="https://www.facebook.com/superplesmx" target="_blank"><span class="ico-circle"><i class="ion-social-facebook"></i></span></a></li>
                            <li><a href="https://www.instagram.com/plesmx/"><span class="ico-circle"><i class="ion-social-instagram"></i></span></a></li>
                            <li><a href="https://twitter.com/plesmx"><span class="ico-circle"><i class="ion-social-twitter"></i></span></a></li>
                            <li><a href="https://www.pinterest.com.mx/plesmx/"><span class="ico-circle"><i class="ion-social-pinterest"></i></span></a></li>
                            <li><a href="https://www.flickr.com/photos/147879401@N08/"><span class="ico-circle"><i class="fab fa-flickr"></i></span></a></li>
                            <li><a href="https://www.linkedin.com/in/ples-mx-087655183/"><span class="ico-circle"><i class="ion-social-linkedin"></i></span></a></li>
                          </ul>
                        </div>
                      </div>
                    </div>

                </div>
              </div>
            </div>
          </div>

          <!--/ Section Contact-footer End /-->
          <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
          <div id="preloader"></div>

          <!-- JavaScript Libraries -->
          <script src="lib/jquery/jquery.min.js"></script>
          <script src="lib/jquery/jquery-migrate.min.js"></script>
          <script src="lib/popper/popper.min.js"></script>
          <script src="lib/bootstrap/js/bootstrap.min.js"></script>
          <script src="lib/easing/easing.min.js"></script>
          <script src="lib/counterup/jquery.waypoints.min.js"></script>
          <script src="lib/counterup/jquery.counterup.js"></script>
          <script src="lib/owlcarousel/owl.carousel.min.js"></script>
          <script src="lib/lightbox/js/lightbox.min.js"></script>
          <script src="lib/typed/typed.min.js"></script>
          <!-- Contact Form JavaScript File -->
          <script src="contactform/contactform.js"></script>
            <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css"
                integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">

          <!-- Template Main Javascript File -->
          <script src="js/maininicio.js"></script>

          <script src="js/independ.js"></script>

      </body>

</html>
