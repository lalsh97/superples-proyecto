<table>
  
    <thead>
        <tr>
          <th> Código de barra </th>
          <th> Marca </th>
          <th> Presentación </th>
          <th> Nombre del proveedor </th>
          <th> Categoría </th>
          <th> Descripción </th>
          <th> Precio de costo </th>
          <th> Precio de venta </th>
          <th> IVA </th>
        </tr>
    </thead>

    <tbody>
        @foreach($Productos as $productos)
            <tr>
              <td>{{ $productos-> codigo }}</td>
              <td>{{ $productos-> nombremarca }}</td>
              <td>{{ $productos-> nombrepresentacion }}</td>
              <td>{{ $productos-> nombrecompleto }}</td>
              <td>{{ $productos-> nombrecategoria }}</td>
              <td>{{ $productos-> descripcion }}</td>
              <td> <?php echo bcadd( $productos->preciocosto , '0', 2); ?>
              <td> <?php echo bcadd( $productos->precioventa , '0', 2); ?>
              <td>
                  @if($productos->iva == 0 )
                      {{ "NO" }}
                  @else
                      {{ "SI" }}
                  @endif
              </td>
            </tr>
        @endforeach
    </tbody>

</table>
