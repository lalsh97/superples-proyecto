<table>

    <thead>
      <tr>
        <th> ID promoción </th>
        <th> Nombre </th>
        <th> Fecha de lanzamiento </th>
        <th> Fecha de terminación </th>
        <th> Total </th>
        <th> Tipo </th>
      </tr>
    </thead>

    <tbody>
      @foreach ( $promociones as $promociones )
        <tr>
          <td>{{ $promociones->idPromociones }}</td>
          <td>{{ $promociones-> Nombre }}</td>
          <td>{{ $promociones-> Fechaini }}</td>
          <td>{{ $promociones-> Fechafin }}</td>
          <td> <?php echo bcadd( $promociones->Total , '0', 2); ?>
          <?php
              if ($promociones->Tipo == 1 ) {
                  ?>
                      <td>Oferta</td>
                  <?php
              } elseif ($promociones->Tipo == 2 ) {
                  ?>
                      <td>Descuento</td>
                  <?php
              } elseif ($promociones->Tipo == 3 ) {
                  ?>
                      <td>Paquete</td>
                  <?php
              }
          ?>
        </tr>
      @endforeach
    </tbody>
    
</table>
