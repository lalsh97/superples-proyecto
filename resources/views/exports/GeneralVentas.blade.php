<table>

    <thead>
        <tr>
            <th> Código producto </th>
            <th> Descripción producto </th>
            <th> Cantidad vendida</th>
            <th> Total precio costo </th>
            <th> Total precio venta </th>
            <th> Ganancias totales </th>
        </tr>
    </thead>

    <tbody>  
        @foreach ( $GeneralVentas as $productos )
            <tr>
                <td>{{$productos->codigo}}</td>
                <td>{{$productos->descripcion}} </td>
                <td>{{$productos->cantidad}}</td>
                <td> <?php echo bcadd( $productos->preciocosto , '0', 2); ?>
                <td> <?php echo bcadd( $productos->precioventa , '0', 2); ?>
                <td> <?php echo bcadd( $productos->gananciatotal , '0', 2); ?>
            </tr>
        @endforeach
    </tbody>

</table>
