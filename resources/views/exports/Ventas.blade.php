<table>

    <thead>
        <tr>
            <th> ID venta </th>
            <th> Nombre del empleado </th>
            <th> Nombre del cliente  </th>
            <th> Fecha de la venta </th>
            <th> Total de la venta </th>
            <th> Tipo de pago </th>
        </tr>
    </thead>

    <tbody>  
        @foreach ( $Ventas as $ventas )
            <tr>
                <td >{{ $ventas->idventa }}</td>
                <td>{{ $ventas-> nombrecompletoempleado }}</td>
                <td>{{ $ventas-> nombrecompletocliente }}</td>
                <td>{{ $ventas-> fechaventa }}</td>
                <td> <?php echo bcadd(  $ventas->totalventa ,'0',2); ?></td>
                <td>
                    @if( $ventas->tipopago == 1 )
                        {{ "Contado" }}
                    @elseif( $ventas->tipopago == 2 )
                        {{ "Crédito" }}
                    @elseif( $ventas->tipopago == 3 )
                        {{ "Contado y crédito" }}
                    @endif
                </td>
            </tr>
        @endforeach
    </tbody>

</table>
