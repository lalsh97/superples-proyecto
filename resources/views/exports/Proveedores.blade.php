<table>

    <thead>
      <tr>
        <th>ID proveedor </th>
        <th>RFC</th>
        <th>Nombre</th>
        <th>Domicilio</th>
        <th>Correo electrónico</th>
        <th>Teléfono</th>
      </tr>
    </thead>
    
    <tbody>
      @foreach($Proveedores as $proveedores)
        <tr>
          <td>{{ $proveedores-> idProveedor }}</td>
          <td>{{$proveedores-> Rfc}}</td>
          <td>{{$proveedores-> nombrecompleto}}</td>
          <td>{{$proveedores-> direccion}} </td>
          <td>{{$proveedores-> correo}}</td>
          <td>{{$proveedores-> telefono}}</td>
        </tr>
      @endforeach
    </tbody>
    
</table>
