  <table>

      <thead>

        <tr>
            <th > ID </th>
            <th > Nombre completo </th>
            <th > Curp </th>
            <th > Fecha de nacimiento </th>
            <th > Sexo </th>
            <th > Teléfono </th>
        </tr>
      </thead>
    
      <tbody>

        @foreach($Clientes as $clientes)

          <tr>
            <td>{{ $clientes-> idcliente }}</td>
            <td>{{ $clientes-> nombrecompleto }}</td>
            <td>{{ $clientes-> curp }}</td>
            <td>{{ $clientes-> fechanac }}</td>
            <td>{{ $clientes-> sexo }}</td>
            <td>{{ $clientes-> telefono }}</td>
          </tr>

        @endforeach

      </tbody>

  </table>
