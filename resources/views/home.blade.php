@extends( ( Auth::user()->Nivel == 1 ) ? 'layouts.plantillamenuadministrador' : 'layouts.plantillamenuempleado')

    @section('title')
        Inicio
    @endsection

    @section('contenido')

        <link rel="STYLESHEET" href="{{ asset( 'css/EstilosHome/estiloshome.css' ) }}">
        
        <!-- CARDS -->
        @if( Auth::user()->Nivel == 1 )

            <!-- Cards -->
            <div class="container">
                <div class="row justify-content-center">

                    <!-- PRIMERA FILA (EMPLEADOS, PRODUCTOS, PROMOCIONES) -->
                    <div class="card-deck" style="margin-bottom: 25px" >
                    
                        <!-- CARD MENU EMPLEADOS -->
                        <div class="card text-center bg-light border-secondary mb-3" style="width: 18rem; margin-right: 25px;">
                            <h4>EMPLEADOS</h4>
                            <img class="card-img-center" src="{{ asset('images/Home/MenuEmpleados.png') }}" alt="Card image cap" width="150" height="150" style="display: block; margin-left: auto; margin-right: auto; margin-top: 10px;" >
                            <div class="card-body">
                                <a href="{{ route( 'registrarempleado' ) }}"> <p> Registrar empleado </p> </a>
                                <a href="{{ route( 'busquedaempleado', 'modificar' ) }}"> <p> Modificar información </p> </a>
                                <a href="{{ route( 'listadoempleados' ) }}"> <p> Mostrar empleados </p> </a>
                                <a href="{{ route( 'busquedaempleado', 'descontinuar' ) }}"> <p> Eliminar empleado </p> </a>
                            </div>
                        </div>

                            <!-- CARD MENU PRODUCTOS -->
                        <div class="card text-center bg-light border-secondary mb-3" style="width: 18rem; margin-right: 25px;">
                            <h4>PRODUCTOS</h4>
                            <img class="card-img-center" src="{{ asset('images/Home/MenuProductos.png') }}" alt="Card image cap" width="150" height="150" style="display: block; margin-left: auto; margin-right: auto; margin-top: 10px;" >
                            <div class="card-body">
                                <a href="{{ route( 'registrarproductos' ) }}"> <p> Registrar producto </p> </a>
                                <a href="{{ route( 'busquedaproducto', 'modificar' ) }}"> <p> Modificar información </p> </a>
                                <a href="{{ route( 'buscarproducto' ) }}"> <p> Buscar producto </p> </a>
                                <a href="{{ route( 'listadoproductos' ) }}"> <p> Mostrar producto </p> </a>
                                <a href="{{ route( 'busquedaproducto', 'descontinuar' ) }}"> <p> Descontinuar producto </p> </a>
                                <a href="{{ route( 'codigobarrasproducto' ) }}"> <p> Generar código barras </p> </a>
                            </div>
                        </div>

                            <!-- CARD MENU PROMOCIONES -->
                        <div class="card text-center bg-light border-secondary mb-3" style="width: 18rem; margin-right: 25px;">
                            <h4>PROMOCIONES</h4>
                            <img class="card-img-center" src="{{ asset('images/Home/MenuPromociones.png') }}" alt="Card image cap" width="150" height="150" style="display: block; margin-left: auto; margin-right: auto; margin-top: 10px;" >
                            <div class="card-body">
                                <a href="{{ route( 'selecciondeproducto' ) }}"> <p> Registrar promoción </p> </a>
                                <a href="{{ route( 'modificarpromociones' ) }}"> <p> Modificar promoción </p> </a>
                                <a href="{{ route( 'listapromociones' ) }}"> <p> Mostrar promociones </p> </a>
                                <a href="{{ route( 'listadopromoterminar' ) }}"> <p> Terminar promoción </p> </a>
                            </div>
                        </div>

                    </div>

                    <!-- SEGUNDA FILA (PROVEEDORES, CLIENTES, VENTAS) -->
                    <div class="card-deck" style="margin-bottom: 20px" >

                            <!-- CARD MENU PROVEEDORES -->
                        <div class="card text-center bg-light border-secondary mb-3" style="width: 18rem; margin-right: 25px;" >
                            <h4>PROVEEDORES</h4>
                            <img class="card-img-center" src="{{ asset('images/Home/MenuProveedores.png') }}" alt="Card image cap" width="150" height="150" style="display: block; margin-left: auto; margin-right: auto; margin-top: 10px;" >
                            <div class="card-body">
                                <a href="{{ route( 'registrarproveedor' ) }}"> <p> Registrar proveedor </p> </a>
                                <a href="{{ route( 'busquedaproveedor', 'modificar' ) }}"> <p> Modificar proveedor </p> </a>
                                <a href="{{ route( 'listadoproveedores' ) }}"> <p> Mostrar proveedores </p> </a>
                                <a href="{{ route( 'busquedaproveedor', 'buscar' ) }}"> <p> Buscar proveedor </p> </a>
                                <a href="{{ route( 'busquedaproveedor', 'descontinuar' ) }}"> <p> Eliminar proveedor </p> </a>
                                <a href="{{ route( 'registrarentregapedido' ) }}"> <p> Registrar compra </p> </a>
                                <a href="{{ route( 'registrardevolucioncompra' ) }}"> <p> Registrar devolución de compra </p> </a>
                            </div>
                        </div>

                            <!-- CARD MENU CLIENTES -->
                        <div class="card text-center bg-light border-secondary mb-3" style="width: 18rem; margin-right: 25px;">
                            <h4>CLIENTES</h4>
                            <img class="card-img-center" src="{{ asset('images/Home/MenuClientes.png') }}" alt="Card image cap" width="150" height="150" style="display: block; margin-left: auto; margin-right: auto; margin-top: 10px;" >
                            <div class="card-body">
                                <a href="{{ route( 'registrarcliente' ) }}"> <p> Registrar cliente </p> </a>
                                <a href="{{ route( 'busquedacliente', 'modificar' ) }}"> <p> Modificar información </p> </a>
                                <a href="{{ route( 'listadoclientes' ) }}"> <p> Mostrar clientes </p> </a>
                                <a href="{{ route( 'busquedacliente', 'buscar' ) }}"> <p> Buscar cliente </p> </a>
                                <a href="{{ route( 'busquedacliente', 'descontinuar' ) }}"> <p> Cancelar crédito </p> </a>
                                <a href="{{ route( 'listadocreditossincredito' ) }}"> <p> Aprobar crédito cliente </p> </a>
                                <a href="{{ route( 'listadocreditosclientes' ) }}"> <p> Realizar abono crédito </p> </a>
                            </div>
                        </div>

                            <!-- CARD MENU VENTAS -->
                        <div class="card text-center bg-light border-secondary mb-3" style="width: 18rem; margin-right: 25px;">
                            <h4>VENTAS</h4>
                            <img class="card-img-center" src="{{ asset('images/Home/MenuVentas.png') }}" alt="Card image cap" width="150" height="150" style="display: block; margin-left: auto; margin-right: auto; margin-top: 10px;" >
                            <div class="card-body">
                                <a href="{{ route( 'registrarventa' ) }}">  <p> Nueva venta </p> </a>
                                <a href="{{ route( 'listadoventas' ) }}">   <p> Mostrar ventas </p> </a>
                                <a href="{{ route( 'busquedaventa' ) }}">   <p> Buscar venta </p> </a>
                                <a href="{{ route( 'cortediacaja' ) }}">    <p> Corte de caja - ventas y compras </p> </a>
                                <a href="{{ route( 'cortediaventas' ) }}">  <p> Corte de caja - ventas </p> </a>
                                <a href="{{ route( 'cortediacompras' ) }}"> <p> Corte de caja - compras </p> </a>
                            </div>
                        </div>

                    </div>

                    <!-- TERCERA FILA (INVENTARIO, REPORTES PARTE GENERALES, REPORTES ESPECIFICOS) -->
                    <div class="card-deck" style="margin-bottom: 20px" >

                            <!-- CARD MENU INVENTARIO -->
                        <div class="card text-center bg-light border-secondary mb-3" style="width: 18rem; margin-right: 25px;" >                        
                            <h4>INVENTARIO</h4>
                            <img class="card-img-center" src="{{ asset('images/Home/MenuInventario.png') }}" alt="Card image cap" width="150" height="150" style="display: block; margin-left: auto; margin-right: auto; margin-top: 10px;" >
                            <div class="card-body">
                                <a href="{{ route( 'registrarentradainventario' ) }}"> <p> Entrada a inventario </p> </a>
                                <a href="{{ route( 'registrarsalidainventario' ) }}"> <p> Salida de inventario </p> </a>
                                <a href="{{ route( 'catalogoproductos' ) }}"> <p> Catálogo de productos </p> </a>
                                <a href="{{ route( 'listadomovimientosinventario' ) }}"> <p> Movimientos de inventario </p> </a>
                            </div>
                        </div>

                            <!-- CARD MENU REPORTES GENERALES ( PARTE 1 ) -->
                        <div class="card text-center bg-light border-secondary mb-3" style="width: 18rem; margin-right: 25px;" >                        
                            <h4>REPORTES GENERALES</h4>
                            <img class="card-img-center" src="{{ asset('images/Home/MenuReportesGenerales.png') }}" alt="Card image cap" width="150" height="150" style="display: block; margin-left: auto; margin-right: auto; margin-top: 10px;" >
                            <div class="card-body">
                                <a href="{{ route( 'reporteclientes' ) }}"> <p> Reporte de clientes </p> </a>
                                <a href="{{ route( 'reporteproveedores' ) }}"> <p> Reporte de proveedores </p> </a>
                                <a href="{{ route( 'reporteproductos' ) }}"> <p> Reporte de productos </p> </a>
                                <a href="{{ route( 'reportepromociones' ) }}"> <p> Reporte de promociones </p> </a>
                                <a href="{{ route( 'reportegeneralventas' ) }}"> <p> Reporte general de ventas </p> </a>
                                <a href="{{ route( 'reporteventas' ) }}"> <p> Reporte de ventas </p> </a>                                
                            </div>
                        </div>

                        <!-- CARD MENU REPORTES ESPECIFICOS ( PARTE 2 ) -->
                        <div class="card text-center bg-light border-secondary mb-3" style="width: 18rem; margin-right: 25px;" >                        
                            <h4>REPORTES ESPECIFICOS</h4>
                            <img class="card-img-center" src="{{ asset('images/Home/MenuReportesEspecificos.png') }}" alt="Card image cap" width="150" height="150" style="display: block; margin-left: auto; margin-right: auto; margin-top: 10px;" >
                            <div class="card-body">
                                <a href="{{ route( 'reporteexistenciasinventario' ) }}"> <p> Reporte de existencias de inventario </p> </a>
                                <a href="{{ route( 'reporteentradasinventario' ) }}"> <p> Reporte de entradas de inventario </p> </a>
                                <a href="{{ route( 'reportesalidasinventario' ) }}"> <p> Reporte de salidas de inventario </p> </a>
                                <a href="{{ route( 'reporteproductosvendidos' ) }}"> <p> Reporte de productos vendidos </p> </a>
                                <a href="{{ route( 'reporteproductosbajos' ) }}"> <p> Reporte de productos bajos </p> </a>
                                <a href="{{ route( 'reportecreditosclientes' ) }}"> <p> Reporte de créditos de clientes </p> </a>
                                <a href="{{ route( 'reporteproveedoresmovimientos' ) }}"> <p> Reporte de movimientos de proveedores </p> </a>
                            </div>
                        </div>

                    </div>

                </div>
            </div>

        @elseif( Auth::user()->Nivel == 2 )

            <!-- Cards -->
            <div class="container">
                <div class="row justify-content-center">

                    <!-- PRIMERA FILA (PRODUCTOS, PROMOCIONES, PROVEEDORES) -->
                    <div class="card-deck" style="margin-bottom: 25px" >

                            <!-- CARD MENU PRODUCTOS -->
                        <div class="card text-center bg-light border-secondary mb-3" style="width: 18rem; margin-right: 25px;">
                            <h4>PRODUCTOS</h4>
                            <img class="card-img-center" src="{{ asset('images/Home/MenuProductos.png') }}" alt="Card image cap" width="150" height="150" style="display: block; margin-left: auto; margin-right: auto; margin-top: 10px;" >
                            <div class="card-body">
                                <a href="{{ route( 'buscarproducto' ) }}"> <p> Buscar producto </p> </a>
                                <a href="{{ route( 'listadoproductos' ) }}"> <p> Mostrar producto </p> </a>
                                <a href="{{ route( 'codigobarrasproducto' ) }}"> <p> Generar código barras </p> </a>
                            </div>
                        </div>

                            <!-- CARD MENU PROMOCIONES -->
                        <div class="card text-center bg-light border-secondary mb-3" style="width: 18rem; margin-right: 25px;">
                            <h4>PROMOCIONES</h4>
                            <img class="card-img-center" src="{{ asset('images/Home/MenuPromociones.png') }}" alt="Card image cap" width="150" height="150" style="display: block; margin-left: auto; margin-right: auto; margin-top: 10px;" >
                            <div class="card-body">
                                <a href="{{ route( 'listapromociones' ) }}"> <p> Mostrar promociones </p> </a>
                            </div>
                        </div>

                            <!-- CARD MENU PROVEEDORES -->
                        <div class="card text-center bg-light border-secondary mb-3" style="width: 18rem; margin-right: 25px;" >
                            <h4>PROVEEDORES</h4>
                            <img class="card-img-center" src="{{ asset('images/Home/MenuProveedores.png') }}" alt="Card image cap" width="150" height="150" style="display: block; margin-left: auto; margin-right: auto; margin-top: 10px;" >
                            <div class="card-body">
                                <a href="{{ route( 'listadoproveedores' ) }}"> <p> Mostrar proveedores </p> </a>
                                <a href="{{ route( 'busquedaproveedor', 'buscar' ) }}"> <p> Buscar proveedor </p> </a>
                            </div>
                        </div>

                    </div>

                    <!-- SEGUNDA FILA (CLIENTES, VENTAS, INVENTARIO) -->
                    <div class="card-deck" style="margin-bottom: 20px" >

                            <!-- CARD MENU CLIENTES -->
                        <div class="card text-center bg-light border-secondary mb-3" style="width: 18rem; margin-right: 25px;">
                            <h4>CLIENTES</h4>
                            <img class="card-img-center" src="{{ asset('images/Home/MenuClientes.png') }}" alt="Card image cap" width="150" height="150" style="display: block; margin-left: auto; margin-right: auto; margin-top: 10px;" >
                            <div class="card-body">
                                <a href="{{ route( 'listadoclientes' ) }}"> <p> Mostrar clientes </p> </a>
                                <a href="{{ route( 'busquedacliente', 'buscar' ) }}"> <p> Buscar cliente </p> </a>
                            </div>
                        </div>

                            <!-- CARD MENU VENTAS -->
                        <div class="card text-center bg-light border-secondary mb-3" style="width: 18rem; margin-right: 25px;">
                            <h4>VENTAS</h4>
                            <img class="card-img-center" src="{{ asset('images/Home/MenuVentas.png') }}" alt="Card image cap" width="150" height="150" style="display: block; margin-left: auto; margin-right: auto; margin-top: 10px;" >
                            <div class="card-body">
                                <a href="{{ route( 'registrarventa' ) }}"> <p> Nueva venta </p> </a>
                                <a href="{{ route( 'listadoventas' ) }}"> <p> Mostrar ventas </p> </a>
                                <a href="{{ route( 'busquedaventa' ) }}"> <p> Buscar venta </p> </a>
                                <a href="{{ route( 'cortediaventas' ) }}">  <p> Corte de caja - ventas </p> </a>
                            </div>
                        </div>

                            <!-- CARD MENU INVENTARIO -->
                        <div class="card text-center bg-light border-secondary mb-3" style="width: 18rem; margin-right: 25px;" >                        
                            <h4>INVENTARIO</h4>
                            <img class="card-img-center" src="{{ asset('images/Home/MenuInventario.png') }}" alt="Card image cap" width="150" height="150" style="display: block; margin-left: auto; margin-right: auto; margin-top: 10px;" >
                            <div class="card-body">
                                <a href="{{ route( 'catalogoproductos' ) }}"> <p> Catálogo de productos </p> </a>
                                <a href="{{ route( 'listadomovimientosinventario' ) }}"> <p> Movimientos de inventario </p> </a>
                            </div>
                        </div>

                    </div>

                </div>
            </div>

        @endif

        <!-- SALDO INICIAL CAJA PRIMER INICIO SESIÓN POR DÍA -->
        @if( isset( $banderainiciosesion ) )

            @if( $banderainiciosesion == 0 )

                <script>

                    window.onload = function( e ) {
                        
                        //if( document.referrer == "http://localhost:8000/login" ) {
                        if( document.referrer.includes("login") ) {
                            
                            $('#modalEmergenteRegistroSaldoInicial').modal( {backdrop: 'static', keyboard: false} );
                            $('#modalEmergenteRegistroSaldoInicial').modal('show');

                        }
                        
                        console.log( "MODAL SALDO INICIAL URL ANTERIOR: ", document.referrer );

                    }
                
                    function RegistrarSaldoInicialCaja() { 
                        
                        //const FechaActual = new Date(new Date().getTime() - new Date().getTimezoneOffset() * 60000).toISOString().split("T")[0];
                        const FechaActual = new Date();

                        var saldoinicial = document.getElementById("saldoinicial").value;
                        console.log("SALDO INICIAL: ", saldoinicial );

                        if( saldoinicial == "" ) {
                            
                            document.getElementById("saldoinicial").setCustomValidity("El saldo inicial de caja es necesaría para que pueda acceder a las opciones del sistema");

                        } else {

                            SaldoInicial = JSON.stringify( document.getElementById("saldoinicial").value );
                        
                            $.ajaxSetup({
                                headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                }
                            });

                            $.ajax({

                                type: "GET",
                                url: "{{ route('registrarsaldoinicialcaja') }}",
                                data: { SaldoInicial : saldoinicial },
                                success: function(data) {
                                
                                    document.getElementById("saldoinicial").value = "";
                                    $("#modalEmergenteRegistroSaldoInicial").modal('hide');

                                },
                                error:function() {

                                }

                            });

                        }

                    }

                </script>                    
    
                <!-- Ventana Emergente INGRESAR SALDOINICIAL -->
                <div class="modal" tabindex="-1" role="dialog" id="modalEmergenteRegistroSaldoInicial">

                    <div class="modal-dialog" role="document">

                        <div class="modal-content">
                            
                            <!--Header-->
                            <div class="modal-header" id="modalheadersaldoinicialcaja" style="background: rgb(33, 37, 41)">
                                <h5 id="textostringheadersaldoinicialcaja" class="heading" style="margin-left: 10%">
                                    <span style="color: rgb(255,255,255)"> REGISTRO DEL SALDO INICIAL DE LA CAJA </span>
                                </h5>

                                <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close"> -->
                                    <!-- <span  aria-hidden="true" style="color: rgb(255, 255, 255)" >&times;</span> -->
                                <!-- </button> -->
                            </div>

                            <!--Body-->
                            <div class="modal-body" id="modalbodysaldoinicialcaja">                                    
                                
                                <div class="form-group">
                                    <div class="row justify-content-center">

                                        <label for="saldoinicial">Ingrese el saldo inicial con el que cuenta la caja</label>
                                        <input type="text" id="saldoinicial" name="saldoinicial"  class="form-control" required
                                            oninvalid="setCustomValidity('El saldo inicial de caja es necesaría para que pueda acceder a las opciones del sistema')"
                                            onkeypress="return ( event.charCode >= 48 && event.charCode <= 57 || event.charCode == 46 )"
                                            oninput="setCustomValidity('')">

                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-md-4 col-form-label text-md-right">
                                        <button name="botonformsaldoinicial" value="Registrar" type="submit" class="btn btn-success" style="margin-left:200px; margin-right:50px;" formnovalidate onclick="javascript:RegistrarSaldoInicialCaja();">Registrar</button>
                                    </div>
                                </div>
                                
                            </div>
                            
                            <!--Footer-->
                            <div class="modal-footer" id="modalfootersaldoinicialcaja">
                                <!-- <button type="button" class="btn btn-dark" data-dismiss="modal">Cerrar</button> -->
                            </div>

                        </div>

                    </div>

                </div>

            
            @endif

        @endif

    @endsection