@if( Auth::user()->Nivel == 1 )

    @extends('home')

    @section('title')
        Información del crédito del cliente
    @endsection

    @section('contenido')

        <style>

            .scrollbar {

                height: 400px;
                overflow: auto;
                overflow-x: hidden;
                overflow-y: scroll;
                white-space:nowrap;
                border-radius: 10px;

            }

            #scrollbar-style::-webkit-scrollbar {

                width: 6px;
                background-color: #F5F5F5;

            }

            /* Se pone un color de fondo y se redondean las esquinas del thumb */
            #scrollbar-style::-webkit-scrollbar-thumb {


                border-radius: 4px;
                /*background-color: rgb(84, 81, 72);*/
                background-color: rgb(49, 163, 204);

            }

            /* Se cambia el fondo y se agrega una sombra cuando esté en hover */
            #scrollbar-style::-webkit-scrollbar-thumb:hover {

                background: #b3b3b3;
                box-shadow: 0 0 2px 1px rgba(0, 0, 0, 0.2);

            }

            /* Se cambia el fondo cuando esté en active */
            #scrollbar-style::-webkit-scrollbar-thumb:active {

                background-color: #999999;

            }
            /* Se pone un color de fondo y se redondean las esquinas del track */
            #scrollbar-style::-webkit-scrollbar-track {

                background: #e1e1e1;
                border-radius: 4px;

            }

            /* Se cambia el fondo cuando esté en active o hover */
            #scrollbar-style::-webkit-scrollbar-track:hover,
            #scrollbar-style::-webkit-scrollbar-track:active {

                background: #d4d4d4;

            }


            fieldset {

                background-color: #eeeeee;
                margin-top: 5px;

            }

            legend {

                background-color: #212529;
                color: white;
                padding: 5px 10px;
                border-radius: 6px;

            }


        </style>

        <script>

            /*
            window.onload = function( e ) {

                var cantidadadeudada = document.getElementById("spancantidadadeudada").innerText;
                console.log( "Cantidad adeudada: ", cantidadadeudada );

                if( cantidadadeudada >= 0 ) {
                    
                    document.getElementById("divcambio").style.display = "none";
                    document.getElementById("divcantidadadeudada").style.display = "block";

                } else {

                    document.getElementById("divcambio").style.display = "block";
                    document.getElementById("divcantidadadeudada").style.display = "block";

                    document.getElementById("spamcambio").innerText =  ( Math.round( cantidadadeudada * - 100 ) / 100 ).toFixed( 2 ); 
                    document.getElementById("spancantidadadeudada").innerText = "0.00";

                }

            }
            */
            

            function MostrarModalConfirmacion() {

                var nombrecliente = document.getElementById("nombrecliente").value;

                document.getElementById("textoerrorbody").innerText =   "¿Está seguro que desea desactivar el crédito "
                                                                        + "\ndel cliente: " + nombrecliente +" ?";

                $('#ModalConfirmacion').modal('show');

            }

            function DismissModalConfirmacion() {

                $("#ModalConfirmacion").modal('hide');

            }

            function DesactivarCreditoCliente() {

                $("#ModalConfirmacion").modal('hide');

                var idcreditocliente = document.getElementById("idcreditocliente").value;
                var idcliente = document.getElementById("idcliente").value;

                location.href ="/desactivarcreditocliente/" + idcreditocliente +"/" + idcliente;

            }

        </script>

        @if(Session::has('success'))
            <div class="alert alert-success">
                {{session('success')}}
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            </div>
        @elseif( Session::has('warning'))
            <div class="alert alert-warning">
                {{session('warning')}}
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            </div>
        @elseif( Session::has('danger'))
            <div class="alert alert-danger">
                {{session('danger')}}
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            </div>
        @endif


        <!-- DIV DATOS CREDITOCLIENTE -->
        <div class="container" style="margin-bottom: 10px;">

            <!-- FIELDSET INFORMACION CLIENTE -->
            <fieldset  class="border p-">
                <legend  class="w-auto"> INFORMACIÓN DEL CLIENTE </legend>

                <!-- DIV DATOS CENTER -->
                <div class="container">
                    <div class="form-row align-items-center">

                        <!--
                            ID CREDITOCLIENTE
                            ID CLIENTE
                        -->

                        <!-- DIV INFORMACIÓN PERSONAL CLIENTE -->
                        <div class="row" style="margin-left: 10%;">

                        
                            <!--ID CLIENTE -->
                            <div class="col-xs-3" style="display: none;">
                                <label for="nombre"  style="margin-left: 20px;">
                                    Nombre Cliente:
                                    <input type="text" id="idcliente" value="{{ ( $datoscreditocliente->idcliente ) }}">
                                </label>
                            </div>

                            <!--ID CREDITO CLIENTE -->
                            <div class="col-xs-3" style="display: none;">
                                <label for="nombre"  style="margin-left: 20px;">
                                    Nombre Cliente:
                                    <input type="text" id="idcreditocliente" value="{{ ( $datoscreditocliente->idcreditocliente ) }}">
                                </label>
                            </div>

                            <!--NOMBRE CLIENTE -->
                            <div class="col-xs-3">
                                <label for="nombre"  style="margin-left: 20px;">
                                    Nombre del cliente:
                                    <span style="font-weight: bold; font-size:18px">
                                        <?php
                                            echo $datoscreditocliente->nombrecompleto;
                                        ?>
                                    </span>
                                </label>
                            </div>

                            <!-- TELEFONO CLIENTE -->
                            <div class="col-xs-3">
                                <label for="telefono"  style="margin-left: 20px;">
                                    Teléfono:
                                    <span style="font-weight: bold; font-size:18px">
                                        <?php
                                            echo $datoscreditocliente->telefono;
                                        ?>
                                    </span>
                                </label>
                            </div>

                            <!-- CURP CLIENTE -->
                            <div class="col-xs-3">
                                <label for="curp"  style="margin-left: 20px;">
                                    Curp:
                                    <span style="font-weight: bold; font-size:18px">
                                        <?php
                                            echo $datoscreditocliente->curp;
                                        ?>
                                    </span>
                                </label>
                            </div>

                        </div>

                        <!-- DIV DIRECCION -->
                        <div class="row" style="margin-left: 10%;">

                            <!-- DIRECCION CLIENTE -->
                            <div class="col-xs-3">
                                <label for="direccion"  style="margin-left: 20px;">
                                    Dirección del cliente:
                                    <span style="font-weight: bold; font-size:18px">
                                        <?php
                                            echo $datoscreditocliente->direccion;
                                        ?>
                                    </span>
                                </label>
                            </div>

                        </div>

                        <!-- DIV LIMITE Y CANTIDAD ADEUDADA -->
                        <div class="row" style="margin-left: 10%;">

                            <!-- LIMITE CREDITO -->
                            <div class="col-xs-3">
                                <label for="limite"  style="margin-left: 20px;">
                                    Límite crédito:
                                    <span style="font-weight: bold; font-size:18px">
                                        <?php
                                            echo bcadd( $datoscreditocliente->limite, '0', 2 );
                                        ?>
                                    </span>
                                </label>
                            </div>

                            <!-- CANTIDAD ADEUDADA -->
                            <div class="col-xs-3" id="divcantidadadeudada">
                                <label for="cantidadadeudada"  style="margin-left: 50px;">
                                    Cantidad adeudada:
                                    <span id="spancantidadadeudada" style="font-weight: bold; font-size:18px">
                                        <?php
                                            echo bcadd( $datoscreditocliente->cantidadadeudada, '0', 2 );
                                        ?>
                                    </span>
                                </label>
                            </div>

                            <!-- CAMBIO -->
                            <?php
                                if( isset( $cambio ) ) {
                                    ?>
                                        <div class="col-xs-3" id="divcambio">
                                            <label for="cambio" style="margin-left: 50px;">
                                                Cambio:
                                                <span id="spamcambio" style="font-weight: bold; font-size:18px">
                                                    <?php
                                                        echo bcadd( $cambio, '0', 2 );
                                                    ?>
                                                </span>
                                            </label>
                                        </div>
                                    <?php
                                } else {
                                    ?>                                
                                        <div class="col-xs-3" id="divcambio" style="display: none;">
                                            <label for="cambio" style="margin-left: 50px;">
                                                Cambio:
                                                <span id="spamcambio" style="font-weight: bold; font-size:18px">
                                                    <?php
                                                        echo "";
                                                    ?>
                                                </span>
                                            </label>
                                        </div>
                                    <?php
                                }
                            ?>                       
                        </div>

                    </div>
                </div>

            </fieldset>

            <!-- ABONAR -->
            <fieldset  class="border p-2">
                <legend  class="w-auto"> ABONAR </legend>

                <div class="container">
                    <div class="row align-items-center">

                    {!!Form::open(array('name' => 'formabonoscreditocliente', 'url'=>'abonarcantidadcredito','method'=>'POST' ,'autocomplete'=>'off'))!!}

                        <!-- DIV CANTIDAD A ABONAR -->
                        <div class="form-row" style="margin-left: 10%;">

                            <!--CANTIDAD A ABONAR -->
                            <div class="col-auto">
                                <input type="text" id="idcliente" name="idcliente" value="{{ ( $datoscreditocliente->idcliente ) }}" style="display: none;">
                                <input type="text" id="idcreditocliente" name="idcreditocliente" value="{{ ( $datoscreditocliente->idcreditocliente ) }}" style="display: none;">
                                <input type="text" id="nombrecliente" name="nombrecliente" value="{{ ( $datoscreditocliente->nombrecompleto ) }}" style="display: none;">
                                <input type="text" id="limite" name="limite" value="{{ ( $datoscreditocliente->limite ) }}" style="display: none;">

                                <label for="cantidadaabonar" style="margin-left: 20px;">Ingrese la cantidad a abonar:</label>
                                <input type="text" id="cantidadaabonar" name="cantidadaabonar" style="margin-left: 5px;" required
                                    oninvalid="setCustomValidity('La cantidad a abonar es oligatoria')"
                                    onkeypress="return ( event.charCode >= 48 && event.charCode <= 57 || event.charCode == 46 )"
                                    oninput="setCustomValidity('')">

                                <button name="botonformabonoscreditocliente" value="Registrar" type="submit" class="btn btn-success" style="margin-left: 30px;">Registrar abono</button>
                                <button name="botonformabonoscreditocliente" value="Regresar" type="submit" class="btn btn-info" style="margin-left: 30px;" formnovalidate>Regresar</button>

                            </div>

                            <div class="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups">
                                <div class="btn-group mr-2" role="group" aria-label="First group">
                                    <label>                                </label>
                                </div>
                            </div>

                        </div>

                    {!!Form::close()!!}

                    @if( $datoscreditocliente->estadocreditocliente == 1 )
                        <button name="botonformabonoscreditocliente" value="Desactivar" type="submit" class="btn btn-warning" style="margin-left: 30px; margin-bottom:30px;" formnovalidate onclick="javascript:MostrarModalConfirmacion();">Desactivar crédito</button>
                    @endif

                    </div>
                </div>

            </fieldset>

        </div>

        <!-- ModalConfirmacion -->
        <div class="modal" tabindex="-1" role="dialog" id="ModalConfirmacion">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">

                    <!--Header-->
                    <div class="modal-header" id="modalheader" style="background: rgb(33, 37, 41)">
                        <h5 id="textostringheader" class="heading" style="margin-left: 25%; color: white;">CONFIRMACIÓN DE OPERACIÓN</h5>

                        <button type="button" class="close" onclick="javascript:DismissModalConfirmacion();" data-dismiss="modal" aria-label="Close">
                            <span  aria-hidden="true" style="color: rgb(255, 255, 255)" >&times;</span>
                        </button>
                    </div>

                    <!--Body-->
                    <div class="modal-body" id="modalbody">
                        <div class="row">
                            <div class="col">
                                <p id="textoerrorbody" style="text-align: center;"></p>
                            </div>
                        </div>

                    </div>

                    <!--Footer-->
                    <div class="modal-footer" id="modalfooter">
                        <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="javascript:DesactivarCreditoCliente();">ACEPTAR</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal" onclick="javascript:DismissModalConfirmacion();" style="margin-left: 55%;">CANCELAR</button>
                    </div>

                </div>
            </div>
        </div>

        <!-- Tabla -->
        <div class="container">
            <div class="row justify-content-center">

                <div class="scrollbar" id="scrollbar-style">

                    <table class="table table-hover" style="width: 1000px;" id="tabladatosabonocredito" name="tabladatosabonocredito">
                        <thead  class="thead-dark">
                            <tr>
                                <th style="padding-left:7%;" scope="col"> Fecha de la operación </th>
                                <th style="padding-left:7%;" scope="col"> Cantidad abonada </th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ( $datosabonocredito as $dac )
                                <tr>
                                    <td style="padding-left: 10%;">{{ $dac->Fecha }}</td>
                                    <td style="padding-left: 10%;"> <?php echo bcadd( $dac->CantidadAbonada , '0', 2); ?> </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>

                </div>

            </div>
        </div>

        <br>
        <br>
        <br>
        <br>
        <br>

    @endsection

@endif
