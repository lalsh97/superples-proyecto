@if( Auth::user()->Nivel == 1 )

    @extends('home')

    @section('title')
        Listado de créditos
    @endsection


    @section('contenido')

        <link rel="STYLESHEET" href="css/estilostooltipylistados.css">


        <script>

            function FiltrarTabla() {
                
                if( document.getElementById("nombrefiltrado").value != "" ) {
                    
                    RealizarFiltradoTabla( document.getElementById("nombrefiltrado").value );

                } else {
                            
                    document.getElementById("textoalerta").value = "Por favor ingrese el nombre a buscar";
                    document.getElementById("contentalert").style.display = "block";

                }

            }

            function RealizarFiltradoTabla( nombre ) {
            
                var arraylistadocreditosclientes = {!! json_encode( $listadocreditosclientes->toArray(), JSON_HEX_TAG ) !!};                    
                VaciarTabla();
                
                var resultadobusqueda = 0;

                var auxnombrecliente = "";

                for( var i = 0; i < arraylistadocreditosclientes.length; i++ ) {

                    auxnombrecliente = arraylistadocreditosclientes[i].nombrecompleto;
                    auxnombrecliente = auxnombrecliente.toUpperCase();
                    nombre = nombre.toUpperCase();

                    if( auxnombrecliente.includes( nombre ) ) {
                                            
                        var limite = ( Math.round( arraylistadocreditosclientes[i].limite * 100 ) / 100 ).toFixed( 2 );
                        var cantidadadeudada =  ( Math.round( arraylistadocreditosclientes[i].cantidadadeudada * 100 ) / 100 ).toFixed( 2 );

                        document.getElementById("tablacreditosclientes").insertRow(-1).innerHTML = 
                        "<td style='display:none;'>" + arraylistadocreditosclientes[i].idcreditocliente + "</td>" +
                        "<td style='display:none;'>" + arraylistadocreditosclientes[i].idcliente + "</td>" +
                        "<td>" + arraylistadocreditosclientes[i].nombrecompleto + "</td>" +
                        "<td>" + arraylistadocreditosclientes[i].direccion + "</td>" +
                        "<td>" + arraylistadocreditosclientes[i].telefono + "</td>" +
                        "<td>" + limite + "</td>" +                        
                        "<td>" + cantidadadeudada + "</td>";

                        resultadobusqueda += 1;

                    }
                
                }
                
                const tabla = document.getElementById('tablacreditosclientes');
                var rowCount = tabla.rows.length;

                if( rowCount == 1 ) {
                            
                    document.getElementById("textoalerta").value = "Ningún cliente coincide con el nombre buscado";
                    document.getElementById("contentalert").style.display = "block";

                } else {
                    
                    if( resultadobusqueda == 0 ) {
                        document.getElementById("resultadofiltrado").innerHTML = "<strong>No se han encontrado coincidencias</strong> "
                    } else if( resultadobusqueda == 1 ) {                    
                        document.getElementById("resultadofiltrado").innerHTML = "<strong>Se ha encontrado "+ resultadobusqueda +" coincidencia</strong> "
                    } else if( resultadobusqueda > 1 ) {
                        document.getElementById("resultadofiltrado").innerHTML = "<strong>Se han encontradon "+ resultadobusqueda +" coincidencias</strong> "
                    }

                }

            }

            function VaciarTabla() {

                const tabla = document.getElementById('tablacreditosclientes');
                var rowCount = tabla.rows.length;         

                for  ( var x = rowCount - 1; x > 0; x-- ) { 
                    tabla.deleteRow(x); 
                } 

            }

            function RestablecerTabla() {
            
                document.getElementById("resultadofiltrado").innerHTML = "";

                var arraylistadocreditosclientes = {!! json_encode( $listadocreditosclientes->toArray(), JSON_HEX_TAG ) !!};
                
                VaciarTabla();

                for( var i = 0; i < arraylistadocreditosclientes.length; i++ ) {
                                                
                    var limite = ( Math.round( arraylistadocreditosclientes[i].limite * 100 ) / 100 ).toFixed( 2 );
                    var cantidadadeudada =  ( Math.round( arraylistadocreditosclientes[i].cantidadadeudada * 100 ) / 100 ).toFixed( 2 );

                    document.getElementById("tablacreditosclientes").insertRow(-1).innerHTML = 
                    "<td style='display:none;'>" + arraylistadocreditosclientes[i].idcreditocliente + "</td>" +
                    "<td style='display:none;'>" + arraylistadocreditosclientes[i].idcliente + "</td>" +
                    "<td>" + arraylistadocreditosclientes[i].nombrecompleto + "</td>" +
                    "<td>" + arraylistadocreditosclientes[i].direccion + "</td>" +
                    "<td>" + arraylistadocreditosclientes[i].telefono + "</td>" +
                    "<td>" + limite + "</td>" +                        
                    "<td>" + cantidadadeudada + "</td>";
                
                }

            }

            function FilaSeleccionada() {
                        
                var table = document.getElementById('tablacreditosclientes'),
                selected = table.getElementsByClassName('selected');
                
                if (selected[0]) selected[0].className = '';
                event.target.parentNode.className = 'selected';

                /*
                console.log("IDCREDITOCLIENTE SELECCIONADO: ", $("tr.selected td:first" ).html() );
                console.log("IDCLIENTE SELECCIONADO: ", $("tr.selected td:nth-child(2)").html() );
                console.log("NOMBRE CLIENTE SELECCIONADO: ", $("tr.selected td:nth-child(3)").html() );
                console.log("DIRECCION CLIENTE SELECCIONADO: ",$("tr.selected td:nth-child(4)").html() );
                console.log("TELEFONO SELECCIONADO: ", $("tr.selected td:nth-child(5)").html() );
                console.log("LIMITE SELECCIONADO: ", $("tr.selected td:nth-child(6)").html() );
                console.log("CANTIDAD ADEUDADA SELECCIONADA: ", $("tr.selected td:nth-child(7)").html() );
                */
                            
                var idcreditocliente = $("tr.selected td:first" ).html();
                var idcliente = $("tr.selected td:nth-child(2)" ).html();
                    
                location.href ="/informacioncreditocliente/" + idcreditocliente +"/" + idcliente;

            }

            function OcultarAlerta() {

                if( document.getElementById("contentalert").style.display == "none" ) {
                    document.getElementById("contentalert").style.display = "block";
                } else {
                    document.getElementById("contentalert").style.display = "none";
                }

            }

        </script>
            
        @if(Session::has('success'))
            <div class="alert alert-success">
                {{session('success')}}
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            </div>
        @elseif( Session::has('warning'))
            <div class="alert alert-warning">
                {{session('warning')}}
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            </div>
        @elseif( Session::has('danger'))
            <div class="alert alert-danger">
                {{session('danger')}}
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            </div>
        @endif

            <!-- DIV FILTRADO -->
        <div class="container" style="margin-bottom: 10px;">
            
            <!-- DIV NOMBRE FILTRADO -->
            <div class="form-row align-items-center">
                
                <!-- NOMBRE ESPECIFICA -->
                <div class="col-auto" style="margin-left: 32%; margin-bottom:10px;">
                    <label for="nombrefiltrado"> Ingrese el nombre a buscar </label>
                    <input type="text" class="form-control-sm" id="nombrefiltrado" style="margin-left: 10px;">
                </div>

                    
                    <label id="resultadofiltrado" style="margin-left: 20px;">  </label>
                
            </div>

            <!-- Botones -->
            <div class="form-row align-items-center" style="margin-top: 2px; margin-bottom: 5px;">

                <a style="margin-left: 35%;"> <button  class="btn btn-primary" id="botonfiltrado"    onclick="javascript:FiltrarTabla();">Filtrar Tabla</button> </a>
                <a style="margin-left: 5%;">  <button  class="btn btn-primary" id="botonrestablecer" onclick="javascript:RestablecerTabla();">Restablecer Tabla</button> </a>

            </div>
                
            <label id="totalsaldoadeudados" style="margin-top:10px; margin-left: 40%;"> Total de los saldos adeudados: <strong> <?php echo bcadd( $totalsaldoadeudados[0]->totalsaldoadeudados, '0', 2 ) ?> </strong>  </label>
            
        </div>

            <!-- Alerta -->
        <div class="container"  id="contentalert" style="display: none;">
            <div class="alert alert-danger alert-dismissible" role="alert" style="margin-top: 1%" >
                <input class="form-control-plaintext" value = "" id="textoalerta" disabled>    
                <button type="button" class="close" onclick="javascript:OcultarAlerta();" style="margin-top: 5px">
                <span>&times;</span>
                </button>
            </div>
        </div>
        
            <!-- TOOLTIPTEXT -->
        <div class="container" style="margin-top: 10px;">
            
            <div class="row justify-content-center">
                <label class="textotooltip">
                    HAGA DOBLE CLICK SOBRE UN CLIENTE PARA VER LA INFORMACIÓN DE SU CRÉDITO
                </label>
            </div>
        </div>

        <!-- Tabla -->
        <div class="container" style="margin-top: 10px;">
            
            <div class="row justify-content-center">
                                    
                <div class="scrollbar" id="scrollbar-style">

                    <table class="table table-hover" style="width: 1000px" id="tablacreditosclientes" name="tablacreditosclientes" ondblclick="javascript:FilaSeleccionada();">
                        <thead  class="thead-dark">
                            <tr>
                                <th scope="col" style="display: none;"> IDCREDITOCLIENTE </th>
                                <th scope="col" style="display: none;"> IDCLIENTE </th>
                                <th scope="col"> Nombre del cliente </th>
                                <th scope="col"> Dirección del cliente  </th>
                                <th scope="col"> Teléfono de la venta </th>
                                <th scope="col"> Límite del crédito </th>
                                <th scope="col"> Cantidad adeudada </th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ( $listadocreditosclientes as $creditoscliente )
                                <tr>
                                    <td style="display: none;">{{ $creditoscliente->idcreditocliente }}</td>
                                    <td style="display: none;">{{ $creditoscliente->idcliente }}</td>
                                    <td>{{ $creditoscliente->nombrecompleto }}</td>
                                    <td>{{ $creditoscliente->direccion }}</td>
                                    <td>{{ $creditoscliente->telefono }}</td>
                                    <td> <?php echo bcadd(  $creditoscliente->limite ,'0',2); ?></td>
                                    <td> <?php echo bcadd(  $creditoscliente->cantidadadeudada ,'0',2); ?></td>   
                                </tr>
                            @endforeach
                        </tbody>
                    </table>

                </div>
            
            </div>
        </div>

        <br><br><br>

    @endsection

@endif