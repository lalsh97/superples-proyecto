@if( Auth::user()->Nivel == 1 )
    
    @extends('home')

    @section('title')
        Desactivación de cliente
    @endsection

    @section('contenido')

        <script>

            function MostrarModalConfirmacionEliminacion() {

                var idcliente = document.getElementById("idcliente").value;

                var ruta  = "{{ route('verificarcreditocliente') }}";
                var url = ruta + "/" + idcliente;

                //Realiza la búsqueda
                $.get( url ).done( function( data ) {
                    var content = JSON.parse( JSON.stringify( data ) );

                    //Verificar si el contenido de la respuesta de la api no esta vacía.
                    if( content.length > 0 ) {

                        var total = content[0].total;

                        var nombreproveedor = document.getElementById("nombre").value + " " + document.getElementById("apellidopat").value + " " + document.getElementById("apellidomat").value;
                        var cantidadadeudada = document.getElementById("cantidadadeudada").value;
                        cantidadadeudada = ( Math.round( cantidadadeudada * 100 ) / 100 ).toFixed( 2 );

                        //El cliente aún tiene un crédito activo.
                        if( total > 0 ) {

                            if( cantidadadeudada > 0 ) {

                                document.getElementById("textoerrorbody").innerHTML =   "<p>El cliente <strong> " + nombreproveedor + "</strong> tiene un crédito activo y aún debe: <strong>$" + cantidadadeudada + "</strong></p>" +
                                                                                        "<p>¿Está seguro que desea eliminarlo y desactivar su crédito?</p>";

                            } else if( cantidadadeudada <= 0 ) {

                                document.getElementById("textoerrorbody").innerHTML =   "<p>El cliente <strong> " + nombreproveedor + "</strong> tiene un crédito activo </p>" +
                                                                                        "<p>¿Está seguro que desea eliminarlo y desactivar su crédito?</p>";
                            }
                            

                        } else if( totalproductos <= 0 ) {//El cliente no tiene un crédito activo.


                            document.getElementById("textoerrorbody").innerHTML =   "<p>¿Está seguro que desea eliminar al cliente: <strong>" + nombreproveedor + "</strong> ? </p>";

                        }

                        $('#ModalConfirmacionEliminacion').modal('show');

                    } else {

                    }

                }).catch(function( error) {
                    console.log("El error es: ", error);
                });

            }

            function DismissModalConfirmacionEliminacion() {

                $("#ModalConfirmacionEliminacion").modal('hide');

            }

            function EliminarCliente() {

                $("#ModalConfirmacionEliminacion").modal('hide');

                var idcliente = document.getElementById("idcliente").value;

                location.href ="/eliminarcambiarestadocliente/" + idcliente;

            }

            function Regresar() {
                location.href = "/listadoclientes";
            }

        </script>


        @if(Session::has('success'))
            <div class="alert alert-success">
                {{session('success')}}
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            </div>
        @elseif(Session::has('danger'))
            <div class="alert alert-danger">
                {{session('danger')}}
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            </div>
        @elseif(Session::has('warning'))
            <div class="alert alert-warning">
                {{session('warning')}}
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            </div>
        @endif

        <!-- INFORMACION CLIENTE -->
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header"  align="center">{{ __('Desactivación del cliente') }}</div>
                        <div class="card-body">

                            {!!Form::open(array('name' => 'formcliente', 'url'=>'cambiarestadocliente/'.$cliente->idCliente,'method'=>'PUT' ,'autocomplete'=>'off'))!!}

                                <fieldset class="border p-2">
                                    <legend class="w-auto"> Datos personales </legend>

                                    <!-- ID CLIENTE -->
                                    <div class="form-group row" style="display: none;">
                                        {!! Form::label( 'idcliente','Id cliente', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                        <div class="col-md-6">
                                        <input type="text" id="idcliente" name="idcliente" class="form-control" value="{{($cliente->idCliente)}}" readonly>
                                        </div>
                                    </div>
                                    
                                    <!-- Nombre del cliente  -->
                                        <div class="form-group row" >
                                            {!! Form::label( 'nombre','Nombre del cliente', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                            <div class="col-md-6">
                                            <input type="text" id="nombre" name="nombre" class="form-control" readonly  value="{{ ($cliente->Nombre) }}" >
                                            </div>
                                        </div>

                                        <!-- Apellido Paterno del cliente -->
                                    <div class="form-group row" >
                                        {!! Form::label( 'apellidopat','Apellido paterno del cliente', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                        <div class="col-md-6">
                                        <input type="text" id="apellidopat" name="apellidopat" class="form-control" value="{{($cliente->ApellidoPat)}}" readonly>
                                        </div>
                                    </div>

                                    <!-- Apellido Materno del cliente -->
                                    <div class="form-group row" >
                                        {!! Form::label( 'apellidomat','Apellido materno del cliente', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                        <div class="col-md-6">
                                        <input type="text" id="apellidomat" name="apellidomat" class="form-control" readonly  value="{{($cliente->ApellidoMat) }}">
                                        </div>
                                    </div>

                                    <!-- Fecha de nacimiento del cliente -->
                                    <div class="form-group row" >
                                        {!! Form::label( 'fechanac','Fecha de nacimiento del cliente', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                        <div class="col-md-6">
                                        <input type="date" id="fechanac" name="fechanac" class="form-control"    value="{{ ($cliente->FechaNac) }}" readonly>
                                        </div>
                                    </div>

                                    <!-- Sexo del cliente -->
                                    <div class="form-group row" >
                                        {!! Form::label( 'sexo','Sexo del clienre', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                        <div class="col-md-6">
                                            <label> <input type="text" name="sexo" id="sexo" readonly class="form-control"
                                            <?php
                                                if( $cliente->Sexo == 'H' ) {
                                            ?>
                                                    value = "Hombre";
                                            <?php
                                            } else {
                                            ?>
                                                    value = "Mujer";
                                            <?php
                                                }
                                            ?>
                                            >
                                        </div>
                                    </div>

                                    <!-- Curp  del cliente -->
                                    <div class="form-group row" >
                                        {!! Form::label( 'curp','Curp del cliente', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                        <div class="col-md-6">
                                        <input type="text" id="curp" name="curp"  readonly class="form-control"   value="{{($cliente->Curp) }}" >
                                        </div>
                                    </div>

                                    <!-- Teléfono del cliente  -->
                                    <div class="form-group row">
                                        {!! Form::label( 'telefono','Teléfono del cliente', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                        <div class="col-md-6">
                                            <input type="text" id="telefono" name="telefono" placeholder="Ingrese el número telefónico del cliente" class="form-control"
                                            value="{{($cliente->Telefono )}}" maxlength="12" minlength="1" pattern="[\d]{1,12}"
                                            oninvalid="setCustomValidity('El telefono es obligatorio y debe tener como máximo 12 digitos')"
                                            oninput="setCustomValidity('')" readonly>
                                        </div>
                                    </div>

                                    <!-- Credito del cliente -->
                                    <div class="form-group row">
                                        {!! Form::label('preciocosto','Crédito máximo aprobado', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                        <div class="col-md-6">
                                            <input type="text" id="limite" name="limite" class="form-control" value="{{ ( $cliente->Limite ) }}" readonly>
                                        </div>
                                    </div>

                                    <!-- Cantidad Adeudada del cliente -->
                                    <div class="form-group row" style="display: none;">
                                        {!! Form::label('preciocosto','Credito máximo aprobado', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                        <div class="col-md-6">
                                            <input type="text" id="cantidadadeudada" name="cantidadadeudada" class="form-control" value="{{ ( $cliente->CantidadAdeudada ) }}" readonly>
                                        </div>
                                    </div>

                                </fieldset>

                                <fieldset class="border p-2">
                                    <legend class="w-auto"> Datos del domicilio </legend>

                                    <!-- Calle -->
                                    <div class="form-group row" >
                                        {!! Form::label( 'calle','Nombre de la calle', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                        <div class="col-md-6">
                                        <input type="text" id="calle" name="calle"  placeholder="Ingrese el nombre de la calle" class="form-control"
                                        value="{{($cliente->Calle )}}" maxlength="45" minlength="1" required pattern="[A-Za-z\s\W]{1,45}"
                                            oninvalid="setCustomValidity('La nombre de la calle es obligatorio y debe tener como máximo 45 carácteres')"
                                            oninput="setCustomValidity('')" readonly>
                                        </div>
                                    </div>

                                    <!-- Número -->
                                    <div class="form-group row" >
                                        {!! Form::label( 'numero','Número del domicilio', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                        <div class="col-md-6">
                                        <input type="number" id="numero" name="numero"  placeholder="Ingrese el numero del domicilio" class="form-control"
                                            value="{{($cliente->Numero )}}"min="1" max="9999" required pattern="[\d]{1,4}"
                                            oninvalid="setCustomValidity('El número del domicilio es obligatorio y debe tener como máximo 4 digitos')"
                                            oninput="setCustomValidity('')" readonly>
                                        </div>
                                    </div>

                                    <!-- Código Postal -->
                                    <div class="form-group row" >
                                        {!! Form::label( 'codigop','Código Postal del domicilio', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                        <div class="col-md-6">
                                        <input type="text" id="codigop" name="codigop"  placeholder="Ingrese el código postal del domicilio" class="form-control"
                                            value="{{($cliente->CodigoP )}}" maxlength="5" minlength="1" required pattern="[\d]{1,5}"
                                            oninvalid="setCustomValidity('El código postal del domicilio es obligatorio y debe tener como máximo 5 digitos')"
                                            oninput="setCustomValidity('')" readonly>
                                        </div>
                                    </div>

                                    <!-- Colonia -->
                                    <div class="form-group row" >
                                        {!! Form::label( 'colonia','Nombre de la colonia', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                        <div class="col-md-6">
                                        <input type="text" id="colonia" name="colonia"  placeholder="Ingrese el nombre de la colonia" class="form-control"
                                            value="{{($cliente->Colonia )}}"maxlength="45" minlength="1" required pattern="[\w\s]{1,45}"
                                            oninvalid="setCustomValidity('El nombre de la colonia es obligatorio y debe tener como máximo 45 carácteres')"
                                            oninput="setCustomValidity('')" readonly>
                                        </div>
                                    </div>

                                    <!-- Municipio -->
                                    <div class="form-group row" >
                                        {!! Form::label( 'municipio','Nombre del municipio', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                        <div class="col-md-6">
                                        <input type="text" id="municipio" name="municipio"  placeholder="Ingrese el nombre del municipio" class="form-control"
                                        value="{{($cliente->Municipio )}}" maxlength="45" minlength="1" required pattern="[\w\s]{1,45}"
                                            oninvalid="setCustomValidity('El nombre del municipio es obligatorio y debe tener como máximo 45 carácteres')"
                                            oninput="setCustomValidity('')" readonly>
                                        </div>
                                    </div>

                                    <!-- Ciudad -->
                                    <div class="form-group row" >
                                        {!! Form::label( 'ciudad','Nombre de la ciudad', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                        <div class="col-md-6">
                                        <input type="text" id="ciudad" name="ciudad"  placeholder="Ingrese el nombre de la ciudad" class="form-control"
                                        value="{{($cliente->Ciudad )}}" maxlength="45" minlength="1" required pattern="[\w\s]{1,45}"
                                            oninvalid="setCustomValidity('El nombre de la ciudad es obligatorio y debe tener como máximo 45 carácteres')"
                                            oninput="setCustomValidity('')" readonly>
                                        </div>
                                    </div>


                                </fieldset>

                                <br>

                            {!!Form::close()!!}

                            <!-- Botones -->
                            <div class="form-group row">
                                <div class="col-md-4 col-form-label text-md-right">
                                    <button name="botonformdescontinuar" value="Descontinuar" type="submit" class="btn btn-success" style="margin-right:50px" formnovalidate onclick="javascript:MostrarModalConfirmacionEliminacion();">Desactivar</button>
                                </div>
                                <div class="col-md-4 col-form-label text-md-right">
                                    <button name="botonformdescontinuar" value="Cancelar" type="submit" class="btn btn-danger" style="margin-left:250px" formnovalidate onclick="javascript:Regresar();">Cancelar</button>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <!-- ModalConfirmacionEliminacion -->
        <div class="modal" tabindex="-1" role="dialog" id="ModalConfirmacionEliminacion">
            <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                <div class="modal-content modal-lg">

                    <!--Header-->
                    <div class="modal-header" id="modalheader" style="background: rgb(33, 37, 41)">
                        <h5 id="textostringheader" class="heading" style="margin-left: 30%; color: white; text-align: center;">CONFIRMACIÓN DE ELIMINACIÓN</h5>

                        <button type="button" class="close" onclick="javascript:DismissModalConfirmacionEliminacion();" data-dismiss="modal" aria-label="Close">
                            <span  aria-hidden="true" style="color: rgb(255, 255, 255)" >&times;</span>
                        </button>
                    </div>

                    <!--Body-->
                    <div class="modal-body" id="modalbody">
                        <div class="row">
                            <div class="col">
                                <p id="textoerrorbody" style="text-align: center;"></p>
                            </div>
                        </div>

                    </div>

                    <!--Footer-->
                    <div class="modal-footer" id="modalfooter">
                        <button type="button" class="btn btn-primary" data-dismiss="modal" id="botonEliminar" onclick="javascript:EliminarCliente();">DESACTIVAR</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal" id="botonCancelar" onclick="javascript:DismissModalConfirmacionEliminacion();" style="margin-left: 70%;">CANCELAR</button>
                    </div>

                </div>
            </div>
        </div>

    @endsection

@endif
