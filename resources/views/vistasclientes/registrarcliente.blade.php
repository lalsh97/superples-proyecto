@if( Auth::user()->Nivel == 1 )

    @extends('home')

    @section('title')
        Registro de cliente
    @endsection

    @section('contenido')
        
        <style>
            strong {

                color: red;
                font-size: 20px;

                }

        </style>
        
        <script>

            window.onload = function( e ) {
                
                var fechacasolimite = new Date();                
                var fechacasolimitemin = new Date();
                fechacasolimite.setFullYear( fechacasolimite.getFullYear() - 18 );     
                fechacasolimite.setDate( fechacasolimite.getDate() - 1 );           
                fechacasolimitemin.setFullYear( fechacasolimitemin.getFullYear() - 90 );

                fechacasolimite = new Date( fechacasolimite ).toISOString().split("T")[0];
                fechacasolimitemin = new Date( fechacasolimitemin ).toISOString().split("T")[0];
                
                //console.log("FECHA: ", fechacasolimite );                
                document.getElementById('fechanac').max = fechacasolimite;
                document.getElementById("fechanac").min = fechacasolimitemin;

            }

            function generarcurp() {

                var curpgenerada = "";

                if( $('#nombre').val() != "" && $('#apellidopat').val() != ""  ) {

                    var primeraletraapellidopat;
                    var vocalapellidopat;
                    var primeraletraapellidomat;
                    var primeraletranombre;
                    var fechanacimientodigitos;
                    var caractersexo;
                    var entidaddenacimiento;
                    var consonanteinternaapellidopat;
                    var consonanteinternaapellidomat;
                    var consonanteinternanombre;
                    var  pcl = ""; // primerascuatroletras
                    var expreg = /(BACA|BAKA|BUEI|BUEY|CACA|CACO|CAGA|CAGO|CAKA|CAKO|COGE|COGI|COJA|COJE|COJI|COJO|COLA|CULO|FALO|FETO|GETA|GUEI|GUEY|JETA|JOTO|KACA|KACO|KAGA|KAGO|KAKA|KAKO|KOGE|KOGI|KOJA|KOJE|KOJI|KOJO|KOLA|KULO|LILO|LOCA|LOCO|LOKA|LOKO|MAME|MAMO|MEAR|MEAS|MEON|MIAR|MION|MOCO|MOKO|MULA|MULO|NACA|NACO|PEDA|PEDO|PENE|PIPI|PITO|POPO|PUTA|PUTO|QULO|RATA|ROBA|ROBE|ROBO|RUIN|SENO|TETA|VACA|VAGA|VAGO|VAKA|VUEI|VUEY|WUEI|WUEY)/;

                    var textonombre = document.getElementById('nombre').value.toUpperCase();
                        textonombre = textonombre.replace("Á","A");
                        textonombre = textonombre.replace("É","E");
                        textonombre = textonombre.replace("Í","I");
                        textonombre = textonombre.replace("Ó","O");
                        textonombre = textonombre.replace("Ú","U");

                    var textoapellidopat = document.getElementById('apellidopat').value.toUpperCase();
                        textoapellidopat = textoapellidopat.replace("Á","A");
                        textoapellidopat = textoapellidopat.replace("É","E");
                        textoapellidopat = textoapellidopat.replace("Í","I");
                        textoapellidopat = textoapellidopat.replace("Ó","O");
                        textoapellidopat = textoapellidopat.replace("Ú","U");

                    var textoapellidomat = document.getElementById('apellidomat').value.toUpperCase();
                        textoapellidomat = textoapellidomat.replace("Á","A");
                        textoapellidomat = textoapellidomat.replace("É","E");
                        textoapellidomat = textoapellidomat.replace("Í","I");
                        textoapellidomat = textoapellidomat.replace("Ó","O");
                        textoapellidomat = textoapellidomat.replace("Ú","U");

                            // PRIMER CARACTER - Primera letra primer apellido.
                    var auxletra = textoapellidopat.substr(0,1);
                    if( auxletra == 'Ñ' ) {
                        primeraletraapellidopat = 'X';
                    } else {
                        primeraletraapellidopat = textoapellidopat.substr(0,1);
                    }

                            //  SEGUNDO CARACTER - Primera vocal interna del primer apellido.
                    for( var i = 1; i< textoapellidopat.length; i++ ) {
                        if( textoapellidopat.charAt(i) == 'A' || textoapellidopat.charAt(i) == 'E' || textoapellidopat.charAt(i) == 'I' || textoapellidopat.charAt(i) == 'O' || textoapellidopat.charAt(i) == 'U' ) {
                                vocalapellidopat = textoapellidopat.charAt(i);
                                break;
                        }
                    }

                    if( vocalapellidopat == null ) {
                        vocalapellidopat = 'X';
                    }

                            //  TERCER CARACTER - Primera letra del segundo apellido.
                    if( textoapellidomat != "" ) {
                        auxletra = textoapellidomat.substr(0,1);
                        if( auxletra == 'Ñ' ) {
                            primeraletraapellidomat = 'X';
                        } else {
                            primeraletraapellidomat = textoapellidomat.substr(0,1);
                        }
                    } else {
                        primeraletraapellidomat = 'X';
                    }


                            //  CUARTO CARACTER - Primera letra del nombre
                    var arraynombres = textonombre.split(" ");

                    if( arraynombres.length > 1 ) {
                        var nombreingresado = arraynombres[0];
                        if( nombreingresado == 'MARIA' || nombreingresado == 'JOSE' ) {
                            auxletra = arraynombres[1].substr(0,1);
                            if( auxletra == 'Ñ' ) {
                                primeraletranombre = 'X';
                            } else {
                                primeraletranombre =  arraynombres[1].substr(0,1);
                            }
                        } else {
                            auxletra = arraynombres[0].substr(0,1);
                            if( auxletra == 'Ñ' ) {
                                primeraletranombre = 'X';
                            } else {
                                primeraletranombre = arraynombres[0].substr(0,1);
                            }
                        }
                    } else {
                        auxletra = arraynombres[0].substr(0,1);
                        if( auxletra == 'Ñ' ) {
                            primeraletranombre = 'X';
                        } else {
                            primeraletranombre = arraynombres[0].substr(0,1);
                        }
                    }

                    pcl = pcl.concat (primeraletraapellidopat, vocalapellidopat,
                        primeraletraapellidomat, primeraletranombre );

                    var resexpreg = expreg.test( pcl );
                    if( resexpreg ) {
                        pcl = "";
                        pcl = pcl.concat (primeraletraapellidopat, "X",
                        primeraletraapellidomat, primeraletranombre );
                    }


                            // QUINTO AL DECIMO CARACTER - año, mes, día.
                    //fecha = document.getElementById('fechanac').value;
                    fecha = $('#fechanac').val();

                    arrayfecha = fecha.split("-");

                    digitoanio = arrayfecha[0];
                    digitoanio = digitoanio.substr( 2, digitoanio.length );
                    digitomes = arrayfecha[1];
                    digitodia = arrayfecha[2];

                    if( fecha == "" ) {
                        fechanacimientodigitos = "******";
                    } else {
                        fechanacimientodigitos = "";
                        fechanacimientodigitos = fechanacimientodigitos.concat( digitoanio, digitomes, digitodia );
                    }

                        //ONCEAVO CARACTER - H -> hombre / M -> mujer

                    if( document.getElementById('sexoh').checked == true ) {
                        caractersexo = "H";
                    } else if( document.getElementById('sexom').checked == true ) {
                        caractersexo = "M";
                    } else {
                        caractersexo = "-";
                    }

                            //  DOCEAVO Y TRECEAVO CARACTER - Letra inicial y última consonante del estado de nacimiento.
                    entidaddenacimiento = 'OC';

                            //CATORCEAVO CARACTER - Primera consonante interna del primer apellido
                    for( var i = 1; i< textoapellidopat.length; i++ ) {
                        if( textoapellidopat.charAt(i) == 'A' || textoapellidopat.charAt(i) == 'E' || textoapellidopat.charAt(i) == 'I' || textoapellidopat.charAt(i) == 'O' || textoapellidopat.charAt(i) == 'U' ) {
                        } else {
                            consonanteinternaapellidopat = textoapellidopat.charAt(i);
                            break;

                        }
                    }
                    if( consonanteinternaapellidopat == null || consonanteinternaapellidopat == 'Ñ' ) {
                        consonanteinternaapellidopat = 'X';
                    }

                            //QUINCEAVO CARACTER - Primera consonante interna del segundo apellido
                    for( var i = 1; i< textoapellidomat.length; i++ ) {
                        if( textoapellidomat.charAt(i) == 'A' || textoapellidomat.charAt(i) == 'E' || textoapellidomat.charAt(i) == 'I' || textoapellidomat.charAt(i) == 'O' || textoapellidomat.charAt(i) == 'U' ) {
                        } else {
                            consonanteinternaapellidomat = textoapellidomat.charAt(i);
                            break;
                        }
                    }
                    if( consonanteinternaapellidomat == null || consonanteinternaapellidomat == 'Ñ' ) {
                        consonanteinternaapellidomat = 'X';
                    }

                            //DIECISEISAVO CARACTER - Primera consonante interna del nombre
                    arraynombres = textonombre.split(" ");

                    if( arraynombres.length > 1 ) {
                        var nombreingresado = arraynombres[0];
                        if( nombreingresado == 'MARIA' || nombreingresado == 'JOSE' ) {
                            auxnombre = arraynombres[1];
                            var contador = 0;
                            for( var i = 0; i< auxnombre.length; i++ ) {
                                if( auxnombre.charAt(i) == 'A' || auxnombre.charAt(i) == 'E' || auxnombre.charAt(i) == 'I' || auxnombre.charAt(i) == 'O' || auxnombre.charAt(i) == 'U' ) {
                                } else {
                                    contador++;
                                    if( contador == 1 ) {
                                        consonanteinternanombre = auxnombre.charAt(i);
                                        break;
                                    }
                                }
                            }
                        } else {
                            for( var i = 1; i< textonombre.length; i++ ) {
                                if( textonombre.charAt(i) == 'A' || textonombre.charAt(i) == 'E' || textonombre.charAt(i) == 'I' || textonombre.charAt(i) == 'O' || textonombre.charAt(i) == 'U' ) {
                                } else {
                                    consonanteinternanombre = textonombre.charAt(i);
                                    break;
                                }
                            }
                        }
                    } else {
                        for( var i = 1; i< textonombre.length; i++ ) {
                            if( textonombre.charAt(i) == 'A' || textonombre.charAt(i) == 'E' || textonombre.charAt(i) == 'I' || textonombre.charAt(i) == 'O' || textonombre.charAt(i) == 'U' ) {
                            } else {
                                consonanteinternanombre = textonombre.charAt(i);
                                break;
                            }
                        }
                    }

                    if( consonanteinternanombre == null || consonanteinternanombre == 'Ñ' ) {
                        consonanteinternanombre = 'X';
                    }


                    curpgenerada = curpgenerada.concat( pcl, fechanacimientodigitos,
                        caractersexo, entidaddenacimiento, consonanteinternaapellidopat,
                        consonanteinternaapellidomat, consonanteinternanombre,"00" );

                    if( curpgenerada.includes("-") ) {
                        curpgenerada = "";
                    } else {
                        document.getElementById('curp').value = curpgenerada;
                        mayusculas();
                    }

                }

            }

            function mayusculas() {

                var textocurp = document.getElementById('curp').value;
                textocurp = textocurp.toUpperCase();

                document.getElementById('curp').value = textocurp;
            }

            function consultar() {

                var endpoint_sepomex  = "http://api-sepomex.hckdrk.mx/query/";
                var method_sepomex = 'info_cp/';
                //var cp = "09810";
                var cp = document.getElementById('codigop').value;
                var variable_string = '?type=simplified';
                var url = endpoint_sepomex + method_sepomex + cp + variable_string;

                $.get( url ).done( function( data ) {
                    var content = JSON.parse( JSON.stringify( data ) );

                    //console.log( content );
                    //document.getElementById('colonia').value = content.response.asentamiento;
                    var opcionescolonia = content.response.asentamiento;

                    var selectcolonias = document.getElementById( 'colonia' );

                    for ( var i = selectcolonias.length; i >= 0; i--) {
                        selectcolonias.remove(i);
                    }

                    if( opcionescolonia.length > 0 ) {
                        var optioncolonia = document.createElement('option');
                                optioncolonia.text = "Seleccione una colonia";
                                optioncolonia.value = "";
                                selectcolonias.add( optioncolonia );

                        for( var i = 0; i < opcionescolonia.length; i++ ) {
                            var optioncolonia = document.createElement( 'option' );
                                optioncolonia.text = opcionescolonia[i];
                                optioncolonia.value = opcionescolonia[i];
                                selectcolonias.add( optioncolonia );

                        }

                    }

                    if( content.response.ciudad == "" && content.response.municipio != "" ) {

                        document.getElementById('ciudad').value = "Oaxaca de Juárez";
                        document.getElementById('municipio').value = content.response.municipio;

                    } else if( content.response.ciudad != "" && content.response.municipio != "" ) {

                        document.getElementById('ciudad').value = content.response.ciudad;
                        document.getElementById('municipio').value = content.response.municipio;

                    }
                }).catch(function( error) {

                    var selectcolonias = document.getElementById( 'colonia' );

                    for ( var i = selectcolonias.length; i >= 0; i--) {
                        selectcolonias.remove(i);
                    }

                    var optioncolonia = document.createElement('option');
                        optioncolonia.text = "Seleccione una colonia";
                        optioncolonia.value = "";
                        selectcolonias.add( optioncolonia );

                    optioncolonia = document.createElement('option');
                        optioncolonia.text = "DOMICILIO CONOCIDO";
                        optioncolonia.value = "DOMICILIO CONOCIDO";
                        selectcolonias.add( optioncolonia );

                    document.getElementById('ciudad').value = "";
                    document.getElementById('municipio').value = "";

                });

            }

        </script>

        @if(Session::has('success'))
            <div class="alert alert-success">
                {{session('success')}}
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            </div>
        @endif

        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header"  align="center">{{ __('Registro de cliente') }}</div>
                            <div class="card-body">

                                {!!Form::open(array('name' => 'formcliente', 'url'=>'insertarcliente','method'=>'POST' ,'autocomplete'=>'off'))!!}

                                    <!-- DATOS PERSONALES -->
                                    <fieldset  class="border p-2">
                                        <legend  class="w-auto"> Datos personales </legend>

                                        <!-- Nombre del cliente  -->
                                        <div class="form-group row" >
                                          <label for="nombre" class="col-md-4 col-form-label text-md-right">{{ __('Nombre(s) del cliente ') }}<strong>*</strong></label>
                                            <div class="col-md-6">
                                            <input type="text" id="nombre" name="nombre"  placeholder="Ingrese el nombre del cliente" class="form-control"
                                                value="{{ old('nombre') }}" maxlength="45" minlength="1" required pattern="[\w\s\u00C0-\u00FF]{1,45}"
                                                onkeypress="return !(event.charCode >= 48 && event.charCode <= 57)"
                                                  oninvalid="setCustomValidity('El nombre es obligatorio y debe tener como máximo 45 carácteres')"
                                                oninput="setCustomValidity('')">
                                            </div>
                                        </div>

                                            <!-- Apellido Paterno del cliente -->
                                        <div class="form-group row" >
                                          <label for="apellidopat" class="col-md-4 col-form-label text-md-right">{{ __('Apellido paterno ') }}<strong>*</strong></label>
                                          <div class="col-md-6">
                                            <input type="text" id="apellidopat" name="apellidopat"  placeholder="Ingrese el apellido paterno del cliente" class="form-control"
                                                value="{{ old('apellidopat') }}" maxlength="45" minlength="1" required pattern="[\w\s\u00C0-\u00FF]{1,45}"
                                                onkeypress="return !(event.charCode >= 48 && event.charCode <= 57)"
                                                oninvalid="setCustomValidity('El apellido paterno es obligatorio y debe tener como máximo 45 carácteres')"
                                                oninput="setCustomValidity('')">
                                            </div>
                                        </div>

                                            <!-- Apellido Materno del cliente -->
                                        <div class="form-group row" >
                                            {!! Form::label( 'apellidomat','Apellido materno del cliente', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                            <div class="col-md-6">
                                            <input type="text" id="apellidomat" name="apellidomat"  placeholder="Ingrese el apellido materno del cliente" class="form-control"
                                                value="{{ old('apellidomat') }}" maxlength="45" minlength="1" pattern="[\w\s\u00C0-\u00FF]{1,45}"
                                                onkeypress="return !(event.charCode >= 48 && event.charCode <= 57)"
                                                  oninvalid="setCustomValidity('El apellido materno debe tener como máximo 45 carácteres')"
                                                oninput="setCustomValidity('')">
                                            </div>
                                        </div>

                                            <!-- Fecha de nacimiento del cliente -->
                                        <div class="form-group row" >
                                          <label for="fechanac" class="col-md-4 col-form-label text-md-right">{{ __('Fecha de nacimiento ') }}<strong>*</strong></label>
                                            <div class="col-md-6">
                                            <input type="date" id="fechanac" name="fechanac" class="form-control"
                                                max="2000-12-31" min="1940-01-01" required
                                                value="{{ old('fechanac') }}">
                                            </div>
                                        </div>

                                            <!-- Sexo del cliente -->
                                        <div class="form-group row" >
                                            {!! Form::label( 'sexo','Sexo del cliente', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                            <div class="col-md-6">
                                                <label> <input type="radio" name="sexo" id="sexoh" value="H" value="{{ old('sexo') }}"
                                                oninvalid="setCustomValidity('El sexo es obligatorio, selecciona una de las opciones')"
                                                oninput="setCustomValidity('')"  style="margin-left: 20px"
                                                <?php
                                                    if( old('sexo') == 'H' ) {
                                                ?>
                                                        checked = "true";
                                                <?php
                                                    }
                                                ?>
                                                > Hombre </label>
                                                <label> <input type="radio" name="sexo" id="sexom" value="M" value="{{ old('sexo') }}" style="margin-left: 20px"
                                                <?php
                                                    if( old('sexo') == 'M' ) {
                                                ?>
                                                        checked = "true";
                                                <?php
                                                    }
                                                ?>
                                                > Mujer </label>
                                            </div>
                                        </div>

                                            <!-- Curp  del cliente -->
                                        <div class="form-group row" >
                                          <label for="curp" class="col-md-4 col-form-label text-md-right">{{ __('Curp del cliente ') }}<strong>*</strong></label>
                                            <div class="col-md-6">
                                            <input type="text" id="curp" name="curp"  placeholder="Ingrese la curp del cliente" class="form-control"
                                                value="{{ old('curp') }}" maxlength="18"
                                                   pattern="^([A-Z]{1})([AEIOU]{1})([A-Z]{2})([0-9]{2})(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1])([HM]{1})([AS|BC|BS|CC|CS|CH|CL|CM|DF|DG|GT|GR|HG|JC|MC|MN|MS|NT|NL|OC|PL|QT|QR|SP|SL|SR|TC|TS|TL|VZ|YN|ZS|NE]{2})([^A|E|I|O|U]{3})([A-Z\d]{1})([0-9]{1})$"
                                                oninvalid="setCustomValidity('La curp es obligatoria, debe respetar el formato de la curp y tener 18 carácteres')"
                                                oninput="setCustomValidity('')"
                                                onfocus="javascript:generarcurp()"
                                                >
                                            </div>
                                        </div>


                                        <!-- Teléfono del cliente  -->
                                        <div class="form-group row">
                                          <label for="telefono" class="col-md-4 col-form-label text-md-right">{{ __('Teléfono del cliente ') }}<strong>*</strong></label>
                                          <div class="col-md-6">
                                                <input type="text" id="telefono" name="telefono" placeholder="Ingrese el número telefónico del cliente" class="form-control"
                                                value="{{session('telefono')}}" maxlength="12" minlength="1" required pattern="[\d]{1,12}"
                                                oninvalid="setCustomValidity('El telefono es obligatorio y debe tener como máximo 12 digitos')"
                                                oninput="setCustomValidity('')" >
                                            </div>
                                        </div>

                                        <!-- Credito del cliente -->
                                        <div class="form-group row">
                                          <label for="limite" class="col-md-4 col-form-label text-md-right">{{ __('Crédito máximo aprobado ') }}<strong>*</strong></label>
                                            <div class="col-md-6">
                                                <input type="number" id="limite" name="limite" placeholder="Ingrese el crédito máximo" class="form-control" max="9999999" min="1" required
                                                oninvalid="setCustomValidity('El máximo del crédito es obligatorio y debe tener como máximo 7 digitos')"
                                                onkeypress="return ( event.charCode >= 48 && event.charCode <= 57 || event.charCode == 46 )"
                                                oninput="setCustomValidity('')">
                                            </div>
                                        </div>

                                    </fieldset>

                                    <!-- DATOS DEL DOMICILIO -->
                                    <fieldset class="border p-2">
                                        <legend class="w-auto"> Datos del domicilio </legend>

                                            <!-- Código Postal -->
                                        <div class="form-group row" >
                                          <label for="codigop" class="col-md-4 col-form-label text-md-right">{{ __('Código Postal ') }}<strong>*</strong></label>
                                          <div class="col-md-6">
                                            <input type="text" id="codigop" name="codigop"  placeholder="Ingrese el código postal del domicilio" class="form-control"
                                                value="{{ old('codigop') }}" maxlength="5" minlength="1" required pattern="[\d]{1,5}"
                                                oninvalid="setCustomValidity('El código postal del domicilio es obligatorio y debe tener como máximo 5 digitos')"
                                                oninput="setCustomValidity('')"
                                                onfocusout="javascript:consultar()">
                                            </div>
                                        </div>

                                            <!-- Calle -->
                                        <div class="form-group row" >
                                            {!! Form::label( 'calle','Nombre de la calle', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                            <div class="col-md-6">
                                            <input type="text" id="calle" name="calle"  placeholder="Ingrese el nombre de la calle" class="form-control"
                                                value="{{ old('calle') }}" maxlength="45" minlength="1"  pattern="[\w\sÁ-ÿ\u00C0-\u00FF]{1,45}"
                                                oninvalid="setCustomValidity('La nombre de la calle es obligatorio y debe tener como máximo 45 carácteres')"
                                                oninput="setCustomValidity('')">
                                            </div>
                                        </div>

                                            <!-- Número -->
                                        <div class="form-group row" >
                                            {!! Form::label( 'numero','Número del domicilio', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                            <div class="col-md-6">
                                            <input type="number" id="numero" name="numero"  placeholder="Ingrese el número del domicilio" class="form-control"
                                                value="{{ old('numero') }}" min="1" max="9999"  pattern="[\d]{1,4}"
                                                oninvalid="setCustomValidity('El número del domicilio es obligatorio y debe tener como máximo 4 digitos')"
                                                oninput="setCustomValidity('')">
                                            </div>
                                        </div>

                                            <!-- Colonia -->
                                        <div class="form-group row" >
                                          <label for="colonia" class="col-md-4 col-form-label text-md-right">{{ __('Nombre de la colonia ') }}<strong>*</strong></label>
                                          <div class="col-md-6">
                                                <select id="colonia" name="colonia" class="form-control" required>
                                                    <option value = ""> Seleccione una colonia </option>
                                                </select>
                                            </div>
                                        </div>

                                            <!-- Municipio -->
                                        <div class="form-group row" >
                                            {!! Form::label( 'municipio','Nombre del municipio', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                            <div class="col-md-6">
                                            <input type="text" id="municipio" name="municipio"  placeholder="Ingrese el nombre del municipio" class="form-control"
                                                value="{{ old('municipio') }}" maxlength="45" minlength="1"  pattern="[\w\sÁ-ÿ\u00C0-\u00FF]{1,45}"
                                                oninvalid="setCustomValidity('El nombre del municipio es obligatorio y debe tener como máximo 45 carácteres')"
                                                oninput="setCustomValidity('')">
                                            </div>
                                        </div>

                                            <!-- Ciudad -->
                                        <div class="form-group row" >
                                            {!! Form::label( 'ciudad','Nombre de la ciudad', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                            <div class="col-md-6">
                                            <input type="text" id="ciudad" name="ciudad"  placeholder="Ingrese el nombre de la ciudad" class="form-control"
                                                value="{{ old('ciudad') }}" maxlength="45" minlength="1"  pattern="[\w\sÁ-ÿ\u00C0-\u00FF]{1,45}"
                                                oninvalid="setCustomValidity('El nombre de la ciudad es obligatorio y debe tener como máximo 45 carácteres')"
                                                oninput="setCustomValidity('')">
                                            </div>
                                        </div>


                                    </fieldset>
                                    <br>

                                    <div class="form-group row">
                                        <div class="col-md-4 col-form-label text-md-right">
                                            <button name="botonformcliente" value="Registrar" type="submit" class="btn btn-success" style="margin-right:50px">Registrar</button>
                                        </div>
                                        <div class="col-md-4 col-form-label text-md-right">
                                            <button name="botonformcliente" value="Cancelar" type="submit" class="btn btn-danger" style="margin-left:250px" formnovalidate>Cancelar</button>
                                        </div>
                                    </div>

                                {!!Form::close()!!}

                            </div>
                    </div>
                </div>
            </div>
        </div>

        <br><br><br>

    @endsection

@endif
