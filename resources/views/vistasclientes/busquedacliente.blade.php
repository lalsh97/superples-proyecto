@extends('home')

    @section('title')
        Búsqueda de cliente
    @endsection

@section('contenido')

    <script src="{{ asset('js/FuncionesClientes/funcionesbusquedacliente.js') }}">
        
    </script>

    @if(Session::has('success'))
        <div class="alert alert-success">
            {{session('success')}}
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        </div>
    @elseif( Session::has('warning'))
        <div class="alert alert-warning">
            {{session('warning')}}
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        </div>
    @elseif( Session::has('danger'))
        <div class="alert alert-danger">
            {{session('danger')}}
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        </div>
    @endif


    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    @if( $operacion == 'descontinuar' )
                        <div class="card-header"  align="center">{{ __('Búsqueda eliminar cliente') }}</div>
                    @elseif( $operacion == 'buscar' )
                        <div class="card-header"  align="center">{{ __('Búsqueda de clientes') }}</div>
                    @elseif( $operacion == 'modificar' )
                        <div class="card-header"  align="center">{{ __('Búsqueda modificar información cliente') }}</div>
                    @endif

                    <div class="card-body">

                    

                            <!-- Buscar cliente por curp -->
                            <div class="form-group"  style="margin-bottom: 10px">
                                <div class="form-check"   style="margin-bottom: 10px">
                                    {!! Form::label( 'codigo','Buscar cliente por CURP', array( 'class' => 'col-md-8 col-form-label', 'onclick' => 'cambiarcheckcodigo2(this);' ) ) !!}
                                </div>

                                <div class="form-group"  id="contentcodigo">
                                    <input type="text" id="curp" name="curp" class="form-control-sm" value="{{  old('curp')  }}" style="margin-left: 35%">
                                </div>
                            </div>


                            <div class="form-group row" style="margin-top: 20px">
                                <div class="col-md-4 col-form-label text-md-right">
                                    @if( $operacion == 'descontinuar' )
                                    <button name="botonbusquedacliente" value="BusquedaDescontinuar" type="submit" class="btn btn-success" style="margin-right:50px" onclick="javascript:IrDescontinuarCliente();">Buscar</button>
                                    @elseif( $operacion == 'buscar' )
                                    <button name="botonbusquedacliente" value="Buscar" type="submit" class="btn btn-success" style="margin-right:50px" onclick="javascript:IrBuscarCliente();">Buscar</button>
                                    @elseif( $operacion == 'modificar' )
                                    <button name="botonbusquedacliente" value="BusquedaModificar" type="submit" class="btn btn-success" style="margin-right:50px" onclick="javascript:IrModificarCliente();">Buscar</button>
                                    @endif
                                </div>
                                <div class="col-md-4 col-form-label text-md-right">
                                    <a href="/home">
                                        <button name="botonbusquedacliente" value="Cancelar" type="submit" class="btn btn-danger" style="margin-left:250px">Cancelar</button>
                                    </a>
                                </div>
                            </div>

                        

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
