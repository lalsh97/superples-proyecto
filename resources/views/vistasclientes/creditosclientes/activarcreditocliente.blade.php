@if( Auth::user()->Nivel == 1 )
    
    @extends('home')

    @section('title')
        Activación del crédito del cliente
    @endsection

    @section('contenido')


        @if(Session::has('success'))
            <div class="alert alert-success">
                {{session('success')}}
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            </div>
        @endif

        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header"  align="center">{{ __('Activación del crédito de cliente') }}</div>
                        <div class="card-body">

                            {!!Form::open(array('name' => 'formactivarcreditocliente', 'url'=>'realizaractivacioncreditocliente/'.$informacioncliente->idCliente,'method'=>'POST' ,'autocomplete'=>'off'))!!}

                                <fieldset class="border p-2">
                                    <legend class="w-auto"> Datos personales </legend>


                                    <!-- ID del cliente  -->
                                    <div class="form-group row" style="display: none;" >
                                        {!! Form::label( 'nombre','ID cliente', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                        <div class="col-md-6">
                                        <input type="text" id="nombre" name="nombre" class="form-control" readonly  value="{{ ($informacioncliente->idCliente) }}" >
                                        </div>
                                    </div>

                                    <!-- Nombre del cliente  -->
                                    <div class="form-group row" >
                                        {!! Form::label( 'nombre','Nombre del cliente', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                        <div class="col-md-6">
                                        <input type="text" id="nombre" name="nombre" class="form-control" readonly  value="{{ ($informacioncliente->Nombre) }}" >
                                        </div>
                                    </div>

                                    <!-- Apellido Paterno del cliente -->
                                    <div class="form-group row" >
                                        {!! Form::label( 'apellidopat','Apellido paterno del cliente', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                        <div class="col-md-6">
                                        <input type="text" id="apellidopat" name="apellidopat" class="form-control" value="{{($informacioncliente->ApellidoPat)}}" readonly>
                                        </div>
                                    </div>

                                    <!-- Apellido Materno del cliente -->
                                    <div class="form-group row" >
                                        {!! Form::label( 'apellidomat','Apellido materno del cliente', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                        <div class="col-md-6">
                                        <input type="text" id="apellidomat" name="apellidomat" class="form-control" readonly  value="{{($informacioncliente->ApellidoMat) }}">
                                        </div>
                                    </div>

                                    <!-- Teléfono del cliente  -->
                                    <div class="form-group row">
                                        {!! Form::label( 'telefono','Teléfono del cliente', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                        <div class="col-md-6">
                                            <input type="text" id="telefono" name="telefono" placeholder="Ingrese el número telefónico del cliente" class="form-control"
                                            value="{{($informacioncliente->Telefono )}}" maxlength="12" minlength="1" pattern="[\d]{1,12}"
                                            oninvalid="setCustomValidity('El telefono es obligatorio y debe tener como máximo 12 digitos')"
                                            oninput="setCustomValidity('')" readonly>
                                        </div>
                                    </div>

                                </fieldset>

                                <fieldset class="border p-2">
                                    <legend class="w-auto"> Datos del domicilio </legend>

                                        <!-- Calle -->
                                    <div class="form-group row" >
                                        {!! Form::label( 'calle','Nombre de la calle', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                        <div class="col-md-6">
                                        <input type="text" id="calle" name="calle"  placeholder="Ingrese el nombre de la calle" class="form-control"
                                        value="{{($informacioncliente->Calle )}}" maxlength="45" minlength="1" required pattern="[A-Za-z\s\W]{1,45}"
                                            oninvalid="setCustomValidity('La nombre de la calle es obligatorio y debe tener como máximo 45 carácteres')"
                                            oninput="setCustomValidity('')" readonly>
                                        </div>
                                    </div>

                                        <!-- Número -->
                                    <div class="form-group row" >
                                        {!! Form::label( 'numero','Número del domicilio', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                        <div class="col-md-6">
                                        <input type="number" id="numero" name="numero"  placeholder="Ingrese el numero del domicilio" class="form-control"
                                            value="{{($informacioncliente->Numero )}}"min="1" max="9999" required pattern="[\d]{1,4}"
                                            oninvalid="setCustomValidity('El número del domicilio es obligatorio y debe tener como máximo 4 digitos')"
                                            oninput="setCustomValidity('')" readonly>
                                        </div>
                                    </div>

                                        <!-- Código Postal -->
                                    <div class="form-group row" >
                                        {!! Form::label( 'codigop','Código postal del domicilio', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                        <div class="col-md-6">
                                        <input type="text" id="codigop" name="codigop"  placeholder="Ingrese el código postal del domicilio" class="form-control"
                                            value="{{($informacioncliente->CodigoP )}}" maxlength="5" minlength="1" required pattern="[\d]{1,5}"
                                            oninvalid="setCustomValidity('El código postal del domicilio es obligatorio y debe tener como máximo 5 digitos')"
                                            oninput="setCustomValidity('')" readonly>
                                        </div>
                                    </div>

                                        <!-- Colonia -->
                                    <div class="form-group row" >
                                        {!! Form::label( 'colonia','Nombre de la colonia', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                        <div class="col-md-6">
                                        <input type="text" id="colonia" name="colonia"  placeholder="Ingrese el nombre de la colonia" class="form-control"
                                            value="{{($informacioncliente->Colonia )}}"maxlength="45" minlength="1" required pattern="[\w\s]{1,45}"
                                            oninvalid="setCustomValidity('El nombre de la colonia es obligatorio y debe tener como máximo 45 carácteres')"
                                            oninput="setCustomValidity('')" readonly>
                                        </div>
                                    </div>

                                        <!-- Municipio -->
                                    <div class="form-group row" >
                                        {!! Form::label( 'municipio','Nombre del municipio', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                        <div class="col-md-6">
                                        <input type="text" id="municipio" name="municipio"  placeholder="Ingrese el nombre del municipio" class="form-control"
                                        value="{{($informacioncliente->Municipio )}}" maxlength="45" minlength="1" required pattern="[\w\s]{1,45}"
                                            oninvalid="setCustomValidity('El nombre del municipio es obligatorio y debe tener como máximo 45 carácteres')"
                                            oninput="setCustomValidity('')" readonly>
                                        </div>
                                    </div>

                                        <!-- Ciudad -->
                                    <div class="form-group row" >
                                        {!! Form::label( 'ciudad','Nombre de la ciudad', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                        <div class="col-md-6">
                                        <input type="text" id="ciudad" name="ciudad"  placeholder="Ingrese el nombre de la ciudad" class="form-control"
                                        value="{{($informacioncliente->Ciudad )}}" maxlength="45" minlength="1" required pattern="[\w\s]{1,45}"
                                            oninvalid="setCustomValidity('El nombre de la ciudad es obligatorio y debe tener como máximo 45 carácteres')"
                                            oninput="setCustomValidity('')" readonly>
                                        </div>
                                    </div>


                                </fieldset>

                                <fieldset>
                                    <legend> Datos del crédito</legend>
                                    
                                    <?php

                                        if( isset( $informacioncliente->LimiteCredito ) ) {
                                            ?>               

                                            <!-- Limite del credito del cliente -->
                                            <div class="form-group row">
                                                {!! Form::label('limite','Límite máximo aprobado', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                                <div class="col-md-6">
                                                    <input type="text" id="limite" name="limite"  class="form-control" readonly                                             
                                                    value="<?php echo bcadd( $informacioncliente->LimiteCredito, '0', 2); ?> ">
                                                </div>
                                            </div>             
                                                                    
                                            <!-- Cantidad adeudada del credito del cliente -->
                                            <div class="form-group row">
                                                {!! Form::label('limite','Cantidad adeudada del crédito', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                                <div class="col-md-6">
                                                    <input type="text" id="cantidadadeudada" name="cantidadadeudada" class="form-control" readonly                                             
                                                    value="<?php echo bcadd( $informacioncliente->CantidadAdeudada, '0', 2); ?> ">
                                                </div>
                                            </div>             
                                                                    
                                            <!-- Estado del credito del cliente -->
                                            <div class="form-group row">
                                                {!! Form::label('limite','Estado del crédito', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                                <div class="col-md-6">
                                                    <input type="text" id="estadocredito" name="estadocredito" class="form-control" readonly                                                
                                                    value=" <?php
                                                    if( $informacioncliente->EstadoCredito == 0 ){
                                                        echo "CREDITO CANCELADO";
                                                    } elseif( $informacioncliente->EstadoCredito == 1 ){
                                                        echo "CREDITO ACTIVO";
                                                    }
                                                    ?>">
                                                </div>
                                            </div>

                                            <?php
                                        } else {
                                            ?>                                        
                                                <!-- Credito del cliente -->
                                                <div class="form-group row">
                                                    {!! Form::label('limite','Credito maximo aprobado', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                                    <div class="col-md-6">
                                                        <input type="text" id="limite" name="limite" placeholder="Ingrese el credito máximo" class="form-control" pattern="^[0-9]+([.][0-9]+)?$" required
                                                        oninvalid="setCustomValidity('El limite del crédito es obligatorio, debe ser una cantidad númerica y debe tener como máximo 7 digitos')"
                                                        onkeypress="return ( event.charCode >= 48 && event.charCode <= 57 || event.charCode == 46 )"
                                                        oninput="setCustomValidity('')">
                                                    </div>
                                                </div>
                                            <?php
                                        }

                                    ?>

                                </fieldset>

                                <br>

                                <!-- Botones -->
                                <div class="form-group row">
                                    <?php
                                        if( isset( $informacioncliente->LimiteCredito ) ) { ?>
                                            
                                            <div class="col-md-4 col-form-label text-md-right">
                                                <button name="botonformactivarcreditocliente" value="Reactivar" type="submit" class="btn btn-success" style="margin-right:50px">Reactivar</button>
                                            </div>
                                    <?php
                                        } else { ?>
                                            <div class="col-md-4 col-form-label text-md-right">
                                                <button name="botonformactivarcreditocliente" value="Activar" type="submit" class="btn btn-success" style="margin-right:50px">Activar</button>
                                            </div>
                                    <?php
                                        }
                                    ?>
                                    
                                    <div class="col-md-4 col-form-label text-md-right">
                                        <button name="botonformactivarcreditocliente" value="Cancelar" type="submit" class="btn btn-danger" style="margin-left:250px">Cancelar</button>
                                    </div>
                                </div>

                            {!!Form::close()!!}

                        </div>
                    </div>
                </div>
            </div>
        </div>

    @endsection

@endif
