@if( Auth::user()->Nivel == 1 )
    
    @extends('home')

    @section('title')
        Listado de clientes sin crédito
    @endsection


    @section('contenido')

        <link rel="STYLESHEET" href="css/estilostooltipylistados.css">


        <script>

            function FiltrarTabla() {
                
                if( document.getElementById("nombrefiltrado").value != "" ) {
                    
                    RealizarFiltradoTabla( document.getElementById("nombrefiltrado").value );

                } else {
                            
                    document.getElementById("textoalerta").value = "Por favor ingrese el nombre a buscar";
                    document.getElementById("contentalert").style.display = "block";

                }

            }

            function RealizarFiltradoTabla( nombre ) {
            
                var arraylistadoclientessincredito = {!! json_encode( $listadoclientessincredito->toArray(), JSON_HEX_TAG ) !!};                    
                VaciarTabla();
                var resultadobusqueda = 0;

                var auxnombrecliente = "";

                for( var i = 0; i < arraylistadoclientessincredito.length; i++ ) {

                    auxnombrecliente = arraylistadoclientessincredito[i].nombrecompleto;
                    auxnombrecliente = auxnombrecliente.toUpperCase();

                    nombre = nombre.toUpperCase();

                    if( auxnombrecliente.includes( nombre ) ) {

                        document.getElementById("tablaclientessincredito").insertRow(-1).innerHTML = 
                        "<td style='display:none;'>" + arraylistadoclientessincredito[i].idcliente + "</td>" +
                        "<td>" + arraylistadoclientessincredito[i].nombrecompleto + "</td>" +
                        "<td>" + arraylistadoclientessincredito[i].direccioncompleta + "</td>" +
                        "<td>" + arraylistadoclientessincredito[i].telefonocliente + "</td>";

                        resultadobusqueda += 1;
                    }
                
                }
                
                const tabla = document.getElementById('tablaclientessincredito');
                var rowCount = tabla.rows.length;

                if( rowCount == 1 ) {
                            
                    document.getElementById("textoalerta").value = "Ningún cliente coincide con el nombre buscado";
                    document.getElementById("contentalert").style.display = "block";

                } else {

                    if( resultadobusqueda == 0 ) {
                        document.getElementById("cantidadfiltrado").innerHTML = "<strong>No se ha encontrado coincidencia</strong> "
                    } else if( resultadobusqueda == 1 ) {
                        document.getElementById("cantidadfiltrado").innerHTML = "<strong>Se ha encontrado "+ resultadobusqueda +" coincidencia</strong> "
                    } else if( resultadobusqueda > 1 ) {                    
                        document.getElementById("cantidadfiltrado").innerHTML = "<strong>Se han encontrado "+ resultadobusqueda +" coincidencias</strong> "
                    }

                    document.getElementById("cantidadfiltrado").style.display = "inline";

                }


            }

            function VaciarTabla() {

                const tabla = document.getElementById('tablaclientessincredito');
                var rowCount = tabla.rows.length;         

                for  ( var x = rowCount - 1; x > 0; x-- ) { 
                    tabla.deleteRow(x); 
                } 

            }

            function RestablecerTabla() {
            
                document.getElementById("cantidadfiltrado").innerHTML = "";

                var arraylistadoclientessincredito = {!! json_encode( $listadoclientessincredito->toArray(), JSON_HEX_TAG ) !!};
                
                VaciarTabla();

                for( var i = 0; i < arraylistadoclientessincredito.length; i++ ) {

                    document.getElementById("tablaclientessincredito").insertRow(-1).innerHTML = 
                    "<td style='display:none;'>" + arraylistadoclientessincredito[i].idcliente + "</td>" +
                    "<td>" + arraylistadoclientessincredito[i].nombrecompleto + "</td>" +
                    "<td>" + arraylistadoclientessincredito[i].direccioncompleta + "</td>" +
                    "<td>" + arraylistadoclientessincredito[i].telefonocliente + "</td>";
                
                }

                document.getElementById("nombrefiltrado").value = "";

            }

            function FilaSeleccionada() {
                        
                var table = document.getElementById('tablaclientessincredito'),
                selected = table.getElementsByClassName('selected');
                
                if (selected[0]) selected[0].className = '';
                event.target.parentNode.className = 'selected';

                /*
                console.log("IDCREDITOCLIENTE SELECCIONADO: ", $("tr.selected td:first" ).html() );
                console.log("IDCLIENTE SELECCIONADO: ", $("tr.selected td:nth-child(2)").html() );
                console.log("NOMBRE CLIENTE SELECCIONADO: ", $("tr.selected td:nth-child(3)").html() );
                console.log("DIRECCION CLIENTE SELECCIONADO: ",$("tr.selected td:nth-child(4)").html() );
                console.log("TELEFONO SELECCIONADO: ", $("tr.selected td:nth-child(5)").html() );
                console.log("LIMITE SELECCIONADO: ", $("tr.selected td:nth-child(6)").html() );
                console.log("CANTIDAD ADEUDADA SELECCIONADA: ", $("tr.selected td:nth-child(7)").html() );
                */
                            
                var idcliente = $("tr.selected td:first" ).html();
                    
                location.href ="/activarcreditocliente/" + idcliente;

            }

            function OcultarAlerta() {

                if( document.getElementById("contentalert").style.display == "none" ) {
                    document.getElementById("contentalert").style.display = "block";
                } else {
                    document.getElementById("contentalert").style.display = "none";
                }

            }

        </script>
            
        @if(Session::has('success'))
            <div class="alert alert-success">
                {{session('success')}}
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            </div>
        @elseif( Session::has('warning'))
            <div class="alert alert-warning">
                {{session('warning')}}
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            </div>
        @elseif( Session::has('danger'))
            <div class="alert alert-danger">
                {{session('danger')}}
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            </div>
        @endif

            <!-- DIV FILTRADO -->
        <div class="container" style="margin-bottom: 10px;">
            
            <!-- DIV NOMBRE FILTRADO -->
            <div class="form-row align-items-center">
                
                <!-- NOMBRE ESPECIFICA -->
                <div class="col-auto" style="margin-left: 25%; margin-bottom:10px;">
                    <label for="nombrefiltrado"> Ingrese el nombre a buscar </label>
                    <input type="text" class="form-control-sm" id="nombrefiltrado" style="margin-left: 10px;">
                </div>
                
                <!-- NOMBRE ESPECIFICA -->
                <div class="col-auto" style="margin-left: 5%; margin-bottom:10px;">
                    <label id="cantidadfiltrado" style="display: none;"> </label>
                </div>

                    
                <label id="resultadofiltrado" style="margin-left: 20px;">  </label>
                            
            </div>

            <!-- Botones -->
            <div class="form-row align-items-center" style="margin-top: 2px; margin-bottom: 5px;">

                <a style="margin-left: 35%;"> <button  class="btn btn-primary" id="botonfiltrado"    onclick="javascript:FiltrarTabla();">Filtrar Tabla</button> </a>
                <a style="margin-left: 5%;">  <button  class="btn btn-primary" id="botonrestablecer" onclick="javascript:RestablecerTabla();">Restablecer Tabla</button> </a>

            </div>
            
            
        </div>

            <!-- Alerta -->
        <div class="container"  id="contentalert" style="display: none;">
            <div class="alert alert-danger alert-dismissible" role="alert" style="margin-top: 1%" >
                <input class="form-control-plaintext" value = "" id="textoalerta" disabled>    
                <button type="button" class="close" onclick="javascript:OcultarAlerta();" style="margin-top: 5px">
                <span>&times;</span>
                </button>
            </div>
        </div>
        
            <!-- TOOLTIPTEXT -->
        <div class="container" style="margin-top: 10px;">
            
            <div class="row justify-content-center">
                <label class="textotooltip">
                    HAGA DOBLE CLICK SOBRE UN CLIENTE PARA ACTIVAR O REACTIVAR EL CRÉDITO
                </label>
            </div>
        </div>

        <!-- Tabla -->
        <div class="container" style="margin-top: 10px;">
            
            <div class="row justify-content-center">
                                    
                <div class="scrollbar" id="scrollbar-style">

                    <table class="table table-hover" style="width: 1000px" id="tablaclientessincredito" name="tablaclientessincredito" ondblclick="javascript:FilaSeleccionada();">
                        <thead  class="thead-dark">
                            <tr>
                                <th scope="col" style="display: none;"> IDCLIENTE </th>
                                <th scope="col"> Nombre del cliente </th>
                                <th scope="col"> Dirección  </th>
                                <th scope="col"> Teléfono </th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ( $listadoclientessincredito as $lcsc )
                                <tr>
                                    <td style="display: none;">{{ $lcsc->idcliente }}</td>
                                    <td>{{ $lcsc->nombrecompleto }}</td>
                                    <td>{{ $lcsc->direccioncompleta }}</td>
                                    <td>{{ $lcsc->telefonocliente }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>

                </div>
            
            </div>
        </div>

    @endsection

@endif