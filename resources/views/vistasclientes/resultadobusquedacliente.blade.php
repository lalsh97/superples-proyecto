@extends('home')

    @section('title')
        Resultado búsqueda de cliente
    @endsection

@section('contenido')

        
    @if(Session::has('success'))
        <div class="alert alert-success">
            {{session('success')}}
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        </div>
    @elseif( Session::has('warning'))
        <div class="alert alert-warning">
            {{session('warning')}}
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        </div>
    @elseif( Session::has('danger'))
        <div class="alert alert-danger">
            {{session('danger')}}
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        </div>
    @endif



    <!-- Tabla -->
    <div class="container">
        <div class="row justify-content-center">

            <table class="table table-hover" id="tablaclientes">
                <thead  class="thead-dark">
                    <tr>
                        <th scope="col>"> ID </th>
                        <th scope="col>"> Nombre completo </th>
                        <th scope="col>"> Curp </th>
                        <th scope="col>"> Fecha de nacimiento </th>
                        <th scope="col>"> Sexo </th>
                        <th scope="col>"> Teléfono </th>

                        <?php
                            if( Auth::user()->Nivel == 1 ) {
                        ?>
                            <th scope="col>" colspan="2"> Acciones </th>
                        <?php
                            }
                        ?>
                    </tr>
                </thead>
                <tbody>
                    @foreach ( $registroclientes as $clientes )
                        <tr>
                            <td>{{ $clientes-> idcliente }}</td>
                            <td>{{ $clientes-> nombrecompleto }}</td>
                            <td>{{ $clientes-> curp }}</td>
                            <td>{{ $clientes-> fechanac }}</td>
                            <td>{{ $clientes-> sexo }}</td>
                            <td>{{ $clientes-> telefono }}</td>


                        <?php
                            if( Auth::user()->Nivel == 1 ) {
                        ?>

                            <td>
                                <a href="{{ url('/modificarcliente/'.$clientes->idcliente ) }}">
                                    <button type="submit" class="btn btn-info"> Modificar </button>
                                </a>
                            </td>

                            <td>
                                <a href="{{ url('/descontinuarcliente/'.$clientes->idcliente ) }}">
                                    <button type="submit" class="btn btn-danger"> Desactivar </button>
                                </a>
                            </td>
                        <?php
                            }
                        ?>

                    </tr>
                    @endforeach
                </tbody>
            </table>

        </div>
    </div>

@endsection
