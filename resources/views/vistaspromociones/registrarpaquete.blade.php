@if( Auth::user()->Nivel == 1 )
    
    @extends('home')

    @section('title')
        Registro de paquete
    @endsection

    <style>
        #logo2 {

                width: 250px;
                height: 250px;

        }
        strong {

            color: red;
            font-size: 20px;

        }

    </style>

    <script >

        const FechaActual = new Date(new Date().getTime() - new Date().getTimezoneOffset() * 60000).toISOString().split("T")[0];
        var FechaMinimaFinal = new Date().setDate( new Date().getDate() + 1 );
        FechaMinimaFinal = new Date( FechaMinimaFinal ).toISOString().split("T")[0];

        window.onload = function( e ) {

            document.getElementById('fechaini').min = FechaActual;
            document.getElementById('fechafin').min = FechaMinimaFinal;

        }

        function FechaInicioSeleccionada() {
            //console.log("CHANGE FECHA INICIO");

            var fechainicial = new Date( document.getElementById("fechaini").value );
            fechainicial.setDate( fechainicial.getDate() + 1 );
            fechainicial = new Date( fechainicial ).toISOString().split("T")[0];
            document.getElementById('fechafin').min = fechainicial;

        }

        function FechaFinalSeleccionada() {
            //console.log("CHANGE FECHA FINAL");

            var fechainicialmaxima = new Date( document.getElementById("fechafin").value );
            fechainicialmaxima.setDate( fechainicialmaxima.getDate() - 1 );
            fechainicialmaxima = new Date( fechainicialmaxima ).toISOString().split("T")[0];
            document.getElementById('fechaini').max = fechainicialmaxima;

        }

        function prueba() {

            var precioproducto = document.getElementById("precio").value;
            var preciototal = parseFloat( precioproducto );

            var precioproducto2 = document.getElementById("precio2").value;
            var preciototal2 = parseFloat( precioproducto2 );

            document.getElementById( "total" ).value = preciototal2 + preciototal;

        }

        function cambiarcheckcodigo() {

            var precioproducto2 = document.getElementById("nuevoprecio2").value;
            var preciototal2 = parseFloat( precioproducto2 );

            var descu = document.getElementById("descuento").value;
            var desc = parseInt( descu );
            var precio = document.getElementById("precio").value;

            //  var precio=4.35;
            var precion = precio - ( ( precio * desc ) / 100 );
            var preciototal = Math.round( precion * 100) / 100;

            document.getElementById( "tipo" ).value = desc;
            document.getElementById( "nuevoprecio" ).value = ( Math.round( preciototal * 100) / 100 ).toFixed( 2 );
            document.getElementById( "total" ).value = ( Math.round( ( preciototal2 + preciototal ) * 100) / 100 ).toFixed( 2 );

        }

        function cambiarcheckcodigo2() {

            var precioproducto2 = document.getElementById("nuevoprecio").value;
            var preciototal2 = parseFloat(precioproducto2);

            var descu = document.getElementById("descuento2").value;
            var desc = parseInt( descu );
            var precio = document.getElementById("precio2").value;

            //  var precio=4.35;
            var precion = precio - ( ( precio * desc ) / 100 );
            var preciototal = Math.round( precion * 100 ) / 100;

            document.getElementById( "tipo2" ).value = desc;
            document.getElementById( "nuevoprecio2" ).value = ( Math.round( preciototal * 100 ) / 100).toFixed( 2 );
            document.getElementById( "total" ).value = ( Math.round( ( preciototal2 + preciototal ) * 100 ) / 100).toFixed( 2 ); 

        }

    </script>

    @section('contenido')

        @if(Session::has('success'))
            <div class="alert alert-success">
                {{session('success')}}
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            </div>
        @endif

        <!-- FORMULARIO -->
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header"  align="center">{{ __('Registro de promoción: Paquete') }}</div>
                        <div class="card-body">

                            {!!Form::open(array('name' => 'formproducto', 'url'=>'insertarpaquete','method'=>'POST' ,'files'=>true,'autocomplete'=>'off'))!!}

                                <!-- Imagen de la promocion  -->
                                <div class="form-group row">
                                    {!! Form::label( 'avatar','Imagen de la promoción', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                    <div class="col-md-6">
                                    <input type="file" id="avatar" name="avatar"  value="{{ old('avatar')}}" >
                                    </div>
                                </div>

                                <!-- Nombre de la promocion  -->
                                <div class="form-group row">
                                  <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Nombre de la promoción ')}}<strong>*</strong></label>
                                  <div class="col-md-6">
                                        <input type="text" id="nombre" name="nombre" placeholder="Ingrese el nombre de la promoción" class="form-control"
                                        value="{{session('nombre')}}" maxlength="45" minlength="1"pattern="([A-Za-z\s\W]|[0-9]){1,30}" required
                                        oninvalid="setCustomValidity('El nombre es obligatorio y debe tener como máximo 45 carácteres')"
                                        oninput="setCustomValidity('')"  onclick="prueba()" >
                                    </div>
                                </div>

                                <!-- Descripcion de la promoción  -->
                                <div class="form-group row">
                                  <label for="des" class="col-md-4 col-form-label text-md-right">{{ __('Descripción de la promoción ')}}<strong>*</strong></label>
                                  <div class="col-md-6">
                                        <input type="text" id="descripcion" name="descripcion" placeholder="Ingrese una descripción " class="form-control"
                                        value="{{session('descripcion')}}" maxlength="45" minlength="1" pattern="([A-Za-z\s\W]|[0-9]){1,45}" required
                                        oninvalid="setCustomValidity('La descripción es obligatoria')"
                                        oninput="setCustomValidity('')" >
                                    </div>
                                </div>

                                <!-- Fecha de inicio -->
                                <div class="form-group row">
                                  <label for="feci" class="col-md-4 col-form-label text-md-right">{{ __('Fecha de lanzamiento ')}}<strong>*</strong></label>
                                  <div class="col-md-6">
                                        <input type="date" id="fechaini" name="fechaini" class="form-control" required onchange="javascript:FechaInicioSeleccionada();"
                                            value="{{ old('fechaini') }}">
                                    </div>
                                </div>

                                <!-- Fecha fin -->
                                <div class="form-group row">
                                  <label for="fecf" class="col-md-4 col-form-label text-md-right">{{ __('Fecha de terminación ')}}<strong>*</strong></label>
                                  <div class="col-md-6">
                                        <input type="date" id="fechafin" name="fechafin" class="form-control" required onchange="javascript:FechaFinalSeleccionada();"
                                            value="{{ old('fechafin') }}">
                                    </div>
                                </div>

                                <div class="container">
                                    <div class="row justify-content-center">
                                        <table class="table table-hover">
                                            <thead  class="thead-dark">
                                                <tr>
                                                    <th scope="col>"> Información </th>
                                                    <th scope="col>"> Primer Producto </th>
                                                    <th scope="col>"> Segundo Producto </th>
                                                </tr>
                                            </thead>
                                            
                                            <tbody>                                            

                                                <!-- IMAGEN -->
                                                <tr>
                                                    <td class="table-info"> Imagen </td>
                                                    <td> <img class="card-img-top " id="logo2" src="/images/{{$producto->imagen}}" alt=""> </td>
                                                    <td> <img class="card-img-top" id="logo2" src="/images/{{$producto2->imagen}}" alt=""> </td>
                                                </tr>

                                                <!-- CODIGO -->
                                                <tr>
                                                    <td class="table-info"> Código</td>
                                                    <td> <input type="number" id="codigo" name="codigo" class="form-control" readonly  value="{{($producto->Codigo)}}"></td>
                                                    <td> <input type="number" id="codigo2" name="codigo2" class="form-control" readonly value="{{($producto2->Codigo)}}"></td>
                                                </tr>

                                                <!-- DESCRIPCION -->
                                                <tr>
                                                    <td class="table-info"> Descripción</td>
                                                    <td> <input type="text" id="descripcionprod" name="descripcionpod" class="form-control" readonly  value="{{($producto->Descripcion)}}"> </td>
                                                    <td> <input type="text" id="descripcionprod2" name="descripcionpod2" class="form-control"  readonly value="{{($producto2->Descripcion)}}"> </td>
                                                </tr>

                                                <!-- PRECIO COSTO -->
                                                <tr>
                                                    <td class="table-info"> Precio Costo </td>
                                                    <td> <input type="text" id="costo" name="costo"  placeholder="$ x unidad" class="form-control"  value="<?php echo bcadd( $producto->PrecioCosto , '0', 2); ?>" readonly></td>
                                                    <td> <input type="text" id="costo2" name="costo2"  placeholder="$ x unidad" class="form-control"  value="<?php echo bcadd( $producto2->PrecioCosto , '0', 2); ?>" readonly></td>
                                                </tr>

                                                <!-- PRECIO VENTA -->
                                                <tr>
                                                    <td class="table-info"> Precio Venta </dh>
                                                    <td> <input type="text" id="precio" name="precio" class="form-control"   value="<?php echo bcadd( $producto->PrecioVenta , '0', 2); ?>" readonly> </td>
                                                    <td> <input type="text" id="precio2" name="precio2" class="form-control"   value="<?php echo bcadd( $producto2->PrecioVenta , '0', 2); ?>" readonly> </td>
                                                </tr>

                                                <!-- DESCUENTO -->
                                                <tr >
                                                    <td class="table-info"> Descuento % <strong>*</strong> </dh>
                                                    <td> <input type="number" id="descuento"name="descuento" required min="0" max="100" step="5" class="form-control"  value="0" onchange="cambiarcheckcodigo()"   onclick="cambiarcheckcodigo()"  > </td>
                                                    <td> <input type="number" id="descuento2"name="descuento2" required min="0" max="100" step="5"class="form-control"  value="0" onchange="cambiarcheckcodigo2()"  onclick="cambiarcheckcodigo2()"  > </td>
                                                </tr>

                                                <!-- NUEVO PRECIO VENTA -->
                                                <tr>
                                                    <td class="table-info"> Nuevo Precio Venta </td>
                                                    <td> <input type="number" id="nuevoprecio" name="nuevoprecio"  placeholder="$ x unidad" class="form-control" readonly value="<?php echo bcadd( $producto->PrecioVenta , '0', 2); ?>" > </td>
                                                    <td> <input type="number" id="nuevoprecio2" name="nuevoprecio2"  placeholder="$ x unidad" class="form-control" readonly value="<?php echo bcadd( $producto2->PrecioVenta , '0', 2); ?>"> </td>
                                                </tr>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                                <input type="hidden"  id="tipo" name="tipo" class="form-control" readonly value="">
                                <input type="hidden"  id="tipo2" name="tipo2" class="form-control" readonly value="">

                                <!-- Total de la promoción -->
                                <div class="form-group row">
                                    {!! Form::label( 'total','Total de la Promoción ', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                        <div class="col-md-6">
                                                <input type="number" id="total" name="total" class="form-control" required readonly value="0.00">
                                        </div>

                                        <script type="application/javascript">

                                            var pro1 = document.getElementById("precio").value;
                                            var ipro = parseFloat( pro1 );
                                            var pro2 = document.getElementById("precio2").value;
                                            var ipro2 = parseFloat( pro2 );
                                            var total = ( Math.round( ( ipro + ipro2 ) * 100 ) / 100 ).toFixed( 2 );

                                            document.getElementById("total").value = total;
                                        </script>
                                    </div>

                                <br>

                                <div class="form-group row">
                                    <div class="col-md-4 col-form-label text-md-right">
                                        <button name="botonformpromocion" value="Registrar" type="submit" class="btn btn-success" style="margin-right:50px">Registrar</button>
                                    </div>
                                    <div class="col-md-4 col-form-label text-md-right">
                                        <button name="botonformpromocion" value="Cancelar" type="submit" class="btn btn-danger" style="margin-left:250px" formnovalidate>Cancelar</button>
                                    </div>
                                </div>

                            {!!Form::close()!!}

                        </div>
                    </div>
                </div>
            </div>
        </div>

        <br><br><br>
        
    @endsection

@endif
