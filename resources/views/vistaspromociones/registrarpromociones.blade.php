@if( Auth::user()->Nivel == 1 )
    
    @extends('home')

    @section('title')
        Registro de promocion
    @endsection

    <script >

        const FechaActual = new Date(new Date().getTime() - new Date().getTimezoneOffset() * 60000).toISOString().split("T")[0];
        var FechaMinimaFinal = new Date().setDate( new Date().getDate() + 1 );
        FechaMinimaFinal = new Date( FechaMinimaFinal ).toISOString().split("T")[0];

        window.onload = function( e ) {

            document.getElementById('fechaini').min = FechaActual;
            document.getElementById('fechafin').min = FechaMinimaFinal;

        }

        function FechaInicioSeleccionada() {
            //console.log("CHANGE FECHA INICIO");
            
            var fechainicial = new Date( document.getElementById("fechaini").value );                
            fechainicial.setDate( fechainicial.getDate() + 1 );
            fechainicial = new Date( fechainicial ).toISOString().split("T")[0];            
            document.getElementById('fechafin').min = fechainicial;

        }

        function FechaFinalSeleccionada() {
            //console.log("CHANGE FECHA FINAL");
            
            var fechainicialmaxima = new Date( document.getElementById("fechafin").value );                
            fechainicialmaxima.setDate( fechainicialmaxima.getDate() - 1 );
            fechainicialmaxima = new Date( fechainicialmaxima ).toISOString().split("T")[0];            
            document.getElementById('fechaini').max = fechainicialmaxima;

        }

    </script>


    @section('contenido')


        @if(Session::has('success'))
            <div class="alert alert-success">
                {{session('success')}}
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            </div>
        @endif

        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header"  align="center">{{ __('Registro de la promoción') }}</div>
                        <div class="card-body">

                            {!!Form::open(array('name' => 'formproducto', 'url'=>'insertarpromocion','method'=>'POST' ,'autocomplete'=>'off'))!!}

                            <fieldset  class="border p-2">
                                <legend  class="w-auto"> Datos de la promoción </legend>

                                <!-- Nombre de la promocion  -->
                                <div class="form-group row">
                                    {!! Form::label( 'nombre','Nombre de la promoción', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                    <div class="col-md-6">
                                        <input type="text" id="nombre" name="nombre" placeholder="Ingrese el nombre de la promoción" class="form-control"
                                        value="{{session('nombre')}}" maxlength="45" minlength="1" pattern="[A-Za-z\s\W]{1,45}" required
                                        oninvalid="setCustomValidity('El nombre es obligatorio y debe tener como máximo 45 carácteres')"
                                        oninput="setCustomValidity('')" >
                                    </div>
                                </div>

                                <!-- Descripcion de la promoción  -->
                                <div class="form-group row">
                                    {!! Form::label( 'des','Descripción de la promoción', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                    <div class="col-md-6">
                                        <input type="text" id="descripcion" name="descripcion" placeholder="Ingrese una descripción " class="form-control"
                                        value="{{session('descripcion')}}" maxlength="45" minlength="1" pattern="[A-Za-z\s\W]{1,45}" required
                                        oninvalid="setCustomValidity('La descripción es obligatoria')"
                                        oninput="setCustomValidity('')" >
                                    </div>
                                </div>

                                <!-- Fecha de inicio -->
                                <div class="form-group row">
                                    {!! Form::label( 'feci','Fecha de lanzamiento', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                    <div class="col-md-6">
                                        <input type="date" id="fechaini" name="fechaini" class="form-control" required onchange="javascript:FechaInicioSeleccionada();"
                                            value="{{ old('fechaini') }}">
                                    </div>
                                </div>

                                <!-- Fecha fin -->
                                <div class="form-group row">
                                    {!! Form::label( 'fecf','Fecha de terminación', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                    <div class="col-md-6">
                                        <input type="date" id="fechafin" name="fechafin" class="form-control" required onchange="javascript:FechaFinalSeleccionada();"
                                            value="{{ old('fechafin') }}">
                                    </div>
                                </div>

                                    <!-- Total -->
                                <div class="form-group row" >
                                    {!! Form::label( 'total','Precio de Venta: ', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                    <div class="col-md-6">
                                    <input type="number" id="total" name="total"  placeholder="Ingrese el precio de la promoción" class="form-control"
                                        value="{{ old('numero') }}" min="1" max="9999" required pattern="[\d]{1,4}"
                                        oninvalid="setCustomValidity('Solo se permite hasta 9999 ')"
                                        oninput="setCustomValidity('')">
                                    </div>
                                </div>


                            </fieldset>
                            <br>

                                <div class="form-group row">
                                    <div class="col-md-4 col-form-label text-md-right">
                                        <button name="botonformpromocion" value="Registrar" type="submit" class="btn btn-success" style="margin-right:50px">Registrar</button>
                                    </div>
                                    <div class="col-md-4 col-form-label text-md-right">
                                        <button name="botonformpromocion" value="Cancelar" type="submit" class="btn btn-danger" style="margin-left:250px">Cancelar</button>
                                    </div>
                                </div>

                            {!!Form::close()!!}




                            <!-- <script src="{{ asset('js/popup.js') }}"></script> -->


                        </div>
                    </div>
                </div>
            </div>
        </div>

    @endsection

@endif
