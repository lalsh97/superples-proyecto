@if( Auth::user()->Nivel == 1 )

    @extends('home')

    @section('title')
        Terminar promoción paquete
    @endsection

    @section('contenido')

        <style>
            #logo2 {

                width: 250px;
                height: 250px;

            }

        </style>

        <script>
            

            function MostrarModalConfirmacionEliminacion() {

                var codigobarras = document.getElementById("codigo").value;

                var descripcion = document.getElementById("descripcion").value;

                document.getElementById("textoerrorbody").innerHTML = "<p>¿Está seguro que desea terminar la promoción <strong>" + descripcion + "</strong> ?</p>";

                $('#ModalConfirmacionEliminacion').modal('show');                    

            }

            function DismissModalConfirmacionEliminacion() {

                $("#ModalConfirmacionEliminacion").modal('hide');

            }

            function TerminarPromocion() {

                $("#ModalConfirmacionEliminacion").modal('hide');

                var idpromociones = document.getElementById("idpromociones").value;

                location.href ="/eliminarcambiarestadopromocion/" + idpromociones;

            }

            function Regresar() {
                location.href = "/home";
            }

        </script>

        @if(Session::has('success'))
            <div class="alert alert-success">
                {{session('success')}}
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            </div>
        @endif

        <!-- TABLA -->
        <div class="container" >
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header"  align="center">{{ __('Terminar con la promoción: Paquete') }}</div>
                        <div class="card-body">

                            {!!Form::open(array('name' => 'formpromocion', 'url'=>'/terminarpromocion/'.$promocion->idPromociones,'method'=>'PUT','files'=>true  ,'autocomplete'=>'off'))!!}

                                <!-- Imagen -->
                                <div class="form-group row">
                                    {!! Form::label( 'imagen','Imagen de la promoción', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                    <div class="card" style="width: 18rem;">
                                        <img class="card-img-top" src="/images/{{$promocion->Imagen}}" alt="">
                                    </div>
                                </div>

                                <!-- ID -->
                                <div class="form-group row" style="display: none;">
                                    {!! Form::label( 'nombre','ID de la promoción', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                    <div class="col-md-6">
                                        <input type="text" id="idpromociones" name="idpromociones" class="form-control" value="{{($promocion->idPromociones)}}" readonly style="display: none;">
                                    </div>
                                </div>

                                <!-- Nombre de la promocion  -->
                                <div class="form-group row">
                                    {!! Form::label( 'nombre','Nombre de la promoción', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                    <div class="col-md-6">
                                        <input type="text" id="nombre" name="nombre" placeholder="Ingrese el nombre de la promoción" class="form-control"
                                        value="{{($promocion->Nombre)}}" readonly onclick="ok()" >
                                    </div>
                                </div>

                                <!-- Descripcion de la promoción  -->
                                <div class="form-group row">
                                    {!! Form::label( 'des','Descripción de la promoción', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                    <div class="col-md-6">
                                        <input type="text" id="descripcion" name="descripcion" placeholder="Ingrese una descripción " class="form-control"
                                        value="{{($promocion->Descripcion)}}" readonly>
                                    </div>
                                </div>

                                <!-- Fecha de inicio -->
                                <div class="form-group row">
                                    {!! Form::label( 'feci','Fecha de lanzamiento', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                    <div class="col-md-6">
                                        <input type="date" id="fechaini" name="fechaini" class="form-control"
                                            max="2030-12-31" min="2019-12-31" readonly
                                            value="{{ ($promocion->Fechaini) }}">
                                    </div>
                                </div>

                                <!-- Fecha fin -->
                                <div class="form-group row">
                                    {!! Form::label( 'fecf','Fecha de terminación', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                    <div class="col-md-6">
                                        <input type="date" id="fechafin" name="fechafin" class="form-control"
                                            max="2030-12-31" min="2019-12-31" readonly
                                            value="{{ ($promocion->Fechafin) }}">
                                    </div>
                                </div>

                                <!-- información del producto -->
                                <div class="container">
                                    <div class="row justify-content-center">
                                        <table class="table table-hover">
                                            <thead  class="thead-dark">
                                                <tr>
                                                    <th scope="col>"> Información </th>
                                                    <th scope="col>"> Primer Producto </th>
                                                    <th scope="col>"> Segundo Producto </th>
                                                </tr>
                                                <tr>
                                                    <td class="table-info"> Imagen </td>
                                                    <td >   <img id="logo2" class="card-img-top " src="/images/{{$producto->imagen}}" alt=""> </td>
                                                    <td> <img id="logo2" class="card-img-top" src="/images/{{$producto2->imagen}}" alt=""> </td>
                                                </tr>
                                                <tr>
                                                    <td class="table-info"> Código</td>
                                                    <td><input type="text" id="codigo" name="codigo" class="form-control" readonly  value="{{($producto->Codigo)}}"></td>
                                                    <td><input type="text" id="codigo2" name="codigo2" class="form-control" readonly value="{{($producto2->Codigo)}}"></td>
                                                </tr>
                                                <tr>
                                                    <td class="table-info"> Descripción</td>
                                                    <td>   <input type="text" id="descripcionprod" name="descripcionpod" class="form-control" readonly  value="{{($producto->Descripcion)}}"> </td>
                                                    <td> <input type="text" id="descripcionprod2" name="descripcionpod2" class="form-control"  readonly value="{{($producto2->Descripcion)}}"> </td>
                                                </tr>



                                            </thead>
                                        </table>
                                    </div>
                                </div>

                                <!-- Total de la promoción -->
                                <div class="form-group row">
                                    {!! Form::label( 'total','Total de la promoción ', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                    <div class="col-md-6">
                                        <input type="text" id="total" name="total" class="form-control"
                                            readonly  
                                            value="<?php echo bcadd( $promocion->Total , '0', 2); ?>">
                                    </div>
                                </div>
                            
                                <br>

                            {!!Form::close()!!}

                            <!-- Botones -->
                            <div class="form-group row">
                                <div class="col-md-4 col-form-label text-md-right">
                                    <button name="botonformdescontinuar" value="Terminar" type="submit" class="btn btn-success" style="margin-right:50px" formnovalidate onclick="javascript:MostrarModalConfirmacionEliminacion();">Terminar</button>
                                </div>
                                <div class="col-md-4 col-form-label text-md-right">
                                    <button name="botonformdescontinuar" value="Cancelar" type="submit" class="btn btn-danger" style="margin-left:250px" formnovalidate  onclick="javascript:Regresar();">Cancelar</button>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>

        <br><br><br>

        <!-- ModalConfirmacionEliminacion -->
        <div class="modal" tabindex="-1" role="dialog" id="ModalConfirmacionEliminacion">
            <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                <div class="modal-content modal-lg">

                    <!--Header-->
                    <div class="modal-header" id="modalheader" style="background: rgb(33, 37, 41)">
                        <h5 id="textostringheader" class="heading" style="margin-left: 25%; color: white; text-align: center;">CONFIRMACIÓN DE ELIMINACIÓN</h5>

                        <button type="button" class="close" onclick="javascript:DismissModalConfirmacionEliminacion();" data-dismiss="modal" aria-label="Close">
                            <span  aria-hidden="true" style="color: rgb(255, 255, 255)" >&times;</span>
                        </button>
                    </div>

                    <!--Body-->
                    <div class="modal-body" id="modalbody">
                        <div class="row">
                            <div class="col">
                                <p id="textoerrorbody" style="text-align: center;"></p>
                            </div>
                        </div>

                    </div>

                    <!--Footer-->
                    <div class="modal-footer" id="modalfooter">
                        <button type="button" class="btn btn-primary" data-dismiss="modal" id="botonEliminar" onclick="javascript:TerminarPromocion();">TERMINAR</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal" id="botonCancelar" onclick="javascript:DismissModalConfirmacionEliminacion();" style="margin-left: 72%;">CANCELAR</button>
                    </div>

                </div>
            </div>
        </div>

    @endsection

@endif
