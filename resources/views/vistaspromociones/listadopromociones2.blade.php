@if( Auth::user()->Nivel == 1 )

    @extends('home')

    @section('title')
        Listado de promociones eliminar
    @endsection

    @if(Session::has('success'))
        <div class="alert alert-success">
            {{session('success')}}
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        </div>
    @endif

    @section('contenido')

        <script>

            function FiltrarTabla( tipopromocion ) {
                
                var arrayregistropromociones = {!! json_encode( $registropromociones->toArray(), JSON_HEX_TAG ) !!};
                
                var niveluser = {{ ( Auth::user()->Nivel ) }};

                VaciarTabla();

                for( var i = 0; i < arrayregistropromociones.length; i++ ) {

                    if( tipopromocion == 1 ) {

                        if( arrayregistropromociones[i].Tipo == tipopromocion ) {

                            if( niveluser == 1 ) {
                                                    
                                document.getElementById("tablapromociones").insertRow(-1).innerHTML = 
                                "<td>" + arrayregistropromociones[i].idPromociones + "</td>" +
                                "<td>" + arrayregistropromociones[i].Nombre + "</td>" +
                                "<td>" + arrayregistropromociones[i].Fechaini + "</td>" +
                                "<td>" + arrayregistropromociones[i].Fechafin + "</td>" +
                                "<td>" + ( Math.round( arrayregistropromociones[i].Total * 100 ) / 100 ).toFixed( 2 ) + "</td>" +
                                "<td> Oferta </td>" +
                                "<td>" + 
                                    "<a href=/confirmaroferta/" + arrayregistropromociones[i].idPromociones + " }}>" +
                                        "<button type='submit' class='btn btn-danger'> Terminar </button>" +
                                    "</a>" +
                                "</td>";

                            } else if ( niveluser == 2 ) {
                                                    
                                document.getElementById("tablapromociones").insertRow(-1).innerHTML = 
                                "<td>" + arrayregistropromociones[i].idPromociones + "</td>" +
                                "<td>" + arrayregistropromociones[i].Nombre + "</td>" +
                                "<td>" + arrayregistropromociones[i].Fechaini + "</td>" +
                                "<td>" + arrayregistropromociones[i].Fechafin + "</td>" +
                                "<td>" + ( Math.round( arrayregistropromociones[i].Total * 100 ) / 100 ).toFixed( 2 ) + "</td>" +
                                "<td> Oferta </td>";

                            }

                        }

                    } else if( tipopromocion == 2 ) {

                        if( arrayregistropromociones[i].Tipo == tipopromocion ) {

                            if( niveluser == 1 ) {
                                                    
                                document.getElementById("tablapromociones").insertRow(-1).innerHTML = 
                                "<td>" + arrayregistropromociones[i].idPromociones + "</td>" +
                                "<td>" + arrayregistropromociones[i].Nombre + "</td>" +
                                "<td>" + arrayregistropromociones[i].Fechaini + "</td>" +
                                "<td>" + arrayregistropromociones[i].Fechafin + "</td>" +
                                "<td>" + ( Math.round( arrayregistropromociones[i].Total * 100 ) / 100 ).toFixed( 2 ) + "</td>" +
                                "<td> Descuento </td>" +
                                "<td>" + 
                                    "<a href=/confirmardescuento/" + arrayregistropromociones[i].idPromociones + " }}>" +
                                        "<button type='submit' class='btn btn-danger'> Terminar </button>" +
                                    "</a>" +
                                "</td>";

                            } else if( niveluser == 2 ) {
                                                    
                                document.getElementById("tablapromociones").insertRow(-1).innerHTML = 
                                "<td>" + arrayregistropromociones[i].idPromociones + "</td>" +
                                "<td>" + arrayregistropromociones[i].Nombre + "</td>" +
                                "<td>" + arrayregistropromociones[i].Fechaini + "</td>" +
                                "<td>" + arrayregistropromociones[i].Fechafin + "</td>" +
                                "<td>" + ( Math.round( arrayregistropromociones[i].Total * 100 ) / 100 ).toFixed( 2 ) + "</td>" +
                                "<td> Descuento </td>";

                            }

                        }

                    } else if( tipopromocion == 3 ) {

                        if( arrayregistropromociones[i].Tipo == tipopromocion ) {

                            if( niveluser == 1 ) {
                                                    
                                document.getElementById("tablapromociones").insertRow(-1).innerHTML = 
                                "<td>" + arrayregistropromociones[i].idPromociones + "</td>" +
                                "<td>" + arrayregistropromociones[i].Nombre + "</td>" +
                                "<td>" + arrayregistropromociones[i].Fechaini + "</td>" +
                                "<td>" + arrayregistropromociones[i].Fechafin + "</td>" +
                                "<td>" + ( Math.round( arrayregistropromociones[i].Total * 100 ) / 100 ).toFixed( 2 ) + "</td>" +
                                "<td> Paquete </td>" +
                                "<td>" + 
                                    "<a href=/confirmarpaquete/" + arrayregistropromociones[i].idPromociones + " }}>" +
                                        "<button type='submit' class='btn btn-danger'> Terminar </button>" +
                                    "</a>" +
                                "</td>";

                            } else if( niveluser == 2 ) {
                                                    
                                document.getElementById("tablapromociones").insertRow(-1).innerHTML = 
                                "<td>" + arrayregistropromociones[i].idPromociones + "</td>" +
                                "<td>" + arrayregistropromociones[i].Nombre + "</td>" +
                                "<td>" + arrayregistropromociones[i].Fechaini + "</td>" +
                                "<td>" + arrayregistropromociones[i].Fechafin + "</td>" +
                                "<td>" + ( Math.round( arrayregistropromociones[i].Total * 100 ) / 100 ).toFixed( 2 ) + "</td>" +
                                "<td> Paquete </td>";

                            }

                        }

                    }

                }
            
                const tabla = document.getElementById('tablapromociones');
                var rowCount = tabla.rows.length;

                if( rowCount == 1 ) {
                            
                    document.getElementById("textoalerta").value = "No hay ninguna promoción que coincida con el tipo seleccionado";
                    document.getElementById("contentalert").style.display = "block";


                } else {
                    
                    document.getElementById("resultadofiltrado").innerHTML = "<p> Total de coincidencias: <strong>"+ ( rowCount - 1) + "</strong> </p>";

                }
            }

            function VaciarTabla() {
                            
                document.getElementById("resultadofiltrado").innerHTML = "";

                const tabla = document.getElementById('tablapromociones');
                var rowCount = tabla.rows.length;         

                for  ( var x = rowCount - 1; x > 0; x-- ) { 
                    tabla.deleteRow(x); 
                } 

            }

            function RestablecerTabla() {

                var arrayregistropromociones = {!! json_encode( $registropromociones->toArray(), JSON_HEX_TAG ) !!};
                
                var niveluser = {{ ( Auth::user()->Nivel ) }};

                VaciarTabla();

                for( var i = 0; i < arrayregistropromociones.length; i++ ) {

                    if( niveluser == 1 ) {

                        if( arrayregistropromociones[i].Tipo == 1 ) {
                                                    
                                document.getElementById("tablapromociones").insertRow(-1).innerHTML = 
                                "<td>" + arrayregistropromociones[i].idPromociones + "</td>" +
                                "<td>" + arrayregistropromociones[i].Nombre + "</td>" +
                                "<td>" + arrayregistropromociones[i].Fechaini + "</td>" +
                                "<td>" + arrayregistropromociones[i].Fechafin + "</td>" +
                                "<td>" + ( Math.round( arrayregistropromociones[i].Total * 100 ) / 100 ).toFixed( 2 ) + "</td>" +
                                "<td> Oferta </td>" +
                                "<td>" + 
                                    "<a href=/confirmaroferta/" + arrayregistropromociones[i].idPromociones + " }}>" +
                                        "<button type='submit' class='btn btn-danger'> Terminar </button>" +
                                    "</a>" +
                                "</td>";

                        } else if( arrayregistropromociones[i].Tipo == 2 ) {
                                                    
                                document.getElementById("tablapromociones").insertRow(-1).innerHTML = 
                                "<td>" + arrayregistropromociones[i].idPromociones + "</td>" +
                                "<td>" + arrayregistropromociones[i].Nombre + "</td>" +
                                "<td>" + arrayregistropromociones[i].Fechaini + "</td>" +
                                "<td>" + arrayregistropromociones[i].Fechafin + "</td>" +
                                "<td>" + ( Math.round( arrayregistropromociones[i].Total * 100 ) / 100 ).toFixed( 2 ) + "</td>" +
                                "<td> Descuento </td>" +
                                "<td>" + 
                                    "<a href=/confirmardescuento/" + arrayregistropromociones[i].idPromociones + " }}>" +
                                        "<button type='submit' class='btn btn-danger'> Terminar </button>" +
                                    "</a>" +
                                "</td>";

                        } else if( arrayregistropromociones[i].Tipo == 3 ) {
                                                    
                                document.getElementById("tablapromociones").insertRow(-1).innerHTML = 
                                "<td>" + arrayregistropromociones[i].idPromociones + "</td>" +
                                "<td>" + arrayregistropromociones[i].Nombre + "</td>" +
                                "<td>" + arrayregistropromociones[i].Fechaini + "</td>" +
                                "<td>" + arrayregistropromociones[i].Fechafin + "</td>" +
                                "<td>" + ( Math.round( arrayregistropromociones[i].Total * 100 ) / 100 ).toFixed( 2 ) + "</td>" +
                                "<td> Paquete </td>" +
                                "<td>" + 
                                    "<a href=/confirmarpaquete/" + arrayregistropromociones[i].idPromociones + " }}>" +
                                        "<button type='submit' class='btn btn-danger'> Terminar </button>" +
                                    "</a>" +
                                "</td>";
                            
                        }

                    } else if( niveluser == 2 ) {

                        if( arrayregistropromociones[i].Tipo == 1 ) {
                                                                                
                            document.getElementById("tablapromociones").insertRow(-1).innerHTML = 
                                "<td>" + arrayregistropromociones[i].idPromociones + "</td>" +
                                "<td>" + arrayregistropromociones[i].Nombre + "</td>" +
                                "<td>" + arrayregistropromociones[i].Fechaini + "</td>" +
                                "<td>" + arrayregistropromociones[i].Fechafin + "</td>" +
                                "<td>" + ( Math.round( arrayregistropromociones[i].Total * 100 ) / 100 ).toFixed( 2 ) + "</td>" +
                                "<td> Oferta </td>";

                        } else if( arrayregistropromociones[i].Tipo == 2 ) {
                                                                                
                            document.getElementById("tablapromociones").insertRow(-1).innerHTML = 
                                "<td>" + arrayregistropromociones[i].idPromociones + "</td>" +
                                "<td>" + arrayregistropromociones[i].Nombre + "</td>" +
                                "<td>" + arrayregistropromociones[i].Fechaini + "</td>" +
                                "<td>" + arrayregistropromociones[i].Fechafin + "</td>" +
                                "<td>" + ( Math.round( arrayregistropromociones[i].Total * 100 ) / 100 ).toFixed( 2 ) + "</td>" +
                                "<td> Descuento </td>";

                        } else if( arrayregistropromociones[i].Tipo == 3 ) {
                                                                                
                            document.getElementById("tablapromociones").insertRow(-1).innerHTML = 
                                "<td>" + arrayregistropromociones[i].idPromociones + "</td>" +
                                "<td>" + arrayregistropromociones[i].Nombre + "</td>" +
                                "<td>" + arrayregistropromociones[i].Fechaini + "</td>" +
                                "<td>" + arrayregistropromociones[i].Fechafin + "</td>" +
                                "<td>" + ( Math.round( arrayregistropromociones[i].Total * 100 ) / 100 ).toFixed( 2 ) + "</td>" +
                                "<td> Paquete </td>";

                        }

                    }

                }
                
            }

            function OcultarAlerta() {

                if( document.getElementById("contentalert").style.display == "none" ) {
                    document.getElementById("contentalert").style.display = "block";
                } else {
                    document.getElementById("contentalert").style.display = "none";
                }

            }

        </script>

        <!-- DVI FILTRADO -->
        <div class="container" style="margin-bottom: 10px;">
            <div class="row justify-content-center">

                <!-- BOTONES FILTRADO -->
                <div class="col" style="margin-left: 1%; margin-bottom:10px;">
                    
                    <a style="margin-left: 5%;"> <button  class="btn btn-primary" id="botonfiltradocodigo" onclick="javascript:FiltrarTabla( 1 );">Filtrar por oferta</button> </a>
                    <a style="margin-left: 5%;"> <button  class="btn btn-primary" id="botonfiltradocodigo" onclick="javascript:FiltrarTabla( 2 );">Filtrar por descuento</button> </a>
                    <a style="margin-left: 5%;"> <button  class="btn btn-primary" id="botonfiltradocodigo" onclick="javascript:FiltrarTabla( 3 );">Filtrar por paquete</button> </a>

                    <a style="margin-left: 5%;"> <button  class="btn btn-warning" id="botonfiltradocodigo" onclick="javascript:RestablecerTabla();">Restablecer</button> </a>
                    
                    <label id="resultadofiltrado" style="margin-left: 20px;"> </label>
                </div>

            </div>
        </div>

        <!-- Alerta -->
        <div class="container"  id="contentalert" style="display: none;">
            <div class="alert alert-danger alert-dismissible" role="alert" style="margin-top: 1%" >
                <input class="form-control-plaintext" value = "" id="textoalerta" disabled>    
                <button type="button" class="close" onclick="javascript:OcultarAlerta();" style="margin-top: 5px">
                <span>&times;</span>
                </button>
            </div>
        </div>


        <!-- TABLA -->
        <div class="container">
            <div class="row justify-content-center">

                <table class="table table-hover" id="tablapromociones">
                    <thead  class="thead-dark">
                        <tr>
                            <th scope="col"> ID Promoción </th>
                            <th scope="col"> Nombre </th>
                            <th scope="col"> Fecha de lanzamiento </th>
                            <th scope="col"> Fecha de terminación </th>
                            <th scope="col"> Total </th>
                            <th scope="col"> Tipo </th>
                            <?php
                                if( Auth::user()->Nivel == 1 ) {
                                    ?>
                                        <th scope="col" colspan="2"> Acciones </th>
                                    <?php
                                }
                            ?>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ( $registropromociones as $promociones )
                            <tr>
                                <td>{{ $promociones->idPromociones }}</td>
                                <td>{{ $promociones-> Nombre }}</td>
                                <td>{{ $promociones-> Fechaini }}</td>
                                <td>{{ $promociones-> Fechafin }}</td>
                                <td> <?php echo bcadd( $promociones->Total , '0', 2); ?> </td>

                                <!-- TIPO OFERTA -->
                                <td>
                                    <?php
                                        if ( $promociones->Tipo == 1 ) {
                                            print("Oferta");
                                        } elseif( $promociones->Tipo == 2 ) {
                                            print("Descuento");
                                        } elseif( $promociones->Tipo == 3 ) {
                                            print("Paquete");
                                        }
                                    ?>
                                </td>

                                <!-- ACCIONES -->
                                <?php
                                    if( Auth::user()->Nivel == 1 ) {
                                        if ( $promociones->Tipo == 1 ) {
                                            ?>
                                                <td>
                                                    <a href="{{ url('/confirmaroferta/'.$promociones->idPromociones ) }}">
                                                        <button type="submit" class="btn btn-danger"> Terminar </button>
                                                    </a>
                                                </td>

                                            <?php
                                        } elseif ( $promociones->Tipo == 2 ) {
                                            ?>
                                                <td>
                                                    <a href="{{ url('/confirmardescuento/'.$promociones->idPromociones ) }}">
                                                        <button type="submit" class="btn btn-danger"> Terminar </button>
                                                    </a>
                                                </td>                                                
                                            <?php
                                        } elseif ( $promociones->Tipo == 3 ) {
                                            ?>
                                                <td>
                                                    <a href="{{ url('/confirmarpaquete/'.$promociones->idPromociones ) }}">
                                                        <button type="submit" class="btn btn-danger"> Terminar </button>
                                                    </a>
                                                </td>
                                            <?php
                                        }
                                    }
                                ?>

                        </tr>
                        @endforeach
                    </tbody>
                </table>

            </div>
        </div>

    @endsection

@endif
