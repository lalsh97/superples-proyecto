@if( Auth::user()->Nivel == 1 )

    @extends('home')

    @section('title')
        Modificación de información de una promoci+on
    @endsection

    @section('contenido')

        <style>
            strong {
                color: red;
                font-size: 20px;

            }

        </style>

        <script >

            const FechaActual = new Date(new Date().getTime() - new Date().getTimezoneOffset() * 60000).toISOString().split("T")[0];
            var FechaMinimaFinal = new Date().setDate( new Date().getDate() + 1 );
            FechaMinimaFinal = new Date( FechaMinimaFinal ).toISOString().split("T")[0];

        

            var promocion = {!! json_encode( $promocion->toArray(), JSON_HEX_TAG ) !!};


            window.onload = function( e ) {

                document.getElementById('fechaini').min = promocion.Fechaini;
                //document.getElementById('fechaini').min = FechaActual;
                //document.getElementById('fechafin').min = FechaMinimaFinal;


                var fechainicial = new Date( document.getElementById("fechaini").value );
                fechainicial.setDate( fechainicial.getDate() + 1 );
                fechainicial = new Date( fechainicial ).toISOString().split("T")[0];
                document.getElementById('fechafin').min = fechainicial;

            }

            function FechaInicioSeleccionada() {
                //console.log("CHANGE FECHA INICIO");

                var fechainicial = new Date( document.getElementById("fechaini").value );
                fechainicial.setDate( fechainicial.getDate() + 1 );
                fechainicial = new Date( fechainicial ).toISOString().split("T")[0];
                document.getElementById('fechafin').min = fechainicial;

            }

            function FechaFinalSeleccionada() {
                //console.log("CHANGE FECHA FINAL");

                var fechainicialmaxima = new Date( document.getElementById("fechafin").value );
                fechainicialmaxima.setDate( fechainicialmaxima.getDate() - 1 );
                fechainicialmaxima = new Date( fechainicialmaxima ).toISOString().split("T")[0];
                document.getElementById('fechaini').max = fechainicialmaxima;

            }

            function cambiarcheckcodigo() {

                var descu = document.getElementById("descuento").value;
                var desc = parseInt(descu);
                var precio = document.getElementById("precio").value;
                //  var precio=4.35;
                var precion = precio - ( ( precio * desc ) / 100 );
                var preciototal = ( Math.round( precion * 100 ) / 100 ).toFixed( 2 );

                document.getElementById( "nuevoprecio" ).value = preciototal;
                document.getElementById( "tipo" ).value = desc;

            }

        </script>

        @if(Session::has('success'))
            <div class="alert alert-success">
                {{session('success')}}
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            </div>
        @endif

        <!-- TABLA -->
        <div class="container" >
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header"  align="center">{{ __('Actualización de datos de la promoción: Descuento') }}</div>
                        <div class="card-body">

                            {!!Form::open(array('name' => 'formpromocion', 'url'=>'/actualizarinfopromocion2/'.$promocion->idPromociones,'method'=>'PUT','files'=>true  ,'autocomplete'=>'off'))!!}


                                <!-- Imagen -->
                                <div class="form-group row">
                                    {!! Form::label( 'imagen','Imagen de la promoción', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                    <div class="card" style="width: 18rem;">
                                        <img class="card-img-top" src="/images/{{$promocion->Imagen}}" alt="">
                                    </div>
                                </div>

                                <!-- Nueva Imagen del producto -->
                                <div class="form-group row">
                                    {!! Form::label( 'Imagen','Cambiar imagen', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                    <div class="col-md-6">
                                        <input type="file" id="avatar" name="avatar" >
                                    </div>
                                </div>

                                <!-- Nombre de la promocion  -->
                                <div class="form-group row">
                                    {!! Form::label( 'nombre','Nombre de la promoción', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                    <div class="col-md-6">
                                        <input type="text" id="nombre" name="nombre" placeholder="Ingrese el nombre de la promoción" class="form-control"
                                        value="{{($promocion->Nombre)}}" readonly onclick="ok()" >
                                    </div>
                                </div>

                                <!-- Descripcion de la promoción  -->
                                <div class="form-group row">
                                    {!! Form::label( 'des','Descripción de la promoción', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                    <div class="col-md-6">
                                        <input type="text" id="descripcion" name="descripcion" placeholder="Ingrese una descripción " class="form-control"
                                        value="{{($promocion->Descripcion)}}" readonly>
                                    </div>
                                </div>

                                <!-- Fecha de inicio -->
                                <div class="form-group row">
                                  <label for="feci" class="col-md-4 col-form-label text-md-right">{{ __('Fecha de lanzamiento ')}}<strong>*</strong></label>
                                  <div class="col-md-6">
                                        <input type="date" id="fechaini" name="fechaini" class="form-control" required onchange="javascript:FechaInicioSeleccionada();"
                                            value="{{ ( $promocion->Fechaini ) }}">
                                    </div>
                                </div>

                                <!-- Fecha fin -->
                                <div class="form-group row">
                                  <label for="fecf" class="col-md-4 col-form-label text-md-right">{{ __('Fecha de terminación ')}}<strong>*</strong></label>
                                  <div class="col-md-6">
                                        <input type="date" id="fechafin" name="fechafin" class="form-control" required onchange="javascript:FechaFinalSeleccionada();"
                                            value="{{ ( $promocion->Fechafin ) }}">
                                    </div>
                                </div>


                                <!-- información del producto -->

                                <!-- Codigo del producto -->
                                <div class="form-group row">
                                    {!! Form::label( 'fecf','Código del producto', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                    <div class="col-md-6">
                                        <input type="number" id="codigo" name="codigo" class="form-control" readonly        value="{{($producto->Codigo)}}">
                                    </div>
                                </div>

                                <!-- Nombre del producto -->
                                <div class="form-group row">
                                    {!! Form::label( 'fecf','Descripción del producto', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                    <div class="col-md-6">
                                        <input type="text" id="descripcionprod" name="descripcionpod" class="form-control"
                                            readonly
                                            value="{{($producto->Descripcion)}}">
                                    </div>
                                </div>

                                <div class="form-group row" >
                                    {!! Form::label( 'precios','Precios actuales por unidad ', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                </div>

                                <!-- precio x unidad compra  -->
                                <div class="form-group row" >
                                    {!! Form::label( 'total','Precio costo ', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                    <div class="col-md-6">
                                        <input type="text" id="costo" name="costo"  placeholder="$ x unidad" class="form-control"                                            
                                            value="<?php echo bcadd( $producto->PrecioCosto , '0', 2); ?>"
                                            min="1" max="9999"
                                            oninvalid="setCustomValidity('Solo se permite hasta 9999 ')" readonly
                                            oninput="setCustomValidity('')">
                                    </div>
                                </div>

                                <!-- Precio del producto -->
                                <div class="form-group row">
                                    {!! Form::label( 'precio','Precio venta', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                    <div class="col-md-6">
                                        <input type="text" id="precio" name="precio" class="form-control" readonly
                                        value="<?php echo bcadd( $producto->PrecioVenta , '0', 2); ?>">
                                    </div>
                                </div>

                                <!-- Descuento -->
                                <div class="form-group row ">
                                  <label for="des" class="col-md-4 col-form-label text-md-right">{{ __('Seleccione el descuento ')}}<strong>*</strong></label>
                                  <div class="col-md-6">
                                            <input type="number" id="descuento"name="descuento" min="0" max="100" step="5" value="{{($oferta->descuento)}}" onchange="cambiarcheckcodigo()"  onclick="cambiarcheckcodigo()" >
                                            %
                                            <input type="hidden"  id="tipo" name="tipo" class="form-control" readonly  value="">
                                    </div>
                                </div>

                                <!-- precio x unidad nuevo  -->
                                <div class="form-group row" >
                                    {!! Form::label( 'total','Nuevo Precio de venta: ', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                    <div class="col-md-6">
                                        <input type="text" id="nuevoprecio" name="nuevoprecio"  placeholder="$ x unidad" class="form-control"
                                            min="1" max="9999"
                                            value="<?php echo bcadd( $oferta->Precionuevo , '0', 2); ?>"
                                            oninvalid="setCustomValidity('Solo se permite hasta 9999 ')" readonly
                                            oninput="setCustomValidity('')">
                                    </div>
                                </div>

                                <br>

                                <div class="form-group row">
                                    <div class="col-md-4 col-form-label text-md-right">
                                        <button name="botonformmodificar" value="Modificar" type="submit" class="btn btn-success" style="margin-right:50px">Modificar</button>
                                    </div>
                                    <div class="col-md-4 col-form-label text-md-right">
                                        <button name="botonformmodificar" value="Cancelar" type="submit" class="btn btn-danger" style="margin-left:250px">Cancelar</button>
                                    </div>
                                </div>

                            {!!Form::close()!!}

                        </div>
                    </div>
                </div>
            </div>
        </div>

        <br><br><br>

    @endsection

@endif
