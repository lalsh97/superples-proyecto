@if( Auth::user()->Nivel == 1 )
    
    @extends('home')

    @section('title')
        Registro de descuento
    @endsection

    @section('contenido')

        <style>
            strong {

                color: red;
                font-size: 20px;
                
            }

        </style>

        <script >

            const FechaActual = new Date(new Date().getTime() - new Date().getTimezoneOffset() * 60000).toISOString().split("T")[0];
            var FechaMinimaFinal = new Date().setDate( new Date().getDate() + 1 );
            FechaMinimaFinal = new Date( FechaMinimaFinal ).toISOString().split("T")[0];

            window.onload = function( e ) {

                document.getElementById('fechaini').min = FechaActual;
                document.getElementById('fechafin').min = FechaMinimaFinal;

            }

            function FechaInicioSeleccionada() {
                //console.log("CHANGE FECHA INICIO");

                var fechainicial = new Date( document.getElementById("fechaini").value );
                fechainicial.setDate( fechainicial.getDate() + 1 );
                fechainicial = new Date( fechainicial ).toISOString().split("T")[0];
                document.getElementById('fechafin').min = fechainicial;

            }

            function FechaFinalSeleccionada() {
                //console.log("CHANGE FECHA FINAL");

                var fechainicialmaxima = new Date( document.getElementById("fechafin").value );
                fechainicialmaxima.setDate( fechainicialmaxima.getDate() - 1 );
                fechainicialmaxima = new Date( fechainicialmaxima ).toISOString().split("T")[0];
                document.getElementById('fechaini').max = fechainicialmaxima;

            }

            function cambiarcheckcodigo() {

                var descu = document.getElementById("descuento").value;
                var desc = parseInt( descu );
                var precio = document.getElementById("precio").value;
                //  var precio=4.35;
                var precion = precio - ( ( precio * desc ) / 100 );
                var preciototal = ( Math.round( precion * 100 ) / 100).toFixed( 2 );

                document.getElementById( "nuevoprecio" ).value = preciototal;
                document.getElementById( "tipo" ).value = desc;

            }

        </script>

        @if(Session::has('success'))
            <div class="alert alert-success">
                {{session('success')}}
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            </div>
        @endif

        <!-- FORMULARIO -->
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header"  align="center">{{ __('Registro de promoción: Descuento') }}</div>
                        <div class="card-body">

                            {!!Form::open(array('name' => 'formproducto', 'url'=>'insertardescuento','method'=>'POST' ,'files'=>true,'autocomplete'=>'off'))!!}

                                <!-- Imagen de la promocion  -->
                                <div class="form-group row">
                                    {!! Form::label( 'avatar','Imagen de la promoción', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                    <div class="col-md-6">
                                        <input type="file" id="avatar" name="avatar"  value="{{ old('avatar')}}" >
                                    </div>
                                </div>

                                <!-- Nombre de la promocion  -->
                                <div class="form-group row">
                                  <label for="nombre" class="col-md-4 col-form-label text-md-right">{{ __('Nombre de la promoción ')}}<strong>*</strong></label>
                                        <div class="col-md-6">
                                        <input type="text" id="nombre" name="nombre" placeholder="Ingrese el nombre de la promoción" class="form-control"
                                            value="{{session('nombre')}}" maxlength="45" minlength="1" pattern="([A-Za-z\s\W]|[0-9]){1,30}" required
                                            oninvalid="setCustomValidity('El nombre es obligatorio y debe tener como máximo 45 carácteres')"
                                            oninput="setCustomValidity('')" >
                                    </div>
                                </div>

                                <!-- Descripcion de la promoción  -->
                                <div class="form-group row">
                                  <label for="des" class="col-md-4 col-form-label text-md-right">{{ __('Descripción de la promoción ')}}<strong>*</strong></label>
                                        <div class="col-md-6">
                                        <input type="text" id="descripcion" name="descripcion" placeholder="Ingrese una descripción " class="form-control"
                                            value="{{session('descripcion')}}" maxlength="45" minlength="1" pattern="([A-Za-z\s\W]|[0-9]){1,30}" required
                                            oninvalid="setCustomValidity('La descripción es obligatoria')"
                                            oninput="setCustomValidity('')" >
                                    </div>
                                </div>

                                <!-- Fecha de inicio -->
                                <div class="form-group row">
                                  <label for="feci" class="col-md-4 col-form-label text-md-right">{{ __('Fecha de lanzamiento ')}}<strong>*</strong></label>
                                      <div class="col-md-6">
                                        <input type="date" id="fechaini" name="fechaini" class="form-control" required onchange="javascript:FechaInicioSeleccionada();"
                                            value="{{ old('fechaini') }}">
                                    </div>
                                </div>

                                <!-- Fecha fin -->
                                <div class="form-group row">
                                  <label for="fecf" class="col-md-4 col-form-label text-md-right">{{ __('Fecha de terminación ')}}<strong>*</strong></label>
                                    <div class="col-md-6">
                                        <input type="date" id="fechafin" name="fechafin" class="form-control" required onchange="javascript:FechaFinalSeleccionada();"
                                            value="{{ old('fechafin') }}">
                                    </div>
                                </div>

                                <!-- Codigo del producto -->
                                <div class="form-group row">
                                    {!! Form::label( 'fecf','Código del producto', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                    <div class="col-md-6">
                                        <input type="number" id="codigo" name="codigo" class="form-control"
                                            readonly
                                            value="{{($producto->Codigo)}}">
                                    </div>
                                </div>

                                <!-- Nombre del producto -->
                                <div class="form-group row">
                                    {!! Form::label( 'fecf','Descripción del producto', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                    <div class="col-md-6">
                                        <input type="text" id="descripcionprod" name="descripcionpod" class="form-control"
                                            readonly
                                            value="{{($producto->Descripcion)}}">
                                    </div>
                                </div>

                                <div class="form-group row" >
                                    {!! Form::label( 'precios','Precios actuales por unidad ', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                </div>

                                <!-- precio x unidad compra  -->
                                <div class="form-group row" >
                                    {!! Form::label( 'total','Precio de costo ', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                    <div class="col-md-6">
                                        <input type="number" id="costo" name="costo"  placeholder="$ x unidad" class="form-control"
                                            value="<?php echo bcadd( $producto->PrecioCosto , '0', 2); ?>"
                                            min="1" max="9999"
                                            oninvalid="setCustomValidity('Solo se permite hasta 9999 ')" readonly
                                            oninput="setCustomValidity('')">
                                    </div>
                                </div>

                                <!-- Precio del producto -->
                                <div class="form-group row">
                                    {!! Form::label( 'precio','Precio de venta', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                    <div class="col-md-6">
                                        <input type="number" id="precio" name="precio" class="form-control"
                                            readonly required
                                            value="<?php echo bcadd( $producto->PrecioVenta , '0', 2); ?>">
                                    </div>
                                </div>

                                <div class="form-group row ">
                                  <label for="des" class="col-md-4 col-form-label text-md-right">{{ __('Especifique el descuento ')}}<strong>*</strong></label>
                                    <div class="col-md-6">
                                            <input type="number" id="descuento"name="descuento" min="0" max="100" step="5" required value="0" onchange="cambiarcheckcodigo()"  onclick="cambiarcheckcodigo()" >
                                            <input type="hidden"  id="tipo" name="tipo" class="form-control" required
                                                readonly required
                                                value="0"> 
                                            <label> % </label>
                                    </div>
                                </div>

                                <!-- precio x unidad nuevo  -->
                                <div class="form-group row" >
                                    {!! Form::label( 'total','Nuevo Precio de venta: ', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                    <div class="col-md-6">
                                        <input type="number" id="nuevoprecio" name="nuevoprecio" class="form-control"
                                        min="1" max="9999"
                                        value="<?php echo bcadd( $producto->PrecioVenta , '0', 2); ?>"
                                        oninvalid="setCustomValidity('Solo se permite hasta 9999 ')" readonly required
                                        oninput="setCustomValidity('')">
                                    </div>
                                </div>

                                <br>

                                <div class="form-group row">
                                    <div class="col-md-4 col-form-label text-md-right">
                                        <button name="botonformpromocion" value="Registrar" type="submit" class="btn btn-success" style="margin-right:50px">Registrar</button>
                                    </div>
                                    <div class="col-md-4 col-form-label text-md-right">
                                        <button name="botonformpromocion" value="Cancelar" type="submit" class="btn btn-danger" style="margin-left:250px" formnovalidate>Cancelar</button>
                                    </div>
                                </div>


                            {!!Form::close()!!}

                        </div>
                    </div>
                </div>
            </div>
        </div>

        <br><br><br>

    @endsection

@endif
