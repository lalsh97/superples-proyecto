@if( Auth::user()->Nivel == 1 )
  
    @extends('home')

    @section('title')
        Modificación de información de una promoción
    @endsection

    @section('contenido')

        <style>
            #logo2 {
                width: 250px;
                height: 250px;
            }
            strong {
                color: red;
                font-size: 20px;
            }

        </style>

        <script>

            const FechaActual = new Date(new Date().getTime() - new Date().getTimezoneOffset() * 60000).toISOString().split("T")[0];
            var FechaMinimaFinal = new Date().setDate( new Date().getDate() + 1 );
            FechaMinimaFinal = new Date( FechaMinimaFinal ).toISOString().split("T")[0];


            

            var promocion = {!! json_encode( $promocion->toArray(), JSON_HEX_TAG ) !!};


            window.onload = function( e ) {

                document.getElementById('fechaini').min = promocion.Fechaini;
                //document.getElementById('fechaini').min = FechaActual;
                //document.getElementById('fechafin').min = FechaMinimaFinal;


                var fechainicial = new Date( document.getElementById("fechaini").value );
                fechainicial.setDate( fechainicial.getDate() + 1 );
                fechainicial = new Date( fechainicial ).toISOString().split("T")[0];
                document.getElementById('fechafin').min = fechainicial;

            }

            function FechaInicioSeleccionada() {
                //console.log("CHANGE FECHA INICIO");

                var fechainicial = new Date( document.getElementById("fechaini").value );
                fechainicial.setDate( fechainicial.getDate() + 1 );
                fechainicial = new Date( fechainicial ).toISOString().split("T")[0];
                document.getElementById('fechafin').min = fechainicial;

            }

            function FechaFinalSeleccionada() {
                //console.log("CHANGE FECHA FINAL");

                var fechainicialmaxima = new Date( document.getElementById("fechafin").value );
                fechainicialmaxima.setDate( fechainicialmaxima.getDate() - 1 );
                fechainicialmaxima = new Date( fechainicialmaxima ).toISOString().split("T")[0];
                document.getElementById('fechaini').max = fechainicialmaxima;

            }

            function prueba() {

                var precioproducto = document.getElementById("precio").value;
                var preciototal = parseFloat(precioproducto);
                var precioproducto2 = document.getElementById("precio2").value;
                var preciototal2 = parseFloat( precioproducto2 );

                document.getElementById( "total" ).value = preciototal2 + preciototal;

            }


            function cambiarcheckcodigo() {

                var precioproducto2 = document.getElementById("nuevoprecio2").value;
                var preciototal2 = parseFloat( precioproducto2 );

                var descu = document.getElementById("descuento").value;
                var desc = parseInt( descu );
                var precio = document.getElementById("precio").value;

                //  var precio=4.35;
                var precion = precio - ( ( precio * desc ) / 100 );
                var preciototal = Math.round( precion * 100 ) / 100;

                document.getElementById( "tipo" ).value = desc;
                document.getElementById( "nuevoprecio" ).value = ( Math.round( preciototal * 100 ) / 100 ).toFixed( 2 );
                document.getElementById( "total" ).value = ( Math.round( ( preciototal2 + preciototal ) * 100 ) / 100 ).toFixed( 2 );

            }

            function cambiarcheckcodigo2() {

                var precioproducto2 = document.getElementById("nuevoprecio").value;
                var preciototal2 = parseFloat(precioproducto2);

                var descu = document.getElementById("descuento2").value;
                var desc=parseInt(descu);
                var precio = document.getElementById("precio2").value;

                //  var precio=4.35;
                var precion = precio - ( ( precio * desc ) / 100 );
                var preciototal = Math.round( precion * 100 ) / 100;

                document.getElementById( "tipo2" ).value = desc;
                document.getElementById( "nuevoprecio2" ).value = ( Math.round( preciototal * 100 ) / 100 ).toFixed( 2 );
                document.getElementById( "total" ).value = ( Math.round( ( preciototal2 + preciototal ) * 100 ) / 100 ).toFixed( 2 );

            }

        </script>

        @if(Session::has('success'))
            <div class="alert alert-success">
                {{session('success')}}
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            </div>
        @endif

        <!-- TABLA -->
        <div class="container" >
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header"  align="center">{{ __('Actualización de datos de la promoción: Paquete') }}</div>
                        <div class="card-body">

                            {!!Form::open(array('name' => 'formpromocion', 'url'=>'/actualizarinfopromocion3/'.$promocion->idPromociones,'method'=>'PUT','files'=>true  ,'autocomplete'=>'off'))!!}

                                <!-- Imagen -->
                                <div class="form-group row">
                                    {!! Form::label( 'imagen','Imagen de la promoción', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                    <div class="card" style="width: 18rem;">
                                        <img class="card-img-top" src="/images/{{$promocion->Imagen}}" alt="">
                                    </div>
                                </div>

                                <!-- Nueva Imagen del producto -->
                                <div class="form-group row">
                                    {!! Form::label( 'Imagen','Cambiar imagen', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                    <div class="col-md-6">
                                        <input type="file" id="avatar" name="avatar" >
                                    </div>
                                </div>

                                <!-- Nombre de la promocion  -->
                                <div class="form-group row">
                                    {!! Form::label( 'nombre','Nombre de la promoción', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                    <div class="col-md-6">
                                        <input type="text" id="nombre" name="nombre" placeholder="Ingrese el nombre de la promoción" class="form-control"
                                            value="{{($promocion->Nombre)}}" readonly onclick="ok()" >
                                    </div>
                                </div>

                                <!-- Descripcion de la promoción  -->
                                <div class="form-group row">
                                    {!! Form::label( 'des','Descripción de la promoción', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                    <div class="col-md-6">
                                        <input type="text" id="descripcion" name="descripcion" placeholder="Ingrese una descripción " class="form-control"
                                        value="{{($promocion->Descripcion)}}" readonly>
                                    </div>
                                </div>

                                <!-- Fecha de inicio -->
                                <div class="form-group row">
                                    <label for="feci" class="col-md-4 col-form-label text-md-right">{{ __('Fecha de lanzamiento ')}}<strong>*</strong></label>
                                    <div class="col-md-6">
                                        <input type="date" id="fechaini" name="fechaini" class="form-control" required onchange="javascript:FechaInicioSeleccionada();"
                                            value="{{ ( $promocion->Fechaini ) }}">
                                    </div>
                                </div>

                                <!-- Fecha fin -->
                                <div class="form-group row">
                                    <label for="fecf" class="col-md-4 col-form-label text-md-right">{{ __('Fecha de terminación ')}}<strong>*</strong></label>
                                    <div class="col-md-6">
                                        <input type="date" id="fechafin" name="fechafin" class="form-control" required onchange="javascript:FechaFinalSeleccionada();"
                                            value="{{ ( $promocion->Fechafin ) }}">
                                    </div>
                                </div>


                                <!-- información del producto -->
                                <div class="container">
                                    <div class="row justify-content-center">
                                            <table class="table table-hover">
                                                <thead  class="thead-dark">
                                                    <tr>
                                                    <th scope="col>"> Información </th>
                                                    <th scope="col>"> Primer Producto </th>
                                                    <th scope="col>"> Segundo Producto </th>

                                                    </tr>

                                                    <!-- IMAGEN -->
                                                    <tr>
                                                        <td class="table-info"> Imagen </td>
                                                        <td> <img class="card-img-top " id="logo2" src="/images/{{$producto->imagen}}" alt=""> </td>
                                                        <td> <img class="card-img-top" id="logo2" src="/images/{{$producto2->imagen}}" alt=""> </td>
                                                    </tr>

                                                    <!-- CODIGO -->
                                                    <tr>
                                                        <td class="table-info"> Código</td>
                                                        <td><input type="number" id="codigo" name="codigo" class="form-control" readonly  value="{{($producto->Codigo)}}"></td>
                                                        <td><input type="number" id="codigo2" name="codigo2" class="form-control" readonly value="{{($producto2->Codigo)}}"></td>
                                                    </tr>

                                                    <!-- DESCRIPCION -->
                                                    <tr>
                                                        <td class="table-info"> Descripción</td>
                                                        <td> <input type="text" id="descripcionprod" name="descripcionpod" class="form-control" readonly  value="{{($producto->Descripcion)}}"> </td>
                                                        <td> <input type="text" id="descripcionprod2" name="descripcionpod2" class="form-control"  readonly value="{{($producto2->Descripcion)}}"> </td>
                                                    </tr>

                                                    <!-- PRECIO COSTO -->
                                                    <tr>
                                                        <td class="table-info"> Precio Costo </td>
                                                        <td> <input type="text" id="costo" name="costo"  placeholder="$ x unidad" class="form-control" value="<?php echo bcadd( $producto->PrecioCosto , '0', 2); ?>" readonly></td>
                                                        <td> <input type="text" id="costo2" name="costo2"  placeholder="$ x unidad" class="form-control" value="<?php echo bcadd( $producto2->PrecioCosto , '0', 2); ?>" readonly></td>
                                                    </tr>

                                                    <!-- PRECIO VENTA -->
                                                    <tr>
                                                        <td class="table-info"> Precio Venta </dh>
                                                        <td> <input type="text" id="precio" name="precio" class="form-control"  value="<?php echo bcadd( $producto->PrecioVenta , '0', 2); ?>" readonly> </td>
                                                        <td> <input type="text" id="precio2" name="precio2" class="form-control"  value="<?php echo bcadd( $producto2->PrecioVenta , '0', 2); ?>" readonly> </td>
                                                    </tr>

                                                    <!-- DESCUENTO -->
                                                    <tr>
                                                        <td class="table-info"> Descuento % <strong>*</strong></dh>
                                                        <td> <input type="number" id="descuento"name="descuento" min="0" max="100" step="5"  value="{{($oferta->Descuento1)}}" class="form-control" value="0" onchange="cambiarcheckcodigo()"  onclick="cambiarcheckcodigo()"  > </td>
                                                        <td> <input type="number" id="descuento2"name="descuento2" min="0" max="100" step="5"  value="{{($oferta->Descuento2)}}"class="form-control" value="0" onchange="cambiarcheckcodigo2()"  onclick="cambiarcheckcodigo2()"  > </td>
                                                    </tr>

                                                    <!-- NUEVO PRECIO VENTA -->
                                                    <tr>
                                                        <td class="table-info"> Nuevo Precio Venta </td>
                                                        <td> <input type="text" id="nuevoprecio" name="nuevoprecio"  placeholder="$ x unidad" class="form-control" readonly value="<?php echo bcadd( $oferta->Precio1 , '0', 2); ?>"> </td>
                                                        <td> <input type="text" id="nuevoprecio2" name="nuevoprecio2"  placeholder="$ x unidad" class="form-control" readonly value="<?php echo bcadd( $oferta->Precio2 , '0', 2); ?>"> </td>
                                                    </tr>

                                                </thead>
                                            </table>
                                        </div>
                                </div>

                                <input type="hidden"  id="tipo" name="tipo" class="form-control" readonly value="">
                                <input type="hidden"  id="tipo2" name="tipo2" class="form-control" readonly value="">

                                <!-- Total de la promoción -->
                                <div class="form-group row">
                                        {!! Form::label( 'total','Total de la promoción ', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                        <div class="col-md-6">
                                            <input type="text" id="total" name="total" class="form-control" readonly value="<?php echo bcadd( $promocion->Total , '0', 2); ?>">
                                        </div>
                                </div>

                                <br>

                                <div class="form-group row">
                                    <div class="col-md-4 col-form-label text-md-right">
                                        <button name="botonformmodificar" value="Modificar" type="submit" class="btn btn-success" style="margin-right:50px">Modificar</button>
                                    </div>
                                    <div class="col-md-4 col-form-label text-md-right">
                                        <button name="botonformmodificar" value="Cancelar" type="submit" class="btn btn-danger" style="margin-left:250px">Cancelar</button>
                                    </div>
                                </div>


                            {!!Form::close()!!}

                        </div>
                    </div>
                </div>
            </div>
        </div>

        <br><br><br>
        
    @endsection

@endif
