@extends('home')

    @section('title')
        Información detalle venta
    @endsection

@section('contenido')

    <style>

        .scrollbar {

            height: 400px;
            overflow: auto;
            overflow-x: hidden;
            overflow-y: scroll;
            white-space:nowrap;
            border-radius: 10px;            

        }

        #scrollbar-style::-webkit-scrollbar {
            
            width: 6px;
            background-color: #F5F5F5;

        }

        /* Se pone un color de fondo y se redondean las esquinas del thumb */
        #scrollbar-style::-webkit-scrollbar-thumb {

            
            border-radius: 4px;
            /*background-color: rgb(84, 81, 72);*/
            background-color: rgb(49, 163, 204);

        }

        /* Se cambia el fondo y se agrega una sombra cuando esté en hover */
        #scrollbar-style::-webkit-scrollbar-thumb:hover {

            background: #b3b3b3;
            box-shadow: 0 0 2px 1px rgba(0, 0, 0, 0.2);

        }

        /* Se cambia el fondo cuando esté en active */
        #scrollbar-style::-webkit-scrollbar-thumb:active {
            
            background-color: #999999;

        }
        /* Se pone un color de fondo y se redondean las esquinas del track */
        #scrollbar-style::-webkit-scrollbar-track {

            background: #e1e1e1;
            border-radius: 4px;

        }

        /* Se cambia el fondo cuando esté en active o hover */
        #scrollbar-style::-webkit-scrollbar-track:hover,
        #scrollbar-style::-webkit-scrollbar-track:active {

            background: #d4d4d4;

        }


    </style>

    

    <!-- DIV DATOS VENTA -->
    <div class="container" style="margin-bottom: 3px;">
        
        <!-- DIV FECHA ESPECIFICA -->
        <div class="form-row align-items-center">
            
            <!-- ID VENTA -->
            <div class="col-xs-3">
                <label for="idventa"  style="margin-left: 20px;">
                    Id venta: 
                    <span style="font-weight: bold; font-size:18px">
                        <?php
                            echo $datosventa->idventa;
                        ?>
                    </span>
                </label>
            </div>
            
            <!-- NOMBRE EMPLEADO -->
            <div class="col-xs-3">
                <label for="nombrecompletoempleado"  style="margin-left: 20px;">
                    Nombre Empleado:  
                    <span style="font-weight: bold; font-size:18px">
                        <?php
                            echo $datosventa->nombrecompletoempleado;
                        ?>
                    </span>
                </label>
            </div>
            
            <!--NOMBRE CLIENTE -->
            <div class="col-xs-3">
                <label for="nombrecompletocliente"  style="margin-left: 20px;">
                    Nombre Cliente:  
                    <span style="font-weight: bold; font-size:18px">
                        <?php
                            echo $datosventa->nombrecompletocliente;
                        ?>
                    </span>
                </label>
            </div>
            
            <!-- FECHA -->
            <div class="col-xs-3">
                <label for="fechaventa"  style="margin-left: 20px;">
                    Fecha de la venta: 
                    <span style="font-weight: bold; font-size:18px">
                        <?php
                            echo $datosventa->fechaventa;
                        ?>
                    </span>
                </label>
            </div>
            
            <!-- TOTAL VENTA -->
            <div class="col-xs-3">
                <label for="totalventa"  style="margin-left: 20px;">
                    Total de la venta: 
                    <span style="font-weight: bold; font-size:18px">
                        <?php
                            echo bcadd( $datosventa->totalventa, '0', 2 );
                        ?>
                    </span>
                </label>
            </div>
            
            <!-- TIPO PAGO -->
            <div class="col-xs-3">
                <label for="tipopago"  style="margin-left: 20px;">
                    Tipo Pago:  
                    <span style="font-weight: bold; font-size:18px">
                        <?php
                            if( $datosventa->tipopago == 0 ) {
                                echo "Crédito";
                            } else {
                                echo "Contado";
                            }
                        ?>
                    </span>
                </label>
            </div>
            
        </div>
        
        
    </div>

    <!-- Tabla -->
    <div class="container">
        <div class="row justify-content-center">    
                                
            <div class="scrollbar" id="scrollbar-style">

                <table class="table table-hover" style="width: 1000px" id="tabladatosdetalleventa" name="tabladatosdetalleventa">
                    <thead  class="thead-dark">
                        <tr>
                            <th scope="col"> Código producto </th>             
                            <th scope="col"> Descripción producto </th>
                            <th scope="col"> Cantidad vendida  </th>
                            <th scope="col"> Precio venta </th>             
                            <th scope="col"> Importe </th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ( $datosdetalleventa as $ddv )
                            <tr>
                                <td>{{ $ddv-> CodigoProductoDetalleVenta }}</td>         
                                <td>{{ $ddv-> DescripcionProducto }}</td>
                                <td>{{ $ddv-> CantidadDetalleVenta }}</td>
                                <td> <?php echo bcadd( $ddv->PrecioVentaProducto , '0', 2); ?></td>   
                                <td><?php
                                    $precioventafor = bcadd( $ddv->PrecioVentaProducto , '0', 2);
                                    $cantidadfor = $ddv->CantidadDetalleVenta;
                                    $importe = ( $precioventafor * $cantidadfor );
                                    echo bcadd( $importe, '0', 2);
                                ?></td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

            </div>
        
        </div>
    </div>

@endsection