<html>
    
    <head>

        <style>

            #logo {

                position: fixed;
                top: 10px;
                left: 10px;
                border-radius: 4px;
                padding: 5px;
                width: 150px;

            }

            #logo2 {

                position: fixed;
                top: 20px;
                left: 850px;
                border-radius: 4px;
                padding: 5px;
                width: 150px;

            }

            .encabezado {
                
                background-color: #91D5F7;
                padding: 25px 10px 40px 0px;
                border: 0px;
                margin: 0px;
                text-align: center;

            }

            h2 {

                padding: 15px 10px 15px 0px;
                border: 0px;
                margin: 0px;
                color:black;
                font-variant: all-petite-caps;

            }

            hr {

                border: black 1px solid;
                background: orange;
                padding: 10px;
                border: 0px;
                margin: 0px;

            }

            table {

                font: oblique bold 100% cursive;
                border-collapse: collapse;
                text-align: left;
                width: 1000px;

            }

            .id {

                width: 25px;
                color: white;
                background: black;

            }

            .codigo {

                width: 100px;
                color: white;
                background: black;

            }

            .descripcion {

                width:  300px;
                color: white;
                background: black;

            }

            .cantidad {

                width: 25px;
                color: white;
                background: black;

            }

            .precioventa {

                width: 75px;
                color: white;
                background: black;

            }

            .preciocosto {

                width: 75px;
                color: white;
                background: black;

            }

            .importe {

                width: 75px;
                color: white;
                background: black;

            }
            
            .utilidad {

                width:  75px;
                color: white;
                background: black;

            }

            #columna {

                background:#F7F5E4;

            }

            p{

                text-align: center;
                padding: 15px;
                font-family: "Segoe IU", sans-serif;

            }

        </style>

      <title>CORTE DEL DIA</title>

    </head>

    <body>

        <script type="text/php">
            if ( isset($pdf) ) {
                $pdf->page_script('
                    $font = $fontMetrics->get_font("Arial, Helvetica, sans-serif", "normal");
                    $pdf->text(400, 20, "Pág $PAGE_NUM de $PAGE_COUNT", $font, 10);
                ');
            }
        </script>

        <div style="border:3px solid black;">
            
            <div>
                <h1 class="encabezado"> {{ $tienda->Nombre }} </h1>
                <img id="logo" src="img/icono.png"  width="150"  alt="return" />
                <img id="logo2" src="img/LogoPLES.png"  width="150"  alt="return" />
            </div>

            <h2 style="text-align:center;"> CORTE DEL DÍA {{ $fechaactual }}</h2>
            <h2 style="text-align:center;"> Reporte de compras </h2>                        
            
            <p style="font-size: 18px; text-align: center;">EMPLEADO<strong> {{ $informacionempleado[0]->nombre . " " . $informacionempleado[0]->apellidopat . " " . $informacionempleado[0]->apellidomat }}</strong> CAJA <strong> {{ $idcaja}} </strong></p>
                    
            <!-- DIV TOTALCOMPRAS -->
            <div style="margin-left: 25%;">
                        
                <!-- TOTAL DE LAS VENTAS -->
                <label for="totalcompras">
                    Total de las compras:
                    <span style="font-weight: bold; font-size:18px">
                        <?php
                            echo bcadd( $consultatotal[0]->totalcompras, '0', 2 );
                        ?>
                    </span>
                </label>
                        
                <!-- CANTIDAD TOTAL PRODUCTOS VENDIDOS -->
                <label for="totalcompras">
                    Cantidad total de productos comprados:
                    <span style="font-weight: bold; font-size:18px">
                        <?php

                            if( !isset( $cantidadtotalproductoscomprados[0]->cantidadtotalproductoscomprados ) ) {
                                
                                echo "0";

                            } else {

                                echo $cantidadtotalproductoscomprados[0]->cantidadtotalproductoscomprados;

                            }
                            
                        ?>
                    </span>
                </label>
                
            </div>

            <br>

            <hr>
              
            <br>
            <br>

            <table border="2" style="margin: 1 auto;">
                <thead>
                    <tr class="odd">
                        <th class="id" scope="col"> ID pedido </th>
                        <th class="codigo" scope="col"> Código producto </th>
                        <th class="descripcion" scope="col"> Descripción producto </th>
                        <th class="cantidad" scope="col"> Cantidad </th>
                        <th class="precioventa" scope="col"> Precio venta </th>
                        <th class="preciocosto" scope="col"> Precio costo </th>
                        <th class="importe" scope="col"> Importe </th>
                        <th class="utilidad" scope="col"> Utilidad </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ( $consultascomprasactual as $cca )
                        <tr id="columna">
                            <td>{{ $cca->idpedido }}</td>
                            <td>{{ $cca->codigoproducto }}</td>
                            <td>{{ $cca->descripcionproducto }}</td>
                            <td>{{ $cca->cantidadpedido }}</td>
                            <td> <?php echo bcadd( $cca->precioventaproducto, '0', 2); ?></td>
                            <td> <?php echo bcadd( $cca->preciocostoproducto, '0', 2); ?></td>
                            <td> <?php echo bcadd( ( $cca->cantidadpedido  * $cca->preciocostoproducto ), '0', 2); ?></td>
                            <td> <?php echo bcadd( ( ( $cca->precioventaproducto  - $cca->preciocostoproducto ) * $cca->cantidadpedido ), '0', 2); ?></td>
                        </tr>
                    @endforeach
                </tbody>
            </table>

            <br>
            <br>
            <br>

        </div>

    </body>

</html>