<html>
    
    <head>

        <style>

            #logo {

                position: fixed;
                top: 10px;
                left: 10px;
                border-radius: 4px;
                padding: 5px;
                width: 150px;

            }

            #logo2 {

                position: fixed;
                top: 20px;
                left: 850px;
                border-radius: 4px;
                padding: 5px;
                width: 150px;

            }

            .encabezado {
                
                background-color: #91D5F7;
                padding: 25px 10px 40px 0px;
                border: 0px;
                margin: 0px;
                text-align: center;

            }

            h2 {

                padding: 15px 10px 15px 0px;
                border: 0px;
                margin: 0px;
                color:black;
                font-variant: all-petite-caps;

            }

            hr {

                border: black 1px solid;
                background: orange;
                padding: 10px;
                border: 0px;
                margin: 0px;

            }

            table {

                font: oblique bold 100% cursive;
                border-collapse: collapse;
                text-align: left;
                width: 1000px;

            }

            .id {

                width: 25px;
                color: white;
                background: black;

            }

            .codigo {

                width: 100px;
                color: white;
                background: black;

            }

            .descripcion {

                width:  300px;
                color: white;
                background: black;

            }

            .cantidad {

                width: 25px;
                color: white;
                background: black;

            }

            .precioventa {

                width: 75px;
                color: white;
                background: black;

            }

            .preciocosto {

                width: 75px;
                color: white;
                background: black;

            }

            .importe {

                width: 75px;
                color: white;
                background: black;

            }
            
            .utilidad {

                width:  75px;
                color: white;
                background: black;

            }

            #columna {

                background:#F7F5E4;

            }

            p{

                text-align: center;
                padding: 15px;
                font-family: "Segoe IU", sans-serif;

            }

        </style>

        <title>CORTE DEL DIA</title>

    </head>

    <body>

        <script type="text/php">
            if ( isset($pdf) ) {
                $pdf->page_script('
                    $font = $fontMetrics->get_font("Arial, Helvetica, sans-serif", "normal");
                    $pdf->text(400, 20, "Pág $PAGE_NUM de $PAGE_COUNT", $font, 10);
                ');
            }
        </script>

        <div style="border:3px solid black;">
              
            <div>
                <h1 class="encabezado"> {{$tienda->Nombre}} </h1>
                <img id="logo" src="img/icono.png"  width="150"  alt="return" />
                <img id="logo2" src="img/LogoPLES.png"  width="150"  alt="return" />
            </div>

            <h2 style="text-align:center;"> CORTE DEL DÍA {{ $fechaactual }}</h2>
            <h2 style="text-align:center;"> Reporte de ventas </h2>
            <p style="font-size: 18px; text-align: center;">EMPLEADO<strong> {{ $informacionempleado[0]->nombre . " " . $informacionempleado[0]->apellidopat . " " . $informacionempleado[0]->apellidomat }}</strong> CAJA <strong> {{ $idcaja}} </strong></p>
                                
            <!-- DIV TOTALVENTAS -->
            <div style="margin-left: 1%;">
                        
                <!-- TOTAL DE LAS VENTAS -->
                <label for="totalventas">
                    Total de las ventas:
                    <span style="font-weight: bold; font-size:18px">
                        <?php
                            echo bcadd( $consultatotal[0]->totalventas, '0', 2 );
                        ?>
                    </span>
                </label>
                        
                <!-- CANTIDAD TOTAL PRODUCTOS VENDIDOS -->
                <label for="totalventas" style="margin-left: 5%;">
                    Cantidad total de productos vendidos:
                    <span style="font-weight: bold; font-size:18px">
                        <?php
                                         
                            if( !isset( $consultatotalproductosvendidos[0]->cantidadtotalproductosvendidos ) ) {
                                
                                echo "0";

                            } else {

                            echo $consultatotalproductosvendidos[0]->cantidadtotalproductosvendidos;

                            }

                        ?>
                    </span>
                </label>
                                
                <!-- TOTAL DE VENTAS REALIZADAS -->
                <label for="consultatotalventas" style="margin-left: 5%;">
                    Total de ventas realizadas:
                    <span style="font-weight: bold; font-size:18px">
                        <?php
                            echo $consultatotalventas[0]->consultatotalventas;
                        ?>
                    </span>
                </label>
                
            </div>
                    
            <!-- DIV IMPORTE TOTAL DE LAS VENTAS PAGADAS COMPLETAMENTE CON CRÉDITO -->
            <div style="margin-left: 1%;">
                        
                <!-- IMPORTE TOTAL DE LAS VENTAS PAGADAS COMPLETAMENTE CON CRÉDITO -->
                <label for="totalventascreditocompleto">
                    Importe total de las ventas pagadas completamente con crédito:
                    <span style="font-weight: bold; font-size:18px">
                        <?php
                            echo bcadd( $consultatotalventascreditocompleto[0]->totalventascreditocompleto, '0', 2 );
                        ?>
                    </span>
                </label>
                
            </div>

            <!-- DIV IMPORTE TOTAL VENTAS PAGADAS CON CRÉDITO - CREDITO -->
            <div style="margin-left: 1%;">
                        
                <!-- IMPORTE TOTAL VENTAS PAGADAS CON CRÉDITO - CREDITO -->
                <label for="consultatotalventascreditopartes">
                    Importe total de las ventas pagadas con crédito en partes (crédito):
                    <span style="font-weight: bold; font-size:18px">
                        <?php
                            echo bcadd( ( $consultatotalventascreditopartes[0]->total - $consultatotalventascreditopartes[0]->totalpagadoefectivo  ), '0', 2 );
                        ?>
                    </span>
                </label>

            </div>

            <!-- DIV IMPORTE TOTAL DE LAS VENTAS PAGADAS EN EFECTIVOS -->
            <div style="margin-left: 1%;">
                        
                <!-- IMPORTE TOTAL DE LAS VENTAS PAGADAS EN EFECTIVOS -->
                <label for="consultatotalventasefectivo">
                    Importe total de las ventas pagadas en efectivo:
                    <span style="font-weight: bold; font-size:18px">
                        <?php
                            echo bcadd( $consultatotalventasefectivo[0]->consultatotalventasefectivo, '0', 2 );
                        ?>
                    </span>
                </label>

            </div>
            
            <!-- DIV IMPORTE TOTAL VENTAS PAGADAS CON CRÉDITO - EFECTIVO -->
            <div style="margin-left: 1%;">
                        
                <!-- IMPORTE TOTAL VENTAS PAGADAS CON CRÉDITO - EFECTIVO -->
                <label for="consultatotalventascreditopartes">
                    Importe total de las ventas pagadas con crédito en partes (efectivo):
                    <span style="font-weight: bold; font-size:18px">
                        <?php
                            echo bcadd( ( $consultatotalventascreditopartes[0]->totalpagadoefectivo  ), '0', 2 );
                        ?>
                    </span>
                </label>
                
            </div>

            <!-- DIV SUMA IMPORTE TOTAL VENTAS PAGADAS CON CRÉDITO - EFECTIVO MÁS VENTAS PAGADAS EFECTIVO -->
            <div style="margin-left: 1%;">

                <!-- SUMA IMPORTE TOTAL VENTAS PAGADAS CON CRÉDITO - EFECTIVO MÁS VENTAS PAGADAS EFECTIVO -->
                <label for="consultatotalventascreditopartes">
                    Importe total de las ventas pagadas con crédito en partes (efectivo) + Importe total de las ventas pagadas en efectivo:
                    <span style="font-weight: bold; font-size:18px">
                        <?php
                            echo bcadd( ( $consultatotalventasefectivo[0]->consultatotalventasefectivo ) + ( $consultatotalventascreditopartes[0]->totalpagadoefectivo ) , '0', 2 );
                        ?>
                    </span>
                </label>

            </div>

            <!-- DIV TOTAL DESCUENTO -->
            <div style="margin-left: 1%;">
                        
                <!-- TOTAL DESCUENTO -->
                <label for="consultatotaldescuento">
                    Total descuento:
                    <span style="font-weight: bold; font-size:18px">
                        <?php
                            echo bcadd( $consultatotaldescuento[0]->totaldescuento, '0', 2 );
                        ?>
                    </span>
                </label>

            </div>
            
            <!-- DIV TOTAL ABONOS -->
            <div style="margin-left: 1%;">
                                
                <!-- TOTAL ABONOS -->
                <label for="consultatotaldescuento">
                    Total abonos:
                    <span style="font-weight: bold; font-size:18px">
                        <?php
                            echo bcadd( $consultaabonoscreditos[0]->totalabonado, '0', 2 );
                        ?>
                    </span>
                </label>

            </div>

            <!-- DIV SALDO INICIAL -->
            <div style="margin-left: 1%;">
                        
                <!-- SALDO INICIAL -->
                <label for="consultasaldoinicial">
                    Saldo inicial de la caja:
                    <span style="font-weight: bold; font-size:18px">
                        <?php
                            echo bcadd( $consultasaldoinicial[0]->saldoinicial, '0', 2 );
                        ?>
                    </span>
                </label>

            </div>

            <!-- DIV SALDO ACTUAL -->
            <div style="margin-left: 1%;">
                        
                <!-- SALDO ACTUAL -->
                <label for="consultasaldoinicial">
                    Saldo actual de la caja:
                    <span style="font-weight: bold; font-size:18px">
                        <?php
                            echo bcadd( ( $consultasaldoinicial[0]->saldoinicial + ( $consultatotalventasefectivo[0]->consultatotalventasefectivo ) + ( $consultatotalventascreditopartes[0]->totalpagadoefectivo ) + ( $consultaabonoscreditos[0]->totalabonado ) ), '0', 2 );
                        ?>
                    </span>
                </label>

            </div>

            <br>
            
            <hr>
            
            <br>

            <table border="2" style="margin: 1 auto;">
                <thead>
                    <tr class="odd">
                        <th class="id" scope="col"> ID venta </th>
                        <th class="codigo" scope="col"> Código producto </th>
                        <th class="descripcion" scope="col"> Descripción producto </th>
                        <th class="cantidad" scope="col"> Cantidad </th>
                        <th class="precioventa" scope="col"> Precio venta </th>                        
                        <?php
                                if( $nivelusuario == 1 ) {
                        ?>
                            <th class="preciocosto" scope="col"> Precio costo </th>
                        <?php
                            }
                        ?>
                        <th class="importe" scope="col"> Importe </th>
                        <?php
                            if( $nivelusuario == 1 ) {
                        ?>
                            <th class="utilidad" scope="col"> Utilidad </th>
                        <?php
                            }
                        ?>
                    </tr>
                </thead>
                <tbody>
                    @foreach ( $consultasventasactual as $cva )
                        <tr id="columna">
                            <td>{{ $cva->idventa }}</td>
                            <td>{{ $cva->codigoproducto }}</td>
                            <td>{{ $cva->descripcionproducto }}</td>
                            <td>{{ $cva->cantidadventa }}</td>
                            <td> <?php echo bcadd( $cva->precioventaproducto, '0', 2); ?></td>
                            <?php
                                if( $nivelusuario == 1 ) {
                            ?>
                                <td> <?php echo bcadd( ( $cva->preciocostoproducto ), '0', 2); ?></td>
                            <?php
                                }
                            ?>
                            <td> <?php echo bcadd( ( $cva->cantidadventa  * $cva->precioventaproducto ), '0', 2); ?></td>
                            <?php
                                if( $nivelusuario == 1 ) {
                            ?>
                                <td> <?php echo bcadd( ( ( $cva->precioventaproducto  - $cva->preciocostoproducto ) * $cva->cantidadventa ), '0', 2); ?></td>
                            <?php
                                }
                            ?>
                        </tr>
                    @endforeach
                </tbody>
            </table>

            <br>
            <br>
            <br>

        </div>

    </body>

</html>