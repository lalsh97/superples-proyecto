@if( Auth::user()->Nivel == 1 )

    @extends('home')

        @section('title')
            Información del corte de caja - compras
        @endsection

    @section('contenido')

        <style>

            .scrollbar {

                height: 400px;
                overflow: auto;
                overflow-x: hidden;
                overflow-y: scroll;
                white-space:nowrap;
                border-radius: 10px;            

            }

            #scrollbar-style::-webkit-scrollbar {
                
                width: 6px;
                background-color: #F5F5F5;

            }

            /* Se pone un color de fondo y se redondean las esquinas del thumb */
            #scrollbar-style::-webkit-scrollbar-thumb {

                
                border-radius: 4px;
                /*background-color: rgb(84, 81, 72);*/
                background-color: rgb(49, 163, 204);

            }

            /* Se cambia el fondo y se agrega una sombra cuando esté en hover */
            #scrollbar-style::-webkit-scrollbar-thumb:hover {

                background: #b3b3b3;
                box-shadow: 0 0 2px 1px rgba(0, 0, 0, 0.2);

            }

            /* Se cambia el fondo cuando esté en active */
            #scrollbar-style::-webkit-scrollbar-thumb:active {
                
                background-color: #999999;

            }
            /* Se pone un color de fondo y se redondean las esquinas del track */
            #scrollbar-style::-webkit-scrollbar-track {

                background: #e1e1e1;
                border-radius: 4px;

            }

            /* Se cambia el fondo cuando esté en active o hover */
            #scrollbar-style::-webkit-scrollbar-track:hover,
            #scrollbar-style::-webkit-scrollbar-track:active {

                background: #d4d4d4;

            }

            
            fieldset {
                
                background-color: #eeeeee;
                margin-top: 5px;

            }

            legend {

                background-color: #212529;
                color: white;
                padding: 5px 10px;
                border-radius: 6px;

            }


        </style>
            
        @if(Session::has('success'))
            <div class="alert alert-success">
                {{session('success')}}
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            </div>
        @elseif( Session::has('warning'))
            <div class="alert alert-warning">
                {{session('warning')}}
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            </div>
        @elseif( Session::has('danger'))
            <div class="alert alert-danger">
                {{session('danger')}}
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            </div>
        @endif

        <p style="font-size: 24px; text-align: center;"><strong>CORTE DE CAJA - COMPRAS</strong></p>
        <p style="font-size: 18px; text-align: center;">EMPLEADO<strong> {{ $informacionempleado[0]->nombre . " " . $informacionempleado[0]->apellidopat . " " . $informacionempleado[0]->apellidomat }}</strong> CAJA <strong> {{ $idcaja}} </strong></p>
        

        <!-- DIV DATOS GENERAL -->
        <div class="container" style="margin-bottom: 20px;">
            
            <!-- FIELDSET INFORMACION GENERAL -->
            <fieldset  class="border p-">
                <legend  class="w-auto"> INFORMACIÓN GENERAL </legend>
                    
                <!-- DIV DATOS CENTER -->
                <div class="container">
                    <div class="form-row align-items-center">
                        
                        <!-- DIV TOTALVENTAS Y FECHAACTUAL -->
                        <div class="row" style="margin-left: 10%;">
                                    
                            <!-- TOTAL DE LAS VENTAS -->
                            <div class="col-xs-3">
                                <label for="totalcompras"  style="margin-left: 20px;">
                                    Total de las compras:
                                    <span style="font-weight: bold; font-size:18px">
                                        <?php
                                            echo bcadd( $consultatotal[0]->totalcompras, '0', 2 );
                                        ?>
                                    </span>
                                </label>
                            </div>
                                    
                            <!-- CANTIDAD TOTAL PRODUCTOS VENDIDOS -->
                            <div class="col-xs-3">
                                <label for="totalcompras"  style="margin-left: 50px;">
                                    Cantidad total de productos comprados:
                                    <span style="font-weight: bold; font-size:18px">
                                        <?php
                                            
                                            if( !isset(  $cantidadtotalproductoscomprados[0]->cantidadtotalproductoscomprados ) ) {
                                                
                                                echo "0";

                                            } else {
                                                
                                                echo $cantidadtotalproductoscomprados[0]->cantidadtotalproductoscomprados;

                                            }

                                        ?>
                                    </span>
                                </label>
                            </div>
                            
                            <!-- FECHA ACTUAL -->
                            <div class="col-xs-3">
                                <label for="fechaactual"  style="margin-left: 200px;">
                                    Fecha:
                                    <span id="fechaactual" style="font-weight: bold; font-size:18px">
                                        <?php
                                            echo $fechaactual;
                                        ?>
                                    </span>
                                </label>
                            </div>
                            
                        </div>
                        
                    </div>
                </div>
                    
            </fieldset>
                    
        </div>

        <!-- Tabla -->
        <div class="container">
            <div class="row justify-content-center">    
                                    
                <div class="scrollbar" id="scrollbar-style">

                    <table class="table table-hover" style="width: 1200px;" id="tabladatosabonocredito" name="tabladatosabonocredito">
                        <thead  class="thead-dark">
                            <tr>
                                <th scope="col" style="width: 100px;"> ID pedido </th>
                                <th scope="col" style="width: 150px;"> Código producto </th>
                                <th scope="col" style="width: 300px;"> Descripción producto </th>
                                <th scope="col" style="width: 100px;"> Cantidad </th>
                                <th scope="col" style="width: 100px;"> Precio venta </th>
                                <th scope="col" style="width: 100px;"> Precio costo </th>
                                <th scope="col" style="width: 150px;"> Importe </th>
                                <th scope="col" style="width: 150px;"> Utilidad </th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ( $consultascomprasactual as $cca )
                                <tr>
                                    <td>{{ $cca->idpedido }}</td>
                                    <td>{{ $cca->codigoproducto }}</td>
                                    <td>{{ $cca->descripcionproducto }}</td>
                                    <td>{{ $cca->cantidadpedido }}</td>
                                    <td> <?php echo bcadd( $cca->precioventaproducto, '0', 2); ?></td>
                                    <td> <?php echo bcadd( $cca->preciocostoproducto, '0', 2); ?></td>
                                    <td> <?php echo bcadd( ( $cca->cantidadpedido  * $cca->preciocostoproducto ), '0', 2); ?></td>
                                    <td> <?php echo bcadd( ( ( $cca->precioventaproducto  - $cca->preciocostoproducto ) * $cca->cantidadpedido ), '0', 2); ?></td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>

                </div>
            
            </div>
        </div>

        <br>
        <br>
        <br>
        
        <!-- Botón Exportar PDF -->
        <div class="container col-2">
            <div class="card">
                <div class="card-body">                                               
                    <form action="{{route('exportarcortecajadiacompras')}}" method="get">
                        <button type="submit" id="botonexportarcortecajadia" class="btn btn-info" style="margin-left: 15%" >Generar PDF</button>
                    </form>
                </div>
            </div>
        </div>

    @endsection

@endif