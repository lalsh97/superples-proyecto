@extends('home')

    @section('title')
        Información del corte de caja - ventas
    @endsection

@section('contenido')

    <style>

        .scrollbar {

            height: 400px;
            overflow: auto;
            overflow-x: hidden;
            overflow-y: scroll;
            white-space:nowrap;
            border-radius: 10px;            

        }

        #scrollbar-style::-webkit-scrollbar {
            
            width: 6px;
            background-color: #F5F5F5;

        }

        /* Se pone un color de fondo y se redondean las esquinas del thumb */
        #scrollbar-style::-webkit-scrollbar-thumb {

            
            border-radius: 4px;
            /*background-color: rgb(84, 81, 72);*/
            background-color: rgb(49, 163, 204);

        }

        /* Se cambia el fondo y se agrega una sombra cuando esté en hover */
        #scrollbar-style::-webkit-scrollbar-thumb:hover {

            background: #b3b3b3;
            box-shadow: 0 0 2px 1px rgba(0, 0, 0, 0.2);

        }

        /* Se cambia el fondo cuando esté en active */
        #scrollbar-style::-webkit-scrollbar-thumb:active {
            
            background-color: #999999;

        }
        /* Se pone un color de fondo y se redondean las esquinas del track */
        #scrollbar-style::-webkit-scrollbar-track {

            background: #e1e1e1;
            border-radius: 4px;

        }

        /* Se cambia el fondo cuando esté en active o hover */
        #scrollbar-style::-webkit-scrollbar-track:hover,
        #scrollbar-style::-webkit-scrollbar-track:active {

            background: #d4d4d4;

        }

        
        fieldset {
            
            background-color: #eeeeee;
            margin-top: 5px;

        }

        legend {

            background-color: #212529;
            color: white;
            padding: 5px 10px;
            border-radius: 6px;

        }


    </style>
        
    @if(Session::has('success'))
        <div class="alert alert-success">
            {{session('success')}}
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        </div>
    @elseif( Session::has('warning'))
        <div class="alert alert-warning">
            {{session('warning')}}
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        </div>
    @elseif( Session::has('danger'))
        <div class="alert alert-danger">
            {{session('danger')}}
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        </div>
    @endif

    <p style="font-size: 24px; text-align: center;"><strong>CORTE DE CAJA - VENTAS</strong></p>
    <p style="font-size: 18px; text-align: center;">EMPLEADO<strong> {{ $informacionempleado[0]->nombre . " " . $informacionempleado[0]->apellidopat . " " . $informacionempleado[0]->apellidomat }}</strong> CAJA <strong> {{ $idcaja}} </strong></p>
    

    <!-- DIV DATOS GENERAL -->
    <div class="container" style="margin-bottom: 20px;">
        
        <!-- FIELDSET INFORMACION GENERAL -->
        <fieldset  class="border p-">
            <legend  class="w-auto"> INFORMACIÓN GENERAL </legend>
                
            <!-- DIV DATOS CENTER -->
            <div class="container">
                <div class="form-row align-items-center">
                    
                    <!-- DIV TOTALVENTAS Y FECHAACTUAL -->
                    <div class="row" style="margin-left: 1%;">
                                
                        <!-- TOTAL DE LAS VENTAS -->
                        <div class="col-auto">
                            <label for="totalventas"  style="margin-left: 10px;">
                                Total de las ventas:
                                <span style="font-weight: bold; font-size:18px">
                                    <?php
                                        echo bcadd( $consultatotal[0]->totalventas, '0', 2 );
                                    ?>
                                </span>
                            </label>
                        </div>
                                
                        <!-- CANTIDAD TOTAL PRODUCTOS VENDIDOS -->
                        <div class="col-auto">
                            <label for="totalventas"  style="margin-left: 10px;">
                                Cantidad de productos vendidos:
                                <span style="font-weight: bold; font-size:18px">
                                    <?php
                                         
                                         if( !isset( $consultatotalproductosvendidos[0]->cantidadtotalproductosvendidos ) ) {

                                            echo "0";
                                            
                                        } else {

                                            echo $consultatotalproductosvendidos[0]->cantidadtotalproductosvendidos;

                                        }

                                    ?>
                                </span>
                            </label>
                        </div>
                                
                        <!-- TOTAL DE VENTAS REALIZADAS -->
                        <div class="col-auto">
                            <label for="consultatotalventas"  style="margin-left: 10px;">
                                Total de ventas realizadas:
                                <span style="font-weight: bold; font-size:18px">
                                    <?php
                                        echo $consultatotalventas[0]->consultatotalventas;
                                    ?>
                                </span>
                            </label>
                        </div>
                        
                        <!-- FECHA ACTUAL -->
                        <div class="col-auto">
                            <label for="fechaactual"  style="margin-left: 10px;">
                                Fecha:
                                <span id="fechaactual" style="font-weight: bold; font-size:18px">
                                    <?php
                                        echo $fechaactual;
                                    ?>
                                </span>
                            </label>
                        </div>
                        
                    </div>
                    
                    <!-- DIV TOTALVENTAS PAGADAS CREDITO -->
                    <div class="row" style="margin-left: 1%;">
                                
                        <!-- IMPORTE TOTAL DE LAS VENTAS PAGADAS COMPLETAMENTE CON CRÉDITO -->
                        <div class="col-auto">
                            <label for="totalventascreditocompleto"  style="margin-left: 10px;">
                                Importe total de las ventas pagadas completamente con crédito:
                                <span style="font-weight: bold; font-size:18px">
                                    <?php
                                        echo bcadd( $consultatotalventascreditocompleto[0]->totalventascreditocompleto, '0', 2 );
                                    ?>
                                </span>
                            </label>
                        </div>
                                
                        <!-- TOTAL DE LAS VENTAS EFECTIVOS -->
                        <div class="col-auto">
                            <label for="consultatotalventasefectivo"  style="margin-left: 10px;">
                                Importe total de las ventas pagadas en efectivo:
                                <span style="font-weight: bold; font-size:18px">
                                    <?php
                                        echo bcadd( $consultatotalventasefectivo[0]->consultatotalventasefectivo, '0', 2 );
                                    ?>
                                </span>
                            </label>
                        </div>
                                
                        <!-- IMPORTE TOTAL VENTAS PAGADAS CON CRÉDITO - EFECTIVO -->
                        <div class="col-auto">
                            <label for="consultatotalventascreditopartes"  style="margin-left: 10px;">
                                Importe total de las ventas pagadas con crédito en partes (efectivo):
                                <span style="font-weight: bold; font-size:18px">
                                    <?php
                                        echo bcadd( ( $consultatotalventascreditopartes[0]->totalpagadoefectivo  ), '0', 2 );
                                    ?>
                                </span>
                            </label>
                        </div>
                                
                        <!-- IMPORTE TOTAL VENTAS PAGADAS CON CRÉDITO - CREDITO -->
                        <div class="col-auto">
                            <label for="consultatotalventascreditopartes"  style="margin-left: 10px;">
                                Importe total de las ventas pagadas con crédito en partes (crédito):
                                <span style="font-weight: bold; font-size:18px">
                                    <?php
                                        echo bcadd( ( $consultatotalventascreditopartes[0]->total - $consultatotalventascreditopartes[0]->totalpagadoefectivo  ), '0', 2 );
                                    ?>
                                </span>
                            </label>
                        </div>

                        <!-- SUMA IMPORTE TOTAL VENTAS PAGADAS CON CRÉDITO - EFECTIVO MÁS VENTAS PAGADAS EFECTIVO -->
                        <div class="col-auto">
                            <label for="consultatotalventascreditopartes"  style="margin-left: 10px;">
                                Importe total de las ventas pagadas con crédito en partes (efectivo) + Importe total de las ventas pagadas en efectivo:
                                <span style="font-weight: bold; font-size:18px">
                                    <?php
                                        echo bcadd( ( $consultatotalventasefectivo[0]->consultatotalventasefectivo ) + ( $consultatotalventascreditopartes[0]->totalpagadoefectivo ) , '0', 2 );
                                    ?>
                                </span>
                            </label>
                        </div>
                                
                        <!-- TOTAL DESCUENTO -->
                        <div class="col-auto">
                            <label for="consultatotaldescuento"  style="margin-left: 10px;">
                                Total descuento:
                                <span style="font-weight: bold; font-size:18px">
                                    <?php
                                        echo bcadd( $consultatotaldescuento[0]->totaldescuento, '0', 2 );
                                    ?>
                                </span>
                            </label>
                        </div>
                                
                        <!-- TOTAL ABONOS -->
                        <div class="col-auto">
                            <label for="consultatotaldescuento"  style="margin-left: 10px;">
                                Total abonos:
                                <span style="font-weight: bold; font-size:18px">
                                    <?php
                                        echo bcadd( $consultaabonoscreditos[0]->totalabonado, '0', 2 );
                                    ?>
                                </span>
                            </label>
                        </div>
                        
                        <!-- SALDO INICIAL -->
                        <label for="consultasaldoinicial"  style="margin-left: 20px;">
                            Saldo inicial de la caja:
                            <span style="font-weight: bold; font-size:18px">
                                <?php
                                    echo bcadd( $consultasaldoinicial[0]->saldoinicial, '0', 2 );
                                ?>
                            </span>
                        </label>
                        
                        <!-- SALDO ACTUAL -->
                        <label for="consultasaldoinicial"  style="margin-left: 20px;">
                            Saldo actual de la caja:
                            <span style="font-weight: bold; font-size:18px">
                                <?php
                                    echo bcadd( ( $consultasaldoinicial[0]->saldoinicial + ( $consultatotalventasefectivo[0]->consultatotalventasefectivo ) + ( $consultatotalventascreditopartes[0]->totalpagadoefectivo ) + ( $consultaabonoscreditos[0]->totalabonado ) ), '0', 2 );
                                ?>
                            </span>
                        </label>
                        
                    </div>
                    
                </div>
            </div>
                
        </fieldset>
                
    </div>

    <!-- Tabla -->
    <div class="container">
        <div class="row justify-content-center">    
                                
            <div class="scrollbar" id="scrollbar-style">

                <table class="table table-hover" style="width: 1200px;" id="tabladatosabonocredito" name="tabladatosabonocredito">
                    <thead  class="thead-dark">
                        <tr>
                            <th scope="col" style="width: 100px;"> ID venta </th>
                            <th scope="col" style="width: 150px;"> Código producto </th>
                            <th scope="col" style="width: 300px;"> Descripción producto </th>
                            <th scope="col" style="width: 100px;"> Cantidad </th>
                            <th scope="col" style="width: 100px;"> Precio venta </th>
                            <?php
                                if( Auth::user()->Nivel == 1 ) {
                            ?>
                                <th scope="col" style="width: 100px;"> Precio costo </th>
                            <?php
                                }
                            ?>
                            <th scope="col" style="width: 150px;"> Importe </th>
                            <?php
                                if( Auth::user()->Nivel == 1 ) {
                            ?>
                            <th scope="col" style="width: 150px;"> Utilidad </th>
                            <?php
                                }
                            ?>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ( $consultasventasactual as $cva )
                            <tr>
                                <td>{{ $cva->idventa }}</td>
                                <td>{{ $cva->codigoproducto }}</td>
                                <td>{{ $cva->descripcionproducto }}</td>
                                <td>{{ $cva->cantidadventa }}</td>
                                <td> <?php echo bcadd( $cva->precioventaproducto, '0', 2); ?></td>                                
                                <?php
                                    if( Auth::user()->Nivel == 1 ) {
                                ?>
                                <td> <?php echo bcadd( ( $cva->preciocostoproducto ), '0', 2); ?></td>
                                <?php
                                    }
                                ?>
                                <td> <?php echo bcadd( ( $cva->cantidadventa  * $cva->precioventaproducto ), '0', 2); ?></td>
                                <?php                                
                                    if( Auth::user()->Nivel == 1 ) {
                                ?>
                                <td> <?php echo bcadd( ( ( $cva->precioventaproducto  - $cva->preciocostoproducto ) * $cva->cantidadventa ), '0', 2); ?></td>
                                <?php
                                }
                                ?>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

            </div>
        
        </div>
    </div>

    <br>
    <br>
    <br>
    
    <!-- Botón Exportar PDF -->
    <div class="container col-2">
        <div class="card">
            <div class="card-body">                                               
                <form action="{{route('exportarcortecajadiaventas')}}" method="get">
                    <button type="submit" id="botonexportarcortecajadia" class="btn btn-info" style="margin-left: 15%" >Generar PDF</button>
                </form>
            </div>
        </div>
    </div>

@endsection