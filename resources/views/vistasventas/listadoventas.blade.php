@extends('home')

    @section('title')
        Listado de ventas
    @endsection


@section('contenido')

    <style>

        .scrollbar {

            height: 400px;
            overflow: auto;
            overflow-x: hidden;
            overflow-y: scroll;
            white-space:nowrap;
            border-radius: 10px;            

        }

        #scrollbar-style::-webkit-scrollbar {
            
            width: 6px;
            background-color: #F5F5F5;

        }

        /* Se pone un color de fondo y se redondean las esquinas del thumb */
        #scrollbar-style::-webkit-scrollbar-thumb {

            
            border-radius: 4px;
            /*background-color: rgb(84, 81, 72);*/
            background-color: rgb(49, 163, 204);

        }

        /* Se cambia el fondo y se agrega una sombra cuando esté en hover */
        #scrollbar-style::-webkit-scrollbar-thumb:hover {

            background: #b3b3b3;
            box-shadow: 0 0 2px 1px rgba(0, 0, 0, 0.2);

        }

        /* Se cambia el fondo cuando esté en active */
        #scrollbar-style::-webkit-scrollbar-thumb:active {
            
            background-color: #999999;

        }
        /* Se pone un color de fondo y se redondean las esquinas del track */
        #scrollbar-style::-webkit-scrollbar-track {

            background: #e1e1e1;
            border-radius: 4px;

        }

        /* Se cambia el fondo cuando esté en active o hover */
        #scrollbar-style::-webkit-scrollbar-track:hover,
        #scrollbar-style::-webkit-scrollbar-track:active {

            background: #d4d4d4;

        }

        .textotooltip {
                        
            width: 800px;
            background-color:rgba(0, 0, 0, 0.78);
            color: #fff;
            text-align: center;
            border-radius: 6px;
            padding: 5px 0;

        }

    </style>

    <script>

        const FechaActual   = new Date(new Date().getTime() - new Date().getTimezoneOffset() * 60000).toISOString().split("T")[0];
        const FechaMinima   = new Date('2020-01-10').toISOString().split("T")[0];
        const MesMinimo     = new Date().getFullYear() + "-0" + ( new Date().getMonth() + 1 );
                    
        const arreglomeses = new Array();
                    arreglomeses[0]  = "Enero";
                    arreglomeses[1]  = "Febrero";
                    arreglomeses[2]  = "Marzo";
                    arreglomeses[3]  = "Abril";
                    arreglomeses[4]  = "Mayo";
                    arreglomeses[5]  = "Junio";
                    arreglomeses[6]  = "Julio";
                    arreglomeses[7]  = "Agosto";
                    arreglomeses[8]  = "Septiembre";
                    arreglomeses[9]  = "Octubre";
                    arreglomeses[10] = "Noviembre";
                    arreglomeses[11] = "Diciembre";
                    arreglomeses[12] = "Enero";

        window.onload = function( e ) {

            document.getElementById('fechafiltrado').max = FechaActual;
            document.getElementById('fechafiltrado').min = FechaMinima;
            
            document.getElementById('fechatipoperiodosemanal').max = FechaActual;
            document.getElementById('fechatipoperiodosemanal').min = FechaMinima;
            
            document.getElementById('fechatipoperiodomensual').max = MesMinimo;
            document.getElementById('fechatipoperiodomensual').min = "2020-01";
            
            document.getElementById('fechaperiodoinicial').max = FechaActual;
            document.getElementById('fechaperiodoinicial').min = FechaMinima;

            document.getElementById('fechaperiodofinal').max = FechaActual;  
            document.getElementById('fechaperiodofinal').min = FechaMinima;

        }

        function CambiarLimitesTipoPeriodoPeriodo( t ) {

            if( t.id == "fechaperiodoinicial" ) {

                var fechainicialminima = new Date( document.getElementById("fechaperiodoinicial").value );
                fechainicialminima.setDate( fechainicialminima.getDate() + 1 );
                fechainicialminima = new Date( fechainicialminima ).toISOString().split("T")[0];
                document.getElementById('fechaperiodofinal').min = fechainicialminima;

            } else if( t.id = "fechaperiodofinal"  ) {

                var fechainicialmaxima = new Date( document.getElementById("fechaperiodofinal").value );
                fechainicialmaxima.setDate( fechainicialmaxima.getDate() - 1 );
                fechainicialmaxima = new Date( fechainicialmaxima ).toISOString().split("T")[0];
                document.getElementById('fechaperiodoinicial').max = fechainicialmaxima;

            }
        }

        function MostrarFiltradoFechaEspecifica() {
                        
            document.getElementById("fechafiltrado").value = "";

            if( document.getElementById("tipofechaespecifica").checked ) {

                document.getElementById("divfechaespecificafiltrado").style.display = "block";
                document.getElementById("divfechaperiodo").style.display = "none";
                $("#tipoperiodo").val("0");

                document.getElementById("divtipoperiodo").style.display = "none";

            }

        }

        function MostrarFiltradoPeriodo() {

            if( document.getElementById("tipofechaperiodo").checked ) {

                document.getElementById("divfechaperiodo").style.display = "block";
                document.getElementById("divfechaespecificafiltrado").style.display = "none";
                $("#tipoperiodo").val("0");

            }

        }

        function MostrarTiposPeriodo() {

            document.getElementById("fechafiltrado").value = "";
                        
            document.getElementById("divtipoperiodosemanal").style.display = "none";
            document.getElementById("fechatipoperiodosemanal").value = "";

            document.getElementById("divtipoperiodomensual").style.display = "none";
            document.getElementById("fechatipoperiodomensual").value = "";

            document.getElementById("divtipoperiodoperiodo").style.display = "none";
            document.getElementById("fechaperiodoinicial").value = "";
            document.getElementById("fechaperiodofinal").value = "";

            document.getElementById("divtipoperiodo").style.display = "block";

            var selecttipoperiodo = $("#tipoperiodo").val();
                                    
            if( selecttipoperiodo == 2 ) { //SEMANAL

                document.getElementById("divtipoperiodosemanal").style.display = "block";
                document.getElementById("divtipoperiodomensual").style.display = "none";
                document.getElementById("divtipoperiodoperiodo").style.display = "none";

            } else if( selecttipoperiodo == 3 ) { //MENSUAL

                document.getElementById("divtipoperiodosemanal").style.display = "none";
                document.getElementById("divtipoperiodomensual").style.display = "block";
                document.getElementById("divtipoperiodoperiodo").style.display = "none";

            } else if( selecttipoperiodo == 4 ) { //PERIODO

                document.getElementById("divtipoperiodosemanal").style.display = "none";
                document.getElementById("divtipoperiodomensual").style.display = "none";
                document.getElementById("divtipoperiodoperiodo").style.display = "block";

            }

        }

        function FiltrarTabla() {

            if( document.getElementById("tipofechaespecifica").checked ) {

                if( document.getElementById("fechafiltrado").value != "" ) {

                    if(  document.getElementById("fechafiltrado").value >= FechaMinima && document.getElementById("fechafiltrado").value <= FechaActual ) {
                        
                        RealizarFiltradoTabla( 1, document.getElementById("fechafiltrado").value,  document.getElementById("fechafiltrado").value );

                    } else if(  document.getElementById("fechafiltrado").value >= FechaMinima && document.getElementById("fechafiltrado").value >= FechaActual ) {

                        document.getElementById("textoalerta").value = "La fecha ingresada debe ser menor a la fecha actual";
                        document.getElementById("contentalert").style.display = "block";

                    } else if(  document.getElementById("fechafiltrado").value <= FechaMinima && document.getElementById("fechafiltrado").value <= FechaActual ) {

                        document.getElementById("textoalerta").value = "La fecha ingresada debe ser mayor a 01/01/2020";
                        document.getElementById("contentalert").style.display = "block";

                    }

                } else {
                        
                    document.getElementById("textoalerta").value = "Por favor seleccione la fecha";
                    document.getElementById("contentalert").style.display = "block";

                }

            } else if( document.getElementById("tipofechaperiodo").checked ) {

                var selecttipoperiodo = $("#tipoperiodo").val();

                if( selecttipoperiodo == 1 ) {
                    
                    var fecha = new Date().toISOString().split("T")[0];
                    RealizarFiltradoTabla( 10 , fecha, fecha );

                } else if( selecttipoperiodo == 2 ) {
                    
                    if( document.getElementById("fechatipoperiodosemanal").value != "" ) {
                        
                        RealizarFiltradoTabla( 2, document.getElementById("fechatipoperiodosemanal").value,  document.getElementById("fechatipoperiodosemanal").value );

                    } else if( document.getElementById("fechatipoperiodosemanal").value == "" ) {
                        
                        document.getElementById("textoalerta").value = "Por favor seleccione la semana";
                        document.getElementById("contentalert").style.display = "block";

                    }
                    
                } else if( selecttipoperiodo == 3 ) {

                    if( document.getElementById("fechatipoperiodomensual").value != "") {
                        
                        RealizarFiltradoTabla( 3, document.getElementById("fechatipoperiodomensual").value,  document.getElementById("fechatipoperiodomensual").value );

                    } else if( document.getElementById("fechatipoperiodomensual").value == "" ) {
                        
                        document.getElementById("textoalerta").value = "Por favor seleccione el mes";
                        document.getElementById("contentalert").style.display = "block";

                    }
                
                } else if( selecttipoperiodo == 4 ) {

                    if( document.getElementById("fechaperiodoinicial").value != "" && document.getElementById("fechaperiodofinal").value != "" ) {
                        
                        var fechainicial = new Date( document.getElementById("fechaperiodoinicial").value ).toISOString().split("T")[0];
                        var fechafinal = new Date( document.getElementById("fechaperiodofinal").value ).toISOString().split("T")[0];

                        if( fechainicial >= fechafinal ) {

                            document.getElementById("textoalerta").value = "La fecha inicial debe ser mayor a la fecha final";
                            document.getElementById("contentalert").style.display = "block";

                        } else {

                            RealizarFiltradoTabla( 4, document.getElementById("fechaperiodoinicial").value,  document.getElementById("fechaperiodofinal").value );

                        }                    

                    } else if( document.getElementById("fechaperiodoinicial").value == "" && document.getElementById("fechaperiodofinal").value != "" ) {
                        
                        document.getElementById("textoalerta").value = "Por favor seleccione la fecha inicial del periodo";
                        document.getElementById("contentalert").style.display = "block";

                    } else if( document.getElementById("fechaperiodoinicial").value != "" && document.getElementById("fechaperiodofinal").value == "" ) {
                                                
                        document.getElementById("textoalerta").value = "Por favor seleccione la fecha final del periodo";
                        document.getElementById("contentalert").style.display = "block";

                    } else if( document.getElementById("fechaperiodoinicial").value == "" && document.getElementById("fechaperiodofinal").value == "" ) {
                                            
                        document.getElementById("textoalerta").value = "Por favor seleccione la fecha inicial y final del periodo";
                        document.getElementById("contentalert").style.display = "block";

                    }

                } else if( selecttipoperiodo == 0 ) {
                                            
                    document.getElementById("textoalerta").value = "Por favor seleccione el tipo de periodo";
                    document.getElementById("contentalert").style.display = "block";

                }

            } else {
                        
                document.getElementById("textoalerta").value = "No ha seleccionado una opción de filtrado";
                document.getElementById("contentalert").style.display = "block";

            }
        }

        function RealizarFiltradoTabla( caso , fecha, fechafinal ) {

            // Caso => 1 -> IGUAL / FECHA ESPECIFICA
            // Caso => 2 -> MAYOR IGUAL / PERIODO SEMANA
            // Caso => 3 -> IGUAL / PERIODO MES
            // Caso => 4 -> MENOR IGUAL, MAYOR IGUAL / PERIODO LAPSO
            // Caso => 5 -> IGUAL / FECHA ACTUAL

            var fechacaso = new Date( fecha ).toISOString().split("T")[0];
            var auxfechaventa = FechaActual;

            var arraylistadoventas = {!! json_encode( $listadoventas->toArray(), JSON_HEX_TAG ) !!};
            VaciarTabla();

            var totalventasfiltradas = 0;

            if( caso == 1 ) {

                for( var i = 0; i < arraylistadoventas.length; i++ ) {

                    auxfechaventa = new Date( arraylistadoventas[i].fechaventa ).toISOString().split("T")[0];                   

                    if( auxfechaventa == fecha ) {

                        var tipopago = "";

                        if( arraylistadoventas[i].tipopago == "1" ) {
                            tipopago = "Contado";
                        } else if( arraylistadoventas[i].tipopago == "2" ) {
                            tipopago = "Crédito";
                        } else if( arraylistadoventas[i].tipopago == "3" ) {                            
                            tipopago = "Contado y crédito";
                        }

                        var auxtotalventa = ( Math.round( arraylistadoventas[i].totalventa * 100 ) / 100 ).toFixed( 2 );

                        document.getElementById("tablaventas").insertRow(-1).innerHTML = 
                        "<td>" + arraylistadoventas[i].idventa + "</td>" +
                        "<td>" + arraylistadoventas[i].nombrecompletoempleado + "</td>" +
                        "<td>" + arraylistadoventas[i].nombrecompletocliente + "</td>" +
                        "<td>" + auxfechaventa + "</td>" +
                        "<td>" + auxtotalventa + "</td>" +                        
                        "<td>" + tipopago + "</td>";

                        totalventasfiltradas += Number.parseFloat(auxtotalventa );

                    }
                
                }

            } else if( caso == 2 ) {

                //Fecha limite de la semana
                var fechacasolimite = new Date( fecha );                
                fechacasolimite.setDate( fechacasolimite.getDate() + 7 );
                fechacasolimite = new Date( fechacasolimite ).toISOString().split("T")[0];
                
                for( var i = 0; i < arraylistadoventas.length; i++ ) {

                    auxfechaventa = new Date( arraylistadoventas[i].fechaventa ).toISOString().split("T")[0];

                    if( auxfechaventa >= fechacaso && auxfechaventa <= fechacasolimite ) {

                        var tipopago = "";

                        if( arraylistadoventas[i].tipopago == "1" ) {
                            tipopago = "Contado";
                        } else if( arraylistadoventas[i].tipopago == "2" ) {
                            tipopago = "Crédito";
                        } else if( arraylistadoventas[i].tipopago == "3" ) {                            
                            tipopago = "Contado y crédito";
                        }

                        var auxtotalventa = ( Math.round( arraylistadoventas[i].totalventa * 100 ) / 100 ).toFixed( 2 );

                        document.getElementById("tablaventas").insertRow(-1).innerHTML = 
                        "<td>" + arraylistadoventas[i].idventa + "</td>" +
                        "<td>" + arraylistadoventas[i].nombrecompletoempleado + "</td>" +
                        "<td>" + arraylistadoventas[i].nombrecompletocliente + "</td>" +
                        "<td>" + auxfechaventa + "</td>" +
                        "<td>" + auxtotalventa + "</td>" +                        
                        "<td>" + tipopago + "</td>";

                        totalventasfiltradas += Number.parseFloat(auxtotalventa );

                    }

                }

            } else if( caso == 3 ) {
                
                fechacaso = new Date( fecha );
                var mesfechacaso = fechacaso.getMonth();
                var annofechacaso = fechacaso.getFullYear();

                for( var i = 0; i < arraylistadoventas.length; i++ ) {

                    auxfechaventa = new Date( arraylistadoventas[i].fechaventa );
                    
                    mesfechacaso = arreglomeses[ ( fechacaso.getMonth() + 1 ) ];
                    mesauxfechaventa = arreglomeses[ auxfechaventa.getMonth() ];

                    //mesauxfechaventa = auxfechaventa.getMonth();
                    annoauxfechaventa = auxfechaventa.getFullYear();

                    auxfechaventa = new Date( arraylistadoventas[i].fechaventa ).toISOString().split("T")[0];

                    if( mesfechacaso == mesauxfechaventa && annofechacaso == annoauxfechaventa ) {

                        var tipopago = "";

                        if( arraylistadoventas[i].tipopago == "1" ) {
                            tipopago = "Contado";
                        } else if( arraylistadoventas[i].tipopago == "2" ) {
                            tipopago = "Crédito";
                        } else if( arraylistadoventas[i].tipopago == "3" ) {                            
                            tipopago = "Contado y crédito";
                        }

                        var auxtotalventa = ( Math.round( arraylistadoventas[i].totalventa * 100 ) / 100 ).toFixed( 2 );

                        document.getElementById("tablaventas").insertRow(-1).innerHTML = 
                        "<td>" + arraylistadoventas[i].idventa + "</td>" +
                        "<td>" + arraylistadoventas[i].nombrecompletoempleado + "</td>" +
                        "<td>" + arraylistadoventas[i].nombrecompletocliente + "</td>" +
                        "<td>" + auxfechaventa + "</td>" +
                        "<td>" + auxtotalventa + "</td>" +                        
                        "<td>" + tipopago + "</td>";

                        totalventasfiltradas += Number.parseFloat(auxtotalventa );

                    }

                }
                                    

            } else if( caso == 4 ) {
                
                var fechacasolimite = new Date( fechafinal ).toISOString().split("T")[0];                

                for( var i = 0; i < arraylistadoventas.length; i++ ) {
                    
                    auxfechaventa = new Date( arraylistadoventas[i].fechaventa ).toISOString().split("T")[0];
                    
                    if( auxfechaventa >= fechacaso && auxfechaventa <= fechacasolimite ) {

                        var tipopago = "";

                        if( arraylistadoventas[i].tipopago == "1" ) {
                            tipopago = "Contado";
                        } else if( arraylistadoventas[i].tipopago == "2" ) {
                            tipopago = "Crédito";
                        } else if( arraylistadoventas[i].tipopago == "3" ) {                            
                            tipopago = "Contado y crédito";
                        }

                        var auxtotalventa = ( Math.round( arraylistadoventas[i].totalventa * 100 ) / 100 ).toFixed( 2 );

                        document.getElementById("tablaventas").insertRow(-1).innerHTML = 
                        "<td>" + arraylistadoventas[i].idventa + "</td>" +
                        "<td>" + arraylistadoventas[i].nombrecompletoempleado + "</td>" +
                        "<td>" + arraylistadoventas[i].nombrecompletocliente + "</td>" +
                        "<td>" + auxfechaventa + "</td>" +
                        "<td>" + auxtotalventa + "</td>" +                        
                        "<td>" + tipopago + "</td>";

                        totalventasfiltradas += Number.parseFloat(auxtotalventa );

                    }

                }

            } else if( caso == 10 ) {

                for( var i = 0; i < arraylistadoventas.length; i++ ) {

                    fecha = new Date( arraylistadoventas[i].fechaventa ).toISOString().split("T")[0];;                    

                    if( auxfechaventa == fecha ) {

                        var tipopago = "";

                        if( arraylistadoventas[i].tipopago == "1" ) {
                            tipopago = "Contado";
                        } else if( arraylistadoventas[i].tipopago == "2" ) {
                            tipopago = "Crédito";
                        } else if( arraylistadoventas[i].tipopago == "3" ) {                            
                            tipopago = "Contado y crédito";
                        }

                        var auxtotalventa = ( Math.round( arraylistadoventas[i].totalventa * 100 ) / 100 ).toFixed( 2 );

                        document.getElementById("tablaventas").insertRow(-1).innerHTML = 
                        "<td>" + arraylistadoventas[i].idventa + "</td>" +
                        "<td>" + arraylistadoventas[i].nombrecompletoempleado + "</td>" +
                        "<td>" + arraylistadoventas[i].nombrecompletocliente + "</td>" +
                        "<td>" + auxfechaventa + "</td>" +
                        "<td>" + auxtotalventa + "</td>" +                        
                        "<td>" + tipopago + "</td>";

                        totalventasfiltradas += Number.parseFloat(auxtotalventa );

                    }
                
                }

            }

            const tabla = document.getElementById("tablaventas");
            var rowCount = ( tabla.rows.length - 1 );
            
            if( rowCount == 0 ) {
                document.getElementById("textoresultadobusqueda").innerHTML = "<p>No hay resultados que coincidan con la búsqueda.</p>"
            } else if( rowCount >= 1 ) {                
                document.getElementById("textoresultadobusqueda").innerHTML = "<p>Resultados coincidentes con la búsqueda: <strong>" + rowCount + "</strong>.</p>"                
                
                totalventasfiltradas = ( Math.round( totalventasfiltradas * 100 ) / 100 ).toFixed( 2 );
                document.getElementById("textoresultadobusquedatotal").innerHTML = "<p>Total de las ventas $<strong>" + totalventasfiltradas + "</strong></p>";

            }


        }

        function VaciarTabla() {

            const tabla = document.getElementById('tablaventas');
            var rowCount = tabla.rows.length;         

            for  ( var x = rowCount - 1; x > 0; x-- ) { 
                tabla.deleteRow(x); 
            } 

        }

        function RestablecerTabla() {
        
            document.getElementById("textoresultadobusqueda").innerHTML = "";
            document.getElementById("textoresultadobusquedatotal").innerHTML = "";
            var arraylistadoventas = {!! json_encode( $listadoventas->toArray(), JSON_HEX_TAG ) !!};
            
            VaciarTabla();

            for( var i = 0; i < arraylistadoventas.length; i++ ) {

                var tipopago = ""; 

                if( arraylistadoventas[i].tipopago == "1" ) {
                    tipopago = "Contado";
                } else if( arraylistadoventas[i].tipopago == "2" ) {
                    tipopago = "Crédito";
                } else if( arraylistadoventas[i].tipopago == "3" ) {                            
                    tipopago = "Contado y crédito";
                }
                                            
                var auxtotalventa = ( Math.round( arraylistadoventas[i].totalventa * 100 ) / 100 ).toFixed( 2 );

                document.getElementById("tablaventas").insertRow(-1).innerHTML = 
                "<td>" + arraylistadoventas[i].idventa + "</td>" +
                "<td>" + arraylistadoventas[i].nombrecompletoempleado + "</td>" +
                "<td>" + arraylistadoventas[i].nombrecompletocliente + "</td>" +
                "<td>" + arraylistadoventas[i].fechaventa + "</td>" +
                "<td>" + auxtotalventa + "</td>" +                        
                "<td>" + tipopago + "</td>";
            
            }

            document.getElementById("tipofechaespecifica").checked = false;
            document.getElementById("tipofechaperiodo").checked = false;
            document.getElementById("divfechaespecificafiltrado").style.display = "none";
            document.getElementById("divfechaperiodo").style.display = "none";

            document.getElementById("fechafiltrado").value = "";
                        
            document.getElementById("divtipoperiodosemanal").style.display = "none";
            document.getElementById("fechatipoperiodosemanal").value = "";

            document.getElementById("divtipoperiodomensual").style.display = "none";
            document.getElementById("fechatipoperiodomensual").value = "";

            document.getElementById("divtipoperiodoperiodo").style.display = "none";
            document.getElementById("fechaperiodoinicial").value = "";
            document.getElementById("fechaperiodofinal").value = "";

            $("#tipoperiodo").val("0");

        }

        function FilaSeleccionada() {
                    
            var table = document.getElementById('tablaventas'),
            selected = table.getElementsByClassName('selected');
            
            if (selected[0]) selected[0].className = '';
            event.target.parentNode.className = 'selected';

            /*
            console.log("CODIGO SELECCIONADO: ", $("tr.selected td:first" ).html() );
            console.log("NOMBRE EMPLEADO SELECCIONADO: ", $("tr.selected td:nth-child(2)").html() );
            console.log("NOMBRE CLIENTE SELECCIONADO: ", $("tr.selected td:nth-child(3)").html() );
            console.log("FECHA SELECCIONADO: ",$("tr.selected td:nth-child(4)").html() );
            console.log("TOTAL SELECCIONADO: ", $("tr.selected td:nth-child(5)").html() );
            console.log("TIPO PAGO SELECCIONADO: ", $("tr.selected td:nth-child(6)").html() );
            */
                        
            var idventa = $("tr.selected td:first" ).html();
                
            location.href ="/informaciondetalleventa/" + idventa;

        }

        function OcultarAlerta() {

            if( document.getElementById("contentalert").style.display == "none" ) {
                document.getElementById("contentalert").style.display = "block";
            } else {
                document.getElementById("contentalert").style.display = "none";
            }

        }

    </script>
        
    @if(Session::has('success'))
        <div class="alert alert-success">
            {{session('success')}}
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        </div>
    @elseif( Session::has('warning'))
        <div class="alert alert-warning">
            {{session('warning')}}
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        </div>
    @elseif( Session::has('danger'))
        <div class="alert alert-danger">
            {{session('danger')}}
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        </div>
    @endif

    <!-- DIV FILTRADO DE LISTADO-->
    <div class="container" style="margin-bottom: 3px;">
        
        <!-- DIV FECHA ESPECIFICA -->
        <div class="form-row align-items-center">
            
            <!-- RADIO FECHA ESPECIFICA -->
            <div class="col-auto" style="margin-right: 10px;">
                <input type="radio" name="tipofiltrado" id="tipofechaespecifica" onclick="javascript:MostrarFiltradoFechaEspecifica();">
                <label for="tipofechaespecifica" onclick="javascript:MostrarFiltradoFechaEspecifica();"> Filtrar por fecha específica </label>
            </div>

            <div class="col-xs-3" id="divfechaespecificafiltrado" style="display: none;">
                <label for="fechafiltrado"  style="margin-left: 20px;"> Seleccione la fecha para filtrar </label>              
                <input type="date" class="form-control-sm" id="fechafiltrado" style="margin-left: 10px;">
            </div>
            
        </div>
        
        <!-- DIV FECHA PERIODO -->
        <div class="form-row align-items-center" style="margin-top: 10px;">

            <!-- RADIO PERIODO -->
            <div class="col-auto" style="margin-right: 10px;">
                <input type="radio" name="tipofiltrado" id="tipofechaperiodo" onclick="javascript:MostrarFiltradoPeriodo();">
                <label for="tipofechaperiodo" onclick="javascript:MostrarFiltradoPeriodo();"> Filtrado por período </label>
            </div>

            <!-- SELECT FECHA PERIODO-->
            <div id="divfechaperiodo" style="display: none;">
                <label for="tipoperiodo" style="margin-left: 10px;"></label>
                <select id="tipoperiodo" class="form-control-sm" style="margin-right: 10px;" onchange="javascript:MostrarTiposPeriodo();">
                    <option value="0">Seleccione una opción</option>
                    <option value="1">Fecha Actual</option>
                    <option value="2">Semanal</option>
                    <option value="3">Mensual</option>
                    <option value="4">Periodo</option>
                </select>
            </div>

            <!-- TIPOS PERIODO -->
            <div id="divtipoperiodo">

                <!-- SEMANA PERIODO -->
                <div id="divtipoperiodosemanal" style="display: none;">
                    <label for="fechatipoperiodosemanal" style="margin-right: 3px;">Seleccione la semana</label>
                    <!-- <input type="week" id="fechatipoperiodosemanal"> -->
                    <input type="date" class="form-control-sm" id="fechatipoperiodosemanal">
                </div>

                <!-- MES PERIODO -->
                <div id="divtipoperiodomensual" style="display: none;">
                    <label for="fechatipoperiodomensual" style="margin-right: 3px;">Seleccione el mes</label>
                    <input type="month" class="form-control-sm" id="fechatipoperiodomensual">
                </div>
                
                <!-- LAPSO PERIODO -->
                <div id="divtipoperiodoperiodo" style="display: none;">
                    <label for="fechaperiodoinicial" style="margin-right: 5px;">Seleccione la fecha inicial</label>
                    <input type="date" class="form-control-sm" id="fechaperiodoinicial" onchange="javascript:CambiarLimitesTipoPeriodoPeriodo(this);">

                    <label for="fechaperiodofinal" style="margin-left: 5px;">Seleccione la fecha final</label>
                    <input type="date" class="form-control-sm" id="fechaperiodofinal" style="margin-left: 5px;" onchange="javascript:CambiarLimitesTipoPeriodoPeriodo(this);">
                </div>

            </div>                

        </div>

        <!-- Botones -->
        <div class="form-row align-items-center" style="margin-top: 2px; margin-bottom: 5px;">

            <a style="margin-left: 35%;"> <button  class="btn btn-primary" id="botonfiltrado"    onclick="javascript:FiltrarTabla();">Filtrar Tabla</button> </a>
            <a style="margin-left: 5%;">  <button  class="btn btn-primary" id="botonrestablecer" onclick="javascript:RestablecerTabla();">Restablecer Tabla</button> </a>

            <label id="textoresultadobusqueda" style="margin-top: 10px; margin-left:20px;"></label>
            <label id="textoresultadobusquedatotal" style="margin-top: 10px; margin-left:40%;"></label>
        </div>
        
        
    </div>

        <!-- Alerta -->
    <div class="container"  id="contentalert" style="display: none;">
        <div class="alert alert-danger alert-dismissible" role="alert" style="margin-top: 1%" >
            <input class="form-control-plaintext" value = "" id="textoalerta" disabled>    
            <button type="button" class="close" onclick="javascript:OcultarAlerta();" style="margin-top: 5px">
            <span>&times;</span>
            </button>
        </div>
    </div>
    
        <!-- TOOLTIPTEXT -->
    <div class="container" style="margin-top: 10px;">
        
        <div class="row justify-content-center">
            <label class="textotooltip">
                HAGA DOBLE CLICK SOBRE UNA VENTA PARA VER SU INFORMACIÓN
            </label>
        </div>
    </div>

    <!-- Tabla -->
    <div class="container" style="margin-top: 10px;">
        
        <div class="row justify-content-center">
                                
            <div class="scrollbar" id="scrollbar-style">

                <table class="table table-hover" style="width: 1000px" id="tablaventas" name="tablaventas" ondblclick="javascript:FilaSeleccionada();">
                    <thead  class="thead-dark">
                        <tr>                     
                            <th scope="col"> ID </th>   
                            <th scope="col"> Nombre del empleado </th>
                            <th scope="col"> Nombre del cliente  </th>
                            <th scope="col"> Fecha de la venta </th>
                            <th scope="col"> Total de la venta </th>
                            <th scope="col"> Tipo de pago </th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ( $listadoventas as $ventas )
                            <tr>                     
                                <td>{{ $ventas->idventa }}</td>       
                                <td>{{ $ventas-> nombrecompletoempleado }}</td>
                                <td>{{ $ventas-> nombrecompletocliente }}</td>
                                <td>{{ $ventas-> fechaventa }}</td>
                                <td> <?php echo bcadd(  $ventas->totalventa ,'0',2); ?></td>                            
                                <td>
                                    @if( $ventas->tipopago == 1 )
                                        {{ "Contado" }}
                                    @elseif( $ventas->tipopago == 2 )
                                        {{ "Crédito" }}
                                    @elseif( $ventas->tipopago == 3 )
                                        {{ "Contado y crédito" }}
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

            </div>
        
        </div>
    </div>

    <br><br><br>

@endsection