@extends('home')

@section('title')
    Búsqueda de venta
@endsection

@section('contenido')

    @if(Session::has('success'))
        <div class="alert alert-success">
            {{session('success')}}
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        </div>
    @elseif( Session::has('warning'))
        <div class="alert alert-warning">
            {{session('warning')}}
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        </div>
    @elseif( Session::has('danger'))
        <div class="alert alert-danger">
            {{session('danger')}}
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        </div>
    @endif

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header"  align="center">{{ __('Búsqueda de venta') }}</div>

                    <div class="card-body">

                    {!!Form::open(array('name' => 'formbuscarproducto', 'url'=>'/realizarbusquedaventa/','method'=>'POST' ,'autocomplete'=>'off'))!!}

                            <!-- Buscar venta por id -->
                            <div class="form-row"  style="margin-bottom: 10px">
                                <div class="col-7"   style="margin-bottom: 10px">
                                    {!! Form::label( 'codigo','Introduzca el número de la venta', array( 'class' => 'col-md-8 col-form-label' ) ) !!}
                                </div>                                
                                <div class="col-auto">
                                    <input type="text" name="idventa" id="idventa" class="form-control" style="margin-top:10px" />
                                </div>
                            </div>                     

                            <div class="form-group row" style="margin-top: 20px">
                                <div class="col-md-4 col-form-label text-md-right">
                                    <button name="botonbuscarventa" value="Buscar" type="submit" class="btn btn-success" style="margin-right:50px">Buscar</button>
                                </div>
                                <div class="col-md-4 col-form-label text-md-right">
                                    <button name="botonbuscarventa" value="Cancelar" type="submit" class="btn btn-danger" style="margin-left:250px">Cancelar</button>
                                </div>
                            </div>

                        {!!Form::close()!!}
                        
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
