@extends('home')

@section('title')
    Listado de movimientos de inventario
@endsection

@section('contenido')
    

    <script>
        
        var FechaMinimaFinal = new Date().setDate( new Date().getDate() + 1 );
        FechaMinimaFinal = new Date( FechaMinimaFinal ).toISOString().split("T")[0];

        window.onload = function( e ) {

            document.getElementById('fechainicial').min = "2020-01-01";
            document.getElementById('fechainicial').max = FechaMinimaFinal;

            document.getElementById('fechafinal').min = "2020-01-01";;
            document.getElementById('fechafinal').max = FechaMinimaFinal;

        }

        function mostrarradios(){
            
            if( document.getElementById("radiocodigo").checked ) {

                document.getElementById("buscarcodigo").style.display = "block";
                document.getElementById("buscarfecha").style.display = "none";
                document.getElementById("codigoproducto").required = "true";
            

                document.getElementById("buscarfechaespecifica").style.display = "none";
                
                document.getElementById("divfechainicial").style.display = "none";
                document.getElementById("fechainicial").style.display = "none";
                $("#fechafinal").removeAttr("required");
                
                document.getElementById("divfechafinal").style.display = "none";
                document.getElementById("fechafinal").style.display = "none";
                $("#fechafinal").removeAttr("required");
            
                cambiartextolabel("Codigo");
            

            } else if( document.getElementById("radioperiodo").checked ) {

                document.getElementById("buscarfecha").style.display = "block";
                document.getElementById("buscarcodigo").style.display = "none";
                $('#codigoproducto').removeAttr("required");

                $("#botonformmovimientos").removeAttr("name");
                $("#botonformmovimientos").attr("name","botonformmovimientosperiodo");

            }

        }

        function mostrarinputfechas( comp ) {

            let id = comp.id;

            if( id == "fechaespecifica" ) {

                document.getElementById("buscarfechaespecifica").style.display = "block";
                
                document.getElementById("divfechainicial").style.display = "block";                
                document.getElementById("labelfechainicial").innerHTML = "Seleccione la fecha";
                $("#fechainicial").prop("required", true );

                var today = new Date().toISOString().split( 'T' )[0];

                document.getElementById("fechainicial").max = today;
                document.getElementById("fechainicial").style.display = "block";
                
                document.getElementById("divfechafinal").style.display = "none";
                $("#fechafinal").removeAttr("required");
                document.getElementById("fechafinal").style.display = "none";

                cambiartextolabel("FechaEspecifica");

            } else if( id == "periodoespecifico") {

                document.getElementById("buscarfechaespecifica").style.display = "block";
                
                document.getElementById("divfechainicial").style.display = "block";
                document.getElementById("fechainicial").style.display = "block"; 
                document.getElementById("labelfechainicial").innerHTML = "Seleccione la fecha inicial";
                $("#fechainicial").prop("required", true );

                
                document.getElementById("divfechafinal").style.display = "block";
                document.getElementById("fechafinal").style.display = "block";
                $("#fechafinal").prop("required", true );

                var today = new Date().toISOString().split( 'T' )[0];
                document.getElementById("fechafinal").max = today;

                cambiartextolabel("PeriodoEspecifico");

            }

        }

        function cambiartextolabel( texto ) {
            document.getElementById("labelbotonformmovimientos").value = texto;
        }

        function cambiartextolabel2( texto ) {
            document.getElementById("labelbotonformmovimientos").value = texto;
            FiltrarTabla();
        }

        function FiltrarTabla() {
        
            var arrayregistromovimientos = {!! json_encode( $registromovimientos->toArray(), JSON_HEX_TAG ) !!};

            const tabla = document.getElementById('tablamovimientos');
            var rowCount = tabla.rows.length;         

            for  ( var x = rowCount - 1; x > 0; x-- ) { 
                tabla.deleteRow(x); 
            } 

            for( var i = 0; i < arrayregistromovimientos.length; i++ ) {

                var PrecioCostoAnt = "0.00";
                var PrecioCostoAct = "0.00";
                var PrecioVentaAnt = "0.00";
                var PrecioVentaAct = "0.00";

                if( arrayregistromovimientos[i].PrecioCostoAnt != null ) {
                    PrecioCostoAnt = arrayregistromovimientos[i].PrecioCostoAnt;
                }

                if( arrayregistromovimientos[i].PrecioCostoAct != null ) {
                    PrecioCostoAct = arrayregistromovimientos[i].PrecioCostoAct;
                }

                if( arrayregistromovimientos[i].PrecioVentaAnt != null ) {
                    PrecioVentaAnt = arrayregistromovimientos[i].PrecioVentaAnt;
                }

                if( arrayregistromovimientos[i].PrecioVentaAct != null ) {
                    PrecioVentaAct = arrayregistromovimientos[i].PrecioVentaAct;
                }

                
                document.getElementById("tablamovimientos").insertRow(-1).innerHTML = 
                    "<td>" + arrayregistromovimientos[i].idMovimientoInventario + "</td>" +
                    "<td>" + arrayregistromovimientos[i].Descripcion + "</td>" +
                    "<td>" + arrayregistromovimientos[i].Codigo + "</td>" +
                    "<td>" + arrayregistromovimientos[i].FechaHora + "</td>" +
                    "<td>" + arrayregistromovimientos[i].CantidadAnt + "</td>" +
                    "<td>" + arrayregistromovimientos[i].CantidadAct + "</td>" +
                    "<td>" + arrayregistromovimientos[i].CantidadMov + "</td>";
                
                /*
                document.getElementById("tablamovimientos").insertRow(-1).innerHTML = 
                    "<td>" + arrayregistromovimientos[i].idMovimientoInventario + "</td>" +
                    "<td>" + arrayregistromovimientos[i].Descripcion + "</td>" +
                    "<td>" + arrayregistromovimientos[i].Codigo + "</td>" +
                    "<td>" + arrayregistromovimientos[i].FechaHora + "</td>" +
                    "<td>" + arrayregistromovimientos[i].CantidadAnt + "</td>" +
                    "<td>" + arrayregistromovimientos[i].CantidadAct + "</td>" +
                    "<td>" + arrayregistromovimientos[i].CantidadMov + "</td>" +
                    "<td>" + ( Math.round( PrecioCostoAnt * 100 ) / 100 ).toFixed( 2 ) + "</td>" +
                    "<td>" + ( Math.round( PrecioCostoAct * 100 ) / 100 ).toFixed( 2 ) + "</td>" +
                    "<td>" + ( Math.round( PrecioVentaAnt * 100 ) / 100 ).toFixed( 2 ) + "</td>" +
                    "<td>" + ( Math.round( PrecioVentaAct * 100 ) / 100 ).toFixed( 2 ) + "</td>";
                    */
            
            }

            var totalcoincidencias = 0;

            if( !document.getElementById("radiocodigo").checked && !document.getElementById("radioperiodo").checked ) {
                                                
                document.getElementById("textoalerta").value = "No ha seleccionado alguna opción";
                document.getElementById("contentalert").style.display = "block";

            } else if( document.getElementById("radiocodigo").checked && !document.getElementById("radioperiodo").checked ) {
                
                codigoproducto = document.getElementById("codigoproducto").value;
                
                if( codigoproducto == "" ) {
                                                
                    document.getElementById("textoalerta").value = "Ingrese el código de barras del producto a buscar";
                    document.getElementById("contentalert").style.display = "block";

                } else {

                    const tabla = document.getElementById('tablamovimientos');            
                
                    for( var i = 1; i < tabla.rows.length; i++ ) {

                        var textotablacodigo = tabla.rows[i].cells[2].innerText;
                        
                        if( codigoproducto == textotablacodigo ) {             

                            totalcoincidencias = totalcoincidencias + 1;

                        } else {
                            
                            tabla.rows[i].style.display = "none";

                        }
                    }
                    
                    if( totalcoincidencias > 0 ) {
                        
                        document.getElementById("resultadofiltrado").innerHTML = "<p> Total de coincidencias: <strong>"+ totalcoincidencias + "</strong> </p>";

                    } else {
                                                
                        document.getElementById("textoalerta").value = "Ningún producto coincide con el código ingresado";
                        document.getElementById("contentalert").style.display = "block";

                    }

                }
            
            } else if( !document.getElementById("radiocodigo").checked && document.getElementById("radioperiodo").checked ) {

                if( document.getElementById("labelbotonformmovimientos").value == "FechaEspecifica" ) {

                    var fecha = document.getElementById("fechainicial").value; 

                    if( fecha == "" ) {
                                                                        
                        document.getElementById("textoalerta").value = "Debe ingresar la fecha a buscar";
                        document.getElementById("contentalert").style.display = "block";

                    } else {

                        var fechaespecifica = new Date( fecha ).toISOString().split("T")[0];    

                        const tabla = document.getElementById('tablamovimientos');            
                    
                        for( var i = 1; i < tabla.rows.length; i++ ) {

                            var auxfechamovimiento = tabla.rows[i].cells[3].innerText;
                            
                            if( fechaespecifica == auxfechamovimiento ) {             
                                                                
                                totalcoincidencias = totalcoincidencias + 1;

                            } else {

                                tabla.rows[i].style.display = "none";

                            }

                        }
                    
                    
                        if( totalcoincidencias > 0 ) {
                        
                            document.getElementById("resultadofiltrado").innerHTML = "<p> Total de coincidencias: <strong>"+ totalcoincidencias + "</strong> </p>";

                        } else {
                                                    
                            document.getElementById("textoalerta").value = "Ninguna fecha coincide con la fecha ingresada";
                            document.getElementById("contentalert").style.display = "block";

                        }

                    }

                } else if( document.getElementById("labelbotonformmovimientos").value == "PeriodoEspecifico" ) {

                    var fechainicial = document.getElementById("fechainicial").value;
                    var fechafinal = document.getElementById("fechafinal").value;

                    if( fechainicial == "" && fechafinal == "" ) {
                                                                        
                        document.getElementById("textoalerta").value = "Debe ingresar la fecha inicial y la fecha final";
                        document.getElementById("contentalert").style.display = "block";

                    } else if( fechainicial == "" && fechafinal != "" ) {
                                                                        
                        document.getElementById("textoalerta").value = "Debe ingresar la fecha inicial";
                        document.getElementById("contentalert").style.display = "block";

                    } else if( fechainicial != "" && fechafinal == "" ) {
                                                                        
                        document.getElementById("textoalerta").value = "Debe ingresar la fecha final";
                        document.getElementById("contentalert").style.display = "block";

                    } else if( fechainicial != "" && fechafinal != "" ) {

                        const tabla = document.getElementById('tablamovimientos');   

                        var auxfechainicial = new Date( fechainicial ).toISOString().split("T")[0];  
                        var auxfechafinal = new Date( fechafinal ).toISOString().split("T")[0];           
                    
                        for( var i = 1; i < tabla.rows.length; i++ ) {

                            var auxfechamovimiento = tabla.rows[i].cells[3].innerText;

                            if( auxfechamovimiento >= auxfechainicial && auxfechamovimiento <= auxfechafinal ) {       
                                                                
                                totalcoincidencias = totalcoincidencias + 1;

                            } else {

                                tabla.rows[i].style.display = "none";

                            }
                        }
                    
                    
                        if( totalcoincidencias > 0 ) {
                        
                            document.getElementById("resultadofiltrado").innerHTML = "<p> Total de coincidencias: <strong>"+ totalcoincidencias + "</strong> </p>";

                        } else {
                                                    
                            document.getElementById("textoalerta").value = "Ninguna fecha está dentro de las fechas ingresadas";
                            document.getElementById("contentalert").style.display = "block";

                        }

                    }

                } else if( document.getElementById("labelbotonformmovimientos").value == "Diario" ) {

                    var fechaactual = new Date(new Date().getTime() - new Date().getTimezoneOffset() * 60000).toISOString().split("T")[0];

                    const tabla = document.getElementById('tablamovimientos');            
                    
                    for( var i = 1; i < tabla.rows.length; i++ ) {                        

                        auxfechamovimiento = new Date( tabla.rows[i].cells[3].innerText ).toISOString().split("T")[0];

                        if(  fechaactual == auxfechamovimiento ) {
                                
                            totalcoincidencias = totalcoincidencias + 1;

                        } else {

                            tabla.rows[i].style.display = "none";

                        }

                    }
                    
                    if( totalcoincidencias > 0 ) {
                    
                        document.getElementById("resultadofiltrado").innerHTML = "<p> Total de coincidencias: <strong>"+ totalcoincidencias + "</strong> </p>";

                    } else {
                                                
                        document.getElementById("textoalerta").value = "Ninguna fecha coincide con el periodo seleccionado";
                        document.getElementById("contentalert").style.display = "block";

                    }

                } else if( document.getElementById("labelbotonformmovimientos").value == "Semanal" ) {

                    var fechaactual = new Date( new Date().getTime() - new Date().getTimezoneOffset() * 60000).toISOString().split("T")[0];

                    //Fecha limite de la semana
                    var fechalimite = new Date( new Date() );
                    fechalimite.setDate( fechalimite.getDate() - 7 );
                    fechalimite = new Date( fechalimite ).toISOString().split("T")[0];

                    const tabla = document.getElementById('tablamovimientos');            
                    
                    for( var i = 1; i < tabla.rows.length; i++ ) {
                        
                        auxfechamovimiento = new Date( tabla.rows[i].cells[3].innerText ).toISOString().split("T")[0];

                        if( auxfechamovimiento >= fechaactual && auxfechamovimiento <= fechalimite ) {
                                
                            totalcoincidencias = totalcoincidencias + 1;

                        } else {
                            
                            tabla.rows[i].style.display = "none";

                        }
                    }
                    
                    
                    if( totalcoincidencias > 0 ) {
                    
                        document.getElementById("resultadofiltrado").innerHTML = "<p> Total de coincidencias: <strong>"+ totalcoincidencias + "</strong> </p>";

                    } else {
                                                
                        document.getElementById("textoalerta").value = "Ninguna fecha coincide con el periodo seleccionado";
                        document.getElementById("contentalert").style.display = "block";

                    }

                } else if( document.getElementById("labelbotonformmovimientos").value == "Mensual" ) {
                
                    var fechaactual = new Date( new Date() );
                    var mesfechaactual = fechaactual.getMonth();
                    var annofechaactual = fechaactual.getFullYear();

                    mesfechaactual += 1;

                    const tabla = document.getElementById('tablamovimientos');            
                    
                    for( var i = 1; i < tabla.rows.length; i++ ) {

                        auxarrayfechamovimiento = tabla.rows[i].cells[3].innerText.split("-");
                        
                        auxmesfechamovimiento = auxarrayfechamovimiento[1];
                        auxannofechamovimiento = auxarrayfechamovimiento[0];

                        if( mesfechaactual == auxmesfechamovimiento && annofechaactual == auxannofechamovimiento ) {
                                
                                totalcoincidencias = totalcoincidencias + 1;

                        } else {

                            tabla.rows[i].style.display = "none";

                        }

                    }
                    
                    
                    if( totalcoincidencias > 0 ) {
                    
                        document.getElementById("resultadofiltrado").innerHTML = "<p> Total de coincidencias: <strong>"+ totalcoincidencias + "</strong> </p>";

                    } else {
                                                
                        document.getElementById("textoalerta").value = "Ninguna fecha coincide con el periodo seleccionado";
                        document.getElementById("contentalert").style.display = "block";

                    }

                } else {
                            
                    document.getElementById("textoalerta").value = "No ha seleccionado alguna opción para filtrar, por favor seleccione alguna.";
                    document.getElementById("contentalert").style.display = "block";

                }

            }

        }        

        function VaciarTabla() {

            document.getElementById("codigoproducto").value = "";
            document.getElementById("resultadofiltrado").innerHTML = "";

            document.getElementById("radiocodigo").checked = false;
            document.getElementById("radioperiodo").checked = false;

            const tabla = document.getElementById('tablamovimientos');
            var rowCount = tabla.rows.length;         

            for  ( var x = rowCount - 1; x > 0; x-- ) { 
                tabla.deleteRow(x); 
            } 

        }

        function RestablecerTabla() {

            document.getElementById("buscarcodigo").style.display = "none";
            document.getElementById("buscarfechaespecifica").style.display = "none";
            document.getElementById("buscarfecha").style.display = "none";
        
            var arrayregistromovimientos = {!! json_encode( $registromovimientos->toArray(), JSON_HEX_TAG ) !!};
            
            VaciarTabla();

            for( var i = 0; i < arrayregistromovimientos.length; i++ ) {

                var PrecioCostoAnt = "0.00";
                var PrecioCostoAct = "0.00";
                var PrecioVentaAnt = "0.00";
                var PrecioVentaAct = "0.00";

                if( arrayregistromovimientos[i].PrecioCostoAnt != null ) {
                    PrecioCostoAnt = arrayregistromovimientos[i].PrecioCostoAnt;
                }

                if( arrayregistromovimientos[i].PrecioCostoAct != null ) {
                    PrecioCostoAct = arrayregistromovimientos[i].PrecioCostoAct;
                }

                if( arrayregistromovimientos[i].PrecioVentaAnt != null ) {
                    PrecioVentaAnt = arrayregistromovimientos[i].PrecioVentaAnt;
                }

                if( arrayregistromovimientos[i].PrecioVentaAct != null ) {
                    PrecioVentaAct = arrayregistromovimientos[i].PrecioVentaAct;
                }
                

                
                document.getElementById("tablamovimientos").insertRow(-1).innerHTML = 
                    "<td>" + arrayregistromovimientos[i].idMovimientoInventario + "</td>" +
                    "<td>" + arrayregistromovimientos[i].Descripcion + "</td>" +
                    "<td>" + arrayregistromovimientos[i].Codigo + "</td>" +
                    "<td>" + arrayregistromovimientos[i].FechaHora + "</td>" +
                    "<td>" + arrayregistromovimientos[i].CantidadAnt + "</td>" +
                    "<td>" + arrayregistromovimientos[i].CantidadAct + "</td>" +
                    "<td>" + arrayregistromovimientos[i].CantidadMov + "</td>";

                /*
                document.getElementById("tablamovimientos").insertRow(-1).innerHTML = 
                    "<td>" + arrayregistromovimientos[i].idMovimientoInventario + "</td>" +
                    "<td>" + arrayregistromovimientos[i].Descripcion + "</td>" +
                    "<td>" + arrayregistromovimientos[i].Codigo + "</td>" +
                    "<td>" + arrayregistromovimientos[i].FechaHora + "</td>" +
                    "<td>" + arrayregistromovimientos[i].CantidadAnt + "</td>" +
                    "<td>" + arrayregistromovimientos[i].CantidadAct + "</td>" +
                    "<td>" + arrayregistromovimientos[i].CantidadMov + "</td>" +
                    "<td>" + ( Math.round( PrecioCostoAnt * 100 ) / 100 ).toFixed( 2 ) + "</td>" +
                    "<td>" + ( Math.round( PrecioCostoAct * 100 ) / 100 ).toFixed( 2 ) + "</td>" +
                    "<td>" + ( Math.round( PrecioVentaAnt * 100 ) / 100 ).toFixed( 2 ) + "</td>" +
                    "<td>" + ( Math.round( PrecioVentaAct * 100 ) / 100 ).toFixed( 2 ) + "</td>";
                */
            
            }

        }

        function OcultarAlerta() {

            if( document.getElementById("contentalert").style.display == "none" ) {
                document.getElementById("contentalert").style.display = "block";
            } else {
                document.getElementById("contentalert").style.display = "none";
            }

        }

        function FechaInicioSeleccionada() {

            //console.log("CHANGE FECHA INICIO");

            var fechainicial = new Date( document.getElementById("fechainicial").value );
            fechainicial.setDate( fechainicial.getDate() + 1 );
            fechainicial = new Date( fechainicial ).toISOString().split("T")[0];
            document.getElementById('fechafinal').min = fechainicial;

        }

        function FechaFinalSeleccionada() {
            //console.log("CHANGE FECHA FINAL");

            var fechainicialmaxima = new Date( document.getElementById("fechafinal").value );
            fechainicialmaxima.setDate( fechainicialmaxima.getDate() - 1 );
            fechainicialmaxima = new Date( fechainicialmaxima ).toISOString().split("T")[0];
            document.getElementById('fechainicial').max = fechainicialmaxima;

        }

    </script>

    @if(Session::has('success'))
        <div class="alert alert-success">
            {{session('success')}}
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        </div>
    @elseif( Session::has('warning'))
        <div class="alert alert-warning">
            {{session('warning')}}
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        </div>
    @elseif( Session::has('danger'))
        <div class="alert alert-danger">
            {{session('danger')}}
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        </div>
    @endif


    <!-- DIV FILTRADO -->
    <div class="container">
        <div class="row justify-content-center">
            <form action="listadomovimientosinventario" method="get" onsubmit="return showLoad()">
                <div  class="row" style="margin: auto; width: 100%; border-collapse: separate; ">

                        <!-- Radio buscar por código producto -->
                    <div class="custom-control  custom-radio" onclick="javascript:mostrarradios();" style="margin-right: 5px">
                        <input type="radio" id="radiocodigo" name="radios" class="custom-control-input">
                        <label class="custom-control-label" for="radiocodigo">Buscar por producto</label>
                    </div>

                        <!-- Radio buscar por periodo -->
                    <div class="custom-control custom-radio" onclick="javascript:mostrarradios();" style="margin-right: 5px">
                        <input type="radio" id="radioperiodo" name="radios" class="custom-control-input">
                        <label class="custom-control-label" for="radioperiodo">Buscar por período</label>
                    </div>

                        <!-- Buscar por código producto -->
                    <div class="custom-control custom-radio" id="buscarcodigo" name="buscarcodigo" style="display: none">
                        <input type="text"  class="form-control-sm" id="codigoproducto" name="codigoproducto" placeholder="Ingrese el código" style="margin-left: 10px; margin-right:5px;">
                    </div>

                        <!-- Buscar por periodo -->
                    <div class="btn-group"  id="buscarfecha" name="buscarfecha" role="group" style="display: none">
                        <a id="fechadiario"  onclick="javascript:cambiartextolabel2('Diario')" > <button type="button"  class="btn btn-info">Diario</button>  </a>
                        <a id="fechasemanal" onclick="javascript:cambiartextolabel2('Semanal')"> <button type="button" class="btn btn-info">Semanal</button>  </a>                                     
                        <a id="fechasemanal" onclick="javascript:cambiartextolabel2('Mensual')"> <button type="button" class="btn btn-info">Mensual</button>  </a>
                        

                        <div class="btn-group" role="group">
                            <button id="btnGroupDrop1" type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Por fecha
                            </button>
                            
                            <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                                <a class="dropdown-item" id="fechaespecifica" onclick="javascript:mostrarinputfechas(this);">Fecha específica</a>
                                <a class="dropdown-item" id="periodoespecifico" onclick="javascript:mostrarinputfechas(this);">Período de fechas</a>
                            </div>
                        </div>
                    </div>

                        <!-- Buscar por periodo especificas -->
                    <div class="btn-group"  id="buscarfechaespecifica" role="group" style="display: none">
                        
                        <div id="divfechainicial" style="display: none">
                            <label for="fechainicial" id="labelfechainicial"> Seleccione la fecha inicial </label>
                            <input type="date" id="fechainicial" class="form-control" onchange="javascript:FechaInicioSeleccionada();">
                        </div>

                        <div id="divfechafinal" style="display: none">
                            <label for="fechafinal" id="labelfechafinal"> Seleccione la fecha final </label>
                            <input type="date" id="fechafinal" class="form-control" onchange="javascript:FechaFinalSeleccionada();">
                        </div>

                    </div>

                    <div style="margin-left: 5px" style="margin-right: 5px">
                        <input type="text" id="labelbotonformmovimientos" style="display: none" value="Buscar">
                        <a onclick="javascript:FiltrarTabla();"> 
                            <button type="button" id="botonformmovimientos" class="btn btn-success">BUSCAR</button>  
                        </a>
                    </div>

                </div>
            </form>
            
            <a style="margin-left: 10px;">  <button  class="btn btn-primary" id="botonrestablecer" onclick="javascript:RestablecerTabla();">Restablecer Tabla</button> </a>
                            
            <label id="resultadofiltrado" style="margin-left: 20px; margin-top:5px;"></label>
        </div>
    </div>

    <!-- Alerta -->
    <div class="container"  id="contentalert" style="display: none;">
        <div class="alert alert-danger alert-dismissible" role="alert" style="margin-top: 1%" >
            <input class="form-control-plaintext" value = "" id="textoalerta" disabled>    
            <button type="button" class="close" onclick="javascript:OcultarAlerta();" style="margin-top: 5px">
            <span>&times;</span>
            </button>
        </div>
    </div>

    <br>
    <br>

    <!-- TABLA -->    
    <div class="container">
        <div class="row justify-content-center">

            <table class="table table-hover" id="tablamovimientos" name="tablamovimientos">
                <thead  class="thead-dark">
                    <tr>
                        <th scope="col"> ID movimiento </th>
                        <th scope="col"> Tipo movimiento </th>
                        <th scope="col"> Código producto </th>
                        <th scope="col"> Fecha </th>
                        <th scope="col"> Cantidad Anterior </th>
                        <th scope="col"> Cantidad Actual </th>
                        <th scope="col"> Cantidad Movida </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ( $registromovimientos as $movimientos )
                        <tr>
                            <td>{{ $movimientos-> idMovimientoInventario }}</td>
                            <td>{{ $movimientos-> Descripcion }}</td>
                            <td>{{ $movimientos-> Codigo }}</td>
                            <td>{{ $movimientos-> FechaHora }}</td>
                            <td>{{ $movimientos-> CantidadAnt }}</td>
                            <td>{{ $movimientos-> CantidadAct }}</td>
                            <td>{{ $movimientos-> CantidadMov }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            

        </div>
    </div>    
    
    <br><br><br>

@endsection
