@extends('home')

    @section('title')
        Filtrado de catálogo de productos
    @endsection

    <link rel="stylesheet" href="{{ asset('css/EstilosInventario/estilosfiltradocatalogoproductos.css') }}">

    <script src="{{ asset('js/FuncionesInventario/funcionesfiltradocatalogoproductos.js') }}">
        //
    </script>

@section('contenido')

    @if(Session::has('success'))
        <div class="alert alert-success">
            {{session('success')}}
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        </div>
    @elseif(Session::has('danger'))
        <div class="alert alert-danger">
            {{session('danger')}}
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        </div>
    @elseif(Session::has('warning'))
        <div class="alert alert-warning">
            {{session('warning')}}
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        </div>
    @endif

    <!-- Botones -->
    <div class="container" style="margin-bottom: 10px;">
        <div class="row justify-content-center">

            <?php
                if( Auth::user()->Nivel == 1 ) {
            ?>
                <button id="botonmodificar" onclick="javascript:ObtenerURL();" class="btn btn-info">Modificar</button>                
            <?php
                }
            ?>
            <a href="{{ route( 'catalogoproductos' ) }}"> <button id="botonregresar" class="btn btn-warning">Regresar</button> </a>

        </div>
        
    </div>

    <!-- Tabla -->
    <div class="container">
        <div class="row justify-content-center">
        
            <table class="table table-hover">
                <thead  class="thead-dark">
                    <tr>
                        <th scope="col>"> Código de barra </th>
                        <th scope="col>"> Marca </th>
                        <th scope="col>"> Presentación </th>
                        <th scope="col>"> Nombre del proveedor </th>
                        <th scope="col>"> Categoría </th>
                        <th scope="col>"> Descripción </th>
                        <th scope="col>"> Precio de costo </th>
                        <th scope="col>"> Precio de venta </th>
                        <th scope="col>"> IVA </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ( $listadoproductos as $productos )
                        <tr>
                            <td>{{ $productos-> codigo }}</td>
                            <td>{{ $productos-> nombremarca }}</td>
                            <td>{{ $productos-> nombrepresentacion }}</td>
                            <td>{{ $productos-> nombrecompleto }}</td>
                            <td>{{ $productos-> nombrecategoria }}</td>
                            <td>{{ $productos-> descripcion }}</td>                            
                            <td> <?php echo bcadd( $productos->preciocosto , '0', 2); ?>
                            <td> <?php echo bcadd( $productos->precioventa , '0', 2); ?>
                            <td>
                                @if($productos->iva == 0 )
                                    {{ "NO" }}
                                @else
                                    {{ "SI" }}
                                @endif
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>

        </div>
    </div>

    <br><br><br>
    
@endsection