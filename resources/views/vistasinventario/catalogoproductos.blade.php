@extends('home')

    @section('title')
        Catálogo de productos
    @endsection


@section('contenido')

    <link rel="STYLESHEET" href="{{ asset( 'css/EstilosInventario/estiloscatalogoproductos.css' ) }}">

    <script>

        function EjecutarFiltrado( opcion, pos ) {

            if( opcion == 1 ) { //CATEGORIAS

                var listadocategorias = {!! json_encode( $listadocategorias->toArray(), JSON_HEX_TAG ) !!};
                //var filtro = listadocategorias[pos].nombre;
                var filtro = listadocategorias[pos].nombre.replace(/ /g, "");                
                
                location.href ="/filtradocatalogoproductos/Categorias/" + filtro;

            } else if ( opcion == 2 ) { //MARCAS
                
                var listadomarcas = {!! json_encode( $listadomarcas->toArray(), JSON_HEX_TAG ) !!};
                //var filtro = listadomarcas[pos].descripcion;
                var filtro = listadomarcas[pos].descripcion.replace(/ /g, "");

                location.href ="/filtradocatalogoproductos/Marcas/" + filtro;

            } else if( opcion == 3 ) { //PRESENTACIONES

                var listadopresentaciones = {!! json_encode( $listadopresentaciones->toArray(), JSON_HEX_TAG ) !!};
                //var filtro = listadopresentaciones[pos].medida;
                var filtro = listadopresentaciones[pos].medida.replace(/ /g, "");
                
                location.href ="/filtradocatalogoproductos/Presentaciones/" + filtro;

            }

        }

        function filtradoporcategorias() {
        
            if( document.getElementById("filtradocategorias").checked ) {
            
                document.getElementById("containertextotooltip").style.display = "block";
                document.getElementById("labeltextotooltip").innerText = "PRESIONE UNA IMAGEN PARA VER MÁS DETALLE SOBRE ESA CATEGORÍA";


                var listadocategorias = {!! json_encode( $listadocategorias->toArray(), JSON_HEX_TAG ) !!};
                //console.log("Datos listadocategorias: ", listadocategorias );

                var tamanoarray = listadocategorias.length;     
                var contenidodivimagenes = "";                    

                document.getElementById("contenedoropcionesfiltrado").innerHTML = "";

                for( var i = 0; i < tamanoarray; i++ ) {

                    var urlimagen = listadocategorias[i].urlimagen;
                    
                        contenidodivimagenes += 
                        "<div class='contenedor' id='contenedoropcionesfiltrado'>" +
                            "<img src=/images/" + urlimagen + " /onerror='imgError( this, 1 );' / width='300px' height='200px' " +
                            "onclick='javascript:EjecutarFiltrado( 1, " + i +" )'>" +
                            "<div class='centrado'>"+
                                "<a href=/filtradocatalogoproductos/Categorias/" + listadocategorias[i].nombre.replace(/ /g, "") +">" +
                                    listadocategorias[i].nombre +
                                "</a>" +
                            "</div>" +
                        "</div>";                    
                    
                }

                document.getElementById("contenedoropcionesfiltrado").innerHTML = contenidodivimagenes;

            }            

        }

        function filtradopormarcas() {

            if( document.getElementById("filtradomarcas").checked ) {
            
                document.getElementById("containertextotooltip").style.display = "block";
                document.getElementById("labeltextotooltip").innerText = "PRESIONE UNA IMAGEN PARA VER MÁS DETALLE SOBRE ESA MARCA";
                
                var listadomarcas = {!! json_encode( $listadomarcas->toArray(), JSON_HEX_TAG ) !!};
                //console.log("Datos listadomarcas: ", listadomarcas );

                var tamanoarray = listadomarcas.length;                            

                document.getElementById("contenedoropcionesfiltrado").innerHTML = "";

                for( var i = 0; i < tamanoarray; i++ ) {

                    var urlimagen = listadomarcas[i].urlimagen;

                    document.getElementById("contenedoropcionesfiltrado").innerHTML += 
                        "<div class='contenedor' id='contenedoropcionesfiltrado'>" +
                            "<img src=/images/" + urlimagen + " /onerror='imgError( this, 2 );' / width='300px' height='200px' " +
                            "onclick='javascript:EjecutarFiltrado( 2, " + i +" )'>" +
                            "<div class='centrado'>"+
                                "<a href=/filtradocatalogoproductos/Marcas/" + listadomarcas[i].descripcion.replace(/ /g, "") +">" +
                                listadomarcas[i].descripcion +
                                "</a>" +
                            "</div>" +
                        "</div>";

                }

            }
        

        }
        
        function filtradoporpresentaciones() {
        
            if( !document.getElementById("filtradopresentaciones").checked ) {
            
                document.getElementById("containertextotooltip").style.display = "block";
                document.getElementById("labeltextotooltip").innerText = "PRESIONE UNA IMAGEN PARA VER MÁS DETALLE SOBRE ESA PRESENTACIÓN";

                var listadopresentaciones = {!! json_encode( $listadopresentaciones->toArray(), JSON_HEX_TAG ) !!};
                //console.log("Datos listadopresentaciones: ", listadopresentaciones );

                var tamanoarray = listadopresentaciones.length;                            

                document.getElementById("contenedoropcionesfiltrado").innerHTML = "";

                for( var i = 0; i < tamanoarray; i++ ) {

                    var urlimagen = listadopresentaciones[i].urlimagen;

                    document.getElementById("contenedoropcionesfiltrado").innerHTML += 
                        "<div class='contenedor' id='contenedoropcionesfiltrado'>" +
                            "<img src=/images/" + urlimagen + " /onerror='imgError( this, 3 );' / width='300px' height='200px' " +
                            "onclick='javascript:EjecutarFiltrado( 3, " + i +" )'>" +
                            "<div class='centrado'>"+
                                "<a href=/filtradocatalogoproductos/Presentaciones/" + listadopresentaciones[i].medida.replace(/ /g, "") +">" +
                                    listadopresentaciones[i].medida +
                                "</a>" +
                            "</div>" +
                        "</div>";

                }

            }            

        }

        function imgError( image, strerror ) {            

            if( strerror == 1 ) {
                
                var urldef = "/images/CatalogoProductos/Categorias/DefaultThumb.jpg";

            } else if( strerror == 2 ) {
            
                var urldef = "/images/CatalogoProductos/Marcas/DefaultThumb.jpg";

            } else if( strerror == 3 ) {
            
                var urldef = "/images/CatalogoProductos/Presentaciones/DefaultThumb.jpg";

            } else {

            }

            //console.log( "ERROR AL CARGAR IMAGEN - ", image );
            image.src = urldef;

        }

    </script>
        
    @if(Session::has('success'))
        <div class="alert alert-success">
            {{session('success')}}
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        </div>
    @elseif( Session::has('warning'))
        <div class="alert alert-warning">
            {{session('warning')}}
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        </div>
    @elseif( Session::has('danger'))
        <div class="alert alert-danger">
            {{session('danger')}}
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        </div>
    @endif
 
    <!-- ALERTAS INVENTARIO -->
    @if(  isset(  $listaalertasinventario  ) )    
        
        <style>
            #divalertas {
                position: fixed;
                bottom: 0;
                right: 0;
            }

        </style>
        
        <script>

            window.onload = function( e ) {

                console.log("ALERTAS INVENTARIO URL ANTERIOR: ", document.referrer );
                //Obtener la url anterior
                var urlanterior = document.referrer;
                                            
                var listaalertasinventario = {!! json_encode( $listaalertasinventario->toArray(), JSON_HEX_TAG ) !!};

                var tamano = listaalertasinventario.length;
                var stringcontentalertas = "";


                for( let i = 0; i < listaalertasinventario.length; i++ ) {

                    stringcontentalertas +=
                        "<strong> "+ listaalertasinventario[i].descripcionproducto  + "</strong> : " + listaalertasinventario[i].cantidadproducto + " unidades en existencia.<br>";

                }
                                    
                document.getElementById("parrafoscantidad").innerHTML = tamano + " productos con baja existencia";
                document.getElementById("parrafoalertas").innerHTML = stringcontentalertas;

                if( tamano > 3 ) {
                    document.getElementById("enlacevermas").style.display = "inline";
                    document.getElementById("divalertas").style.display = "block";
                } else {
                    document.getElementById("enlacevermas").style.display = "none";
                    document.getElementById("divalertas").style.display = "none";
                }

            }

        </script>

        <div id="divalertas" style="display: none;">
            <div class="alert alert-warning alert-dismissible fade show" role="alert">
                
                <strong>Alerta de inventario bajo - </strong> <strong id="parrafoscantidad"></strong>
                <br>
                <p id="parrafoalertas" style="text-align: right;">
                </p>
                <p id="parrafoenlacevermas" style="text-align: center;">
                    <a id="enlacevermas" style="text-decoration: none; color: blue; display: none;" href="{{ route( 'alertasstockminimo' ) }}">Haga click para ver más información</a>
                </p>       
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

        </div>

    @endif

    <!-- DIV FILTRADO DE CATALOGO-->
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header"  align="center">{{ __('Filtrado de catálogo') }}</div>

                    <div class="card-body">

                            <!-- Buscar producto por código de barras -->
                            <div class="form-group row"  style="margin-bottom: 10px">

                                <!-- FILTRAR POR CATEGORÍA -->
                                <div class="form-check"  onclick="filtradoporcategorias();">
                                    <input class="form-check-input" type="radio" name="radiofiltrado" id="filtradocategorias">
                                    <label class="form-check-label" for="filtradocategorias">
                                        Filtrar por categorías
                                    </label>
                                </div>

                                <!-- FILTRAR POR MARCAS -->
                                <div class="form-check"  onclick="filtradopormarcas();" style="margin-left: 15%">
                                    <input class="form-check-input" type="radio" name="radiofiltrado" id="filtradomarcas">
                                    <label class="form-check-label" for="filtradomarcas">
                                        Filtrar por marcas
                                    </label>
                                </div>
                                
                                <!-- FILTRAR POR PRESENTACIONES -->
                                <div class="form-check" onclick="filtradoporpresentaciones();" style="margin-left: 15%">
                                    <input class="form-check-input" type="radio" name="radiofiltrado" id="filtradopresentaciones">
                                    <label class="form-check-label" for="filtradopresentaciones">
                                        Filtrar por presentaciones
                                    </label>
                                </div>
                                
                            </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    
    <!-- TOOLTIPTEXT -->
    <div class="container" id="containertextotooltip" style="margin-left:110px; margin-top: 10px; display:none;">
        
        <div class="row justify-content-center">
            <label class="textotooltip" id="labeltextotooltip">
                HAGA DOBLE CLICK SOBRE UN GRUPO PARA VER SU INFORMACIÓN
            </label>
        </div>
    </div>
    
    <div class="container" id="contenedoropcionesfiltrado" style="margin-left: 15%">
        <div class="row justify-content-center">

        </div>
    </div>

    <br><br><br>

@endsection