@if( Auth::user()->Nivel == 1 )

    @extends('home')

    @section('title')
        Modificación de la información de presentación
    @endsection

    @section('contenido')


        @if(Session::has('success'))
            <div class="alert alert-success">
                {{session('success')}}
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            </div>
        @elseif(Session::has('danger'))
            <div class="alert alert-danger">
                {{session('danger')}}
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            </div>
        @elseif(Session::has('warning'))
            <div class="alert alert-warning">
                {{session('warning')}}
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            </div>
        @endif

        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header"  align="center">{{ __('Actualización de la información de la presentación') }}</div>
                        <div class="card-body">
                            
                            {!!Form::open(array('name' => 'formpresentacion', 'url'=>'actualizarinformacionpresentacion/'.$informacionpresentacion->idPresentacion,'method'=>'PUT','files'=>true , 'enctype'=>'multipart/form-data','autocomplete'=>'off'))!!}
                                
                                <!-- Imagen de la categoría -->
                                
                                <div class="form-group row">
                                    <div class="col-md-6" style="margin-left: 40%;">
                                        <img src="/images/{{ ( $informacionpresentacion->UrlImagen ) }}" alt="Imagen actual" width="150px" height="100px">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    {!! Form::label( 'avatar','Imagen de la presentación', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                    <div class="col-md-6">
                                    <input type="file" id="avatar" name="avatar"  value="{{ old('avatar')}}" >
                                    </div>
                                </div>
                                
                                <!-- Medida de la presentación -->
                                <div class="form-group row">
                                    <label for="medida" class="col-md-4 col-form-label text-md-right">{{ __('Medida') }}</label>
                                    <div class="col-md-6">
                                        <input type="text" id="medida" name="medida" class="form-control" value="{{ ( $informacionpresentacion->MedidaPresentacion ) }}" readonly>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col">
                                        <button name="botonformpresentacion" value="Modificar" type="submit" class="btn btn-success" style="margin-left:150px" style="margin-right:50px">Modificar</button>
                                        <button name="botonformpresentacion" value="Cancelar" type="submit" class="btn btn-danger" style="margin-left:200px" style="margin-right:50px">Cancelar</button>
                                    </div>
                                </div>

                            {!!Form::close()!!}

                        </div>
                    </div>
                </div>
            </div>
        </div>

    @endsection

@endif