@if( Auth::user()->Nivel == 1 )

    @extends('home')

    @section('title')
        Registro de presentación
    @endsection

    @section('contenido')


        @if(Session::has('success'))
            <div class="alert alert-success">
                {{session('success')}}
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            </div>
        @elseif(Session::has('danger'))
            <div class="alert alert-danger">
                {{session('danger')}}
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            </div>
        @elseif(Session::has('warning'))
            <div class="alert alert-warning">
                {{session('warning')}}
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            </div>
        @endif

        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header"  align="center">{{ __('Registro de presentación') }}</div>
                        <div class="card-body">                            

                            {!!Form::open(array('name' => 'formpresentacion', 'url'=>'añadirpresentacion','method'=>'POST','files'=>true , 'enctype'=>'multipart/form-data' ,'autocomplete'=>'off'))!!}
                                                                                                    
                                <div class="form-group row">
                                    {!! Form::label( 'avatar','Imagen de la presentación', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                    <div class="col-md-6">
                                    <input type="file" id="avatar" name="avatar"  value="{{ old('avatar')}}" >
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="medida" class="col-md-4 col-form-label text-md-right">{{ __('Medida') }}</label>
                                    <div class="col-md-6">
                                        {!! Form::text('medida',null, array( 'class' => 'form-control', 'placeholder' => 'Ingrese la medida' ) ) !!}
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col">
                                        <button name="botonformpresentacion" value="Registrar" type="submit" class="btn btn-success" style="margin-left:150px" style="margin-right:50px">Registrar</button>
                                        <button name="botonformpresentacion" value="Cancelar" type="submit" class="btn btn-danger" style="margin-left:200px" style="margin-right:50px">Cancelar</button>
                                    </div>
                                </div>

                            {!!Form::close()!!}

                        </div>
                    </div>
                </div>
            </div>
        </div>

    @endsection

@endif