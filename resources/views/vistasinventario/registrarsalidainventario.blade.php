@if( Auth::user()->Nivel == 1 )

    @extends('home')

    @section('title')
        Registro de salida de inventario
    @endsection

    <style>

        .scrollbar {

            height: 400px;
            overflow: auto;
            overflow-x: hidden;
            overflow-y: scroll;
            white-space:nowrap;
            border-radius: 10px;
            

        }

        #scrollbar-style::-webkit-scrollbar {
            
            width: 6px;
            background-color: #F5F5F5;

        }

        /* Se pone un color de fondo y se redondean las esquinas del thumb */
        #scrollbar-style::-webkit-scrollbar-thumb {

            
            border-radius: 4px;            
            /*background-color: rgb(84, 81, 72);*/
            background-color: rgb(49, 163, 204);

        }

        /* Se cambia el fondo y se agrega una sombra cuando esté en hover */
        #scrollbar-style::-webkit-scrollbar-thumb:hover {

            background: #b3b3b3;
            box-shadow: 0 0 2px 1px rgba(0, 0, 0, 0.2);

        }

        /* Se cambia el fondo cuando esté en active */
        #scrollbar-style::-webkit-scrollbar-thumb:active {
            
            background-color: #999999;

        }
        /* Se pone un color de fondo y se redondean las esquinas del track */
        #scrollbar-style::-webkit-scrollbar-track {

            background: #e1e1e1;
            border-radius: 4px;

        }

        /* Se cambia el fondo cuando esté en active o hover */
        #scrollbar-style::-webkit-scrollbar-track:hover,
        #scrollbar-style::-webkit-scrollbar-track:active {

            background: #d4d4d4;

        }


    </style>
    
    <script>

        var banderatipoentradaselected = false;
        var tipomovimientoentradaseleccionado;

        var textocantidadmaxinventario = 0;

        var arregloproductodescuentos = [];

        //Bandera para saber si el botón de agregar producto a sido presionado y no tener que cargar la lista de 
        //productos al modal listadoproductos más de una vez.
        var banderalistadoproductos = false;

        //Guardará el listado de todos los productos y servirá como auxiliar para hacer la búsqueda y filtrado por descripción.
        var arraylistadoproductos = [];

            //Llamada a API - Producto
        function BuscarProducto() {

            var codigoproducto = $("#codigo").val();

            if( banderatipoentradaselected ) {

                var ruta  = "{{ route('buscarproductocompleto') }}";
                var url = ruta + "/" + codigoproducto;

                //hace la búsqueda
                $.get( url ).done( function( data ) {    
                    var content = JSON.parse( JSON.stringify( data ) );  

                    var textocodigo = content.Codigo;
                    var textodescripcion = content.Descripcion;
                    var textoprecioventa = content.PrecioVenta;
                    var textoprecioventa = content.PrecioVenta;
                    var textopreciocompra = content.PrecioCosto;
                    var textopresentacion = content.NombrePresentacion;
                    textocantidadmaxinventario = content.Cantidad;

                    if(  textocodigo === undefined ) {
                        //console.log("Consulta vacía");
                        document.getElementById("codigo").value = "";
                        document.getElementById("descripcion").value = "";
                        document.getElementById("cantidad").value = "";
                        document.getElementById("precio").value = "";
                        document.getElementById("preciocom").value = "";
                        document.getElementById("presentacion").value = "";
                        document.getElementById("codigo").focus();
                        
                        document.getElementById("textoalerta").value = "No hay ningún producto registrado con ese código de barras";
                        document.getElementById("contentalert").style.display = "block";

                    } else {

                        //console.log("Consula exitosa");
                        document.getElementById("codigo").value = textocodigo;
                        document.getElementById("descripcion").value = textodescripcion;
                        document.getElementById("precio").value = ( Math.round( textoprecioventa * 100 ) / 100).toFixed( 2 );
                        document.getElementById("preciocom").value = ( Math.round( textopreciocompra * 100 ) / 100).toFixed( 2 );
                        document.getElementById("cantidad").value = "1";
                        document.getElementById("cantidad").max = textocantidadmaxinventario;
                        document.getElementById("cantidad").readOnly = false;
                        document.getElementById("presentacion").value = textopresentacion;
                        
                        var posicion = BuscarProductoArray( textocodigo );

                        if( posicion > -1 ){

                            CambiarMaximoCantidadInput( textocodigo, "Agregar" );

                        } else {
                            
                            document.getElementById('cantidad').setAttribute('max', textocantidadmaxinventario );

                        }

                        CalcularImporte();
                        document.getElementById("cantidad").focus();

                        //Imagen Producto
                        document.getElementById("imagenproducto").src = "images/" + content.Imagen;
                        document.getElementById("imagenproducto").style.display = "block";

                    }
                    
                }).catch(function( error) {
                    console.log("El error es: ", error);
                });

            } else {
                    
                document.getElementById("textoalerta").value = "Primero debe seleccionar el tipo de movimiento para agregar productos";
                document.getElementById("contentalert").style.display = "block";

            }

        }

        //CARGA LOS DATOS DE LA TABLA PRODUCTOS AL MODAL DE LISTADO PRODUCTOS
        function CargarListadoProductosModal() {      
            
            document.getElementById("listadoproductosdescripcion").value = "";        
            
            if( !banderalistadoproductos ) {
                                
                //var url  = "{{ route('listadoproductosmodal') }}";
                var url  = "{{ route('proveedoreslistadoproductosmodal') }}";

                //hace la búsqueda
                $.get( url ).done( function( data ) {

                    var content = JSON.parse( JSON.stringify( data ) );

                    arraylistadoproductos = content;

                    console.log( "Contenido listadoproductos: ", content );

                    const tablalistadoproductos = document.getElementById( 'tablalistadoproductos' );
                    var rowCount = tablalistadoproductos.rows.length;         

                    for  ( var x = rowCount - 1; x > 0; x-- ) { 
                        tablalistadoproductos.deleteRow(x); 
                    }
                    
                    if( content.length > 0 ) {

                        for( var i = 0; i < content.length; i++ ) {

                            var codigoproducto = content[i].CodigoProducto;
                            var descripcionproducto = content[i].DescripcionProducto;
                            var precioventaproducto = content[i].PrecioVentaProducto;

                            precioventaproducto = ( Math.round( precioventaproducto * 100 ) / 100).toFixed( 2 );

                            document.getElementById("tablalistadoproductos").insertRow(-1).innerHTML = 
                                "<td>" + codigoproducto + "</td>" +
                                "<td>" + descripcionproducto + "</td>" +
                                "<td>"+ precioventaproducto + "</td>";

                        }

                    }

                });

            } else {

                banderalistadoproductos = true;

            }
            
        }
        
        //LLAMA A LA FUNCION BuscarProductoDescripcionListadoProductosModal, SI LA TECLA ENTER FUE PRESIONADA
        function BuscarProductoDescripcionListadoProductosModalKD( event ) {

            var keycode = event.keyCode;

            if(keycode == '13') {
                BuscarProductoDescripcionListadoProductosModal();
            }

        }

        //FILTRA EL LISTADO A PARTIR DE LA DESCRIPCION INGRESADA
        function BuscarProductoDescripcionListadoProductosModal() {

            if( document.getElementById("listadoproductosdescripcion").value != "" ) {

                var descripcionlistadoproductos = document.getElementById("listadoproductosdescripcion").value;
                document.getElementById("listadoproductosdescripcion").value = "";

                const tablalistadoproductos = document.getElementById( 'tablalistadoproductos' );
                var rowCount = tablalistadoproductos.rows.length;         

                for  ( var x = rowCount - 1; x > 0; x-- ) { 
                    tablalistadoproductos.deleteRow(x); 
                }
                
                if( arraylistadoproductos.length > 0 ) {

                    for( var i = 0; i < arraylistadoproductos.length; i++ ) {

                        if(  arraylistadoproductos[i].DescripcionProducto.toLowerCase().includes( descripcionlistadoproductos.toLowerCase() ) ) {

                            var codigoproducto = arraylistadoproductos[i].CodigoProducto;
                            var descripcionproducto = arraylistadoproductos[i].DescripcionProducto;
                            var precioventaproducto = arraylistadoproductos[i].PrecioVentaProducto;
                        
                            precioventaproducto = ( Math.round( precioventaproducto * 100 ) / 100 ).toFixed( 2 );

                            document.getElementById("tablalistadoproductos").insertRow(-1).innerHTML = 
                                "<td>" + codigoproducto + "</td>" +
                                "<td>" + descripcionproducto + "</td>" +
                                "<td>"+ precioventaproducto + "</td>";

                        }                        

                    }

                    rowCount = tablalistadoproductos.rows.length; 
                    if( rowCount == 1 ) {

                        document.getElementById("textoalertbusquedalistadoproductos").innerHTML = "<strong>No hay productos que coincidan con la descripción buscada.</strong>";
                        document.getElementById("contentalertbusquedalistadoproductos").style.display = "block";

                    }

                }

            } else {

                document.getElementById("textoalertbusquedalistadoproductos").innerHTML = "<strong>Ingrese la descripción.</strong>";
                document.getElementById("contentalertbusquedalistadoproductos").style.display = "block";
                
            }

        }        

        function OcultarAlertaBusquedaListadoProductos() {

            if( document.getElementById("contentalertbusquedalistadoproductos").style.display == "none" ) {
                document.getElementById("contentalertbusquedalistadoproductos").style.display = "block";
            } else {
                document.getElementById("contentalertbusquedalistadoproductos").style.display = "none";
            }

        }

        //RESTABLECE TODOS LOS PRODUCTOS DEL LISTADO DE PRODUCTOS
        function RestablecerListaListadoProductosModal() {   

            const tablalistadoproductos = document.getElementById( 'tablalistadoproductos' );
            var rowCount = tablalistadoproductos.rows.length;         

            for  ( var x = rowCount - 1; x > 0; x-- ) { 
                    tablalistadoproductos.deleteRow(x); 
            }
                
            if( arraylistadoproductos.length > 0 ) {

                for( var i = 0; i < arraylistadoproductos.length; i++ ) {

                    var codigoproducto = arraylistadoproductos[i].CodigoProducto;
                    var descripcionproducto = arraylistadoproductos[i].DescripcionProducto;
                    var precioventaproducto = arraylistadoproductos[i].PrecioVentaProducto;
                
                    precioventaproducto = ( Math.round( precioventaproducto * 100 ) / 100 ).toFixed( 2 );

                    document.getElementById("tablalistadoproductos").insertRow(-1).innerHTML = 
                        "<td>" + codigoproducto + "</td>" +
                        "<td>" + descripcionproducto + "</td>" +
                        "<td>"+ precioventaproducto + "</td>";                       

                }

            }

        }

        //FUNCION PARA OBTENER EL CODIGO SELECCIONADO DE LA TABLA DEL MODAL LISTADOPRODUCTO
        function ObtenerFilaListadoProductos( event ) {

            var tablalistadoproductos = document.getElementById('tablalistadoproductos'),
            selected = tablalistadoproductos.getElementsByClassName('selected');
            
            if (selected[0]) selected[0].className = '';
            event.target.parentNode.className = 'selected';

            document.getElementById("codigo").value = $("tr.selected td:first" ).html();
            
            BuscarProducto();

            $("#modalEmergenteListadoProductos").modal('hide');

        }

        function onKeyUp( event ) {
            var keycode = event.keyCode;

            if(keycode == '13'){
                BuscarProducto();
            }
        }

        //Saber si fue una tecla correspondiente a un número
        function onKeyDownHandler() {

            var keypresionada = event.which;
            if( ( keypresionada >= 48 && keypresionada <= 57 ) || ( keypresionada >=96 && keypresionada <= 105 ) ) {
                
                CalcularImporte();

            } else if( keypresionada == 13 ) { //TECLA ENTER 

                if( document.getElementById("codigo").value != "" ) {                                                
                    
                    var cantidadmaxinventarioproducto = VerificarCantidadMaximaProductoInventario( document.getElementById("codigo").value );
                    console.log("onKeyDownHandler - Caso: AGREGAR - CantidadMaxInventarioProducto: ", cantidadmaxinventarioproducto );

                    if ( document.getElementById("cantidad").value > cantidadmaxinventarioproducto ) {
                                                                            
                        document.getElementById("textoalerta").value = "No se puede añadir el producto a la tabla ya que actualmente hay " + cantidadmaxinventarioproducto + " unidades de ese producto en el inventario";
                        document.getElementById("contentalert").style.display = "block";
                    
                    } else {
                        
                        document.getElementById("codigo").focus();
                        CalcularImporte();
                        VerificarFilaDefault();

                    }
                    
                }
                
            }

        }

        function CalcularImporte() {

            var textoprecio = document.getElementById("preciocom").value;
            var textocantidad = document.getElementById("cantidad").value;

            if( textocantidad != "" ) {

                var numeroprecio = parseFloat( textoprecio );
                var numerocantidad = parseInt( textocantidad );

                var cantidadimporte = ( numerocantidad * numeroprecio );
                
                cantidadimporte = ( Math.round( cantidadimporte * 100 ) / 100 ).toFixed( 2 ); 
                document.getElementById("importe").value = cantidadimporte;

            } else {
                document.getElementById("importe").value = "";
            }
                

        }

        function CalcularTotal() {
            
            var total = 0.0;

            $('#tablaproductos tr').each(function (row, tr ) {
                var textocoluman = 0;
                textocolumna = Number.parseFloat( $(tr).find('td:eq(6)').text().trim() );
                
                if( !isNaN( textocolumna ) ) {
                    total = textocolumna  + total;
                }

            }); 


            total = ( Math.round( total * 100 ) / 100 ).toFixed( 2 );
            document.getElementById("total").value = total;

        }

        function BuscarProductoArray( codigo ) {

            var posicion = -1;

            for( let i = 0; i < arregloproductodescuentos.length; i++ ) {

                if( arregloproductodescuentos[i].Codigo == codigo ) {
                    posicion = i;
                }

            }

            return posicion;

        }
        
        //Función para modificar el input de cantidad y no permitir ingresar o seleccionar una cantidad mayor a la cantidad de productos que están en la tabla inventario.
        function CambiarMaximoCantidadInput( codigo, tipooperacion ) {

            var posicion = BuscarProductoArray( codigo );

            var cantidadmax;
            var cantidadproductoagregado;

            if( tipooperacion == "Agregar" ) {

                if( posicion > -1 ) {
                                        
                    cantidadmax = arregloproductodescuentos[ posicion ].CantidadMaximaInventario;
                    cantidadproductoagregado = arregloproductodescuentos[posicion].CantidadProducto;

                    var nuevacantidadmax = cantidadmax - cantidadproductoagregado;

                    if( cantidadmax == 0 ) {
                        
                        console.log("CANTIDAD MAX MENOR 0");
                        
                        document.getElementById("textoalerta").value = "Se han agotado las cantidades existentes en el inventario";
                        document.getElementById("contentalert").style.display = "block";
                             
                        document.getElementById("enlaceagregar").text = "Agregar";
                        document.getElementById("enlaceeliminar").style.display = "none"; 
    
                        document.getElementById("enlaceagregar").text = "Agregar";
                        document.getElementById("enlaceeliminar").style.display = "none";

                        document.getElementById("codigo").value = "";
                        document.getElementById("descripcion").value = "";
                        document.getElementById("cantidad").value = "";
                        document.getElementById("precio").value = "";
                        document.getElementById("presentacion").value = "";

                        document.getElementById("cantidad").readOnly = true;
                        document.getElementById("codigo").readOnly = false;
                        document.getElementById("codigo").focus();
                        

                    } else {
                        
                        console.log("CANTIDAD MAX MAYOR 0");
                        console.log("Cantidad max: ", nuevacantidadmax );
                        
                        arregloproductodescuentos[ posicion ].CantidadMaximaInventario = nuevacantidadmax;

                        document.getElementById("cantidad").setAttribute("max", nuevacantidadmax );

                    }

                } else {    
                                            
                    cantidadmax = arregloproductodescuentos[ posicion ].CantidadMaximaInventario;
                    document.getElementById("cantidad").setAttribute("max", cantidadmax );

                }

            } else if( tipooperacion == "Actualizar" || tipooperacion == "Eliminar" ) {

                cantidadmax = arregloproductodescuentos[ posicion ].CantidadMaximaInventario;
                document.getElementById("cantidad").setAttribute("max", cantidadmax );

            }

            

        }

        function VerificarCantidadMaximaProductoInventario ( codigo ) {

            var cantidadmaximaexistencias = 0;

           $.ajaxSetup({
                headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

           $.ajax({

                type: "GET",
                url: "{{ route('cantidadmaximainventarioproducto' ) }}" + "/" + codigo,
                data: { codigo : codigo },
                async:false,
                success: function(data){

                    cantidadmaximaexistencias = data[0].Cantidad;

                },
                error:function()
                {
                }
            });

            return cantidadmaximaexistencias;

        }

        function VerificarFilaDefault() {

            const tabla = document.getElementById('tablaproductos');
            var rowCount = document.getElementById("tablaproductos").rows.length;

                //Sólo está la fila por defecto
            if( rowCount == 2 ) {            

                var bool = false;

                $("#tablaproductos td").each(function(){
                        //cambiar "CODIGO" por el texto que está por defecto en la primera columna de código de producto.
                    if( $(this).text() == "-" ) {
                        bool = true;
                        //console.log($(this).text());
                    }
                });

                //Tiene la fila por default.
                if( bool ) {
                                                                                            
                    var cantidadmaxinventarioproducto = VerificarCantidadMaximaProductoInventario( document.getElementById("codigo").value );
                    console.log("VerificarFilaDefault - Caso: FilaDefault - CantidadMaxInventarioProducto: ", cantidadmaxinventarioproducto );

                    if ( document.getElementById("cantidad").value > cantidadmaxinventarioproducto ) {
                                                                            
                        document.getElementById("textoalerta").value = "No se puede añadir el producto a la tabla ya que actualmente hay " + cantidadmaxinventarioproducto + " unidades de ese producto en el inventario";
                        document.getElementById("contentalert").style.display = "block";
                    
                    } else {

                        tabla.deleteRow( rowCount - 1 );
                        AgregarFila();

                    }

                    //Ya tiene el registro de un producto en la primera fila.
                } else {

                    var textocodigobuscado = document.getElementById("codigo").value;
                    var bool = false;

                    $("#tablaproductos td").each(function(){
                            //cambiar "CODIGO" por el texto que está por defecto en la primera columna de código de producto.
                        if( $(this).text() == textocodigobuscado ) {
                            bool = true;
                            //console.log($(this).text());
                        }
                    });

                        //El producto ya está en la tabla ( Cuando sólo hay un producto en la tabla).
                    if( bool ) {

                        var auxiliarcantidad = 0;

                        $('#tablaproductos tr').each(function(row, tr) {
                            
                            varauxiliarcodigo = $(tr).find('td:eq(0)').text().trim();

                            if( varauxiliarcodigo == textocodigobuscado ) {
                                
                                auxiliarcantidad = $(tr).find('td:eq(2)').text().trim();
                                
                            }
                        });

                        var cantidadmaxinventarioproducto = VerificarCantidadMaximaProductoInventario( textocodigobuscado );
                        console.log("VerificarFilaDefault - Caso: ElProductoYaEstaEnLaPrimerFila - CantidadMaxInventarioProducto: ", cantidadmaxinventarioproducto , " AuxiliarCantidad: ", auxiliarcantidad );

                        if ( document.getElementById("cantidad").value > cantidadmaxinventarioproducto ) {
                                                                                
                            document.getElementById("textoalerta").value = "No se puede añadir el producto a la tabla ya que actualmente hay " + cantidadmaxinventarioproducto + " unidades de ese producto en el inventario";
                            document.getElementById("contentalert").style.display = "block";
                        
                        } else {
                            
                            ActualizarProductoTabla( 1 );

                        }
                        
                    } else {//El producto que se quiere ingresar no está en la tabla, pero ya hay más productos.
                                                                        
                        var cantidadmaxinventarioproducto = VerificarCantidadMaximaProductoInventario( textocodigobuscado );
                        console.log("VerificarFilaDefault - Caso: ElNoProductoEstaEnLaTablaPeroYaHayMasProductos - CantidadMaxInventarioProducto: ", cantidadmaxinventarioproducto );

                        if ( document.getElementById("cantidad").value > cantidadmaxinventarioproducto ) {
                                                                                
                            document.getElementById("textoalerta").value = "No se puede añadir el producto a la tabla ya que actualmente hay " + cantidadmaxinventarioproducto + " unidades de ese producto en el inventario";
                            document.getElementById("contentalert").style.display = "block";
                        
                        } else {
                                                        
                            AgregarFila();

                        }

                    }

                }

                // Más de un producto agregado a la tabla
            } else {

                var textocodigobuscado = document.getElementById("codigo").value;
                var bool = false;
                var fila = 1;    
                
                for( var i = 1; i < tabla.rows.length; i++ ) {
                    var textotablacodigo = tabla.rows[i].cells[0].innerText;
                    if( textocodigobuscado == textotablacodigo ) {
                        bool = true;
                        fila = i;
                    }
                }            
                
                        //El producto ya está en la tabla ( Cuando hay más de un producto en la tabla).
                if( bool ) {

                    var cantidadmaxinventarioproducto = VerificarCantidadMaximaProductoInventario( textocodigobuscado );
                    console.log("VerificarFilaDefault - Caso: ElProductoYaEstaEnLaPrimerFila - CantidadMaxInventarioProducto: ", cantidadmaxinventarioproducto , " AuxiliarCantidad: ", auxiliarcantidad );

                    if ( document.getElementById("cantidad").value > cantidadmaxinventarioproducto ) {
                                                                            
                        document.getElementById("textoalerta").value = "No se puede añadir el producto a la tabla ya que actualmente hay " + cantidadmaxinventarioproducto + " unidades de ese producto en el inventario";
                        document.getElementById("contentalert").style.display = "block";
                    
                    } else {
                        
                        ActualizarProductoTabla( fila );

                    }
                        
                } else {
                                                                        
                    var cantidadmaxinventarioproducto = VerificarCantidadMaximaProductoInventario( textocodigobuscado );
                    console.log("VerificarFilaDefault - Caso: ElNoProductoEstaEnLaTablaPeroYaHayMasProductos - CantidadMaxInventarioProducto: ", cantidadmaxinventarioproducto );

                    if ( document.getElementById("cantidad").value > cantidadmaxinventarioproducto ) {
                                                                            
                        document.getElementById("textoalerta").value = "No se puede añadir el producto a la tabla ya que actualmente hay " + cantidadmaxinventarioproducto + " unidades de ese producto en el inventario";
                        document.getElementById("contentalert").style.display = "block";
                    
                    } else {
                                                    
                        AgregarFila();

                    }

                }

            }

        }

        function ActualizarProductoTabla( fila ) {

            var textocodigo = document.getElementById("codigo").value;
                    
            const tabla = document.getElementById('tablaproductos');

            var cantidadold = parseInt( tabla.rows[fila].cells[2].innerText );
            var cantidadnew = parseInt( document.getElementById("cantidad").value );
            var precio = parseFloat( tabla.rows[fila].cells[4].innerText );
            var preciocomp = parseFloat( tabla.rows[fila].cells[5].innerText );
            //var precio = parseFloat( tabla.rows[fila].cells[3].innerText );
            //var preciocomp = parseFloat( tabla.rows[fila].cells[4].innerText );

            var precio = parseFloat( document.getElementById("precio").value );
            var preciocomp = parseFloat( document.getElementById("preciocom").value );

            // Verificación del texto del botón de agregar producto.
            textoenlaceagregar = document.getElementById("enlaceagregar").text;
            console.log("Texto del enlace: " , textoenlaceagregar );

            if( document.getElementById("enlaceagregar").text == "Agregar" ) {
                cantidadnew += cantidadold;

            } else if( document.getElementById("enlaceagregar").text == "Modificar" ) {  

                document.getElementById("enlaceagregar").text = "Agregar";
                document.getElementById("enlaceeliminar").style.display = "none";
                document.getElementById("codigo").readOnly = false;
                document.getElementById("codigo").focus();

            }

            var importenew = ( cantidadnew * preciocomp );

            precio = ( Math.round( precio * 100 ) / 100).toFixed( 2 );
            preciocomp = ( Math.round( preciocomp * 100 ) / 100).toFixed( 2 );
            importenew = ( Math.round( importenew * 100 ) / 100).toFixed( 2 );

            document.getElementById("tablaproductos").rows[fila].cells[2].innerText = cantidadnew;
            document.getElementById("tablaproductos").rows[fila].cells[3].innerText = precio;
            document.getElementById("tablaproductos").rows[fila].cells[4].innerText = preciocomp;
            document.getElementById("tablaproductos").rows[fila].cells[6].innerText = importenew;

            document.getElementById("codigo").value = "";
            document.getElementById("descripcion").value = "";
            document.getElementById("cantidad").value = "";
            document.getElementById("precio").value = "";
            document.getElementById("preciocom").value = "";
            document.getElementById("presentacion").value = "";
            document.getElementById("importe").value = "";

            document.getElementById("cantidad").readOnly = true;
            document.getElementById("codigo").focus();

            //Imagen Producto
            document.getElementById("imagenproducto").src = "";
            document.getElementById("imagenproducto").style.display = "none";
                                
            var posicion = BuscarProductoArray( textocodigo );

            if( posicion > -1 ) {

                arregloproductodescuentos[posicion].CantidadProducto = cantidadnew;

            }
            
            CambiarMaximoCantidadInput( textocodigo, "Actualizar" );
            
            CalcularTotal();

        }

        function ModificarFilaProducto( event ) {
            
            var table = document.getElementById('tablaproductos'),
            selected = table.getElementsByClassName('selected');
            
            if (selected[0]) selected[0].className = '';
            event.target.parentNode.className = 'selected';

            /*              SIN LA TABLA LISTADO PRODUCTOS MODAL
            document.getElementById("codigo").value = $("tr.selected td:first" ).html();
            document.getElementById("descripcion").value = $("tr.selected td:nth-child(2)").html();
            document.getElementById("cantidad").value = $("tr.selected td:nth-child(3)").html();
            document.getElementById("precio").value = $("tr.selected td:nth-child(4)").html();     
            document.getElementById("presentacion").value = $("tr.selected td:nth-child(5)").html();
            */
            
            //Imagen PRODUCTO
            //var rutaimagen = $("tr.selected td:nth-child(6)").html();
            var rutaimagen = $("tr.selected td:nth-child(8)").html();
            var arrayrutaimagen = rutaimagen.split("/");
            document.getElementById("imagenproducto").src = "images/Productos/" + arrayrutaimagen[5];
            document.getElementById("imagenproducto").style.display = "block";
            
            document.getElementById("codigo").value = $("#tablaproductos tr.selected td:first" ).html();
            document.getElementById("descripcion").value = $("#tablaproductos tr.selected td:nth-child(2)").html();
            document.getElementById("cantidad").value = $("#tablaproductos tr.selected td:nth-child(3)").html();
            document.getElementById("precio").value = $("#tablaproductos tr.selected td:nth-child(4)").html(); 
            document.getElementById("preciocom").value = $("#tablaproductos tr.selected td:nth-child(5)").html();   
            document.getElementById("presentacion").value = $("#tablaproductos tr.selected td:nth-child(6)").html();
            document.getElementById("importe").value = $("#tablaproductos tr.selected td:nth-child(7)").html();


            document.getElementById("codigo").readOnly = true;
            document.getElementById("descripcion").readOnly = true;
            document.getElementById("cantidad").readOnly = false; 
            document.getElementById("presentacion").readOnly = true;
            document.getElementById("importe").readOnly = true;

            document.getElementById("cantidad").focus();

            document.getElementById("enlaceagregar").text = "Modificar";
            document.getElementById("enlaceeliminar").style.display = "block";
            //document.getElementById("enlaceeliminar").style.marginTop = "35%";

            var textocodigobuscado = document.getElementById("codigo").value;
            
            CambiarMaximoCantidadInput( textocodigobuscado, "Actualizar" );

        }

        function AgregarFila() {
                    
            var textocodigo = document.getElementById("codigo").value;
            var textodescripcion = document.getElementById("descripcion").value;
            var textocantidad = document.getElementById("cantidad").value;
            var textoprecioventa = document.getElementById("precio").value;      
            var textopreciocompra = document.getElementById("preciocom").value;            
            var textopresentacion = document.getElementById("presentacion").value;
            var textoimporte = document.getElementById("importe").value;

            textoprecioventa = ( Math.round( textoprecioventa * 100 ) / 100 ).toFixed( 2 );
            textopreciocompra = ( Math.round( textopreciocompra * 100 ) / 100 ).toFixed( 2 );


            if( textocodigo != "" && textodescripcion != "" && textocantidad != "" && textoprecioventa != "" && textopresentacion != "" ) {

                const tabla = document.getElementById('tablaproductos');
                var rowCount = document.getElementById("tablaproductos").rows.length;
                //console.log("rowCount: " , rowCount );

                document.getElementById("tablaproductos").insertRow(-1).innerHTML = 
                    "<td>" + textocodigo + "</td>" +
                    "<td>" + textodescripcion + "</td>" +
                    "<td>"+ textocantidad + "</td>" +
                    "<td>" + textoprecioventa + "</td>" +
                    "<td>" + textopreciocompra + "</td>" +
                    "<td>" + textopresentacion + "</td>" +
                    "<td>"+ textoimporte + "</td>" +
                    "<td style='display:none;'>"+ document.getElementById("imagenproducto").src + "</td>";

                document.getElementById("codigo").value = "";
                document.getElementById("descripcion").value = "";
                document.getElementById("cantidad").value = "";
                document.getElementById("precio").value = "";
                document.getElementById("preciocom").value = "";
                document.getElementById("presentacion").value = "";
                document.getElementById("importe").value = "";
                document.getElementById("cantidad").readOnly = true;

                //arrayimagenesproductos.unshift ( textocodigo + "-" + document.getElementById("imagenproducto").src );
                document.getElementById("imagenproducto").src = "";                
                document.getElementById("imagenproducto").style.display = "none";
            
                let datospromocion = {   
                                        'Codigo' : textocodigo,
                                        'CantidadProducto' : textocantidad,
                                        'CantidadMaximaInventario' : textocantidadmaxinventario
                                    };

                arregloproductodescuentos.push( datospromocion );
            
                CambiarMaximoCantidadInput( textocodigo, "Agregar" );

                CalcularTotal();
                

            } else {

                document.getElementById("textoalerta").value = "No hay producto que agregar";
                document.getElementById("contentalert").style.display = "block";

            }

        }

        function EliminarFilaProducto() {
                    
            const tabla = document.getElementById('tablaproductos');
            var textocodigobuscado = document.getElementById("codigo").value;
            var bool = false;
            var fila = 1;    
            
            for( var i = 1; i < tabla.rows.length; i++ ) {
                var textotablacodigo = tabla.rows[i].cells[0].innerText;
                if( textocodigobuscado == textotablacodigo ) {
                    bool = true;
                    fila = i;
                }
            } 

            if( bool ) {

                tabla.deleteRow( fila, -1 ); 
                document.getElementById("enlaceagregar").text = "Agregar";
                document.getElementById("enlaceeliminar").style.display = "none";

                document.getElementById("codigo").value = "";
                document.getElementById("descripcion").value = "";
                document.getElementById("cantidad").value = "";
                document.getElementById("precio").value = "";
                document.getElementById("preciocom").value = "";
                document.getElementById("presentacion").value = "";
                document.getElementById("importe").value = "";

                document.getElementById("cantidad").readOnly = true;
                document.getElementById("codigo").readOnly = false;
                document.getElementById("codigo").focus();

                var posicion = BuscarProductoArray( textocodigobuscado );

                //ELIMINAR EL PRODUCTO DEL ARRAY A PARTIR DE SU POSICION
                if( posicion > -1 ) {

                    arregloproductodescuentos.splice( posicion, posicion + 1 );

                }

            
                //Imagen Producto
                document.getElementById("imagenproducto").src = "";                
                document.getElementById("imagenproducto").style.display = "none";
            
                CalcularTotal();

            }

        }

        function LimpiarInputs() {        

            const tabla = document.getElementById('tablaproductos');
            var rowCount = tabla.rows.length; 

            for  ( var x = rowCount - 1; x > 0; x-- ) { 
                tabla.deleteRow(x); 
            } 
    
            document.getElementById("enlaceagregar").text = "Agregar";
            document.getElementById("enlaceeliminar").style.display = "none";

            document.getElementById("codigo").value = "";
            document.getElementById("descripcion").value = "";
            document.getElementById("cantidad").value = "";
            document.getElementById("precio").value = "";
            document.getElementById("presentacion").value = "";
            document.getElementById("importe").value = "";


            document.getElementById("cantidad").readOnly = true;
            document.getElementById("codigo").readOnly = false;
            document.getElementById("codigo").focus();
            
            CalcularTotal();

        }

            //Obtiene los datos de todas las columnas de la tabla
        function GuardarDatosTabla() {
            
            var TableData = new Array();

            $('#tablaproductos tr').each(function(row, tr){

            TableData[row]=
            {
                "ColumnaCodigos" : $(tr).find('td:eq(0)').text().trim() //for first column value
                , "ColumnaCantidades" :$(tr).find('td:eq(2)').text().trim()  //for third column value
                , "ColumnaPrecioVen" :$(tr).find('td:eq(3)').text().trim()  //for third column value
                , "ColumnaPrecioCom" :$(tr).find('td:eq(4)').text().trim()  //for third column value
            }    
            }); 
            console.log("Valores: " , TableData);
            
            for (let index = 1; index < TableData.length; index++) {
                const element = TableData[index];
                console.log("Elementos array: " , element);
                
            }
            TableData.shift();  // first row will be empty - so remove
            return TableData;

        }

        function EnviarDatosTabla() {   

            var TableData;
            TableData = GuardarDatosTabla()
            TableData = JSON.stringify(TableData);

            const tabla = document.getElementById('tablaproductos');
            var numfilas = tabla.rows.length;        
            var textotabla = undefined;

            if( numfilas > 1 ) {
                if( tabla.rows[1].cells[0].innerText == "-" ) {
                    textotabla = undefined;
                } else {
                    textotabla = "Aplica";
                }
            }
        
            var selectmovimientos = document.getElementById("idtipomovimiento").value;
            var IdTipoMovimiento;
            IdTipoMovimiento = ( parseInt( selectmovimientos ) );

            IdTipoMovimiento = JSON.stringify( IdTipoMovimiento );
            console.log("ID seleccionado: ", IdTipoMovimiento );

        
            var selecttipomovimientos = document.getElementById("idtipomovimiento");
            var descripcionseleccionada = selecttipomovimientos.options[selecttipomovimientos.selectedIndex].text;
            var DescripcionMovimiento;
            DescripcionMovimiento = (  descripcionseleccionada );
            console.log("Texto seleccionado: ", DescripcionMovimiento );

            DescripcionMovimiento = JSON.stringify( DescripcionMovimiento );


            if( IdTipoMovimiento == "null" && textotabla == "Aplica" ) {
                MostrarmodalErrorVenta( "ERROR", "ERROR AL REGISTRAR LA ENTRADA", "La salida de inventario no ha podido ser registrada ya que no ha seleccionado el tipo de movimiento." );       
                //OcultarModal( "No ha seleccionado el tipo de movimiento" );
            } else if( IdTipoMovimiento == "null" && textotabla == undefined) {          
                MostrarmodalErrorVenta( "ERROR", "ERROR AL REGISTRAR LA ENTRADA", "La salida de inventario no ha podido ser registrada ya que no ha agregado ningún producto y no ha seleccionado el tipo de movimiento." );
                //OcultarModal( "No ha agregado ningún producto y no ha seleccionado el tipo de movimiento" );
            } else if( IdTipoMovimiento != "null" && textotabla == undefined ) {          
                MostrarmodalErrorVenta( "ERROR", "ERROR AL REGISTRAR LA ENTRADA", "La salida de inventario no ha agregado ningún producto." );
                //OcultarModal( "No ha agregado ningún producto" );
            } else if( IdTipoMovimiento != "null" && textotabla == "Aplica" ) {
                        
                var urlcaso;

                
                if( IdTipoMovimiento == "201" ) { //Entrega de pedido proveedor
                    urlcaso = "{{ route('insertardevolucioncompra') }}";
                } else {
                    urlcaso = "{{ route('insertarsalidainventario') }}";
                }
                

                //urlcaso = "{{ route('insertarsalidainventario') }}";

                $.ajaxSetup({
                headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
                });

                $.ajax({
                    type: "POST",
                    
                    //url: "{{ route('insertarentradainventario') }}",
                    url : urlcaso,
                    data: { TableData : TableData, IdTipoMovimiento : IdTipoMovimiento, DescripcionMovimiento : DescripcionMovimiento },
                    success: function(data){
                        LimpiarInputs();
                        MostrarmodalErrorVenta( "SUCCESS", "SALIDA DE INVENTARIO REGISTRADA", "El registro de la salida de inventario se ha realizado exitosamente." );             
                        document.getElementById("idtipomovimiento").value = "";
                        banderatipoentradaselected = false;

                        //setTimeout(() => { location.reload(); }, 2000 );

                    },
                    error:function()
                    { 
                        MostrarmodalErrorVenta( "ERROR", "ERROR AL REGISTRAR LA SALIDA DE INVENTARIO", "La salida de inventario no ha podido ser registrada, por favor revise todos los campos e intente otra vez." );
                    }
                });

            }

        }

        function MostrarmodalErrorVenta( caso, textoheader, textobody ) {
            
            $('#modalErrorVenta').modal('show');      
            document.getElementById("textostringheader").innerHTML = textoheader;
            document.getElementById("textoerrorbody").innerHTML = textobody;

            if( caso == "ERROR" ) {
                
                document.getElementById("modalheader").style.backgroundColor = "rgb(176, 28, 28)";
                document.getElementById("textostringheader").style.color = "rgb(255, 255, 255)";


            } else if( caso == "SUCCESS") {
                
                document.getElementById("modalheader").style.backgroundColor = "rgb(2, 102, 17)";
                document.getElementById("textostringheader").style.color = "rgb(255, 255, 255)";

            }    
            
        }

        function OcultarAlerta() {

            if( document.getElementById("contentalert").style.display == "none" ) {
                document.getElementById("contentalert").style.display = "block";
            } else {
                document.getElementById("contentalert").style.display = "none";
            }

        }

        function TipoMovimientoSeleccionado() {

            var selectmovimientos = document.getElementById("idtipomovimiento").value;
            tipomovimientoentradaseleccionado = ( parseInt( selectmovimientos ) );
            console.log("TIPOMOVIMIENTO: ", tipomovimientoentradaseleccionado );
            
            //document.getElementById("idtipomovimiento").disabled = true;
            banderatipoentradaselected = true;

            if( isNaN( tipomovimientoentradaseleccionado ) ) {
                banderatipoentradaselected = false;
            }

        }

    </script>


    @section('contenido')


        <!-- Datos del empleado y del tipo de movimiento -->
        <div class="container">            
            <div class="form-row">       

                    <!-- Nombre e id del vendedor -->
                <div class="col-3">        
                    <label for="nombrevendedor"> Nombre del empleado</label>
                    <input type="text" value="{{Auth::user()->name}}" name="nombrevendedor" id="nombrevendedor" class="form-control" readonly>
                    <input type="text" value="{{Auth::user()->id}}" name="idvendedor" id="idvendedor" class="form-control" style="display: none" disabled>
                </div> 

                <!-- SE ABRE EL MODAL QUE MUESTRA TODOS LOS PRODUCTOS -->
                <div class="col-auto" style="margin-top: 10px;">
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalEmergenteListadoProductos" style="margin-top: 10.5%" onclick="javascript:CargarListadoProductosModal()">Agregar Producto</button>
                </div>

                <!-- TIPO MOVIMIENTO -->             
                <div class="col-auto" style="margin-left: 30px;">
                    <label for="descripcionmovimiento"> Tipo de movimiento </label>
                    <select id="idtipomovimiento" class="form-control" oninvalid="setCustomValidity('Debe seleccionar un tipo de movimiento')"
                        oninput="setCustomValidity('')" onchange="javascript:TipoMovimientoSeleccionado();" required>
                        <option value="">Seleccione un tipo de movimiento</option>

                        @foreach( $movimientosinventario as $movimientos )
                        <option value="{{ $movimientos-> idtipomovimiento }}"> {{ $movimientos-> descripcion }} </option>
                        @endforeach

                    </select>
                </div>   
                
                <div class="col-2">
                    <div style="margin-left: 30px;">
                        <img width="120px;" id="imagenproducto">
                    </div>  
                </div> 

            </div>
        </div> 

            <!-- Alerta Modal Emergente Listado Productos -->
        <div class="modal" tabindex="-1" role="dialog" id="modalEmergenteListadoProductos">

            <div class="modal-dialog modal-dialog-centered modal-lg" role="document">

                <div class="modal-content">
                    
                    <!--Header-->
                    <div class="modal-header" id="modalheaderlistadoproductos" style="background: rgb(33, 37, 41)">
                        <h5 id="textostringheaderlistadoproductos" class="heading" style="margin-left: 35%">
                            <span style="color: rgb(255,255,255)"> LISTADO DE PRODUCTOS </span>
                        </h5>

                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span  aria-hidden="true" style="color: rgb(255, 255, 255)" >&times;</span>
                        </button>
                    </div>

                    <!--Body-->
                    <div class="modal-body" id="modalbodylistadoproductos">

                        <!-- DIV DESCRICPION PRODUCTO Y BUSCADOR -->
                        <div class="container">
                            <div class="row justify-content-center">

                                <!-- BUSCADOR -->
                                <div class="form-group row">
                                    <label for="listadoproductosdescripcion" style="margin-top: 10px;">Ingrese la descripción</label>
                                    <div class="col-auto">
                                        <input type="text" id="listadoproductosdescripcion" class="form-control" onkeydown="javascript:BuscarProductoDescripcionListadoProductosModalKD(event);">
                                    </div>
                                    <button type="submit" class="btn btn-primary mb-2" style="margin-right: 20px;" onclick="javascript:BuscarProductoDescripcionListadoProductosModal();">Buscar</button>
                                    
                                    <button type="submit" class="btn btn-info mb-2" onclick="javascript:RestablecerListaListadoProductosModal();">Restablecer</button>
                                                                        
                                </div>
                                                
                                <!-- Alerta Búsqueda ListadoProductos-->
                                <div class="container"  id="contentalertbusquedalistadoproductos" style="display: none;">
                                    <div class="alert alert-danger alert-dismissible" role="alert" style="margin-top: 1%" >
                                        <label id="textoalertbusquedalistadoproductos"> </label>
                                        <button type="button" class="close" onclick="javascript:OcultarAlertaBusquedaListadoProductos();">
                                            <span>&times;</span>
                                        </button>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                        
                        <!-- Tabla ListadoProductos -->
                        <div class="container">
                            <div class="row justify-content-center">
                                
                                <div class="scrollbar" id="scrollbar-style">
                                    
                                    <table class="table table-hover" style="width: 750px" name="tablalistadoproductos" id="tablalistadoproductos" ondblclick="javascript:ObtenerFilaListadoProductos(event)" >
                                        <thead  class="thead-dark">
                                            <tr>
                                                <th scope="col>"> Código producto </th>
                                                <th scope="col>"> Descripción producto </th>
                                                <th scope="col>"> Precio de venta </th>
                                            </tr>
                                        </thead> 
                                        <tbody>                        
                                            <tr>
                                            </tr>                     
                                        </tbody>  
                                    </table>

                                </div>

                            </div>
                        </div>
                        
                    </div>
                    
                    <!--Footer-->
                    <div class="modal-footer" id="modalfooterlistadoproductos">
                        <button type="button" class="btn btn-dark" data-dismiss="modal">Cerrar</button>
                    </div>

                </div>

            </div>

        </div>
        
            <!-- Alerta -->
        <div class="container"  id="contentalert" style="display: none;">
            <div class="alert alert-danger alert-dismissible" role="alert" style="margin-top: 1%" >
                <input class="form-control-plaintext" value = "" id="textoalerta" disabled>    
                <button type="button" class="close" onclick="javascript:OcultarAlerta();" style="margin-top: 5px">
                <span>&times;</span>
                </button>
            </div>
        </div>
            
            <!-- Alerta Modal ERRORVENTA -->
        <div class="modal" tabindex="-1" role="dialog" id="modalErrorVenta">

            <div class="modal-dialog modal-dialog-centered" role="document">

                <div class="modal-content">
                    <!--Header-->
                    <div class="modal-header" id="modalheader" style="background: rgb(33, 37, 41)">
                        <h5 id="textostringheader" class="heading" style="margin-left: 15%" style="color: rgb(251, 255, 247)"></h5>

                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span  aria-hidden="true" style="color: rgb(255, 255, 255)" >&times;</span>
                        </button>
                    </div>

                    <!--Body-->
                    <div class="modal-body" id="modalbody">

                        <div class="row">

                            <div class="col">
                                <p id="textoerrorbody"></p>
                            </div>

                        </div>
                        
                    </div>
                    
                    <!--Footer-->
                    <div class="modal-footer" id="modalfooter">
                        <button type="button" class="btn btn-dark" data-dismiss="modal">Cerrar</button>
                    </div>

                </div>

            </div>

        </div>

            <!-- Datos del producto a agregar"-->
        <div class="container" style="margin-top: 1%">
            <fieldset  class="border p-2">
                <legend  class="w-auto"> Datos del producto </legend>
                <div class="form-row">  
                        
                    <!-- Codigo -->
                    <div class="col-2">
                        <label for="codigo"> Código: </label>
                        <input type="text" name="codigo" id="codigo" class="form-control" onkeyup="onKeyUp(event)" autofocus>
                    </div>   
                            
                    <!-- Descripción -->
                    <div class="col-3" style="margin-left: 2%" >
                        <label for="descripcion"> Descripción: </label>
                        <input type="text" name="descripcion" id="descripcion" readonly class="form-control">
                    </div>   
                                                    
                    <!-- Cantidad -->
                    <div class="col-1" style="margin-left: 2%" >
                        <label for="cantidad"> Cantidad: </label>
                        <input type="number" name="cantidad" id="cantidad" min="1" readonly class="form-control" onkeyup="onKeyDownHandler();" onchange="CalcularImporte();"
                            onkeypress="return ( event.charCode >= 48 && event.charCode <= 57 )"
                        >
                    </div>   
                                                    
                    <!-- Presentación -->
                    <div class="col-2" style="margin-left: 2%" >
                        <label for="presentacion"> Presentación: </label>
                        <input type="text" name="presentacion" id="presentacion" readonly class="form-control">
                    </div>   
                                                                            
                    <!-- Precio de venta-->  
                    <div class="col-2" style="margin-left: 1%">
                        <label for="precio"> Precio de venta: </label>
                        <input type="text" name="precio" id="precio" class="form-control"
                            onkeypress="return ( event.charCode >= 48 && event.charCode <= 57 || event.charCode == 46 )"
                        >
                    </div>        
                                                                            
                    <!-- Precio de compra -->  
                    <div class="col-2">
                        <label for="preciocom"> Precio de compra: </label>
                        <input type="text" name="preciocom" id="preciocom" min="1" max="999999" step="0.01" class="form-control" onchange="CalcularImporte();" oninput="CalcularImporte();"
                                onkeypress="return ( event.charCode >= 48 && event.charCode <= 57 || event.charCode == 46 )"
                        >
                    </div>     
                                                    
                    <!-- Importe -->
                    <div class="col-2" style="margin-left: 2%" >
                        <label for="importe"> Importe: </label>
                        <input type="text" name="importe" id="importe" readonly class="form-control">
                    </div>   
                    
                    <div class="row" style="margin-left: 5%; margin-top:1%">
                        <div class="col">
                            <a class="btn btn-success" id="enlaceagregar" onclick="javascript:VerificarFilaDefault()" role="button">Agregar</a>
                                <span  style="color: rgb(255, 255, 255)">Agregar</span>
                            </a>
                        </div>
                        <div class="col">
                            <a class="btn btn-danger" id="enlaceeliminar"  onclick="javascript:EliminarFilaProducto()" role="button"  style="display: none;" >Eliminar</a>
                                <span></span>
                            </a>
                        </div>                     
                    </div>

                </div>
            </fieldset>
        </div>
        
            <!-- Tabla -->
        <div class="container" style="margin-top: 2%">
            <div class="row justify-content-center">
                
                <table class="table table-hover" name="tablaproductos" id="tablaproductos" ondblclick="javascript:ModificarFilaProducto(event)" >      
                    <thead  class="thead-dark">
                        <tr>
                            <th scope="col"> Código de barra </th>
                            <th scope="col"> Descripción </th>
                            <th scope="col"> Cantidad </th>
                            <th scope="col"> Precio de venta </th>
                            <th scope="col"> Precio de compra </th>
                            <th scope="col"> Presentación </th>
                            <th scope="col"> Importe </th>
                        </tr>
                        </tr>
                    </thead> 
                    <tbody>                        
                        <tr>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                        </tr>                     
                    </tbody>  
                </table>
                <!-- <button type="submit" class="btn btn-primary" id="enlaceenviar">Generar</button> -->    
                
            </div>
        </div>

        <br>

                <!-- Registrar Entrega Pedido -->
        <div class="container" style="margin-top: 2%">            
            <div class="row justify-content-center">    

                <div class="col-auto">
                    <label for="total" style="margin-top: 10%"> Total: </label>
                </div>

                <div class="col-auto">
                    <input type="text" name="total" id="total" class="form-control" readonly>
                </div>

                <div class="col-auto">
                    <a class="btn btn-primary form-control" id="enlaceenviar" onclick="javascript:EnviarDatosTabla()" role="button">Registrar Salida</a> 
                </div>        

            </div>
        </div>

        <br><br><br>

    @endsection

@endif