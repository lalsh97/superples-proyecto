@if( Auth::user()->Nivel == 1 )

    @extends('home')

    @section('title')
        Registro de categoría
    @endsection

    @section('contenido')


        @if(Session::has('success'))
            <div class="alert alert-success">
                {{session('success')}}
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            </div>
        @elseif(Session::has('danger'))
            <div class="alert alert-danger">
                {{session('danger')}}
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            </div>
        @elseif(Session::has('warning'))
            <div class="alert alert-warning">
                {{session('warning')}}
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            </div>
        @endif

        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header"  align="center">{{ __('Registro de categoría') }}</div>
                        <div class="card-body">
                            {!!Form::open(array('name' => 'formcategoria', 'url'=>'añadircategoria','method'=>'POST','files'=>true , 'enctype'=>'multipart/form-data','autocomplete'=>'off'))!!}
                                
                                <!-- Imagen de la categoría -->
                                <div class="form-group row">
                                    {!! Form::label( 'avatar','Imagen de la categoría', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                    <div class="col-md-6">
                                    <input type="file" id="avatar" name="avatar"  value="{{ old('avatar')}}" >
                                    </div>
                                </div>

                                <!-- Nombre de la categoría -->
                                <div class="form-group row">
                                    <label for="nombre" class="col-md-4 col-form-label text-md-right">{{ __('Nombre') }}</label>

                                    <div class="col-md-6">
                                    {!! Form::text('nombre', null, array( 'class' => 'form-control', 'placeholder' => 'Ingrese el nombre' ) ) !!}
                                    </div>
                                </div>
                                
                                <!-- Descripción de la categoría -->
                                <div class="form-group row">
                                    <label for="descripcion" class="col-md-4 col-form-label text-md-right">{{ __('Descripción') }}</label>
                                    <div class="col-md-6">
                                        {!! Form::text('descripcion', null, array( 'class' => 'form-control', 'placeholder' => 'Ingrese la descripción' ) ) !!}
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col">
                                        <button name="botonformcategoria" value="Registrar" type="submit" class="btn btn-success" style="margin-left:150px" style="margin-right:50px">Registrar</button>
                                        <button name="botonformcategoria" value="Cancelar" type="submit" class="btn btn-danger" style="margin-left:200px" style="margin-right:50px">Cancelar</button>
                                    </div>
                                </div>

                            {!!Form::close()!!}

                        </div>
                    </div>
                </div>
            </div>
        </div>

    @endsection

@endif