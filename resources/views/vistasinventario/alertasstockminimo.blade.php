@extends('home')

@section('title')
    Listado de alertas de existencias de inventario
@endsection

@section('contenido')
    
    @if(Session::has('success'))
        <div class="alert alert-success">
            {{session('success')}}
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        </div>
    @elseif( Session::has('warning'))
        <div class="alert alert-warning">
            {{session('warning')}}
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        </div>
    @elseif( Session::has('danger'))
        <div class="alert alert-danger">
            {{session('danger')}}
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        </div>
    @endif

    @if ( isset( $listaalertasinventario ) )
        <div class="container">
            <div class="row justify-content-center">

                <table class="table table-hover" id="tablalistaalertasinventario" name="tablalistaalertasinventario">
                    <thead  class="thead-dark">
                        <tr>
                            <th scope="col"> CÓDIGO PRODUCTO </th>
                            <th scope="col"> DESCRIPCIÓN PRODUCTO </th>
                            <th scope="col"> MARCA </th>
                            <th scope="col"> PRESENTACIÓN PRODUCTO </th>
                            <th scope="col"> STOCK MÍNIMO PERMITIDO </th>
                            <th scope="col"> CANTIDAD DE EXISTENCIAS </th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ( $listaalertasinventario as $alerta )
                            <tr>

                                <td>{{ $alerta-> codigoinventario }}</td>
                                <td>{{ $alerta-> descripcionproducto }}</td>
                                <td>{{ $alerta-> nombremarca }}</td>
                                <td>{{ $alerta-> presentacionproducto }}</td>
                                <td>{{ $alerta-> stockminimoproducto }}</td>
                                <td>{{ $alerta-> cantidadproducto }}</td>

                            </tr>
                        @endforeach
                    </tbody>
                </table>

            </div>
        </div>
    @endif

@endsection
