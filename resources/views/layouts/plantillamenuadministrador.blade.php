<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="description"
            content="Responsive sidebar template with sliding effect and dropdown menu based on bootstrap 3">
        <title> @yield('title') </title>

        <!-- using online links -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css"
            integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css"
            integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
        <link rel="stylesheet" href="//malihu.github.io/custom-scrollbar/jquery.mCustomScrollbar.min.css">


        <!-- using local links -->
        <!-- <link rel="stylesheet" href="../node_modules/bootstrap/dist/css/bootstrap.min.css">
        <link rel="stylesheet" href="../node_modules/@fortawesome/fontawesome-free/css/all.min.css">
        <link rel="stylesheet" href="../node_modules/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.css"> -->

        <link rel="stylesheet" href="{{ asset('css/main.css') }}">
        <link rel="stylesheet" href="{{ asset('css/sidebar-themes.css') }}">
        <link rel="stylesheet" href="{{ asset('css/estilos.css') }}">
    </head>
    <body>
        <script>

            function imgError( image ) {

                var urldef = "/images/Usuarios/AvatarUserDefault.png";
                image.src = urldef;

            }

        </script>
            <!--     <div class="page-wrapper default-theme sidebar-bg bg1 toggled">     -->
        <div class="page-wrapper legacy-theme">
                <!-- Icono menú desplegable  -->
            <nav class="navbar navbar-expand" style="background-color: orange" style="padding-bottom: 20%">
                <main class="page-content pt-2" style="background-color: white">
                    <div class="form-group" style="margin-left: 1%">
                        <a id="toggle-sidebar" class="btn btn-default rounded-0" href="#">
                            <span class="fas fa-bars"></span>
                        </a>
                        <a href="/home">
                            <img src="{{ asset('images/LogoPLES.png') }}" width="13%" height="10%" class="img-rounded" style="margin-left: 80%" style="background-color: white">
                        </a>
                    </div>
                </main>
            </nav>
            @if ($errors->any())
                <div class="alert alert-danger">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <br> <br>
            @section('contenido')
            @show


                <!-- Menú desplegable  -->
            <nav id="toogle-sidebar" class="sidebar-wrapper">
                <div class="sidebar-content">
                    <!-- sidebar-brand Menu despegable -->
                    <div class="sidebar-item sidebar-brand">
                        <a align="center">Menú</a>
                    </div>
                    <!-- sidebar-header  -->
                    <div class="sidebar-item sidebar-header d-flex flex-nowrap">
                        <div class="user-pic">
                            <img class="img-responsive img-rounded" src="/images/{{ ( Auth::user()->avatar ) }}" onerror="imgError( this );" alt="User picture">
                        </div>
                        <div class="user-info">
                            <span class="user-name">
                                @if( Auth::user()->Nivel ==1 )
                                    Tipo: {{ 'Administrador' }}
                                @elseif( Auth::user()->Nivel ==2 )
                                    Tipo: {{ 'Vendedor' }}
                                @endif
                            </span>
                            <span class="user-role">{{ Auth::user()->name }}</span>
                        </div>
                    </div>

                    <!-- sidebar-menu  -->
                    <div class=" sidebar-item sidebar-menu">
                        <ul>
                            <li class="header-menu">
                                <span>Opciones</span>
                            </li>

                            <!--   Opciones Empleado -->
                            <li class="sidebar-dropdown">
                                <a href="#">
                                    <i class="fas fa-user" aria-hidden="true"></i>
                                    <span class="menu-text">Empleados</span>
                                </a>
                                <div class="sidebar-submenu">
                                    <ul>
                                        <li>
                                            <a href="{{ route('registrarempleado') }}">
                                                <span class="fas fa-user-plus" style="font-size: 15px"> Registrar empleado</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ route('busquedaempleado','modificar') }}">
                                                <span class="fas fa-user-edit" style="font-size: 15px"> Modificar información</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ route('listadoempleados') }}">
                                                <span class="fas fa-clipboard" style="font-size: 15px" >  Mostrar empleados</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ route('busquedaempleado','descontinuar') }}">
                                                <span class="fas fa-user-minus" style="font-size: 15px"> Eliminar empleado</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </li>

                            <!--   Opciones productos -->
                            <li class="sidebar-dropdown">
                                <a href="#">
                                    <i class="fas fa-boxes" aria-hidden="true"></i>
                                    <span class="menu-text">Productos</span>
                                </a>
                                <div class="sidebar-submenu">
                                    <ul>
                                        <li>
                                            <a href="{{ route('registrarproductos') }}">
                                                <span class="fas fa-plus-circle" style="font-size: 15px"> Registrar producto</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ route('busquedaproducto','modificar') }}">
                                                <span class="fas fa-edit" style="font-size: 15px"> Modificar información</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ route('buscarproducto') }}">
                                                <span class="fas fa-search" style="font-size: 15px" > Buscar producto</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ route('listadoproductos') }}">
                                                <span class="fas fa-clipboard" style="font-size: 15px" >  Mostrar productos</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ route('busquedaproducto','descontinuar') }}">
                                                <span class="fas fa-minus-circle" style="font-size: 15px"> Descontinuar producto</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ route('codigobarrasproducto') }}">
                                                <span class="fa fa-barcode" style="font-size: 15px"> Generación código barra</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </li>

                            <!--   Opciones promociones -->
                            <li class="sidebar-dropdown">
                                <a href="#">
                                    <i class="fas fa-gifts" aria-hidden="true"></i>
                                    <span class="menu-text">Promociones</span>
                                </a>
                                <div class="sidebar-submenu">
                                    <ul>
                                        <li>
                                        <a href="{{ route('selecciondeproducto') }}">
                                                <span class="fas fa-plus-circle" style="font-size: 15px"> Registrar promoción</span>
                                            </a>
                                        </li>
                                        <li>

                                            <a href="{{ route('modificarpromociones') }}">
                                                <span class="fas fa-edit" style="font-size: 15px"> Modificar promoción</span>

                                            </a>
                                        </li>
                                        <!--
                                        <li>
                                            <a href="#">
                                                <span class="fas fa-search" style="font-size: 15px" >  Buscar promoción</span>
                                            </a>
                                        </li>
                                        -->
                                        <li>
                                            <a href="{{ route('listapromociones') }}">
                                                <span class="fas fa-clipboard" style="font-size: 15px" > Mostrar promociones</span>
                                            </a>
                                        </li>
                                        <li>

                                            <a href="{{ route('listadopromoterminar') }}">
                                                <span class="fas fa-times-circle" style="font-size: 15px"> Terminar promoción</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </li>

                                <!--   Opciones proveedores -->
                            <li class="sidebar-dropdown">
                                <a href="#">
                                    <i class="fas fa-chalkboard-teacher"></i>
                                    <span class="menu-text">Proveedores</span>
                                </a>
                                <div class="sidebar-submenu">
                                    <ul>
                                        <li>
                                            <a href="{{ route('registrarproveedor') }} ">
                                                <span class="fas fa-user-plus" style="font-size: 15px">  Registrar proveedor</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ route('busquedaproveedor','modificar') }}">
                                                <span class="fas fa-user-edit" style="font-size: 15px"> Modificar proveedor</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ route('listadoproveedores') }}">
                                                <span class="fas fa-address-book" style="font-size: 15px"> Mostrar proveedores</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ route('busquedaproveedor','buscar') }}">
                                                <span class="fas fa-search" style="font-size: 15px"> Buscar proveedor</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ route('busquedaproveedor','descontinuar') }}">
                                                <span class="fas fa-user-minus" style="font-size: 15px"> Eliminar proveedor</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ route('registrarentregapedido') }}">
                                                <span class="fas fa-truck-loading" style="font-size: 15px"> Registrar compra</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ route('registrardevolucioncompra') }}">
                                                <span class="fas fa-truck" style="font-size: 15px"> Registrar devolución de compra</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </li>

                                <!--   Opciones clientes -->
                            <li class="sidebar-dropdown">
                                <a href="#">
                                    <i class="far fa-address-card"></i>
                                    <span class="menu-text">Clientes</span>
                                </a>
                                <div class="sidebar-submenu">
                                    <ul>
                                        <li>
                                        <a href="{{ route('registrarcliente') }}">
                                            <span class="fas fa-user-plus" style="font-size: 15px">  Registrar cliente</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ route('busquedacliente','modificar') }}">
                                                <span class="fas fa-user-edit" style="font-size: 15px"> Modificar cliente</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ route('listadoclientes') }}">
                                                <span class="fas fa-address-book" style="font-size: 15px"> Mostrar clientes</span>
                                            </a>
                                        </li>
                                        <li>
                                        <a href="{{ route('busquedacliente','buscar') }}">
                                                <span class="fas fa-search" style="font-size: 15px"> Buscar cliente</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ route('busquedacliente','descontinuar') }}">
                                                <!-- <span class="fas fa-user-slash" style="font-size: 15px"> Eliminar cliente</span> -->
                                                <span class="fas fa-user-slash" style="font-size: 15px"> Cancelar crédito</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ route('listadocreditossincredito') }}">
                                                <span class="fas fa-clipboard-check" style="font-size: 15px"> Aprobar crédito cliente</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ route('listadocreditosclientes') }}">
                                                <span class="fas fa-money-check-alt" style="font-size: 15px"> Realizar abono crédito</span>
                                            </a>
                                        </li>

                                    </ul>
                                </div>
                            </li>

                                <!--   Opciones ventas -->
                            <li class="sidebar-dropdown">
                                <a href="#">
                                    <i class="fa fa-shopping-cart"></i>
                                    <span class="menu-text">Venta</span>
                                </a>
                                <div class="sidebar-submenu">
                                    <ul>
                                        <li>
                                            <a href="{{ route('registrarventa') }}">
                                            <span class="fas fa-cart-plus" style="font-size: 15px"> Nueva venta</span>
                                        </a>
                                        </li>
                                        <li>
                                            <a href="{{ route('listadoventas') }}">
                                                <span class="fas fa-address-book" style="font-size: 15px"> Mostrar ventas</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ route('busquedaventa') }}">
                                                <span class="fas fa-search" style="font-size: 15px"> Buscar venta</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ route('cortediacaja') }}">
                                                <span class="fas fa-paste" style="font-size: 15px"> Corte de caja</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ route('cortediaventas') }}">
                                                <span class="fas fa-file-alt" style="font-size: 15px"> Corte de caja - ventas</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ route('cortediacompras') }}">
                                                <span class="fas fa-file-alt" style="font-size: 15px"> Corte de caja - compras</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </li>

                                <!--   Opciones inventario -->
                            <li class="sidebar-dropdown">
                                <a href="#">
                                    <i class="fas fa-warehouse"></i>
                                    <span class="menu-text">Inventario</span>
                                </a>
                                <div class="sidebar-submenu">
                                    <ul>
                                        <li>
                                            <a href="{{ route('registrarentradainventario') }}">
                                                <span class="fas fa-arrow-alt-circle-right" style="font-size: 15px"> Entrada a inventario</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ route('registrarsalidainventario') }}">
                                                <span class="fas fa-arrow-alt-circle-left" style="font-size: 15px"> Salida de inventario</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ route('catalogoproductos') }}">
                                                <span class="fas fa-clipboard-list" style="font-size: 15px"> Catálogo de productos</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ route('listadomovimientosinventario') }}">
                                                <span class="fas fa-people-carry" style="font-size: 15px"> Movimientos de inventario</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </li>

                                <!--   Opciones reportes -->
                            <li class="sidebar-dropdown">
                                <a href="#">
                                    <i class="fas fa-file-invoice-dollar"></i>
                                    <span class="menu-text">Reportes</span>
                                </a>
                                <div class="sidebar-submenu">
                                    <ul>
                                        <li>
                                            <a href="{{ route('reportes') }}">
                                                <span class="fas fa-coins" style="font-size: 15px"> Reportes</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </li>

                                <!-- Extra Separación -->
                            <li class="header-menu">
                                <span>Extra</span>
                            </li>
                                <!-- Extra -->
                            <li>
                                <a href="{{ route('manualdeusuario') }}">
                                    <i class="fa fa-book"></i>
                                    <span class="menu-text">Manual de usuario</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <!-- sidebar-menu  -->
                </div>
                <!-- sidebar-footer  -->
                <div class="sidebar-footer">

                    <div class="dropdown">
                        <a href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-cog"></i>
                            <!-- <span class="badge-sonar"></span> -->
                        </a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuMessage">
                            <a class="dropdown-item" href="{{ route('importardatos') }}"> Importar Datos</a>
                            <a class="dropdown-item" href="{{ route('exportardatos') }}"> Exportar Datos</a>
                            <a class="dropdown-item" href="{{ route('modificarinformacionusuario') }}"> Modificar Información Cuenta</a>
                            <a class="dropdown-item" href="{{ route('menuprincipalreactivar') }}"> Menú Reactivar</a>
                            <a class="dropdown-item" href="{{ route('listadoempleadoscambioinformacionusuario') }}"> Modificar Cuentas Empleados</a>
                        </div>
                    </div>

                    <div>
                        <a href="{{ route('logout') }}" onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">
                        <i class="fa fa-power-off"></i>
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </div>

                </div>
            </nav>
            <!-- page-content" -->
        </div>
        <!-- using online scripts -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js" ></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"
            integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut"  crossorigin="anonymous">
        </script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"
            integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous">
        </script>
        <script src="//malihu.github.io/custom-scrollbar/jquery.mCustomScrollbar.concat.min.js" ></script>
        <!-- using local scripts -->
        <!-- <script src="../node_modules/jquery/dist/jquery.min.js"></script>
        <script src="../node_modules/popper.js/dist/umd/popper.min.js"></script>
        <script src="../node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="../node_modules/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script> -->
        <script src="{{ asset('js/main.js') }}"></script>

    </body>

</html>
