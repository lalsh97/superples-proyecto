@if( Auth::user()->Nivel == 1 )
  
    @extends('home')

    @section('title')
        Listado de productos desactivados
    @endsection

    @section('contenido')


        <link rel="STYLESHEET" href="css/estilostooltipylistados.css">


        <script>

            var codigobarras = "";

            function FiltrarTablaCodigo() {
                
                if( document.getElementById("codigofiltrado").value != "" ) {
                    
                    RealizarFiltradoTablaCodigo( document.getElementById("codigofiltrado").value );

                } else {
                            
                    document.getElementById("textoalerta").value = "Por favor ingrese el código de barras a buscar";
                    document.getElementById("contentalert").style.display = "block";

                }

            }

            function FiltrarTablaDescripcion() {

                
                if( document.getElementById("descripcionfiltrado").value != "" ) {
                    
                    RealizarFiltradoTablaDescripcion( document.getElementById("descripcionfiltrado").value );

                } else {
                            
                    document.getElementById("textoalerta").value = "Por favor ingrese la descripción del producto a buscar";
                    document.getElementById("contentalert").style.display = "block";

                }

            }

            function RealizarFiltradoTablaCodigo( codigo ) {

                document.getElementById("descripcionfiltrado").value = "";
                document.getElementById("resultadofiltradodescripcion").innerText = "";

                var arrayregistroproductos = {!! json_encode( $registroproductos->toArray(), JSON_HEX_TAG ) !!};

                VaciarTabla();

                var auxcodigoproducto = "";

                for( var i = 0; i < arrayregistroproductos.length; i++ ) {

                    auxcodigoproducto = arrayregistroproductos[i].codigo;

                    if( auxcodigoproducto == codigo ) {

                        var textoiva = "";

                        if( arrayregistroproductos[i].iva == 0 ) {
                            textoiva = "NO";
                        } else {
                            textoiva = "SI";
                        }
                        
                        document.getElementById("tablaproductos").insertRow(-1).innerHTML = 
                            "<td>" + arrayregistroproductos[i].codigo + "</td>" +
                            "<td>" + arrayregistroproductos[i].nombremarca + "</td>" +
                            "<td>" + arrayregistroproductos[i].nombrepresentacion + "</td>" +
                            "<td>" + arrayregistroproductos[i].nombrecompleto + "</td>" +
                            "<td>" + arrayregistroproductos[i].nombrecategoria + "</td>" +
                            "<td>" + arrayregistroproductos[i].descripcion + "</td>" +
                            "<td>" + arrayregistroproductos[i].preciocosto + "</td>" +
                            "<td>" + arrayregistroproductos[i].precioventa + "</td>" +
                            "<td>" + textoiva + "</td>";

                    }
                
                }
                
                const tabla = document.getElementById('tablaproductos');
                var rowCount = tabla.rows.length;

                if( rowCount == 1 ) {
                            
                    document.getElementById("textoalerta").value = "Ningún producto coincide con el código buscado";
                    document.getElementById("contentalert").style.display = "block";


                } else {
                    
                    document.getElementById("resultadofiltradocodigo").innerHTML = "<p> Total de coincidencias: <strong>"+ ( rowCount - 1) + "</strong> </p>";

                }


            }

            function RealizarFiltradoTablaDescripcion( descripcion ) {

                document.getElementById("codigofiltrado").value = "";
                document.getElementById("resultadofiltradocodigo").innerText = "";

                var arrayregistroproductos = {!! json_encode( $registroproductos->toArray(), JSON_HEX_TAG ) !!};

                VaciarTabla();

                var auxcodigoproducto = "";

                for( var i = 0; i < arrayregistroproductos.length; i++ ) {

                    auxdescripcionproducto = arrayregistroproductos[i].descripcion;
                    auxdescripcionproducto = auxdescripcionproducto.toUpperCase();
                    descripcion = descripcion.toUpperCase();

                    if( auxdescripcionproducto.includes( descripcion ) ) {

                        var textoiva = "";

                        if( arrayregistroproductos[i].iva == 0 ) {
                            textoiva = "NO";
                        } else {
                            textoiva = "SI";
                        }                    

                        document.getElementById("tablaproductos").insertRow(-1).innerHTML = 
                            "<td>" + arrayregistroproductos[i].codigo + "</td>" +
                            "<td>" + arrayregistroproductos[i].nombremarca + "</td>" +
                            "<td>" + arrayregistroproductos[i].nombrepresentacion + "</td>" +
                            "<td>" + arrayregistroproductos[i].nombrecompleto + "</td>" +
                            "<td>" + arrayregistroproductos[i].nombrecategoria + "</td>" +
                            "<td>" + arrayregistroproductos[i].descripcion + "</td>" +
                            "<td>" + arrayregistroproductos[i].preciocosto + "</td>" +
                            "<td>" + arrayregistroproductos[i].precioventa + "</td>" +
                            "<td>" + textoiva + "</td>";

                    }
                
                }
                
                const tabla = document.getElementById('tablaproductos');
                var rowCount = tabla.rows.length;

                if( rowCount == 1 ) {
                            
                    document.getElementById("textoalerta").value = "Ningún producto coincide con la descripción buscada";
                    document.getElementById("contentalert").style.display = "block";


                } else {
                    
                    document.getElementById("resultadofiltradodescripcion").innerHTML = "<p> Total de coincidencias: <strong>"+ ( rowCount - 1) + "</strong> </p>";

                }


            }

            function VaciarTabla() {
                
                document.getElementById("codigofiltrado").value = "";
                document.getElementById("resultadofiltradocodigo").innerHTML = "";

                document.getElementById("descripcionfiltrado").value = "";
                document.getElementById("resultadofiltradodescripcion").innerHTML = "";
                

                const tabla = document.getElementById('tablaproductos');
                var rowCount = tabla.rows.length;         

                for  ( var x = rowCount - 1; x > 0; x-- ) { 
                    tabla.deleteRow(x); 
                } 

            }

            function RestablecerTabla() {

                document.getElementById("codigofiltrado").value = "";
                document.getElementById("descripcionfiltrado").value = "";
                
                document.getElementById("resultadofiltradocodigo").innerText = "";
                document.getElementById("resultadofiltradodescripcion").innerText = "";
            
                var arrayregistroproductos = {!! json_encode( $registroproductos->toArray(), JSON_HEX_TAG ) !!};
                
                VaciarTabla();

                for( var i = 0; i < arrayregistroproductos.length; i++ ) {

                    var textoiva = "";

                    if( arrayregistroproductos[i].iva == 0 ) {
                        textoiva = "NO";
                    } else {
                        textoiva = "SI";
                    }                

                    document.getElementById("tablaproductos").insertRow(-1).innerHTML = 
                        "<td>" + arrayregistroproductos[i].codigo + "</td>" +
                        "<td>" + arrayregistroproductos[i].nombremarca + "</td>" +
                        "<td>" + arrayregistroproductos[i].nombrepresentacion + "</td>" +
                        "<td>" + arrayregistroproductos[i].nombrecompleto + "</td>" +
                        "<td>" + arrayregistroproductos[i].nombrecategoria + "</td>" +
                        "<td>" + arrayregistroproductos[i].descripcion + "</td>" +
                        "<td>" + arrayregistroproductos[i].preciocosto + "</td>" +
                        "<td>" + arrayregistroproductos[i].precioventa + "</td>" +
                        "<td>" + textoiva + "</td>";
                
                }

            }

            function OcultarAlerta() {

                if( document.getElementById("contentalert").style.display == "none" ) {
                    document.getElementById("contentalert").style.display = "block";
                } else {
                    document.getElementById("contentalert").style.display = "none";
                }

            }

            function FilaSeleccionada() {
                
                var table = document.getElementById('tablaproductos'),
                selected = table.getElementsByClassName('selected');
                
                if (selected[0]) selected[0].className = '';
                event.target.parentNode.className = 'selected';

                codigobarras = $("tr.selected td:first" ).html();
                var descripcionproducto = $("tr.selected td:nth-child(6)").html();

                MostrarModalConfirmacionReactivacion( descripcionproducto, codigobarras );
            }
            
            function MostrarModalConfirmacionReactivacion( descripcionproducto, codigobarras) {

                document.getElementById("textoerrorbody").innerHTML =   "<p>¿Está seguro que desea reactivar al empleado: <strong>" + descripcionproducto + "</strong> con código: <strong>" + codigobarras + "</strong>? </p>";
                $('#ModalConfirmacionReactivacion').modal('show');

            }

            function DismissModalConfirmacionReactivacion() {

                $("#ModalConfirmacionReactivacion").modal('hide');

            }

            function ReactivarProducto() {

                $("#ModalConfirmacionReactivacion").modal('hide');            

                location.href ="/reactivarproducto/" + codigobarras;

            }

        </script>

        @if(Session::has('success'))
            <div class="alert alert-success">
                {{session('success')}}
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            </div>
        @elseif(Session::has('danger'))
            <div class="alert alert-danger">
                {{session('danger')}}
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            </div>
        @elseif(Session::has('warning'))
            <div class="alert alert-warning">
                {{session('warning')}}
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            </div>
        @endif

        

            <!-- DIV FILTRADO -->
        <div class="container" style="margin-bottom: 10px;">
            
            <!-- DIV FILTRAR POR CODIGO -->
            <div class="form-row align-items-center">

                <!-- FILTRAR POR CODIGO -->
                <div class="col" style="margin-left: 15%; margin-bottom:10px;">
                    <label for="codigofiltrado"> Ingrese el código de barras a buscar </label>
                    <input type="text" class="form-control-sm" id="codigofiltrado" style="margin-left: 10px;">
                    
                    <a style="margin-left: 5%;"> <button  class="btn btn-primary" id="botonfiltradocodigo"    onclick="javascript:FiltrarTablaCodigo();">Filtrar por código</button> </a>
                    
                    <label id="resultadofiltradocodigo" style="margin-left: 20px;"> </label>
                </div>
                
            </div>

            <!-- DIV FILTRAR POR DESCRIPCION -->
            <div class="form-row align-items-center">
                

                <!-- FILTRAR POR DESCRIPCION -->
                <div class="col" style="margin-left: 15%; margin-bottom:10px;">
                    <label for="descripcionfiltrado"> Ingrese la descripción a buscar </label>
                    <input type="text" class="form-control-sm" id="descripcionfiltrado" style="margin-left: 45px;">
                    
                    <a style="margin-left: 5%;"> <button  class="btn btn-primary" id="botonfiltradodescripcion"    onclick="javascript:FiltrarTablaDescripcion();">Filtrar por descripción</button> </a>
                    
                    <label id="resultadofiltradodescripcion" style="margin-left: 20px;"> </label>
                </div>

            </div>


            <!-- Botones -->
            <div class="form-row align-items-center" style="margin-top: 10px; margin-bottom: 5px;">
                <a style="margin-left: 40%;">  <button  class="btn btn-primary" id="botonrestablecer" onclick="javascript:RestablecerTabla();">Restablecer Tabla</button> </a>
            </div>
            
            
        </div>

            <!-- Alerta -->
        <div class="container"  id="contentalert" style="display: none;">
            <div class="alert alert-danger alert-dismissible" role="alert" style="margin-top: 1%" >
                <input class="form-control-plaintext" value = "" id="textoalerta" disabled>    
                <button type="button" class="close" onclick="javascript:OcultarAlerta();" style="margin-top: 5px">
                <span>&times;</span>
                </button>
            </div>
        </div>
        
            <!-- TOOLTIPTEXT -->
        <div class="container" style="margin-top: 10px;">
            
            <div class="row justify-content-center">
                <label class="textotooltip">
                    HAGA DOBLE CLICK SOBRE UN PRODUCTO PARA REACTIVARLO
                </label>
            </div>
        </div>


        <!-- Tabla -->
        <div class="container">
            <div class="row justify-content-center">

                <br> <br> <br>

                <table class="table table-hover" id="tablaproductos" name="tablaproductos" ondblclick="javascript:FilaSeleccionada();">
                    <thead  class="thead-dark">
                        <tr>
                            <th scope="col>"> Código de barras </th>
                            <th scope="col>"> Marca </th>
                            <th scope="col>"> Presentación </th>
                            <th scope="col>"> Nombre del proveedor </th>
                            <th scope="col>"> Categoria </th>
                            <th scope="col>"> Descripción </th>
                            <th scope="col>"> Precio de costo </th>
                            <th scope="col>"> Precio de venta </th>
                            <th scope="col>"> IVA </th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ( $registroproductos as $productos )
                            <tr>
                                <td>{{ $productos-> codigo }}</td>
                                <td>{{ $productos-> nombremarca }}</td>
                                <td>{{ $productos-> nombrepresentacion }}</td>
                                <td>{{ $productos-> nombrecompleto }}</td>
                                <td>{{ $productos-> nombrecategoria }}</td>
                                <td>{{ $productos-> descripcion }}</td>
                                <td>{{ $productos-> preciocosto }}</td>
                                <td>{{ $productos-> precioventa }}</td>
                                <td>
                                    @if($productos->iva == 0 )
                                        {{ "NO" }}
                                    @else
                                        {{ "SI" }}
                                    @endif
                                </td>

                            </tr>
                        @endforeach
                    </tbody>
                </table>

            </div>
        </div>
            
        <!-- ModalConfirmacionReactivación -->
        <div class="modal" tabindex="-1" role="dialog" id="ModalConfirmacionReactivacion">
            <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                <div class="modal-content modal-lg">

                    <!--Header-->
                    <div class="modal-header" id="modalheader" style="background: rgb(33, 37, 41)">
                        <h5 id="textostringheader" class="heading" style="margin-left: 25%; color: white; text-align: center;">CONFIRMACIÓN DE REACTIVACIÓN</h5>

                        <button type="button" class="close" onclick="javascript:DismissModalConfirmacionReactivacion();" data-dismiss="modal" aria-label="Close">
                            <span  aria-hidden="true" style="color: rgb(255, 255, 255)" >&times;</span>
                        </button>
                    </div>

                    <!--Body-->
                    <div class="modal-body" id="modalbody">
                        <div class="row">
                            <div class="col">
                                <p id="textoerrorbody" style="text-align: center;"></p>
                            </div>
                        </div>

                    </div>

                    <!--Footer-->
                    <div class="modal-footer" id="modalfooter">
                        <button type="button" class="btn btn-primary" data-dismiss="modal" id="botonEliminar" onclick="javascript:ReactivarProducto();">Reactivar</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal" id="botonCancelar" onclick="javascript:DismissModalConfirmacionReactivacion();" style="margin-left: 72%;">CANCELAR</button>
                    </div>

                </div>
            </div>
        </div>

    @endsection

@endif

