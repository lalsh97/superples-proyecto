@if( Auth::user()->Nivel == 1 )
    
    @extends('home')

    @section('title')
        Menú principal reactivar
    @endsection

    @section('contenido')

        @if(Session::has('success'))
            <div class="alert alert-success">
                {{session('success')}}
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            </div>
        @elseif(Session::has('danger'))
            <div class="alert alert-danger">
                {{session('danger')}}
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            </div>
        @elseif(Session::has('warning'))
            <div class="alert alert-warning">
                {{session('warning')}}
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            </div>
        @endif

        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header"  align="center">{{ __('Opciones del menú reactivar') }}</div>
                            <div class="card-body">
                                
                                <!-- REACTIVAR EMPLEADOS -->
                                <div class="form-group" style="margin-right: 15%;">                                    
                                    <label class="col-md-8 col-form-label text-md-left" style="margin-right: 12.5%;">Reactivar empleados</label>
                                    <a href="{{ route('listadoempleadosdesactivados') }}" > <button type="button" class="btn btn-success btn-sm">Reactivar</button> </a>
                                </div> 
                                
                                <!-- REACTIVAR PRODUCTOS -->
                                <div class="form-group" style="margin-right: 15%;">                                    
                                    <label class="col-md-8 col-form-label text-md-left" style="margin-right: 12.5%;">Reactivar productos</label>
                                    <a href="{{ route('listadoproductosdesactivados') }}" > <button type="button" class="btn btn-success btn-sm">Reactivar</button> </a>
                                </div>
                                
                                <!-- REACTIVAR PROVEEDORES -->
                                <div class="form-group" style="margin-right: 15%;">                                    
                                    <label class="col-md-8 col-form-label text-md-left" style="margin-right: 12.5%;">Reactivar proveedores</label>
                                    <a href="{{ route('listadoproveedoresdesactivados') }}" > <button type="button" class="btn btn-success btn-sm">Reactivar</button> </a>
                                </div>
                                
                                <!-- REACTIVAR CLIENTES -->
                                <div class="form-group" style="margin-right: 15%;">                                    
                                    <label class="col-md-8 col-form-label text-md-left" style="margin-right: 12.5%;">Reactivar clientes</label>
                                    <a href="{{ route('listadoclientesdesactivados') }}" > <button type="button" class="btn btn-success btn-sm">Reactivar</button> </a>
                                </div>

                            </div>
                    </div>
                </div>
            </div>
        </div>

    @endsection

@endif