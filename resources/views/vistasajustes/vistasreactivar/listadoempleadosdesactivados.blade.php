@if( Auth::user()->Nivel == 1 )

    @extends('home')

    @section('title')
        Listado de empleados desactivados
    @endsection

    @section('contenido')


        <link rel="STYLESHEET" href="css/estilostooltipylistados.css">


        <script>

            var idempleado = "";

            function FiltrarTabla() {
                
                if( document.getElementById("nombrefiltrado").value != "" ) {
                    
                    RealizarFiltradoTabla( document.getElementById("nombrefiltrado").value );

                } else {
                            
                    document.getElementById("textoalerta").value = "Por favor ingrese el nombre a buscar";
                    document.getElementById("contentalert").style.display = "block";

                }

            }

            function RealizarFiltradoTabla( nombre ) {
            
                var arrayregistroempleados = {!! json_encode( $registroempleados->toArray(), JSON_HEX_TAG ) !!};

                VaciarTabla();

                var auxnombreempleado = "";

                for( var i = 0; i < arrayregistroempleados.length; i++ ) {

                    auxnombreempleado = arrayregistroempleados[i].nombrecompleto;
                    auxnombreempleado = auxnombreempleado.toUpperCase();
                    nombre = nombre.toUpperCase();

                    if( auxnombreempleado.includes( nombre ) ) {
                                            
                        document.getElementById("tablaempleados").insertRow(-1).innerHTML = 
                                "<td>" + arrayregistroempleados[i].idempleado + "</td>" +
                                "<td>" + arrayregistroempleados[i].nombrecompleto + "</td>" +
                                "<td>" + arrayregistroempleados[i].curp + "</td>" +
                                //"<td>" + arrayregistroempleados[i].rfc + "</td>" +
                                "<td>" + arrayregistroempleados[i].fechanac + "</td>" +
                                //"<td>" + arrayregistroempleados[i].sexo + "</td>" +
                                "<td>" + arrayregistroempleados[i].telefono + "</td>" +
                                "<td>" + arrayregistroempleados[i].correo + "</td>";

                    }
                
                }
                
                const tabla = document.getElementById('tablaempleados');
                var rowCount = tabla.rows.length;

                if( rowCount == 1 ) {
                            
                    document.getElementById("textoalerta").value = "Ningún empleado coincide con el nombre buscado";
                    document.getElementById("contentalert").style.display = "block";


                } else {
                    
                    document.getElementById("resultadofiltrado").innerHTML = "<p> Total de coincidencias: <strong>"+ ( rowCount - 1) + "</strong> </p>";

                }


            }

            function VaciarTabla() {

                document.getElementById("nombrefiltrado").value = "";
                document.getElementById("resultadofiltrado").innerHTML = "";

                const tabla = document.getElementById('tablaempleados');
                var rowCount = tabla.rows.length;         

                for  ( var x = rowCount - 1; x > 0; x-- ) { 
                    tabla.deleteRow(x); 
                } 

            }

            function RestablecerTabla() {
            
                var arrayregistroempleados = {!! json_encode( $registroempleados->toArray(), JSON_HEX_TAG ) !!};
                
                VaciarTabla();

                for( var i = 0; i < arrayregistroempleados.length; i++ ) {
                                        
                    document.getElementById("tablaempleados").insertRow(-1).innerHTML = 
                            "<td>" + arrayregistroempleados[i].idempleado + "</td>" +
                            "<td>" + arrayregistroempleados[i].nombrecompleto + "</td>" +
                            "<td>" + arrayregistroempleados[i].curp + "</td>" +
                            //"<td>" + arrayregistroempleados[i].rfc + "</td>" +
                            "<td>" + arrayregistroempleados[i].fechanac + "</td>" +
                            //"<td>" + arrayregistroempleados[i].sexo + "</td>" +
                            "<td>" + arrayregistroempleados[i].telefono + "</td>" +
                            "<td>" + arrayregistroempleados[i].correo + "</td>";
                
                }

            }

            function OcultarAlerta() {

                if( document.getElementById("contentalert").style.display == "none" ) {
                    document.getElementById("contentalert").style.display = "block";
                } else {
                    document.getElementById("contentalert").style.display = "none";
                }

            }

            function FilaSeleccionada() {
                
                var table = document.getElementById('tablaempleados'),
                selected = table.getElementsByClassName('selected');
                
                if (selected[0]) selected[0].className = '';
                event.target.parentNode.className = 'selected';

                idempleado = $("tr.selected td:first" ).html();
                var nombreempleado = $("tr.selected td:nth-child(2)").html();

                MostrarModalConfirmacionReactivacion( nombreempleado );
            }
            
            function MostrarModalConfirmacionReactivacion( nombreempleado ) {

                document.getElementById("textoerrorbody").innerHTML =   "<p>¿Está seguro que desea reactivar al empleado: <strong>" + nombreempleado + "</strong> ? </p>";
                $('#ModalConfirmacionReactivacion').modal('show');

            }

            function DismissModalConfirmacionReactivacion() {

                $("#ModalConfirmacionReactivacion").modal('hide');

            }

            function ReactivarEmpleado() {

                $("#ModalConfirmacionReactivacion").modal('hide');            

                location.href ="/reactivarempleado/" + idempleado;

            }

        </script>
            
        @if(Session::has('success'))
            <div class="alert alert-success">
                {{session('success')}}
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            </div>
        @elseif( Session::has('warning'))
            <div class="alert alert-warning">
                {{session('warning')}}
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            </div>
        @elseif( Session::has('danger'))
            <div class="alert alert-danger">
                {{session('danger')}}
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            </div>
        @endif

            <!-- DIV FILTRADO -->
        <div class="container" style="margin-bottom: 10px;">
            
            <!-- DIV NOMBRE FILTRADO -->
            <div class="form-row align-items-center">
                
                <!-- NOMBRE ESPECIFICA -->
                <div class="col-auto" style="margin-left: 32%; margin-bottom:10px;">
                    <label for="nombrefiltrado"> Ingrese el nombre a buscar </label>
                    <input type="text" class="form-control-sm" id="nombrefiltrado" style="margin-left: 10px;">

                    
                    <label id="resultadofiltrado" style="margin-left: 20px;">  </label>
                </div>
                
            </div>


            <!-- Botones -->
            <div class="form-row align-items-center" style="margin-top: 2px; margin-bottom: 5px;">

                <a style="margin-left: 35%;"> <button  class="btn btn-primary" id="botonfiltrado"    onclick="javascript:FiltrarTabla();">Filtrar Tabla</button> </a>
                <a style="margin-left: 5%;">  <button  class="btn btn-primary" id="botonrestablecer" onclick="javascript:RestablecerTabla();">Restablecer Tabla</button> </a>

            </div>
            
            
        </div>

            <!-- Alerta -->
        <div class="container"  id="contentalert" style="display: none;">
            <div class="alert alert-danger alert-dismissible" role="alert" style="margin-top: 1%" >
                <input class="form-control-plaintext" value = "" id="textoalerta" disabled>    
                <button type="button" class="close" onclick="javascript:OcultarAlerta();" style="margin-top: 5px">
                <span>&times;</span>
                </button>
            </div>
        </div>
        
            <!-- TOOLTIPTEXT -->
        <div class="container" style="margin-top: 10px;">
            
            <div class="row justify-content-center">
                <label class="textotooltip">
                    HAGA DOBLE CLICK SOBRE UN EMPLEADO PARA REACTIVARLO
                </label>
            </div>
        </div>



        <!-- Tabla -->
        <div class="container">
            <div class="row justify-content-center">

                        <table class="table table-hover" id="tablaempleados" name="tablaempleados" ondblclick="javascript:FilaSeleccionada();">
                            <thead  class="thead-dark">
                                <tr>
                                    <th scope="col>"> ID </th>
                                    <th scope="col>"> Nombre completo </th>
                                    <th scope="col>"> Curp </th>
                                    <th scope="col>"> Fecha de nacimiento </th>
                                    <th scope="col>"> Teléfono </th>
                                    <th scope="col>"> Correo </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ( $registroempleados as $empleados )
                                    <tr>
                                        <td>{{ $empleados-> idempleado }}</td>
                                        <td>{{ $empleados-> nombrecompleto }}</td>
                                        <td>{{ $empleados-> curp }}</td>
                                        <td>{{ $empleados-> fechanac }}</td>
                                        <td>{{ $empleados-> telefono }}</td>
                                        <td>{{ $empleados-> correo }}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>

            </div>
        </div>
            
        <!-- ModalConfirmacionReactivación -->
        <div class="modal" tabindex="-1" role="dialog" id="ModalConfirmacionReactivacion">
            <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                <div class="modal-content modal-lg">

                    <!--Header-->
                    <div class="modal-header" id="modalheader" style="background: rgb(33, 37, 41)">
                        <h5 id="textostringheader" class="heading" style="margin-left: 25%; color: white; text-align: center;">CONFIRMACIÓN DE REACTIVACIÓN</h5>

                        <button type="button" class="close" onclick="javascript:DismissModalConfirmacionReactivacion();" data-dismiss="modal" aria-label="Close">
                            <span  aria-hidden="true" style="color: rgb(255, 255, 255)" >&times;</span>
                        </button>
                    </div>

                    <!--Body-->
                    <div class="modal-body" id="modalbody">
                        <div class="row">
                            <div class="col">
                                <p id="textoerrorbody" style="text-align: center;"></p>
                            </div>
                        </div>

                    </div>

                    <!--Footer-->
                    <div class="modal-footer" id="modalfooter">
                        <button type="button" class="btn btn-primary" data-dismiss="modal" id="botonEliminar" onclick="javascript:ReactivarEmpleado();">Reactivar</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal" id="botonCancelar" onclick="javascript:DismissModalConfirmacionReactivacion();" style="margin-left: 72%;">CANCELAR</button>
                    </div>

                </div>
            </div>
        </div>

    @endsection

@endif
