@if( Auth::user()->Nivel == 1 )

    @extends('home')

    @section('title')
        Listado de clientes desactivados
    @endsection

    @section('contenido')


        <link rel="STYLESHEET" href="css/estilostooltipylistados.css">


        <script>

            var idcliente = "";

            function FiltrarTabla() {
                
                if( document.getElementById("nombrefiltrado").value != "" ) {
                    
                    RealizarFiltradoTabla( document.getElementById("nombrefiltrado").value );

                } else {
                            
                    document.getElementById("textoalerta").value = "Por favor ingrese el nombre a buscar";
                    document.getElementById("contentalert").style.display = "block";

                }

            }

            function RealizarFiltradoTabla( nombre ) {
            
                var arrayregistroclientes = {!! json_encode( $registroclientes->toArray(), JSON_HEX_TAG ) !!};

                VaciarTabla();

                var auxnombreproveedor = "";

                for( var i = 0; i < arrayregistroclientes.length; i++ ) {

                    auxnombreproveedor = arrayregistroclientes[i].nombrecompleto;
                    auxnombreproveedor = auxnombreproveedor.toUpperCase();
                    nombre = nombre.toUpperCase();

                    if( auxnombreproveedor.includes( nombre ) ) {
                                                
                        document.getElementById("tablaclientes").insertRow(-1).innerHTML =
                                "<td>" + arrayregistroclientes[i].idcliente + "</td>" +
                                "<td>" + arrayregistroclientes[i].nombrecompleto + "</td>" +
                                "<td>" + arrayregistroclientes[i].curp + "</td>" +
                                "<td>" + arrayregistroclientes[i].fechanac + "</td>" +
                                "<td>" + arrayregistroclientes[i].sexo + "</td>" +
                                "<td>" + arrayregistroclientes[i].telefono + "</td>";

                    }
                
                }
                
                const tabla = document.getElementById('tablaclientes');
                var rowCount = tabla.rows.length;

                if( rowCount == 1 ) {
                            
                    document.getElementById("textoalerta").value = "Ningún cliente coincide con el nombre buscado";
                    document.getElementById("contentalert").style.display = "block";


                } else {
                    
                    document.getElementById("resultadofiltrado").innerHTML = "<p> Total de coincidencias: <strong>"+ ( rowCount - 1) + "</strong> </p>";

                }


            }

            function VaciarTabla() {

                document.getElementById("nombrefiltrado").value = "";
                document.getElementById("resultadofiltrado").innerHTML = "";

                const tabla = document.getElementById('tablaclientes');
                var rowCount = tabla.rows.length;         

                for  ( var x = rowCount - 1; x > 0; x-- ) { 
                    tabla.deleteRow(x); 
                } 

            }

            function RestablecerTabla() {
            
                var arrayregistroclientes = {!! json_encode( $registroclientes->toArray(), JSON_HEX_TAG ) !!};
                
                VaciarTabla();

                for( var i = 0; i < arrayregistroclientes.length; i++ ) {                    
                            
                    document.getElementById("tablaclientes").insertRow(-1).innerHTML =
                                "<td>" + arrayregistroclientes[i].idcliente + "</td>" +
                                "<td>" + arrayregistroclientes[i].nombrecompleto + "</td>" +
                                "<td>" + arrayregistroclientes[i].curp + "</td>" +
                                "<td>" + arrayregistroclientes[i].fechanac + "</td>" +
                                "<td>" + arrayregistroclientes[i].sexo + "</td>" +
                                "<td>" + arrayregistroclientes[i].telefono + "</td>";
                
                }

            }

            function OcultarAlerta() {

                if( document.getElementById("contentalert").style.display == "none" ) {
                    document.getElementById("contentalert").style.display = "block";
                } else {
                    document.getElementById("contentalert").style.display = "none";
                }

            }

            function FilaSeleccionada() {
                
                var table = document.getElementById('tablaclientes'),
                selected = table.getElementsByClassName('selected');
                
                if (selected[0]) selected[0].className = '';
                event.target.parentNode.className = 'selected';

                idcliente = $("tr.selected td:first" ).html();
                var nombrecliente = $("tr.selected td:nth-child(2)").html();

                MostrarModalConfirmacionReactivacion( nombrecliente );
            }
            
            function MostrarModalConfirmacionReactivacion( nombrecliente ) {

                document.getElementById("textoerrorbody").innerHTML =   "<p>¿Está seguro que desea reactivar al cliente: <strong>" + nombrecliente + "</strong> ? </p>";
                $('#ModalConfirmacionReactivacion').modal('show');

            }

            function DismissModalConfirmacionReactivacion() {

                $("#ModalConfirmacionReactivacion").modal('hide');

            }

            function ReactivarCliente() {

                $("#ModalConfirmacionReactivacion").modal('hide');            

                location.href ="/reactivarcliente/" + idcliente;

            }

        </script>
            
        @if(Session::has('success'))
            <div class="alert alert-success">
                {{session('success')}}
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            </div>
        @elseif( Session::has('warning'))
            <div class="alert alert-warning">
                {{session('warning')}}
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            </div>
        @elseif( Session::has('danger'))
            <div class="alert alert-danger">
                {{session('danger')}}
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            </div>
        @endif

            <!-- DIV FILTRADO -->
        <div class="container" style="margin-bottom: 10px;">
            
            <!-- DIV NOMBRE FILTRADO -->
            <div class="form-row align-items-center">
                
                <!-- NOMBRE ESPECIFICA -->
                <div class="col-auto" style="margin-left: 32%; margin-bottom:10px;">
                    <label for="nombrefiltrado"> Ingrese el nombre a buscar </label>
                    <input type="text" class="form-control-sm" id="nombrefiltrado" style="margin-left: 10px;">

                    
                    <label id="resultadofiltrado" style="margin-left: 20px;">  </label>
                </div>
                
            </div>


            <!-- Botones -->
            <div class="form-row align-items-center" style="margin-top: 2px; margin-bottom: 5px;">

                <a style="margin-left: 35%;"> <button  class="btn btn-primary" id="botonfiltrado"    onclick="javascript:FiltrarTabla();">Filtrar Tabla</button> </a>
                <a style="margin-left: 5%;">  <button  class="btn btn-primary" id="botonrestablecer" onclick="javascript:RestablecerTabla();">Restablecer Tabla</button> </a>

            </div>
            
            
        </div>

            <!-- Alerta -->
        <div class="container"  id="contentalert" style="display: none;">
            <div class="alert alert-danger alert-dismissible" role="alert" style="margin-top: 1%" >
                <input class="form-control-plaintext" value = "" id="textoalerta" disabled>    
                <button type="button" class="close" onclick="javascript:OcultarAlerta();" style="margin-top: 5px">
                <span>&times;</span>
                </button>
            </div>
        </div>
        
        <!-- TOOLTIPTEXT -->
        <div class="container" style="margin-top: 10px;">
            
            <div class="row justify-content-center">
                <label class="textotooltip">
                    HAGA DOBLE CLICK SOBRE UN CLIENTE PARA REACTIVARLO
                </label>
            </div>
        </div>



        <!-- Tabla -->
        <div class="container">
            <div class="row justify-content-center">

                <table class="table table-hover" id="tablaclientes" name="tablaclientes" ondblclick="javascript:FilaSeleccionada();">
                    <thead  class="thead-dark">
                        <tr>
                            <th scope="col>"> ID </th>
                            <th scope="col>"> Nombre completo </th>
                            <th scope="col>"> Curp </th>
                            <th scope="col>"> Fecha de nacimiento </th>
                            <th scope="col>"> Sexo </th>
                            <th scope="col>"> Teléfono </th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ( $registroclientes as $clientes )
                            <tr>
                                <td>{{ $clientes-> idcliente }}</td>
                                <td>{{ $clientes-> nombrecompleto }}</td>
                                <td>{{ $clientes-> curp }}</td>
                                <td>{{ $clientes-> fechanac }}</td>
                                <td>{{ $clientes-> sexo }}</td>
                                <td>{{ $clientes-> telefono }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>

            </div>
        </div>
            
        <!-- ModalConfirmacionReactivación -->
        <div class="modal" tabindex="-1" role="dialog" id="ModalConfirmacionReactivacion">
            <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                <div class="modal-content modal-lg">

                    <!--Header-->
                    <div class="modal-header" id="modalheader" style="background: rgb(33, 37, 41)">
                        <h5 id="textostringheader" class="heading" style="margin-left: 25%; color: white; text-align: center;">CONFIRMACIÓN DE REACTIVACIÓN</h5>

                        <button type="button" class="close" onclick="javascript:DismissModalConfirmacionReactivacion();" data-dismiss="modal" aria-label="Close">
                            <span  aria-hidden="true" style="color: rgb(255, 255, 255)" >&times;</span>
                        </button>
                    </div>

                    <!--Body-->
                    <div class="modal-body" id="modalbody">
                        <div class="row">
                            <div class="col">
                                <p id="textoerrorbody" style="text-align: center;"></p>
                            </div>
                        </div>

                    </div>

                    <!--Footer-->
                    <div class="modal-footer" id="modalfooter">
                        <button type="button" class="btn btn-primary" data-dismiss="modal" id="botonEliminar" onclick="javascript:ReactivarCliente();">Reactivar</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal" id="botonCancelar" onclick="javascript:DismissModalConfirmacionReactivacion();" style="margin-left: 72%;">CANCELAR</button>
                    </div>

                </div>
            </div>
        </div>

    @endsection

@endif
