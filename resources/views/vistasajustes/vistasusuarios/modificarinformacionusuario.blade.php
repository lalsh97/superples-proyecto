    @extends('home')

    @section('title')
        Modificar información del usuario
    @endsection

    <script>

        var banderamodificarpassword = false;

        function MostrarInputsPassword() {

            if( document.getElementById("switchcambiarpassword").checked ) {

                banderamodificarpassword = true;

                document.getElementById("labelpassword").style.display = "inline" ;
                document.getElementById("password").style.display = "inline" ;

                document.getElementById("labelpassword-confirm").style.display = "inline" ;
                document.getElementById("password-confirm").style.display = "inline" ;

            } else {

                banderamodificarpassword = false;

                document.getElementById("labelpassword").style.display = "none" ;
                document.getElementById("password").style.display = "none" ;

                document.getElementById("labelpassword-confirm").style.display = "none" ;
                document.getElementById("password-confirm").style.display = "none" ;

            }

        }

        function ValidarPasswords( formulario ) {

            console.log("banderamodificarpassword: ", banderamodificarpassword );
            console.log("PASSWORD: ", document.getElementById("password").value );
            console.log("PASSWORD-CONFIRM: ", document.getElementById("password-confirm").value );

            var banderavalidado = true;

            if( banderamodificarpassword ) {

                if( document.getElementById("password").value == "" && document.getElementById("password-confirm").value == "" ) {

                    banderavalidado = false;

                    document.getElementById("password").setCustomValidity("Este campo es requerido");
                    document.getElementById("password-confirm").setCustomValidity("Este campo es requerido");

                } else if( document.getElementById("password").value != "" && document.getElementById("password-confirm").value == "" ) {

                    banderavalidado = false;

                    document.getElementById("password").setCustomValidity("");
                    document.getElementById("password-confirm").setCustomValidity("Este campo es requerido");

                } else if( document.getElementById("password").value == "" && document.getElementById("password-confirm").value != "" ) {

                    banderavalidado = false;

                    document.getElementById("password-confirm").setCustomValidity("");
                    document.getElementById("password").setCustomValidity("Este campo es requerido");

                } else if( document.getElementById("password").value != "" && document.getElementById("password-confirm").value != "" ) {

                    banderavalidado = true;

                    if( document.getElementById("password").value != document.getElementById("password-confirm").value ) {


                        document.getElementById("password").setCustomValidity('');
                        document.getElementById("password-confirm").setCustomValidity("Las constraseñas no coinciden");
                        banderavalidado = false;

                    } else {

                        banderavalidado = true;

                        document.getElementById("password-confirm").setCustomValidity('');

                    }

                }

            }

            return banderavalidado;

        }

    </script>

    @section('contenido')


        @if(Session::has('success'))
            <div class="alert alert-success">
                {{session('success')}}
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            </div>
        @elseif(Session::has('danger'))
            <div class="alert alert-danger">
                {{session('danger')}}
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            </div>
        @elseif(Session::has('warning'))
            <div class="alert alert-warning">
                {{session('warning')}}
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            </div>
        @endif

        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header"  align="center">{{ __('Modificación de la información de usuario') }}</div>
                        <div class="card-body">


                            {!!Form::open(array('name' => 'formproducto', 'url'=>'/actualizarinformacionusuario/'.$informacionusuario->idusuario,'method'=>'PUT','files'=>true , 'enctype'=>'multipart/form-data' ,'autocomplete'=>'off' ))!!}
                            @csrf


                                <!-- ID del usuario -->
                                <div class="form-group row" style="display: none;">
                                    {!! Form::label( 'imagen','Imagen del usuario', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                    <div class="col-md-6" style="margin-left: 10%">
                                        <input type="text" id="idusuario" name="idusuario" value=" {{ ( $informacionusuario->idusuario ) }}">
                                    </div>
                                </div>

                                <!-- Avatar del usuario -->
                                <div class="form-group row">
                                    {!! Form::label( 'imagen','Perfil del usuario', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                    <div class="col-md-6" style="margin-left: 10%">
                                        <img  width="150px" src="/images/{{ ( $informacionusuario->avatarusuario ) }}">
                                        <!-- <img  width="150px" src="/public/storage/Productos/ProductoDefault.jpg" alt=""> -->
                                    </div>
                                </div>


                                <!-- Nuevo avatar del usuario -->
                                <div class="form-group row">
                                    {!! Form::label( 'avatar','Imagen del usuario', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                    <div class="col-md-6">
                                    <input type="file" id="avatar" name="avatar"  value="{{ old('avatar')}}" >
                                    </div>
                                </div>

                                <!-- NOMBRE DE USUARIO -->
                                <div class="form-group row">
                                    <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Nombre de usuario') }}</label>

                                    <div class="col-md-6">
                                        <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ ( $informacionusuario->nombreusuario ) }}" required autocomplete="name" autofocus>

                                        @error('name')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>


                                <!-- EMAIL -->
                                <div class="form-group row">
                                    <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('Correo Electrónico') }}</label>

                                    <div class="col-md-6">
                                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ ( $informacionusuario->emailusuario ) }}" required autocomplete="email">

                                        @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>


                                <!-- SWITCH BUTTON CAMBIO CONTRASEÑA -->
                                <div class="custom-control custom-switch" onclick="javascript:MostrarInputsPassword();" style="margin-top: 5%; margin-left: 38%; margin-bottom: 5%">
                                    <input type="checkbox" class="custom-control-input" id="switchcambiarpassword" name="switchcambiarpassword">
                                    <label class="custom-control-label" for="switchcambiarpassword">Cambiar contraseña</label>
                                </div>

                                <!-- PASSWORD -->
                                <div class="form-group row">
                                    <label for="password" id="labelpassword" class="col-md-4 col-form-label text-md-right" style="display: none;">{{ __('Contraseña') }}</label>

                                    <div class="col-md-6">
                                        <input id="password" type="password" class="form-control" name="password" style="display: none;">
                                    </div>
                                </div>

                                <!-- CONFIRMAR PASSWORD -->
                                <div class="form-group row">
                                    <label for="password-confirm" id="labelpassword-confirm" class="col-md-4 col-form-label text-md-right" style="display: none;">{{ __('Confirmar contraseña') }}</label>

                                    <div class="col-md-6">
                                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" style="display: none;">
                                    </div>
                                </div>


                                <div class="form-group row">
                                    <div class="col-md-4 col-form-label text-md-right">
                                        <button name="botonformproducto" value="Registrar" type="submit" class="btn btn-success" style="margin-right:50px"  onclick="javascript:ValidarPasswords(this);" >Actualizar</button>
                                    </div>
                                    <div class="col-md-4 col-form-label text-md-right">
                                        <button name="botonformproducto" value="Cancelar" type="submit" class="btn btn-danger" style="margin-left:250px" formnovalidate >Cancelar</button>
                                    </div>
                                </div>

                            {!!Form::close()!!}

                        </div>
                    </div>
                </div>
            </div>
        </div>

        <br><br>

    @endsection

