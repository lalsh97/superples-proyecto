@if( Auth::user()->Nivel == 1 )
    
    @extends('home')

    @section('title')
        Importar datos
    @endsection

    @section('contenido')

        @if(Session::has('success'))
            <div class="alert alert-success">
                {{session('success')}}
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            </div>
        @elseif(Session::has('danger'))
            <div class="alert alert-danger">
                {{session('danger')}}
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            </div>
        @elseif(Session::has('warning'))
            <div class="alert alert-warning">
                {{session('warning')}}
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            </div>
        @endif

        <!-- OPCIONES -->
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header"  align="center">{{ __('Opciones para importar') }}</div>
                            <div class="card-body">
                                
                                <!-- IMPORTAR DATOS PRODUCTOS -->
                                <div class="form-group" style="margin-right: 15%;">                                    
                                    <label class="col-md-8 col-form-label text-md-left" style="margin-right: 12.5%;">Importar datos de productos desde BusinessControl</label>
                                    <a href="{{ route('importardatosproductosbusinesscontrol') }}" > <button type="button" class="btn btn-success btn-sm">Importar datos</button> </a>
                                </div> 
                                
                                <!-- IMPORTAR DATOS PRODUCTOS -->
                                <div class="form-group" style="margin-right: 15%;">                                    
                                    <label class="col-md-8 col-form-label text-md-left" style="margin-right: 12.5%;">Importar datos de productos desde archivo Excel</label>
                                    <a href="{{ route('importardatosproductosexcel') }}" > <button type="button" class="btn btn-success btn-sm">Importar datos</button> </a>
                                </div>

                            </div>
                    </div>
                </div>
            </div>
        </div>

    @endsection

@endif