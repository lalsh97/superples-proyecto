@if( Auth::user()->Nivel == 1 )
    
    @extends('home')

    @section('title')
        Importar datos productos MYBUSINESS
    @endsection


    <style>

        .loader {
            position: fixed;
            left: 0px;
            top: 0px;
            width: 100%;
            height: 100%;
            z-index: 9999;
            background: url('gifs/loader.gif') 50% 50% no-repeat rgb(249,249,249);
            opacity: .8;
        }

    </style>    

    <script type="text/javascript">

        window.onload = function( e ) {

            $(".loader").fadeOut("slow");
            
        }

        function enviar() {
            console.log("ENVIAR");

            if( $("#rutaarchivo").val() == "" ) {
                console.log("NULL");
            } else {
                $("#loaderimportar").css( {"display": "block"} );
            }
        }

        function DismissModalMensajeImportar() {

            document.getElementById("modalMensajeImportar").style.display = "none";
            $("#modalMensajeImportar").modal('hide');

        }

    </script>

    @section('contenido')

        @if(Session::has('success'))
            <div class="alert alert-success">
                {{session('success')}}
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            </div>
        @elseif(Session::has('danger'))
            <div class="alert alert-danger">
                {{session('danger')}}
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            </div>
        @elseif(Session::has('warning'))
            <div class="alert alert-warning">
                {{session('warning')}}
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            </div>
        @elseif(Session::has('successimportar'))            
            <div class="modal" tabindex="-1" role="dialog" id="modalMensajeImportar" style="display: block;">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        
                    <!--Header-->
                        <div class="modal-header" id="modalheader" style="background: rgb(33, 37, 41)">
                            <h5 id="textostringheader" class="heading" style="margin-left: 25%; color: white;">OPERACIÓN EXITOSA</h5>

                            <button type="button" class="close" onclick="javascript:DismissModalMensajeImportar();" data-dismiss="modal" aria-label="Close">
                                <span  aria-hidden="true" style="color: rgb(255, 255, 255)" >&times;</span>
                            </button>
                        </div>

                        <!--Body-->
                        <div class="modal-body" id="modalbody">
                            <div class="row">
                                <div class="col">
                                    <p id="textoerrorbody">Los datos se han importado correctamente</p>
                                </div>
                            </div>
                            
                            <button type="button" class="btn btn-dark" data-dismiss="modal" onclick="javascript:DismissModalMensajeImportar();" style="margin-left: 85%;">Cerrar</button>                        
                        </div>
                    </div>
                </div>
            </div>
        @endif

        <div class="loader" id="loaderimportar" style="display: none;"></div>

        <!-- OPCIONES -->
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header"  align="center">{{ __('Importar datos desde archivo MYBUSINESS') }}</div>
                            <div class="card-body">

                                {!!Form::open(array('name' => 'botonformimportarproductos', 'url'=>'guardardatosproductosbusinesscontrol','method'=>'POST','files'=>true , 'enctype'=>'multipart/form-data','autocomplete'=>'off'))!!}
                                    
                                    <div class="form-group row" style="margin-left: 15%;">
                                        <label>Seleccione su archivo</label>
                                        <div class="col-md-6">
                                            <!-- <input type="file" id="rutaarchivo" name="rutaarchivo"  value="{{ old('rutaarchivo')}}" required> -->
                                            <input id="rutaarchivo" name="rutaarchivo" type="file" required>
                                        </div>
                                    </div>

                                    <br>

                                    <div class="form-group row">
                                        <div class="col-md-4 col-form-label text-md-right">
                                            <button name="botonformimportarproductos" value="Importar" type="submit" class="btn btn-success" style="margin-right:50px" onclick="javascript:enviar();">Importar</button>
                                        </div>
                                        <div class="col-md-4 col-form-label text-md-right">
                                            <button name="botonformimportarproductos" value="Cancelar" type="submit" class="btn btn-danger" style="margin-left:250px" formnovalidate>Cancelar</button>
                                        </div>
                                    </div>

                                {!!Form::close()!!}

                            </div>
                    </div>
                </div>
            </div>
        </div>

    @endsection

@endif