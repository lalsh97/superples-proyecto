<html>
    <head>

      <style>

        #logo {

            position: fixed;
            top: 10px;
            left: 10px;
            border-radius: 4px;
            padding: 5px;
            width: 150px;

        }

        #logo2 {

            position: fixed;
            top: 20px;
            left: 850px;
            border-radius: 4px;
            padding: 5px;
            width: 150px;

        }

        .encabezado {

            background-color: #91D5F7;
            padding: 25px 10px 40px 0px;
            border: 0px;
            margin: 0px;
            text-align: center;

        }

        h2 {

            padding: 15px 10px 15px 0px;
            border: 0px;
            margin: 0px;
            color:black;
            font-variant: all-petite-caps;

        }

        hr {

            border: black 1px solid;
            background: orange;
            padding: 10px;
            border: 0px;
            margin: 0px;

        }

        table {

            font: oblique bold 100% cursive;
            border-collapse: collapse;
            text-align: left;
            width: 1000px;

        }

        .descripcion {

            width: 650px;
            color: white;
            background: black;

        }

        .codigo {

            width: 250px;
            color: white;
            background: black;

        }

        .cantidad {

            width: 100px;
            color: white;
            background: black;

        }

        #columna {

            background:#F7F5E4;

        }

        p {

            text-align: center;
            padding: 15px;
            font-family: "Segoe IU", sans-serif;

        }

      </style>

      <title>Reporte de existencias en el inventario</title>

    </head>

    <body>

        <script type="text/php">
            if ( isset($pdf) ) {
                $pdf->page_script('
                    $font = $fontMetrics->get_font("Arial, Helvetica, sans-serif", "normal");
                    $pdf->text(400, 20, "Pág $PAGE_NUM de $PAGE_COUNT", $font, 10);
                ');
            }
        </script>

        <div style="border:3px solid black;">

            <div>
                <h1 class="encabezado"> {{$tienda->Nombre}} </h1>
                <img id="logo" src="img/icono.png"  width="150"  alt="return" />
                <img id="logo2" src="img/LogoPLES.png"  width="150"  alt="return" />
            </div>
        
            <h2 style="text-align:center;"> Reporte de existencias en el inventario</h2>
            <hr>

            <p>   Actualmente "{{$tienda->Nombre}}" cuenta con un total de {{sizeof($existencias)}} productos en existencia</p>
            <br><br>

            <table border="2" style="margin: 1 auto;">
                <thead>
                    <tr class="odd">
                        <th class="codigo"> Código producto </th>
                        <th class="descripcion"> Descripción producto </th>
                        <th class="cantidad">  Cantidad </th>
                    </tr>
                </thead>

                <tbody>
                    @foreach ( $existencias as $ex )
                      <tr id="columna">
                        <td>{{ $ex-> Codigo }}</td>
                        <td>{{ $ex-> DescripcionProducto }}</td>
                        <td>{{ $ex-> Cantidad }}</td>
                      </tr>
                    @endforeach
                </tbody>

            </table>
            
            <div>
                <br><br>
                <p>"{{$tienda->Nombre}}" ------------Correo: {{$tienda->Correo}} ------------Teléfono: {{$tienda->Telefono}}------------ RFC: {{$tienda->Telefono}}
                    <br>  Dirección: {{$tienda->Calle}}  #{{$tienda->Numero}}
                        {{$tienda->Colonia}} {{$tienda->Municipio}} {{$tienda->CodigoP}} {{$tienda->Ciudad}}
                </p>
            </div>

        </div>

    </body>

</html>
