@if( Auth::user()->Nivel == 1 )

    @extends('home')

    @section('title')
        Reporte de existencias en el inventario
    @endsection

    @section('contenido')
        

        @if(Session::has('success'))
            <div class="alert alert-success">
                {{session('success')}}
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            </div>
        @elseif( Session::has('warning'))
            <div class="alert alert-warning">
                {{session('warning')}}
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            </div>
        @elseif( Session::has('danger'))
            <div class="alert alert-danger">
                {{session('danger')}}
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            </div>
        @endif

        <p style="font-size: 24px; text-align: center;"><strong>REPORTE DE EXISTENCIAS EN EL INVENTARIO</strong></p>

        <br>
        <br>    

            <!-- Tabla -->
        <div class="container">
            <div class="row justify-content-center">

                <table class="table table-hover" id="tablamovimientos" name="tablamovimientos">
                    <thead  class="thead-dark">
                        <tr>
                            <th width="15%"> Código producto </th>
                            <th width="20%"> Descripción producto </th>
                            <th width="5%">  Cantidad </th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ( $existencias as $ex )
                            <tr>
                                <td>{{ $ex-> Codigo }}</td>
                                <td>{{ $ex-> DescripcionProducto }}</td>
                                <td>{{ $ex-> Cantidad }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>

        <br>
        <br>
            
            <!-- Botón Exportar PDF -->
        <div class="container col-2" style="margin-bottom: 50px">
            <div class="card secondary bg-light mb-3">
                <div class="card-body">                   
                    <form action="{{route('generarreporteexistenciasinventario')}}" method="get">
                        <button type="submit" id="existenciasinventario" class="btn btn-info" style="margin-left: 15%" >Generar PDF</button>
                    </form>
                </div>
            </div>
        </div>

        <br>
        <br>    
        

    @endsection

@endif