@if( Auth::user()->Nivel == 1 )

    @extends('home')

    @section('title')
        Reporte de ventas
    @endsection

    @section('contenido')


        @if(Session::has('success'))
            <div class="alert alert-success">
                {{session('success')}}
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            </div>
        @elseif( Session::has('warning'))
            <div class="alert alert-warning">
                {{session('warning')}}
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            </div>
        @elseif( Session::has('danger'))
            <div class="alert alert-danger">
                {{session('danger')}}
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            </div>
        @endif

        <p style="font-size: 24px; text-align: center;"><strong>REPORTE DE VENTAS</strong></p>

        <br>
        <br>

            <!-- Tabla -->
        <div class="container">
            <div class="row justify-content-center">

                <table class="table table-hover" id="tablamovimientos" name="tablamovimientos">
                    <thead  class="thead-dark">
                        <tr>
                            <th width="5%">  ID venta </th>
                            <th scope="col"> Nombre del empleado </th>
                            <th scope="col"> Nombre del cliente  </th>
                            <th scope="col"> Fecha de la venta </th>
                            <th scope="col"> Total de la venta </th>
                            <th scope="col"> Tipo de pago </th>

                        </tr>
                    </thead>
                    <tbody>
                            @foreach ( $listadoventas as $ventas )
                            <tr>
                            <td >{{ $ventas->idventa }}</td>
                            <td>{{ $ventas-> nombrecompletoempleado }}</td>
                            <td>{{ $ventas-> nombrecompletocliente }}</td>
                            <td>{{ $ventas-> fechaventa }}</td>
                            <td> <?php echo bcadd(  $ventas->totalventa ,'0',2); ?></td>
                            <td>
                                @if( $ventas->tipopago == 1 )
                                    {{ "Contado" }}
                                @elseif( $ventas->tipopago == 2 )
                                    {{ "Crédito" }}
                                @elseif( $ventas->tipopago == 3 )
                                    {{ "Contado y crédito" }}
                                @endif
                            </td>

                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>

        <br>
        <br>

            <!-- Botón Exportar PDF -->
        <div class="container col-4">
            <div class="card">
                <div class="card-body">        
                    <div class="form-group row">                    
                        <div class="col">
                            <form action="{{route('generarreporteventas')}}" method="get">
                                <button type="submit" id="existenciasinventario" class="btn btn-info" style="margin-left: 15%;" >Generar PDF</button>
                            </form>
                        </div>
                        <div class="col">
                            <form action="{{route('reporteventasexcel')}}" method="get">
                                <button type="submit" id="existenciasinventario" class="btn btn-info" style="margin-left: 15%;" >Generar Excel</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <br>
        <br>


    @endsection

@endif