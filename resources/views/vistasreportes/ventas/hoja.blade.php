<html>

    <head>

      <style>

            #logo {

                position: fixed;
                top: 10px;
                left: 10px;
                border-radius: 4px;
                padding: 5px;
                width: 150px;

            }

            #logo2 {

                position: fixed;
                top: 20px;
                left: 850px;
                border-radius: 4px;
                padding: 5px;
                width: 150px;

            }

            .encabezado {

                background-color: #91D5F7;
                padding: 25px 10px 40px 0px;
                border: 0px;
                margin: 0px;
                text-align: center;

            }

            h2 {

                padding: 15px 10px 15px 0px;
                border: 0px;
                margin: 0px;
                color:black;
                font-variant: all-petite-caps;

            }
            hr {

                border: black 1px solid;
                background: orange;
                padding: 10px;
                border: 0px;
                margin: 0px;

            }

            table {

                font: oblique bold 100% cursive;
                border-collapse: collapse;
                text-align: left;
                width: 1000px;

            }

            .id {

                width: 50px;
                color: white;
                background: black;

            }

            .empleado, .cliente {

                width: 310px;
                color: white;
                background: black;

            }

            .fecha {

              width: 100px;
              color: white;
              background: black;
              
            }

            .total {

              width: 75px;
              color: white;
              background: black;

            }

            .tipo {

              width: 125px;
              color: white;
              background: black;

            }

          #columna {

              background:#F7F5E4;

          }

          p {

              text-align: center;
              padding: 15px;
              font-family: "Segoe IU", sans-serif;

          }

      </style>

      <title>Reporte de ventas</title>

    </head>

    <body>

        <script type="text/php">
            if ( isset($pdf) ) {
                $pdf->page_script('
                    $font = $fontMetrics->get_font("Arial, Helvetica, sans-serif", "normal");
                    $pdf->text(400, 20, "Pág $PAGE_NUM de $PAGE_COUNT", $font, 10);
                ');
            }
        </script>

        <div style="border:2px solid black;">

            <div>
                <h1 class="encabezado"> {{$tienda->Nombre}} </h1>
                <img id="logo" src="img/icono.png"  width="150"  alt="return" />
                <img id="logo2" src="img/LogoPLES.png"  width="150"  alt="return" />
            </div>

            <h2 style="text-align:center;"> Reporte de ventas</h2>
            <hr>

            <p>   Actualmente en "{{$tienda->Nombre}}" se han realizado  {{sizeof($listadoventas)}} ventas</p>
            <br><br>

            <table border="2" style="margin: 1 auto;">

                <thead>
                    <tr class="odd">
                        <th class="id">  ID venta </th>
                        <th class="empleado"> Nombre del empleado </th>
                        <th class="cliente"> Nombre del cliente  </th>
                        <th class="fecha"> Fecha de la venta </th>
                        <th class="total"> Total de la venta </th>
                        <th class="tipo"> Tipo de pago </th>
                    </tr>
                </thead>

                <tbody>

                    @foreach ( $listadoventas as $ventas )

                        <tr id="columna">
                            <td >{{ $ventas->idventa }}</td>
                            <td>{{ $ventas-> nombrecompletoempleado }}</td>
                            <td>{{ $ventas-> nombrecompletocliente }}</td>
                            <td>{{ $ventas-> fechaventa }}</td>
                            <td> <?php echo bcadd(  $ventas->totalventa ,'0',2); ?></td>
                            <td>
                                @if( $ventas->tipopago == 1 )
                                    {{ "Contado" }}
                                @elseif( $ventas->tipopago == 2 )
                                    {{ "Crédito" }}
                                @elseif( $ventas->tipopago == 3 )
                                    {{ "Contado y crédito" }}
                                @endif
                            </td>
                        </tr>

                    @endforeach

                </tbody>

            </table>

            <div>

                <br><br>
                <p>"{{$tienda->Nombre}}" ------------Correo: {{$tienda->Correo}} ------------Teléfono: {{$tienda->Telefono}}------------ RFC: {{$tienda->Telefono}}
                    <br>  Dirección: {{$tienda->Calle}}  #{{$tienda->Numero}}
                    {{$tienda->Colonia}} {{$tienda->Municipio}} {{$tienda->CodigoP}} {{$tienda->Ciudad}}
                </p>

            </div>

        </div>

    </body>

</html>
