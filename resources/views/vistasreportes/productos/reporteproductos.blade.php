@if( Auth::user()->Nivel == 1 )

    @extends('home')

    @section('title')
        Reporte de productos
    @endsection

    @section('contenido')


        @if(Session::has('success'))
            <div class="alert alert-success">
                {{session('success')}}
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            </div>
        @elseif( Session::has('warning'))
            <div class="alert alert-warning">
                {{session('warning')}}
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            </div>
        @elseif( Session::has('danger'))
            <div class="alert alert-danger">
                {{session('danger')}}
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            </div>
        @endif

        <p style="font-size: 24px; text-align: center;"><strong>REPORTE DE PRODUCTOS</strong></p>

        <br>
        <br>

            <!-- Tabla -->
        <div class="container">
            <div class="row justify-content-center">

                <table class="table table-hover" id="tablamovimientos" name="tablamovimientos">
                    <thead  class="thead-dark">
                        <tr>
                            <th width="15%"> Código producto </th>
                            <th width="10%">  Marca </th>
                            <th width="10%">  Presentación </th>
                            <th width="20%"> Proveedor </th>
                            <th width="10%"> Categoría </th>
                            <th width="20%"> Descripción producto </th>
                            <th width="5%">  Precio costo </th>
                            <th width="5%">  Precio venta </th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ( $productos as $pro )
                            <tr>
                                <td>{{ $pro-> codigo }}</td>
                                <td>{{ $pro-> nombremarca }}</td>
                                <td>{{ $pro-> nombrepresentacion }}</td>
                                <td>{{ $pro-> nombrecompleto }}</td>
                                <td>{{ $pro-> nombrecategoria }}</td>
                                <td>{{ $pro-> descripcion }}</td>
                                <td> <?php echo bcadd( $pro->preciocosto , '0', 2); ?>
                                <td> <?php echo bcadd( $pro->precioventa , '0', 2); ?>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>

        <br>
        <br>

            <!-- Botón Exportar PDF -->
        <div class="container col-4">
            <div class="card">
                <div class="card-body">

                    <div class="form-group row">                    
                        <div class="col">                
                            <form action="{{route('generarreporteproductos')}}" method="get">
                                <button type="submit" id="existenciasinventario" class="btn btn-info" style="margin-left: 15%;" >Generar PDF</button>
                            </form>
                        </div>
                        <div class="col">
                            <form action="{{route('reporteproductosexcel')}}" method="get">
                                <button type="submit" id="existenciasinventario" class="btn btn-info" style="margin-left: 15%;" >Generar Excel</button>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <br>
        <br>


    @endsection

@endif
