<html>

    <head>

      <style>
          #logo {

              position: fixed;
              top: 10px;
              left: 10px;
              border-radius: 4px;
              padding: 5px;
              width: 150px;

          }
          #logo2 {

              position: fixed;
              top: 20px;
              left: 850px;
              border-radius: 4px;
              padding: 5px;
              width: 150px;

          }

          .encabezado {

              background-color: #91D5F7;
              padding: 25px 10px 40px 0px;
              border: 0px;
              margin: 0px;
              text-align: center;

          }

          h2 {

              padding: 15px 10px 15px 0px;
              border: 0px;
              margin: 0px;
              color:black;
              font-variant: all-petite-caps;

          }
          hr {

              border: black 1px solid;
              background: orange;
              padding: 10px;
              border: 0px;
              margin: 0px;

          }

          table {

              font: oblique bold 100% cursive;
              border-collapse: collapse;
              text-align: left;
              width: 900px;

          }

          .id {

              width: 50px;
              color: white;
              background: black;

          }

           .descripcion {

              width: 100px;
              color: white;
              background: black;

          }

          .codi {

              width: 50px;
              color: black;
              background: #EFEDD1;

          }

          .marca , .presentacion , .categoria {

              width: 80px;
              color: white;
              background: black;
              align-content: center;

          }

          .proveedor {
            
              width: 150px;
              color: white;
              background: black;
              align-content: center;

          }
          .costo {

                width: 40px;
                color: white;
                background: black;
                align-content: center;

          }
          #columna {

              background:#F7F5E4;

          }

          p {

              text-align: center;
              padding: 15px;
              font-family: "Segoe IU", sans-serif;

          }

      </style>

      <title>Reporte Productos</title>

    </head>

    <body>    

        <script type="text/php">
            if ( isset($pdf) ) {
                $pdf->page_script('
                    $font = $fontMetrics->get_font("Arial, Helvetica, sans-serif", "normal");
                    $pdf->text(400, 20, "Pág $PAGE_NUM de $PAGE_COUNT", $font, 10);
                ');
            }
        </script>

        <div style="border:3px solid black;">

            <div>
                <h1 class="encabezado"> {{$tienda->Nombre}} </h1>
                <img id="logo" src="img/icono.png"  width="150"  alt="return" />
                <img id="logo2" src="img/LogoPLES.png"  width="150"  alt="return" />
            </div>

            <h2 style="text-align:center;"> Reporte de Productos</h2>
            <hr>

            <p>   Actualmente "{{$tienda->Nombre}}" cuenta con una varidad de {{sizeof($productos)}} productos a la venta.</p>
            <br><br>

            <table border="2" style="margin: 1 auto;">

                <thead>

                    <tr class="odd">
                        <th class="id"> Código producto</th>
                        <th class="marca"> Marca </th>
                        <th class="presentacion"> Presentación </th>
                        <th class="proveedor"> Proveedor</th>
                        <th class="categoria"> Categoría </th>
                        <th class="descripcion"> Descripción</th>
                        <th class="costo"> Precio costo</th>
                        <th class="costo"> Precio venta</th>
                    </tr>
                    
                </thead>

                <tbody>
                    @foreach ( $productos as $pro )
                        <tr id="columna">
                            <td class="codi">{{ $pro-> codigo }}</td>
                            <td>{{ $pro-> nombremarca }}</td>
                            <td>{{ $pro-> nombrepresentacion }}</td>
                            <td>{{ $pro-> nombrecompleto }}</td>
                            <td>{{ $pro-> nombrecategoria }}</td>
                            <td>{{ $pro-> descripcion }}</td>
                            <td> <?php echo bcadd( $pro->preciocosto , '0', 2); ?>
                            <td> <?php echo bcadd( $pro->precioventa , '0', 2); ?>
                        </tr>
                    @endforeach
                </tbody>

            </table>

            <div>

                <br><br>
                <p>"{{$tienda->Nombre}}" ------------Correo: {{$tienda->Correo}} ------------Teléfono: {{$tienda->Telefono}}------------ RFC: {{$tienda->Telefono}}
                    <br>  Dirección: {{$tienda->Calle}}  #{{$tienda->Numero}}
                    {{$tienda->Colonia}} {{$tienda->Municipio}} {{$tienda->CodigoP}} {{$tienda->Ciudad}}
                </p>

            </div>

        </div>

    </body>

</html>
