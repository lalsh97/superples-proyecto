<html>

    <head>

      <style>

            #logo {
                
                position: fixed;
                top: 10px;
                left: 10px;
                border-radius: 4px;
                padding: 5px;
                width: 150px;

            }

            #logo2 {

                position: fixed;
                top: 20px;
                left: 850px;
                border-radius: 4px;
                padding: 5px;
                width: 150px;

            }

            .encabezado {

                background-color: #91D5F7;
                padding: 25px 10px 40px 0px;
                border: 0px;
                margin: 0px;
                text-align: center;

            }

            h2 {

                padding: 15px 10px 15px 0px;
                border: 0px;
                margin: 0px;
                color:black;
                font-variant: all-petite-caps;

            }

            hr {

                border: black 1px solid;
                background: orange;
                padding: 10px;
                border: 0px;
                margin: 0px;

            }

            table {

                font: oblique bold 100% cursive;
                border-collapse: collapse;
                text-align: left;
                width: 1000px;

            }

            .codigo {

                width: 150px;
                color: white;
                background: black;

            }

            .cantidad {

                width: 150px;
                color: white;
                background: black;

            }

            .precio {

                width: 150px;
                color: white;
                background: black;

            }
            
            .importe {

                width: 100px;
                color: white;
                background: black;

            }

            .detalles {

                width: 20px;
                color: white;
                background: black;

            }

            .descripcion {

                width:  450px;
                color: white;
                background: black;

            }

            #columna {

                background:#F7F5E4;

            }

            p {

                text-align: center;
                padding: 15px;
                font-family: "Segoe IU", sans-serif;

            }

      </style>

      <title>Reporte de productos vendidos</title>

    </head>

    <body>

        <script type="text/php">
            if ( isset($pdf) ) {
                $pdf->page_script('
                    $font = $fontMetrics->get_font("Arial, Helvetica, sans-serif", "normal");
                    $pdf->text(400, 20, "Pág $PAGE_NUM de $PAGE_COUNT", $font, 10);
                ');
            }
        </script>

        <div style="border:2px solid black;">

            <div>
                <h1 class="encabezado"> {{$tienda->Nombre}} </h1>
                <img id="logo" src="img/icono.png"  width="150"  alt="return" />
                <img id="logo2" src="img/LogoPLES.png"  width="150"  alt="return" />
            </div>


            <h2 style="text-align:center;"> Reporte de productos vendidos</h2>
            <hr>
            <br><br>

            <table border="2" style="margin: 1 auto;">

                <thead>
                    <tr class="odd">                  
                            <th class="codigo"> Código producto </th>
                            <th class="descripcion"> Descripción producto </th>
                            <th class="cantidad"> Cantidad vendida </th>
                            <th class="precio"> Precio de venta </th>
                            <th class="importe"> Importe </th>
                    </tr>
                
                </thead>

                <tbody>
                    @foreach ( $productos as $pro )
                        <tr id="columna">
                            <td>{{ $pro->codigoproducto }}</td>
                            <td>{{ $pro->DescripcionProducto }}</td>
                            <td>{{ $pro->cantidadvendida }}</td>
                            <td> <?php echo bcadd( $pro->precioventa , '0', 2); ?>
                            <td> <?php echo bcadd( ( $pro->cantidadvendida *  $pro->precioventa )  , '0', 2); ?>
                        </tr>
                    @endforeach
                </tbody>

            </table>

            <div>
                <br><br>
                <p>"{{$tienda->Nombre}}" ------------Correo: {{$tienda->Correo}} ------------Teléfono: {{$tienda->Telefono}}------------ RFC: {{$tienda->Telefono}}
                    <br>  Dirección: {{$tienda->Calle}}  #{{$tienda->Numero}}
                    {{$tienda->Colonia}} {{$tienda->Municipio}} {{$tienda->CodigoP}} {{$tienda->Ciudad}}
                </p>
            </div>

        </div>

    </body>

</html>
