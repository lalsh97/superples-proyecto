@if( Auth::user()->Nivel == 1 )

    @extends('home')

    @section('title')
        Reporte de productos vendidos
    @endsection

    @section('contenido')


        @if(Session::has('success'))
            <div class="alert alert-success">
                {{session('success')}}
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            </div>
        @elseif( Session::has('warning'))
            <div class="alert alert-warning">
                {{session('warning')}}
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            </div>
        @elseif( Session::has('danger'))
            <div class="alert alert-danger">
                {{session('danger')}}
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            </div>
        @endif

        <p style="font-size: 24px; text-align: center;"><strong>REPORTE DE PRODUCTOS VENDIDOS</strong></p>

        <br>
        <br>

            <!-- Tabla -->
        <div class="container">
            <div class="row justify-content-center">

                <table class="table table-hover" id="tablamovimientos" name="tablamovimientos">
                    <thead  class="thead-dark">
                        <tr>
                        <th width="15%"> Código producto </th>
                        <th width="45%"> Descripción producto </th>
                        <th width="15%"> Cantidad vendida </th>
                        <th width="15%"> Precio de venta </th>
                        <th width="10%"> Importe </th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ( $productos as $pro )
                            <tr>
                                <td>{{ $pro->codigoproducto }}</td>
                                <td>{{ $pro->DescripcionProducto }}</td>
                                <td>{{ $pro->cantidadvendida }}</td>
                                <td> <?php echo bcadd( $pro->precioventa , '0', 2); ?>
                                <td> <?php echo bcadd( ( $pro->cantidadvendida *  $pro->precioventa )  , '0', 2); ?>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

            </div>
        </div>

        <br>
        <br>

            <!-- Botón Exportar PDF -->
        <div class="container col-2">
            <div class="card">
                <div class="card-body">
                    <form action="{{route('generarreporteproductosvendidos')}}" method="get">
                        <button type="submit" id="existenciasinventario" class="btn btn-info" style="margin-left: 15%" >Generar PDF</button>
                    </form>

                </div>
            </div>
        </div>

        <br>
        <br>


    @endsection

@endif
