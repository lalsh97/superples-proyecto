@if( Auth::user()->Nivel == 1 )
    
    @extends('home')

    @section('title')
        Reporte de promociones
    @endsection

    @section('contenido')


        @if(Session::has('success'))
            <div class="alert alert-success">
                {{session('success')}}
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            </div>
        @elseif( Session::has('warning'))
            <div class="alert alert-warning">
                {{session('warning')}}
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            </div>
        @elseif( Session::has('danger'))
            <div class="alert alert-danger">
                {{session('danger')}}
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            </div>
        @endif

        <p style="font-size: 24px; text-align: center;"><strong>REPORTE DE PROMOCIONES</strong></p>

        <br>
        <br>

            <!-- Tabla -->
        <div class="container">
            <div class="row justify-content-center">

                <table class="table table-hover" id="tablamovimientos" name="tablamovimientos">
                    <thead  class="thead-dark">
                    <tr>
                        <th scope="col>"> ID promoción </th>
                        <th scope="col>"> Nombre </th>
                        <th scope="col>"> Fecha de lanzamiento </th>
                        <th scope="col>"> Fecha de terminación </th>
                        <th scope="col>"> Total </th>
                        <th scope="col>"> Tipo </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ( $promociones as $promociones )
                        <tr>
                                <td>{{ $promociones->idPromociones }}</td>
                                <td>{{ $promociones-> Nombre }}</td>
                                <td>{{ $promociones-> Fechaini }}</td>
                                <td>{{ $promociones-> Fechafin }}</td>
                                <td> <?php echo bcadd( $promociones->Total , '0', 2); ?>
                                <?php
                                    if ($promociones->Tipo == 1 ) {
                                        ?> 
                                            <td>Oferta</td>
                                        <?php
                                    } elseif ($promociones->Tipo == 2 ) {
                                        ?>
                                            <td>Descuento</td>
                                        <?php
                                    } elseif ($promociones->Tipo == 3 ) {
                                        ?> 
                                            <td>Paquete</td>
                                        <?php
                                    }
                                ?>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>

        <br>
        <br>

            <!-- Botón Exportar PDF -->
        <div class="container col-4">
            <div class="card">
                <div class="card-body">

                    <div class="form-group row">                    
                        <div class="col">                
                            <form action="{{route('generarreportepromociones')}}" method="get">
                                <button type="submit" id="existenciasinventario" class="btn btn-info" style="margin-left: 15%" >Generar PDF</button>
                            </form>
                        </div>
                        <div class="col">
                            <form action="{{route('reportepromocionesexcel')}}" method="get">
                                <button type="submit" id="existenciasinventario" class="btn btn-info" style="margin-left: 15%" >Generar Excel</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <br>
        <br>


    @endsection

@endif
