@if( Auth::user()->Nivel == 1 )

    @extends('home')

    @section('title')
        Reportes
    @endsection

    @section('contenido')


        @if(Session::has('success'))
            <div class="alert alert-success">
                {{session('success')}}
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            </div>
        @elseif( Session::has('warning'))
            <div class="alert alert-warning">
                {{session('warning')}}
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            </div>
        @elseif( Session::has('danger'))
            <div class="alert alert-danger">
                {{session('danger')}}
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            </div>
        @endif

        <div class="container">
            <div class="row justify-content-center">

                <div class="card-deck" style="margin-bottom: 10px" >

                        <!-- Reporte de entradas de inventario -->
                    <div class="card text-center bg-light border-secondary mb-3" style="width: 18rem; margin-right: 10px;">
                        <img class="card-img-center" src="{{ asset('images/Reportes/EntradaAlmacen.png') }}" alt="Card image cap" width="150" height="150" style="display: block; margin-left: auto; margin-right: auto; margin-top: 10px;" >
                        <div class="card-body">
                            <p class="card-text"> Reporte de entradas de inventario </p>
                            <a href="{{ route('reporteentradasinventario') }}" class="btn btn-info">IR</a>
                        </div>
                    </div>

                        <!-- Reporte de salidas de inventario -->
                    <div class="card text-center bg-light border-secondary mb-3" style="width: 18rem; margin-right: 10px;">
                        <img class="card-img-center" src="{{ asset('images/Reportes/SalidaAlmacen.png') }}" alt="Card image cap" width="150" height="150" style="display: block; margin-left: auto; margin-right: auto; margin-top: 10px;" >
                        <div class="card-body">
                            <p class="card-text"> Reporte de salidas de inventario </p>
                            <a href="{{ route('reportesalidasinventario') }}" class="btn btn-info">IR</a>
                        </div>
                    </div>

                        <!-- Reporte de existencias de inventario -->
                    <div class="card text-center bg-light border-secondary mb-3" style="width: 18rem; margin-right: 10px;" >
                        <img class="card-img-center" src="{{ asset('images/Reportes/ExistenciasInventario.png') }}" alt="Card image cap" width="150" height="150" style="display: block; margin-left: auto; margin-right: auto; margin-top: 10px;" >
                        <div class="card-body">
                            <p class="card-text"> Reporte de existencias de inventario </p>
                            <a href="{{ route('reporteexistenciasinventario') }}" class="btn btn-info">IR</a>
                        </div>
                    </div>

                </div>

                <div class="card-deck" style="margin-bottom: 10px" >

                        <!-- Reporte de productos bajo de inventario -->
                    <div class="card text-center bg-light border-secondary mb-3" style="width: 18rem; margin-right: 10px;">
                        <img class="card-img-center" src="{{ asset('images/Reportes/ProductosBajos.png') }}" alt="Card image cap" width="150" height="150" style="display: block; margin-left: auto; margin-right: auto; margin-top: 10px;" >
                        <div class="card-body">
                            <p class="card-text"> Reporte de productos bajos </p>
                            <a href="{{ route('reporteproductosbajos') }}" class="btn btn-info">IR</a>
                        </div>
                    </div>

                        <!-- Reporte general de ventas -->
                    <div class="card text-center bg-light border-secondary mb-3" style="width: 18rem; margin-right: 10px;">
                        <img class="card-img-center" src="{{ asset('images/Reportes/GeneralVentas.png') }}" alt="Card image cap" width="150" height="150" style="display: block; margin-left: auto; margin-right: auto; margin-top: 10px;" >
                        <div class="card-body">
                            <p class="card-text"> Reporte general de ventas </p>
                            <a href="{{ route('reportegeneralventas') }}" class="btn btn-info">IR</a>
                        </div>
                    </div>

                        <!-- Reporte de ventas -->
                    <div class="card text-center bg-light border-secondary mb-3" style="width: 18rem; margin-right: 10px;" >
                        <img class="card-img-center" src="{{ asset('images/Reportes/Ventas.png') }}" alt="Card image cap" width="150" height="150" style="display: block; margin-left: auto; margin-right: auto; margin-top: 10px;" >
                        <div class="card-body">
                            <p class="card-text"> Reporte de ventas </p>
                            <a href="{{ route('reporteventas') }}" class="btn btn-info">IR</a>
                        </div>
                    </div>

                </div>

                <div class="card-deck" style="margin-bottom: 10px" >

                        <!-- Reporte de créditos de clientes -->
                    <div class="card text-center bg-light border-secondary mb-3" style="width: 18rem; margin-right: 10px;">
                        <img class="card-img-center" src="{{ asset('images/Reportes/CreditosCliente.png') }}" alt="Card image cap" width="150" height="150" style="display: block; margin-left: auto; margin-right: auto; margin-top: 10px;" >
                        <div class="card-body">
                            <p class="card-text"> Reporte de créditos de clientes </p>
                            <a href="{{ route('reportecreditosclientes') }}" class="btn btn-info">IR</a>
                        </div>
                    </div>

                        <!-- Reporte de productos vendidos -->
                    <div class="card text-center bg-light border-secondary mb-3" style="width: 18rem; margin-right: 10px;">
                        <img class="card-img-center" src="{{ asset('images/Reportes/ProductosVendidos.png') }}" alt="Card image cap" width="150" height="150" style="display: block; margin-left: auto; margin-right: auto; margin-top: 10px;" >
                        <div class="card-body">
                            <p class="card-text"> Reporte de productos vendidos </p>
                            <a href="{{ route('reporteproductosvendidos') }}" class="btn btn-info">IR</a>
                        </div>
                    </div>

                        <!-- Reporte de movimientos de proveedores -->
                    <div class="card text-center bg-light border-secondary mb-3" style="width: 18rem; margin-right: 10px;" >
                        <img class="card-img-center" src="{{ asset('images/Reportes/MovimientosProveedores.png') }}" alt="Card image cap" width="150" height="150" style="display: block; margin-left: auto; margin-right: auto; margin-top: 10px;" >
                        <div class="card-body">
                            <p class="card-text"> Reporte de movimientos de proveedores </p>
                            <a href="{{ route('reporteproveedoresmovimientos') }}" class="btn btn-info">IR</a>
                        </div>
                    </div>



                </div>

                <div class="card-deck" style="margin-bottom: 10px" >

                        <!-- Reporte de movimientos de proveedores -->
                    <div class="card text-center bg-light border-secondary mb-3" style="width: 18rem; margin-right: 10px;" >
                        <img class="card-img-center" src="{{ asset('images/Reportes/Productos.png') }}" alt="Card image cap" width="150" height="150" style="display: block; margin-left: auto; margin-right: auto; margin-top: 10px;" >
                        <div class="card-body">
                            <p class="card-text"> Reporte de productos</p>
                            <a href="{{ route('reporteproductos') }}" class="btn btn-info">IR</a>
                        </div>
                    </div>

                        <!-- Reporte de productos vendidos -->
                    <div class="card text-center bg-light border-secondary mb-3" style="width: 18rem; margin-right: 10px;">
                        <img class="card-img-center" src="{{ asset('images/Reportes/Proveedores.png') }}" alt="Card image cap" width="150" height="150" style="display: block; margin-left: auto; margin-right: auto; margin-top: 10px;" >
                        <div class="card-body">
                            <p class="card-text"> Reporte de proveedores </p>
                            <a href="{{ route('reporteproveedores') }}"class="btn btn-info">IR</a>
                        </div>
                    </div>

                        <!-- Reporte de movimientos de proveedores -->
                    <div class="card text-center bg-light border-secondary mb-3" style="width: 18rem; margin-right: 10px;" >
                        <img class="card-img-center" src="{{ asset('images/Reportes/Clientes.png') }}" alt="Card image cap" width="150" height="150" style="display: block; margin-left: auto; margin-right: auto; margin-top: 10px;" >
                        <div class="card-body">
                            <p class="card-text"> Reporte de clientes</p>
                            <a href="{{ route('reporteclientes') }}" class="btn btn-info">IR</a>
                        </div>
                    </div>

                </div>

                <div class="card-deck" style="margin-bottom: 10px" >

                        <!-- Reporte de promociones -->
                    <div class="card text-center bg-light border-secondary mb-3" style="width: 18rem; margin-right: 10px;" >
                        <img class="card-img-center" src="{{ asset('images/Reportes/Promociones.png') }}" alt="Card image cap" width="150" height="150" style="display: block; margin-left: auto; margin-right: auto; margin-top: 10px;" >
                        <div class="card-body">
                            <p class="card-text"> Reporte de promociones</p>
                            <a href="{{ route('reportepromociones') }}" class="btn btn-info">IR</a>
                        </div>
                    </div>

                </div>

            </div>
        </div>

    @endsection

@endif
