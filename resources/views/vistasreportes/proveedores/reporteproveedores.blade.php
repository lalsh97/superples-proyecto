@if( Auth::user()->Nivel == 1 )

    @extends('home')

    @section('title')
        Reporte de proveedores
    @endsection

    @section('contenido')


        @if(Session::has('success'))
            <div class="alert alert-success">
                {{session('success')}}
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            </div>
        @elseif( Session::has('warning'))
            <div class="alert alert-warning">
                {{session('warning')}}
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            </div>
        @elseif( Session::has('danger'))
            <div class="alert alert-danger">
                {{session('danger')}}
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            </div>
        @endif

        <p style="font-size: 24px; text-align: center;"><strong>REPORTE DE PROVEEDORES</strong></p>

        <br>
        <br>

            <!-- Tabla -->
        <div class="container">
            <div class="row justify-content-center">

                <table class="table table-hover" id="tablamovimientos" name="tablamovimientos">
                    <thead  class="thead-dark">
                    <tr>
                        <th scope="col"> ID proveedor </th>
                        <th scope="col"> RFC </th>
                        <th scope="col"> Nombre </th>
                        <th scope="col"> Correo electrónico</th>
                        <th scope="col"> Teléfono</th>
                    </tr>
                </thead>
                <tbody>
                        @foreach ( $proveedores as $proveedores )
                            <tr>
                                <td>{{ $proveedores-> idProveedor }}</td>
                                <td>{{ $proveedores-> Rfc }}</td>
                                <td>{{ $proveedores-> Nombre }} {{ $proveedores-> ApellidoPat }} {{ $proveedores-> ApellidoMat }}</td>
                                <td>{{ $proveedores-> Correo }}</td>
                                <td>{{ $proveedores-> Telefono }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>

        <br>
        <br>

            <!-- Botón Exportar PDF -->
        <div class="container col-4">
            <div class="card">
                <div class="card-body">

                    <div class="form-group row">                    
                        <div class="col">
                            <form action="{{route('generarreporteproveedores')}}" method="get">
                                <button type="submit" id="existenciasinventario" class="btn btn-info" style="margin-left: 15%;" >Generar PDF</button>
                            </form>
                        </div>
                        <div class="col">
                            <form action="{{route('reporteproveedoresexcel')}}" method="get">
                                <button type="submit" id="existenciasinventario" class="btn btn-info" style="margin-left: 15%;" >Generar Excel</button>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <br>
        <br>


    @endsection

@endif
