<html>

    <head>

        <style>
            #logo {

                position: fixed;
                top: 10px;
                left: 10px;
                border-radius: 4px;
                padding: 5px;
                width: 150px;

            }
            #logo2 {
            
                position: fixed;
                top: 20px;
                left: 850px;
                border-radius: 4px;
                padding: 5px;
                width: 150px;

            }

            .encabezado {
                
                background-color: #91D5F7;
                padding: 25px 10px 40px 0px;
                border: 0px;
                margin: 0px;
                text-align: center;

            }
            h2 {
              
                padding: 15px 10px 15px 0px;
                border: 0px;
                margin: 0px;
                color:black;
                font-variant: all-petite-caps;

            }
            hr {

                border: black 1px solid;
                background: orange;
                padding: 10px;
                border: 0px;
                margin: 0px;

            }

            table {

                font: oblique bold 100% cursive;
                border-collapse: collapse;
                text-align: left;
                width: 820px;

            }

            .id {

                width:40px;
                color: white;
                background: black;

            }

            .rfc {

                width: 145px;
                color: white;
                background: black;

            }

            .telefono {
              
                width: 120px;
                color: white;
                background: black;
            }

            .correo {

                width: 200px;
                color: white;
                background: black;

            }

            .nombre {

                width: 300px;
                color: white;
                background: black;

            }

            .codi {

                width: 40px;
                color: black;
                background: #EFEDD1;

            }

              #columna {
                
                  background:#F7F5E4;

              }

              p{

                  text-align: center;
                  padding: 15px;
                  font-family: "Segoe IU", sans-serif;

              }

        </style>

        <title>Reporte Proveedores</title>

    </head>

    <body>    

        <script type="text/php">
            if ( isset($pdf) ) {
                $pdf->page_script('
                    $font = $fontMetrics->get_font("Arial, Helvetica, sans-serif", "normal");
                    $pdf->text(400, 20, "Pág $PAGE_NUM de $PAGE_COUNT", $font, 10);
                ');
            }
        </script>

        <div style="border:3px solid black;">

            <div>

                <h1 class="encabezado"> {{$tienda->Nombre}} </h1>
                <img id="logo" src="img/icono.png"  width="150"  alt="return" />
                <img id="logo2" src="img/LogoPLES.png"  width="150"  alt="return" />
            </div>

            <h2 style="text-align:center;"> Reporte de Proveedores</h2>
            <hr>

            <p>   Actualmente "{{$tienda->Nombre}}" cuenta con {{sizeof($proveedores)}} proveedores registrados.</p>
            <br><br>

            <table border="2" style="margin: 1 auto;">
                <thead>
                    <tr class="odd">
                        <th class="id"> ID proveedor </th>
                        <th class="rfc"> RFC </th>
                        <th class="nombre"> Nombre </th>
                        <th class="correo"> Correo electrónico </th>
                        <th class="telefono"> Teléfono</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ( $proveedores as $proveedores )
                          <tr id="columna">
                              <td class="codi">{{ $proveedores-> idProveedor }}</td>
                              <td>{{ $proveedores-> Rfc }}</td>
                              <td>{{ $proveedores-> Nombre }} {{ $proveedores-> ApellidoPat }} {{ $proveedores-> ApellidoMat }}</td>
                              <td>{{ $proveedores-> Correo }}</td>
                              <td>{{ $proveedores-> Telefono }}</td>
                          </tr>
                    @endforeach
                </tbody>
            </table>

            <div>

                <br><br>
                <p>"{{$tienda->Nombre}}" ------------Correo: {{$tienda->Correo}} ------------Teléfono: {{$tienda->Telefono}}------------ RFC: {{$tienda->Telefono}}
                    <br>  Dirección: {{$tienda->Calle}}  #{{$tienda->Numero}}
                    {{$tienda->Colonia}} {{$tienda->Municipio}} {{$tienda->CodigoP}} {{$tienda->Ciudad}}
                </p>

            </div>

        </div>

    </body>

</html>
