@if( Auth::user()->Nivel == 1 )

    @extends('home')

    @section('title')
        Reporte general de ventas
    @endsection

    @section('contenido')


        @if(Session::has('success'))
            <div class="alert alert-success">
                {{session('success')}}
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            </div>
        @elseif( Session::has('warning'))
            <div class="alert alert-warning">
                {{session('warning')}}
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            </div>
        @elseif( Session::has('danger'))
            <div class="alert alert-danger">
                {{session('danger')}}
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            </div>
        @endif

        <p style="font-size: 24px; text-align: center;"><strong>REPORTE GENERAL DE VENTAS</strong></p>

        <br>
        <br>

            <!-- Tabla -->
        <div class="container">
            <div class="row justify-content-center">

                <table class="table table-hover" id="tablamovimientos" name="tablamovimientos">
                    <thead  class="thead-dark">
                        <tr>
                            <th width="15%"> Código producto </th>
                            <th width="20%"> Descripción producto </th>
                            <th width="20%"> Cantidad vendida</th>
                            <th width="15%"> Total precio costo </th>
                            <th width="15%"> Total precio venta </th>
                            <th width="15%"> Ganancias totales </th>
                        </tr>
                    </thead>
                    <tbody>
                            @foreach ( $listadoproductos as $productos )
                            <tr>
                                <td>{{$productos->codigo}}</td>
                                <td>{{$productos->descripcion}} </td>
                                <td>{{$productos->cantidad}}</td>
                                <td> <?php echo bcadd( $productos->preciocosto , '0', 2); ?>
                                <td> <?php echo bcadd( $productos->precioventa , '0', 2); ?>
                                <td> <?php echo bcadd( $productos->gananciatotal , '0', 2); ?>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>

        <br>
        <br>

            <!-- Botón Exportar PDF -->
        <div class="container col-4">
            <div class="card">
                <div class="card-body">        
                    <div class="form-group row">                    
                        <div class="col">
                            <form action="{{route('generarreportegeneralventas')}}" method="get">
                                <button type="submit" id="existenciasinventario" class="btn btn-info" style="margin-left: 15%;" >Generar PDF</button>
                            </form>
                        </div>
                        <div class="col">
                            <form action="{{route('reportegeneralventasexcel')}}" method="get">
                                <button type="submit" id="existenciasinventario" class="btn btn-info" style="margin-left: 15%;" >Generar Excel</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <br>
        <br>


    @endsection

@endif
