@if( Auth::user()->Nivel == 1 )

    @extends('home')

    @section('title')
        Reporte de créditos de clientes
    @endsection

    @section('contenido')

        <script>

            var arrayregistrocreditos;

            window.onload = function( e ) {
                
                arrayregistrocreditos = {!! json_encode( $creditos->toArray(), JSON_HEX_TAG ) !!};

            }

            function EstadoActivoPresionado() {

                document.getElementById("estadoactivo").checked = true;
                FiltrarTabla( 1, "Activo" );

            }

            function EstadoInactivoPresionado() {

                document.getElementById("estadoinactivo").checked = true;
                FiltrarTabla( 0, "Inactivo" );

            }

            function FiltrarTabla( caso, textoestado ) {

                VaciarTabla();

                for( var i = 0; i < arrayregistrocreditos.length; i++ ) {
                    
                    if( arrayregistrocreditos[i].Estado == caso ) {
                        
                        document.getElementById("tablacreditosclientes").insertRow(-1).innerHTML = 
                                "<td>" + arrayregistrocreditos[i].NombreCompleto + "</td>" +
                                "<td>" + arrayregistrocreditos[i].TelefonoCliente + "</td>" +
                                "<td>" + arrayregistrocreditos[i].Limite + "</td>" +
                                "<td>" + arrayregistrocreditos[i].CanAdeudada + "</td>" +
                                "<td>" + textoestado + "</td>";

                    }                

                }

            }


            function VaciarTabla() {

                const tabla = document.getElementById('tablacreditosclientes');
                var rowCount = tabla.rows.length;         

                for  ( var x = rowCount - 1; x > 0; x-- ) { 
                    tabla.deleteRow(x); 
                } 

            }


            function RestablecerTabla() {

                VaciarTabla();

                for( var i = 0; i < arrayregistrocreditos.length; i++ ) {
                    
                    if( arrayregistrocreditos[i].Estado == 1 ) {
                        
                        document.getElementById("tablacreditosclientes").insertRow(-1).innerHTML = 
                                "<td>" + arrayregistrocreditos[i].NombreCompleto + "</td>" +
                                "<td>" + arrayregistrocreditos[i].TelefonoCliente + "</td>" +
                                "<td>" + arrayregistrocreditos[i].Limite + "</td>" +
                                "<td>" + arrayregistrocreditos[i].CanAdeudada + "</td>" +
                                "<td> Activo </td>";

                    } else if( arrayregistrocreditos[i].Estado == 0 ) {
                            
                        document.getElementById("tablacreditosclientes").insertRow(-1).innerHTML = 
                                "<td>" + arrayregistrocreditos[i].NombreCompleto + "</td>" +
                                "<td>" + arrayregistrocreditos[i].TelefonoCliente + "</td>" +
                                "<td>" + arrayregistrocreditos[i].Limite + "</td>" +
                                "<td>" + arrayregistrocreditos[i].CanAdeudada + "</td>" +
                                "<td> Inactivo </td>";

                    }            

                }

            }

        </script>

        @if(Session::has('success'))
            <div class="alert alert-success">
                {{session('success')}}
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            </div>
        @elseif( Session::has('warning'))
            <div class="alert alert-warning">
                {{session('warning')}}
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            </div>
        @elseif( Session::has('danger'))
            <div class="alert alert-danger">
                {{session('danger')}}
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            </div>
        @endif

        <p style="font-size: 24px; text-align: center;"><strong>REPORTE DE CRÉDITOS DE CLIENTES</strong></p>

        <br>
        <br>     

        <!-- OPCIONES FILTRADO -->
        <div class="container" style="margin-left:40%; margin-bottom: 10px;">
                    
            <div class="form-row align-items-center">     

                <div class="form-check form-check-inline" onclick="javascript:EstadoActivoPresionado();">
                    <input class="form-check-input" type="radio" name="radioestadocredito" id="estadoactivo" value="option1">
                    <label class="form-check-label" for="inlineRadio1"> CRÉDITOS ACTIVOS </label>
                </div>

                <div class="form-check form-check-inline" style="margin-left: 10px;" onclick="javascript:EstadoInactivoPresionado();">
                    <input class="form-check-input" type="radio" name="radioestadocredito" id="estadoinactivo" value="option2">
                    <label class="form-check-label" for="inlineRadio2"> CRÉDITOS INACTIVOS </label>
                </div>
                
                <button id="botonrestablecer" class="btn btn-info" onclick="javascript:RestablecerTabla();">Restablecer reporte</button>

            </div>
        </div>

            <!-- Tabla -->
        <div class="container">
            <div class="row justify-content-center">

                <table class="table table-hover" id="tablacreditosclientes" name="tablacreditosclientes">
                    <thead  class="thead-dark">
                        <tr>
                            <th width="20%"> Nombre del cliente </th>
                            <th width="20%"> Teléfono del cliente </th>
                            <th width="15%"> Límite del crédito </th>
                            <th width="20%"> Cantidad adeudada </th>
                            <th width="10%"> Estado del crédito </th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ( $creditos as $cre )
                            <tr>
                                <td>{{ $cre-> NombreCompleto }}</td>
                                <td>{{ $cre->TelefonoCliente }}</td>
                                <td> <?php echo bcadd( $cre->Limite , '0', 2); ?>
                                <td> <?php echo bcadd( $cre->CanAdeudada , '0', 2); ?>
                                <?php
                                    if( $cre->Estado == 1 ) {
                                ?>
                                    <td>Activo</td>
                                <?php
                                    } else {
                                ?>
                                    <td>Inactivo</td>
                                <?php
                                    }
                                ?>                            
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>

        <br>
        <br>
            <!-- Botón Exportar PDF -->
        <div class="container col-2">
            <div class="card">
                <div class="card-body">                                               
                    <form action="{{route('generarreportecreditosclientes')}}" method="get">
                        <button type="submit" id="creditosclientes" class="btn btn-info" style="margin-left: 15%" >Generar PDF</button>
                    </form>
                </div>
            </div>
        </div>

        <br>
        <br>    

    @endsection
    
@endif
