<html>

    <head>

        <style>

            #logo {

                position: fixed;
                top: 10px;
                left: 10px;
                border-radius: 4px;
                padding: 5px;
                width: 150px;

            }

            #logo2 {

                position: fixed;
                top: 20px;
                left: 850px;
                border-radius: 4px;
                padding: 5px;
                width: 150px;

            }

            .encabezado {

                background-color: #91D5F7;
                padding: 25px 10px 40px 0px;
                border: 0px;
                margin: 0px;
                text-align: center;

            }

            h2 {

                padding: 15px 10px 15px 0px;
                border: 0px;
                margin: 0px;
                color:black;
                font-variant: all-petite-caps;

            }
            hr {

                border: black 1px solid;
                background: orange;
                padding: 10px;
                border: 0px;
                margin: 0px;

            }

            table {

                font: oblique bold 100% cursive;
                border-collapse: collapse;
                text-align: left;
                width: 1000px;

            }

            .nombre {

                width: 200px;
                color: white;
                background: black;

            }

            .telefono {

                width: 110px;
                color: white;
                background: black;

            }

            .limite, .cantidad {

                width: 80px;
                color: white;
                background: black;

            }

            .estado {

                width: 100px;
                color: white;
                background: black;

            }

            #columna {

                background:#F7F5E4;

            }

            p {

                text-align: center;
                padding: 15px;
                font-family: "Segoe IU", sans-serif;

            }

        </style>

        <title>Reporte Creditos Clientes</title>

    </head>

    <body>

        <script type="text/php">
            if ( isset($pdf) ) {
                $pdf->page_script('
                    $font = $fontMetrics->get_font("Arial, Helvetica, sans-serif", "normal");
                    $pdf->text(400, 20, "Pág $PAGE_NUM de $PAGE_COUNT", $font, 10);
                ');
            }
        </script>

        <div style="border:3px solid black;">

            <div>
                <h1 class="encabezado"> {{$tienda->Nombre}} </h1>
                <img id="logo" src="img/icono.png"  width="150"  alt="return" />
                <img id="logo2" src="img/LogoPLES.png"  width="150"  alt="return" />
            </div>

            <h2 style="text-align:center;"> Reporte de Creditos Clientes</h2>
            <hr>

            <p>   Actualmente "{{$tienda->Nombre}}" cuenta con {{sizeof($creditos)}} clientes con una cuenta de crédito activo</p>
            <br><br>

            <table border="2" style="margin: 1 auto;">
            
                <thead>
                    <tr class="odd">
                        <th class="nombre"> Nombre del cliente </th>
                        <th class="telefono"> Teléfono del cliente </th>
                        <th class="limite"> Límite del crédito </th>
                        <th class="cantidad"> Cantidad adeudada </th>
                        <th class="estado"> Estado del crédito </th>
                    </tr>
                </thead>

                <tbody>
                    @foreach ( $creditos as $cre )
                        <tr id="columna">
                            <td>{{ $cre-> NombreCompleto }}</td>
                            <td>{{ $cre->TelefonoCliente }}</td>
                            <td> <?php echo bcadd( $cre->Limite , '0', 2); ?>
                            <td> <?php echo bcadd( $cre->CanAdeudada , '0', 2); ?>
                            <?php
                                if( $cre->Estado == 1 ) {
                                    ?>
                                        <td>Activo</td>
                                    <?php
                                } else {
                                    ?>
                                        <td>Inactivo</td>
                                    <?php
                                }
                            ?>
                        </tr>
                    @endforeach
                </tbody>

            </table>

            <div>
                <br><br>
                <p>"{{$tienda->Nombre}}" ------------Correo: {{$tienda->Correo}} ------------Teléfono: {{$tienda->Telefono}}------------ RFC: {{$tienda->Telefono}}
                    <br>  Dirección: {{$tienda->Calle}}  #{{$tienda->Numero}}
                    {{$tienda->Colonia}} {{$tienda->Municipio}} {{$tienda->CodigoP}} {{$tienda->Ciudad}}
                </p>
            </div>

        </div>

    </body>

</html>
