@if( Auth::user()->Nivel == 1 )

    @extends('home')

    @section('title')
        Reporte de productos
    @endsection

    @section('contenido')


        @if(Session::has('success'))
            <div class="alert alert-success">
                {{session('success')}}
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            </div>
        @elseif( Session::has('warning'))
            <div class="alert alert-warning">
                {{session('warning')}}
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            </div>
        @elseif( Session::has('danger'))
            <div class="alert alert-danger">
                {{session('danger')}}
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            </div>
        @endif

        <p style="font-size: 24px; text-align: center;"><strong>REPORTE DE PRODUCTOS BAJOS DE EXISTENCIAS EN EL INVENTARIO</strong></p>

        <br>
        <br>

            <!-- Tabla -->
        <div class="container">
            <div class="row justify-content-center">

                <table class="table table-hover" id="tablamovimientos" name="tablamovimientos">
                    <thead  class="thead-dark">
                        <tr>
                            <th width="15%"> Código producto </th>
                            <th width="15%"> Descripción producto </th>
                            <th width="10%"> Marca </th>
                            <th width="10%"> Categoría </th>
                            <th width="10%"> Presentación </th>
                            <th width="10%"> Existencias en inventario</th>
                            <th width="10%"> Cantidad mínima permitida</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ( $lista as $pro )
                            <tr>
                            <td>{{$pro->codigoproducto}}</td>
                            <td>{{$pro->nombre}}</td>
                            <td>{{ $pro->nombremarca }}</td>
                            <td>{{ $pro->nombrecategoria }}</td>
                            <td>{{ $pro->presentacion }}</td>
                            <td>{{ $pro->Cantidad }}</td>
                            <td>{{ $pro->Stockminimo }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>

        <br>
        <br>

            <!-- Botón Exportar PDF -->
        <div class="container col-2">
            <div class="card">
                <div class="card-body">
                    <form action="{{route('generarreporteproductosbajos')}}" method="get">
                        <button type="submit" id="existenciasinventario" class="btn btn-info" style="margin-left: 15%" >Generar PDF</button>
                    </form>

                </div>
            </div>
        </div>

        <br>
        <br>


    @endsection

@endif
