@if( Auth::user()->Nivel == 1 )

    @extends('home')

    @section('title')
        Reporte de salidas de inventario
    @endsection

    @section('contenido')

        @if(Session::has('success'))
            <div class="alert alert-success">
                {{session('success')}}
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            </div>
        @elseif( Session::has('warning'))
            <div class="alert alert-warning">
                {{session('warning')}}
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            </div>
        @elseif( Session::has('danger'))
            <div class="alert alert-danger">
                {{session('danger')}}
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            </div>
        @endif

        <p style="font-size: 24px; text-align: center;"><strong>REPORTE DE SALIDAS DE INVENTARIO</strong></p>

        <br>
        <br>     

            <!-- Tabla -->
        <div class="container">
            <div class="row justify-content-center">

                <table class="table table-hover" id="tablamovimientos" name="tablamovimientos">
                    <thead  class="thead-dark">
                        <tr>
                            <th width="25%"> Descripción del movimiento </th>
                            <th width="20%"> Nombre del responsable </th>
                            <th width="15%"> Código producto </th>
                            <th width="20%"> Descripción producto </th>
                            <th width="10%"> Fecha </th>
                            <th width="5%"> Cantidad Anterior </th>
                            <th width="5%"> Cantidad Actual </th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ( $movimientos as $mov )
                            <tr>
                                <td>{{ $mov-> DescripcionMovimiento }}</td>
                                <td>{{ $mov-> NombreCompleto }}</td>
                                <td>{{ $mov-> Codigo }}</td>
                                <td>{{ $mov-> DescripcionProducto }}</td>
                                <td>{{ $mov-> FechaHora }}</td>
                                <td>{{ $mov-> CantidadAnt }}</td>
                                <td>{{ $mov-> CantidadAct }}</td>

                            </tr>
                        @endforeach
                    </tbody>
                </table>            
            </div>
        </div>

        <br>
        <br>
            <!-- Botón Exportar PDF -->
        <div class="container col-2" style="margin-bottom: 50px">
            <div class="card secondary bg-light mb-3">
                <div class="card-body">                                   
                    <form action="{{route('generarreportesalidasinventario')}}" method="get">
                        <button type="submit" id="salidasinventario" class="btn btn-info" style="margin-left: 15%" >Generar PDF</button>
                    </form>
                </div>
            </div>
        </div>

        <br>
        <br>

    @endsection

@endif