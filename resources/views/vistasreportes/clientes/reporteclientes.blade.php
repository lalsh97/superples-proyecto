@if( Auth::user()->Nivel == 1 )

    @extends('home')

    @section('title')
        Reporte de clientes
    @endsection

    @section('contenido')


        @if(Session::has('success'))
            <div class="alert alert-success">
                {{session('success')}}
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            </div>
        @elseif( Session::has('warning'))
            <div class="alert alert-warning">
                {{session('warning')}}
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            </div>
        @elseif( Session::has('danger'))
            <div class="alert alert-danger">
                {{session('danger')}}
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            </div>
        @endif

        <p style="font-size: 24px; text-align: center;"><strong>REPORTE DE CLIENTES</strong></p>

        <br>
        <br>

            <!-- Tabla -->
        <div class="container">
            <div class="row justify-content-center">

                <table class="table table-hover" id="tablamovimientos" name="tablamovimientos">
                    <thead  class="thead-dark">
                    <tr>
                        <th scope="col"> ID </th>
                        <th scope="col"> Nombre completo </th>
                        <th scope="col"> Curp </th>
                        <th scope="col"> Fecha de nacimiento </th>
                        <th scope="col"> Sexo </th>
                        <th scope="col"> Teléfono </th>

                    </tr>
                </thead>
                <tbody>
                        @foreach ( $clientes as $clientes )
                            <tr>
                                <td>{{ $clientes-> idcliente }}</td>
                                <td>{{ $clientes-> nombrecompleto }}</td>
                                <td>{{ $clientes-> curp }}</td>
                                <td>{{ $clientes-> fechanac }}</td>
                                <td>{{ $clientes-> sexo }}</td>
                                <td>{{ $clientes-> telefono }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>

        <br>
        <br>

            <!-- Botón Exportar PDF -->
        <div class="container col-4">
            <div class="card">
                <div class="card-body">

                    <div class="form-group row">                    
                        <div class="col">
                            <form action="{{route('generarreporteclientes')}}" method="get">
                                <button type="submit" id="existenciasinventario" class="btn btn-info" style="margin-left: 15%" >Generar PDF</button>
                            </form>
                        </div>
                        <div class="col">
                            <form action="{{route('reporteclientesexcel')}}" method="get">
                                <button type="submit" id="existenciasinventario" class="btn btn-info" style="margin-left: 15%" >Generar Excel</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <br>
        <br>    


    @endsection

@endif
