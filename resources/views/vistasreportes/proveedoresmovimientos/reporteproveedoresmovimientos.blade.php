@if( Auth::user()->Nivel == 1 )

    @extends('home')

    @section('title')
        Reporte de los movimientos de proveedores
    @endsection

    @section('contenido')


        @if(Session::has('success'))
            <div class="alert alert-success">
                {{session('success')}}
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            </div>
        @elseif( Session::has('warning'))
            <div class="alert alert-warning">
                {{session('warning')}}
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            </div>
        @elseif( Session::has('danger'))
            <div class="alert alert-danger">
                {{session('danger')}}
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            </div>
        @endif

        <p style="font-size: 24px; text-align: center;"><strong>REPORTE DE MOVIMIENTOS DE COMPRA Y DEVOLUCIÓN A PROVEEDORES</strong></p>

        <br>
        <br>    

            <!-- Tabla -->
        <div class="container">
            <div class="row justify-content-center">

                <table class="table table-hover" id="tablamovimientos" name="tablamovimientos">
                    <thead  class="thead-dark">
                        <tr>
                            <th width="25%"> Nombre del proveedor </th>
                            <th width="10%"> Tipo movimiento </th>
                            <th width="10%"> Fecha </th>
                            <th width="20%"> Código producto </th>
                            <th width="20%"> Descripción producto </th>
                            <th width="5%"> Cant. Movida </th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ( $detalles as $det )
                            <tr>
                                <td>{{ $det-> NombreCompleto }}</td>
                                <td>{{ $det-> TipoMov }}</td>
                                <td>{{ $det-> FechaMov }}</td>
                                <td>{{ $det-> CodigoProducto }}</td>
                                <td>{{ $det-> DescripcionProducto }}</td>
                                <td>{{ $det-> CantidadMov }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>

        <br>
        <br>
            <!-- Botón Exportar PDF -->
        <div class="container col-2">
            <div class="card">
                <div class="card-body">                                               
                    <form action="{{route('generarreporteproveedoresmovimientos')}}" method="get">
                        <button type="submit" id="movimientosproveedores" class="btn btn-info" style="margin-left: 15%" >Generar PDF</button>
                    </form>
                </div>
            </div>
        </div>

        <br>
        <br>    

    @endsection

@endif