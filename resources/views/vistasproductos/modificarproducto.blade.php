@if( Auth::user()->Nivel == 1 )
    
    @extends('home')

    @section('title')
        Modificación de información de producto
    @endsection

    @section('contenido')
    
        <style>
            strong {
            
                color: red;
                font-size: 20px;
            
            }

        </style>

        <script src="{{ asset('js/FuncionesProductos/funcionesmodificarproducto.js') }}">
            //
        </script>

        @if(Session::has('success'))
            <div class="alert alert-success">
                {{session('success')}}
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            </div>
        @endif

        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header"  align="center">{{ __('Modificación de información') }}</div>

                        <div class="card-body">

                            {!!Form::open(array('name' => 'formproducto', 'url'=>'/actualizarinfoproducto/'.$producto->Codigo,'method'=>'PUT','files'=>true , 'enctype'=>'multipart/form-data' ,'autocomplete'=>'off'))!!}

                                <!-- Imagen -->
                                <div class="form-group row">
                                    {!! Form::label( 'imagen','Imagen del producto', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                    <div class="col-md-6" style="margin-left: 10%">
                                        <img width="150px" src="/images/{{ ( $producto->imagen ) }}" onerror="imgErrorProducto( this );">
                                    </div>
                                </div>

                                <!-- Nueva Imagen del producto -->
                                <div class="form-group row">
                                    {!! Form::label( 'Imagen','Cambiar imagen', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                    <div class="col-md-6">
                                        <input type="file" id="avatar" name="avatar" >
                                    </div>
                                </div>

                                <!-- Código barras -->
                                <div class="form-group row">
                                    {!! Form::label( 'codigo','Código de barras', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                    <div class="col-md-6">
                                        <input type="number" id="codigo" name="codigo" class="form-control" value="{{ ( $producto->Codigo ) }}" readonly >
                                    </div>
                                </div>

                                <!-- Descripción del producto -->
                                <div class="form-group row" >
                                    {!! Form::label( 'descripcion','Descripción del producto', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                    <div class="col-md-6">
                                        <input type="text" id="descripcion" name="descripcion" class="form-control" value="{{ ( $producto->Descripcion ) }}" readonly>
                                    </div>
                                </div>

                                <!-- Proveedor del producto -->
                                <div class="form-group row">
                                  <label for="idproveedor" class="col-md-4 col-form-label text-md-right">{{ __('Proveedor del producto ')}}</label>
                                  
                                    <div class="col-md-6">
                                        <select name="idproveedor" id="idproveedor" class="form-control">
                                            @foreach ( $proveedores as $todosproveedores )
                                                @if ( $producto->idProveedor == $todosproveedores->idproveedor )
                                                    <option value="{{$todosproveedores->idproveedor}}" selected>{{ $todosproveedores->nombrecompleto }}</option>
                                                @else
                                                    <option value="{{$todosproveedores->idproveedor}}" >{{ $todosproveedores->nombrecompleto }}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <!-- Marca del producto -->
                                <div class="form-group row">
                                    {!! Form::label('idmarca', 'Marca del producto', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                    <div class="col-md-6">
                                        <select name="idmarca" id="idmarca" class="form-control">
                                            @foreach ( $marcas as $todasmarcas )
                                                @if ( $producto->idMarca == $todasmarcas->idMarca )
                                                    <option value="{{$todasmarcas->idMarca}}" selected>{{ $todasmarcas->Descripcion }}</option>
                                                @else
                                                    <option value="{{$todasmarcas->idMarca}}" >{{ $todasmarcas->Descripcion }}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                    <!-- <a href="#" id="btn-abrir-popup" class="btn-abrir-popup" style="margin-top:10px"><i class="fas fa-plus"><span> Nueva </span></i></a> -->
                                </div>

                                <!-- Presentación del producto -->
                                <div class="form-group row">
                                    {!! Form::label('idpresentacion', 'Presentación del producto', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                    <div class="col-md-6">
                                        <select name="idpresentacion" id="idpresentacion" class="form-control">
                                            @foreach ( $presentaciones as $todaspresentaciones )
                                                @if ( $producto->idPresentacion == $todaspresentaciones->idPresentacion )
                                                    <option value="{{ $todaspresentaciones->idPresentacion }}" selected >{{ $todaspresentaciones->Medida }}</option>
                                                @else
                                                    <option value="{{ $todaspresentaciones->idPresentacion }}" >{{ $todaspresentaciones->Medida }}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                    <!-- <a href="#" id="btn-abrir-popuppresentacion" class="btn-abrir-popuppresentacion" style="margin-top:10px"><i class="fas fa-plus"><span> Nueva </span></i></a> -->
                                </div>

                                <!-- Categoria del producto -->
                                <div class="form-group row">
                                    {!! Form::label('idcategoria', 'Categoria del producto', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                    <div class="col-md-6">
                                        <select name="idcategoria" id="idcategoria" class="form-control">
                                            @foreach ( $categorias as $todascategorias )
                                                @if ( $producto->idCategoria == $todascategorias->idCategoria )
                                                    <option value="{{ $todascategorias->idCategoria }}" selected >
                                                        {{ $todascategorias->Nombre }}
                                                    </option>
                                                @else
                                                    <option value="{{ $todascategorias->idCategoria }}" >
                                                        {{ $todascategorias->Nombre }}
                                                    </option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                    <!-- <a href="#" id="btn-abrir-popupcategoria" class="btn-abrir-popupcategoria" style="margin-top:10px"><i class="fas fa-plus"><span> Nueva </span></i></a> -->
                                </div>

                                    <!-- Stock mínimo -->
                                <div class="form-group row">
                                  <label for="stockminimo" class="col-md-4 col-form-label text-md-right">{{ __('Cantidad mínima inventario ')}}<strong>*</strong></label>
                                    <div class="col-md-6">
                                        <input type="number" id="stockminimo" name="stockminimo" placeholder="Ingrese la cantidad mínima en inventario" class="form-control" max="9999999" min="1" step="1" value="{{ ( $producto->Stockminimo ) }}"
                                        oninvalid="setCustomValidity('La cantidad mínima de existencia en inventario es obligatoria')"
                                        oninput="setCustomValidity('')">
                                    </div>
                                </div>


                                <!-- Impuesto del producto -->
                                <div class="form-group row">
                                    {!! Form::label('impuesto','IVA' , array( 'class' => 'col-md-4 col-form-label text-md-right' )) !!}
                                    <div class="col-md-6">
                                        {!! Form::select( 'impuesto', array('0' => 'No', '1' => 'SÍ' ), $producto->IVA, array( 'class' => 'form-control' ) ) !!}
                                    </div>
                                </div>

                                <!-- Botones -->
                                <div class="form-group row">
                                    <div class="col-md-4 col-form-label text-md-right">
                                        <button name="botonformmodificar" value="Modificar" type="submit" class="btn btn-success" style="margin-right:50px">Modificar</button>
                                    </div>
                                    <div class="col-md-4 col-form-label text-md-right">
                                        <button name="botonformmodificar" value="Cancelar" type="submit" class="btn btn-danger" style="margin-left:250px">Cancelar</button>
                                    </div>
                                </div>

                            {!!Form::close()!!}

                        </div>
                    </div>
                </div>
            </div>
        </div>

    @endsection

@endif
