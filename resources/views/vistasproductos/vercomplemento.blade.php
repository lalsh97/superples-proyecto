@if( Auth::user()->Nivel == 1 )

    @extends('home')

    @section('title')
        Listado de productos registro paquete
    @endsection

    @section('contenido')

        <!-- BUSCADOR -->
        <table  class="table table-responsive table-striped vertabla" style="margin: auto; width: 975px; ">
            <tr>
                <td>
                    <div class="panel panel-success" style="margin: auto; width: 500px; border-collapse: separate; ">
                        <form action="/producto2/{{ ( $codigo->codigo ) }}" method="PUT" onsubmit="return showLoad()">
                            <input class="input" size="35"type="text" name="descripcion" class="form-control" placeholder="Código / Descripción / Marca" required="required">
                            <button type="submit" class="btn btn-success">BUSCAR</button>
                            <br>
                        </form>
                    </div>
                </td>
                <td>
                    <a href="/producto2/{{ ( $codigo->codigo ) }}" class="btn btn-warning">Restaurar búsqueda</a>
                </td>
            </tr>
        </table>

        <br>
        <br>

        @if (isset($buscar))

            <div class="container">
                <div class="row justify-content-center">

                    <table class="table table-hover">
                        <thead  class="thead-dark">
                            <tr>
                                <th scope="col"> Código de barra </th>
                                <th scope="col"> Marca </th>
                                <th scope="col"> Presentación </th>
                                <th scope="col"> Nombre del proveedor </th>
                                <th scope="col"> Categoría </th>
                                <th scope="col"> Descripción </th>
                                <th scope="col"> Precio costo </th>
                                <th scope="col"> Precio venta </th>
                                
                                <?php
                                    if( Auth::user()->Nivel == 1 ) {
                                ?>
                                    <th scope="col>" colspan="2"> Acciones </th>
                                <?php
                                    }
                                ?>
                            </tr>
                        <tbody>
                            <tr class="table-info">
                                <td>{{ $codigo-> codigo }}</td>
                                <td>{{ $codigo-> nombremarca }}</td>
                                <td>{{ $codigo-> nombrepresentacion }}</td>
                                <td>{{ $codigo-> nombrecompleto }}</td>
                                <td>{{ $codigo-> nombrecategoria }}</td>
                                <td>{{ $codigo-> descripcion }}</td>
                                <td> <?php echo bcadd( $codigo->preciocosto , '0', 2); ?> </td>
                                <td> <?php echo bcadd( $codigo->precioventa , '0', 2); ?> </td>
                                <td>
                                    <a href="{{ url('/home' ) }}">
                                        <button type="submit" class="btn btn-danger"> Cancelar </button>
                                    </a>
                                </td>
                            </tr>
                                
                            @foreach( $buscar as $productos )
                            <tr>
                                <td>{{ $productos-> codigo }}</td>
                                <td>{{ $productos-> nombremarca }}</td>
                                <td>{{ $productos-> nombrepresentacion }}</td>
                                <td>{{ $productos-> nombrecompleto }}</td>
                                <td>{{ $productos-> nombrecategoria }}</td>
                                <td>{{ $productos-> descripcion }}</td>
                                <td> <?php echo bcadd( $productos->preciocosto , '0', 2); ?> </td>
                                <td> <?php echo bcadd( $productos->precioventa , '0', 2); ?> </td>
                                <?php
                                    if( Auth::user()->Nivel == 1 ) {
                                ?>
                                    <td>
                                        <a href="/registrarpaquete/{{ ( $codigo->codigo ) }}/{{ ( $productos->codigo )}} ">
                                            <button type="submit" class="btn btn-info"> Seleccionar </button>
                                        </a>
                                    </td>
                                <?php
                                    }
                                ?>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    
                    <!-- Nombre del producto -->
                    <center>{{ $buscar->appends(Request::only('Codigo','Descripcion'))->links() }}</center>
                </div>
            </div>
        
        @endif

    @endsection

@endif
