@extends('home')

@section('title')
    Listado de productos
@endsection

@section('contenido')

    <table  class="table table-responsive table-striped vertabla" style="margin: auto; width: 975px; ">
        <tr>
            <td>  
                <div class="panel panel-success" style="margin: auto; width: 500px; border-collapse: separate; ">
                    <form action="Seleccionarproducto" method="get" onsubmit="return showLoad()">
                        <input class="input" size="35"type="text" name="descripcion" class="form-control" placeholder="Codigo / Descripcion / Marca" required="required">
                        <button type="submit" class="btn btn-success">BUSCAR</button>
                        <br>
                    </form>
                </div>
            </td>
            <td>
                <a href="{{url('Seleccionarproducto')}}" class="btn btn-warning">Restaurar busqueda</a>
            </td>
        </tr>
    </table>

    <br>
    <br>

    @if (isset($buscar))

        <div class="container">
            <div class="row justify-content-center">

                <table class="table table-hover">
                    <thead  class="thead-dark">
                        <tr>
                            <th>Código</th>
                            <th>Descripcion</th>
                            <th>Otro :V</th>
                            <?php
                                if( Auth::user()->Nivel == 1 ) {
                            ?>
                                <th scope="col>" colspan="2"> Acciones </th>
                            <?php
                                }
                            ?>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($buscar as $buscars)
                            <tr>
                                <td>{{$buscars->codigo}}</td>
                                <td>{{$buscars->descripcion}} </td>
                                <td>{{ $buscars-> nombremarca }}</td>
                                <td>
                                    <?php
                                        if( Auth::user()->Nivel == 1 ) {
                                            ?>
                                                <a href="{{ url('/modificarproducto/'.$buscars->codigo ) }}">
                                                    <button type="submit" class="btn btn-info"> Modificar </button>
                                                </a>
                                            <?php
                                        }
                                    ?>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                
                <center>{{ $buscar->appends(Request::only('Codigo','Descripcion'))->links() }}</center>
            </div>
        </div>
    
    @endif

@endsection
