@if( Auth::user()->Nivel == 1 )

    @extends('home')

    @section('title')
        Registro rápido de producto
    @endsection

    @section('contenido')

        <style>

            strong {
            
                color: red;
                font-size: 20px;

            }

        </style>

        <script>
            
            
            window.onload = function(e) { 

                console.log("onload");

                var caso = {!! json_encode($caso) !!};

                console.log( "Valor variable: ", caso );

                if( caso == "close") {
                    window.close();
                }

            }

        </script>


        @if(Session::has('success'))
            <div class="alert alert-success">
                {{session('success')}}
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            </div>
        @elseif(Session::has('danger'))
            <div class="alert alert-danger">
                {{session('danger')}}
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            </div>
        @elseif(Session::has('warning'))
            <div class="alert alert-warning">
                {{session('warning')}}
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            </div>
        @endif

        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header"  align="center">{{ __('Registro de producto') }}</div>
                        <div class="card-body">

                            {!!Form::open(array('name' => 'formproducto', 'url'=>'insertarproducto','method'=>'POST','files'=>true , 'enctype'=>'multipart/form-data','autocomplete'=>'off'))!!}
                                
                                <!-- Imagen del producto -->
                                <div class="form-group row">
                                    {!! Form::label( 'avatar','Imagen del producto', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                    <div class="col-md-6">
                                    <input type="file" id="avatar" name="avatar"  value="{{ old('avatar')}}">
                                    </div>
                                </div>

                                <!-- Descripción del producto -->
                                <div class="form-group row" >
                                    <label for="descripcion" class="col-md-4 col-form-label text-md-right">{{ __('Descripción del producto ')}}<strong>*</strong></label>
                                  
                                    <div class="col-md-6">
                                    <input type="text" id="descripcion" name="descripcion"  placeholder="Ingrese la descripción del producto" class="form-control"
                                        value="{{ old('descripcion') }}" maxlength="45" minlength="1" required pattern="[\w\s\u00C0-\u00FF]{1,45}"
                                        oninvalid="setCustomValidity('La descripción es obligatorio y debe tener como máximo 45 carácteres')"
                                        oninput="setCustomValidity('')">
                                    </div>
                                </div>

                                <!-- Proveedor del producto -->
                                <div class="form-group row">
                                  <label for="idproveedor" class="col-md-4 col-form-label text-md-right">{{ __('Proveedor del producto ')}}<strong>*</strong></label>
                                    <div class="col-md-6">
                                        {!! Form::select('idproveedor', $proveedores, null, array( 'class' => 'form-control', 'required' => 'true',
                                            'oninvalid' => 'setCustomValidity("Debe seleccionar un proveedor")', 'oninput' => 'setCustomValidity("")' ) ) !!}
                                    </div>
                                </div>

                                <!-- Marca del producto -->
                                <div class="form-group row">
                                    <label for="idmarca" class="col-md-4 col-form-label text-md-right">{{ __('Marca del producto ')}}<strong>*</strong></label>
                                  
                                    <div class="col-md-6">
                                        {!! Form::select('idmarca', $marcas, null, array( 'class' => 'form-control', 'required' => 'true',
                                            'oninvalid' => 'setCustomValidity("Debe seleccionar una marca")', 'oninput' => 'setCustomValidity("")' ) ) !!}
                                    </div>
                                    <a href="#" id="btn-abrir-popup" class="btn-abrir-popup" style="margin-top:10px"><i class="fas fa-plus"><span> Nueva </span></i></a>
                                </div>

                                <!-- Presentación del producto -->
                                <div class="form-group row">
                                    <label for="idpresentacion" class="col-md-4 col-form-label text-md-right">{{ __('Presentación del producto ')}}<strong>*</strong></label>

                                    <div class="col-md-6">
                                        {!! Form::select('idpresentacion', $presentaciones, null, array( 'class' => 'form-control', 'required' => 'true',
                                            'oninvalid' => 'setCustomValidity("Debe seleccionar una presentación")', 'oninput' => 'setCustomValidity("")' ) ) !!}
                                    </div>
                                    <a href="#" id="btn-abrir-popuppresentacion" class="btn-abrir-popuppresentacion" style="margin-top:10px"><i class="fas fa-plus"><span> Nueva </span></i></a>
                                </div>

                                <!-- Categoria del producto -->
                                <div class="form-group row">
                                    <label for="idcategoria" class="col-md-4 col-form-label text-md-right">{{ __('Categoría del producto ')}}<strong>*</strong></label>

                                    <div class="col-md-6">
                                        {!! Form::select('idcategoria', $categorias, null, array( 'class' => 'form-control', 'required' => 'true',
                                            'oninvalid' => 'setCustomValidity("Debe seleccionar una categoria")', 'oninput' => 'setCustomValidity("")' ) ) !!}
                                    </div>
                                    <a href="#" id="btn-abrir-popupcategoria" class="btn-abrir-popupcategoria" style="margin-top:10px"><i class="fas fa-plus"><span> Nueva </span></i></a>
                                </div>

                                <!-- Stock mínimo -->
                                <div class="form-group row">
                                    <label for="stockminimo" class="col-md-4 col-form-label text-md-right">{{ __('Cantidad mínima inventario ')}}<strong>*</strong></label>
                                    
                                    <div class="col-md-6">
                                        <input type="number" id="stockminimo" name="stockminimo" placeholder="Ingrese la cantidad mínima en inventario" class="form-control" max="9999999" min="1"
                                        step="1" pattern="^\d" required
                                        value="{{ old('stockminimo')}}"
                                        oninvalid="setCustomValidity('La cantidad mínima de existencia en inventario es obligatoria')"
                                        oninput="setCustomValidity('')">
                                    </div>
                                </div>


                                <!-- Impuesto del producto -->
                                <div class="form-group row">
                                    {!! Form::label('impuesto','IVA' , array( 'class' => 'col-md-4 col-form-label text-md-right' )) !!}
                                    <div class="col-md-6">
                                        {!! Form::select( 'impuesto', array('0' => 'No', '1' => 'SÍ' ), '0', array( 'class' => 'form-control') ) !!}
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-md-4 col-form-label text-md-right">
                                        <button name="botonformproductonocodigo" value="RegistrarRapido" type="submit" class="btn btn-success" style="margin-right:50px">Registrar</button>
                                    </div>
                                    <div class="col-md-4 col-form-label text-md-right">
                                        <button name="botonformproductonocodigo" value="Cancelar" type="submit" class="btn btn-danger" style="margin-left:250px" formnovalidate>Cancelar</button>
                                    </div>
                                </div>

                            {!!Form::close()!!}


                            <!-- Ventana Emergente Marca-->
                            <div class="overlayventana" id="overlayventana">
                                <div class="popup" id="popup">
                                <a href="#" id="btn-cerrar-popup" class="btn-cerrar-popup"><i class="fas fa-times"></i></a>
                                    <div class="card">
                                        <div class="card-header"  align="center">{{ __('Registro de marca') }}</div>
                                        <div class="card-body">
                                            
                                            {!!Form::open(array('name' => 'formcategoria', 'url'=>'añadirmarca','method'=>'POST', 'files'=>true , 'enctype'=>'multipart/form-data' ,'autocomplete'=>'off'))!!}
                                                                                                                    
                                                <div class="form-group row">
                                                    {!! Form::label( 'avatar','Imagen de la marca', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                                    <div class="col-md-6">
                                                    <input type="file" id="avatar" name="avatar"  value="{{ old('avatar')}}" >
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="descripcion" class="col-md-4 col-form-label text-md-right">{{ __('Descripción') }}</label>
                                                    <div class="col-md-8">
                                                        {!! Form::text( 'descripcion', null, array( 'class' => 'form-control', 'placeholder' => 'Ingrese la descripción' ) ) !!}
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <div class="col-md-4 col-form-label text-md-right">
                                                        <button name="botonformmarca" value="Registrar" type="submit" class="btn btn-success" style="margin-left:200px" style="margin-right:50px">Registrar</button>
                                                    </div>
                                                </div>
                                            {!!Form::close()!!}

                                        </div>
                                    </div>
                                </div>
                            </div>


                            <!-- Ventana Emergente Presentación-->
                            <div class="overlayventanapresentacion" id="overlayventanapresentacion">
                                <div class="popuppresentacion" id="popuppresentacion">
                                <a href="#" id="btn-cerrar-popuppresentacion" class="btn-cerrar-popuppresentacion"><i class="fas fa-times"></i></a>
                                    <div class="card">
                                        <div class="card-header"  align="center">{{ __('Registro de presentación') }}</div>
                                        <div class="card-body">

                                            {!!Form::open(array('name' => 'formpresentacion', 'url'=>'añadirpresentacion','method'=>'POST','files'=>true , 'enctype'=>'multipart/form-data' ,'autocomplete'=>'off'))!!}
                                                                                                                    
                                                <div class="form-group row">
                                                    {!! Form::label( 'avatar','Imagen de la presentación', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                                    <div class="col-md-6">
                                                    <input type="file" id="avatar" name="avatar"  value="{{ old('avatar')}}" >
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="medida" class="col-md-4 col-form-label text-md-right">{{ __('Medida') }}</label>
                                                    <div class="col-md-6">
                                                        {!! Form::text('medida',null, array( 'class' => 'form-control', 'placeholder' => 'Ingrese la medida' ) ) !!}
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <div class="col-md-4 col-form-label text-md-right">
                                                        <button name="botonformpresentacion" value="Registrar" type="submit" class="btn btn-success" style="margin-left:200px" style="margin-right:50px">Registrar</button>
                                                    </div>
                                                </div>
                                            {!!Form::close()!!}

                                        </div>
                                    </div>
                                </div>
                            </div>


                            <!-- Ventana Emergente Categoria -->
                            <div class="overlayventanacategoria" id="overlayventanacategoria">
                                <div class="popupcategoria" id="popupcategoria">
                                <a href="#" id="btn-cerrar-popupcategoria" class="btn-cerrar-popupcategoria"><i class="fas fa-times"></i></a>
                                    <div class="card">
                                        <div class="card-header"  align="center">{{ __('Registro de categoría') }}</div>
                                        <div class="card-body">
                                            
                                            {!!Form::open(array('name' => 'formcategoria', 'url'=>'añadircategoria','method'=>'POST','files'=>true , 'enctype'=>'multipart/form-data' ,'autocomplete'=>'off'))!!}
                                                                                                                    
                                                <div class="form-group row">
                                                    {!! Form::label( 'avatar','Imagen de la categoría', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                                    <div class="col-md-6">
                                                    <input type="file" id="avatar" name="avatar"  value="{{ old('avatar')}}" >
                                                    </div>
                                                </div>
                                        
                                                <div class="form-group row">
                                                    <label for="nombre" class="col-md-4 col-form-label text-md-right">{{ __('Nombre') }}</label>

                                                    <div class="col-md-6">
                                                    {!! Form::text('nombre', null, array( 'class' => 'form-control', 'placeholder' => 'Ingrese el nombre' ) ) !!}
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="descripcion" class="col-md-4 col-form-label text-md-right">{{ __('Descripción') }}</label>
                                                    <div class="col-md-6">
                                                        {!! Form::text('descripcion', null, array( 'class' => 'form-control', 'placeholder' => 'Ingrese la descripción' ) ) !!}
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <div class="col-md-4 col-form-label text-md-right">
                                                        <button name="botonformcategoria" value="Registrar" type="submit" class="btn btn-success" style="margin-left:200px" style="margin-right:50px">Registrar</button>
                                                    </div>
                                                </div>
                                            {!!Form::close()!!}
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- Necesaria para abrir los popups -->
                            <script src="{{ asset('js/popup.js') }}"></script>

                        </div>
                    </div>
                </div>
            </div>
        </div>

    @endsection

@endif
