@extends('home')

@section('title')
    Listado de productos
@endsection

@section('contenido')

 <table  class="table table-responsive table-striped vertabla" style="margin: auto; width: 975px; ">
<tr> <td><a href="formulario_usuario" class="btn btn-info butons" >Registrar Nuevo Producto</a></td>
<td>  <div class="panel panel-success" style="margin: auto; width: 500px; border-collapse: separate; ">

            <form action="Productos" method="get" onsubmit="return showLoad()">


                <input class="input" size="35"type="text" name="descripcion" class="form-control" placeholder="Codigo / Descripcion" required="required">


            <button type="submit" class="btn btn-success">BUSCAR</button>
        <br>
      </form></td>
      <td>
            <a href="{{url('Productos')}}" class="btn btn-warning">Restaurar busqueda</a>
        </td>
    </div></tr></table>

    @if (isset($buscar))

        <div class="panel panel-success">
            <div class="panel-body">

                <div class='table-responsive'>
                  <table  class="table table-responsive table-striped vertabla" style="margin: auto; width: 900px; border-collapse: separate; ">
                    <tr class="uno">
                          <th colspan="6"> Resultados de la busqueda </th>
                        </tr>
                      <tr>
                        <th>Código</th>
                        <th>Descripcion</th>
                        <th>Otro</th>
                      </tr>
                    <tbody>

                    @foreach($buscar as $buscars)
                        <tr>
                            <td>{{$buscars->Codigo}}</td>
                            <td>{{$buscars->Descripcion}} </td>
                            <td>{{$buscars->PrecioCosto}}{{$buscars->PrecioVenta}}</td>
                            <td><a href="actualizar/{{$buscars->Codigo}}" class='btn btn-success butons'>Actualizar</a></td>
                            <td><a href="comprobar_usuario/{{$buscars->Codigo}}"class='btn btn-danger butons'>Eliminar</a></td>
                       </tr>
                    @endforeach
                    </tbody>
                        </table>
                        <center>{{ $buscar->appends(Request::only('IdUsuariofk','Telefono'))->links() }}</center>
                    </div>

            </div>

        </div>
    @endif

@endsection
