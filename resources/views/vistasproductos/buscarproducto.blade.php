@extends('home')

    @section('title')
        Búsqueda de producto
    @endsection

    @section('contenido')

        <script src="{{ asset('js/FuncionesProductos/funcionesbuscarproducto.js') }}">
            //           
        </script>

        @if(Session::has('success'))
            <div class="alert alert-success">
                {{session('success')}}
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            </div>
        @elseif( Session::has('warning'))
            <div class="alert alert-warning">
                {{session('warning')}}
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            </div>
        @elseif( Session::has('danger'))
            <div class="alert alert-danger">
                {{session('danger')}}
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            </div>
        @endif


        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header"  align="center">{{ __('Búsqueda de producto') }}</div>

                        <div class="card-body">                        

                                <!-- Buscar producto por código de barras -->
                                <div class="form-group"  style="margin-bottom: 10px">
                                    <div class="form-check"   style="margin-bottom: 10px">
                                        {!! Form::label( 'codigo','Buscar producto por código de barras', array( 'class' => 'col-md-8 col-form-label', 'onclick' => 'cambiarcheckcodigo2(this);' ) ) !!}
                                        <input type="checkbox" name="checkcodigo" id="checkcodigo" value="1" onchange="javascript:showContent()"  onclick="cambiarcheckcodigo(this);" class="col-md-2 col-form-label" style="margin-top:10px" />
                                    </div>

                                    <div class="form-group"  id="contentcodigo" style="display: none;">
                                        <input type="text" id="codigo" name="codigo" class="form-control-sm" value="{{  old('codigo')  }}" pattern="^[0-9]{1,13}$"
                                            oninvalid="setCustomValidity('El código de barras debe constar únicamente de números y tener como máximo 13 digitos')"
                                            onkeypress="return ( event.charCode >= 48 && event.charCode <= 57 )"
                                            oninput="setCustomValidity('')" style="margin-left: 35%">
                                    </div>
                                </div>

                                <!-- Buscar productos por proveedor -->
                                <div class="form-group"   style="margin-bottom: 10px">
                                    <div class="form-check"  style="margin-bottom: 10px">
                                        {!! Form::label( 'idproveedor','Buscar productos por proveedor', array( 'class' => 'col-md-8 col-form-label', 'onclick' => 'cambiarcheckproveedores2(this);' ) ) !!}
                                        <input type="checkbox" name="checkproveedores" id="checkproveedores" value="1" onchange="javascript:showContent()" onclick="cambiarcheckproveedores(this);" class="col-md-2 col-form-label" style="margin-top:10px" />
                                    </div>

                                    <div class="form-group"  id="contentproveedores" style="display: none;">
                                        <select name="idproveedor" id="idproveedor" class="form-control-sm" style="margin-left: 25%">
                                            <option value=""> Seleccione a un proveedor </option>
                                            @foreach ( $proveedores as $todosproveedores )
                                                    <option value="{{ $todosproveedores->idProveedor }}" >
                                                        {{ $todosproveedores->Nombre.' '.$todosproveedores->ApellidoPat.' '.$todosproveedores->ApellidoMat }}
                                                    </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <!-- Buscar productos por categoria -->
                                <div class="form-group"   style="margin-bottom: 10px">
                                    <div class="form-check"  style="margin-bottom: 10px">
                                        {!! Form::label( 'idcategoria','Buscar productos por categoría', array( 'class' => 'col-md-8 col-form-label', 'onclick' => 'cambiarcheckcategorias2(this);' ) ) !!}
                                        <input type="checkbox" name="checkcategorias" id="checkcategorias" value="1" onchange="javascript:showContent()"  onclick="cambiarcheckcategorias(this);" class="col-md-2 col-form-label" style="margin-top:10px" />
                                    </div>

                                    <div class="form-group"  id="contentcategorias" style="display: none;">
                                        <select name="idcategoria" id="idcategoria" class="form-control-sm"  style="margin-left: 25%">
                                            <option value=""> Seleccione una categoría </option>
                                            @foreach ( $categorias as $todascategorias )
                                                    <option value="{{ $todascategorias->idCategoria }}" >
                                                        {{ $todascategorias->Nombre }}
                                                    </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>                        

                                <!-- Botones -->
                                <div class="form-group row" style="margin-top: 20px">
                                    <div class="col-md-4 col-form-label text-md-right">
                                        <button name="botonbuscarproducto" value="Buscar" type="submit" class="btn btn-success" style="margin-right:50px" onclick="javascript:IrBuscarProducto();">Buscar</button>
                                    </div>
                                    <div class="col-md-4 col-form-label text-md-right">
                                        <a href="/home">                                            
                                            <button name="botonbuscarproducto" value="Cancelar" type="submit" class="btn btn-danger" style="margin-left:250px">Cancelar</button>
                                        </a>
                                    </div>
                                </div>
                    
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>

    @endsection
