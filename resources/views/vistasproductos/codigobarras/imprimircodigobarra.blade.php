@extends('home')

@section('title')
    Imprimir Código Barra
@endsection

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

    <script>
                
        $(document).ready(function () {
            $('#botonimprimir').click(function () {
                $('#botonimprimir').hide();
                window.print();
                return false;
            });

        });

    </script>

@section('contenido')
          
    <div class="container" style="margin-top: 1%">
    
        <div class="row justify-content-center">
        
            <div class="form">
        
                @foreach ( $codigoproducto as $producto )

                    <div class="col-2" id="divcodigobarra" style="width: 20%">                    
                        <!-- {!! DNS1D::getBarcodeHTML( $producto->Codigo, 'C128' ) !!} -->
                        {!! DNS1D::getBarcodeSVG( $producto->Codigo, 'EAN13' ) !!}
                                                
                    </div>

                    <br><br>

                    <div class="col-2" style="margin-left: 60px">
                        <a > <input type="button" value="IMPRIMIR" class="btn btn-success" id="botonimprimir"> </a>
                    </div>

                @endforeach
        
            </div>

        </div> 
        
    </div>

@endsection