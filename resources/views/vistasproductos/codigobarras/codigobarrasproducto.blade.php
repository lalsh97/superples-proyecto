@extends('home')

@section('title')
    Generación código barras producto
@endsection



@section('contenido')


    <link rel="STYLESHEET" href="css/estilostooltipylistados.css">


    <script>

        function FiltrarTablaCodigo() {
            
            if( document.getElementById("codigofiltrado").value != "" ) {
                
                RealizarFiltradoTablaCodigo( document.getElementById("codigofiltrado").value );

            } else {
                        
                document.getElementById("textoalerta").value = "Por favor ingrese el código de barras a buscar";
                document.getElementById("contentalert").style.display = "block";

            }

        }

        function FiltrarTablaDescripcion() {

            
            if( document.getElementById("descripcionfiltrado").value != "" ) {
                
                RealizarFiltradoTablaDescripcion( document.getElementById("descripcionfiltrado").value );

            } else {
                        
                document.getElementById("textoalerta").value = "Por favor ingrese la descripción del producto a buscar";
                document.getElementById("contentalert").style.display = "block";

            }

        }

        function RealizarFiltradoTablaCodigo( codigo ) {

            document.getElementById("descripcionfiltrado").value = "";
            document.getElementById("resultadofiltradodescripcion").innerText = "";

            var arrayregistroproductos = {!! json_encode( $registroproductos->toArray(), JSON_HEX_TAG ) !!};

            VaciarTabla();

            var auxcodigoproducto = "";

            for( var i = 0; i < arrayregistroproductos.length; i++ ) {

                auxcodigoproducto = arrayregistroproductos[i].codigo;

                if( auxcodigoproducto == codigo ) {                          

                    document.getElementById("tablaproductos").insertRow(-1).innerHTML = 
                        "<td>" + arrayregistroproductos[i].codigo + "</td>" +
                        "<td>" + arrayregistroproductos[i].nombremarca + "</td>" +
                        "<td>" + arrayregistroproductos[i].nombrepresentacion + "</td>" +
                        "<td>" + arrayregistroproductos[i].nombrecompleto + "</td>" +
                        "<td>" + arrayregistroproductos[i].nombrecategoria + "</td>" +
                        "<td>" + arrayregistroproductos[i].descripcion + "</td>" +
                        "<td>" + arrayregistroproductos[i].preciocosto + "</td>" +
                        "<td>" + arrayregistroproductos[i].precioventa + "</td>" +
                        "<td>" + 
                            "<a href=/imprimircodigobarra/" + arrayregistroproductos[i].codigo + " }}>" +
                                "<button type='submit' class='btn btn-info'> Imprimir </button>" +
                            "</a>" +
                        "</td>";

                }
            
            }
            
            const tabla = document.getElementById('tablaproductos');
            var rowCount = tabla.rows.length;

            if( rowCount == 1 ) {
                        
                document.getElementById("textoalerta").value = "Ningún producto coincide con el código buscado";
                document.getElementById("contentalert").style.display = "block";


            } else {
                
                document.getElementById("resultadofiltradocodigo").innerHTML = "<p> Total de coincidencias: <strong>"+ ( rowCount - 1) + "</strong> </p>";

            }


        }

        function RealizarFiltradoTablaDescripcion( descripcion ) {

            document.getElementById("codigofiltrado").value = "";
            document.getElementById("resultadofiltradocodigo").innerText = "";

            var arrayregistroproductos = {!! json_encode( $registroproductos->toArray(), JSON_HEX_TAG ) !!};

            VaciarTabla();

            var auxcodigoproducto = "";

            for( var i = 0; i < arrayregistroproductos.length; i++ ) {

                auxdescripcionproducto = arrayregistroproductos[i].descripcion;
                auxdescripcionproducto = auxdescripcionproducto.toUpperCase();
                descripcion = descripcion.toUpperCase();

                if( auxdescripcionproducto.includes( descripcion ) ) {
                                              
                    document.getElementById("tablaproductos").insertRow(-1).innerHTML = 
                        "<td>" + arrayregistroproductos[i].codigo + "</td>" +
                        "<td>" + arrayregistroproductos[i].nombremarca + "</td>" +
                        "<td>" + arrayregistroproductos[i].nombrepresentacion + "</td>" +
                        "<td>" + arrayregistroproductos[i].nombrecompleto + "</td>" +
                        "<td>" + arrayregistroproductos[i].nombrecategoria + "</td>" +
                        "<td>" + arrayregistroproductos[i].descripcion + "</td>" +
                        "<td>" + arrayregistroproductos[i].preciocosto + "</td>" +
                        "<td>" + arrayregistroproductos[i].precioventa + "</td>" +
                        "<td>" + 
                            "<a href=/imprimircodigobarra/" + arrayregistroproductos[i].codigo + " }}>" +
                                "<button type='submit' class='btn btn-info'> Imprimir </button>" +
                            "</a>" +
                        "</td>";

                }
            
            }
            
            const tabla = document.getElementById('tablaproductos');
            var rowCount = tabla.rows.length;

            if( rowCount == 1 ) {
                        
                document.getElementById("textoalerta").value = "Ningún producto coincide con la descripción buscada";
                document.getElementById("contentalert").style.display = "block";


            } else {
                
                document.getElementById("resultadofiltradodescripcion").innerHTML = "<p> Total de coincidencias: <strong>"+ ( rowCount - 1) + "</strong> </p>";

            }


        }

        function VaciarTabla() {
            
            document.getElementById("codigofiltrado").value = "";
            document.getElementById("resultadofiltradocodigo").innerHTML = "";

            document.getElementById("descripcionfiltrado").value = "";
            document.getElementById("resultadofiltradodescripcion").innerHTML = "";

            const tabla = document.getElementById('tablaproductos');
            var rowCount = tabla.rows.length;         

            for  ( var x = rowCount - 1; x > 0; x-- ) { 
                tabla.deleteRow(x); 
            } 

        }

        function RestablecerTabla() {

            document.getElementById("codigofiltrado").value = "";
            document.getElementById("descripcionfiltrado").value = "";
            
            document.getElementById("resultadofiltradocodigo").innerText = "";
            document.getElementById("resultadofiltradodescripcion").innerText = "";
        
            var arrayregistroproductos = {!! json_encode( $registroproductos->toArray(), JSON_HEX_TAG ) !!};
            
            VaciarTabla();

            for( var i = 0; i < arrayregistroproductos.length; i++ ) {                           

                document.getElementById("tablaproductos").insertRow(-1).innerHTML = 
                    "<td>" + arrayregistroproductos[i].codigo + "</td>" +
                    "<td>" + arrayregistroproductos[i].nombremarca + "</td>" +
                    "<td>" + arrayregistroproductos[i].nombrepresentacion + "</td>" +
                    "<td>" + arrayregistroproductos[i].nombrecompleto + "</td>" +
                    "<td>" + arrayregistroproductos[i].nombrecategoria + "</td>" +
                    "<td>" + arrayregistroproductos[i].descripcion + "</td>" +
                    "<td>" + arrayregistroproductos[i].preciocosto + "</td>" +
                    "<td>" + arrayregistroproductos[i].precioventa + "</td>" +
                    "<td>" + 
                        "<a href=/imprimircodigobarra/" + arrayregistroproductos[i].codigo + " }}>" +
                            "<button type='submit' class='btn btn-info'> Imprimir </button>" +
                        "</a>" +
                    "</td>";
            
            }

        }

        function OcultarAlerta() {

            if( document.getElementById("contentalert").style.display == "none" ) {
                document.getElementById("contentalert").style.display = "block";
            } else {
                document.getElementById("contentalert").style.display = "none";
            }

        }

    </script>

    @if(Session::has('success'))
        <div class="alert alert-success">
            {{session('success')}}
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        </div>
    @elseif(Session::has('danger'))
        <div class="alert alert-danger">
            {{session('danger')}}
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        </div>
    @elseif(Session::has('warning'))
        <div class="alert alert-warning">
            {{session('warning')}}
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        </div>
    @endif

    

        <!-- DIV FILTRADO -->
    <div class="container" style="margin-bottom: 10px;">
        
        <!-- DIV FILTRAR POR CODIGO -->
        <div class="form-row align-items-center">

            <!-- FILTRAR POR CODIGO -->
            <div class="col" style="margin-left: 15%; margin-bottom:10px;">
                <label for="codigofiltrado"> Ingrese el código de barras a buscar </label>
                <input type="text" class="form-control-sm" id="codigofiltrado" style="margin-left: 10px;">
                
                <a style="margin-left: 5%;"> <button  class="btn btn-primary" id="botonfiltradocodigo"    onclick="javascript:FiltrarTablaCodigo();">Filtrar por código</button> </a>
                
                <label id="resultadofiltradocodigo" style="margin-left: 20px;"> </label>
            </div>
            
        </div>

        <!-- DIV FILTRAR POR DESCRIPCION -->
        <div class="form-row align-items-center">
            

            <!-- FILTRAR POR DESCRIPCION -->
            <div class="col" style="margin-left: 15%; margin-bottom:10px;">
                <label for="descripcionfiltrado"> Ingrese la descripción a buscar </label>
                <input type="text" class="form-control-sm" id="descripcionfiltrado" style="margin-left: 45px;">
                
                <a style="margin-left: 5%;"> <button  class="btn btn-primary" id="botonfiltradodescripcion"    onclick="javascript:FiltrarTablaDescripcion();">Filtrar por descripción</button> </a>
                
                <label id="resultadofiltradodescripcion" style="margin-left: 20px;"> </label>
            </div>

        </div>


        <!-- Botones -->
        <div class="form-row align-items-center" style="margin-top: 10px; margin-bottom: 5px;">
            <a style="margin-left: 30%;">  <button  class="btn btn-primary" id="botonrestablecer" onclick="javascript:RestablecerTabla();">Restablecer Tabla</button> </a>
            

            <!-- Botón Exportar PDF -->
            <div class="container col-4" style="margin-left: 10%;">
                <form action="{{route('generarreporteproductos')}}" method="get">
                    <button type="submit"  class="btn btn-outline-dark" >Guardar listado en PDF</button>
                </form>
            </div>

        </div>
        
        
    </div>

        <!-- Alerta -->
    <div class="container"  id="contentalert" style="display: none;">
        <div class="alert alert-danger alert-dismissible" role="alert" style="margin-top: 1%" >
            <input class="form-control-plaintext" value = "" id="textoalerta" disabled>    
            <button type="button" class="close" onclick="javascript:OcultarAlerta();" style="margin-top: 5px">
            <span>&times;</span>
            </button>
        </div>
    </div>

    <!-- Tabla -->
    <div class="container">

        <table class="table table-hover" id="tablaproductos">
            <thead  class="thead-dark">
                <tr>
                    <th scope="col"> Código de barra </th>
                    <th scope="col"> Marca </th>
                    <th scope="col"> Presentación </th>
                    <th scope="col"> Nombre del proveedor </th>
                    <th scope="col"> Categoría </th>
                    <th scope="col"> Descripción </th>
                    <th scope="col"> Precio costo </th>
                    <th scope="col"> Precio venta </th>
                    <th scope="col"> Imprimir código </th>
                </tr>
            </thead>
            <tbody>
                @foreach ( $registroproductos as $productos )
                    <tr>
                        <td>{{ $productos-> codigo }}</td>
                        <td>{{ $productos-> nombremarca }}</td>
                        <td>{{ $productos-> nombrepresentacion }}</td>
                        <td>{{ $productos-> nombrecompleto }}</td>
                        <td>{{ $productos-> nombrecategoria }}</td>
                        <td>{{ $productos-> descripcion }}</td>
                        <td>{{ $productos-> preciocosto }}</td>
                        <td>{{ $productos-> precioventa }}</td>
                        <td>
                            <a href="{{ url('/imprimircodigobarra/'.$productos->codigo ) }}">
                                <button type="submit" class="btn btn-info"> Imprimir </button>
                            </a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>

    </div>


@endsection