@if( Auth::user()->Nivel == 1 )

    @extends('home')

    @section('title')
        Descontinuación de producto
    @endsection


    @section('contenido')

        <script src="{{ asset('js/FuncionesProductos/funcionesdescontinuarproducto.js') }}">                
            //
        </script>


        @if(Session::has('success'))
            <div class="alert alert-success">
                {{session('success')}}
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            </div>
        @endif

        <!-- INFORMACION PRODUCTO -->
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header"  align="center">{{ __('Descontinuar producto') }}</div>

                        <div class="card-body">

                            {!!Form::open(array('name' => 'formproducto', 'url'=>'cambiarestadoproducto/'.$producto->Codigo,'method'=>'PUT' ,'autocomplete'=>'off'))!!}

                                <!-- Imagen -->
                                <div class="form-group row">
                                    {!! Form::label( 'imagen','Imagen del producto', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                    <div class="col-md-6" style="margin-left: 10%">
                                        <img width="150px" src="/images/{{ ( $producto->imagen ) }}" onerror="imgErrorProducto( this );">
                                    </div>
                                </div>

                                <!-- Código de barras -->
                                <div class="form-group row">
                                    {!! Form::label( 'codigo','Código de barras', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                    <div class="col-md-6">
                                        <input type="text" id="codigo" name="codigo" class="form-control" value="{{ ( $producto->Codigo ) }}" readonly >
                                    </div>
                                </div>

                                <!-- Descripción del producto -->
                                <div class="form-group row" >
                                    {!! Form::label( 'descripcion','Descripción del producto', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                    <div class="col-md-6">
                                    <input type="text" id="descripcion" name="descripcion" class="form-control" value="{{ ( $producto->Descripcion ) }}" readonly>
                                    </div>
                                </div>

                                <!-- Marca del producto -->
                                <div class="form-group row">
                                    {!! Form::label('idmarca', 'Marca del producto', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                    <div class="col-md-6">
                                        <input type="text" id="idmarca" name="idmarca" class="form-control"
                                        <?php
                                            foreach( $marcas as $todasmarcas ) {
                                                if( $producto->idMarca == $todasmarcas->idMarca ) {
                                                    ?>
                                                        value=" {{ ($todasmarcas->Descripcion) }}"
                                                    <?php
                                                }
                                            }
                                        ?>
                                        readonly>
                                    </div>
                                </div>

                                <!-- Presentación del producto -->
                                <div class="form-group row">
                                    {!! Form::label('idpresentacion', 'Presentación del producto', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                    <div class="col-md-6">
                                        <input type="text" id="idpresentacion" name="idpresentacion" class="form-control"
                                            <?php
                                                foreach( $presentaciones as $todaspresentaciones ) {
                                                    if( $producto->idPresentacion == $todaspresentaciones->idPresentacion ) {
                                                        ?>
                                                            value=" {{ ($todaspresentaciones->Medida) }}"
                                                        <?php
                                                    }
                                                }
                                            ?>
                                            readonly>
                                    </div>
                                </div>
                                

                                <!-- Proveedor del producto -->
                                <div class="form-group row">
                                  <label for="idproveedor" class="col-md-4 col-form-label text-md-right">{{ __('Proveedor del producto ')}}</label>
                                  
                                    <div class="col-md-6">
                                        <input type="text" id="idproveedor" name="idproveedor" class="form-control"
                                            <?php
                                                foreach( $proveedores as $todosproveedores ) {
                                                    if( $producto->idProveedor == $todosproveedores->idproveedor ) {
                                                        ?>
                                                            value=" {{ $todosproveedores->nombrecompleto }} "
                                                        <?php
                                                    }
                                                }
                                            ?>
                                            readonly>
                                    </div>
                                </div>

                                <!-- Categoria del producto -->
                                <div class="form-group row">
                                    {!! Form::label('idcategoria', 'Categoria del producto', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                    <div class="col-md-6">
                                        <input type="text" id="idcategoria" name="idcategoria" class="form-control"
                                            <?php
                                                foreach( $categorias as $todascategorias ) {
                                                    if( $producto->idCategoria == $todascategorias->idCategoria ) {
                                                        ?>
                                                            value=" {{ $todascategorias->Nombre }} "
                                                        <?php
                                                    }
                                                }
                                            ?>
                                            readonly>
                                    </div>
                                </div>

                                <!-- Precio de costo del producto -->
                                <div class="form-group row">
                                    {!! Form::label('preciocosto','Precio de costo del producto', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                    <div class="col-md-6">
                                        <input type="text" id="preciocosto" name="preciocosto" class="form-control" value="<?php echo bcadd( $producto->PrecioCosto , '0', 2); ?>" readonly>
                                    </div>
                                </div>

                                    <!-- Precio de venta del producto -->
                                <div class="form-group row">
                                    {!! Form::label('precioventa','Precio de venta del producto', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                    <div class="col-md-6">
                                        <input type="text" id="precioventa" name="precioventa" class="form-control" value="<?php echo bcadd( $producto->PrecioVenta , '0', 2); ?>" readonly>
                                    </div>
                                </div>

                                    <!-- Stock mínimo -->
                                <div class="form-group row">
                                    {!! Form::label('stockminimo','Cantidad mínima en inventario', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                    <div class="col-md-6">
                                        <input type="text" id="stockminimo" name="stockminimo" class="form-control" value="{{ $producto->Stockminimo }}" readonly >
                                    </div>
                                </div>

                                <!-- Impuesto del producto -->
                                <div class="form-group row">
                                    {!! Form::label('impuesto','IVA' , array( 'class' => 'col-md-4 col-form-label text-md-right' )) !!}
                                    <div class="col-md-6">
                                        <input type="text" id="impuesto" name="impuesto" class="form-control" readonly
                                        <?php
                                            if( $producto->IVA == 1 ) {
                                                ?>
                                                value="SÍ"
                                                <?php
                                            } else {
                                            ?>
                                            value="NO"
                                            <?php
                                            }
                                        ?>
                                        >
                                    </div>
                                </div>

                            {!!Form::close()!!}

                            <!-- BOTONES -->
                            <div class="form-group row">
                                <div class="col-md-4 col-form-label text-md-right">
                                    <button name="botonformdescontinuar" value="Descontinuar" type="submit" class="btn btn-success" style="margin-right:50px" formnovalidate onclick="javascript:MostrarModalConfirmacionEliminacion();">Descontinuar</button>
                                </div>
                                <div class="col-md-4 col-form-label text-md-right">
                                    <button name="botonformdescontinuar" value="Cancelar" type="submit" class="btn btn-danger" style="margin-left:250px" formnovalidate onclick="javascript:Regresar();">Cancelar</button>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        

        <br><br>

        <!-- ModalConfirmacionEliminacion -->
        <div class="modal" tabindex="-1" role="dialog" id="ModalConfirmacionEliminacion">
            <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                <div class="modal-content modal-lg">

                    <!--Header-->
                    <div class="modal-header" id="modalheader" style="background: rgb(33, 37, 41)">
                        <h5 id="textostringheader" class="heading" style="margin-left: 25%; color: white; text-align: center;">CONFIRMACIÓN DE ELIMINACIÓN</h5>

                        <button type="button" class="close" onclick="javascript:DismissModalConfirmacionEliminacion();" data-dismiss="modal" aria-label="Close">
                            <span  aria-hidden="true" style="color: rgb(255, 255, 255)" >&times;</span>
                        </button>
                    </div>

                    <!--Body-->
                    <div class="modal-body" id="modalbody">
                        <div class="row">
                            <div class="col">
                                <p id="textoerrorbody" style="text-align: center;"></p>
                            </div>
                        </div>

                    </div>

                    <!--Footer-->
                    <div class="modal-footer" id="modalfooter">
                        <button type="button" class="btn btn-primary" data-dismiss="modal" id="botonEliminar" onclick="javascript:EliminarProducto();">ELIMINAR</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal" id="botonCancelar" onclick="javascript:DismissModalConfirmacionEliminacion();" style="margin-left: 72%;">CANCELAR</button>
                    </div>

                </div>
            </div>
        </div>

    @endsection

@endif
