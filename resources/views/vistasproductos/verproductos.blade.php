@if( Auth::user()->Nivel == 1 )

    @extends('home')

        @section('title')
            Listado producto registrar promoción
        @endsection

    @section('contenido')


        @if(Session::has('success'))
            <div class="alert alert-success">
                {{session('success')}}
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            </div>
        @elseif( Session::has('warning'))
            <div class="alert alert-warning">
                {{session('warning')}}
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            </div>
        @elseif( Session::has('danger'))
            <div class="alert alert-danger">
                {{session('danger')}}
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            </div>
        @endif

        <!-- Buscador -->
        <table  class="table table-responsive table-striped vertabla" style="margin: auto; width: 975px; ">
            <tr>
            <td>
                <div class="panel panel-success" style="margin: auto; width: 500px; border-collapse: separate; ">

                    <form action="Seleccionarproducto" method="get" onsubmit="return showLoad()">

                        <input class="input" size="35"type="text" name="descripcion" class="form-control" placeholder="Código / Descripción / Marca" required="required">
                        <button type="submit" class="btn btn-success">BUSCAR</button>
                        <br>
                    </form>
                    </td>
                    <td>
                        <a href="{{url('Seleccionarproducto')}}" class="btn btn-warning">Restaurar búsqueda</a>
                    </td>
                </div>
            </tr>
        </table>

            <br>
            <br>

        @if (isset($buscar))
            <div class="container">
                <div class="row justify-content-center">

                    <table class="table table-hover">
                        <thead  class="thead-dark">
                            <tr>
                                <th scope="col"> Código de barra </th>
                                <th scope="col"> Marca </th>
                                <th scope="col"> Presentación </th>
                                <th scope="col"> Nombre del proveedor </th>
                                <th scope="col"> Categoría </th>
                                <th scope="col"> Descripción </th>
                                <th scope="col"> Precio costo </th>
                                <th scope="col"> Precio venta </th>                              
                                <?php
                                    if( Auth::user()->Nivel == 1 ) {
                                        ?>
                                            <th scope="col" colspan="3" style="text-align: center;"> Acciones </th>
                                        <?php
                                    }
                                ?>
                            </tr>
                            <tbody>
                                @foreach($buscar as $productos)
                                    <tr>
                                        <td>{{ $productos-> codigo }}</td>
                                        <td>{{ $productos-> nombremarca }}</td>
                                        <td>{{ $productos-> nombrepresentacion }}</td>
                                        <td>{{ $productos-> nombrecompleto }}</td>
                                        <td>{{ $productos-> nombrecategoria }}</td>
                                        <td>{{ $productos-> descripcion }}</td>
                                        <td> <?php echo bcadd( $productos->preciocosto , '0', 2); ?> </td>
                                        <td> <?php echo bcadd( $productos->precioventa , '0', 2); ?> </td>

                                        <?php
                                            if( Auth::user()->Nivel == 1 ) {
                                                ?>

                                                    <td>
                                                        <a href="{{ url('/regitrarpromo/'.$productos->codigo ) }}">
                                                            <button type="submit" class="btn btn-info"> Oferta </button>
                                                        </a>
                                                    </td>

                                                    <td>
                                                        <a href="{{ url('/registrardescuento/'.$productos->codigo ) }}">
                                                            <button type="submit" class="btn btn-info"> Descuento </button>
                                                        </a>
                                                    </td>
                                                    
                                                    <td>
                                                        <a href="{{ url('/producto2/'.$productos->codigo ) }}">
                                                            <button type="submit" class="btn btn-info"> Paquete </button>
                                                        </a>
                                                    </td>

                                                <?php
                                            }
                                        ?>
                                    </tr>
                                @endforeach
                            </tbody>
                    </table>

                    <center>{{ $buscar->appends(Request::only('Codigo','Descripcion'))->links() }}</center>

                </div>
            </div>
        @endif


    @endsection

@endif
