@if( Auth::user()->Nivel == 1 )

    @extends('home')

    @section('title')
        Búsqueda de producto
    @endsection

    @section('contenido')

        <script src="{{ asset('js/FuncionesProductos/funcionesbusquedaproducto.js') }}">
            //
        </script>

        @if(Session::has('success'))
            <div class="alert alert-success">
                {{session('success')}}
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            </div>
        @elseif( Session::has('warning'))
            <div class="alert alert-warning">
                {{session('warning')}}
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            </div>
        @elseif( Session::has('danger'))
            <div class="alert alert-danger">
                {{session('danger')}}
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            </div>
        @endif

        <!-- DIV -->
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">                    
                        @if( $operacion == 'descontinuar' )
                            <div class="card-header"  align="center">{{ __('Búsqueda descontinuar producto') }}</div>
                        @elseif( $operacion == 'modificar' )
                            <div class="card-header"  align="center">{{ __('Búsqueda modificar producto') }}</div>                        
                        @endif
                            <div class="card-body">

                                    <!-- Buscar producto por código de barras -->
                                    <div class="form-group"  style="margin-bottom: 10px">
                                        <div class="form-check"   style="margin-bottom: 10px">
                                            {!! Form::label( 'codigo','Buscar producto por código de barras', array( 'class' => 'col-md-8 col-form-label', 'onclick' => 'cambiarcheckcodigo2(this);' ) ) !!}                            
                                        </div>

                                        <div class="form-group"  id="contentcodigo">
                                            <input type="text" id="codigo" name="codigo" class="form-control-sm" value="{{  old('codigo')  }}" pattern="^[0-9]{1,13}$"
                                                oninvalid="setCustomValidity('El código de barras debe constar únicamente de números y tener como máximo 13 digitos')"
                                                onkeypress="return ( event.charCode >= 48 && event.charCode <= 57 )"
                                                oninput="setCustomValidity('')" style="margin-left: 35%">
                                        </div>
                                    </div>
                                    

                                    <div class="form-group row" style="margin-top: 20px">
                                        <div class="col-md-4 col-form-label text-md-right">
                                            @if( $operacion == 'descontinuar' )                                            
                                                <button name="botonbusquedaproducto" value="BusquedaDescontinuar" type="submit" class="btn btn-success" style="margin-right:50px" onclick="javascript:IrDescontinuarProducto();">Buscar</button>
                                            @elseif( $operacion == 'modificar' )
                                                <button name="botonbusquedaproducto" value="BusquedaModificar" type="submit" class="btn btn-success" style="margin-right:50px" onclick="javascript:IrModificarProducto();">Buscar</button>
                                            @endif
                                        </div>
                                        <div class="col-md-4 col-form-label text-md-right">
                                            <a href="/home">
                                                <button name="botonbusquedaproducto" value="Cancelar" type="submit" class="btn btn-danger" style="margin-left:250px" formnovalidate>Cancelar</button>
                                            </a>
                                        </div>
                                    </div>

                            </div>
                    </div>
                </div>
            </div>
        </div>

    @endsection

@endif