@if( Auth::user()->Nivel == 1 )
    
    @extends('home')

    @section('title')
        Eliminación de empleado
    @endsection

    @section('contenido')

        <script src="{{ asset('js/FuncionesEmpleados/funcionesdescontinuarempleado.js') }}">
            //
        </script>


        @if(Session::has('success'))
            <div class="alert alert-success">
                {{session('success')}}
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            </div>
        @endif
        
        <!-- INFORMACION EMPLEADO -->
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header"  align="center">{{ __('Eliminación de empleado') }}</div>
                            <div class="card-body">

                                {!!Form::open(array('name' => 'formempleado', 'url'=>'cambiarestadoempleado/'.$empleado->idEmpleado,'method'=>'PUT' ,'autocomplete'=>'off'))!!}

                                    <fieldset class="border p-2">
                                        <legend class="w-auto"> Datos personales </legend>


                                        <!-- ID del empleado -->
                                        <div class="form-group row" >
                                            {!! Form::label( 'nombre','ID del empleado', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                            <div class="col-md-6">
                                            <input type="text" id="idempleado" name="idempleado" class="form-control" readonly value="{{ ( $empleado->idEmpleado ) }}">
                                            </div>
                                        </div>

                                        <!-- Nombre del empleado -->
                                        <div class="form-group row" >
                                            {!! Form::label( 'nombre','Nombre del empleado', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                            <div class="col-md-6">
                                            <input type="text" id="nombre" name="nombre" class="form-control"  readonly value="{{ ( $empleado->Nombre ) }}">
                                            </div>
                                        </div>

                                        <!-- Apellido Paterno del empleado -->
                                        <div class="form-group row" >
                                            {!! Form::label( 'apellidopat','Apellido paterno del empleado', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                            <div class="col-md-6">
                                            <input type="text" id="apellidopat" name="apellidopat" class="form-control" readonly value="{{ ( $empleado->ApellidoPat ) }}">
                                            </div>
                                        </div>

                                        <!-- Apellido Materno del empleado -->
                                        <div class="form-group row" >
                                            {!! Form::label( 'apellidomat','Apellido materno del empleado', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                            <div class="col-md-6">
                                            <input type="text" id="apellidomat" name="apellidomat" class="form-control"  readonly value="{{ ( $empleado->ApellidoMat ) }}">
                                            </div>
                                        </div>

                                        <!-- Fecha de nacimiento del empleado -->
                                        <div class="form-group row" >
                                            {!! Form::label( 'fechanac','Fecha de nacimiento del empleado', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                            <div class="col-md-6">
                                            <input type="text" id="fechanac" name="fechanac" class="form-control" readonly value="{{ ( $empleado->FechaNac ) }}">
                                            </div>
                                        </div>

                                        <!-- Sexo del empleado -->
                                        <div class="form-group row" >
                                            {!! Form::label( 'sexo','Sexo del empleado', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                            <div class="col-md-6">
                                                <input type="text" name="sexo" id="sexo"  class="form-control" readonly
                                                    <?php
                                                        if( $empleado->Sexo == 'H' ) {
                                                            ?>
                                                            value = "Hombre";
                                                            <?php
                                                        } else {
                                                            ?>
                                                            value = "Mujer";
                                                            <?php
                                                        }
                                                    ?>
                                                >
                                            </div>
                                        </div>

                                        <!-- Curp Materno del empleado -->
                                        <div class="form-group row" >
                                            {!! Form::label( 'curp','Curp del empleado', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                            <div class="col-md-6">
                                            <input type="text" id="curp" name="curp" class="form-control" readonly value="{{ ( $empleado->Curp ) }}">
                                            </div>
                                        </div>

                                        <!-- Teléfono del empleado  -->
                                        <div class="form-group row">
                                            {!! Form::label( 'telefono','Teléfono del empleado', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                            <div class="col-md-6">
                                                <input type="text" id="telefono" name="telefono" class="form-control" readonly value="{{ ( $empleado->Telefono ) }}" >
                                            </div>
                                        </div>


                                        <!-- R.F.C. del empleado  -->
                                        <div class="form-group row">
                                            {!! Form::label( 'rfc','RFC del empleado', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                            <div class="col-md-6">
                                                <input type="text" id="rfc" name="rfc" class="form-control" readonly value="{{ ( $empleado->Rfc ) }}" >
                                            </div>
                                        </div>


                                        <!-- Correo del empleado  -->
                                        <div class="form-group row">
                                            {!! Form::label( 'correo','Correo del empleado', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                            <div class="col-md-6">
                                                <input type="text" id="correo" name="correo" class="form-control" readonly value="{{ ( $empleado->Correo ) }}" >
                                            </div>
                                        </div>

                                    </fieldset>

                                    <fieldset class="border p-2">
                                        <legend class="w-auto"> Datos del domicilio </legend>

                                        <!-- Calle -->
                                        <div class="form-group row" >
                                            {!! Form::label( 'calle','Nombre de la calle', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                            <div class="col-md-6">
                                            <input type="text" id="calle" name="calle"  class="form-control" readonly value="{{ ( $empleado->Calle ) }}">
                                            </div>
                                        </div>

                                        <!-- Número -->
                                        <div class="form-group row" >
                                            {!! Form::label( 'numero','Número del domicilio', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                            <div class="col-md-6">
                                            <input type="number" id="numero" name="numero" class="form-control" readonly  value="{{ ( $empleado->Numero ) }}">
                                            </div>
                                        </div>

                                        <!-- Código Postal -->
                                        <div class="form-group row" >
                                            {!! Form::label( 'codigop','Código Postal del domicilio', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                            <div class="col-md-6">
                                            <input type="text" id="codigop" name="codigop" class="form-control" readonly  value="{{ ( $empleado->CodigoP ) }}">
                                            </div>
                                        </div>

                                        <!-- Colonia -->
                                        <div class="form-group row" >
                                            {!! Form::label( 'colonia','Nombre de la colonia', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                            <div class="col-md-6">
                                            <input type="text" id="colonia" name="colonia" class="form-control" readonly value="{{ ( $empleado->Colonia ) }}">
                                            </div>
                                        </div>

                                        <!-- Municipio -->
                                        <div class="form-group row" >
                                            {!! Form::label( 'municipio','Nombre del municipio', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                            <div class="col-md-6">
                                            <input type="text" id="municipio" name="municipio" class="form-control" readonly value="{{ ( $empleado->Municipio ) }}">
                                            </div>
                                        </div>

                                        <!-- Ciudad -->
                                        <div class="form-group row" >
                                            {!! Form::label( 'ciudad','Nombre de la ciudad', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                            <div class="col-md-6">
                                            <input type="text" id="ciudad" name="ciudad" class="form-control" readonly value="{{ ( $empleado->Ciudad ) }}">
                                            </div>
                                        </div>


                                    </fieldset>
                                    <br>

                                {!!Form::close()!!}

                                <!-- BOTONES -->
                                <div class="form-group row">
                                    <div class="col-md-4 col-form-label text-md-right">
                                        <button name="botonformdescontinuar" value="Descontinuar" type="submit" class="btn btn-success" style="margin-right:50px" formnovalidate onclick="javascript:MostrarModalConfirmacionEliminacion();">Eliminar</button>
                                    </div>
                                    <div class="col-md-4 col-form-label text-md-right">
                                        <button name="botonformdescontinuar" value="Cancelar" type="submit" class="btn btn-danger" style="margin-left:250px" formnovalidate onclick="javascript:Regresar();" >Cancelar</button>
                                    </div>
                                </div>

                            </div>
                    </div>
                </div>
            </div>
        </div>
        

        <br><br>

        <!-- ModalConfirmacionEliminacion -->
        <div class="modal" tabindex="-1" role="dialog" id="ModalConfirmacionEliminacion">
            <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                <div class="modal-content modal-lg">

                    <!--Header-->
                    <div class="modal-header" id="modalheader" style="background: rgb(33, 37, 41)">
                        <h5 id="textostringheader" class="heading" style="margin-left: 25%; color: white; text-align: center;">CONFIRMACIÓN DE ELIMINACIÓN</h5>

                        <button type="button" class="close" onclick="javascript:DismissModalConfirmacionEliminacion();" data-dismiss="modal" aria-label="Close">
                            <span  aria-hidden="true" style="color: rgb(255, 255, 255)" >&times;</span>
                        </button>
                    </div>

                    <!--Body-->
                    <div class="modal-body" id="modalbody">
                        <div class="row">
                            <div class="col">
                                <p id="textoerrorbody" style="text-align: center;"></p>
                            </div>
                        </div>

                    </div>

                    <!--Footer-->
                    <div class="modal-footer" id="modalfooter">
                        <button type="button" class="btn btn-primary" data-dismiss="modal" id="botonEliminar" onclick="javascript:EliminarEmpleado();">ELIMINAR</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal" id="botonCancelar" onclick="javascript:DismissModalConfirmacionEliminacion();" style="margin-left: 72%;">CANCELAR</button>
                    </div>

                </div>
            </div>
        </div>
        
        <br><br><br>

    @endsection

@endif
