@if( Auth::user()->Nivel == 1 )

    @extends('home')


    @section('title')
        Registro de empleado
    @endsection

    @section('contenido')

        <style>

            strong {

                color: red;
                font-size: 20px;

            }

        </style>

        <script>

            window.onload = function( e ) {
                
                var fechacasolimite = new Date();                                
                var fechacasolimitemin = new Date();         

                fechacasolimite.setFullYear( fechacasolimite.getFullYear() - 18 );
                fechacasolimite.setDate( fechacasolimite.getDate() - 1 );
                fechacasolimitemin.setFullYear( fechacasolimitemin.getFullYear() - 90 );

                fechacasolimite = new Date( fechacasolimite ).toISOString().split("T")[0];
                fechacasolimitemin = new Date( fechacasolimitemin ).toISOString().split("T")[0];
                
                //console.log("FECHA: ", fechacasolimite );                
                document.getElementById('fechanac').max = fechacasolimite;
                document.getElementById('fechanac').min = fechacasolimitemin;

            }

            function generarcurp() {

                var curpgenerada = "";

                if( $('#nombre').val() != "" && $('#apellidopat').val() != ""  ) {

                    var primeraletraapellidopat;
                    var vocalapellidopat;
                    var primeraletraapellidomat;
                    var primeraletranombre;
                    var fechanacimientodigitos;
                    var caractersexo;
                    var entidaddenacimiento;
                    var consonanteinternaapellidopat;
                    var consonanteinternaapellidomat;
                    var consonanteinternanombre;
                    var  pcl = ""; // primerascuatroletras
                    var expreg = /(BACA|BAKA|BUEI|BUEY|CACA|CACO|CAGA|CAGO|CAKA|CAKO|COGE|COGI|COJA|COJE|COJI|COJO|COLA|CULO|FALO|FETO|GETA|GUEI|GUEY|JETA|JOTO|KACA|KACO|KAGA|KAGO|KAKA|KAKO|KOGE|KOGI|KOJA|KOJE|KOJI|KOJO|KOLA|KULO|LILO|LOCA|LOCO|LOKA|LOKO|MAME|MAMO|MEAR|MEAS|MEON|MIAR|MION|MOCO|MOKO|MULA|MULO|NACA|NACO|PEDA|PEDO|PENE|PIPI|PITO|POPO|PUTA|PUTO|QULO|RATA|ROBA|ROBE|ROBO|RUIN|SENO|TETA|VACA|VAGA|VAGO|VAKA|VUEI|VUEY|WUEI|WUEY)/;

                    var textonombre = document.getElementById('nombre').value.toUpperCase();
                        textonombre = textonombre.replace("Á","A");
                        textonombre = textonombre.replace("É","E");
                        textonombre = textonombre.replace("Í","I");
                        textonombre = textonombre.replace("Ó","O");
                        textonombre = textonombre.replace("Ú","U");

                    var textoapellidopat = document.getElementById('apellidopat').value.toUpperCase();
                        textoapellidopat = textoapellidopat.replace("Á","A");
                        textoapellidopat = textoapellidopat.replace("É","E");
                        textoapellidopat = textoapellidopat.replace("Í","I");
                        textoapellidopat = textoapellidopat.replace("Ó","O");
                        textoapellidopat = textoapellidopat.replace("Ú","U");

                    var textoapellidomat = document.getElementById('apellidomat').value.toUpperCase();
                        textoapellidomat = textoapellidomat.replace("Á","A");
                        textoapellidomat = textoapellidomat.replace("É","E");
                        textoapellidomat = textoapellidomat.replace("Í","I");
                        textoapellidomat = textoapellidomat.replace("Ó","O");
                        textoapellidomat = textoapellidomat.replace("Ú","U");

                            // PRIMER CARACTER - Primera letra primer apellido.
                    var auxletra = textoapellidopat.substr(0,1);
                    if( auxletra == 'Ñ' ) {
                        primeraletraapellidopat = 'X';
                    } else {
                        primeraletraapellidopat = textoapellidopat.substr(0,1);
                    }

                            //  SEGUNDO CARACTER - Primera vocal interna del primer apellido.
                    for( var i = 1; i< textoapellidopat.length; i++ ) {
                        if( textoapellidopat.charAt(i) == 'A' || textoapellidopat.charAt(i) == 'E' || textoapellidopat.charAt(i) == 'I' || textoapellidopat.charAt(i) == 'O' || textoapellidopat.charAt(i) == 'U' ) {
                                vocalapellidopat = textoapellidopat.charAt(i);
                                break;
                        }
                    }

                    if( vocalapellidopat == null ) {
                        vocalapellidopat = 'X';
                    }

                            //  TERCER CARACTER - Primera letra del segundo apellido.
                    if( textoapellidomat != "" ) {
                        auxletra = textoapellidomat.substr(0,1);
                        if( auxletra == 'Ñ' ) {
                            primeraletraapellidomat = 'X';
                        } else {
                            primeraletraapellidomat = textoapellidomat.substr(0,1);
                        }
                    } else {
                        primeraletraapellidomat = 'X';
                    }


                            //  CUARTO CARACTER - Primera letra del nombre
                    var arraynombres = textonombre.split(" ");

                    if( arraynombres.length > 1 ) {
                        var nombreingresado = arraynombres[0];
                        if( nombreingresado == 'MARIA' || nombreingresado == 'JOSE' ) {
                            auxletra = arraynombres[1].substr(0,1);
                            if( auxletra == 'Ñ' ) {
                                primeraletranombre = 'X';
                            } else {
                                primeraletranombre =  arraynombres[1].substr(0,1);
                            }
                        } else {
                            auxletra = arraynombres[0].substr(0,1);
                            if( auxletra == 'Ñ' ) {
                                primeraletranombre = 'X';
                            } else {
                                primeraletranombre = arraynombres[0].substr(0,1);
                            }
                        }
                    } else {
                        auxletra = arraynombres[0].substr(0,1);
                        if( auxletra == 'Ñ' ) {
                            primeraletranombre = 'X';
                        } else {
                            primeraletranombre = arraynombres[0].substr(0,1);
                        }
                    }

                    pcl = pcl.concat (primeraletraapellidopat, vocalapellidopat,
                        primeraletraapellidomat, primeraletranombre );

                    var resexpreg = expreg.test( pcl );
                    if( resexpreg ) {
                        pcl = "";
                        pcl = pcl.concat (primeraletraapellidopat, "X",
                        primeraletraapellidomat, primeraletranombre );
                    }


                            // QUINTO AL DECIMO CARACTER - año, mes, día.
                    //fecha = document.getElementById('fechanac').value;
                    fecha = $('#fechanac').val();

                    arrayfecha = fecha.split("-");

                    digitoanio = arrayfecha[0];
                    digitoanio = digitoanio.substr( 2, digitoanio.length );
                    digitomes = arrayfecha[1];
                    digitodia = arrayfecha[2];

                    if( fecha == "" ) {
                        fechanacimientodigitos = "******";
                    } else {
                        fechanacimientodigitos = "";
                        fechanacimientodigitos = fechanacimientodigitos.concat( digitoanio, digitomes, digitodia );
                    }

                        //ONCEAVO CARACTER - H -> hombre / M -> mujer

                    if( document.getElementById('sexoh').checked == true ) {
                        caractersexo = "H";
                    } else if( document.getElementById('sexom').checked == true ) {
                        caractersexo = "M";
                    } else {
                        caractersexo = "-";
                    }

                            //  DOCEAVO Y TRECEAVO CARACTER - Letra inicial y última consonante del estado de nacimiento.
                    entidaddenacimiento = 'OC';

                            //CATORCEAVO CARACTER - Primera consonante interna del primer apellido
                    for( var i = 1; i< textoapellidopat.length; i++ ) {
                        if( textoapellidopat.charAt(i) == 'A' || textoapellidopat.charAt(i) == 'E' || textoapellidopat.charAt(i) == 'I' || textoapellidopat.charAt(i) == 'O' || textoapellidopat.charAt(i) == 'U' ) {
                        } else {
                            consonanteinternaapellidopat = textoapellidopat.charAt(i);
                            break;

                        }
                    }
                    if( consonanteinternaapellidopat == null || consonanteinternaapellidopat == 'Ñ' ) {
                        consonanteinternaapellidopat = 'X';
                    }

                            //QUINCEAVO CARACTER - Primera consonante interna del segundo apellido
                    for( var i = 1; i< textoapellidomat.length; i++ ) {
                        if( textoapellidomat.charAt(i) == 'A' || textoapellidomat.charAt(i) == 'E' || textoapellidomat.charAt(i) == 'I' || textoapellidomat.charAt(i) == 'O' || textoapellidomat.charAt(i) == 'U' ) {
                        } else {
                            consonanteinternaapellidomat = textoapellidomat.charAt(i);
                            break;
                        }
                    }
                    if( consonanteinternaapellidomat == null || consonanteinternaapellidomat == 'Ñ' ) {
                        consonanteinternaapellidomat = 'X';
                    }

                            //DIECISEISAVO CARACTER - Primera consonante interna del nombre
                    arraynombres = textonombre.split(" ");

                    if( arraynombres.length > 1 ) {
                        var nombreingresado = arraynombres[0];
                        if( nombreingresado == 'MARIA' || nombreingresado == 'JOSE' ) {
                            auxnombre = arraynombres[1];
                            var contador = 0;
                            for( var i = 0; i< auxnombre.length; i++ ) {
                                if( auxnombre.charAt(i) == 'A' || auxnombre.charAt(i) == 'E' || auxnombre.charAt(i) == 'I' || auxnombre.charAt(i) == 'O' || auxnombre.charAt(i) == 'U' ) {
                                } else {
                                    contador++;
                                    if( contador == 1 ) {
                                        consonanteinternanombre = auxnombre.charAt(i);
                                        break;
                                    }
                                }
                            }
                        } else {
                            for( var i = 1; i< textonombre.length; i++ ) {
                                if( textonombre.charAt(i) == 'A' || textonombre.charAt(i) == 'E' || textonombre.charAt(i) == 'I' || textonombre.charAt(i) == 'O' || textonombre.charAt(i) == 'U' ) {
                                } else {
                                    consonanteinternanombre = textonombre.charAt(i);
                                    break;
                                }
                            }
                        }
                    } else {
                        for( var i = 1; i< textonombre.length; i++ ) {
                            if( textonombre.charAt(i) == 'A' || textonombre.charAt(i) == 'E' || textonombre.charAt(i) == 'I' || textonombre.charAt(i) == 'O' || textonombre.charAt(i) == 'U' ) {
                            } else {
                                consonanteinternanombre = textonombre.charAt(i);
                                break;
                            }
                        }
                    }

                    if( consonanteinternanombre == null || consonanteinternanombre == 'Ñ' ) {
                        consonanteinternanombre = 'X';
                    }


                    curpgenerada = curpgenerada.concat( pcl, fechanacimientodigitos,
                        caractersexo, entidaddenacimiento, consonanteinternaapellidopat,
                        consonanteinternaapellidomat, consonanteinternanombre,"00" );

                    if( curpgenerada.includes("-") ) {
                        curpgenerada = "";
                    } else {
                        document.getElementById('curp').value = curpgenerada;
                        mayusculas();
                    }

                }

            }

            function generarrfc() {

                var rfcgenerado = "";

                if( $('#nombre').val() != "" && $('#apellidopat').val() != ""  ) {

                    var primeraletraapellidopat;
                    var segundaletraapellidopat;
                    var primeraletraapellidomat;
                    var primeraletranombre;
                    var fechanacimientodigitos;
                    var primerasletrasrazonsocial;

                    var textonombre = document.getElementById('nombre').value.toUpperCase();
                        textonombre = textonombre.replace("Á","A");
                        textonombre = textonombre.replace("É","E");
                        textonombre = textonombre.replace("Í","I");
                        textonombre = textonombre.replace("Ó","O");
                        textonombre = textonombre.replace("Ú","U");

                    var textoapellidopat = document.getElementById('apellidopat').value.toUpperCase();
                        textoapellidopat = textoapellidopat.replace("Á","A");
                        textoapellidopat = textoapellidopat.replace("É","E");
                        textoapellidopat = textoapellidopat.replace("Í","I");
                        textoapellidopat = textoapellidopat.replace("Ó","O");
                        textoapellidopat = textoapellidopat.replace("Ú","U");

                    var textoapellidomat = document.getElementById('apellidomat').value.toUpperCase();
                        textoapellidomat = textoapellidomat.replace("Á","A");
                        textoapellidomat = textoapellidomat.replace("É","E");
                        textoapellidomat = textoapellidomat.replace("Í","I");
                        textoapellidomat = textoapellidomat.replace("Ó","O");
                        textoapellidomat = textoapellidomat.replace("Ú","U");

                    //Personas físicas 13 carácteres ( 4 de iniciales de apellidos y nombre, 6 fecha y 3 homoclave ).
                    //Personas morales 12 carácteres( 4 -> "Primeras letras de su razón social", 6 fecha, 3 homoclave ).


                        // PRIMER Y SEGUNDO CARACTER - Primeras dos letras del apellido paterno.
                    primeraletraapellidopat = textoapellidopat.substr(0,1);
                    segundaletraapellidopat = textoapellidopat.substr(1,1);


                            //  TERCER CARACTER - Primera letra del apellido materno.
                    primeraletraapellidomat = textoapellidomat.substr(0,1);


                            //  CUARTO CARACTER - Primera letra del nombre
                    primeraletranombre = textonombre.substr(0,1);


                            // QUINTO AL DECIMO CARACTER - año, mes, día.
                    //fecha = document.getElementById('fechanac').value;
                    fecha = $('#fechanac').val();

                    arrayfecha = fecha.split("-");

                    digitoanio = arrayfecha[0];
                    digitoanio = digitoanio.substr( 2, digitoanio.length );
                    digitomes = arrayfecha[1];
                    digitodia = arrayfecha[2];

                    if( fecha == "" ) {
                        fechanacimientodigitos = "******";
                    } else {
                        fechanacimientodigitos = "";
                        fechanacimientodigitos = fechanacimientodigitos.concat( digitoanio, digitomes, digitodia );
                    }

                    rfcgenerado = rfcgenerado.concat( primeraletraapellidopat, segundaletraapellidopat,
                    primeraletraapellidomat, primeraletranombre, fechanacimientodigitos, "000");

                    if( rfcgenerado.includes("-") ) {
                        rfcgenerado = "";
                    } else {
                        document.getElementById('rfc').value = rfcgenerado;
                        mayusculas();
                    }

                }

            }

            function mayusculas() {

                var textocurp = document.getElementById('curp').value;
                textocurp = textocurp.toUpperCase();

                var textorfc = document.getElementById('rfc').value;
                textorfc = textorfc.toUpperCase();

                document.getElementById('curp').value = textocurp;
                document.getElementById('rfc').value = textorfc;

            }

            function consultar() {

                var endpoint_sepomex  = "http://api-sepomex.hckdrk.mx/query/";
                var method_sepomex = 'info_cp/';
                //var cp = "09810";
                var cp = document.getElementById('codigop').value;
                var variable_string = '?type=simplified';
                var url = endpoint_sepomex + method_sepomex + cp + variable_string;

                $.get( url ).done( function( data ) {
                    var content = JSON.parse( JSON.stringify( data ) );

                    //console.log( content );
                    //document.getElementById('colonia').value = content.response.asentamiento;
                    var opcionescolonia = content.response.asentamiento;

                    var selectcolonias = document.getElementById( 'colonia' );

                    for ( var i = selectcolonias.length; i >= 0; i--) {
                        selectcolonias.remove(i);
                    }

                    if( opcionescolonia.length > 0 ) {
                        var optioncolonia = document.createElement('option');
                                optioncolonia.text = "Seleccione una colonia";
                                optioncolonia.value = "";
                                selectcolonias.add( optioncolonia );

                        for( var i = 0; i < opcionescolonia.length; i++ ) {
                            var optioncolonia = document.createElement( 'option' );
                                optioncolonia.text = opcionescolonia[i];
                                optioncolonia.value = opcionescolonia[i];
                                selectcolonias.add( optioncolonia );

                        }

                    }

                    if( content.response.ciudad == "" && content.response.municipio != "" ) {

                        document.getElementById('ciudad').value = "Oaxaca de Juárez";
                        document.getElementById('municipio').value = content.response.municipio;

                    } else if( content.response.ciudad != "" && content.response.municipio != "" ) {

                        document.getElementById('ciudad').value = content.response.ciudad;
                        document.getElementById('municipio').value = content.response.municipio;

                    }
                }).catch(function( error) {

                    var selectcolonias = document.getElementById( 'colonia' );

                    for ( var i = selectcolonias.length; i >= 0; i--) {
                        selectcolonias.remove(i);
                    }

                    var optioncolonia = document.createElement('option');
                        optioncolonia.text = "Seleccione una colonia";
                        optioncolonia.value = "";
                        selectcolonias.add( optioncolonia );

                    optioncolonia = document.createElement('option');
                        optioncolonia.text = "DOMICILIO CONOCIDO";
                        optioncolonia.value = "DOMICILIO CONOCIDO";
                        selectcolonias.add( optioncolonia );

                    document.getElementById('ciudad').value = "";
                    document.getElementById('municipio').value = "";

                });

            }

        </script>

        @if(Session::has('success'))
            <div class="alert alert-success">
                {{session('success')}}
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            </div>
        @elseif(Session::has('danger'))
            <div class="alert alert-danger">
                {{session('danger')}}
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            </div>
        @elseif(Session::has('warning'))
            <div class="alert alert-warning">
                {{session('warning')}}
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            </div>
        @endif


        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header"  align="center">{{ __('Registro de empleado') }}</div>
                            <div class="card-body">

                                {!!Form::open(array('name' => 'formempleado', 'url'=>'insertarempleado','method'=>'POST' , 'files'=>true , 'enctype'=>'multipart/form-data' , 'autocomplete'=>'off' ))!!}

                                <fieldset class="border p-2">
                                    <legend class="w-auto"> Datos de la cuenta </legend>

                                        <!-- Nombre de usuario -->
                                        <div class="form-group row">
                                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Nombre de usuario ')}}<strong>*</strong></label>

                                            <div class="col-md-6">
                                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}"
                                                 placeholder="Ingrese el usuario del empleado"   required autocomplete="name" autofocus>

                                                @error('name')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>


                                        <!-- Avatar  del usuario -->
                                        <div class="form-group row">
                                            {!! Form::label( 'avatar','Imagen del usuario', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                            <div class="col-md-6">
                                            <input type="file" id="avatar" name="avatar"  value="{{ old('avatar')}}" >
                                            </div>
                                        </div>

                                        <!-- Email del usuario -->
                                        <div class="form-group row">
                                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('Correo Electrónico ') }}<strong>*</strong></label>

                                            <div class="col-md-6">
                                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}"
                                                 placeholder="Ingrese el correo del empleado"required autocomplete="email"
                                                    oninvalid="setCustomValidity('La dirección de correo electrónico es obligatoria')"
                                                    oninput="setCustomValidity('')">

                                                @error('email')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>

                                        <!-- Password del usuario -->
                                        <div class="form-group row">
                                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Contraseña ') }}<strong>*</strong></label>

                                            <div class="col-md-6">
                                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password"
                                                 placeholder="Ingrese la contraseña del empleado" required autocomplete="new-password">

                                                @error('password')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>

                                        <!-- Confirmación password del usuario -->
                                        <div class="form-group row">
                                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirmar contraseña ') }}<strong>*</strong></label>

                                            <div class="col-md-6">
                                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation"
                                                 placeholder="Confirme la contraseña del empleado" required autocomplete="new-password">
                                            </div>
                                        </div>

                                </fieldset>

                                <fieldset class="border p-2">
                                    <legend class="w-auto"> Datos personales </legend>

                                        <!-- Nombre del empleado -->
                                    <div class="form-group row" >
                                      <label for="nombre" class="col-md-4 col-form-label text-md-right">{{ __('Nombre(s) del empleado ') }}<strong>*</strong></label>
                                        <div class="col-md-6">
                                        <input type="text" id="nombre" name="nombre"  placeholder="Ingrese el nombre del empleado" class="form-control"
                                            value="{{ old('nombre') }}" maxlength="45" minlength="1" required pattern="[\w\sÁ-ÿ\u00C0-\u00FF]{1,45}"
                                            onkeypress="return !(event.charCode >= 48 && event.charCode <= 57)"

                                            oninvalid="setCustomValidity('El nombre es obligatorio y debe tener como máximo 45 carácteres')"
                                            oninput="setCustomValidity('')">
                                        </div>
                                    </div>

                                        <!-- Apellido Paterno del empleado -->
                                    <div class="form-group row" >
                                      <label for="apellidopat" class="col-md-4 col-form-label text-md-right">{{ __('Apellido paterno ') }}<strong>*</strong></label>
                                      <div class="col-md-6">
                                        <input type="text" id="apellidopat" name="apellidopat"  placeholder="Ingrese el apellido paterno del empleado" class="form-control"
                                            value="{{ old('apellidopat') }}" maxlength="45" minlength="1" required pattern="[\w\sÁ-ÿ\u00C0-\u00FF]{1,45}"
                                            onkeypress="return !(event.charCode >= 48 && event.charCode <= 57)"
                                            oninvalid="setCustomValidity('El apellido paterno es obligatorio y debe tener como máximo 45 carácteres')"
                                            oninput="setCustomValidity('')">
                                        </div>
                                    </div>

                                        <!-- Apellido Materno del empleado -->
                                    <div class="form-group row" >
                                        {!! Form::label( 'apellidomat','Apellido materno ', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                        <div class="col-md-6">
                                        <input type="text" id="apellidomat" name="apellidomat"  placeholder="Ingrese el apellido materno del empleado" class="form-control"
                                            value="{{ old('apellidomat') }}" maxlength="45" minlength="1" pattern="[\w\sÁ-ÿ\u00C0-\u00FF]{1,45}"
                                            onkeypress="return !(event.charCode >= 48 && event.charCode <= 57)"
                                            oninvalid="setCustomValidity('El apellido materno debe tener como máximo 45 carácteres')"
                                            oninput="setCustomValidity('')">
                                        </div>
                                    </div>

                                        <!-- Fecha de nacimiento del empleado -->
                                    <div class="form-group row" >
                                      <label for="fechanac" class="col-md-4 col-form-label text-md-right">{{ __('Fecha de nacimiento ') }}<strong>*</strong></label>

                                                                              <div class="col-md-6">
                                        <input type="date" id="fechanac" name="fechanac" class="form-control"
                                            max="2000-12-31" min="1940-01-01" required
                                            value="{{ old('fechanac') }}">
                                        </div>
                                    </div>

                                        <!-- Sexo del empleado -->
                                    <div class="form-group row" >
                                        {!! Form::label( 'sexo','Sexo del empleado', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                        <div class="col-md-6">
                                            <label> <input type="radio" name="sexo" id="sexoh" value="H" value="{{ old('sexo') }}"
                                            oninvalid="setCustomValidity('El sexo es obligatorio, selecciona una de las opciones')"
                                            oninput="setCustomValidity('')"  style="margin-left: 20px"
                                            <?php
                                                if( old('sexo') == 'H' ) {
                                            ?>
                                                    checked = "true";
                                            <?php
                                                }
                                            ?>
                                            > Hombre </label>
                                            <label> <input type="radio" name="sexo" id="sexom" value="M" value="{{ old('sexo') }}" style="margin-left: 20px"
                                            <?php
                                                if( old('sexo') == 'M' ) {
                                            ?>
                                                    checked = "true";
                                            <?php
                                                }
                                            ?>
                                            > Mujer </label>
                                        </div>
                                    </div>

                                        <!-- Curp  del empleado -->
                                    <div class="form-group row" >

                                        <label for="curp" class="col-md-4 col-form-label text-md-right">{{ __('Curp del empleado ') }}<strong>*</strong></label>
                                        <div class="col-md-6">
                                        <input type="text" id="curp" name="curp"  placeholder="Ingrese la curp del empleado" class="form-control"
                                            value="{{ old('curp') }}" maxlength="18"
                                          required  pattern="^([A-Z]{1})([AEIOU]{1})([A-Z]{2})([0-9]{2})(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1])([HM]{1})([AS|BC|BS|CC|CS|CH|CL|CM|DF|DG|GT|GR|HG|JC|MC|MN|MS|NT|NL|OC|PL|QT|QR|SP|SL|SR|TC|TS|TL|VZ|YN|ZS|NE]{2})([^A|E|I|O|U]{3})([A-Z\d]{1})([0-9]{1})$"
                                            oninvalid="setCustomValidity('La curp es obligatoria, debe respetar el formato de la curp y tener 18 carácteres')"
                                            oninput="setCustomValidity('')"
                                            onfocus="javascript:generarcurp()"
                                            >
                                        </div>
                                    </div>

                                    <!-- Teléfono del empleado  -->
                                    <div class="form-group row">

                                        <label for="telefono" class="col-md-4 col-form-label text-md-right">{{ __('Teléfono del empleado ') }}<strong>*</strong></label>
                                        <div class="col-md-6">
                                            <input type="text" id="telefono" name="telefono" placeholder="Ingrese el número telefónico del empleado" class="form-control"
                                            value="{{old('telefono')}}" maxlength="12" minlength="1" pattern="[\d]{1,12}"
                                            onkeypress="return (event.charCode >= 48 && event.charCode <= 57)"
                                            oninvalid="setCustomValidity('El telefono es obligatorio y debe tener como máximo 12 digitos')"
                                            oninput="setCustomValidity('')" >
                                        </div>
                                    </div>

                                    <!-- R.F.C. del empleado  -->
                                    <div class="form-group row">
                                        {!! Form::label( 'rfc','RFC del empleado', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                        <div class="col-md-6">
                                            <input type="text" id="rfc" name="rfc" placeholder="Ingrese el rfc del empleado" class="form-control"
                                            value="{{old('rfc')}}" maxlength="45" minlength="1"
                                            pattern="^([A-ZÑ\x26]{3,4}([0-9]{2})(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1]))((-)?([A-Z\d]{3}))?$"
                                            oninvalid="setCustomValidity('El rfc debe respetar el formato del rfc y puede tener un máximo de 13 carácteres')"
                                            oninput="setCustomValidity('')"
                                            onfocus="javascript:generarrfc()">
                                        </div>
                                    </div>

                                </fieldset>

                                <fieldset class="border p-2">
                                    <legend class="w-auto"> Datos del domicilio </legend>

                                        <!-- Código Postal -->
                                    <div class="form-group row" >

                                        <label for="codigop" class="col-md-4 col-form-label text-md-right">{{ __('Código Postal ') }}<strong>*</strong></label>
                                        <div class="col-md-6">
                                        <input type="text" id="codigop" name="codigop"  placeholder="Ingrese el código postal del domicilio" class="form-control"
                                            value="{{ old('codigop') }}" maxlength="5" minlength="1" required pattern="[\d]{1,5}"
                                            onkeypress="return (event.charCode >= 48 && event.charCode <= 57)"
                                            oninvalid="setCustomValidity('El código postal del domicilio es obligatorio y debe tener como máximo 5 digitos')"
                                            oninput="setCustomValidity('')"
                                            onfocusout="javascript:consultar()">
                                        </div>
                                    </div>

                                        <!-- Calle -->
                                    <div class="form-group row" >
                                        {!! Form::label( 'calle','Nombre de la calle', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                        <div class="col-md-6">
                                        <input type="text" id="calle" name="calle"  placeholder="Ingrese el nombre de la calle" class="form-control"
                                            value="{{ old('calle') }}" maxlength="45" minlength="1"  pattern="[\w\sÁ-ÿ\u00C0-\u00FF]{1,45}"
                                            oninvalid="setCustomValidity('La nombre de la calle es obligatorio y debe tener como máximo 45 carácteres')"
                                            oninput="setCustomValidity('')">
                                        </div>
                                    </div>

                                        <!-- Número -->
                                    <div class="form-group row" >
                                        {!! Form::label( 'numero','Número del domicilio', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                        <div class="col-md-6">
                                        <input type="number" id="numero" name="numero"  placeholder="Ingrese el número del domicilio" class="form-control"
                                            value="{{ old('numero') }}" min="1" max="9999"  pattern="[\d]{1,4}"
                                            oninvalid="setCustomValidity('El número del domicilio es obligatorio y debe tener como máximo 4 digitos')"
                                            oninput="setCustomValidity('')">
                                        </div>
                                    </div>

                                        <!-- Colonia -->
                                    <div class="form-group row" >
                                      <label for="colonia" class="col-md-4 col-form-label text-md-right">{{ __('Nombre de la colonia ') }}<strong>*</strong></label>
                                              <div class="col-md-6">
                                            <select id="colonia" name="colonia" class="form-control" required>
                                                <option value = ""> Seleccione una colonia </option>
                                            </select>
                                        </div>
                                    </div>

                                        <!-- Municipio -->
                                    <div class="form-group row" >
                                        {!! Form::label( 'municipio','Nombre del municipio', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                        <div class="col-md-6">
                                        <input type="text" id="municipio" name="municipio"  placeholder="Ingrese el nombre del municipio" class="form-control"
                                            value="{{ old('municipio') }}" maxlength="45" minlength="1"  pattern="[\w\sÁ-ÿ\u00C0-\u00FF]{1,45}"
                                            oninvalid="setCustomValidity('El nombre del municipio es obligatorio y debe tener como máximo 45 carácteres')"
                                            oninput="setCustomValidity('')">
                                        </div>
                                    </div>

                                        <!-- Ciudad -->
                                    <div class="form-group row" >
                                        {!! Form::label( 'ciudad','Nombre de la ciudad', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                        <div class="col-md-6">
                                        <input type="text" id="ciudad" name="ciudad"  placeholder="Ingrese el nombre de la ciudad" class="form-control"
                                            value="{{ old('ciudad') }}" maxlength="45" minlength="1"  pattern="[\w\sÁ-ÿ\u00C0-\u00FF]{1,45}"

                                            oninvalid="setCustomValidity('El nombre de la ciudad es obligatorio y debe tener como máximo 45 carácteres')"
                                            oninput="setCustomValidity('')">
                                        </div>
                                    </div>


                                </fieldset>
                                <br>

                                <div class="form-group row">
                                    <div class="col-md-4 col-form-label text-md-right">
                                        <button name="botonformempleado" value="Registrar" type="submit" class="btn btn-success" style="margin-right:50px">Registrar</button>
                                    </div>
                                    <div class="col-md-4 col-form-label text-md-right">
                                        <button name="botonformempleado" value="Cancelar" type="submit" class="btn btn-danger" style="margin-left:250px" formnovalidate>Cancelar</button>
                                    </div>
                                </div>

                                {!!Form::close()!!}

                            </div>
                    </div>
                </div>
            </div>
        </div>

        <br><br><br>

    @endsection

@endif
