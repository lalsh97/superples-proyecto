@if( Auth::user()->Nivel == 1 )

    @extends('home')

    @section('title')
        Modificación de la información de empleado
    @endsection

    @section('contenido')

        <style>

            strong {
            
                color: red;
                font-size: 20px;

            }
            
        </style>

        <script>

            function consultar() {

                var endpoint_sepomex  = "http://api-sepomex.hckdrk.mx/query/";
                var method_sepomex = 'info_cp/';
                //var cp = "09810";
                var cp = document.getElementById('codigop').value;
                var variable_string = '?type=simplified';
                var url = endpoint_sepomex + method_sepomex + cp + variable_string;

                $.get( url ).done( function( data ) {
                    var content = JSON.parse( JSON.stringify( data ) );

                    //console.log( content );
                    //document.getElementById('colonia').value = content.response.asentamiento;
                    var opcionescolonia = content.response.asentamiento;

                    var selectcolonias = document.getElementById( 'colonia' );

                    for ( var i = selectcolonias.length; i >= 0; i--) {
                        selectcolonias.remove(i);
                    }

                    if( opcionescolonia.length > 0 ) {
                        var optioncolonia = document.createElement('option');
                                optioncolonia.text = "Seleccione una colonia";
                                optioncolonia.value = "";
                                selectcolonias.add( optioncolonia );

                        for( var i = 0; i < opcionescolonia.length; i++ ) {
                            var optioncolonia = document.createElement( 'option' );
                                optioncolonia.text = opcionescolonia[i];
                                optioncolonia.value = opcionescolonia[i];
                                selectcolonias.add( optioncolonia );

                        }

                    }

                    if( content.response.ciudad == "" && content.response.municipio != "" ) {

                        document.getElementById('ciudad').value = "Oaxaca de Juárez";
                        document.getElementById('municipio').value = content.response.municipio;

                    } else if( content.response.ciudad != "" && content.response.municipio != "" ) {

                        document.getElementById('ciudad').value = content.response.ciudad;
                        document.getElementById('municipio').value = content.response.municipio;

                    }
                }).catch(function( error) {

                    var selectcolonias = document.getElementById( 'colonia' );

                    for ( var i = selectcolonias.length; i >= 0; i--) {
                        selectcolonias.remove(i);
                    }

                    var optioncolonia = document.createElement('option');
                        optioncolonia.text = "Seleccione una colonia";
                        optioncolonia.value = "";
                        selectcolonias.add( optioncolonia );

                    optioncolonia = document.createElement('option');
                        optioncolonia.text = "DOMICILIO CONOCIDO";
                        optioncolonia.value = "DOMICILIO CONOCIDO";
                        selectcolonias.add( optioncolonia );

                    document.getElementById('ciudad').value = "";
                    document.getElementById('municipio').value = "";

                });

            }

        </script>

        @if(Session::has('success'))
            <div class="alert alert-success">
                {{session('success')}}
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            </div>
        @endif

        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header"  align="center">{{ __('Modificación de información de empleado') }}</div>
                            <div class="card-body">

                            {!!Form::open(array('name' => 'formempleado', 'url'=>'actualizarinfoempleado/'.$empleado->idEmpleado,'method'=>'PUT' ,'autocomplete'=>'off'))!!}

                                <!-- DATOS PERSONALES -->
                                <fieldset class="border p-2">
                                    <legend class="w-auto"> Datos personales </legend>


                                        <!-- ID del empleado -->
                                    <div class="form-group row" >
                                        {!! Form::label( 'nombre','ID del empleado', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                        <div class="col-md-6">
                                        <input type="text" id="idempleado" name="idempleado" class="form-control" readonly value="{{ ( $empleado->idEmpleado ) }}">
                                        </div>
                                    </div>

                                        <!-- Nombre del empleado -->
                                    <div class="form-group row" >
                                        {!! Form::label( 'nombre','Nombre del empleado', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                        <div class="col-md-6">
                                        <input type="text" id="nombre" name="nombre" class="form-control"  readonly value="{{ ( $empleado->Nombre ) }}">
                                        </div>
                                    </div>

                                        <!-- Apellido Paterno del empleado -->
                                    <div class="form-group row" >
                                        {!! Form::label( 'apellidopat','Apellido paterno del empleado', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                        <div class="col-md-6">
                                        <input type="text" id="apellidopat" name="apellidopat" class="form-control" readonly value="{{ ( $empleado->ApellidoPat ) }}">
                                        </div>
                                    </div>

                                        <!-- Apellido Materno del empleado -->
                                    <div class="form-group row" >
                                        {!! Form::label( 'apellidomat','Apellido materno del empleado', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                        <div class="col-md-6">
                                        <input type="text" id="apellidomat" name="apellidomat" class="form-control"  readonly value="{{ ( $empleado->ApellidoMat ) }}">
                                        </div>
                                    </div>

                                        <!-- Fecha de nacimiento del empleado -->
                                    <div class="form-group row" >
                                        {!! Form::label( 'fechanac','Fecha de nacimiento del empleado', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                        <div class="col-md-6">
                                        <input type="text" id="fechanac" name="fechanac" class="form-control" readonly value="{{ ( $empleado->FechaNac ) }}">
                                        </div>
                                    </div>

                                        <!-- Sexo del empleado -->
                                    <div class="form-group row" >
                                        {!! Form::label( 'sexo','Sexo del empleado', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                        <div class="col-md-6">
                                            <input type="text" name="sexo" id="sexo"  class="form-control" readonly
                                            <?php
                                                if( $empleado->Sexo == 'H' ) {
                                                    ?>
                                                    value = "Hombre";
                                                    <?php
                                                } else {
                                                    ?>
                                                    value = "Mujer";
                                                    <?php
                                                }
                                            ?>
                                        >
                                        </div>
                                    </div>

                                        <!-- Curp Materno del empleado -->
                                    <div class="form-group row" >
                                        {!! Form::label( 'curp','Curp del empleado', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                        <div class="col-md-6">
                                        <input type="text" id="curp" name="curp" class="form-control" readonly value="{{ ( $empleado->Curp ) }}">
                                        </div>
                                    </div>

                                    <!-- Teléfono del empleado  -->
                                    <div class="form-group row">
                                      <label for="telefono" class="col-md-4 col-form-label text-md-right">{{ __('Teléfono del empleado ') }}<strong>*</strong></label>

                                        <div class="col-md-6">
                                            <input type="text" id="telefono" name="telefono" placeholder="Ingrese el número telefónico del empleado" class="form-control"
                                            value="{{ ( $empleado->Telefono ) }}" maxlength="12" minlength="1" pattern="[\d]{1,12}"
                                              onkeypress="return (event.charCode >= 48 && event.charCode <= 57)"
                                            oninvalid="setCustomValidity('El telefono es obligatorio y debe tener como máximo 12 digitos')"
                                            oninput="setCustomValidity('')" >
                                        </div>
                                    </div>


                                    <!-- R.F.C. del empleado  -->
                                    <div class="form-group row">
                                        {!! Form::label( 'rfc','RFC del empleado', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                        <div class="col-md-6">
                                          <input type="text" id="rfc" name="rfc" placeholder="Ingrese el rfc del empleado" class="form-control"
                                          value="{{ ($empleado->Rfc)}}" maxlength="45" minlength="1" readonly
                                          pattern="^(([A-Z]{4})|([A-Z]{3}))([0-9]{2})(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1])([\w]{3})$"
                                          oninvalid="setCustomValidity('El rfc debe respetar el formato del rfc y puede tener un máximo de 13 carácteres')"
                                          oninput="setCustomValidity('')"
                                          onfocus="javascript:generarrfc()">
                                        </div>
                                    </div>


                                    <!-- Correo del empleado  -->
                                    <div class="form-group row">
                                      <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('Correo Electrónico ') }}<strong>*</strong></label>

                                        <div class="col-md-6">
                                            <input type="email" id="correo" name="correo" placeholder="Ingrese el correo electrónico del empleado" class="form-control"
                                            value="{{ ( $empleado->Correo ) }}" maxlength="45" minlength="1" required pattern="[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*@[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{1,5}"
                                            oninvalid="setCustomValidity('El correo electrónico es obligatorio y debe tener como máximo 45 carácteres')"
                                            oninput="setCustomValidity('')" >


                                        </div>
                                    </div>

                                </fieldset>

                                <!-- DATOS DEL DOMICILIO -->
                                <fieldset class="border p-2">
                                    <legend class="w-auto"> Datos del domicilio </legend>

                                        <!-- Código Postal -->
                                    <div class="form-group row" >
                                      <label for="codigop" class="col-md-4 col-form-label text-md-right">{{ __('Código Postal ') }}<strong>*</strong></label>

                                        <div class="col-md-6">
                                        <input type="text" id="codigop" name="codigop"  placeholder="Ingrese el código postal del domicilio" class="form-control"
                                            value="{{($empleado->CodigoP )}}" maxlength="5" minlength="1" required pattern="[\d]{1,5}"
                                            oninvalid="setCustomValidity('El código postal del domicilio es obligatorio y debe tener como máximo 5 digitos')"
                                            onkeypress="return (event.charCode >= 48 && event.charCode <= 57)"
                                            oninput="setCustomValidity('')"
                                            onfocusout="javascript:consultar()">
                                        </div>
                                    </div>

                                        <!-- Calle -->
                                    <div class="form-group row" >
                                        {!! Form::label( 'calle','Nombre de la calle', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                        <div class="col-md-6">
                                        <input type="text" id="calle" name="calle"  placeholder="Ingrese el nombre de la calle" class="form-control"
                                            value="{{ ( $empleado->Calle ) }}" maxlength="45" minlength="1"  pattern="[\w\sÁ-ÿ\u00C0-\u00FF]{1,45}"
                                            oninvalid="setCustomValidity('La nombre de la calle es obligatorio y debe tener como máximo 45 carácteres')"
                                            oninput="setCustomValidity('')">
                                        </div>
                                    </div>

                                        <!-- Número -->
                                    <div class="form-group row" >
                                        {!! Form::label( 'numero','Número del domicilio', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                        <div class="col-md-6">
                                        <input type="number" id="numero" name="numero"  placeholder="Ingrese el numero del domicilio" class="form-control"
                                        value="{{ ( $empleado->Numero ) }}" min="1" max="9999"  pattern="[\d]{1,4}"
                                            oninvalid="setCustomValidity('El número del domicilio es obligatorio y debe tener como máximo 4 digitos')"
                                            oninput="setCustomValidity('')">
                                        </div>
                                    </div>

                                        <!-- Colonia -->
                                    <div class="form-group row" >
                                      <label for="colonia" class="col-md-4 col-form-label text-md-right">{{ __('Nombre de la colonia ') }}<strong>*</strong></label>
                                                <div class="col-md-6">
                                            <select id="colonia" name="colonia" class="form-control" >
                                                <option value = "{{ ( $empleado->Colonia ) }}"> {{($empleado->Colonia )}} </option>
                                            </select>
                                        </div>
                                    </div>

                                        <!-- Municipio -->
                                    <div class="form-group row" >
                                        {!! Form::label( 'municipio','Nombre del municipio', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                        <div class="col-md-6">
                                        <input type="text" id="municipio" name="municipio"  placeholder="Ingrese el nombre del municipio" class="form-control"
                                            value="{{ ( $empleado->Municipio ) }}" maxlength="45" minlength="1"  pattern="[\w\sÁ-ÿ\u00C0-\u00FF]{1,45}"
                                            oninvalid="setCustomValidity('El nombre del municipio es obligatorio y debe tener como máximo 45 carácteres')"
                                            oninput="setCustomValidity('')">
                                        </div>
                                    </div>

                                        <!-- Ciudad -->
                                    <div class="form-group row" >
                                        {!! Form::label( 'ciudad','Nombre de la ciudad', array( 'class' => 'col-md-4 col-form-label text-md-right' ) ) !!}
                                        <div class="col-md-6">
                                        <input type="text" id="ciudad" name="ciudad"  placeholder="Ingrese el nombre de la ciudad" class="form-control"
                                            value="{{ ( $empleado->Ciudad ) }}" maxlength="45" minlength="1"  pattern="[\w\sÁ-ÿ\u00C0-\u00FF]{1,45}"
                                            oninvalid="setCustomValidity('El nombre de la ciudad es obligatorio y debe tener como máximo 45 carácteres')"
                                            oninput="setCustomValidity('')">
                                        </div>
                                    </div>


                                </fieldset>
                                <br>

                                <div class="form-group row">
                                    <div class="col-md-4 col-form-label text-md-right">
                                        <button name="botonformmodificar" value="Modificar" type="submit" class="btn btn-success" style="margin-right:50px">Modificar</button>
                                    </div>
                                    <div class="col-md-4 col-form-label text-md-right">
                                        <button name="botonformmodificar" value="Cancelar" type="submit" class="btn btn-danger" style="margin-left:250px" formnovalidate>Cancelar</button>
                                    </div>
                                </div>

                            {!!Form::close()!!}

                            </div>
                    </div>
                </div>
            </div>
        </div>
        
        
        <br><br><br>

    @endsection

@endif
