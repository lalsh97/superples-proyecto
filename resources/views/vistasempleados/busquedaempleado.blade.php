@if( Auth::user()->Nivel == 1 )

    @extends('home')

    @section('title')
        Búsqueda de empleado
    @endsection

    @section('contenido')

        <script src="{{ asset('js/FuncionesEmpleados/funcionesbusquedaempleado.js') }}">
            //
        </script>

        @if(Session::has('success'))
            <div class="alert alert-success">
                {{session('success')}}
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            </div>
        @elseif( Session::has('warning'))
            <div class="alert alert-warning">
                {{session('warning')}}
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            </div>
        @elseif( Session::has('danger'))
            <div class="alert alert-danger">
                {{session('danger')}}
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            </div>
        @endif


        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        @if( $operacion == 'descontinuar' )
                            <div class="card-header"  align="center">{{ __('Búsqueda eliminar empleado') }}</div>
                        @elseif( $operacion == 'modificar' )
                            <div class="card-header"  align="center">{{ __('Búsqueda modificar información empleado') }}</div>
                        @endif

                        <div class="card-body">

                                <!-- Buscar empleado por id -->
                                <div class="form-group"  style="margin-bottom: 10px">
                                    <div class="form-check"   style="margin-bottom: 10px">
                                        {!! Form::label( 'idempleado','Buscar empleado por id', array( 'class' => 'col-md-8 col-form-label' ) ) !!}                            
                                    </div>

                                    <div class="form-group"  id="contentcodigo">
                                        <input type="number" id="idempleado" name="idempleado" class="form-control-sm" value="{{  old('idempleado')  }}" style="margin-left: 35%">
                                    </div>
                                </div>
                                

                                <div class="form-group row" style="margin-top: 20px">
                                    <div class="col-md-4 col-form-label text-md-right">
                                        @if( $operacion == 'descontinuar' )
                                            <button name="botonbuscarempleado" value="BusquedaDescontinuar" type="submit" class="btn btn-success" style="margin-right:50px" onclick="javascript:IrDescontinuarEmpleado()">Buscar</button>
                                        @elseif( $operacion == 'modificar' )
                                            <button name="botonbuscarempleado" value="BusquedaModificar" type="submit" class="btn btn-success" style="margin-right:50px" onclick="javascript:IrModificarEmpleado();">Buscar</button>
                                        @endif
                                    </div>
                                    <div class="col-md-4 col-form-label text-md-right">
                                        <a href="/home">
                                            <button name="botonbuscarempleado" value="Cancelar" type="submit" class="btn btn-danger" style="margin-left:250px">Cancelar</button>
                                        </a>
                                    </div>
                                </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <br><br><br>

    @endsection

@endif